# Rogue Arena
Rogue Arena is a first-person shooter (FPS) game based on idTech 2. It is a stand alone game, not a replacement or mod for Quake 2.

## Source Code
Rogue Arena's source code is a fork of the Quake2XP source code with contributions from the QFusion project.

## Goals
In addition to code refactoring and bug fixes, RA's goals include cleaning up the code and bringing it up to modern standards, improving overall engine architecture by providing much needed encapsulation and introducing new features such as a much more secure abstract filesystem, map header updates, etc.

### Changes from Q2XP
RA provides an updated and much more fully fleshed out texture material system (it's not foolproof but generally works), better code documentation and updated real-time light editor features.

Additionally, references to Quake 2 have been removed, limitations from Quake 2 have been removed and support for old file formats (such as PCX, WAL and CIN) have been entirely removed.

### Features
* *Virtual Filesystem*: Quake 2's filesystem functions have been rewritten from the ground up on top of [PhysicsFS](https://icculus.org/physfs/). This allows for completely transparent file operations in flat files and archives, vastly improves security ('.', '..' and symbolic links are explicitly disallowed) and support for several archive formats (including ZIP, 7z among others).

### Future Features
* Adding volumetric shadows and global illumination values for much better outdoor lighting.
* Support for MD3 models (partially done as of this writing) and possibly other model formats.
* Vastly improved mouse-driven GUI.
