
/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// cl_fx.c -- entity effects parsing and management

#include "client.h"
#include "cl_tempents.h"

#include "../common/def_muzzle.h"
#include "../common/string_util.h"


#include <string>
#include <vector>


const float MONSTERS_ATTACK_ATTENUATION = 0.1f;

void CL_BrassShells(vec3_t org, vec3_t dir, int count, bool mshell);
void CL_GunFire(vec3_t start, vec3_t end);
void CL_LogoutEffect(vec3_t org, int type);
void CL_ItemRespawnParticles(vec3_t org);
void CL_ParticleSmoke(vec3_t org, vec3_t dir, int count);

void S_fastsound_load(const std::string& input_name, ALuint bufferNum);
void S_fastsound_get_descriptors_pool(unsigned count, ALuint * descriptors_pool);
void S_fastsound_kill_descriptors_pool(unsigned count, ALuint * descriptors_pool);
void S_fastsound_queue(vec3_t origin, int entnum, int entchannel, ALuint bufferNum, float fvol, float attenuation, unsigned timeofs);


///\fixme Find a sane place for this
std::vector<std::string> fastsound_name;


// Resource descriptors
ALuint fastsound_descriptor[40];


/**
 * LIGHT STYLE MANAGEMENT
 */
typedef struct
{
	int length;
	float value[3];
	float map[MAX_QPATH];
} clightstyle_t;


clightstyle_t cl_lightstyle[MAX_LIGHTSTYLES];

int lastofs;


/**
 * 
 */
void CL_ClearLightStyles()
{
	memset(cl_lightstyle, 0, sizeof(cl_lightstyle));
	lastofs = -1;
}


/**
 * 
 */
void CL_RunLightStyles()
{
	int i = 0;
	clightstyle_t* ls = nullptr;

	int ofs = cl.time / 100;
	if (ofs == lastofs)
		return;
	
	lastofs = ofs;

	for (i = 0, ls = cl_lightstyle; i < MAX_LIGHTSTYLES; i++, ls++)
	{
		if (!ls->length)
		{
			ls->value[0] = ls->value[1] = ls->value[2] = 1.0;
			continue;
		}
		if (ls->length == 1)
			ls->value[0] = ls->value[1] = ls->value[2] = ls->map[0];
		else
			ls->value[0] = ls->value[1] = ls->value[2] = ls->map[ofs % ls->length];
	}
}


/**
 * 
 */
void CL_SetLightstyle(int i)
{
	int j, k;
	char* s = cl.configstrings[i + CS_LIGHTS];

	j = strlen(s);

	cl_lightstyle[i].length = j;

	for (k = 0; k < j; k++)
	{
		cl_lightstyle[i].map[k] = (float)(s[k] - 'a') / (float)('m' - 'a');
	}
}


/**
 * 
 */
void CL_AddLightStyles()
{
	int i;
	clightstyle_t *ls;

	for (i = 0, ls = cl_lightstyle; i < MAX_LIGHTSTYLES; i++, ls++)
		V_AddLightStyle(i, ls->value[0], ls->value[1], ls->value[2]);
}


/**
 * DLIGHT MANAGEMENT
 */

cdlight_t cl_dlights[MAX_DLIGHTS];

/**
 * 
 */
void CL_ClearDlights()
{
	memset(cl_dlights, 0, sizeof(cl_dlights));
}


/**
 * 
 */
cdlight_t *CL_AllocDlight(int key)
{
	int i;
	cdlight_t *dl;

	// first look for an exact key match
	if (key) {
		dl = cl_dlights;
		for (i = 0; i < MAX_DLIGHTS; i++, dl++)
		{
			if (dl->key == key) {
				memset(dl, 0, sizeof(*dl));
				dl->key = key;
				return dl;
			}
		}
	}
	// then look for anything else
	dl = cl_dlights;
	for (i = 0; i < MAX_DLIGHTS; i++, dl++)
	{
		if (dl->die < cl.time)
		{
			memset(dl, 0, sizeof(*dl));
			dl->key = key;
			return dl;
		}
	}

	dl = &cl_dlights[0];
	memset(dl, 0, sizeof(*dl));
	dl->key = key;
	return dl;
}


/**
 * 
 */
void CL_NewDlight(int key, vec3_t org, float r, float g, float b, float radius, float time)
{
	cdlight_t *dl;

	dl = CL_AllocDlight(key);
	dl->origin[0] = org[0];
	dl->origin[1] = org[1];
	dl->origin[2] = org[2];
	dl->color[0] = r;
	dl->color[0] = g;
	dl->color[0] = b;
	dl->radius = radius;
	dl->die = cl.time + time;
}


/**
 * 
 */
void CL_RunDLights()
{
	int i;
	cdlight_t *dl;

	dl = cl_dlights;
	for (i = 0; i < MAX_DLIGHTS; i++, dl++)
	{
		if (!dl->radius)
			continue;

		if (dl->die < cl.time)
		{
			dl->radius = 0;
			return;
		}
		dl->radius -= cls.frametime * dl->decay;
		if (dl->radius < 0)
			dl->radius = 0;
	}
}


/**
 * Fills the sound name table.
 * 
 * \note	Must match order of SoundID enumeration.
 */
void CL_fillSoundIdTable()
{
	fastsound_name.push_back("weapons/blastf1a.wav");
	fastsound_name.push_back("weapons/hyprbf1a.wav");
	fastsound_name.push_back("weapons/gunshot1.wav");
	fastsound_name.push_back("weapons/gunshot2.wav");
	fastsound_name.push_back("weapons/gunshot3.wav");
	fastsound_name.push_back("weapons/gunshot4.wav");
	fastsound_name.push_back("weapons/shotgun_1_fire.wav");
	fastsound_name.push_back("weapons/shotgun_1_reload.wav");
	fastsound_name.push_back("weapons/sshotf1b.wav");
	fastsound_name.push_back("weapons/railgf1a.wav");
	fastsound_name.push_back("weapons/rocklf1a.wav");
	fastsound_name.push_back("weapons/rocklr1b.wav");
	fastsound_name.push_back("weapons/grenlf1a.wav");
	fastsound_name.push_back("weapons/grenlr1b.wav");
	fastsound_name.push_back("weapons/bfg__f1y.wav");
	fastsound_name.push_back("misc/talk.wav");
	fastsound_name.push_back("ui/menu1.wav");
	fastsound_name.push_back("ui/menu2.wav");
	fastsound_name.push_back("ui/menu3.wav");
	fastsound_name.push_back("world/ric1.wav");
	fastsound_name.push_back("world/ric2.wav");
	fastsound_name.push_back("world/ric3.wav");
	fastsound_name.push_back("weapons/lashit.wav");
	fastsound_name.push_back("world/spark5.wav");
	fastsound_name.push_back("world/spark6.wav");
	fastsound_name.push_back("world/spark7.wav");
	fastsound_name.push_back("weapons/railgf1a.wav");
	fastsound_name.push_back("weapons/rocklx1a.wav");
	fastsound_name.push_back("weapons/grenlx1a.wav");
	fastsound_name.push_back("weapons/xpld_wat.wav");
	fastsound_name.push_back("world/lava1.wav");
	fastsound_name.push_back("misc/brass_shell.wav");
	fastsound_name.push_back("misc/debris.wav");
	fastsound_name.push_back("player/step1.wav");
	fastsound_name.push_back("player/step2.wav");
	fastsound_name.push_back("player/step3.wav");
	fastsound_name.push_back("player/step4.wav");
	fastsound_name.push_back("weapons/tesla.wav");
	fastsound_name.push_back("weapons/disrupthit.wav");
	fastsound_name.push_back("misc/radar_snd.wav");
}


/**
 * 
 */
void CL_fast_sound_init()
{
	// Register waves
	S_fastsound_get_descriptors_pool(SOUND_ID_COUNT, fastsound_descriptor);
	
	CL_fillSoundIdTable();

	for (int i = 0; i < SOUND_ID_COUNT; ++i)
	{
		S_fastsound_load(fastsound_name[i], fastsound_descriptor[i]);
	}

	cl_sfx_lashit = fastsound_descriptor[id_cl_sfx_lashit];
	cl_sfx_railg = fastsound_descriptor[id_cl_sfx_railg];
	cl_sfx_rockexp = fastsound_descriptor[id_cl_sfx_rockexp];
	cl_sfx_grenexp = fastsound_descriptor[id_cl_sfx_grenexp];
	cl_sfx_watrexp = fastsound_descriptor[id_cl_sfx_watrexp];

	cl_sfx_lava = fastsound_descriptor[id_cl_sfx_lava];
	cl_sfx_shell = fastsound_descriptor[id_cl_sfx_shell];
	cl_sfx_debris = fastsound_descriptor[id_cl_sfx_debris];

	cl_sfx_lightning = fastsound_descriptor[id_cl_sfx_lightning];
	cl_sfx_disrexp = fastsound_descriptor[id_cl_sfx_disrexp];
}


/**
 * 
 */
void CL_fast_sound_close()
{
	// Kill and free the waves
	S_fastsound_kill_descriptors_pool(SOUND_ID_COUNT, fastsound_descriptor);
}


/**
 * \fixme	JFC, break this up into smaller worker functions. Yikes.
 */
void CL_ParseMuzzleFlash()
{
	vec3_t fv, rv, smoke_origin, shell_brass, dir;
	cdlight_t *dl;
	int i, weapon, j;

	centity_t *pl;
	int silenced;
	float volume;
	vec3_t up;

	i = MSG_ReadShort(Net_Message());

	if (i < 1 || i >= MAX_EDICTS)
		// Com_Error (ERR_DROP, "CL_ParseMuzzleFlash: bad entity");
		Com_Error(ERR_DROP, "CL_ParseMuzzleFlash: bad entity - %i", i);

	weapon = MSG_ReadByte(Net_Message());

	silenced = weapon & MZ_SILENCED;
	weapon &= ~MZ_SILENCED;

	pl = &cl_entities[i];

	dl = CL_AllocDlight(i);


	VectorCopy(pl->current.origin, dl->origin);

	AngleVectors(pl->current.angles, fv, rv, up);
	VectorScaleAndAdd(dl->origin, fv, dl->origin, 18);
	VectorScaleAndAdd(dl->origin, rv, dl->origin, 16);
	VectorScaleAndAdd(dl->origin, up, dl->origin, 43);

	// shell brass and gun smoke origins
	if (cl.playernum == i - 1 && !cl_thirdPerson->value) //local player w/o third person view
	{
		VectorCopy(smoke_puff, smoke_origin);
		VectorCopy(shell_puff, shell_brass);
	}
	else
	{
		VectorScaleAndAdd(pl->current.origin, fv, shell_brass, 10);
		VectorScaleAndAdd(shell_brass, rv, shell_brass, 6);
		VectorScaleAndAdd(shell_brass, up, shell_brass, 21);

		VectorScaleAndAdd(pl->current.origin, fv, smoke_origin, 20);
		VectorScaleAndAdd(smoke_origin, rv, smoke_origin, 5);
		VectorScaleAndAdd(smoke_origin, up, smoke_origin, 18);
	}

	if (silenced)
		dl->radius = 100 + (rand() & 31);
	else
		dl->radius = 200 + (rand() & 31);

	dl->minlight = 32;
	dl->die = cl.time;			// + 0.1;

	if (silenced)
		volume = 0.2;
	else
		volume = 1;

	for (j = 0; j < 3; j++)
		dir[j] = fv[j] + rv[j] + up[j] * 2;

	switch (weapon)
	{
	case MZ_BLASTER:
		dl->color[0] = 1;
		dl->color[1] = 1;
		dl->color[2] = 0;
		S_fastsound(nullptr, i, CHAN_WEAPON, fastsound_descriptor[weapons_blastf1a], volume * 0.75, ATTN_WEAPON_LIGHT);
		break;

	case MZ_BLUEHYPERBLASTER:
		dl->color[0] = 0;
		dl->color[1] = 0;
		dl->color[2] = 1;
		S_fastsound(nullptr, i, CHAN_WEAPON, fastsound_descriptor[weapons_hyprbf1a], volume * 0.8, ATTN_WEAPON_LIGHT);
		break;

	case MZ_HYPERBLASTER:
		dl->color[0] = 1;
		dl->color[1] = 1;
		dl->color[2] = 0;
		S_fastsound(nullptr, i, CHAN_WEAPON, fastsound_descriptor[weapons_hyprbf1a], volume * 0.8, ATTN_WEAPON_LIGHT);
		break;

	case MZ_MACHINEGUN:
		dl->color[0] = 1;
		dl->color[1] = 1;
		dl->color[2] = 0;
		CL_ParticleGunSmoke(smoke_origin, vec3_origin, 1);
		CL_BrassShells(shell_brass, dir, 1, true);
		S_fastsound(nullptr, i, CHAN_WEAPON, fastsound_descriptor[weapons_gunshot1 + (rand() % 4)], volume, ATTN_WEAPON_LIGHT);
		break;

	case MZ_SHOTGUN:
		dl->color[0] = 1;
		dl->color[1] = 1;
		dl->color[2] = 0;
		CL_ParticleGunSmoke(smoke_origin, vec3_origin, 4);
		CL_BrassShells(shell_brass, dir, 1, false);
		S_fastsound(nullptr, i, CHAN_WEAPON, fastsound_descriptor[weapons_shotgun_1_fire], volume, ATTN_WEAPON_LIGHT);
		S_fastsound_queue(nullptr, i, CHAN_AUTO, fastsound_descriptor[weapons_shotgun_1_reload], volume, ATTN_WEAPON_LIGHT, 340);
		break;

	case MZ_SSHOTGUN:
		dl->color[0] = 1;
		dl->color[1] = 1;
		dl->color[2] = 0;
		CL_ParticleGunSmoke(smoke_origin, vec3_origin, 6);
		CL_BrassShells(shell_brass, dir, 2, false);
		S_fastsound(nullptr, i, CHAN_WEAPON, fastsound_descriptor[weapons_sshotf1b], volume * 0.9, ATTN_WEAPON_LIGHT);
		break;

	case MZ_CHAINGUN1:
		CL_ParticleGunSmoke(smoke_origin, vec3_origin, 1);
		CL_BrassShells(shell_brass, dir, 1, true);
		dl->radius = 200 + (rand() & 31);
		dl->color[0] = 1;
		dl->color[1] = 0.25;
		dl->color[2] = 0;
		S_fastsound(nullptr, i, CHAN_WEAPON, fastsound_descriptor[weapons_gunshot1 + (rand() % 4)], volume * 0.8, ATTN_WEAPON_LIGHT);
		break;

	case MZ_CHAINGUN2:
		CL_ParticleGunSmoke(smoke_origin, vec3_origin, 1);
		CL_BrassShells(shell_brass, dir, 1, true);
		dl->radius = 225 + (rand() & 31);
		dl->color[0] = 1;
		dl->color[1] = 0.5;
		dl->color[2] = 0;
		dl->die = cl.time + 0.1;	// long delay
		S_fastsound(nullptr, i, CHAN_WEAPON, fastsound_descriptor[weapons_gunshot1 + (rand() % 4)], volume * 0.8, ATTN_WEAPON_LIGHT);
		S_fastsound_queue(nullptr, i, CHAN_WEAPON, fastsound_descriptor[weapons_gunshot1 + (rand() % 4)], volume * 0.8, ATTN_WEAPON_LIGHT, 50);
		break;

	case MZ_CHAINGUN3:
		CL_ParticleGunSmoke(smoke_origin, vec3_origin, 1);
		CL_BrassShells(shell_brass, dir, 1, true);
		dl->radius = 250 + (rand() & 31);
		dl->color[0] = 1;
		dl->color[1] = 1;
		dl->color[2] = 0;
		dl->die = cl.time + 0.1;	// long delay
		S_fastsound(nullptr, i, CHAN_WEAPON, fastsound_descriptor[weapons_gunshot1 + (rand() % 4)], volume * 0.8, ATTN_WEAPON_LIGHT);
		S_fastsound_queue(nullptr, i, CHAN_WEAPON, fastsound_descriptor[weapons_gunshot1 + (rand() % 4)], volume * 0.8, ATTN_WEAPON_LIGHT, 33);
		S_fastsound_queue(nullptr, i, CHAN_WEAPON, fastsound_descriptor[weapons_gunshot1 + (rand() % 4)], volume * 0.8, ATTN_WEAPON_LIGHT, 66);
		break;

	case MZ_RAILGUN:
		dl->color[0] = 0.5;
		dl->color[1] = 0.5;
		dl->color[2] = 1.0;
		S_fastsound(nullptr, i, CHAN_WEAPON, fastsound_descriptor[weapons_railgf1a], volume * 0.8, ATTN_WEAPON_LIGHT);
		break;

	case MZ_ROCKET:
		dl->color[0] = 1;
		dl->color[1] = 0.5;
		dl->color[2] = 0.2;
		S_fastsound(nullptr, i, CHAN_WEAPON, fastsound_descriptor[weapons_rocklf1a], volume * 0.95, ATTN_WEAPON_HEAVY);
		S_fastsound_queue(nullptr, i, CHAN_AUTO, fastsound_descriptor[weapons_rocklr1b], volume * 0.8, ATTN_WEAPON_HEAVY, 150);
		CL_ParticleGunSmoke(smoke_origin, vec3_origin, 8);
		break;

	case MZ_GRENADE:
		dl->color[0] = 1;
		dl->color[1] = 0.5;
		dl->color[2] = 0;
		S_fastsound(nullptr, i, CHAN_WEAPON, fastsound_descriptor[weapons_grenlf1a], volume * 0.95, ATTN_WEAPON_LIGHT);
		S_fastsound_queue(nullptr, i, CHAN_AUTO, fastsound_descriptor[weapons_grenlr1b], volume * 0.8, ATTN_WEAPON_LIGHT, 100);
		break;

	case MZ_BFG:
		dl->color[0] = 0;
		dl->color[1] = 1;
		dl->color[2] = 0;
		S_fastsound(nullptr, i, CHAN_WEAPON, fastsound_descriptor[weapons_bfg__f1y], volume, ATTN_WEAPON_HEAVY);
		break;

	case MZ_LOGIN:
		dl->color[0] = 0;
		dl->color[1] = 1;
		dl->color[2] = 0;
		dl->die = cl.time + 1.0;
		S_fastsound(nullptr, i, CHAN_WEAPON, fastsound_descriptor[weapons_grenlf1a], 0.95, ATTN_MEDIUM);
		CL_LogoutEffect(pl->current.origin, weapon);
		break;

	case MZ_LOGOUT:
		dl->color[0] = 1;
		dl->color[1] = 0;
		dl->color[2] = 0;
		dl->die = cl.time + 1.0;
		S_fastsound(nullptr, i, CHAN_WEAPON, fastsound_descriptor[weapons_grenlf1a], 0.95, ATTN_MEDIUM);
		CL_LogoutEffect(pl->current.origin, weapon);
		break;

	case MZ_RESPAWN:
		dl->color[0] = 1;
		dl->color[1] = 1;
		dl->color[2] = 0;
		dl->die = cl.time + 1.0;
		S_fastsound(nullptr, i, CHAN_WEAPON, fastsound_descriptor[weapons_grenlf1a], 0.95, ATTN_MEDIUM);
		CL_LogoutEffect(pl->current.origin, weapon);
		break;

	case MZ_PHALANX:
		dl->color[0] = 1;
		dl->color[1] = 0.5;
		dl->color[2] = 0.5;
		S_fastsound(nullptr, i, CHAN_WEAPON, S_RegisterSound("weapons/plasshot.wav"), volume, ATTN_WEAPON_HEAVY);
		break;

	case MZ_IONRIPPER:
		dl->color[0] = 1;
		dl->color[1] = 0.5;
		dl->color[2] = 0.5;
		S_fastsound(nullptr, i, CHAN_WEAPON, S_RegisterSound("weapons/rippfire.wav"), volume, ATTN_WEAPON_LIGHT);
		break;
	}
}


/**
 * 
 */
void CL_ParseMuzzleFlashMonsters()
{
	int ent, j;
	vec3_t origin, dlorg;
	int flash_number;
	cdlight_t *dl;
	vec3_t forward, right, up, dir, shell, end;
	char soundname[64];

	ent = MSG_ReadShort(Net_Message());
	if (ent < 1 || ent >= MAX_EDICTS)
		Com_Error(ERR_DROP, "CL_ParseMuzzleFlashMonsters(): bad entity");

	flash_number = MSG_ReadByte(Net_Message());

	// locate the origin
	AngleVectors(cl_entities[ent].current.angles, forward, right, up);
	origin[0] =
		cl_entities[ent].current.origin[0] +
		forward[0] * monster_flash_offset[flash_number][0] +
		right[0] * monster_flash_offset[flash_number][1];
	origin[1] =
		cl_entities[ent].current.origin[1] +
		forward[1] * monster_flash_offset[flash_number][0] +
		right[1] * monster_flash_offset[flash_number][1];
	origin[2] =
		cl_entities[ent].current.origin[2] +
		forward[2] * monster_flash_offset[flash_number][0] +
		right[2] * monster_flash_offset[flash_number][1] +
		monster_flash_offset[flash_number][2];

	VectorScaleAndAdd(origin, forward, shell, -5);
	VectorScaleAndAdd(origin, forward, dlorg, 15);
	VectorScaleAndAdd(dlorg, up, dlorg, 35);
	VectorScaleAndAdd(origin, forward, end, 15);

	for (j = 0; j < 3; j++)
		dir[j] = forward[j] + right[j] + up[j] * 3;


	dl = CL_AllocDlight(ent);
	VectorCopy(dlorg, dl->origin);
	dl->radius = 200 + (rand() & 31);
	dl->minlight = 32;
	dl->die = cl.time + 0.1;			// + 0.1;

	switch (flash_number)
	{
	case MZ2_INFANTRY_MACHINEGUN_1:
	case MZ2_INFANTRY_MACHINEGUN_2:
	case MZ2_INFANTRY_MACHINEGUN_3:
	case MZ2_INFANTRY_MACHINEGUN_4:
	case MZ2_INFANTRY_MACHINEGUN_5:
	case MZ2_INFANTRY_MACHINEGUN_6:
	case MZ2_INFANTRY_MACHINEGUN_7:
	case MZ2_INFANTRY_MACHINEGUN_8:
	case MZ2_INFANTRY_MACHINEGUN_9:
	case MZ2_INFANTRY_MACHINEGUN_10:
	case MZ2_INFANTRY_MACHINEGUN_11:
	case MZ2_INFANTRY_MACHINEGUN_12:
	case MZ2_INFANTRY_MACHINEGUN_13:
		dl->color[0] = 1;
		dl->color[1] = 1;
		dl->color[2] = 0;
		CL_BrassShells(shell, dir, 1, true);
		CL_ParticleSmoke(origin, vec3_origin, 3);
		CL_GunFire(origin, end);
		S_fastsound(nullptr, ent, CHAN_WEAPON, S_RegisterSound("infantry/infatck1.wav"), 1, MONSTERS_ATTACK_ATTENUATION);
		break;

	case MZ2_SOLDIER_MACHINEGUN_1:
	case MZ2_SOLDIER_MACHINEGUN_2:
	case MZ2_SOLDIER_MACHINEGUN_3:
	case MZ2_SOLDIER_MACHINEGUN_4:
	case MZ2_SOLDIER_MACHINEGUN_5:
	case MZ2_SOLDIER_MACHINEGUN_6:
	case MZ2_SOLDIER_MACHINEGUN_7:
	case MZ2_SOLDIER_MACHINEGUN_8:
		dl->color[0] = 1;
		dl->color[1] = 1;
		dl->color[2] = 0;
		CL_BrassShells(shell, dir, 1, true);
		CL_ParticleSmoke(origin, vec3_origin, 1);
		CL_GunFire(origin, end);
		S_fastsound(nullptr, ent, CHAN_WEAPON, S_RegisterSound("soldier/solatck3.wav"), 1, MONSTERS_ATTACK_ATTENUATION);
		break;

	case MZ2_GUNNER_MACHINEGUN_1:
	case MZ2_GUNNER_MACHINEGUN_2:
	case MZ2_GUNNER_MACHINEGUN_3:
	case MZ2_GUNNER_MACHINEGUN_4:
	case MZ2_GUNNER_MACHINEGUN_5:
	case MZ2_GUNNER_MACHINEGUN_6:
	case MZ2_GUNNER_MACHINEGUN_7:
	case MZ2_GUNNER_MACHINEGUN_8:
		dl->color[0] = 1;
		dl->color[1] = 1;
		dl->color[2] = 0;
		CL_BrassShells(shell, dir, 1, true);
		CL_ParticleSmoke(origin, vec3_origin, 3);
		CL_GunFire(origin, end);
		S_fastsound(nullptr, ent, CHAN_WEAPON, S_RegisterSound("gunner/gunatck2.wav"), 1, MONSTERS_ATTACK_ATTENUATION);
		break;

	case MZ2_ACTOR_MACHINEGUN_1:
	case MZ2_SUPERTANK_MACHINEGUN_1:
	case MZ2_SUPERTANK_MACHINEGUN_2:
	case MZ2_SUPERTANK_MACHINEGUN_3:
	case MZ2_SUPERTANK_MACHINEGUN_4:
	case MZ2_SUPERTANK_MACHINEGUN_5:
	case MZ2_SUPERTANK_MACHINEGUN_6:
		dl->color[0] = 1;
		dl->color[1] = 1;
		dl->color[2] = 0;
		CL_BrassShells(shell, dir, 1, true);
		CL_ParticleSmoke(origin, vec3_origin, 3);
		CL_GunFire(origin, end);
		S_fastsound(nullptr, ent, CHAN_WEAPON, S_RegisterSound("infantry/infatck1.wav"), 1, MONSTERS_ATTACK_ATTENUATION);
		break;

	case MZ2_SOLDIER_BLASTER_1:
	case MZ2_SOLDIER_BLASTER_2:
	case MZ2_SOLDIER_BLASTER_3:
	case MZ2_SOLDIER_BLASTER_4:
	case MZ2_SOLDIER_BLASTER_5:
	case MZ2_SOLDIER_BLASTER_6:
	case MZ2_SOLDIER_BLASTER_7:
	case MZ2_SOLDIER_BLASTER_8:
		dl->color[0] = 1;
		dl->color[1] = 1;
		dl->color[2] = 0;
		S_fastsound(nullptr, ent, CHAN_WEAPON, S_RegisterSound("soldier/solatck2.wav"), 1, MONSTERS_ATTACK_ATTENUATION);
		break;

	case MZ2_FLYER_BLASTER_1:
	case MZ2_FLYER_BLASTER_2:
		dl->color[0] = 1;
		dl->color[1] = 1;
		dl->color[2] = 0;
		S_fastsound(nullptr, ent, CHAN_WEAPON, S_RegisterSound("flyer/flyatck3.wav"), 1, MONSTERS_ATTACK_ATTENUATION);
		break;

	case MZ2_MEDIC_BLASTER_1:
		dl->color[0] = 1;
		dl->color[1] = 1;
		dl->color[2] = 0;
		S_fastsound(nullptr, ent, CHAN_WEAPON, S_RegisterSound("medic/medatck1.wav"), 1, MONSTERS_ATTACK_ATTENUATION);
		break;

	case MZ2_HOVER_BLASTER_1:
		dl->color[0] = 1;
		dl->color[1] = 1;
		dl->color[2] = 0;
		S_fastsound(nullptr, ent, CHAN_WEAPON, S_RegisterSound("hover/hovatck1.wav"), 1, MONSTERS_ATTACK_ATTENUATION);
		break;

	case MZ2_FLOAT_BLASTER_1:
		dl->color[0] = 1;
		dl->color[1] = 1;
		dl->color[2] = 0;
		S_fastsound(nullptr, ent, CHAN_WEAPON, S_RegisterSound("floater/fltatck1.wav"), 1, MONSTERS_ATTACK_ATTENUATION);
		break;

	case MZ2_SOLDIER_SHOTGUN_1:
	case MZ2_SOLDIER_SHOTGUN_2:
	case MZ2_SOLDIER_SHOTGUN_3:
	case MZ2_SOLDIER_SHOTGUN_4:
	case MZ2_SOLDIER_SHOTGUN_5:
	case MZ2_SOLDIER_SHOTGUN_6:
	case MZ2_SOLDIER_SHOTGUN_7:
	case MZ2_SOLDIER_SHOTGUN_8:
		dl->color[0] = 1;
		dl->color[1] = 1;
		dl->color[2] = 0;
		// CL_SmokeAndFlash(origin);
		CL_BrassShells(shell, dir, 1, false);
		CL_ParticleSmoke(origin, vec3_origin, 2);
		CL_GunFire(origin, end);
		S_fastsound(nullptr, ent, CHAN_WEAPON, S_RegisterSound("soldier/solatck1.wav"), 1, MONSTERS_ATTACK_ATTENUATION);
		break;

	case MZ2_TANK_BLASTER_1:
	case MZ2_TANK_BLASTER_2:
	case MZ2_TANK_BLASTER_3:
		dl->color[0] = 1;
		dl->color[1] = 1;
		dl->color[2] = 0;
		S_fastsound(nullptr, ent, CHAN_WEAPON, S_RegisterSound("tank/tnkatck3.wav"), 1, MONSTERS_ATTACK_ATTENUATION);
		break;

	case MZ2_TANK_MACHINEGUN_1:
	case MZ2_TANK_MACHINEGUN_2:
	case MZ2_TANK_MACHINEGUN_3:
	case MZ2_TANK_MACHINEGUN_4:
	case MZ2_TANK_MACHINEGUN_5:
	case MZ2_TANK_MACHINEGUN_6:
	case MZ2_TANK_MACHINEGUN_7:
	case MZ2_TANK_MACHINEGUN_8:
	case MZ2_TANK_MACHINEGUN_9:
	case MZ2_TANK_MACHINEGUN_10:
	case MZ2_TANK_MACHINEGUN_11:
	case MZ2_TANK_MACHINEGUN_12:
	case MZ2_TANK_MACHINEGUN_13:
	case MZ2_TANK_MACHINEGUN_14:
	case MZ2_TANK_MACHINEGUN_15:
	case MZ2_TANK_MACHINEGUN_16:
	case MZ2_TANK_MACHINEGUN_17:
	case MZ2_TANK_MACHINEGUN_18:
	case MZ2_TANK_MACHINEGUN_19:
		dl->color[0] = 1;
		dl->color[1] = 1;
		dl->color[2] = 0;
		CL_BrassShells(shell, dir, 1, true);
		CL_ParticleSmoke(origin, vec3_origin, 3);
		CL_GunFire(origin, end);
		Q_sprintf(soundname, sizeof(soundname), "tank/tnkatk2%c.wav", 'a' + rand() % 5);
		S_fastsound(nullptr, ent, CHAN_WEAPON, S_RegisterSound(soundname), 1, MONSTERS_ATTACK_ATTENUATION);
		break;

	case MZ2_CHICK_ROCKET_1:
		dl->color[0] = 1;
		dl->color[1] = 0.5;
		dl->color[2] = 0.2;
		S_fastsound(nullptr, ent, CHAN_WEAPON, S_RegisterSound("chick/chkatck2.wav"), 1, MONSTERS_ATTACK_ATTENUATION);
		break;

	case MZ2_TANK_ROCKET_1:
	case MZ2_TANK_ROCKET_2:
	case MZ2_TANK_ROCKET_3:
		dl->color[0] = 1;
		dl->color[1] = 0.5;
		dl->color[2] = 0.2;
		S_fastsound(nullptr, ent, CHAN_WEAPON, S_RegisterSound("tank/tnkatck1.wav"), 1, MONSTERS_ATTACK_ATTENUATION);
		break;

	case MZ2_SUPERTANK_ROCKET_1:
	case MZ2_SUPERTANK_ROCKET_2:
	case MZ2_SUPERTANK_ROCKET_3:
		dl->color[0] = 1;
		dl->color[1] = 0.5;
		dl->color[2] = 0.2;
		S_fastsound(nullptr, ent, CHAN_WEAPON, S_RegisterSound("tank/rocket.wav"), 1, MONSTERS_ATTACK_ATTENUATION);
		break;

	case MZ2_GUNNER_GRENADE_1:
	case MZ2_GUNNER_GRENADE_2:
	case MZ2_GUNNER_GRENADE_3:
	case MZ2_GUNNER_GRENADE_4:
		dl->color[0] = 1;
		dl->color[1] = 0.5;
		dl->color[2] = 0;
		S_fastsound(nullptr, ent, CHAN_WEAPON, S_RegisterSound("gunner/gunatck3.wav"), 1, MONSTERS_ATTACK_ATTENUATION);
		break;

	case MZ2_GLADIATOR_RAILGUN_1:
		S_fastsound(nullptr, ent, CHAN_WEAPON, S_RegisterSound("weapons/railgf1a.wav"), 1, MONSTERS_ATTACK_ATTENUATION);
		dl->color[0] = 0.5;
		dl->color[1] = 0.5;
		dl->color[2] = 1.0;
		break;
	}
}


/**
 *
 */
void CL_AddDLights()
{
	cdlight_t* dl = cl_dlights;

	for (int i = 0; i < MAX_DLIGHTS; i++, dl++)
	{
		if (!dl->radius) continue;
		V_AddLight(dl->origin, dl->radius, dl->color[0], dl->color[1], dl->color[2], vec3_origin, 0, 0);
	}
}


/**
 * An entity has just been parsed that has an event value.
 * 
 * The female events are there for backwards compatability
 */
void CL_EntityEvent(entity_state_t * ent)
{
	switch (ent->event)
	{
	case EV_ITEM_RESPAWN:
		S_StartSound(nullptr, ent->number, CHAN_WEAPON, S_RegisterSound("items/respawn1.wav"), 1, ATTN_IDLE, 0);
		CL_ItemRespawnParticles(ent->origin);
		break;

	case EV_PLAYER_TELEPORT:
		S_StartSound(nullptr, ent->number, CHAN_WEAPON, S_RegisterSound("misc/tele1.wav"), 1, ATTN_IDLE, 0);
		CL_TeleportParticles(ent->origin);
		break;

	case EV_FOOTSTEP:
		if (cl_footsteps->value)
			S_fastsound(nullptr, ent->number, CHAN_BODY, fastsound_descriptor[id_cl_sfx_footsteps_0 + (rand() & 3)], 0.5, ATTN_NORM);
		break;

	case EV_FALLSHORT:
		S_StartSound(nullptr, ent->number, CHAN_AUTO, S_RegisterSound("player/land1.wav"), 1, ATTN_NORM, 0);
		break;

	case EV_FALL:
		S_StartSound(nullptr, ent->number, CHAN_AUTO,
			S_RegisterSexedSound(&cl_entities[ent->number].current, "*fall2.wav"), 1, ATTN_NORM, 0);
		break;

	case EV_FALLFAR:
		S_StartSound(nullptr, ent->number, CHAN_AUTO,
			S_RegisterSexedSound(&cl_entities[ent->number].current, "*fall1.wav"), 1, ATTN_NORM, 0);
		break;
	}
}


/**
 *
 */
void CL_ClearEffects()
{
	CL_ClearParticles();
	CL_ClearDecals();
	CL_ClearDlights();
	CL_ClearLightStyles();
	CL_ClearClEntities();
}
