/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// cl_parse.c  -- parse a message received from the server

#include "client.h"

#include "cl_hud.h"

#include "music.h"

#include "../common/filesystem.h"
#include "../common/string_util.h"

#include "../ref_gl/r_image.h"

#include <string>


char* svc_strings[256] =
{
	"svc_bad",

	"svc_muzzleflash",
	"svc_muzzlflash2",
	"svc_temp_entity",
	"svc_layout",
	"svc_inventory",

	"svc_nop",
	"svc_disconnect",
	"svc_reconnect",
	"svc_sound",
	"svc_print",
	"svc_stufftext",
	"svc_serverdata",
	"svc_configstring",
	"svc_spawnbaseline",
	"svc_centerprint",
	"svc_download",
	"svc_playerinfo",
	"svc_packetentities",
	"svc_deltapacketentities",
	"svc_frame"
};

//=============================================================================

/**
 * Returns true if the file exists, otherwise it attempts
 * to start a download from the server.
 */
bool CL_CheckOrDownloadFile(const std::string& filename)
{
	char name[MAX_OSPATH] = { '\0' };

	if (filename.find("..") != std::string::npos)
	{
		Com_Printf("Refusing to download a path with \'..\'\n");
		return true;
	}

	if (FS_FileExists(filename))
	{
		return true;
	}

	strcpy(cls.downloadname, filename.c_str());

	// download to a temp name, and only rename to the real name when done,
	// so if interrupted a runt file wont be left
	Com_StripExtension(cls.downloadname, cls.downloadtempname);
	strcat(cls.downloadtempname, ".tmp");

	FS_File* f = FS_Open(name, FS_OPEN_WRITE);
	size_t position = 0;

	if (f) // it exists
	{
		FS_Seek(f, FS_FileLength(f));
		position = FS_Tell(f);

		cls.download = f;

		// give the server an offset to start the download
		Com_Printf("Resuming %s\n", cls.downloadname);
	}
	else
	{
		Com_Printf("Downloading %s\n", cls.downloadname);
	}

	MSG_WriteByte(&cls.netchan.message, clc_stringcmd);
	MSG_WriteString(&cls.netchan.message, va("download %s %i", cls.downloadname, position));

	cls.downloadnumber++;
	return false;
}


/**
 * Request a download from the server
 */
void CL_Download_f()
{

	if (Cmd_Argc() != 2)
	{
		Com_Printf("Usage: download <filename>\n");
		return;
	}

	std::string filename = Cmd_Argv(1);

	if (filename.find("..") != std::string::npos)
	{
		Com_Printf("Refusing to download a path with \'..\'\n");
		return;
	}
	
	// it exists, no need to download
	if (FS_FileExists(filename))
	{
		Com_Printf("File already exists.\n");
		return;
	}

	strcpy(cls.downloadname, filename.c_str());
	Com_Printf("Downloading %s\n", cls.downloadname);

	// download to a temp name, and only rename to the real name when done,
	// so if interrupted a runt file wont be left
	Com_StripExtension(cls.downloadname, cls.downloadtempname);
	strcat(cls.downloadtempname, ".tmp");

	MSG_WriteByte(&cls.netchan.message, clc_stringcmd);
	MSG_WriteString(&cls.netchan.message, va("download %s", cls.downloadname));

	cls.downloadnumber++;
}


/**
 * \todo	Find a better way to do this.
 */
void WILLOW_HACK_SOUND(int i)
{
	// At first, make sure we set default values to hacking database
	cl.sound_precache_hacks[i] = 0;
	cl.sound_precache_rolloff_factor[i] = 1.0f;
	cl.sound_precache_gain[i] = 1.0f;

	// willow: TO DO: "misc/secret.wav" hide this sound from other players!
	if (!strncmp(cl.configstrings[CS_SOUNDS + i], "misc/secret", 11))
	{
		cl.sound_precache_hacks[i] = sound_precache_hacks_FLAT2D;
	}

	// ARMOR PICKUP
	else if (!strncmp(cl.configstrings[CS_SOUNDS + i], "misc/ar", 7))
	{
		cl.sound_precache_hacks[i] = sound_precache_hacks_AUTOFIX_gain | sound_precache_hacks_AUTOFIX_rolloff_factor;
		cl.sound_precache_gain[i] = 0.8;
		cl.sound_precache_rolloff_factor[i] = 1;
	}

	// Hack railgun background noise
	else if (!strcmp(cl.configstrings[CS_SOUNDS + i], "weapons/rg_hum.wav"))
	{
		cl.sound_precache_hacks[i] = sound_precache_hacks_AUTOFIX_gain;
		cl.sound_precache_gain[i] = 0.2f;
	}

	// Hack hyperblaster drum rotation noise
	else if (!strcmp(cl.configstrings[CS_SOUNDS + i], "weapons/hyprbl1a.wav"))
	{
		cl.sound_precache_hacks[i] = sound_precache_hacks_AUTOFIX_gain;
		cl.sound_precache_gain[i] = 0.7f;
	}

	else if (!strcmp(cl.configstrings[CS_SOUNDS + i], "weapons/hyprbu1a.wav"))
	{
		cl.sound_precache_hacks[i] = sound_precache_hacks_AUTOFIX_gain;
		cl.sound_precache_gain[i] = 0.7f;
	}

	else if (!strcmp(cl.configstrings[CS_SOUNDS + i], "infantry/inflies1.wav"))
	{
		cl.sound_precache_hacks[i] = sound_precache_hacks_AUTOFIX_gain | sound_precache_hacks_AUTOFIX_rolloff_factor;
		cl.sound_precache_gain[i] = 0.7f;
		cl.sound_precache_rolloff_factor[i] = 0.5f;
	}

	// grenades timer countdown
	else if (!strcmp(cl.configstrings[CS_SOUNDS + i], "weapons/hgrenc1b.wav"))
	{
		cl.sound_precache_hacks[i] = sound_precache_hacks_AUTOFIX_gain;
		cl.sound_precache_gain[i] = 0.2f;
	}

	else if (!strcmp(cl.configstrings[CS_SOUNDS + i], "world/wind2.wav"))
	{
		cl.sound_precache_hacks[i] = sound_precache_hacks_AUTOFIX_gain | sound_precache_hacks_AUTOFIX_rolloff_factor;
		cl.sound_precache_gain[i] = 0.55f;
		cl.sound_precache_rolloff_factor[i] = 0.888f;
	}
}


/**
 * 
 */
void CL_RegisterSounds()
{
	int i = 1;
	while (i < MAX_SOUNDS && cl.configstrings[CS_SOUNDS + i][0])
	{
		WILLOW_HACK_SOUND(i);

		cl.sound_precache[i] = S_RegisterSound(cl.configstrings[CS_SOUNDS + i]);

		if (cl.configstrings[CS_SOUNDS + i][0] == '*')
			cl.sound_sexedname[i] = cl.configstrings[CS_SOUNDS + i];
		else
			cl.sound_sexedname[i] = nullptr;

		++i;
	}
}


/**
 * A download message has been received from the server
 */
void CL_ParseDownload()
{
	char name[MAX_OSPATH] = { '\0' };

	// read the data
	int size = MSG_ReadShort(Net_Message());
	int percent = MSG_ReadByte(Net_Message());

	if (size == -1)
	{
		Com_Printf("Server does not have this file.\n");
		if (cls.download)
		{
			FS_Close(cls.download);
			cls.download = nullptr;
		}
		CL_RequestNextDownload();
		return;
	}

	// open the file if not opened yet
	if (!cls.download)
	{
		Q_sprintf(name, sizeof(name), "%s", cls.downloadtempname);

		FS_MakeDirectory(name);

		cls.download = FS_Open(name, FS_OPEN_WRITE);
		if (!cls.download)
		{
			Net_Message()->readcount += size;
			Com_Printf("Failed to open %s\n", cls.downloadtempname);
			CL_RequestNextDownload();
			return;
		}
	}

	FS_Write(cls.download, Net_Message()->data + Net_Message()->readcount, 1, size);
	Net_Message()->readcount += size;

	if (percent != 100)
	{
		// request next block
		cls.downloadpercent = percent;

		MSG_WriteByte(&cls.netchan.message, clc_stringcmd);
		SZ_Print(&cls.netchan.message, "nextdl");
	}
	else
	{
		char oldn[MAX_OSPATH];
		char newn[MAX_OSPATH];

		FS_Close(cls.download);

		// rename the temp file to it's final name
		Q_sprintf(oldn, sizeof(oldn), "%s", cls.downloadtempname);
		Q_sprintf(newn, sizeof(newn), "%s", cls.downloadname);
		if (!FS_Rename(oldn, newn)) { Com_Printf("failed to rename.\n"); }

		cls.download = nullptr;
		cls.downloadpercent = 0;

		// get another file if needed
		CL_RequestNextDownload();
	}
}


/*
==================
CL_ParseServerData
==================
*/
void CL_ParseServerData()
{
	extern ConsoleVariable *fs_gamedirvar;

	Com_DPrintf("Serverdata packet received.\n");
	
	CL_ClearState();
	cls.state = ca_connected;

	// parse protocol version number
	int i = MSG_ReadLong(Net_Message());
	cls.serverProtocol = i;

	if (i != PROTOCOL_VERSION) { Com_Error(ERR_DROP, "Server returned version %i, not %i", i, PROTOCOL_VERSION); }

	cl.servercount = MSG_ReadLong(Net_Message());
	cl.attractloop = MSG_ReadByte(Net_Message()) != 0;

	// game directory
	std::string game_directory = MSG_ReadString(Net_Message());
	strncpy(cl.gamedir, game_directory.c_str(), sizeof(cl.gamedir) - 1);


	// set gamedir
	/// \todo	I really hate this riduclous set of checks. Must find a better way to do this.
	if ((!game_directory.empty() && (fs_gamedirvar->string.empty() || game_directory != fs_gamedirvar->string)) ||
		(game_directory.empty() && !fs_gamedirvar->string.empty()))
	{
		Cvar_Set("game", game_directory);
	}

	// parse player entity number
	cl.playernum = MSG_ReadShort(Net_Message());

	// get the full level name
	game_directory = MSG_ReadString(Net_Message());

	if (cl.playernum == -1)// playing a cinematic or showing a pic, not a level
	{}
	else
	{
		// seperate the printfs so the server message can have a color
		Com_Printf("\n\n" BAR_GRFX "\n\n");
		Com_Printf("%c%s\n", 2, game_directory.c_str());

		// need to prep refresh at next oportunity
		cl.refresh_prepped = false;
	}
}


/**
 * 
 */
void CL_ParseBaseline()
{
	entity_state_t *es;
	int bits;
	int newnum;
	entity_state_t nullstate;

	memset(&nullstate, 0, sizeof(nullstate));

	newnum = CL_ParseEntityBits(reinterpret_cast<unsigned int*>(&bits));
	es = &cl_entities[newnum].baseline;
	CL_ParseDelta(&nullstate, es, newnum, bits);
}


/**
 * 
 */
char gender_model[MAX_QPATH] = { '\0' };

void CL_LoadClientinfo(clientinfo_t * ci, char* s)
{
	char model_name[MAX_QPATH] = { '\0' };
	char skin_name[MAX_QPATH] = { '\0' };
	char model_filename[MAX_QPATH] = { '\0' };
	char skin_filename[MAX_QPATH] = { '\0' };
	char weapon_filename[MAX_QPATH] = { '\0' };

	strncpy(ci->cinfo, s, sizeof(ci->cinfo));
	ci->cinfo[sizeof(ci->cinfo) - 1] = 0;

	// isolate the player's name
	strncpy(ci->name, s, sizeof(ci->name));
	ci->name[sizeof(ci->name) - 1] = 0;
	char* t = strstr(s, "\\");
	if (t)
	{
		ci->name[t - s] = 0;
		s = t + 1;
	}

	if (cl_noskins->value || *s == 0)
	{
		Q_sprintf(model_filename, sizeof(model_filename), "players/male/tris.md2");
		Q_sprintf(weapon_filename, sizeof(weapon_filename), "players/male/weapon.md2");
		Q_sprintf(skin_filename, sizeof(skin_filename), "players/male/grunt.tga");
		Q_sprintf(ci->iconname, sizeof(ci->iconname), "/players/male/grunt_i.tga");
		ci->model = R_RegisterModel(model_filename);
		memset(ci->weaponmodel, 0, sizeof(ci->weaponmodel));
		ci->weaponmodel[0] = R_RegisterModel(weapon_filename);
		ci->skin = R_RegisterSkin(skin_filename);
		ci->bump = R_RegisterPlayerBump(skin_filename);
		ci->icon = Draw_FindPic(ci->iconname);
	}
	else
	{
		// isolate the model name
		strcpy(model_name, s);
		t = strchr(model_name, '/');
		if (!t)
			t = strstr(model_name, "\\");
		if (!t)
			t = model_name;
		*t = 0;

		// isolate the skin name
		strcpy(skin_name, s + strlen(model_name) + 1);

		// model file
		Q_sprintf(model_filename, sizeof(model_filename), "players/%s/tris.md2", model_name);
		
		ci->model = R_RegisterModel(model_filename);
		if (!ci->model)
		{
			strcpy(model_name, "male");
			Q_sprintf(model_filename, sizeof(model_filename), "players/male/tris.md2");
			ci->model = R_RegisterModel(model_filename);
		}

		// skin file
		Q_sprintf(skin_filename, sizeof(skin_filename), "players/%s/%s.tga", model_name, skin_name);
		ci->skin = R_RegisterSkin(skin_filename);
		ci->bump = R_RegisterPlayerBump(skin_filename);

		// if we don't have the skin and the model wasn't male,
		// see if the male has it (this is for CTF's skins)
		if ((!ci->skin || !ci->bump) && _stricmp(model_name, "male"))
		{
			// change model to male
			strcpy(model_name, "male");
			Q_sprintf(model_filename, sizeof(model_filename),
				"players/male/tris.md2");
			ci->model = R_RegisterModel(model_filename);

			// see if the skin exists for the male model
			Q_sprintf(skin_filename, sizeof(skin_filename), "players/%s/%s.tga", model_name, skin_name);
			ci->skin = R_RegisterSkin(skin_filename);
			ci->bump = R_RegisterPlayerBump(skin_filename);
		}

		// if we still don't have a skin, it means that the male model
		// didn't have
		// it, so default to grunt
		if (!ci->skin || !ci->bump)
		{
			// see if the skin exists for the male model
			Q_sprintf(skin_filename, sizeof(skin_filename), "players/%s/grunt.tga", model_name, skin_name);
			ci->skin = R_RegisterSkin(skin_filename);
			ci->bump = R_RegisterPlayerBump(skin_filename);
		}
		// weapon file
		for (int i = 0; i < num_cl_weaponmodels; i++)
		{
			Q_sprintf(weapon_filename, sizeof(weapon_filename), "players/%s/%s", model_name, cl_weaponmodels[i]);
			ci->weaponmodel[i] = R_RegisterModel(weapon_filename);

			if (!ci->weaponmodel[i] && strcmp(model_name, "cyborg") == 0)
			{
				// try male
				Q_sprintf(weapon_filename, sizeof(weapon_filename), "players/male/%s", cl_weaponmodels[i]);
				ci->weaponmodel[i] = R_RegisterModel(weapon_filename);
			}
			if (!cl_vwep->value)
				break;			// only one when vwep is off
		}
		// icon file
		Q_sprintf(ci->iconname, sizeof(ci->iconname), "/players/%s/%s_i.tga", model_name, skin_name);
		ci->icon = Draw_FindPic(ci->iconname);
	}
	if (!strcmp(model_name, "cyborg") || !strcmp(model_name, "male") || !strcmp(model_name, "female"))
	{
		strncpy(ci->sex, model_name, sizeof(ci->sex));
		Cvar_ForceSet("gender", ci->sex);
	}
	else
		strncpy(ci->sex, "null", sizeof(ci->sex));


	// must have loaded all data types to be valud
	if (!ci->skin || !ci->bump || !ci->icon || !ci->model || !ci->weaponmodel[0])
	{
		ci->skin = ci->bump = ci->icon = nullptr;
		ci->model = nullptr;
		ci->weaponmodel[0] = nullptr;
		return;
	}
}


/*
================
CL_ParseClientinfo

Load the skin, icon, and model for a client
================
*/
void CL_ParseClientinfo (int player) {
	char* s;
	clientinfo_t *ci;

	s = cl.configstrings[player + CS_PLAYERSKINS];

	ci = &cl.clientinfo[player];

	CL_LoadClientinfo (ci, s);
}







/*
================
CL_ParseConfigString
================
*/
void CL_ParseConfigString () {
	int i;
	char* s;
	char olds[MAX_QPATH];

	i = MSG_ReadShort (Net_Message());
	if (i < 0 || i >= MAX_CONFIGSTRINGS)
		Com_Error (ERR_DROP, "configstring > MAX_CONFIGSTRINGS");
	s = MSG_ReadString (Net_Message());

	strncpy (olds, cl.configstrings[i], sizeof(olds));
	olds[sizeof(olds)-1] = 0;

	strcpy (cl.configstrings[i], s);

	// do something apropriate 

	if (i >= CS_LIGHTS && i < CS_LIGHTS + MAX_LIGHTSTYLES)
		CL_SetLightstyle (i - CS_LIGHTS);
	else if (i == CS_CDTRACK) {
		if (cl.refresh_prepped)
			Music_Play ();

	}
	else if (i >= CS_MODELS && i < CS_MODELS + MAX_MODELS) {
		if (cl.refresh_prepped) {
			cl.model_draw[i - CS_MODELS] =
				R_RegisterModel (cl.configstrings[i]);
			if (cl.configstrings[i][0] == '*')
				cl.model_clip[i - CS_MODELS] =
				CM_InlineModel (cl.configstrings[i]);
			else
				cl.model_clip[i - CS_MODELS] = nullptr;
		}
	}
	else if (i >= CS_SOUNDS && i < CS_SOUNDS + MAX_MODELS) {
		if (cl.refresh_prepped) {
			WILLOW_HACK_SOUND (i - CS_SOUNDS);
			cl.sound_precache[i - CS_SOUNDS] =
				S_RegisterSound (cl.configstrings[i]);
		}
	}
	else if (i >= CS_IMAGES && i < CS_IMAGES + MAX_MODELS) {
		if (cl.refresh_prepped)
			cl.image_precache[i - CS_IMAGES] =
			Draw_FindPic (cl.configstrings[i]);
	}
	else if (i >= CS_PLAYERSKINS && i < CS_PLAYERSKINS + MAX_CLIENTS) {
		if (cl.refresh_prepped && strcmp (olds, s))
			CL_ParseClientinfo (i - CS_PLAYERSKINS);
	}

}


/*
=====================================================================

ACTION MESSAGES

=====================================================================
*/

/*
==================
CL_ParseStartSoundPacket
==================
*/
void CL_ParseStartSoundPacket () {
	vec3_t pos_v;
	float *pos;
	int channel, ent;
	int sound_num;
	float volume;
	float attenuation;
	unsigned ofs;
	int id;
	int flags = MSG_ReadByte (Net_Message());

	sound_num = MSG_ReadByte (Net_Message());
	volume =
		(flags & SND_VOLUME) ? MSG_ReadByte (Net_Message()) /
		255.0 : DEFAULT_SOUND_PACKET_VOLUME;
	attenuation =
		(flags & SND_ATTENUATION) ? MSG_ReadByte (Net_Message()) /
		64.0 : DEFAULT_SOUND_PACKET_ATTENUATION;
	ofs = (flags & SND_OFFSET) ? MSG_ReadByte (Net_Message()) : 0;

	if (flags & SND_ENT) {		// entity reletive
		channel = MSG_ReadShort (Net_Message());
		ent = channel >> 3;
		if (ent > MAX_EDICTS)
			Com_Error (ERR_DROP, "CL_ParseStartSoundPacket: ent = %i", ent);

		channel &= 7;
	}
	else {
		ent = 0;
		channel = 0;
	}

	if (flags & SND_POS) {		// positioned in space
		MSG_ReadPos (Net_Message(), pos_v);
		pos = pos_v;
	}
	else						// use entity number
		pos = nullptr;

	if (cl.sound_sexedname[sound_num])
		id = S_RegisterSexedSound (&cl_entities[ent].current, cl.sound_sexedname[sound_num]);
	else
		id = cl.sound_precache[sound_num];

	// willow: F**K THE HACK'S!!! trying to cheat ugly id's map design and 
	// stuff
	if (!attenuation
		&& (cl.
		sound_precache_hacks[sound_num] & sound_precache_hacks_FLAT2D)) {
		S_StartLocalSound (id);
		return;
	}

	if (cl.
		sound_precache_hacks[sound_num] &
		sound_precache_hacks_AUTOFIX_gain)
		volume = cl.sound_precache_gain[sound_num];

	if (cl.
		sound_precache_hacks[sound_num] &
		sound_precache_hacks_AUTOFIX_rolloff_factor)
		attenuation = cl.sound_precache_rolloff_factor[sound_num];

	// willow: simple as it should be ^_^
	S_StartSound (pos, ent, channel, id, volume,
		attenuation, ofs);
}

void SHOWNET (char* s) {
	if (cl_shownet->value >= 2)
		Com_Printf ("%3i:%s\n", Net_Message()->readcount - 1, s);
}

/*
=====================
CL_ParseServerMessage
=====================
*/
void CL_ParseServerMessage()
{
	int cmd;
	char* s;
	int i;

	//
	// if recording demos, copy the message out
	//
	if (cl_shownet->value == 1)
		Com_Printf("%i ", Net_Message()->cursize);
	else if (cl_shownet->value >= 2)
		Com_Printf("------------------\n");


	// parse the message
	for(;;)
	{
		if (Net_Message()->readcount > Net_Message()->cursize)
		{
			Com_Error(ERR_DROP, "CL_ParseServerMessage: Bad server message");
			break;
		}

		cmd = MSG_ReadByte(Net_Message());

		if (cmd == -1) ///\fixme magic number
		{
			SHOWNET("END OF MESSAGE");
			break;
		}

		if (cl_shownet->value >= 2)
		{
			if (!svc_strings[cmd])
				Com_Printf("%3i:BAD CMD %i\n", Net_Message()->readcount - 1,
					cmd);
			else
				SHOWNET(svc_strings[cmd]);
		}

		// other commands
		switch (cmd)
		{
		default:
			Com_Error(ERR_DROP, "CL_ParseServerMessage: Illegible server message\n");
			break;

		case svc_nop:
			//          Com_Printf ("svc_nop\n");
			break;

		case svc_disconnect:
			Com_Error(ERR_DISCONNECT, "Server disconnected\n");
			break;

		case svc_reconnect:
			Com_Printf("Server disconnected, reconnecting\n");
			if (cls.download)
			{
				FS_Close(cls.download);
				cls.download = nullptr;
			}
			cls.state = ca_connecting;
			cls.connect_time = -99999;	// CL_CheckForResend() will fire immediately
			break;

		case svc_print:
			i = MSG_ReadByte(Net_Message());
			if (i == PRINT_CHAT)
			{
				S_StartLocalSound(fastsound_descriptor[misc_talk]);
				Con_Console()->ormask = 128;
			}
			Com_Printf("%s", MSG_ReadString(Net_Message()));
			Con_Console()->ormask = 0;
			break;

		case svc_centerprint:
			SCR_CenterPrint(MSG_ReadString(Net_Message()));
			break;

		case svc_stufftext:
			s = MSG_ReadString(Net_Message());
			Com_DPrintf("stufftext: %s\n", s);
			Cbuf_AddText(s);
			break;

		case svc_serverdata:
			Cbuf_Execute(); // make sure any stuffed commands are done
			CL_ParseServerData();
			break;

		case svc_configstring:
			CL_ParseConfigString();
			break;

		case svc_sound:
			CL_ParseStartSoundPacket();
			break;

		case svc_spawnbaseline:
			CL_ParseBaseline();
			break;

		case svc_temp_entity:
			CL_ParseTEnt();
			break;

		case svc_muzzleflash:
			CL_ParseMuzzleFlash();
			break;

		case svc_muzzleflash2:
			CL_ParseMuzzleFlashMonsters();
			break;

		case svc_download:
			CL_ParseDownload();
			break;

		case svc_frame:
			CL_ParseFrame();
			break;

		case svc_inventory:
			CL_ParseInventory();
			break;

		case svc_layout:
			s = MSG_ReadString(Net_Message());
			strncpy(cl.layout, s, sizeof(cl.layout) - 1);
			break;

		case svc_playerinfo:
		case svc_packetentities:
		case svc_deltapacketentities:
			Com_Error(ERR_DROP, "Out of place frame data");
			break;
		}
	}

	// we don't know if it is ok to save a demo message until
	// after we have parsed the frame
	if (cls.demorecording && !cls.demowaiting)
		CL_WriteDemoMessage();
}
