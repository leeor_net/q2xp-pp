/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// console.c

#include "client.h"

#include "../common/colordef.h"
#include "../common/filesystem.h"
#include "../common/string_util.h"
#include "../ref_gl/r_local.h"

// ===============================================================================
// = DEFINE'S
// ===============================================================================
#define MAXCMDLINE		256


// ===============================================================================
// = LOCAL VARIABLES
// ===============================================================================
console_t CONSOLE;
ConsoleVariable* con_notifytime;


// ===============================================================================
// = IMPORTS
// ===============================================================================
/// \fixme These should be wrapped in set/get type functions.
extern char key_lines[32][MAXCMDLINE];
extern int edit_line;
extern int key_linepos;


// ===============================================================================
// = INTERNAL FUNCTIONS
// ===============================================================================
/**
 * \fixme	Magic number 7. Is this the number of available colors?
 */
static int _colorIndex(int c)
{
	return (((c)-'0') & 7); 
}


// ===============================================================================
// = PUBLIC FUNCTIONS
// ===============================================================================
/**
 * 
 */
console_t* Con_Console()
{
	return &CONSOLE;
}


/**
 * 
 */
void Key_ClearTyping()
{
	key_lines[edit_line][1] = 0;	// clear any typing
	key_linepos = 1;
}


/**
 * 
 */
void Con_ToggleConsole()
{
	SCR_EndLoadingPlaque(); // get rid of loading plaque

	if (cl.attractloop)
	{
		Cbuf_AddText("killserver\n");
		return;
	}

	if (cls.state == ca_disconnected) // start the demo loop again
	{
		//Cbuf_AddText("d1\n");
		return;
	}

	Key_ClearTyping();
	Con_ClearNotify();

	if (cls.key_dest == KEY_CONSOLE)
	{
		M_ForceMenuOff();
		Cvar_Set("paused", "0");
	}
	else
	{
		M_ForceMenuOff();
		cls.key_dest = KEY_CONSOLE;

		if (Cvar_VariableValue("maxclients") == 1 && Com_ServerState())
			Cvar_Set("paused", "1");
	}
}


/**
 * 
 */
void Con_ToggleChat()
{
	Key_ClearTyping();

	if (cls.key_dest == KEY_CONSOLE)
	{
		if (cls.state == ca_active)
		{
			M_ForceMenuOff();
			cls.key_dest = KEY_GAME;
		}
	}
	else
		cls.key_dest = KEY_CONSOLE;

	Con_ClearNotify();
}


/**
 * 
 */
void Con_Clear()
{
	for (int i = 0; i < CON_TEXTSIZE; i++)
		CONSOLE.text[i] = (_colorIndex(COLOR_WHITE) << 8) | ' ';

	CONSOLE.current = CONSOLE.totalLines - 1;
	CONSOLE.display = CONSOLE.current;
}


/**
 * Save the console contents out to a file
 */
void Con_Dump()
{
	int l, x;
	short *line;
	char buffer[1024];
	char name[MAX_OSPATH];

	if (Cmd_Argc() != 2)
	{
		Com_Printf(S_COLOR_YELLOW"usage: condump <filename>\n");
		return;
	}

	Q_sprintf(name, sizeof(name), "%s.txt", Cmd_Argv(1));

	Com_Printf("Dumped console text to " S_COLOR_GREEN "%s\n", name);
	FS_MakeDirectory(name);
	FS_File* f = FS_Open(name, FS_OPEN_WRITE);
	if (!f)
	{
		Com_Printf(S_COLOR_RED"ERROR: couldn't open.\n");
		return;
	}

	// skip empty lines
	for (l = CONSOLE.current - CONSOLE.totalLines + 1; l <= CONSOLE.current; l++)
	{
		line = CONSOLE.text + (l % CONSOLE.totalLines)*CONSOLE.lineWidth;
		for (x = 0; x < CONSOLE.lineWidth; x++)
		{
			if ((line[x] & 0xff) != ' ')
				break;
		}

		if (x != CONSOLE.lineWidth)
			break;
	}

	// write the remaining lines
	buffer[CONSOLE.lineWidth] = 0;
	for (; l <= CONSOLE.current; l++)
	{
		line = CONSOLE.text + (l % CONSOLE.totalLines)*CONSOLE.lineWidth;
		for (x = 0; x < CONSOLE.lineWidth; x++)
			buffer[x] = line[x] & 0xff;

		for (x = CONSOLE.lineWidth - 1; x >= 0; x--)
		{
			if (buffer[x] == ' ')
				buffer[x] = 0;
			else
				break;
		}

		FS_fprintf(f, "%s\n", buffer);
	}

	FS_Close(f);
}


/**
 * 
 */
void Con_ClearNotify()
{
	for (int i = 0; i < NUM_CON_TIMES; i++)
		CONSOLE.times[i] = 0;
}


/**
 * 
 */
void Con_MessageMode()
{
	chat_team = false;
	cls.key_dest = KEY_MESSAGE;
}


/**
 * 
 */
void Con_MessageMode2()
{
	chat_team = true;
	cls.key_dest = KEY_MESSAGE;
}


/*
================
Con_CheckResize

If the line width has changed, reformat the buffer.
================
*/

#define	CON_DEFAULT_WIDTH		78

void Con_CheckResize()
{
	short tbuf[CON_TEXTSIZE];

	int width = (640 >> 3) - 2; /// \fixme Magic Numbers
	if (width == CONSOLE.lineWidth) { return; }

	int oldwidth = CONSOLE.lineWidth;
	CONSOLE.lineWidth = width;
	int oldtotalLines = CONSOLE.totalLines;
	CONSOLE.totalLines = CON_TEXTSIZE / CONSOLE.lineWidth;
	int numlines = oldtotalLines;

	if (numlines > CONSOLE.totalLines)
	{
		numlines = CONSOLE.totalLines;
	}

	int numchars = oldwidth;

	if (numchars > CONSOLE.lineWidth)
	{
		numchars = CONSOLE.lineWidth;
	}

	memcpy(tbuf, CONSOLE.text, CON_TEXTSIZE * sizeof(short));

	for (int i = 0; i < CON_TEXTSIZE; i++)
	{
		CONSOLE.text[i] = (_colorIndex(COLOR_WHITE) << 8) | ' ';
	}

	for (int i = 0; i < numlines; i++)
	{
		for (int j = 0; j < numchars; j++)
		{
			CONSOLE.text[(CONSOLE.totalLines - 1 - i) * CONSOLE.lineWidth + j] = tbuf[((CONSOLE.current - i + oldtotalLines) % oldtotalLines) * oldwidth + j];
		}
	}

	Con_ClearNotify();

	CONSOLE.current = CONSOLE.totalLines - 1;
	CONSOLE.display = CONSOLE.current;
}


/*
================
Con_Init
================
*/
void Con_Init()
{
	CONSOLE.lineWidth = -1;

	Con_CheckResize();

	Com_Printf(S_COLOR_YELLOW "Rogue Arena Console initialized\n");
	Com_Printf(BAR_BOLD "\n\n");

	// register our commands
	con_notifytime = Cvar_Get("con_notifytime", "3", 0);

	Cmd_AddCommand("toggleconsole", Con_ToggleConsole);
	Cmd_AddCommand("togglechat", Con_ToggleChat);
	Cmd_AddCommand("messagemode", Con_MessageMode);
	Cmd_AddCommand("messagemode2", Con_MessageMode2);
	Cmd_AddCommand("clear", Con_Clear);
	Cmd_AddCommand("condump", Con_Dump);

	CONSOLE.initialized = true;
}


/**
 *
 */
static void Con_Linefeed (bool skipNotify)
{
	// mark time for transparent overlay
	if (CONSOLE.current >= 0)
		CONSOLE.times[CONSOLE.current % NUM_CON_TIMES] = skipNotify ? 0 : cls.realtime;

	CONSOLE.x = 0;
	if (CONSOLE.display == CONSOLE.current)
		CONSOLE.display++;

	CONSOLE.current++;

	for (int i = 0; i < CONSOLE.lineWidth; i++)
		CONSOLE.text[(CONSOLE.current % CONSOLE.totalLines)*CONSOLE.lineWidth + i] = (_colorIndex (COLOR_WHITE) << 8) | ' ';
}


/**
 * Handles cursor positioning, line wrapping, etc.
 * 
 * \note	All console printing must go through this in order to be logged to disk
 * \note	If no console is visible, the text will appear at the top of the game window
 * 
 * \todo	define what 'etc.' means above.
 * \todo	Update documentation to explain use of this function including color formatting.
 */
void Con_Print(char* txt)
{
	bool skipNotify = false;

	if (!Q_strnicmp(txt, "[skipnotify]", 12))
	{
		skipNotify = true;
		txt += 12;
	}

	if (!CONSOLE.initialized)
	{
		CONSOLE.lineWidth = -1;
		Con_CheckResize();
		CONSOLE.initialized = true;
	}

	int color = _colorIndex(COLOR_WHITE);

	int c = 0, l = 0;
	while ((c = *txt))
	{
		if (isColorString(txt))
		{
			color = _colorIndex(*(txt + 1));
			txt += 2;
			continue;
		}

		// count word length
		for (l = 0; l < CONSOLE.lineWidth; l++)
		{
			if (txt[l] <= ' ')
				break;
		}

		// word wrap
		if (l != CONSOLE.lineWidth && (CONSOLE.x + l > CONSOLE.lineWidth))
			Con_Linefeed(skipNotify);

		txt++;

		switch (c)
		{
		case '\n':
			Con_Linefeed(skipNotify);
			break;

		case '\r':
			CONSOLE.x = 0;
			break;

		default:	// display character and advance
		{
			int y = CONSOLE.current % CONSOLE.totalLines;
			CONSOLE.text[y*CONSOLE.lineWidth + CONSOLE.x] = (color << 8) | c;
			CONSOLE.x++;
			if (CONSOLE.x >= CONSOLE.lineWidth)
				Con_Linefeed(skipNotify);
			break;
		}
		}
	}
}


/**
 * 
 */
void Con_CenteredPrint(char* text)
{
	int l;
	char buffer[1024];

	l = strlen(text);
	l = (CONSOLE.lineWidth - l) * 0.5;
	if (l < 0)
		l = 0;
	memset(buffer, ' ', l);
	strcpy(buffer + l, text);
	strcat(buffer, "\n");
	Con_Print(buffer);
}


// ==============================================================================
// = DRAWING
// ==============================================================================


/**
 * The input line scrolls horizontally if typing goes beyond the right edge
 */
void Con_DrawInput()
{
	if (cls.key_dest == KEY_MENU) return;
	if (cls.key_dest != KEY_CONSOLE && cls.state == ca_active)
		return;

	char* text = key_lines[edit_line];

	// add the cursor frame
	text[key_linepos] = 10 + ((int)(cls.realtime / 256) & 1);

	// fill out remainder with spaces
	/// \fixme	Do we really need to fill out the line?
	for (int i = key_linepos + 1; i < CONSOLE.lineWidth; i++)
		text[i] = ' ';

	// prestep if horizontally scrolling
	if (key_linepos >= CONSOLE.lineWidth)
		text += 1 + key_linepos - CONSOLE.lineWidth;

	// draw it
	GL_SetFontColor(GetColorFromIndex(COLOR_WHITE));

	for (int i = 0; i < CONSOLE.lineWidth; i++)
		Draw_Char((i + 1) * 8, CONSOLE.vislines - 15, text[i]);

	key_lines[edit_line][key_linepos] = 0; // remove cursor
}


/**
 * Draws the last few lines of output transparently over the game top
 */
void Con_DrawNotify()
{
	short* text;
	char* s;
	//int currentColor;
	float fontscale = cl_fontScale->value;

	GL_SetFontColor(GetColorFromIndex(COLOR_WHITE));
	Set_FontShader(true);

	int v = 0;
	int time = 0;
	for (int i = CONSOLE.current - NUM_CON_TIMES + 1; i <= CONSOLE.current; i++)
	{
		if (i < 0) continue;

		time = CONSOLE.times[i % NUM_CON_TIMES];
		if (time == 0)
			continue;
		time = cls.realtime - time;
		if (time > con_notifytime->value * 1000)
			continue;
		text = CONSOLE.text + (i % CONSOLE.totalLines)*CONSOLE.lineWidth;

		int color = COLOR_WHITE;
		for (int x = 0; x < CONSOLE.lineWidth; x++)
		{
			if ((text[x] & 0xff) == ' ') continue;

			if (((text[x] >> 8) & 7) != color)
			{
				color = (text[x] >> 8) & 7;
				GL_SetFontColor(GetColorFromIndex((ColorIndex)color));
			}
			Draw_CharScaled(((x * fontscale) + 1) * 8, v, fontscale, fontscale, text[x] & 0xff);
		}

		v += (8 * fontscale);
	}

	GL_SetFontColor(GetColorFromIndex(COLOR_WHITE));

	int skip = 0;
	if (cls.key_dest == KEY_MESSAGE)
	{
		if (chat_team)
		{
			Draw_StringScaled(8 * fontscale, v, fontscale, fontscale, "say_team:");
			skip = 11;
		}
		else
		{
			Draw_StringScaled(8 * fontscale, v, fontscale, fontscale, "say:");
			skip = 5;
		}

		s = chat_buffer;

		if (chat_bufferlen > ((videoWidth() / fontscale) / 8) - (skip + 1))
			s += chat_bufferlen - (int)(((videoWidth() / fontscale) / 8) - (skip + 1));

		Draw_StringScaled(skip*fontscale * 8, v, fontscale, fontscale, s);
		Draw_CharScaled((strlen(s) + skip)*fontscale * 8, v, fontscale, fontscale, 10 + ((cls.realtime >> 8) & 1));

		v += 8;
	}

	Set_FontShader(false);
}



/**
 * Draws the console with the solid background
 *
 * \param frac	??? Is this how much of the screen should be covered?
 * 
 * \fixme	Constant '8' use throughout this and many other console line
 *			drawing functions. Should be replaced with a named constant and
 *			eventually configurable in some way.
 */
void Con_DrawConsole(float frac)
{
	int lines = videoHeight() * frac;
	if (lines <= 0)
		return;
	if (lines > videoHeight())
		lines = videoHeight();

	int			i, j, x, y, n;
	short		*text;
	int			row;
	char		version[64];
	char		dlbar[1024];
	ColorIndex currentColor;

	// draw the background
	Draw_StretchPic(0, lines - videoHeight(), videoWidth(), videoHeight(), i_conback);

	Set_FontShader(true);

	Q_sprintf(version, sizeof(version), "ra " VERSION " (%s)", __DATE__);
	for (x = 0; x < strlen(version); x++)
	{
		version[x] += 128;
	}

	Draw_String(videoWidth() - 200, lines - 12, version);

	// draw the text <- ???
	CONSOLE.vislines = lines;

	int rows = (lines - 22) / 8;	// rows of text to draw
	y = lines - 30;

	// draw from the bottom up
	if (CONSOLE.display != CONSOLE.current)
	{
		GL_SetFontColor(GetColorFromIndex(COLOR_CYAN));

		// draw arrows to show the buffer is backscrolled
		for (x = 0; x < CONSOLE.lineWidth; x += 4)
		{
			Draw_Char((x + 1) * 8, y, '^');
		}

		GL_SetFontColor(GetColorFromIndex(COLOR_WHITE));
		y -= 8;
		rows--;
	}

	currentColor = COLOR_DEFAULT;
	GL_SetFontColor(GetColorFromIndex(currentColor));

	row = CONSOLE.display;
	for (i = 0; i < rows; i++, y -= 8, row--)
	{
		if (row < 0) { break; }
		if (CONSOLE.current - row >= CONSOLE.totalLines) { break; } // past scrollback wrap point

		text = CONSOLE.text + (row % CONSOLE.totalLines) * CONSOLE.lineWidth;

		for (x = 0; x < CONSOLE.lineWidth; x++)
		{
			if ((text[x] & 0xFF) == ' ') { continue; }

			if (((text[x] >> 8) & 7) != currentColor)
			{
				currentColor = static_cast<ColorIndex>((text[x] >> 8) & 7);
				GL_SetFontColor(GetColorFromIndex(currentColor));
			}

			Draw_Char((x + 1) * 8, y, text[x] & 0xFF);
		}
	}

	if (cls.download)
	{
		char* textch = (char*)text;

		if ((textch = strrchr(cls.downloadname, '/')) != nullptr) textch++;
		else textch = cls.downloadname;

		x = CONSOLE.lineWidth - ((CONSOLE.lineWidth * 7) * 0.025);
		y = x - strlen(textch) - 8;
		i = CONSOLE.lineWidth * 0.3333333333;
		if (strlen(textch) > i)
		{
			y = x - i - 11;
			strncpy(dlbar, textch, i);
			dlbar[i] = 0;
			strcat(dlbar, "...");
		}
		else
			strcpy(dlbar, textch);

		strcat(dlbar, ": ");
		i = strlen(dlbar);
		dlbar[i++] = '\x80';
		if (cls.downloadpercent == 0)
			n = 0;
		else
			n = y * cls.downloadpercent / 100;

		for (j = 0; j < y; j++)
		{
			if (j == n) dlbar[i++] = '\x83';
			else dlbar[i++] = '\x81';
		}

		dlbar[i++] = '\x82';
		dlbar[i] = 0;

		sprintf(dlbar + strlen(dlbar), " %02d%%", cls.downloadpercent);

		y = CONSOLE.vislines - 12;

		for (int i = 0, len = strlen(dlbar); i < len; ++i)
		{
			Draw_Char((i + 1) * 8, y, dlbar[i]);
		}
	}

	Con_DrawInput();
	GL_SetFontColor(GetColorFromIndex(COLOR_WHITE));
	Set_FontShader(false);
}
