#pragma once

#include "client.h"


/**
 * 
 */
class Beam
{
public:
	Beam() {}
	virtual ~Beam() {}


public:
	int entity = 0;
	int dest_entity = 0;
	int endtime = 0;

	Model* model = nullptr;

	vec3_t offset = { 0 };
	vec3_t start = { 0 }, end = { 0 };
};
