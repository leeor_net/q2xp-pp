/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// cl_tent.c -- client side temporary entities

#include "cl_hud.h"

#include "Beam.h"
#include "Explosion.h"

#include "../common/def_renderfx.h"

#include <array>


// ===============================================================================
// = DEFINES
// ===============================================================================
#define	MAX_BEAMS			32		/**<  */
#define	MAX_EXPLOSIONS		32		/**<  */
#define MAX_SUSTAINS		32		/**<  */


// ===============================================================================
// = IMPORT FUNCTIONS
// ===============================================================================
void CL_ClientGibs(vec3_t org, vec3_t velocity);
void CL_BlasterParticles(vec3_t org, vec3_t dir);
void CL_ExplosionParticles(vec3_t org);
void CL_BFGExplosionParticles(vec3_t org);
void CL_ParticleSplashLava(vec3_t org, vec3_t dir);
void CL_ParticleSplashSlime(vec3_t org, vec3_t dir);


// ===============================================================================
// = IMPORT VARIABLES
// ===============================================================================
extern laser_t cl_lasers[MAX_LASERS];
extern ConsoleVariable *hand;





// ===============================================================================
// = MODULE ARRAYS
// ===============================================================================
typedef std::array<Beam, MAX_BEAMS> BeamArray;

BeamArray CL_BEAMS;
BeamArray CL_PLAYER_BEAMS; // PMM Added this for player-linked beams. Currently only used by the plasma beam.

std::array<Explosion, MAX_EXPLOSIONS> CL_EXPLOSIONS;


// ===============================================================================
// = MODULE VARIABLES
// ===============================================================================
ALuint cl_sfx_lashit;
ALuint cl_sfx_railg;
ALuint cl_sfx_rockexp;
ALuint cl_sfx_grenexp;
ALuint cl_sfx_watrexp;
ALuint cl_sfx_plasexp;
ALuint cl_sfx_lightning;
ALuint cl_sfx_disrexp;

Model* cl_mod_explode;
Model* cl_mod_smoke;
Model* cl_mod_parasite_segment;
Model* cl_mod_grapple_cable;
Model* cl_mod_parasite_tip;
Model* cl_mod_explo4;
Model* cl_mod_bfg_explo;
Model* cl_mod_powerscreen;
Model* cl_mod_distort;
Model* cl_mod_plasmaexplo;
Model* cl_mod_lightning;
Model* cl_mod_heatbeam;
Model* cl_mod_monster_heatbeam;
Model* cl_mod_explo4_big;



// ===============================================================================
// = PRIVATE FUNCTIONS
// ===============================================================================

/**
 * 
 */
static bool beamParse(Model* model, BeamArray& beamArray, int ent, bool hasOffset)
{
	vec3_t start = { 0.0f }, end = { 0.0f }, offset = { 0.0f };
	entity_t ents = { 0 };

	MSG_ReadPos(Net_Message(), start);
	MSG_ReadPos(Net_Message(), end);

	if (hasOffset) { MSG_ReadPos(Net_Message(), offset); }

	// override any beam with the same entity
	for (int i = 0; i < MAX_BEAMS; ++i)
	{
		Beam& _beam = beamArray[i];
		if (_beam.entity == ent)
		{
			_beam.entity = ent;
			_beam.model = model;
			_beam.endtime = cl.time + 200;
			
			ents.model = _beam.model;
			ents.flags |= RF_NOSHADOW;
			
			VectorCopy(start, _beam.start);
			VectorCopy(end, _beam.end);
			if (hasOffset) { VectorCopy(offset, _beam.offset); }
			else { VectorClear(_beam.offset); }
			
			return true;
		}
	}

	// find a free beam
	for (int i = 0; i < MAX_BEAMS; ++i)
	{
		Beam& _beam = beamArray[i];
		if (!_beam.model || _beam.endtime < cl.time)
		{
			_beam.model = model;
			_beam.endtime = cl.time + 200;
			
			ents.model = _beam.model;
			ents.flags |= RF_NOSHADOW;
			
			VectorCopy(start, _beam.start);
			VectorCopy(end, _beam.end);
			
			if (hasOffset) { VectorCopy(offset, _beam.offset); }
			else { VectorClear(_beam.offset); }

			return true;
		}
	}

	return false;
}


/**
 * ReadScaledDir - Berserker's non normalized vector compression
 */
static void ReadScaledDir(vec3_t vec)
{
	MSG_ReadDir(Net_Message(), vec);
	float len = MSG_ReadFloat(Net_Message());
	VectorScale(vec, len, vec);
}


// ===============================================================================
// = PUBLIC FUNCTIONS
// ===============================================================================

/**
 * 
 */
void CL_RegisterTEntModels()
{
	cl_mod_explode			= R_RegisterModel("models/objects/explode/tris.md2");
	cl_mod_smoke			= R_RegisterModel("models/objects/smoke/tris.md2");
	cl_mod_parasite_segment	= R_RegisterModel("models/monsters/parasite/segment/tris.md2");
	cl_mod_grapple_cable	= R_RegisterModel("models/ctf/segment/tris.md2");
	cl_mod_parasite_tip		= R_RegisterModel("models/monsters/parasite/tip/tris.md2");

	cl_mod_explo4			= R_RegisterModel("models/misc/dst_model.md2");
	cl_mod_mshell			= R_RegisterModel("models/shells/m_shell.md2");
	cl_mod_sshell			= R_RegisterModel("models/shells/s_shell.md2");

	cl_mod_debris1			= R_RegisterModel("models/objects/debris1/tris.md2");
	cl_mod_debris2			= R_RegisterModel("models/objects/debris2/tris.md2");
	cl_mod_debris3			= R_RegisterModel("models/objects/debris3/tris.md2");

	cl_mod_gib0				= R_RegisterModel("models/gore/chunk1.md2");
	//cl_mod_gib1 <-- ??
	cl_mod_gib2				= R_RegisterModel("models/objects/gibs/bone/tris.md2");
	cl_mod_gib3				= R_RegisterModel("models/objects/gibs/bone2/tris.md2");
	cl_mod_gib4				= R_RegisterModel("models/gore/torso.md2");
	cl_mod_gib5				= R_RegisterModel("models/gore/head2.md2");

	cl_mod_distort			= R_RegisterModel("sprites/distort.sp2");
	cl_mod_bfg_explo		= R_RegisterModel("sprites/s_bfg2.sp2");
	cl_mod_powerscreen		= R_RegisterModel("models/items/armor/effect/tris.md2");

	R_RegisterModel("models/laser/laser.md2");
	R_RegisterModel("models/objects/grenade2/grenade.md2");

	// Precache all first person player guns
	// All items and world guns precache in 3d had
	R_RegisterModel("models/weapons/v_bfg/tris.md2");
	R_RegisterModel("models/weapons/v_blast/tris.md2");
	R_RegisterModel("models/weapons/v_chain/tris.md2");
	R_RegisterModel("models/weapons/v_handgr/tris.md2");
	R_RegisterModel("models/weapons/v_hyperb/tris.md2");
	R_RegisterModel("models/weapons/v_launch/tris.md2");
	R_RegisterModel("models/weapons/v_machn/tris.md2");
	R_RegisterModel("models/weapons/v_rail/tris.md2");
	R_RegisterModel("models/weapons/v_rocket/tris.md2");
	R_RegisterModel("models/weapons/v_shotg/tris.md2");
	R_RegisterModel("models/weapons/v_shotg2/tris.md2");

	//Precache gibs
	R_RegisterModel("models/objects/gibs/bone/tris.md2");
	R_RegisterModel("models/gore/chunk1.md2");
	R_RegisterModel("models/objects/gibs/bone2/tris.md2");
}


/**
 * 
 */
void CL_ClearTEnts()
{
	for (auto it = CL_EXPLOSIONS.begin(); it != CL_EXPLOSIONS.end(); ++it)
		it->clear();

	memset(cl_lasers, 0, sizeof(cl_lasers));
}


/**
 * Gets an available Explosion entity from the Explosion array.
 * 
 * \return	Returns a pointer to an available Explosion.
 * 
 * \note	Will first attempt to find an Explosion that has been set to
 *			EX_FREE. If none is found it will search for the oldest
 *			Explosion and reallocate it.
 */
Explosion* CL_AllocExplosion()
{
	for (int i = 0; i < MAX_EXPLOSIONS; i++)
	{
		if (CL_EXPLOSIONS[i].type == Explosion::EX_FREE)
		{
			CL_EXPLOSIONS[i].clear();
			return &CL_EXPLOSIONS[i];
		}
	}

	// find the oldest explosion
	int time = cl.time;
	int index = 0;

	for (int i = 0; i < MAX_EXPLOSIONS; i++)
	{
		if (CL_EXPLOSIONS[i].start < time)
		{
			time = CL_EXPLOSIONS[i].start;
			index = i;
		}
	}

	CL_EXPLOSIONS[index].clear();
	return &CL_EXPLOSIONS[index];
}


/**
 * 
 */
int CL_ParseBeam(Model* model, bool hasOffset)
{
	int ent = MSG_ReadShort(Net_Message());
	if (!beamParse(model, CL_BEAMS, ent, hasOffset)) { Com_Printf("CL_ParseBeam():: Beam list overflow!\n"); }
	return ent;
}


/**
 * adds to the CL_PLAYER_BEAMS array instead of the cl_beams array
 */
int CL_ParsePlayerBeam(Model* model)
{
	int ent = MSG_ReadShort(Net_Message());
	if (!beamParse(model, CL_PLAYER_BEAMS, ent, true)) { Com_Printf("CL_ParsePlayerBeam():: Beam list overflow!\n"); }
	return ent;
}


/**
 * 
 */
void CL_ParseLaser (int colors)
{
	vec3_t start;
	vec3_t end;
	laser_t *l;
	int i;

	MSG_ReadPos (Net_Message(), start);
	MSG_ReadPos (Net_Message(), end);

	for (i = 0, l = cl_lasers; i < MAX_LASERS; i++, l++) {
		if (l->endtime < cl.time) {
			l->ent.flags = RF_TRANSLUCENT | RF_BEAM;
			VectorCopy (start, l->ent.origin);
			VectorCopy (end, l->ent.oldorigin);
			l->ent.alpha = 0.30;
			l->ent.skinnum = (colors >> ((rand () % 4) * 8)) & 0xff;
			l->ent.model = nullptr;
			l->ent.frame = 4;
			l->endtime = cl.time + 100;
			return;
		}
	}
}


/**
 * Disgusting hack
 */
void CL_FindTrailPlane(vec3_t start, vec3_t end, vec3_t dir)
{
	vec3_t vec, point;

	VectorSubtract(end, start, vec);
	float len = VectorNormalize(vec);
	VectorScaleAndAdd(start, vec, point, len + 0.5);

	trace_t trace = CM_BoxTrace(start, point, vec3_origin, vec3_origin, 0, MASK_SOLID);
	if (trace.allsolid || trace.fraction == 1.0)
	{
		VectorClear(dir);
		return;
	}

	VectorCopy(trace.plane.normal, dir);
}


/**
 * 
 */
void CL_FindRailedSurface (vec3_t start, vec3_t end, vec3_t dir)
{
	vec3_t vec, point;

	VectorSubtract (end, start, vec);
	float len = VectorNormalize (vec);
	VectorScaleAndAdd (start, vec, point, len + 0.5);

	trace_t trace = CM_BoxTrace (start, point, vec3_origin, vec3_origin, 0, MASK_SOLID);

	if (!(trace.surface->flags & SURF_SKY))
	{
		CL_ParticleRailRick (end, dir);
	}
}


/**
 * 
 */
void CL_ParseTEnt()
{
	int type = 0, cont = 0;
	vec3_t pos, pos2, dir, size, velocity, pos3;
	Explosion* ex = nullptr;
	int cnt = 0;
	int color = 0;
	int r = 0;
	int ent = 0;
	int magnitude = 0;

	type = MSG_ReadByte(Net_Message());

	switch (type)
	{

	case TE_BLOOD:				// bullet hitting flesh
		MSG_ReadPos(Net_Message(), pos);
		MSG_ReadDir(Net_Message(), dir);
		CL_ParticleBlood(pos, dir, 3);
		break;

	case TE_SPARKS:
	case TE_BULLET_SPARKS:
		MSG_ReadPos(Net_Message(), pos);
		MSG_ReadDir(Net_Message(), dir);

		if (type == TE_BULLET_SPARKS)
		{
			// impact sound
			cnt = rand() & 15;
			if (cnt < 3)
			{
				S_fastsound(pos, 0, 0, fastsound_descriptor[id_cl_sfx_ric1 + cnt], 1, ATTN_NORM);
			}
		}
		break;

	case TE_SCREEN_SPARKS:
	case TE_SHIELD_SPARKS:
		MSG_ReadPos(Net_Message(), pos);
		MSG_ReadDir(Net_Message(), dir);
		if (type == TE_SCREEN_SPARKS)
		{
			CL_ParticleArmorSpark(pos, dir, 3, false);
		}
		else
		{
			CL_ParticleArmorSpark(pos, dir, 3, true);
		}
		
		/// \fixme Replace or remove this sound
		S_StartSound(pos, 0, 0, cl_sfx_lashit, 1, ATTN_NORM, 0);
		break;

	case TE_GUNSHOT:
	case TE_SHOTGUN:			// bullet hitting wall
		MSG_ReadPos(Net_Message(), pos); // end pos
		MSG_ReadPos(Net_Message(), pos2); // start pos
		MSG_ReadDir(Net_Message(), dir);

		CL_AddDecalToScene(pos, dir, 1, 1, 1, 1, 1, 1, 1, 1, 3, 20000, DECAL_BULLET, 0, frand() * 360, GL_ZERO, GL_ONE_MINUS_SRC_COLOR);

		cont = (CL_PMpointcontents(pos) & MASK_WATER);
		if (!cont) { CL_ParticleRick(pos, dir); }

		if (type != TE_SHOTGUN) { CL_ParticleTracer(pos2, pos); }

		if (type == TE_GUNSHOT) // impact sound
		{
			cnt = rand() & 15;
			if (cnt < 3) { S_fastsound(pos, 0, 0, fastsound_descriptor[id_cl_sfx_ric1 + cnt], 1, ATTN_NORM); }
		}

		break;

	case TE_SPLASH:			// bullet hitting water
		cnt = MSG_ReadByte(Net_Message());
		MSG_ReadPos(Net_Message(), pos);
		MSG_ReadDir(Net_Message(), dir);
		VectorNormalize(dir);
		r = MSG_ReadByte(Net_Message());

		if (r != SPLASH_SPARKS)
		{
			if (r == SPLASH_BLUE_WATER || r == SPLASH_BROWN_WATER) { CL_ParticleSplash(pos, dir, 0.9, 0.9, 0.9); }
			else if (r == SPLASH_LAVA) { CL_ParticleSmoke2(pos, dir, 1, 0.3, 0, 15, true); }
			else if (r == SPLASH_SLIME) { CL_ParticleSplashSlime(pos, dir); }
		}

		if (r == SPLASH_SPARKS)
		{
			S_fastsound(pos, 0, 0, fastsound_descriptor[id_cl_sfx_spark5 + (rand() & 3)], 0.5, ATTN_STATIC);
			CL_ParticleSmoke(pos, dir, 6);
			CL_ParticleSpark(pos, dir, 25);
		}
		break;

	case TE_LASER_SPARKS:
		cnt = MSG_ReadByte(Net_Message());
		MSG_ReadPos(Net_Message(), pos);
		MSG_ReadDir(Net_Message(), dir);
		color = MSG_ReadByte(Net_Message());
		CL_AddDecalToScene(pos, dir, 1, 1, 0, 1, 0, -0.1, 0, 1, 3, 2000, DECAL_BLASTER, 0, frand() * 360, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		CL_LaserParticle(pos, dir, color, cnt);
		break;

	case TE_BLASTER:			// blaster hitting wall
		MSG_ReadPos(Net_Message(), pos);
		MSG_ReadDir(Net_Message(), dir);
		CL_AddDecalToScene(pos, dir, 1, 1, 0, 1, -0.1, -2, 0, 1, 3, 15000, DECAL_BLASTER, 0, frand() * 360, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		CL_ParticleSmoke2(pos, dir, 0.97, 0.46, 0.14, 16, true);
		CL_BlasterParticles(pos, dir);
		S_StartSound(pos, 0, 0, cl_sfx_lashit, 1, ATTN_NORM, 0);
		break;

	case TE_RAILTRAIL:			// railgun effect
		MSG_ReadPos(Net_Message(), pos);
		MSG_ReadPos(Net_Message(), pos2);
		MSG_ReadDir(Net_Message(), dir);
		CL_RailTrail(pos, pos2);
		CL_FindRailedSurface(pos, pos2, dir);
		CL_AddDecalToScene(pos2, dir, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 3.5, 20000, DECAL_RAIL, 0, frand() * 360, GL_ZERO, GL_ONE_MINUS_SRC_COLOR);
		S_StartSound(pos2, 0, 0, cl_sfx_railg, 1, ATTN_NORM, 0);
		break;

	case TE_EXPLOSION2:
	case TE_GRENADE_EXPLOSION:
	case TE_GRENADE_EXPLOSION_WATER:
		MSG_ReadPos(Net_Message(), pos);
		MSG_ReadDir(Net_Message(), dir);
		if (type == TE_EXPLOSION2) { CL_Debris(pos, dir); }
		pos[2] += 3.0; // grenade explosion decal hack
		CL_AddDecalToScene(pos, vec3_origin, 0.1, 0.1, 0.1, 1, 0, 0, 0, 1, 45, 60000, DECAL_EXPLODE, 0, frand() * 360, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		CL_Explosion(pos);
		ex = CL_AllocExplosion();
		VectorCopy(pos, ex->entity.origin);
		ex->type = Explosion::EX_POLY;
		ex->entity.flags = RF_FULLBRIGHT | RF_NOSHADOW | RF_DISTORT;
		ex->entity.alpha = 0.0;
		ex->start = cl.frame.servertime - 100;
		ex->entity.model = cl_mod_distort;
		ex->frames = 5;
		ex->baseframe = 0;
		ex->entity.angles[1] = rand() % 360;

		if (type == TE_GRENADE_EXPLOSION_WATER) { S_StartSound(pos, 0, 0, cl_sfx_watrexp, 1, 0.08, 0); }
		else { S_StartSound(pos, 0, 0, cl_sfx_grenexp, 1, 0.08, 0); }
		break;

	case TE_EXPLOSION1:
	case TE_ROCKET_EXPLOSION:
	case TE_ROCKET_EXPLOSION_WATER:
		MSG_ReadPos(Net_Message(), pos);

		if (type == TE_ROCKET_EXPLOSION || type == TE_ROCKET_EXPLOSION_WATER)
		{
			MSG_ReadDir(Net_Message(), dir);
		}

		CL_AddDecalToScene(pos, vec3_origin, 0.1, 0.1, 0.1, 1, 0, 0, 0, 1, 45, 60000, DECAL_EXPLODE, 0, frand() * 360, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		CL_Explosion(pos);
		ex = CL_AllocExplosion();
		VectorCopy(pos, ex->entity.origin);
		ex->type = Explosion::EX_POLY;
		ex->entity.flags = RF_FULLBRIGHT | RF_NOSHADOW | RF_DISTORT;
		ex->entity.alpha = 0.0;
		ex->start = cl.frame.servertime - 100;
		ex->entity.model = cl_mod_distort;
		ex->frames = 5;
		ex->baseframe = 0;
		ex->entity.angles[1] = rand() % 360;

		if (type == TE_ROCKET_EXPLOSION_WATER) { S_StartSound(pos, 0, 0, cl_sfx_watrexp, 1, 0.08, 0); }
		else { S_StartSound(pos, 0, 0, cl_sfx_rockexp, 1, 0.08, 0); }
		break;

	case TE_BFG_EXPLOSION:
		MSG_ReadPos(Net_Message(), pos);
		ex = CL_AllocExplosion();
		VectorCopy(pos, ex->entity.origin);
		ex->type = Explosion::EX_POLY;
		ex->entity.flags = RF_FULLBRIGHT;
		ex->start = cl.frame.servertime - 100;
		ex->light = 350;
		ex->lightColor(0.0f, 1.0f, 0.0f);
		ex->entity.model = cl_mod_bfg_explo;
		ex->entity.flags |= RF_TRANSLUCENT;
		ex->entity.flags |= RF_DISTORT;
		ex->entity.flags |= RF_BFG_SPRITE;
		ex->entity.alpha = 0.30;
		ex->frames = 4;
		break;

	case TE_BFG_BIGEXPLOSION:
		MSG_ReadPos(Net_Message(), pos);
		MSG_ReadDir(Net_Message(), dir);
		CL_AddDecalToScene(pos, vec3_origin, 0.1, 0.1, 0.1, 1, 0.1, 0.1, 0.1, 1, 45, 60000, DECAL_BFG, 0, frand() * 360, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		CL_BFGExplosionParticles(pos);
		break;

	case TE_BFG_LASER:
		CL_ParseLaser(0xd0d1d2d3);
		break;

	case TE_BUBBLETRAIL:
		MSG_ReadPos(Net_Message(), pos);
		MSG_ReadPos(Net_Message(), pos2);
		CL_BubbleTrail(pos, pos2);
		break;

	case TE_GRAPPLE_CABLE:
		ent = CL_ParseBeam(cl_mod_grapple_cable, true);
		break;

	case TE_REBORN:
		MSG_ReadPos(Net_Message(), pos);
		MSG_ReadDir(Net_Message(), dir);
		CL_GibExplosion(pos, dir);
		CL_ParticleGibBlood(pos);
		break;

	case TE_GIB:
		MSG_ReadPos(Net_Message(), pos);
		CL_ParticleGibBlood(pos);
		break;

	case TE_GIB_CLIENT:
		MSG_ReadPos(Net_Message(), pos3);
		size[0] = MSG_ReadByte(Net_Message());
		size[1] = MSG_ReadByte(Net_Message());
		size[2] = MSG_ReadByte(Net_Message());
		ReadScaledDir(velocity);          // read the 5 bytes instead 12
		VectorAdd(pos3, size, pos2);
		pos[0] = pos2[0] + crandom() * size[0];
		pos[1] = pos2[1] + crandom() * size[1];
		pos[2] = pos2[2] + crandom() * size[2];
		CL_ClientGibs(pos, velocity);
		CL_ParticleGibBlood(pos);
		break;

	default:
		Com_Error(ERR_DROP, "CL_ParseTEnt():: Bad entity type.");
	}
}


/**
 * 
 */
void CL_AddBeams()
{
	int i;
	vec3_t dist, org;
	float d;
	entity_t ent;
	float yaw, pitch;
	float forward;
	float len, steps;
	float model_length;

	// update beams
	for (i = 0; i < MAX_BEAMS; ++i)
	{
		Beam& _beam = CL_BEAMS[i];

		if (!_beam.model || _beam.endtime < cl.time) { continue; }

		// if coming from the player, update the start position
		if (_beam.entity == cl.playernum + 1)	// entity 0 is the world
		{
			VectorCopy(cl.refdef.vieworg, _beam.start);
			_beam.start[2] -= 22;	// adjust for view height
		}

		VectorAdd(_beam.start, _beam.offset, org);

		// calculate pitch and yaw
		VectorSubtract(_beam.end, org, dist);

		if (dist[1] == 0 && dist[0] == 0)
		{
			yaw = 0;
			if (dist[2] > 0) { pitch = 90; }
			else { pitch = 270; }
		}
		else
		{
			// PMM - fixed to correct for pitch of 0
			if (dist[0]) { yaw = (atan2(dist[1], dist[0]) * 180 / M_PI); }
			else if (dist[1] > 0) { yaw = 90; }
			else { yaw = 270; }
			if (yaw < 0) { yaw += 360; }

			forward = sqrt(dist[0] * dist[0] + dist[1] * dist[1]);
			pitch = (atan2(dist[2], forward) * -180.0 / M_PI);
			if (pitch < 0) { pitch += 360.0; }
		}

		// add new entities for the beams
		d = VectorNormalize(dist);

		memset(&ent, 0, sizeof(ent));
		if (_beam.model == cl_mod_lightning)
		{
			model_length = 35.0;
			d -= 20.0; // correction so it doesn't end in middle of tesla
		}
		else
		{
			model_length = 30.0;
		}

		steps = ceil(d / model_length);
		len = (d - model_length) / (steps - 1);

		// PMM - special case for lightning model .. if the real length is
		// shorter than the model, flip it around & draw it from the end to
		// the start. This prevents the model from going through the tesla
		// mine (instead it goes through the target)
		if ((_beam.model == cl_mod_lightning) && (d <= model_length))
		{
			VectorCopy(_beam.end, ent.origin);
			ent.model = _beam.model;
			ent.flags = RF_FULLBRIGHT | RF_NOSHADOW;
			ent.angles[0] = pitch;
			ent.angles[1] = yaw;
			ent.angles[2] = rand() % 360;
			V_AddEntity(&ent);
			return;
		}
		while (d > 0)
		{
			VectorCopy(org, ent.origin);
			ent.model = _beam.model;

			if (_beam.model == cl_mod_lightning)
			{
				ent.flags = RF_FULLBRIGHT | RF_NOSHADOW;
				ent.angles[0] = -pitch;
				ent.angles[1] = yaw + 180.0;
				ent.angles[2] = rand() % 360;
			}
			else
			{
				ent.angles[0] = pitch;
				ent.angles[1] = yaw;
				ent.angles[2] = rand() % 360;
			}

			ent.flags |= RF_NOSHADOW;
			V_AddEntity(&ent);

			VectorScaleAndAdd(org, dist, org, len);

			d -= model_length;
		}
	}
}


/**
 * ROGUE - draw player locked beams
 * 
 * \fixme	There is a lot of copy/paste code from CL_AddBeams
 */
void CL_AddPlayerBeams()
{
	int i, j;
	vec3_t dist, org;
	float d;
	entity_t ent;
	float yaw, pitch;
	float forward;
	float len, steps;
	int framenum;
	float model_length;

	float hand_multiplier;
	frame_t *oldframe;
	player_state_t *ps, *ops;

	if (hand)
	{
		if (hand->value == 2) { hand_multiplier = 0; }
		else if (hand->value == 1) { hand_multiplier = -1; }
		else { hand_multiplier = 1; }
	}
	else
	{
		hand_multiplier = 1;
	}

	for (i = 0; i < MAX_BEAMS; ++i)
	{
		Beam& _beam = CL_PLAYER_BEAMS[i];
		vec3_t f, r, u;
		if (!_beam.model || _beam.endtime < cl.time) { continue; }

		if (cl_mod_heatbeam && (_beam.model == cl_mod_heatbeam))
		{
			ent.model = _beam.model;
			ent.flags = RF_FULLBRIGHT | RF_NOSHADOW;

			// if coming from the player, update the start position
			if (_beam.entity == cl.playernum + 1)	// entity 0 is the world
			{
				// set up gun position
				ps = &cl.frame.playerstate;
				j = (cl.frame.serverframe - 1) & UPDATE_MASK;
				oldframe = &cl.frames[j];
				if (oldframe->serverframe != cl.frame.serverframe - 1 || !oldframe->valid)
				{
					oldframe = &cl.frame; // previous frame was dropped or involid
				}

				ops = &oldframe->playerstate;

				for (j = 0; j < 3; j++)	/// \fixme Magic number 3
				{
					_beam.start[j] = cl.refdef.vieworg[j] + ops->gunoffset[j] + cl.lerpfrac * (ps->gunoffset[j] - ops->gunoffset[j]);
				}

				VectorScaleAndAdd(_beam.start, cl.v_right, org, (hand_multiplier * _beam.offset[0]));
				VectorScaleAndAdd(org, cl.v_forward, org, _beam.offset[1]);
				VectorScaleAndAdd(org, cl.v_up, org, _beam.offset[2]);

				if ((hand) && (hand->value == 2))
				{
					VectorScaleAndAdd(org, cl.v_up, org, -1);
				}

				// FIXME - take these out when final
				VectorCopy(cl.v_right, r);
				VectorCopy(cl.v_forward, f);
				VectorCopy(cl.v_up, u);
			}
			else
			{
				VectorCopy(_beam.start, org);
			}
		}
		else
		{
			// if coming from the player, update the start position
			if (_beam.entity == cl.playernum + 1)	// entity 0 is the world
			{
				VectorCopy(cl.refdef.vieworg, _beam.start);
				_beam.start[2] -= 22;	// adjust for view height
			}
			VectorAdd(_beam.start, _beam.offset, org);
		}

		// calculate pitch and yaw
		VectorSubtract(_beam.end, org, dist);

		if (cl_mod_heatbeam && (_beam.model == cl_mod_heatbeam) && (_beam.entity == cl.playernum + 1))
		{
			vec_t len = VectorLength(dist);
			VectorScale(f, len, dist);
			VectorScaleAndAdd(dist, r, dist, (hand_multiplier * _beam.offset[0]));
			VectorScaleAndAdd(dist, f, dist, _beam.offset[1]);
			VectorScaleAndAdd(dist, u, dist, _beam.offset[2]);

			if ((hand) && (hand->value == 2))
			{
				VectorScaleAndAdd(org, cl.v_up, org, -1);
			}
		}

		if (dist[1] == 0 && dist[0] == 0)
		{
			yaw = 0;
			if (dist[2] > 0) { pitch = 90; }
			else { pitch = 270; }
		}
		else
		{
			if (dist[0]) { yaw = (atan2(dist[1], dist[0]) * 180 / M_PI); }
			else if (dist[1] > 0) { yaw = 90; }
			else { yaw = 270; }
			if (yaw < 0) { yaw += 360; }

			forward = sqrt(dist[0] * dist[0] + dist[1] * dist[1]);
			pitch = (atan2(dist[2], forward) * -180.0 / M_PI);

			if (pitch < 0) { pitch += 360.0; }
		}

		if (cl_mod_heatbeam && (_beam.model == cl_mod_heatbeam))
		{
			if (_beam.entity != cl.playernum + 1)
			{
				framenum = 2;
				ent.flags = RF_NOSHADOW;
				ent.angles[0] = -pitch;
				ent.angles[1] = yaw + 180.0;
				ent.angles[2] = 0;
				AngleVectors(ent.angles, f, r, u);

				// if it's a non-origin offset, it's a player, so use the hardcoded player offset
				if (!VectorsEqual(_beam.offset, vec3_origin))
				{
					VectorScaleAndAdd(org, r, org, -(_beam.offset[0]) + 1);
					VectorScaleAndAdd(org, f, org, -(_beam.offset[1]));
					VectorScaleAndAdd(org, u, org, -(_beam.offset[2]) - 10);
				}
				else
				{
					// if it's a monster, do the particle effect
					CL_MonsterPlasma_Shell(_beam.start);
				}
			}
			else
			{
				framenum = 1;
			}
		}

		// if it's the heatbeam, draw the particle effect
		if ((cl_mod_heatbeam && (_beam.model == cl_mod_heatbeam) && (_beam.entity == cl.playernum + 1)))
		{
			CL_Heatbeam(org, dist);
			ent.flags = RF_FULLBRIGHT | RF_NOSHADOW;
		}

		// add new entities for the beams
		d = VectorNormalize(dist);

		memset(&ent, 0, sizeof(ent));
		if (_beam.model == cl_mod_heatbeam)
		{
			model_length = 32.0;
		}
		else if (_beam.model == cl_mod_lightning)
		{
			model_length = 35.0;
			d -= 20.0; // correction so it doesn't end in middle of tesla
		}
		else
		{
			model_length = 30.0;
		}

		steps = ceil(d / model_length);
		len = (d - model_length) / (steps - 1);

		// PMM - special case for lightning model .. if the real length is
		// shorter than the model, flip it around & draw it from the end to
		// the start.  This prevents the model from going through the tesla
		// mine (instead it goes through the target)
		if ((_beam.model == cl_mod_lightning) && (d <= model_length))
		{
			VectorCopy(_beam.end, ent.origin);
			ent.model = _beam.model;
			ent.flags = RF_FULLBRIGHT | RF_NOSHADOW;
			ent.angles[0] = pitch;
			ent.angles[1] = yaw;
			ent.angles[2] = rand() % 360;
			V_AddEntity(&ent);
			return;
		}
		while (d > 0)
		{
			VectorCopy(org, ent.origin);
			ent.model = _beam.model;
			if (cl_mod_heatbeam && (_beam.model == cl_mod_heatbeam))
			{
				ent.flags = RF_FULLBRIGHT | RF_NOSHADOW;
				ent.angles[0] = -pitch;
				ent.angles[1] = yaw + 180.0;
				ent.angles[2] = (cl.time) % 360;
				ent.frame = framenum;
			}
			else if (_beam.model == cl_mod_lightning)
			{
				ent.flags = RF_FULLBRIGHT | RF_NOSHADOW;
				ent.angles[0] = -pitch;
				ent.angles[1] = yaw + 180.0;
				ent.angles[2] = rand() % 360;
			}
			else
			{
				ent.angles[0] = pitch;
				ent.angles[1] = yaw;
				ent.angles[2] = rand() % 360;
			}

			V_AddEntity(&ent);

			for (j = 0; j < 3; j++)
			{
				org[j] += dist[j] * len;
			}
			d -= model_length;
		}
	}
}


/**
 * 
 */
void CL_AddExplosions()
{
	for (int i = 0; i < MAX_EXPLOSIONS; ++i)
	{
		Explosion& explosion = CL_EXPLOSIONS[i];

		if (explosion.type == Explosion::EX_FREE) { continue; }

		float frac = (cl.time - explosion.start) / 100.0;
		int f = floor(frac);


		switch (explosion.type)
		{
		case Explosion::EX_MFLASH:
			if (f >= explosion.frames - 1)
			{
				explosion.type = Explosion::EX_FREE;
			}
			break;

		case Explosion::EX_MISC:
			if (f >= explosion.frames - 1)
			{
				explosion.type = Explosion::EX_FREE;
				break;
			}
			explosion.entity.alpha = 1.0f - frac / (explosion.frames - 1);
			break;

		case Explosion::EX_FLASH:
			if (f >= 1)
			{
				explosion.type = Explosion::EX_FREE;
				break;
			}
			explosion.entity.alpha = 1.0f;
			break;

		case Explosion::EX_POLY:
			if (f >= explosion.frames - 1)
			{
				explosion.type = Explosion::EX_FREE;
				break;
			}

			explosion.entity.alpha = (16.0f - (float)f) / 16.0f;

			if (f < 10)
			{
				explosion.entity.skinnum = (f >> 1);
				if (explosion.entity.skinnum < 0) { explosion.entity.skinnum = 0; }
			}
			else
			{
				explosion.entity.flags |= RF_TRANSLUCENT;
				if (f < 13) { explosion.entity.skinnum = 5; }
				else { explosion.entity.skinnum = 6; }
			}
			break;

		case Explosion::EX_POLY2:
			if (f >= explosion.frames - 1)
			{
				explosion.type = Explosion::EX_FREE;
				break;
			}
			explosion.entity.alpha = (5.0f - (float)f) / 5.0f;
			explosion.entity.skinnum = 0;
			explosion.entity.flags |= RF_TRANSLUCENT;
			break;

		case Explosion::EX_FREE:
			break;

		case Explosion::EX_EXPLOSION:
			break;
		}

		if (explosion.type == Explosion::EX_FREE)
		{
			continue;
		}

		if (explosion.hasLight())
		{
			V_AddLight(explosion.entity.origin, explosion.light * explosion.entity.alpha, explosion.lightColor()[0], explosion.lightColor()[1], explosion.lightColor()[2], vec3_origin, 0, 0);
		}

		VectorCopy(explosion.entity.origin, explosion.entity.oldorigin);

		if (f < 0) { f = 0; }

		explosion.entity.frame = explosion.baseframe + f + 1;
		explosion.entity.oldframe = explosion.baseframe + f;
		explosion.entity.backlerp = 1.0f - cl.lerpfrac;

		V_AddEntity(&explosion.entity);
	}
}


/**
 * 
 */
void CL_AddTEnts ()
{
	CL_AddBeams ();
	CL_AddPlayerBeams ();
	CL_AddExplosions ();
	CL_AddLasers ();
}
