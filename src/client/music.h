#pragma once

void Music_Init();
void Music_Shutdown();
void Music_Play();
void Music_Stop();
void Music_Pause();
void Music_Resume();
void Music_Update();
