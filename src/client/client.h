/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
#pragma once

//define    PARANOID            // speed sapping error checking

#include <math.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>

#include "ref.h"
#include "vid.h"

#include "screen.h"
#include "sound.h"
#include "input.h"
#include "keys.h"
#include "cl_console.h"
#include "snd_loc.h"

#include "../common/filesystem.h"
#include "../common/net_chan.h"

#include "../game/game.h"

#include "../win32/win_network.h"


// Berserker client entities code
#define MAX_CLENTITIES								256
#define	MAX_LASERS									32
#define MAX_CLIENTWEAPONMODELS						20	// PGM -- upped from 16 to fit the chainfist vwep
#define	CMD_BACKUP									64		// allow a lot of command backups for very fast systems
#define MAX_FOV										120.0f
#define	MAX_PARSE_ENTITIES							1024
#define INSTANT_PARTICLE							-10000.f
#define NUM_CROSS_FRAMES							15


#define sound_precache_hacks_FLAT2D					1
#define sound_precache_hacks_AUTOFIX_gain			2
#define sound_precache_hacks_AUTOFIX_rolloff_factor	4

//=============================================================================
// Sound effect ID's:

enum SoundID
{
	weapons_blastf1a,
	weapons_hyprbf1a,
	weapons_gunshot1,
	weapons_gunshot2,
	weapons_gunshot3,
	weapons_gunshot4,
	weapons_shotgun_1_fire,
	weapons_shotgun_1_reload,
	weapons_sshotf1b,
	weapons_railgf1a,
	weapons_rocklf1a,
	weapons_rocklr1b,
	weapons_grenlf1a,
	weapons_grenlr1b,
	weapons_bfg__f1y,
	misc_talk,

	MENU_IN_SOUND,
	MENU_MOVE_SOUND,
	MENU_OUT_SOUND,

	id_cl_sfx_ric1,	// +0
	id_cl_sfx_ric2,	// +1
	id_cl_sfx_ric3,	// +2
	id_cl_sfx_lashit,
	id_cl_sfx_spark5, // +0
	id_cl_sfx_spark6, // +1
	id_cl_sfx_spark7, // +2
	id_cl_sfx_railg,
	id_cl_sfx_rockexp,
	id_cl_sfx_grenexp,
	id_cl_sfx_watrexp,

	//q2xp
	id_cl_sfx_lava,
	id_cl_sfx_shell,
	id_cl_sfx_debris,
	id_cl_sfx_footsteps_0,
	id_cl_sfx_footsteps_1,
	id_cl_sfx_footsteps_2,
	id_cl_sfx_footsteps_3,

	//PGM
	id_cl_sfx_lightning,
	id_cl_sfx_disrexp,
	id_radar_sound,

	SOUND_ID_COUNT
};


//=============================================================================

trace_t SV_Trace(vec3_t start, vec3_t mins, vec3_t maxs, vec3_t end, edict_t * passedict, int contentmask);
trace_t CL_Trace(vec3_t start, vec3_t end, float size, int contentmask);
void CL_ParticleSmoke2(vec3_t org, vec3_t dir, float r, float g, float b, int count, bool add);


typedef struct
{
	entity_t ent;
	int endtime;
} laser_t;


extern vec3_t viewweapon;



typedef struct
{
	bool valid;						// cleared if delta parsing was invalid
	int serverframe;
	int servertime;						// server time the message is valid for (in msec)
	int deltaframe;
	byte areabits[MAX_MAP_AREAS / 8];	// portalarea visibility bits
	player_state_t playerstate;
	int num_entities;
	int parse_entities;					// non-masked index into cl_parse_entities array
} frame_t;


typedef struct
{
	entity_state_t baseline;	// delta from this if not from a previous frame
	entity_state_t current;
	entity_state_t prev;		// will always be valid, but might just be a copy of current

	int serverframe;			// if not current, this ent isn't in the frame
	int trailcount;				// for diminishing grenade trails
	vec3_t lerp_origin;			// for trails (variable hz)

	int fly_stoptime;
} centity_t;


typedef struct
{
	char name[MAX_QPATH];
	char cinfo[MAX_QPATH];
	Image *skin;
	Image *icon;
	Image *bump;
	char iconname[MAX_QPATH];
	char sex[MAX_QPATH];
	Model* model;
	Model* weaponmodel[MAX_CLIENTWEAPONMODELS];
} clientinfo_t;

extern char cl_weaponmodels[MAX_CLIENTWEAPONMODELS][MAX_QPATH];
extern int num_cl_weaponmodels;


// the client_state_t structure is wiped completely at every
// server map change
typedef struct
{
	int timeoutcount;

	int timedemo_frames;
	int timedemo_start;

	bool refresh_prepped;					// false if on new level or new ref dll
	bool sound_prepped;						// ambient sounds can start
	bool force_refdef;						// vid has changed, so we can't use a paused refdef

	int parse_entities;						// index (not anded off) into cl_parse_entities[]

	usercmd_t cmd;
	usercmd_t cmds[CMD_BACKUP];				// each mesage will send several old cmds
	int cmd_time[CMD_BACKUP];				// time sent, for calculating pings
	short predicted_origins[CMD_BACKUP][3];	// for debug comparing against server

	float predicted_step;					// for stair up smoothing
	unsigned predicted_step_time;

	vec3_t predicted_origin;				// generated by CL_PredictMovement
	vec3_t predicted_angles;
	vec3_t prediction_error;

	frame_t frame;							// received from server
	int surpressCount;						// number of messages rate supressed
	frame_t frames[UPDATE_BACKUP];

	// The client maintains its own idea of view angles, which are sent to the
	// server each frame.  It is cleared to 0 upon entering each level.
	// 
	// The server sends a delta each frame which is added to the locally tracked
	// view angles to account for standing on rotating objects, and teleport
	// direction changes
	vec3_t viewangles;
	float		viewangles_YAW;
	float		viewangles_PITCH;

	int time;								// this is the time value that the client is rendering at.  always <= cls.realtime
	float lerpfrac;							// between oldframe and frame
	
	int minFps, maxFps;

	refdef_t refdef;

	vec3_t v_forward, v_right, v_up;		// set when refdef.angles is set

	// Transient data from server
	char layout[1024];						// general 2D overlay
	int inventory[MAX_ITEMS];

	// non-gameserver infornamtion

	// server state information
	bool attractloop;						// running the attract loop, any key will menu
	int servercount;						// server identification for prespawns
	char gamedir[MAX_QPATH];
	int playernum;

	char configstrings[MAX_CONFIGSTRINGS][MAX_QPATH];

	// Locally derived information from server state
	Model* model_draw[MAX_MODELS];
	cmodel_t *model_clip[MAX_MODELS];

	ALuint sound_precache[MAX_SOUNDS];
	const char* sound_sexedname[MAX_SOUNDS];

	unsigned long sound_precache_hacks[MAX_SOUNDS];
	ALfloat sound_precache_rolloff_factor[MAX_SOUNDS];
	ALfloat sound_precache_gain[MAX_SOUNDS];


	Image* image_precache[MAX_IMAGES];

	clientinfo_t clientinfo[MAX_CLIENTS];
	clientinfo_t baseclientinfo;

	vec3_t v_Forward, v_Right, v_Up;		// set when refdef.angles is set
} client_state_t;

extern client_state_t cl;

/*
==================================================================
the client_static_t structure is persistant through an arbitrary number
of server connections
==================================================================
*/
typedef enum
{
	ca_uninitialized,
	ca_disconnected,			// not talking to a server
	ca_connecting,				// sending request packets to the server
	ca_connected,				// netchan_t established, waiting for svc_serverdata
	ca_active					// game views should be displayed
} connstate_t;


/**
 * Download Type
 */
typedef enum
{
	DL_NONE,
	DL_MODEL,
	DL_SOUND,
	DL_SKIN,
	DL_SINGLE
} dltype_t;


/**
 * Keycode desitnation
 */
typedef enum
{
	KEY_GAME,
	KEY_CONSOLE,
	KEY_MESSAGE,
	KEY_MENU
} keydest_t;


/**
 * \todo	DOCUMENT ME
 */
typedef struct
{
	connstate_t state;
	keydest_t key_dest;
	bool consoleActive;
	bool menuActive;

	int framecount;
	int realtime;				// always increasing, no clamping, etc
	float frametime;			// seconds since last frame

	// screen rendering information
	float disable_screen;		// showing loading plaque between levels or changing rendering dlls if time gets > 30 seconds ahead, break it
	int disable_servercount;	// when we receive a frame and cl.servercount > cls.disable_servercount, clear disable_screen

	// connection information
	char servername[MAX_OSPATH];	// name of server from original connect
	float connect_time;				// for connection retransmits

	int quakePort;					// a 16 bit value that allows quake servers to work around address translating routers
	netchan_t netchan;
	int serverProtocol;				// in case we are doing some kind of version hack

	int challenge;					// from the server to use for connecting

	FS_File* download;				// file transfer from server
	char downloadtempname[MAX_OSPATH];
	char downloadname[MAX_OSPATH];
	int downloadnumber;
	dltype_t downloadtype;
	int downloadpercent;

	// demo recording info must be here, so it isn't cleared on level change
	bool demorecording;
	bool demowaiting;			// don't record until a non-delta message is received
	FS_File* demofile;

} client_static_t;

extern client_static_t cls;

//=============================================================================

//
// cvars
//
extern ConsoleVariable *cl_stereo_separation;
extern ConsoleVariable *cl_stereo;

extern ConsoleVariable *cl_gun;
extern ConsoleVariable *cl_add_blend;
extern ConsoleVariable *cl_add_lights;
extern ConsoleVariable *cl_add_particles;
extern ConsoleVariable *cl_add_entities;
extern ConsoleVariable *cl_predict;
extern ConsoleVariable *cl_footsteps;
extern ConsoleVariable *cl_noskins;
extern ConsoleVariable *cl_autoskins;

extern ConsoleVariable *zoomfov;

extern ConsoleVariable *cl_upspeed;
extern ConsoleVariable *cl_forwardspeed;
extern ConsoleVariable *cl_sidespeed;

extern ConsoleVariable *cl_yawspeed;
extern ConsoleVariable *cl_pitchspeed;

extern ConsoleVariable *cl_run;

extern ConsoleVariable *cl_anglespeedkey;

extern ConsoleVariable *cl_shownet;
extern ConsoleVariable *cl_showmiss;
extern ConsoleVariable *cl_showclamp;

extern ConsoleVariable *dmflags;

extern ConsoleVariable *lookspring;
extern ConsoleVariable *lookstrafe;
extern ConsoleVariable *sensitivity;

extern ConsoleVariable *m_pitch;
extern ConsoleVariable *m_yaw;
extern ConsoleVariable *m_forward;
extern ConsoleVariable *m_side;

extern ConsoleVariable *freelook;

extern ConsoleVariable *cl_lightlevel;	// FIXME HACK

extern ConsoleVariable *cl_paused;
extern ConsoleVariable *cl_timedemo;

extern ConsoleVariable *cl_vwep;
extern ConsoleVariable *cl_drawFPS;
extern ConsoleVariable *cl_drawTime;


extern ConsoleVariable *cl_thirdPerson;;
extern ConsoleVariable *cl_thirdPersonAngle;
extern ConsoleVariable *cl_thirdPersonRange;
extern ConsoleVariable *cl_blood;

extern ConsoleVariable *music_source;
extern ConsoleVariable *music_volume;

extern ConsoleVariable *cl_brass;

extern ConsoleVariable *cl_railcore_red;
extern ConsoleVariable *cl_railcore_green;
extern ConsoleVariable *cl_railcore_blue;
extern ConsoleVariable *cl_railspiral_red;
extern ConsoleVariable *cl_railspiral_green;
extern ConsoleVariable *cl_railspiral_blue;

extern ConsoleVariable *cl_decals;
extern ConsoleVariable *cl_drawhud;
extern ConsoleVariable *s_initsound;
extern ConsoleVariable *cl_fontScale;
extern ConsoleVariable *cl_itemsBobbing;

extern ALuint cl_sfx_lava;
extern ALuint cl_sfx_shell;
extern ALuint cl_sfx_debris;
extern Model* cl_mod_mshell;
extern Model* cl_mod_sshell;
extern Model* cl_mod_debris1;
extern Model* cl_mod_debris2;
extern Model* cl_mod_debris3;
extern Model* cl_mod_gib0;
extern Model* cl_mod_gib1;
extern Model* cl_mod_gib2;
extern Model* cl_mod_gib3;
extern Model* cl_mod_gib4;
extern Model* cl_mod_gib5;
extern Model* cl_mod_debris0;


extern vec3_t r_origin;
extern vec3_t  smoke_puff, shell_puff;


typedef struct
{
	int key;					// so entities can reuse same entry
	vec3_t color;
	vec3_t origin;
	float radius;
	float die;					// stop lighting after this time
	float decay;				// drop this each second
	float minlight;				// don't add when contributing less
} cdlight_t;


extern centity_t cl_entities[MAX_EDICTS];
extern cdlight_t cl_dlights[MAX_DLIGHTS];


// the cl_parse_entities must be large enough to hold UPDATE_BACKUP frames of
// entities, so that when a delta compressed message arives from the server
// it can be un-deltad from the original
extern entity_state_t cl_parse_entities[MAX_PARSE_ENTITIES];


int CL_PMpointcontents(vec3_t point);
void CL_GetEntityOrigin(int ent, vec3_t origin);

void CL_InitImages();
void CL_ClearParticles();
void CL_ClearDecals();
void CL_ClearClEntities();
void CL_AddClEntities();
void CL_AddLasers();

void VectorReflect(const vec3_t v, const vec3_t normal, vec3_t out);
void V_ClearScene();

void CL_ParticleBlood(vec3_t org, vec3_t dir, int count);
void CL_ParticleRick(vec3_t org, vec3_t dir);
void CL_ParticleSplash(vec3_t org, vec3_t dir, float r, float g, float b);
void CL_ParticleSmoke(vec3_t org, vec3_t dir, int count);
void CL_TeleportParticles(vec3_t org);
void CL_TeleporterParticles(entity_state_t * ent);
void CL_RocketFire(vec3_t start, vec3_t end);
void CL_ParticleSpark(vec3_t org, vec3_t dir, int count);
void CL_LaserParticle(vec3_t org, vec3_t dir, int color, int count);
void CL_Debris(vec3_t org, vec3_t dir);
void CL_Explosion(vec3_t org);
void CL_GibExplosion(vec3_t org, vec3_t dir);
void CL_ParticleTracer(vec3_t start, vec3_t end);
void CL_ParticleArmorSpark(vec3_t org, vec3_t dir, int count, bool power);
void CL_ParticleGibBlood(vec3_t org);
void CL_ParticleGunSmoke(vec3_t org, vec3_t dir, int count);

/// \fixme	Find a sane place for these
bool AL_Init(int hardreset);
void AL_Shutdown();
void S_fastsound(vec3_t origin, int entnum, int entchannel, ALuint bufferNum, ALfloat gain, ALfloat rolloff_factor);


extern vec3_t dclAng;

extern const float PARTICLE_GRAVITY;




typedef struct clentity_s
{
	struct clentity_s *next;
	float time;
	vec3_t org;
	vec3_t lastOrg;
	vec3_t vel;
	vec3_t accel;
	Model* model;
	float ang;
	float avel;
	float alpha;
	float alphavel;
	int flags;

} clentity_t;

void CL_ClearEffects();
void CL_ClearTEnts();
void CL_BlasterTrail(vec3_t start, vec3_t end);
void CL_RailTrail(vec3_t start, vec3_t end);
void CL_BubbleTrail(vec3_t start, vec3_t end);
void CL_FlagTrail(vec3_t start, vec3_t end, float r, float g, float b);

void CL_IonripperTrail(vec3_t start, vec3_t end);

void CL_Flashlight(int ent, vec3_t pos);

void CL_Heatbeam(vec3_t start, vec3_t end);
void CL_TrackerTrail(vec3_t start, vec3_t end, int particleColor);

void CL_TagTrail(vec3_t start, vec3_t end, float color);
void CL_ColorFlash(vec3_t pos, int ent, int intensity, float r, float g, float b);
void CL_Tracker_Shell(vec3_t origin);
void CL_MonsterPlasma_Shell(vec3_t origin);

int CL_ParseEntityBits(unsigned *bits);
void CL_ParseDelta(entity_state_t * from, entity_state_t * to, int number, int bits);
void CL_ParseFrame();

void CL_ParseTEnt();
void CL_ParseConfigString();
void CL_ParseMuzzleFlash();
void CL_ParseMuzzleFlashMonsters();

void CL_SetLightstyle(int i);

void CL_RunDLights();
void CL_RunLightStyles();

void CL_AddEntities();
void CL_AddDLights();
void CL_AddTEnts();
void CL_AddLightStyles();

void CL_PrepRefresh();
void CL_RegisterSounds();

void CL_Quit_f();

//
// cl_main
//
void CL_Init ();

void CL_FixUpGender ();
void CL_Disconnect ();
void CL_Disconnect_f ();
void CL_PingServers_f ();
void CL_Snd_Restart_f ();
void CL_RequestNextDownload ();

///\fixme	Have this pass a structure instead of fourteen thousand paramters.
void CL_AddDecalToScene (vec3_t origin, vec3_t dir,	float red, float green, float blue, float alpha, float endRed, float endGreen, float endBlue, float endAlpha, float size,	float endTime, int type, int flags, float angle, int sFactor, int dFactor);

trace_t CL_PMTraceWorld (vec3_t start, vec3_t mins, vec3_t maxs, vec3_t end, int mask, bool checkAliases);
void CL_NewDlight (int key, vec3_t org, float r, float g, float b, float radius, float time);
void CL_ParticleRailRick (vec3_t org, vec3_t dir);

// cl_input
typedef struct
{
	int down[2];				// key nums holding it down
	unsigned downtime;			// msec timestamp
	unsigned msec;				// msec down this frame
	int state;
} kbutton_t;

extern kbutton_t in_mlook, in_klook;
extern kbutton_t in_strafe;
extern kbutton_t in_speed;
extern kbutton_t in_zoom;

void CL_InitInput();
void CL_SendCmd();

void CL_ClearState();

void CL_ReadPackets();
void CL_BaseMove(usercmd_t * cmd);

void IN_CenterView();

float CL_KeyState(kbutton_t * key);
char* Key_KeynumToString(int keynum);

//
// cl_demo.c
//
void CL_WriteDemoMessage();
void CL_Stop_f();
void CL_Record_f();

//
// cl_parse.c
//
extern char* svc_strings[256];

void CL_ParseServerMessage();
void CL_LoadClientinfo(clientinfo_t * ci, char* s);
void SHOWNET(char* s);
void CL_ParseClientinfo(int player);
void CL_Download_f();


//
// cl_view.c
//
extern int gun_frame;
extern Model* gun_model;

void V_Init();
void V_RenderView();
void V_AddEntity(entity_t * ent);
void V_AddParticle(vec3_t org, vec3_t length, vec3_t color, float alpha, int type, float size, int sFactor, int dFactor, int flags, int time, float orient, float len, vec3_t oldOrg, vec3_t dir);
void V_AddLight(vec3_t org, float intensity, float r, float g, float b, vec3_t ang, float cone, int filter);
void V_AddLightStyle(int style, float r, float g, float b);

//
// cl_tent.c
//
void CL_RegisterTEntModels();

//
// cl_pred.c
//
void CL_CheckPredictionError();

// cl_fx.c
cdlight_t *CL_AllocDlight(int key);
void CL_BigTeleportParticles(vec3_t org);
void CL_RocketTrail(vec3_t start, vec3_t end, centity_t * old);
void CL_DiminishingTrail(vec3_t start, vec3_t end, centity_t * old, int flags);
void CL_FlyEffect(centity_t * ent, vec3_t origin);
void CL_BfgParticles(entity_t * ent);
void CL_AddParticles();
void CL_EntityEvent(entity_state_t * ent);
void CL_TrapParticles(entity_t * ent);

// menus
void M_Init();
void M_Keydown(int key);
void M_Draw();
void M_Menu_Main_f();
void M_ForceMenuOff();
void M_AddToServerList(netadr_t adr, char* info);


// cl_pred.c
void CL_PredictMovement();

extern Image* i_conback;
extern Image* i_inventory;
extern Image* i_net;
extern Image* i_pause;
extern Image* i_loading;
extern Image* i_turtle;

extern Image* im_main_menu[5];
