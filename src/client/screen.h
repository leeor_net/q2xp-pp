/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
#pragma once

void SCR_BeginLoadingPlaque();
void SCR_CenterPrint(char* str);
void SCR_DrawCrosshair();
void SCR_DrawField(int x, int y, int color, int width, int value);
void SCR_EndLoadingPlaque();
void SCR_Init();
void SCR_UpdateScreen();
void SCR_TouchPics();
void SCR_RunConsole();

void SCR_DrawHUDString(float x, float y, float scale_x, float scale_y, int centerwidth, int xor, char* string);

vrect_t* SCR_ViewRectangle();

bool SCR_DrawingLoad();
