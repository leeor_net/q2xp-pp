/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// cl.input.c  -- builds an intended movement command to send to the server

#include "client.h"

ConsoleVariable *cl_nodelta;

unsigned int SystemFrameTime();

unsigned frame_msec;
unsigned old_sys_frame_time;

/*
===============================================================================

KEY BUTTONS

Continuous button event tracking is complicated by the fact that two different
input sources (say, mouse button 1 and the control key) can both press the
same button, but the button should only be released when both of the
pressing key have been released.

When a key event issues a button command (+forward, +attack, etc), it appends
its key number as a parameter to the command so it can be matched up with
the release.

state bit 0 is the current state of the key
state bit 1 is edge triggered on the up to down transition
state bit 2 is edge triggered on the down to up transition


Key_Event (int key, bool down, unsigned time);

+mlook src time

===============================================================================
*/


kbutton_t in_klook;
kbutton_t in_left, in_right, in_forward, in_back;
kbutton_t in_lookup, in_lookdown, in_moveleft, in_moveright, in_flashlight;
kbutton_t in_strafe, in_speed, in_use, in_attack;
kbutton_t in_up, in_down;
kbutton_t in_zoom;

int in_impulse;


void KeyDown(kbutton_t * b)
{
	int k;
	char* c;

	c = Cmd_Argv(1);
	if (c[0])
		k = atoi(c);
	else
		k = -1;					// typed manually at the console for
	// continuous down

	if (k == b->down[0] || k == b->down[1])
		return;					// repeating key

	if (!b->down[0])
		b->down[0] = k;
	else if (!b->down[1])
		b->down[1] = k;
	else {
		Com_Printf("Three keys down for a button!\n");
		return;
	}

	if (b->state & 1)
		return;					// still down

	// save timestamp
	c = Cmd_Argv(2);
	b->downtime = atoi(c);
	if (!b->downtime)
		b->downtime = SystemFrameTime() - 100;

	b->state |= 1 + 2;			// down + impulse down
}


void KeyUp(kbutton_t * b)
{
	int k;
	char* c;
	unsigned uptime;

	c = Cmd_Argv(1);
	if (c[0])
		k = atoi(c);
	else {						// typed manually at the console, assume
		// for unsticking, so clear all
		b->down[0] = b->down[1] = 0;
		b->state = 4;			// impulse up
		return;
	}

	if (b->down[0] == k)
		b->down[0] = 0;
	else if (b->down[1] == k)
		b->down[1] = 0;
	else
		return;					// key up without coresponding down (menu
	// pass through)
	if (b->down[0] || b->down[1])
		return;					// some other key is still holding it down

	if (!(b->state & 1))
		return;					// still up (this should not happen)

	// save timestamp
	c = Cmd_Argv(2);
	uptime = atoi(c);
	if (uptime)
		b->msec += uptime - b->downtime;
	else
		b->msec += 10;

	b->state &= ~1;				// now up
	b->state |= 4;				// impulse up
}


void IN_KLookDown()
{
	KeyDown(&in_klook);
}


void IN_KLookUp()
{
	KeyUp(&in_klook);
}


void IN_UpDown()
{
	KeyDown(&in_up);
}


void IN_UpUp()
{
	KeyUp(&in_up);
}


void IN_DownDown()
{
	KeyDown(&in_down);
}


void IN_DownUp () {
	KeyUp (&in_down);
}
void IN_LeftDown () {
	KeyDown (&in_left);
}
void IN_LeftUp () {
	KeyUp (&in_left);
}
void IN_RightDown () {
	KeyDown (&in_right);
}
void IN_RightUp () {
	KeyUp (&in_right);
}
void IN_ForwardDown () {
	KeyDown (&in_forward);
}
void IN_ForwardUp () {
	KeyUp (&in_forward);
}
void IN_BackDown () {
	KeyDown (&in_back);
}
void IN_BackUp () {
	KeyUp (&in_back);
}
void IN_LookupDown () {
	KeyDown (&in_lookup);
}
void IN_LookupUp () {
	KeyUp (&in_lookup);
}
void IN_LookdownDown () {
	KeyDown (&in_lookdown);
}
void IN_LookdownUp () {
	KeyUp (&in_lookdown);
}
void IN_MoveleftDown () {
	KeyDown (&in_moveleft);
}
void IN_MoveleftUp () {
	KeyUp (&in_moveleft);
}
void IN_MoverightDown () {
	KeyDown (&in_moveright);
}
void IN_MoverightUp () {
	KeyUp (&in_moveright);
}

void IN_SpeedDown () {
	KeyDown (&in_speed);
}
void IN_SpeedUp () {
	KeyUp (&in_speed);
}
void IN_StrafeDown () {
	KeyDown (&in_strafe);
}
void IN_StrafeUp () {
	KeyUp (&in_strafe);
}

void IN_AttackDown () {
	KeyDown (&in_attack);
}
void IN_AttackUp () {
	KeyUp (&in_attack);
}

void IN_UseDown () {
	KeyDown (&in_use);
}
void IN_UseUp () {
	KeyUp (&in_use);
}

void IN_Impulse () {
	in_impulse = atoi (Cmd_Argv (1));
}

void IN_ZoomUp () {
	KeyUp (&in_zoom);
}

void IN_ZoomDown () {
	KeyDown (&in_zoom);
}

void IN_FlashLightUp() {
	KeyUp(&in_flashlight);
}

void IN_FlashLightDown() {
	KeyDown(&in_flashlight);
}

/*
===============
CL_KeyState

Returns the fraction of the frame that the key was down
===============
*/
float CL_KeyState (kbutton_t * key) {
	float val;
	int msec;

	key->state &= 1;			// clear impulses

	msec = key->msec;
	key->msec = 0;

	if (key->state) {			// still down
		msec += SystemFrameTime() - key->downtime;
		key->downtime = SystemFrameTime();
	}
#if 0
	if (msec) {
		Com_Printf ("%i ", msec);
	}
#endif

	val = (float)msec / frame_msec;
	if (val < 0)
		val = 0;
	if (val > 1)
		val = 1;

	return val;
}




//==========================================================================

ConsoleVariable *cl_upspeed;
ConsoleVariable *cl_forwardspeed;
ConsoleVariable *cl_sidespeed;

ConsoleVariable *cl_yawspeed;
ConsoleVariable *cl_pitchspeed;

ConsoleVariable *cl_run;

ConsoleVariable *cl_anglespeedkey;


/*
================
CL_AdjustAngles

Moves the local angle positions
================
*/
void CL_AdjustAngles () {
	float speed;
	float up, down;

	if (in_speed.state & 1)
		speed = cls.frametime * cl_anglespeedkey->value;
	else
		speed = cls.frametime;

	if (!(in_strafe.state & 1)) {

#ifdef _WIN32
		cl.viewangles_YAW -= speed*cl_yawspeed->value * CL_KeyState(&in_right);
		cl.viewangles_YAW += speed*cl_yawspeed->value * CL_KeyState(&in_left);
#else
		cl.viewangles[YAW] -= speed * cl_yawspeed->value * CL_KeyState (&in_right);
		cl.viewangles[YAW] += speed * cl_yawspeed->value * CL_KeyState (&in_left);
#endif
	}
	if (in_klook.state & 1) {

#ifdef _WIN32
		cl.viewangles_PITCH -= speed*cl_pitchspeed->value * CL_KeyState(&in_forward);
		cl.viewangles_PITCH += speed*cl_pitchspeed->value * CL_KeyState(&in_back);
#else
		cl.viewangles[PITCH] -= speed * cl_pitchspeed->value * CL_KeyState (&in_forward);
		cl.viewangles[PITCH] += speed * cl_pitchspeed->value * CL_KeyState (&in_back);
#endif
	}

	up = CL_KeyState (&in_lookup);
	down = CL_KeyState (&in_lookdown);

#ifdef _WIN32
	cl.viewangles_PITCH -= speed*cl_pitchspeed->value * up;
	cl.viewangles_PITCH += speed*cl_pitchspeed->value * down;
#else
	cl.viewangles[PITCH] -= speed * cl_pitchspeed->value * up;
	cl.viewangles[PITCH] += speed * cl_pitchspeed->value * down;
#endif
}

/*
================
CL_BaseMove

Send the intended movement message to the server
================
*/
void CL_BaseMove (usercmd_t * cmd) {
	CL_AdjustAngles ();

	memset (cmd, 0, sizeof(*cmd));

#ifdef _WIN32
	cmd->angles[0] = (short)cl.viewangles_PITCH;
	cmd->angles[1] = (short)cl.viewangles_YAW;
	cmd->angles[2] = 0;
#else
	VectorCopy (cl.viewangles, cmd->angles);
#endif

	if (in_strafe.state & 1) {
		cmd->sidemove += cl_sidespeed->value * CL_KeyState (&in_right);
		cmd->sidemove -= cl_sidespeed->value * CL_KeyState (&in_left);
	}

	cmd->sidemove += cl_sidespeed->value * CL_KeyState (&in_moveright);
	cmd->sidemove -= cl_sidespeed->value * CL_KeyState (&in_moveleft);

	cmd->upmove += cl_upspeed->value * CL_KeyState (&in_up);
	cmd->upmove -= cl_upspeed->value * CL_KeyState (&in_down);

	if (!(in_klook.state & 1)) {
		cmd->forwardmove +=
			cl_forwardspeed->value * CL_KeyState (&in_forward);
		cmd->forwardmove -= cl_forwardspeed->value * CL_KeyState (&in_back);
	}
	//
	// adjust for speed key / running
	//
	if ((in_speed.state & 1) ^ (int)(cl_run->value)) {
		cmd->forwardmove *= 2;
		cmd->sidemove *= 2;
		cmd->upmove *= 2;
	}
}

void CL_ClampPitch()
{
	float pitch;

	pitch = SHORT2ANGLE(cl.frame.playerstate.pmove.delta_angles[PITCH]);

	if (pitch > 180)
		pitch -= 360;

	if (cl.viewangles_PITCH + pitch < -360)
		cl.viewangles_PITCH += 360; // wrapped
	if (cl.viewangles_PITCH + pitch > 360)
		cl.viewangles_PITCH -= 360; // wrapped

	if (cl.viewangles_PITCH + pitch > 89)
		cl.viewangles_PITCH = 89 - pitch;
	if (cl.viewangles_PITCH + pitch < -89)
		cl.viewangles_PITCH = -89 - pitch;
}


/*
==============
CL_FinishMove
==============
*/
void CL_FinishMove(usercmd_t * cmd)
{
	int ms;

	// figure button bits
	if (in_flashlight.state & 3)
		cmd->buttons |= BUTTON_FLASHLIGHT;

	in_flashlight.state &= ~2;

	if (in_attack.state & 3)
		cmd->buttons |= BUTTON_ATTACK;
	in_attack.state &= ~2;

	if (in_use.state & 3)
		cmd->buttons |= BUTTON_USE;
	in_use.state &= ~2;

	if (anykeydown && cls.key_dest == KEY_GAME)
		cmd->buttons |= BUTTON_ANY;

	// send milliseconds of time to apply the move
	ms = (int)(cls.frametime * 1000);
	if (ms > 250)
		ms = 100;				// time was unreasonable
	cmd->msec = ms;

	CL_ClampPitch();

#ifdef _WIN32
	cmd->angles[0] = ANGLE2SHORT(cl.viewangles_PITCH);
	cmd->angles[1] = ANGLE2SHORT(cl.viewangles_YAW);
	cmd->angles[2] = 0;
#else
	{
		int i;
		for (i = 0; i < 3; i++)
			cmd->angles[i] = ANGLE2SHORT(cl.viewangles[i]);
	}
#endif

	cmd->impulse = in_impulse;
	in_impulse = 0;

	// send the ambient light level at the player's current position
	cmd->lightlevel = (byte)cl_lightlevel->value;
}

/*
=================
CL_CreateCmd
=================
*/

usercmd_t CL_CreateCmd () {
	usercmd_t cmd;

	frame_msec = SystemFrameTime() - old_sys_frame_time;
	if (frame_msec < 1)
		frame_msec = 1;
	if (frame_msec > 200)
		frame_msec = 200;

	// get basic movement from keyboard
	CL_BaseMove (&cmd);

	// allow mice or other external controllers to add to the move
	IN_Move (&cmd);

	CL_FinishMove (&cmd);

	old_sys_frame_time = SystemFrameTime();

	//cmd.impulse = cls.framecount;

	return cmd;
}

void IN_CenterView()
{

#ifdef _WIN32
	cl.viewangles_PITCH = -SHORT2ANGLE(cl.frame.playerstate.pmove.delta_angles[PITCH]);
#else
	cl.viewangles[PITCH] = -SHORT2ANGLE(cl.frame.playerstate.pmove.delta_angles[PITCH]);
#endif
}

/*
============
CL_InitInput
============
*/
void CL_InitInput()
{
	Cmd_AddCommand("centerview", IN_CenterView);

	Cmd_AddCommand("+moveup", IN_UpDown);
	Cmd_AddCommand("-moveup", IN_UpUp);
	Cmd_AddCommand("+movedown", IN_DownDown);
	Cmd_AddCommand("-movedown", IN_DownUp);
	Cmd_AddCommand("+left", IN_LeftDown);
	Cmd_AddCommand("-left", IN_LeftUp);
	Cmd_AddCommand("+right", IN_RightDown);
	Cmd_AddCommand("-right", IN_RightUp);
	Cmd_AddCommand("+forward", IN_ForwardDown);
	Cmd_AddCommand("-forward", IN_ForwardUp);
	Cmd_AddCommand("+back", IN_BackDown);
	Cmd_AddCommand("-back", IN_BackUp);
	Cmd_AddCommand("+lookup", IN_LookupDown);
	Cmd_AddCommand("-lookup", IN_LookupUp);
	Cmd_AddCommand("+lookdown", IN_LookdownDown);
	Cmd_AddCommand("-lookdown", IN_LookdownUp);
	Cmd_AddCommand("+strafe", IN_StrafeDown);
	Cmd_AddCommand("-strafe", IN_StrafeUp);
	Cmd_AddCommand("+moveleft", IN_MoveleftDown);
	Cmd_AddCommand("-moveleft", IN_MoveleftUp);
	Cmd_AddCommand("+moveright", IN_MoverightDown);
	Cmd_AddCommand("-moveright", IN_MoverightUp);
	Cmd_AddCommand("+speed", IN_SpeedDown);
	Cmd_AddCommand("-speed", IN_SpeedUp);
	Cmd_AddCommand("+attack", IN_AttackDown);
	Cmd_AddCommand("-attack", IN_AttackUp);
	Cmd_AddCommand("+use", IN_UseDown);
	Cmd_AddCommand("-use", IN_UseUp);
	Cmd_AddCommand("impulse", IN_Impulse);
	Cmd_AddCommand("+klook", IN_KLookDown);
	Cmd_AddCommand("-klook", IN_KLookUp);

	Cmd_AddCommand("+zoom", IN_ZoomDown);
	Cmd_AddCommand("-zoom", IN_ZoomUp);

	Cmd_AddCommand("+flashlight", IN_FlashLightDown);
	Cmd_AddCommand("-flashlight", IN_FlashLightUp);

	cl_nodelta = Cvar_Get("cl_nodelta", "0", 0);
}



/*
=================
CL_SendCmd
=================
*/
void CL_SendCmd () {
	sizebuf_t buf;
	byte data[128];
	int i;
	usercmd_t *cmd, *oldcmd;
	usercmd_t nullcmd;
	int checksumIndex;

	buf.data = nullptr;			// shut up compiler

	// build a command even if not connected

	// save this command off for prediction
	i = cls.netchan.outgoing_sequence & (CMD_BACKUP - 1);
	cmd = &cl.cmds[i];
	cl.cmd_time[i] = cls.realtime;	// for netgraph ping calculation

	*cmd = CL_CreateCmd ();

	cl.cmd = *cmd;

	if (cls.state == ca_disconnected || cls.state == ca_connecting)
		return;

	if (cls.state == ca_connected) {
		if (cls.netchan.message.cursize
			|| curtime - cls.netchan.last_sent > 1000)
			Netchan_Transmit (&cls.netchan, 0, buf.data);
		return;
	}
	// send a userinfo update if needed
	if (userInfoModified())
	{
		CL_FixUpGender();
		userInfoModified(false);
		MSG_WriteByte (&cls.netchan.message, clc_userinfo);
		MSG_WriteString (&cls.netchan.message, Cvar_Userinfo ());
	}

	SZ_Init (&buf, data, sizeof(data));

	/*
	if (cmd->buttons && cl.cinematictime > 0 && !cl.attractloop && cls.realtime - cl.cinematictime > 1000)
		SCR_FinishCinematic ();
	*/

	// begin a client move command
	MSG_WriteByte (&buf, clc_move);

	// save the position for a checksum byte
	checksumIndex = buf.cursize;
	MSG_WriteByte (&buf, 0);

	// let the server know what the last frame we
	// got was, so the next message can be delta compressed
	if (cl_nodelta->value || !cl.frame.valid || cls.demowaiting)
		MSG_WriteLong (&buf, -1);	// no compression
	else
		MSG_WriteLong (&buf, cl.frame.serverframe);

	// send this and the previous cmds in the message, so
	// if the last packet was dropped, it can be recovered
	i = (cls.netchan.outgoing_sequence - 2) & (CMD_BACKUP - 1);
	cmd = &cl.cmds[i];
	memset (&nullcmd, 0, sizeof(nullcmd));
	MSG_WriteDeltaUsercmd (&buf, &nullcmd, cmd);
	oldcmd = cmd;

	i = (cls.netchan.outgoing_sequence - 1) & (CMD_BACKUP - 1);
	cmd = &cl.cmds[i];
	MSG_WriteDeltaUsercmd (&buf, oldcmd, cmd);
	oldcmd = cmd;

	i = (cls.netchan.outgoing_sequence) & (CMD_BACKUP - 1);
	cmd = &cl.cmds[i];
	MSG_WriteDeltaUsercmd (&buf, oldcmd, cmd);

	// calculate a checksum over the move commands
	buf.data[checksumIndex] =
		COM_BlockSequenceCRCByte (buf.data + checksumIndex + 1,
		buf.cursize - checksumIndex - 1,
		cls.netchan.outgoing_sequence);

	// 
	// deliver the message
	// 

	Netchan_Transmit (&cls.netchan, buf.cursize, buf.data);
}
