/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// cl_main.c  -- client main loop

#include "client.h"
#include "music.h"

#include "../common/filesystem.h"
#include "../common/string_util.h"

#include "../ref_gl/r_model_md2.h"
#include "../win32/win_network.h"

#include "cl_parse.h"

#include "../common/cvar.h"

#include <iostream>

#ifndef _WIN32
#include <unistd.h>
#endif

ConsoleVariable *freelook;

ConsoleVariable *adr0;
ConsoleVariable *adr1;
ConsoleVariable *adr2;
ConsoleVariable *adr3;
ConsoleVariable *adr4;
ConsoleVariable *adr5;
ConsoleVariable *adr6;
ConsoleVariable *adr7;
ConsoleVariable *adr8;


ConsoleVariable *rcon_client_password;
ConsoleVariable *rcon_address;

ConsoleVariable *cl_noskins;
ConsoleVariable *cl_autoskins;
ConsoleVariable *cl_footsteps;
ConsoleVariable *cl_timeout;
ConsoleVariable *cl_predict;
ConsoleVariable *cl_maxfps;
ConsoleVariable *cl_gun;

ConsoleVariable *cl_add_particles;
ConsoleVariable *cl_add_lights;
ConsoleVariable *cl_add_entities;
ConsoleVariable *cl_add_blend;

ConsoleVariable *cl_shownet;
ConsoleVariable *cl_showmiss;
ConsoleVariable *cl_showclamp;

ConsoleVariable *cl_paused;
ConsoleVariable *cl_timedemo;

ConsoleVariable *lookspring;
ConsoleVariable *lookstrafe;
ConsoleVariable *sensitivity;

ConsoleVariable *m_pitch;
ConsoleVariable *m_yaw;
ConsoleVariable *m_forward;
ConsoleVariable *m_side;

ConsoleVariable *cl_lightlevel;

//
// userinfo
//
ConsoleVariable *info_password;
ConsoleVariable *info_spectator;
ConsoleVariable *name;
ConsoleVariable *skin;
ConsoleVariable *rate;
ConsoleVariable *fov;
ConsoleVariable *zoomfov;
ConsoleVariable *msg;
ConsoleVariable *hand;
ConsoleVariable *gender;
ConsoleVariable *gender_auto;

ConsoleVariable *cl_vwep;
ConsoleVariable *dmflags;

//q2xp stuff
ConsoleVariable *cl_drawTime;
ConsoleVariable *cl_drawFPS;
ConsoleVariable *cl_brass;
ConsoleVariable *cl_railcore_red;
ConsoleVariable *cl_railcore_green;
ConsoleVariable *cl_railcore_blue;
ConsoleVariable *cl_railspiral_red;
ConsoleVariable *cl_railspiral_green;
ConsoleVariable *cl_railspiral_blue;
ConsoleVariable *cl_decals;
ConsoleVariable *cl_drawhud;
ConsoleVariable *cl_thirdPerson;
ConsoleVariable *cl_thirdPersonAngle;
ConsoleVariable *cl_thirdPersonRange;
ConsoleVariable *cl_blood;
extern ConsoleVariable *deathmatch;
ConsoleVariable *cl_fontScale;
ConsoleVariable *cl_itemsBobbing;

client_static_t cls;
client_state_t cl;


centity_t cl_entities[MAX_EDICTS];

entity_state_t cl_parse_entities[MAX_PARSE_ENTITIES];

extern ConsoleVariable *allow_download;
extern ConsoleVariable *allow_download_players;
extern ConsoleVariable *allow_download_models;
extern ConsoleVariable *allow_download_sounds;
extern ConsoleVariable *allow_download_maps;


/// Modified by Berserker: ignore
int CL_PMpointcontents2 (vec3_t point, Model*  ignore) {
	int i;
	entity_state_t *ent;
	int num;
	cmodel_t *cmodel;
	int contents;

	contents = CM_PointContents (point, 0);

	for (i = 0; i < cl.frame.num_entities; i++) {
		num = (cl.frame.parse_entities + i) & (MAX_PARSE_ENTITIES - 1);
		ent = &cl_parse_entities[num];

		if (ent->solid != 31)	// special value for bmodel
			continue;

		if (cl.model_draw[ent->modelindex] == ignore)
			continue;

		cmodel = cl.model_clip[ent->modelindex];
		if (!cmodel)
			continue;

		contents |=
			CM_TransformedPointContents (point, cmodel->headnode,
			ent->origin, ent->angles);
	}

	return contents;
}



void CL_Toggle_f () {
	ConsoleVariable *var;

	if (Cmd_Argc () != 2) {
		Com_Printf ("ConsoleVariableoggle <cvar>\n");
		return;
	}

	var = Cvar_FindVar (Cmd_Argv (1));
	if (!var) {
		Com_Printf ("Unknown cvar '%s'\n", Cmd_Argv (1));
		return;
	}

	if (var->value)
		Cvar_SetValue (Cmd_Argv (1), 0);
	else
		Cvar_SetValue (Cmd_Argv (1), 1);
}

void CL_Increase_f () {
	ConsoleVariable *var;
	float val;

	if (Cmd_Argc () != 3) {
		Com_Printf ("cvar_inc <cvar> <value>\n");
		return;
	}

	var = Cvar_FindVar (Cmd_Argv (1));
	if (!var) {
		Com_Printf ("Unknown cvar '%s'\n", Cmd_Argv (1));
		return;
	}

	val = atof (Cmd_Argv (2));
	Cvar_SetValue (Cmd_Argv (1), var->value + val);
}



/**
 * Dumps the current net message, prefixed by the length
*/
void CL_WriteDemoMessage()
{
	// the first eight bytes are just packet sequencing stuff
	int len = Net_Message()->cursize - 8;
	int swlen = LittleLong(len);
	FS_Write(cls.demofile, &swlen, 4, 1);
	FS_Write(cls.demofile, Net_Message()->data + 8, len, 1);
}


/*
====================
CL_Stop_f

stop recording a demo
====================
*/
void CL_Stop_f () {
	int len;

	if (!cls.demorecording) {
		Com_Printf ("Not recording a demo.\n");
		return;
	}
	// finish up
	len = -1;
	FS_Write(cls.demofile, &len, 4, 1);
	FS_Close(cls.demofile);
	cls.demofile = nullptr;
	cls.demorecording = false;
	Com_Printf ("Stopped demo.\n");
}

/*
====================
CL_Record_f

record <demoname>

Begins recording a demo from the current position
====================
*/
void CL_Record_f() {
	char name[MAX_OSPATH];
	char buf_data[MAX_MSGLEN];
	sizebuf_t buf;
	int i;
	int len;
	entity_state_t *ent;
	entity_state_t nullstate;

	if (Cmd_Argc() != 2) {
		Com_Printf("record <demoname>\n");
		return;
	}

	if (cls.demorecording)
	{
		Com_Printf("Already recording.\n");
		return;
	}

	if (cls.state != ca_active)
	{
		Com_Printf("You must be in a level to record.\n");
		return;
	}

	// open the demo file
	Q_sprintf(name, sizeof(name), "demos/%s.dm2", Cmd_Argv(1));

	Com_Printf("recording to %s.\n", name);
	FS_MakeDirectory(name);
	cls.demofile = FS_Open(name, FS_OPEN_WRITE);
	if (!cls.demofile)
	{
		Com_Printf("ERROR: couldn't open.\n");
		return;
	}

	cls.demorecording = true;

	// don't start saving messages until a non-delta compressed message is received
	cls.demowaiting = true;

	// write out messages to hold the startup information
	SZ_Init(&buf, reinterpret_cast<byte*>(buf_data), sizeof(buf_data));

	// send the serverdata
	MSG_WriteByte(&buf, svc_serverdata);
	MSG_WriteLong(&buf, PROTOCOL_VERSION);
	MSG_WriteLong(&buf, 0x10000 + cl.servercount);
	MSG_WriteByte(&buf, 1);		// demos are always attract loops
	MSG_WriteString(&buf, cl.gamedir);
	MSG_WriteShort(&buf, cl.playernum);

	MSG_WriteString(&buf, cl.configstrings[CS_NAME]);

	// configstrings
	for (i = 0; i < MAX_CONFIGSTRINGS; i++)
	{
		if (cl.configstrings[i][0])
		{
			if (buf.cursize + strlen(cl.configstrings[i]) + 32 > buf.maxsize)
			{	// write it out
				len = LittleLong(buf.cursize);
				FS_Write(cls.demofile, &len, 4, 1);
				FS_Write(cls.demofile, buf.data, buf.cursize, 1);
				buf.cursize = 0;
			}

			MSG_WriteByte(&buf, svc_configstring);
			MSG_WriteShort(&buf, i);
			MSG_WriteString(&buf, cl.configstrings[i]);
		}

	}

	// baselines
	memset(&nullstate, 0, sizeof(nullstate));
	for (i = 0; i < MAX_EDICTS; i++)
	{
		ent = &cl_entities[i].baseline;
		if (!ent->modelindex)
			continue;

		if (buf.cursize + 64 > buf.maxsize) {	// write it out
			len = LittleLong(buf.cursize);
			FS_Write(cls.demofile, &len, 4, 1);
			FS_Write(cls.demofile, buf.data, buf.cursize, 1);
			buf.cursize = 0;
		}

		MSG_WriteByte(&buf, svc_spawnbaseline);
		MSG_WriteDeltaEntity(&nullstate, &cl_entities[i].baseline, &buf, true, true);
	}

	MSG_WriteByte(&buf, svc_stufftext);
	MSG_WriteString(&buf, "precache\n");

	// write it to the demo file
	len = LittleLong(buf.cursize);
	FS_Write(cls.demofile, &len, 4, 1);
	FS_Write(cls.demofile, buf.data, buf.cursize, 1);

	// the rest of the demo file will be individual frames
}

//======================================================================

/*
===================
Cmd_ForwardToServer

adds the current command line as a clc_stringcmd to the client message.
things like godmode, noclip, etc, are commands directed to the server,
so when they are typed in at the console, they will need to be forwarded.
===================
*/
void Cmd_ForwardToServer () {
	char* cmd;

	cmd = Cmd_Argv (0);
	if (cls.state <= ca_connected || *cmd == '-' || *cmd == '+') {
		Com_Printf ("Unknown command \"%s\"\n", cmd);
		return;
	}

	MSG_WriteByte (&cls.netchan.message, clc_stringcmd);
	SZ_Print (&cls.netchan.message, cmd);
	if (Cmd_Argc () > 1) {
		SZ_Print (&cls.netchan.message, " ");
		SZ_Print (&cls.netchan.message, Cmd_Args ());
	}
}

void CL_Setenv_f () {
	int argc = Cmd_Argc ();

	if (argc > 2) {
		char buffer[1000];
		int i;

		strcpy (buffer, Cmd_Argv (1));
		strcat (buffer, "=");

		for (i = 2; i < argc; i++) {
			strcat (buffer, Cmd_Argv (i));
			strcat (buffer, " ");
		}

		putenv (buffer);
	}
	else if (argc == 2) {
		char* env = getenv (Cmd_Argv (1));

		if (env) {
			Com_Printf ("%s=%s\n", Cmd_Argv (1), env);
		}
		else {
			Com_Printf ("%s undefined\n", Cmd_Argv (1), env);
		}
	}
}


/*
==================
CL_ForwardToServer_f
==================
*/
void CL_ForwardToServer_f () {
	if (cls.state != ca_connected && cls.state != ca_active) {
		Com_Printf ("Can't \"%s\", not connected\n", Cmd_Argv (0));
		return;
	}
	// don't forward the first argument
	if (Cmd_Argc () > 1) {
		MSG_WriteByte (&cls.netchan.message, clc_stringcmd);
		SZ_Print (&cls.netchan.message, Cmd_Args ());
	}
}


/*
==================
CL_Pause_f
==================
*/
void CL_Pause_f () {
	// never pause in multiplayer
	if (Cvar_VariableValue ("maxclients") > 1 || !Com_ServerState ()) {
		Cvar_SetValue ("paused", 0);
		return;
	}

	Cvar_SetValue ("paused", !cl_paused->value);
}

/*
==================
CL_Quit_f
==================
*/
void CL_Quit_f () {
	CL_Disconnect ();
	Com_Quit ();
}

/*
================
CL_Drop

Called after an ERR_DROP was thrown
================
*/
void CL_Drop () {
	if (cls.state == ca_uninitialized)
		return;
	if (cls.state == ca_disconnected)
		return;

	CL_Disconnect ();

	// drop loading plaque unless this is the initial game start
	if (cls.disable_servercount != -1)
		SCR_EndLoadingPlaque ();	// get rid of loading plaque
}


/*
=======================
CL_SendConnectPacket

We have gotten a challenge from the server, so try and
connect.
======================
*/
void CL_SendConnectPacket()
{
	netadr_t adr;
	int port;

	if (!NET_StringToAdr(cls.servername, &adr))
	{
		Com_Printf("Bad server address\n");
		cls.connect_time = 0;
		return;
	}
	if (adr.port == 0)
		adr.port = BigShort(PORT_SERVER);

	port = Cvar_VariableValue("qport");
	userInfoModified(false);

	Netchan_OutOfBandPrint(NS_CLIENT, adr, "connect %i %i %i \"%s\"\n", PROTOCOL_VERSION, port, cls.challenge, Cvar_Userinfo());
}

/*
=================
CL_CheckForResend

Resend a connect message if the last one has timed out
=================
*/
void CL_CheckForResend () {
	netadr_t adr;

	// if the local server is running and we aren't
	// then connect
	if (cls.state == ca_disconnected && Com_ServerState ()) {
		cls.state = ca_connecting;
		strncpy (cls.servername, "localhost", sizeof(cls.servername) - 1);
		// we don't need a challenge on the localhost
		CL_SendConnectPacket ();
		return;
		//      cls.connect_time = -99999;  // CL_CheckForResend() will fire immediately
	}
	// resend if we haven't gotten a reply yet
	if (cls.state != ca_connecting)
		return;

	if (cls.realtime - cls.connect_time < 3000)
		return;

	CL_ClearDecals ();
	CL_ClearParticles ();

	if (!NET_StringToAdr (cls.servername, &adr)) {
		Com_Printf ("Bad server address\n");
		cls.state = ca_disconnected;
		return;
	}
	if (adr.port == 0)
		adr.port = BigShort (PORT_SERVER);

	cls.connect_time = cls.realtime;	// for retransmit requests

	Com_Printf ("Connecting to %s...\n", cls.servername);

	Netchan_OutOfBandPrint (NS_CLIENT, adr, "getchallenge\n");
}


/*
================
CL_Connect_f

================
*/
void CL_Connect_f () {
	char* server;

	if (Cmd_Argc () != 2) {
		Com_Printf ("usage: connect <server>\n");
		return;
	}

	if (Com_ServerState ()) {	// if running a local server, kill it and
		// reissue
		SV_Shutdown (va ("Server quit\n", msg), false);
	}
	else {
		CL_Disconnect ();
	}

	server = Cmd_Argv (1);

	NET_Config (true);			// allow remote

	CL_Disconnect ();

	cls.state = ca_connecting;
	strncpy (cls.servername, server, sizeof(cls.servername) - 1);
	cls.connect_time = -99999;	// CL_CheckForResend() will fire
	// immediately
}


/*
=====================
CL_Rcon_f

Send the rest of the command line over as
an unconnected command.
=====================
*/
void CL_Rcon_f() {
	char message[1024];
	int i;
	netadr_t to;

	if (rcon_client_password->string.empty())
	{
		Com_Printf("You must set 'rcon_password' before\nissuing an rcon command.\n");
		return;
	}

	message[0] = (char)255;
	message[1] = (char)255;
	message[2] = (char)255;
	message[3] = (char)255;
	message[4] = 0;

	NET_Config(true);			// allow remote

	strcat(message, "rcon ");

	strcat(message, rcon_client_password->string.c_str());
	strcat(message, " ");

	for (i = 1; i < Cmd_Argc(); i++) {
		strcat(message, Cmd_Argv(i));
		strcat(message, " ");
	}

	if (cls.state >= ca_connected)
		to = cls.netchan.remote_address;
	else {
		if (rcon_address->string.empty())
		{
			Com_Printf("You must either be connected,\nor set the 'rcon_address' cvar\to issue rcon commands\n");
			return;
		}
		NET_StringToAdr((char*)rcon_address->string.c_str(), &to); /// \fixme	Yuck
		if (to.port == 0)
		{
			to.port = BigShort(PORT_SERVER);
		}
	}

	NET_SendPacket(NS_CLIENT, strlen(message) + 1, message, to);
}


/*
=====================
CL_ClearState

=====================
*/
void CL_ClearState () {
	S_StopAllSounds ();
	CL_ClearEffects ();
	CL_ClearTEnts ();

	// wipe the entire cl structure
	memset (&cl, 0, sizeof(cl));
	memset (&cl_entities, 0, sizeof(cl_entities));

	SZ_Clear (&cls.netchan.message);

}

/*
=====================
CL_Disconnect

Goes from a connected state to full screen console state
Sends a disconnect message to the server
This is also called on Com_Error, so it shouldn't cause any errors
=====================
*/

#include "../ref_gl/r_model.h"
extern Model* currentPlayerWeapon;

void CL_Disconnect () {
	byte final[32];

	if (cls.state == ca_disconnected)
		return;

	if (cl_timedemo && cl_timedemo->value) {
		int time;

		time = Sys_Milliseconds () - cl.timedemo_start;
		if (time > 0)
			Com_Printf ("%i frames, %3.1f seconds: %3.1f fps\n",
			cl.timedemo_frames, time / 1000.0,
			cl.timedemo_frames * 1000.0 / time);
	}

	VectorClear (cl.refdef.blend);

	M_ForceMenuOff ();

	cls.connect_time = 0;

	if (cls.demorecording)
		CL_Stop_f ();

	// send a disconnect message to the server
	final[0] = clc_stringcmd;
	strcpy ((char* )final + 1, "disconnect");
	Netchan_Transmit (&cls.netchan, strlen ((const char*)final), final);
	Netchan_Transmit (&cls.netchan, strlen ((const char*)final), final);
	Netchan_Transmit (&cls.netchan, strlen ((const char*)final), final);

	CL_ClearState ();

	// stop download
	if (cls.download) {
		FS_Close (cls.download);
		cls.download = nullptr;
	}

	cls.state = ca_disconnected;
	currentPlayerWeapon = nullptr;
}


void CL_Disconnect_f () {
	Com_Error (ERR_DROP, "Disconnected from server");
}


/*
====================
CL_Packet_f

packet <destination> <contents>

Contents allows \n escape character
====================
*/
void CL_Packet_f () {
	char send[2048];
	int i, l;
	char* in, *out;
	netadr_t adr;

	if (Cmd_Argc () != 3) {
		Com_Printf ("packet <destination> <contents>\n");
		return;
	}

	NET_Config (true);			// allow remote

	if (!NET_StringToAdr (Cmd_Argv (1), &adr)) {
		Com_Printf ("Bad address\n");
		return;
	}
	if (!adr.port)
		adr.port = BigShort (PORT_SERVER);

	in = Cmd_Argv (2);
	out = send + 4;
	send[0] = send[1] = send[2] = send[3] = (char)0xff;

	l = strlen (in);
	for (i = 0; i < l; i++) {
		if (in[i] == '\\' && in[i + 1] == 'n') {
			*out++ = '\n';
			i++;
		}
		else
			*out++ = in[i];
	}
	*out = 0;

	NET_SendPacket (NS_CLIENT, out - send, send, adr);
}

/*
=================
CL_Changing_f

Just sent as a hint to the client that they should
drop to full console
=================
*/
void CL_Changing_f () {
	// ZOID
	// if we are downloading, we don't change! This so we don't suddenly
	// stop downloading a map
	if (cls.download)
		return;

	SCR_BeginLoadingPlaque ();
	cls.state = ca_connected;	// not active anymore, but not
	// disconnected
	Com_Printf ("\nChanging map...\n");
}


/*
=================
CL_Reconnect_f

The server is changing levels
=================
*/
void CL_Reconnect_f () {
	// ZOID
	// if we are downloading, we don't change! This so we don't suddenly
	// stop downloading a map
	if (cls.download)
		return;

	S_StopAllSounds ();
	if (cls.state == ca_connected) {
		Com_Printf ("reconnecting...\n");
		cls.state = ca_connected;
		MSG_WriteChar (&cls.netchan.message, clc_stringcmd);
		MSG_WriteString (&cls.netchan.message, "new");
		return;
	}

	if (*cls.servername) {
		if (cls.state >= ca_connected) {
			CL_Disconnect ();
			cls.connect_time = cls.realtime - 1500;
		}
		else
			cls.connect_time = -99999;	// fire immediately

		cls.state = ca_connecting;
		Com_Printf ("reconnecting...\n");
	}
}

/*
=================
CL_ParseStatusMessage

Handle a reply from a ping
=================
*/
void CL_ParseStatusMessage () {
	char* s;

	s = MSG_ReadString (Net_Message());

	Com_Printf ("%s\n", s);
	M_AddToServerList (*Net_From(), s);
}


/*
=================
CL_PingServers_f
=================
*/
void CL_PingServers_f()
{
	netadr_t adr;
	char name[32];
	ConsoleVariable *noudp;

	NET_Config(true); // allow remote

	// send a broadcast packet
	Com_Printf("pinging broadcast...\n");

	noudp = Cvar_Get("noudp", "0", CVAR_NOSET);
	if (!noudp->value)
	{
		adr.type = NA_BROADCAST;
		adr.port = BigShort(PORT_SERVER);
		Netchan_OutOfBandPrint(NS_CLIENT, adr, va("info %i", PROTOCOL_VERSION));
	}

	// send a packet to each address book entry
	for (int i = 0; i < 9; i++)
	{
		Q_sprintf(name, sizeof(name), "adr%i", i);
		std::string address_string = Cvar_VariableString(name);

		if (address_string.empty()) { continue; }

		Com_Printf("pinging %s...\n", address_string.c_str());
		if (!NET_StringToAdr((char*)address_string.c_str(), &adr)) /// \fixme	Yuck
		{
			Com_Printf("Bad address: %s\n", address_string.c_str());
			continue;
		}
		if (!adr.port)
		{
			adr.port = BigShort(PORT_SERVER);
		}

		Netchan_OutOfBandPrint(NS_CLIENT, adr, va("info %i", PROTOCOL_VERSION));
	}
}


/*
=================
CL_Skins_f

Load or download any custom player skins and models
=================
*/
void CL_Skins_f () {
	int i;

	for (i = 0; i < MAX_CLIENTS; i++) {
		if (!cl.configstrings[CS_PLAYERSKINS + i][0])
			continue;
		Com_Printf ("client %i: %s\n", i,
			cl.configstrings[CS_PLAYERSKINS + i]);
		SCR_UpdateScreen ();
		Sys_SendKeyEvents ();	// pump message loop
		CL_ParseClientinfo (i);
	}
}


/*
=================
CL_ConnectionlessPacket

Responses to broadcasts, etc
=================
*/
void CL_ConnectionlessPacket () {
	char* s;
	char* c;

	MSG_BeginReading (Net_Message());
	MSG_ReadLong (Net_Message());	// skip the -1

	s = MSG_ReadStringLine (Net_Message());

	Cmd_TokenizeString (s, false);

	c = Cmd_Argv (0);

	Com_Printf ("%s: %s\n", NET_AdrToString (*Net_From()), c);

	// server connection
	if (!strcmp (c, "client_connect")) {
		if (cls.state == ca_connected) {
			Com_Printf ("Dup connect received.  Ignored.\n");
			return;
		}
		Netchan_Setup (NS_CLIENT, &cls.netchan, *Net_From(), cls.quakePort);
		MSG_WriteChar (&cls.netchan.message, clc_stringcmd);
		MSG_WriteString (&cls.netchan.message, "new");
		cls.state = ca_connected;
		return;
	}
	// server responding to a status broadcast
	if (!strcmp (c, "info")) {
		CL_ParseStatusMessage ();
		return;
	}
	// remote command from gui front end
	if (!strcmp (c, "cmd")) {
		if (!NET_IsLocalAddress (*Net_From())) {
			Com_Printf ("Command packet from remote host.  Ignored.\n");
			return;
		}
		Sys_AppActivate ();
		s = MSG_ReadString (Net_Message());
		Cbuf_AddText (s);
		Cbuf_AddText ("\n");
		return;
	}
	// print command from somewhere
	if (!strcmp (c, "print")) {
		s = MSG_ReadString (Net_Message());
		Com_Printf ("%s", s);
		return;
	}
	// ping from somewhere
	if (!strcmp (c, "ping")) {
		Netchan_OutOfBandPrint (NS_CLIENT, *Net_From(), "ack");
		return;
	}
	// challenge from the server we are connecting to
	if (!strcmp (c, "challenge")) {
		cls.challenge = atoi (Cmd_Argv (1));
		CL_SendConnectPacket ();
		return;
	}
	// echo request from server
	if (!strcmp (c, "echo")) {
		Netchan_OutOfBandPrint (NS_CLIENT, *Net_From(), "%s", Cmd_Argv (1));
		return;
	}

	Com_Printf ("Unknown command.\n");
}


/*
=================
CL_DumpPackets

A vain attempt to help bad TCP stacks that cause problems
when they overflow
=================
*/
void CL_DumpPackets()
{
	while (NET_GetPacket(NS_CLIENT, Net_From(), Net_Message()))
		Com_Printf("dumping a packet\n");
}


/*
=================
CL_ReadPackets
=================
*/
void CL_ReadPackets () {
	while (NET_GetPacket (NS_CLIENT, Net_From(), Net_Message())) {
		//  Com_Printf ("packet\n");
		//
		// remote command packet
		//
		if (*(int *)Net_Message()->data == -1) {
			CL_ConnectionlessPacket ();
			continue;
		}

		if (cls.state == ca_disconnected || cls.state == ca_connecting)
			continue;			// dump it if not connected

		if (Net_Message()->cursize < 8) {
			Com_Printf ("%s: Runt packet\n", NET_AdrToString (*Net_From()));
			continue;
		}
		//
		// packet from server
		//
		if (!NET_CompareAdr (*Net_From(), cls.netchan.remote_address))
		{
			Com_DPrintf ("%s:sequenced packet without connection\n", NET_AdrToString (*Net_From()));
			continue;
		}
		if (!Netchan_Process (&cls.netchan, Net_Message()))
			continue;			// wasn't accepted for some reason
		CL_ParseServerMessage ();
	}

	//
	// check timeout
	//
	if (cls.state >= ca_connected
		&& cls.realtime - cls.netchan.last_received >
		cl_timeout->value * 1000) {
		if (++cl.timeoutcount > 5)	// timeoutcount saves debugger
		{
			Com_Printf ("\nServer connection timed out.\n");
			CL_Disconnect ();
			return;
		}
	}
	else
		cl.timeoutcount = 0;

}


//=============================================================================

/*
==============
CL_FixUpGender_f
==============
*/
void CL_FixUpGender()
{
	char* p;
	char sk[80];

	if (gender_auto->value)
	{
		if (gender->modified)
		{
			// was set directly, don't override the user
			gender->modified = false;
			return;
		}

		strncpy(sk, skin->string.c_str(), sizeof(sk) - 1);
		if ((p = strchr(sk, '/')) != nullptr)
		{
			*p = 0;
		}

		if (_stricmp(sk, "male") == 0 || _stricmp(sk, "cyborg") == 0)
		{
			Cvar_Set("gender", "male");
		}
		else if (_stricmp(sk, "female") == 0 || _stricmp(sk, "crackhor") == 0)
		{
			Cvar_Set("gender", "female");
		}
		else
		{
			Cvar_Set("gender", "none");
		}

		gender->modified = false;
	}
}


/*
==============
CL_Userinfo_f
==============
*/
void CL_Userinfo_f () {
	Com_Printf ("User info settings:\n");
	Info_Print (Cvar_Userinfo ());
}

/*
=================
CL_Snd_Restart_f

Restart the sound subsystem so it can pick up
new parameters and flush all sounds
=================
*/
void CL_Snd_Restart_f () {
	S_Restart ();
	CL_RegisterSounds ();

	// cause music track to reload if already playing
	s_musicsrc->modified = true;
}

int precache_check;				// for autodownload of precache items
int precache_spawncount;
int precache_tex;
int precache_model_skin;

byte *precache_model;			// used for skin checking in alias models

#define PLAYER_MULT 5

// ENV_CNT is map load, ENV_CNT+1 is first env map
#define ENV_CNT (CS_PLAYERSKINS + MAX_CLIENTS * PLAYER_MULT)
#define TEXTURE_CNT (ENV_CNT+13)

/**
 * \fixme	This function is HUGE. It needs to be broken apart into
 *			individual sections.
 */
void CL_RequestNextDownload()
{
	unsigned map_checksum; // for detecting cheater maps
	char fn[MAX_OSPATH];
	dmd2header_t* pheader;

	if (cls.state != ca_connected) { return; }

	if (!allow_download->value && precache_check < ENV_CNT) { precache_check = ENV_CNT; }

	if (precache_check == CS_MODELS) // confirm map
	{
		precache_check = CS_MODELS + 2;	// 0 isn't used
		if (allow_download_maps->value)
		{
			if (!CL_CheckOrDownloadFile(cl.configstrings[CS_MODELS + 1]))
				return;			// started a download
		}
	}

	if (precache_check >= CS_MODELS && precache_check < CS_MODELS + MAX_MODELS)
	{
		if (allow_download_models->value)
		{
			while (precache_check < CS_MODELS + MAX_MODELS && cl.configstrings[precache_check][0])
			{
				if (cl.configstrings[precache_check][0] == '*' || cl.configstrings[precache_check][0] == '#')
				{
					precache_check++;
					continue;
				}
				if (precache_model_skin == 0)
				{
					if (!CL_CheckOrDownloadFile(cl.configstrings[precache_check]))
					{
						precache_model_skin = 1;
						return;	// started a download
					}
					precache_model_skin = 1;
				}
				// checking for skins in the model
				if (!precache_model)
				{
					FS_LoadFile(cl.configstrings[precache_check], reinterpret_cast<void**>(&precache_model));
					if (!precache_model)
					{
						precache_model_skin = 0;
						precache_check++;
						continue;	// couldn't load it
					}
					if (LittleLong(*(unsigned*)precache_model) != IDALIASMD2)
					{
						// not an alias model
						FS_FreeFile(precache_model);
						precache_model = 0;
						precache_model_skin = 0;
						precache_check++;
						continue;
					}
					pheader = (dmd2header_t *)precache_model;
					if (LittleLong(pheader->version) != ALIAS_MD2_VERSION)
					{
						precache_check++;
						precache_model_skin = 0;
						continue;	// couldn't load it
					}
				}

				pheader = (dmd2header_t *)precache_model;

				while (precache_model_skin - 1 < LittleLong(pheader->num_skins))
				{
					if (!CL_CheckOrDownloadFile((char*)precache_model + LittleLong(pheader->ofs_skins) + (precache_model_skin - 1) * MAX_SKINNAME))
					{
						precache_model_skin++;
						return;	// started a download
					}
					precache_model_skin++;
				}
				if (precache_model)
				{
					FS_FreeFile(precache_model);
					precache_model = 0;
				}

				precache_model_skin = 0;
				precache_check++;
			}
		}
		precache_check = CS_SOUNDS;
	}

	if (precache_check >= CS_SOUNDS && precache_check < CS_SOUNDS + MAX_SOUNDS)
	{
		if (allow_download_sounds->value)
		{
			if (precache_check == CS_SOUNDS) { precache_check++; } // zero is blank

			while (precache_check < CS_SOUNDS + MAX_SOUNDS && cl.configstrings[precache_check][0])
			{
				if (cl.configstrings[precache_check][0] == '*')
				{
					precache_check++;
					continue;
				}
				
				Q_sprintf(fn, sizeof(fn), "sound/%s", cl.configstrings[precache_check++]);
				if (!CL_CheckOrDownloadFile(fn)) { return; } // started a download
			}
		}
		precache_check = CS_IMAGES;
	}

	precache_check = CS_PLAYERSKINS;

	// skins are special, since a player has three things to download:
	// model, weapon model and skin
	// so precache_check is now *3
	if (precache_check >= CS_PLAYERSKINS && precache_check < CS_PLAYERSKINS + MAX_CLIENTS * PLAYER_MULT)
	{
		if (allow_download_players->value)
		{
			while (precache_check < CS_PLAYERSKINS + MAX_CLIENTS * PLAYER_MULT)
			{
				char model[MAX_QPATH], skin[MAX_QPATH], *p;

				int i = (precache_check - CS_PLAYERSKINS) / PLAYER_MULT;
				int n = (precache_check - CS_PLAYERSKINS) % PLAYER_MULT;

				if (!cl.configstrings[CS_PLAYERSKINS + i][0])
				{
					precache_check = CS_PLAYERSKINS + (i + 1) * PLAYER_MULT;
					continue;
				}

				if ((p = strchr(cl.configstrings[CS_PLAYERSKINS + i], '\\')) != nullptr)
					p++;
				else
					p = cl.configstrings[CS_PLAYERSKINS + i];

				strcpy(model, p);
				p = strchr(model, '/');
				if (!p)
					p = strchr(model, '\\');
				if (p)
				{
					*p++ = 0;
					strcpy(skin, p);
				}
				else
					*skin = 0;

				switch (n)
				{
				case 0: // model
					Q_sprintf(fn, sizeof(fn), "players/%s/tris.md2", model);
					if (!CL_CheckOrDownloadFile(fn))
					{
						precache_check =
							CS_PLAYERSKINS + i * PLAYER_MULT + 1;
						return;	// started a download
					}
					n++;
					/* FALL THROUGH */

				case 1: // weapon model
					Q_sprintf(fn, sizeof(fn), "players/%s/weapon.md2", model);
					if (!CL_CheckOrDownloadFile(fn))
					{
						precache_check = CS_PLAYERSKINS + i * PLAYER_MULT + 2;
						return;	// started a download
					}
					n++;
					/* FALL THROUGH */

				case 2: // weapon skin
					Q_sprintf(fn, sizeof(fn), "players/%s/weapon.tga", model);
					if (!CL_CheckOrDownloadFile(fn)) {
						precache_check = CS_PLAYERSKINS + i * PLAYER_MULT + 3;
						return;	// started a download
					}
					n++;
					/* FALL THROUGH */

				case 3: // skin
					Q_sprintf(fn, sizeof(fn), "players/%s/%s.tga", model, skin);
					if (!CL_CheckOrDownloadFile(fn))
					{
						precache_check = CS_PLAYERSKINS + i * PLAYER_MULT + 4;
						return;	// started a download
					}
					n++;
					/* FALL THROUGH */

				case 4: // skin_i
					Q_sprintf(fn, sizeof(fn), "players/%s/%s_i.tga", model, skin);
					if (!CL_CheckOrDownloadFile(fn))
					{
						precache_check = CS_PLAYERSKINS + i * PLAYER_MULT + 5;
						return;	// started a download
					}
					// move on to next model
					precache_check = CS_PLAYERSKINS + (i + 1) * PLAYER_MULT;
				}
			}
		}
		// precache phase completed
		precache_check = ENV_CNT;
	}

	if (precache_check == ENV_CNT)
	{
		precache_check = ENV_CNT + 1;

		CM_LoadMap(cl.configstrings[CS_MODELS + 1], true, &map_checksum);

		if (map_checksum != atoi(cl.configstrings[CS_MAPCHECKSUM]))
		{
			Com_Error(ERR_DROP, "Local map version differs from server: %i != '%s'\n", map_checksum, cl.configstrings[CS_MAPCHECKSUM]);
			return;
		}
	}

	if (precache_check > ENV_CNT && precache_check < TEXTURE_CNT)
	{
		if (allow_download->value && allow_download_maps->value)
		{
			/*
			while (precache_check < TEXTURE_CNT)
			{
				int n = precache_check++ - ENV_CNT - 1;

				if (n & 1)
					Q_sprintf (fn, sizeof(fn), "env/%s%s.pcx", cl.configstrings[CS_SKY], env_suf[n / 2]);
				else
					Q_sprintf (fn, sizeof(fn), "env/%s%s.tga", cl.configstrings[CS_SKY], env_suf[n / 2]);
				if (!CL_CheckOrDownloadFile (fn))
					return;		// started a download
			}
			*/
		}
		precache_check = TEXTURE_CNT;
	}

	if (precache_check == TEXTURE_CNT)
	{
		precache_check = TEXTURE_CNT + 1;
		precache_tex = 0;
	}

	// confirm existance of textures, download any that don't exist
	/*
	if (precache_check == TEXTURE_CNT + 1)
	{
		// from qcommon/cmodel.c
		extern int numTexInfo;
		extern mapsurface_t MAP_SURFACES[];

		if (allow_download->value && allow_download_maps->value)
		{
			while (precache_tex < numTexInfo)
			{
				char fn[MAX_OSPATH];
				unsigned int pt;

				pt = precache_tex + 1;

				Q_sprintf(fn, sizeof(fn), "textures/%s.tga", MAP_SURFACES[pt].rname);
				if (!FS_FileExists(fn))
				{
					Con_Print(va("CL_RequestNextDownload(): Unable to find texture \'%s\'.", MAP_SURFACES[pt].rname));
					return;
				}
				precache_tex++; // don't update precache count unless we know we have an available texture.
			}
		}
		precache_check = TEXTURE_CNT + 999;
	}
	*/

	CL_RegisterSounds();
	CL_PrepRefresh();

	MSG_WriteByte(&cls.netchan.message, clc_stringcmd);
	MSG_WriteString(&cls.netchan.message, va("begin %i\n", precache_spawncount));
}


/*
=================
CL_Precache_f

The server will send this command right
before allowing the client into the server
=================
*/
void CL_Precache_f()
{
	// Yet another hack to let old demos work
	// the old precache sequence
	if (Cmd_Argc() < 2)
	{
		unsigned map_checksum;	// for detecting cheater maps

		CM_LoadMap(cl.configstrings[CS_MODELS + 1], true, &map_checksum);
		CL_RegisterSounds();
		CL_PrepRefresh();
		return;
	}

	precache_check = CS_MODELS;
	precache_spawncount = atoi(Cmd_Argv(1));
	precache_model = 0;
	precache_model_skin = 0;

	CL_RequestNextDownload();
}


/*
=================
CL_InitLocal
=================
*/
void CL_InitLocal()
{
	cls.state = ca_disconnected;
	cls.realtime = Sys_Milliseconds();

	CL_InitInput();

	adr0 = Cvar_Get("adr0", "", CVAR_ARCHIVE);
	adr1 = Cvar_Get("adr1", "", CVAR_ARCHIVE);
	adr2 = Cvar_Get("adr2", "", CVAR_ARCHIVE);
	adr3 = Cvar_Get("adr3", "", CVAR_ARCHIVE);
	adr4 = Cvar_Get("adr4", "", CVAR_ARCHIVE);
	adr5 = Cvar_Get("adr5", "", CVAR_ARCHIVE);
	adr6 = Cvar_Get("adr6", "", CVAR_ARCHIVE);
	adr7 = Cvar_Get("adr7", "", CVAR_ARCHIVE);
	adr8 = Cvar_Get("adr8", "", CVAR_ARCHIVE);

	// register our variables
	cl_add_blend = Cvar_Get("cl_blend", "1", CVAR_ARCHIVE);
	cl_add_lights = Cvar_Get("cl_lights", "1", 0);
	cl_add_particles = Cvar_Get("cl_particles", "1", 0);
	cl_add_entities = Cvar_Get("cl_entities", "1", 0);
	cl_gun = Cvar_Get("cl_gun", "1", 0);
	cl_footsteps = Cvar_Get("cl_footsteps", "1", 0);
	cl_noskins = Cvar_Get("cl_noskins", "0", 0);
	cl_autoskins = Cvar_Get("cl_autoskins", "0", 0);
	cl_predict = Cvar_Get("cl_predict", "1", 0);
	cl_maxfps = Cvar_Get("cl_maxfps", "600", CVAR_ARCHIVE);
	cl_upspeed = Cvar_Get("cl_upspeed", "200", 0);
	cl_forwardspeed = Cvar_Get("cl_forwardspeed", "200", 0);
	cl_sidespeed = Cvar_Get("cl_sidespeed", "200", 0);
	cl_yawspeed = Cvar_Get("cl_yawspeed", "140", 0);
	cl_pitchspeed = Cvar_Get("cl_pitchspeed", "150", 0);
	cl_anglespeedkey = Cvar_Get("cl_anglespeedkey", "1.5", 0);

	cl_drawFPS = Cvar_Get("cl_drawFPS", "0", CVAR_ARCHIVE);	// drawfps - // MrG
	cl_drawFPS->help = "off / average / full";

	cl_run = Cvar_Get("cl_run", "0", CVAR_ARCHIVE);

	freelook = Cvar_Get("freelook", "1", CVAR_ARCHIVE);
	lookspring = Cvar_Get("lookspring", "0", CVAR_ARCHIVE);
	lookstrafe = Cvar_Get("lookstrafe", "0", CVAR_ARCHIVE);
	sensitivity = Cvar_Get("sensitivity", "3", CVAR_ARCHIVE);

	m_pitch = Cvar_Get("m_pitch", "0.022", CVAR_ARCHIVE);
	m_yaw = Cvar_Get("m_yaw", "0.022", 0);
	m_forward = Cvar_Get("m_forward", "1", 0);
	m_side = Cvar_Get("m_side", "1", 0);

	cl_shownet = Cvar_Get("cl_shownet", "0", 0);
	cl_showmiss = Cvar_Get("cl_showmiss", "0", 0);
	cl_showclamp = Cvar_Get("showclamp", "0", 0);
	cl_timeout = Cvar_Get("cl_timeout", "120", 0);
	cl_paused = Cvar_Get("paused", "0", 0);
	cl_timedemo = Cvar_Get("timedemo", "0", 0);

	rcon_client_password = Cvar_Get("rcon_password", "", 0);
	rcon_address = Cvar_Get("rcon_address", "", 0);

	cl_lightlevel = Cvar_Get("r_lightLevel", "0", 0);
	cl_drawTime = Cvar_Get("cl_drawTime", "0", CVAR_ARCHIVE);

	cl_brass = Cvar_Get("cl_brass", "512", CVAR_ARCHIVE);

	cl_railcore_red = Cvar_Get("cl_railcore_red", "1", CVAR_ARCHIVE);
	cl_railcore_green = Cvar_Get("cl_railcore_green", "1", CVAR_ARCHIVE);
	cl_railcore_blue = Cvar_Get("cl_railcore_blue", "1", CVAR_ARCHIVE);

	cl_railspiral_red = Cvar_Get("cl_railspiral_red", "0", CVAR_ARCHIVE);
	cl_railspiral_green = Cvar_Get("cl_railspiral_green", "0", CVAR_ARCHIVE);
	cl_railspiral_blue = Cvar_Get("cl_railspiral_blue", "1", CVAR_ARCHIVE);

	cl_decals = Cvar_Get("cl_decals", "1", CVAR_ARCHIVE);
	cl_drawhud = Cvar_Get("cl_drawhud", "1", CVAR_ARCHIVE);

	cl_thirdPerson = Cvar_Get("cl_thirdPerson", "0", CVAR_ARCHIVE);
	cl_thirdPersonAngle = Cvar_Get("cl_thirdPersonAngle", "30", CVAR_ARCHIVE);
	cl_thirdPersonRange = Cvar_Get("cl_thirdPersonRange", "50", CVAR_ARCHIVE);
	cl_blood = Cvar_Get("cl_blood", "1", CVAR_ARCHIVE);
	cl_fontScale = Cvar_Get("cl_fontScale", "1", 0);
	cl_itemsBobbing = Cvar_Get("cl_itemsBobbing", "1", CVAR_ARCHIVE);

	if (cl_fontScale->value < 1.0f) { Cvar_Set("cl_fontScale", "1"); }

	// userinfo
	info_password = Cvar_Get("password", "", CVAR_USERINFO);
	info_spectator = Cvar_Get("spectator", "0", CVAR_USERINFO);
	name = Cvar_Get("name", "Unknown Quaker", CVAR_USERINFO | CVAR_ARCHIVE);
	skin = Cvar_Get("skin", "male/grunt", CVAR_USERINFO | CVAR_ARCHIVE);
	rate = Cvar_Get("rate", "25000", CVAR_USERINFO | CVAR_ARCHIVE);	// FIXME
	msg = Cvar_Get("msg", "1", CVAR_USERINFO | CVAR_ARCHIVE);
	hand = Cvar_Get("hand", "0", CVAR_USERINFO | CVAR_ARCHIVE);

	fov = Cvar_Get("fov", "91", CVAR_USERINFO | CVAR_ARCHIVE);
	fov->help = "Field Of Vision (degrees). '90' will block '+zoom'.";
	zoomfov = Cvar_Get("zoomfov", "22.5", CVAR_ARCHIVE);
	zoomfov->help = "lower FOV limit for '+zoom'";

	gender = Cvar_Get("gender", "male", CVAR_USERINFO | CVAR_ARCHIVE);
	gender_auto = Cvar_Get("gender_auto", "1", CVAR_ARCHIVE);
	gender->modified = false;	// clear this so we know when user sets it manually

	dmflags = Cvar_Get("dmflags", "0", CVAR_SERVERINFO);
	cl_vwep = Cvar_Get("cl_vwep", "1", CVAR_ARCHIVE);
	deathmatch = Cvar_Get("deathmatch", "0", CVAR_SERVERINFO);

	// register our commands
	Cmd_AddCommand("cmd", CL_ForwardToServer_f);
	Cmd_AddCommand("pause", CL_Pause_f);
	Cmd_AddCommand("pingservers", CL_PingServers_f);
	Cmd_AddCommand("skins", CL_Skins_f);

	Cmd_AddCommand("userinfo", CL_Userinfo_f);
	Cmd_AddCommand("snd_restart", CL_Snd_Restart_f);

	Cmd_AddCommand("changing", CL_Changing_f);
	Cmd_AddCommand("disconnect", CL_Disconnect_f);
	Cmd_AddCommand("record", CL_Record_f);
	Cmd_AddCommand("stop", CL_Stop_f);

	Cmd_AddCommand("cvar_inc", CL_Increase_f);
	Cmd_AddCommand("ConsoleVariableoggle", CL_Toggle_f);

	Cmd_AddCommand("quit", CL_Quit_f);
	Cmd_AddCommand("exit", CL_Quit_f);

	Cmd_AddCommand("connect", CL_Connect_f);
	Cmd_AddCommand("reconnect", CL_Reconnect_f);

	Cmd_AddCommand("rcon", CL_Rcon_f);

	Cmd_AddCommand("setenv", CL_Setenv_f);
	Cmd_AddCommand("precache", CL_Precache_f);
	Cmd_AddCommand("download", CL_Download_f);

	// forward to server commands

	// the only thing this does is allow command completion
	// to work -- all unknown commands are automatically
	// forwarded to the server
	Cmd_AddCommand("wave", nullptr);
	Cmd_AddCommand("inven", nullptr);
	Cmd_AddCommand("kill", nullptr);
	Cmd_AddCommand("use", nullptr);
	Cmd_AddCommand("drop", nullptr);
	Cmd_AddCommand("say", nullptr);
	Cmd_AddCommand("say_team", nullptr);
	Cmd_AddCommand("info", nullptr);
	Cmd_AddCommand("prog", nullptr);
	Cmd_AddCommand("give", nullptr);
	Cmd_AddCommand("god", nullptr);
	Cmd_AddCommand("notarget", nullptr);
	Cmd_AddCommand("noclip", nullptr);
	Cmd_AddCommand("invuse", nullptr);
	Cmd_AddCommand("invprev", nullptr);
	Cmd_AddCommand("invnext", nullptr);
	Cmd_AddCommand("invdrop", nullptr);
	Cmd_AddCommand("weapnext", nullptr);
	Cmd_AddCommand("weapprev", nullptr);
}



/**
 * Writes key bindings and archived cvars to config.cfg
 */
void CL_WriteConfiguration()
{
	if (cls.state == ca_uninitialized) { return; }

	FS_File* f = FS_Open("config.cfg", FS_OPEN_WRITE);
	if (!f)
	{
		Com_Printf("Couldn't write config.cfg.\n");
		return;
	}

	FS_fprintf(f, "// Automatically generated. Modify at your own risk.\n");
	Key_WriteBindings(f);
	FS_Close(f);

	Cvar_WriteVariables("config.cfg");
}


/*
==================
CL_FixCvarCheats

==================
*/

typedef struct {
	char* name;
	char* value;
	ConsoleVariable *var;
} cheatvar_t;

cheatvar_t cheatvars[] = {
	{ "timescale", "1" },
	{ "timedemo", "0" },
	{ "r_drawWorld", "1" },
	{ "cl_testlights", "0" },
	{ "r_fullbright", "0" },
	{ "r_drawflat", "0" },
	{ "paused", "0" },
	{ "fixedtime", "0" },
	{ "sw_draworder", "0" },
	{ "gl_lightmap", "0" },
	{ "gl_saturatelighting", "0" },
	{ nullptr, nullptr }
};

int numcheatvars;


void CL_FixCvarCheats()
{
	int i;
	cheatvar_t *var;

	if (!strcmp(cl.configstrings[CS_MAXCLIENTS], "1") || !cl.configstrings[CS_MAXCLIENTS][0])
	{
		return; // single player can cheat
	}

	// find all the cvars if we haven't done it yet
	if (!numcheatvars)
	{
		while (cheatvars[numcheatvars].name)
		{
			cheatvars[numcheatvars].var = Cvar_Get(cheatvars[numcheatvars].name, cheatvars[numcheatvars].value, 0);
			numcheatvars++;
		}
	}
	// make sure they are all set to the proper values
	for (i = 0, var = cheatvars; i < numcheatvars; i++, var++)
	{
		if (var->var->string == var->value)
		{
			Cvar_Set(var->name, var->value);
		}
	}
}

//============================================================================

/*
==================
CL_SendCommand

==================
*/

void CL_SendCommand () {
	// get new key events
	Sys_SendKeyEvents ();

	// process console commands
	Cbuf_Execute ();

	// fix any cheating cvars
	CL_FixCvarCheats ();

	// send intentions now
	CL_SendCmd ();

	// resend a connection request if necessary
	CL_CheckForResend ();
}


/**
 * 
 */
void CL_Frame(int msec)
{
	static int extratime;
	static int lasttimecalled;

	if (dedicated->value)
		return;

	extratime += msec;

	if (cl_maxfps->value == 0)
		Cvar_SetValue("cl_maxfps", 100);

	if (!cl_timedemo->value)
	{
		int temptime = 1000 / cl_maxfps->value - extratime;

		if (cls.state == ca_connected && extratime < 100)
			return;				// don't flood packets out while connecting

		// give CPU to other processes while idle
		if (temptime > 0)
		{
			// XXX: sleep half the time? for now it doesn't seem necessary
#ifndef _WIN32
			usleep(temptime);
#else
			Sleep(temptime);
#endif
			return;				// framerate is too high
		}
	}

	// let the mouse activate or deactivate  <-- ??
	IN_Frame();

	// decide the simulation time
	cls.frametime = extratime / 1000.0;
	cl.time += extratime;
	cls.realtime = curtime;

	extratime = 0;
	if (cls.frametime > (1.0 / 5))
		cls.frametime = (1.0 / 5);

	// if in the debugger last frame, don't timeout
	if (msec > 5000)
		cls.netchan.last_received = Sys_Milliseconds();


	CL_ReadPackets();// fetch results from server
	CL_SendCommand();// send a new command message to the server
	CL_PredictMovement();// predict all unacknowledged movements


	VID_CheckChanges(); // allow rendering DLL change

	if (!cl.refresh_prepped && cls.state == ca_active) { CL_PrepRefresh(); }


	if (host_speeds->value) { time_before_ref = Sys_Milliseconds(); } // update the screen

	SCR_UpdateScreen();

	if (host_speeds->value) { time_after_ref = Sys_Milliseconds(); }

	// update audio
	{
		float orientation[6];

		memcpy(orientation, cl.v_forward, sizeof(vec3_t));
		memcpy(&orientation[3], cl.v_up, sizeof(vec3_t));
		S_Update(cl.refdef.vieworg, cl.v_forward, orientation);
	}

	Music_Update();

	// advance local effects for next frame
	CL_RunDLights();
	CL_RunLightStyles();
	SCR_RunConsole();

	cls.framecount++;

	if (log_stats->value)
	{
		if (cls.state == ca_active)
		{
			if (!lasttimecalled)
			{
				lasttimecalled = Sys_Milliseconds();
				if (LOG_STATS_FILE)
					FS_fprintf(LOG_STATS_FILE, "0\n");
			}
			else
			{
				int now = Sys_Milliseconds();

				if (LOG_STATS_FILE)
					FS_fprintf(LOG_STATS_FILE, "%d\n", now - lasttimecalled);

				lasttimecalled = now;
			}
		}
	}
}


/**
 * 
 */
void CL_Init()
{
	// nothing running on the client
	if (dedicated->value)
		return;

	VID_Init();
	S_Init(1);

	V_Init();
	M_Init();

	SCR_Init();
	cls.disable_screen = 1.0f; // don't draw yet
	Music_Init();
	CL_InitLocal();
	IN_Init();

	Cbuf_Execute();
}


/**
 * \fixme	This is a callback from Sys_Quit and Com_Error.  It would be better
 * 			to run quit through here before the final handoff to the sys code.
 */
void CL_Shutdown()
{
	static bool _isdown = false;

	if (_isdown)
	{
		std::cout << "recursive shutdown" << std::endl;
		return;
	}

	_isdown = true;

	CL_WriteConfiguration();

	Music_Shutdown();
	S_Shutdown();
	FS_Shutdown();
	IN_Shutdown();
	VID_Shutdown();
}
