#pragma once

extern ALuint cl_sfx_lashit;
extern ALuint cl_sfx_railg;
extern ALuint cl_sfx_rockexp;
extern ALuint cl_sfx_grenexp;
extern ALuint cl_sfx_watrexp;
extern ALuint cl_sfx_plasexp;
extern ALuint cl_sfx_lightning;
extern ALuint cl_sfx_disrexp;


extern Model* cl_mod_explode;
extern Model* cl_mod_smoke;
extern Model* cl_mod_parasite_segment;
extern Model* cl_mod_grapple_cable;
extern Model* cl_mod_parasite_tip;
extern Model* cl_mod_explo4;
extern Model* cl_mod_bfg_explo;
extern Model* cl_mod_powerscreen;
extern Model* cl_mod_distort;
extern Model* cl_mod_plasmaexplo;
extern Model* cl_mod_lightning;
extern Model* cl_mod_heatbeam;
extern Model* cl_mod_monster_heatbeam;
extern Model* cl_mod_explo4_big;
