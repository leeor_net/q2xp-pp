﻿/*
Copyright (C) 2004-2007 Quake2xp Team.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// cl_particles.c -- client side particle effects

#include "client.h"

#include "Particle.h"

#include "../common/def_ef.h"
#include "../common/def_muzzle.h"

#include "../ref_gl/r_local.h"	

static vec3_t avelocities[NUM_VERTEX_NORMALS];


// ==============================================================
// CONSTANTS
// ==============================================================
const float	PARTICLE_GRAVITY = 40.0f;

const int BLASTER_SPARK_PARTICLES = 15;


laser_t cl_lasers[MAX_LASERS];


// ==============================================================
// PARTICLE MANAGEMENT
// ==============================================================
Particle* active_particles, *free_particles;

Particle particles[MAX_PARTICLES];
int cl_numparticles = MAX_PARTICLES;
void CL_ParticleSmoke2 (vec3_t origin, vec3_t dir, float r, float g, float b, int count, bool add);

/*
===============
CL_ClearParticles
===============
*/
void CL_ClearParticles()
{
	int i;

	free_particles = &particles[0];
	active_particles = nullptr;

	for (i = 0; i < cl_numparticles; i++)
		particles[i].next = &particles[i + 1];
	particles[cl_numparticles - 1].next = nullptr;
}


/*
===============
CL_AddParticles
===============
*/


void VectorReflect(const vec3_t v, const vec3_t normal, vec3_t out)
{
	float d;

	d = 2.0 * (v[0] * normal[0] + v[1] * normal[1] + v[2] * normal[2]);

	out[0] = v[0] - normal[0] * d;
	out[1] = v[1] - normal[1] * d;
	out[2] = v[2] - normal[2] * d;
}

trace_t CL_PMTrace (vec3_t start, vec3_t mins, vec3_t maxs, vec3_t end);


void CL_AddParticles()
{
	Particle* p, *next;
	vec3_t origin, mins, maxs, kls, old;
	vec3_t color, lcol;
	Particle* active, *tail;
	float alpha, lightradius;
	float time, time2, grav = Cvar_VariableValue("sv_gravity");
	float size, len, lerp, endLerp;
	float orient, backup;
	int sFactor, dFactor, flags;
	int cont;
	bool ground;

	if (!grav)
		grav = 1;
	else
		grav /= 800; /// \fixme	MAGIC NUMBER

	active = nullptr;
	tail = nullptr;

	for (p = active_particles; p; p = next)
	{
		next = p->next;

		endLerp = (float)(cl.time - p->time) / (p->endTime - p->time);
		lerp = 1.0 - endLerp;

		time = (cl.time - p->time) * 0.001;
		alpha = p->alpha + time * p->alphavel;
		len = p->len * lerp + p->endLen * endLerp;

		if (alpha <= 0) // faded out
		{
			p->next = free_particles;
			free_particles = p;
			continue;
		}
		size = p->size + time * p->sizeVel; // kill
		if (size <= 0)
		{
			p->next = free_particles;
			free_particles = p;
			continue;
		}

		if (p->flags & PARTICLE_CLAMP)
		{
			if (size >= p->sizeVel)
				size = p->sizeVel; //clamp!
		}

		if (p->endTime <= cl.time) // kill
		{
			p->next = free_particles;
			free_particles = p;
			continue;
		}

		if (len <= 0.0 && p->len > p->endLen) // kill
		{
			p->next = free_particles;
			free_particles = p;
			continue;
		}

		if (alpha > 1.0)
			alpha = 1;

		color[0] = p->color[0] + p->colorVel[0] * time;
		color[1] = p->color[1] + p->colorVel[1] * time;
		color[2] = p->color[2] + p->colorVel[2] * time;

		time2 = time * time;

		origin[0] = p->origin[0] + p->vel[0] * time + p->accel[0] * time2;
		origin[1] = p->origin[1] + p->vel[1] * time + p->accel[1] * time2;
		backup = p->origin[2] + p->vel[2] * time + p->accel[2] * time2 * grav;

		origin[2] = backup - 5;
		cont = CL_PMpointcontents(origin);
		origin[2] = backup;
		ground = (cont & MASK_SOLID) != 0;

		VectorCopy(p->origin, old);

		sFactor = p->sFactor;
		dFactor = p->dFactor;
		flags = p->flags;
		orient = p->orient;

		// = Particle dlight
		lightradius = p->lightradius;
		VectorCopy(p->lcolor, lcol);
		if (p->flags & PARTICLE_LIGHTING)
		{
			V_AddLight(origin, lightradius, lcol[0], lcol[1], lcol[2], vec3_origin, 0, 0);
		}
		VectorSet(kls, origin[0], origin[1], origin[2] + size * 2); //killed particle origin -  in air, water

		if (p->flags & PARTICLE_UNDERWATER)
		{

			if (!(CL_PMpointcontents(kls) & MASK_WATER)) //in water
			{
				p->next = free_particles;
				free_particles = p;
				continue;
			}
		}

		if (p->flags & PARTICLE_NONSOLID)
		{

			if (CL_PMpointcontents(kls) & MASK_SHOT)
			{
				p->next = free_particles;
				free_particles = p;
				continue;
			}
		}

		if (p->flags & PARTICLE_AIRONLY) // on air :-)
		{

			if (CL_PMpointcontents(kls) & MASK_WATER)
			{
				p->next = free_particles;
				free_particles = p;
				continue;
			}
		}

		if (!ground)
		{

			if (p->flags & PARTICLE_STOPED)
			{

				p->flags &= ~PARTICLE_STOPED;
				p->flags |= PARTICLE_BOUNCE;
				p->accel[2] = -PARTICLE_GRAVITY * 15;
				//Reset
				p->time = cl.time;
				VectorCopy(origin, p->origin);
				p->len = p->endLen = len = 1.0;
				VectorCopy(color, p->color);
				p->flags &= ~PARTICLE_DIRECTIONAL;
				p->flags &= ~PARTICLE_STRETCH;
			}
		}

		p->next = nullptr;
		if (!tail)
			active = tail = p;
		else
		{
			tail->next = p;
			tail = p;
		}

		if (p->flags & PARTICLE_FRICTION)
		{
			// Water friction affected particle
			int contents = CL_PMpointcontents(origin);
			if (contents & MASK_WATER)
			{
				// Add friction
				if (contents & CONTENTS_WATER)
				{
					VectorScale(p->vel, 0.25, p->vel);
					VectorScale(p->accel, 0.25, p->accel);
				}
				else if (contents & CONTENTS_SLIME)
				{
					VectorScale(p->vel, 0.20, p->vel);
					VectorScale(p->accel, 0.20, p->accel);
				}
				else if (contents & CONTENTS_LAVA)
				{
					VectorScale(p->vel, 0.10, p->vel);
					VectorScale(p->accel, 0.10, p->accel);
				}
				else if (contents & MASK_SOLID)
				{
					p->alpha = 0;	// kill the particle
					continue;	// don't add the particle
				}
				// Don't add friction again
				p->flags &= ~PARTICLE_FRICTION;

				// Reset
				p->time = cl.time;
				p->len = p->endLen = len = 1.0;
				VectorCopy(origin, p->origin);
				VectorCopy(color, p->color);
			}
		}


		if (p->flags & PARTICLE_BOUNCE)
		{
			trace_t trace;
			VectorSet(mins, -size, -size, -size);
			VectorSet(maxs, size, size, size);
			VectorScale(mins, 2, mins);
			VectorScale(maxs, 2, maxs);

			trace = CL_PMTraceWorld(p->previous_origin, mins, maxs, origin, MASK_SOLID, false);

			if (trace.fraction > 0 && trace.fraction < 1)
			{
				vec3_t	vel;
				float time = cl.time - (cls.frametime + cls.frametime * trace.fraction) * 1000;
				time = (time - p->time) * 0.001;

				VectorSet(vel, p->vel[0], p->vel[1], p->vel[2] + p->accel[2] * time * grav);
				VectorReflect(vel, trace.plane.normal, p->vel);
				VectorScale(p->vel, 0.3, p->vel);

				// Check for stop or slide along the plane
				if (trace.plane.normal[2] > 0 && p->vel[2] < 1)
				{
					if (trace.plane.normal[2] > 0.9) {
						VectorClear(p->vel);
						VectorClear(p->accel);
						p->flags &= ~PARTICLE_BOUNCE;
						p->flags |= PARTICLE_STOPED;
					}
					else
					{
						/// \fixme check for new plane or free fall
						float dot = DotProduct(p->vel, trace.plane.normal);
						VectorScaleAndAdd(p->vel, trace.plane.normal, p->vel, -dot);
						dot = DotProduct(p->accel, trace.plane.normal);
						VectorScaleAndAdd(p->accel, trace.plane.normal, p->accel, -dot);
					}
				}

				VectorCopy(trace.endpos, origin);

				// Reset
				p->time = cl.time;
				VectorCopy(origin, p->origin);
				p->len = p->endLen = len = 1.0;
				VectorCopy(color, p->color);
				p->flags &= ~PARTICLE_DIRECTIONAL;
				p->flags &= ~PARTICLE_STRETCH;
			}
		}

		if (cl_blood->value)
		{
			if (p->type == Particle::PT_BLOODDRIP)
			{
				trace_t trace;
				trace = CL_Trace(p->previous_origin, origin, p->size * 2, MASK_SOLID);

				if (trace.fraction != 1.0)
				{
					p->alpha = 0;	// kill the particle after marking
					CL_AddDecalToScene(origin, trace.plane.normal, 1, 1, 1, 1, 1, 1, 1, 1, 7, 12000, DECAL_BLOOD1 + (rand() & 9), 0, frand() * 360, GL_ZERO, GL_ONE_MINUS_SRC_COLOR);
					continue;
				}

			}
		}

		V_AddParticle(origin, p->length, color, alpha, p->type, size, p->sFactor, p->dFactor, p->flags, p->time, p->orient, len, old, p->dir);
	}

	active_particles = active;
}




void CL_ParticleBlood(vec3_t origin, vec3_t dir, int count)
{
	int i, j;
	Particle* p;
	float d;

	if (!cl_blood->value) return;

	for (i = 0; i < count; i++)
	{
		if (!free_particles)
			return;

		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;

		p->type = Particle::PT_BLOODDRIP;
		p->orient = 0;
		p->flags = PARTICLE_DIRECTIONAL;
		p->flags |= PARTICLE_NONSOLID;

		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE_MINUS_SRC_ALPHA;

		p->size = 1.3;
		p->sizeVel = 0;
		p->time = cl.time;
		p->endTime = cl.time + 10000;
		p->color[0] = 0.1;
		p->color[1] = 0;
		p->color[2] = 0;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		p->len = 10;
		p->endLen = 90;

		for (j = 0; j < 3; j++)
		{
			p->origin[j] = origin[j];
			p->vel[j] = dir[j] * 30 + crand() * 60;
		}

		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = -PARTICLE_GRAVITY * 4;
		p->alpha = 1;

		p->alphavel = 0;
		VectorCopy(p->origin, p->previous_origin);
	}

	for (i = 0; i < 2; i++)
	{
		if (!free_particles)
			return;

		p = free_particles;

		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		VectorClear(p->vel);
		p->orient = 0;
		p->flags = PARTICLE_DEFAULT;
		p->time = cl.time;
		p->endTime = cl.time + 5000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE_MINUS_SRC_ALPHA;

		p->color[0] = 0.1;
		p->color[1] = 0;
		p->color[2] = 0;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		p->type = Particle::PT_BLOODMIST;
		p->size = 5;
		p->sizeVel = 20;

		d = rand() & 7;
		for (j = 0; j < 3; j++)
			p->origin[j] = origin[j] + ((rand() & 7) - 4) + d * dir[j];


		p->accel[0] = p->accel[1] = p->accel[2] = 0;
		p->alpha = 1;

		p->alphavel = -1.0 / (0.5 + frand() * 0.3);
	}
}


void CL_LaserParticle (vec3_t origin, vec3_t dir, int color, int count) {
	int i, j;
	Particle* p;
	float d;

	color &= 0xff;

	for (i = 0; i < count; i++) {
		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;

		p->flags = PARTICLE_BOUNCE;
		p->flags |= PARTICLE_FRICTION;
		p->orient = 0;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE;

		/*
		p->color[0] = cl_indexPalette[color][0];
		p->color[1] = cl_indexPalette[color][1];
		p->color[2] = cl_indexPalette[color][2];
		*/
		/// \fixme This is a hack after I removed the original palette and forced to red.
		p->color[0] = 200;
		p->color[1] = 0;
		p->color[2] = 0;


		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		p->type = Particle::PT_BLASTER;
		p->size = 1;
		p->sizeVel = 0;
		d = rand () & 7;
		for (j = 0; j < 3; j++) {
			p->origin[j] = origin[j] + ((rand () & 7) - 4) + d * dir[j];
			p->vel[j] = crand () * 20;
		}

		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = -PARTICLE_GRAVITY;
		p->alpha = 1.0;

		p->alphavel = -1.0 / (0.5 + frand () * 0.3);
		VectorCopy (p->origin, p->previous_origin);
	}
}



void CL_ParticleSmoke (vec3_t origin, vec3_t dir, int count)
{
	int i, j;
	Particle* p;
	float d;

	for (i = 0; i < count; i++)
	{
		if (!free_particles)
			return;

		p = free_particles;

		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		p->orient = 0;
		p->flags = PARTICLE_VERTEXLIGHT;
		p->flags |= PARTICLE_AIRONLY;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE_MINUS_SRC_ALPHA;

		p->color[0] = 0.4;
		p->color[1] = 0.4;
		p->color[2] = 0.4;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		p->type = Particle::PT_SMOKE;
		p->size = 4;
		p->sizeVel = 7;

		d = rand () & 7;
		for (j = 0; j < 3; j++) {
			p->origin[j] = origin[j] + ((rand () & 7) - 4) + d * dir[j];
			p->vel[j] = crand () * 20;
		}

		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = PARTICLE_GRAVITY;
		p->alpha = 1.0;

		p->alphavel = -1.0 / (0.5 + frand () * 0.3);
	}
}


void CL_ParticleSmoke2 (vec3_t origin, vec3_t dir, float r, float g, float b,
	int count, bool add) {
	int i, j;
	Particle* p;
	float d;


	for (i = 0; i < count; i++) {
		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		p->type = Particle::PT_SMOKE;
		p->flags = PARTICLE_AIRONLY;
		p->orient = frand () * 360;
		if (add) {
			p->sFactor = GL_SRC_ALPHA;
			p->dFactor = GL_ONE;
		}
		else {
			p->sFactor = GL_SRC_ALPHA;
			p->dFactor = GL_ONE_MINUS_SRC_ALPHA;
		}

		p->size = 3;
		p->sizeVel = 20;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->color[0] = r;
		p->color[1] = g;
		p->color[2] = b;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		d = rand () & 15;
		for (j = 0; j < 3; j++) {
			p->origin[j] = origin[j] + d * dir[j];
			p->vel[j] = dir[j] * 30;
		}

		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = 9;
		p->alpha = 1.0;

		p->alphavel = -1.0 / (0.5 + frand () * 0.5);

	}
}


void CL_ParticleGunSmoke (vec3_t origin, vec3_t dir, int count) {
	int i, j;
	Particle* p;
	float d;

	for (i = 0; i < count; i++) {
		if (!free_particles)
			return;
		p = free_particles;

		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		p->orient = frand () * 360;
		p->flags = PARTICLE_VERTEXLIGHT;
		p->flags |= PARTICLE_AIRONLY;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE_MINUS_SRC_ALPHA;

		p->color[0] = 0.45;
		p->color[1] = 0.45;
		p->color[2] = 0.45;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		p->type = Particle::PT_SMOKE;
		p->size = 1;
		p->sizeVel = 15;

		d = rand () & 7;
		for (j = 0; j < 3; j++) {
			p->origin[j] = origin[j];
			p->vel[j] = crand () * 20;
		}

		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = PARTICLE_GRAVITY*0.5;
		p->alpha = 0.45;

		p->alphavel = -1.0 / (0.5 + frand () * 5.3);
	}
}


/**
 * Electrical sparks?
 */
void CL_ParticleSpark (vec3_t origin, vec3_t dir, int count)
{
	int i, j;
	Particle* p;
	float d;

	for (i = 0; i < count; i++)
	{
		if (!free_particles)
			return;

		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		p->orient = 0;
		p->flags = PARTICLE_OVERBRIGHT;
		p->flags |= PARTICLE_DIRECTIONAL;
		p->time = cl.time;
		p->endTime = cl.time + 1000;
		p->sFactor = GL_ONE;
		p->dFactor = GL_ONE;
		p->len = 4;
		p->endLen = 9;
		
		p->color[0] = 1;
		p->color[1] = 1;
		p->color[2] = 0.7;

		p->colorVel[0] = 0;
		p->colorVel[1] = -0.40;
		p->colorVel[2] = -1.0;

		p->type = Particle::PT_SPARK;
		p->size = 0.4;
		p->sizeVel = 0;
		d = rand () & 7;
		for (j = 0; j < 3; j++) {
			p->origin[j] = origin[j] + ((rand () & 7) - 4) + d * dir[j];
			p->vel[j] = crand () * 20;
		}

		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = -PARTICLE_GRAVITY * 5;
		p->alpha = 1.0;

		p->alphavel = -1.0 / (1.5 + frand () * 1.666);
		VectorCopy (p->origin, p->previous_origin);
	}

	for (i = 0; i < 50; i++) {
		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		p->len = 0;
		p->endLen = 0;
		p->orient = 0;
		p->flags = PARTICLE_OVERBRIGHT;
		p->time = cl.time;
		p->endTime = cl.time + 1000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE_MINUS_SRC_ALPHA;
		p->color[0] = 1;
		p->color[1] = 0.5;
		p->color[2] = 0;

		p->colorVel[0] = 0;
		p->colorVel[1] = -0.40;
		p->colorVel[2] = 0;

		p->type = Particle::PT_DEFAULT;
		p->size = 0.5;
		p->sizeVel = 0;
		d = rand () & 19;
		for (j = 0; j < 3; j++) {
			p->origin[j] = origin[j] + ((rand () & 7) - 4) + d * dir[j];
			p->vel[j] = crand () * 70;
		}

		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = -PARTICLE_GRAVITY * 5;
		p->alpha = 1.0;

		p->alphavel = -1.0 / (1.5 + frand () * 1.666);
		VectorCopy (p->origin, p->previous_origin);
	}
}


void CL_ParticleArmorSpark (vec3_t origin, vec3_t dir, int count,
	bool power) {
	int i, j;
	Particle* p;
	float d;

	for (i = 0; i < count; i++) {
		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		p->orient = 0;
		p->flags = PARTICLE_OVERBRIGHT;
		p->flags |= PARTICLE_DIRECTIONAL;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->sFactor = GL_ONE;
		p->dFactor = GL_ONE;
		p->len = 1;
		p->endLen = 500;
		if (power)
		{
			p->color[0] = 0;
			p->color[1] = 1;
			p->color[2] = 0;

			p->colorVel[0] = 0;
			p->colorVel[1] = -1;
			p->colorVel[2] = 0;
		}
		else
		{
			p->color[0] = 1;
			p->color[1] = 1;
			p->color[2] = 0.7;

			p->colorVel[0] = 0;
			p->colorVel[1] = -1;
			p->colorVel[2] = -1.5;
		}

		p->type = Particle::PT_SPARK;
		p->size = 0.4;
		p->sizeVel = 0;
		d = rand () & 9;
		for (j = 0; j < 3; j++) {
			// p->origin[j] = origin[j]+ d*dir[j];
			// p->vel[j] = dir[j] * 30;
			p->origin[j] = origin[j] + ((rand () & 7) - 4) + d * dir[j];
			p->vel[j] = dir[j] * 30 + crand () * 30;
		}

		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = -PARTICLE_GRAVITY * 8;
		p->alpha = 1.0;

		p->alphavel = -1.0 / (1.5 + frand () * 1.666);
	}
}


void CL_ParticleTracer (vec3_t start, vec3_t end) {
	int j;
	Particle* p;
	float d;
	vec3_t dir;
	float dist = 5000;

	if (!free_particles)
		return;
	p = free_particles;
	free_particles = p->next;
	p->next = active_particles;

	VectorClear (p->accel);
	VectorSubtract (end, start, dir);
	VectorNormalize (dir);

	active_particles = p;
	p->orient = 0;
	p->flags = PARTICLE_DIRECTIONAL;
	p->flags |= PARTICLE_NONSOLID;
	p->time = cl.time;
	p->endTime = cl.time + 20000;
	p->sFactor = GL_SRC_ALPHA;
	p->dFactor = GL_ONE_MINUS_SRC_ALPHA;
	p->len = 100;
	p->endLen = 200;
	p->color[0] = 1;
	p->color[1] = 0.35;
	p->color[2] = 0;

	p->colorVel[0] = 0;
	p->colorVel[1] = 0;
	p->colorVel[2] = 0;

	p->type = Particle::PT_BLASTER;
	p->size = 0.3;
	p->sizeVel = 0;
	d = rand () & 7;
	for (j = 0; j < 3; j++) {
		p->origin[j] = start[j];
		p->vel[j] = dir[j] * dist;
	}


	p->alpha = 1.0;

	p->alphavel = -1.0 / (1.5 + frand () * 1.666);

}


/**
 * 
 */
void CL_ParticleBlasterBolt(vec3_t start, vec3_t end)
{
	vec3_t dir;

	if (!free_particles) return;

	Particle*  p = free_particles;
	free_particles = p->next;
	p->next = active_particles;

	VectorClear(p->accel);
	VectorSubtract(end, start, dir);
	VectorNormalize(dir);

	active_particles = p;
	p->orient = 0;
	p->flags = PARTICLE_DIRECTIONAL;
	p->flags |= PARTICLE_NONSOLID;
	p->time = cl.time;
	p->endTime = cl.time + 20000;
	p->sFactor = GL_ONE;
	p->dFactor = GL_ONE;
	p->len = 25;
	p->endLen = 0;
	p->color[0] = 1.0;
	p->color[1] = 1.0;
	p->color[2] = 1.0;

	p->colorVel[0] = 0;
	p->colorVel[1] = 0;
	p->colorVel[2] = 0;

	p->type = Particle::PT_BLASTER_BOLT;
	p->size = 1.5;
	p->sizeVel = 1.5;

	for (int j = 0; j < 3; j++)
	{
		p->origin[j] = start[j];		// ↓↓↓↓↓↓ lol what?
		p->vel[j] = dir[j] * 1000;	// Fuck the id! 600 for solder, 800 for tank, 1000 for others!!!!!!!
	}
	p->alpha = 1.0;
	p->alphavel = 1.0;

	VectorCopy(p->origin, p->previous_origin);
}


void CL_ParticleSplash (vec3_t origin, vec3_t dir, float r, float g, float b) {

	float d;
	int j, i;
	Particle* p;


	for (i = 0; i < 40; i++) {
		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		p->orient = frand () * 360;
		p->flags = PARTICLE_AIRONLY;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE;

		p->color[0] = r;
		p->color[1] = g;
		p->color[2] = b;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		p->type = Particle::PT_DEFAULT;
		p->size = 0.85;
		p->sizeVel = 0;

		d = rand () & 9;

		for (j = 0; j < 3; j++) {
			p->origin[j] = origin[j] + ((rand () & 7) - 4) + d * dir[j];
			p->vel[j] = dir[j] * 60 + crand () * 90;
		}

		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = -PARTICLE_GRAVITY * 4.5;
		p->alpha = 1;

		p->alphavel = -1.0 / (0.5 + frand () * 0.3);
	}


	if (!free_particles)
		return;
	p = free_particles;
	free_particles = p->next;
	p->next = active_particles;
	active_particles = p;
	VectorClear (p->accel);
	VectorClear (p->vel);
	p->orient = 0;
	p->flags = PARTICLE_DIRECTIONAL;
	p->flags |= PARTICLE_AIRONLY;
	p->time = cl.time;
	p->endTime = cl.time + 20000;
	p->sFactor = GL_SRC_ALPHA;
	p->dFactor = GL_ONE;

	p->color[0] = r;
	p->color[1] = g;
	p->color[2] = b;

	p->colorVel[0] = 0;
	p->colorVel[1] = 0;
	p->colorVel[2] = 0;

	p->alpha = 0.5;
	p->alphavel = -1.0 / (0.5 + frand () * 0.5);

	p->type = Particle::PT_WATERPLUME;
	p->size = 7;
	p->sizeVel = 0;
	p->len = 10;
	p->endLen = 2000;


	for (j = 0; j < 3; j++) {
		p->origin[j] = origin[j];
		p->vel[j] = dir[j];
	}

	for (i = 0; i < 3; i++) {

		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		VectorClear (p->accel);
		VectorClear (p->vel);
		VectorCopy (dir, p->dir);
		VectorNormalize (p->dir);
		p->orient = 0;
		p->flags = PARTICLE_ALIGNED;
		p->flags |= PARTICLE_AIRONLY;
		p->flags |= PARTICLE_NOFADE;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE;

		p->color[0] = r;
		p->color[1] = b;
		p->color[2] = g;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		p->alpha = 1;
		p->alphavel = -1.0 / (0.5 + frand () * 0.5);

		p->type = Particle::PT_WATERCIRCLE;
		p->size = 10 + frand () * 6;
		p->sizeVel = 30;
		p->len = 0;
		p->endLen = 0;

		for (j = 0; j < 3; j++) {
			p->origin[j] = origin[j];
			p->vel[j] = p->dir[j];
		}
	}


}

void CL_ParticleSplashSlime (vec3_t origin, vec3_t dir) {

	float d;
	int j, i;
	Particle* p;


	for (i = 0; i < 40; i++) {
		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		p->orient = frand () * 360;
		p->flags = PARTICLE_AIRONLY;
		p->flags |= PARTICLE_OVERBRIGHT;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE;

		p->color[0] = 0.2;
		p->color[1] = 1.0;
		p->color[2] = 0;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		p->type = Particle::PT_SPLASH;
		p->size = 3;
		p->sizeVel = -6;

		d = rand () & 9;

		for (j = 0; j < 3; j++) {
			p->origin[j] = origin[j] + ((rand () & 7) - 4) + d * dir[j];
			p->vel[j] = dir[j] * 60 + crand () * 90;
		}

		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = -PARTICLE_GRAVITY * 2.5;
		p->alpha = 1;

		p->alphavel = -1.0 / (0.5 + frand () * 0.5);
	}


	if (!free_particles)
		return;
	p = free_particles;
	free_particles = p->next;
	p->next = active_particles;
	active_particles = p;
	VectorClear (p->accel);
	VectorClear (p->vel);
	p->orient = 0;
	p->flags = PARTICLE_DIRECTIONAL;
	p->flags |= PARTICLE_AIRONLY;
	p->flags |= PARTICLE_OVERBRIGHT;
	p->time = cl.time;
	p->endTime = cl.time + 20000;
	p->sFactor = GL_SRC_ALPHA;
	p->dFactor = GL_ONE;

	p->color[0] = 0.2;
	p->color[1] = 1.0;
	p->color[2] = 0;

	p->colorVel[0] = 0;
	p->colorVel[1] = 0;
	p->colorVel[2] = 0;

	p->alpha = 0.5;
	p->alphavel = -1.0 / (0.5 + frand () * 0.5);

	p->type = Particle::PT_WATERPLUME;
	p->size = 7;
	p->sizeVel = 0;
	p->len = 10;
	p->endLen = 2000;


	for (j = 0; j < 3; j++) {
		p->origin[j] = origin[j];
		p->vel[j] = dir[j];
	}

	for (i = 0; i < 4; i++) {

		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		VectorClear (p->accel);
		VectorClear (p->vel);
		VectorCopy (dir, p->dir);
		VectorNormalize (p->dir);
		p->orient = 0;
		p->flags = PARTICLE_ALIGNED;
		p->flags |= PARTICLE_AIRONLY;
		p->flags |= PARTICLE_OVERBRIGHT;
		p->flags |= PARTICLE_NOFADE;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE;

		p->color[0] = 0.2;
		p->color[1] = 1.0;
		p->color[2] = 0;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		p->alpha = 1;
		p->alphavel = -1.0 / (0.5 + frand () * 0.5);

		p->type = Particle::PT_WATERCIRCLE;
		p->size = 10 + frand () * 6;
		p->sizeVel = 30;
		p->len = 0;
		p->endLen = 0;

		for (j = 0; j < 3; j++) {
			p->origin[j] = origin[j];
			p->vel[j] = p->dir[j];
		}
	}


}


void CL_ParticleSplashLava (vec3_t origin, vec3_t dir) {

	float d;
	int j, i;
	Particle* p;


	for (i = 0; i < 80; i++) {
		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		p->orient = frand () * 360;
		p->flags = PARTICLE_AIRONLY;
		p->flags |= PARTICLE_OVERBRIGHT;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE;

		p->color[0] = 1;
		p->color[1] = 0.3;
		p->color[2] = 0;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		p->type = Particle::PT_SPLASH;
		p->size = 3;
		p->sizeVel = -5;

		d = rand () & 9;

		for (j = 0; j < 3; j++) {
			p->origin[j] = origin[j] + ((rand () & 7) - 4) + d * dir[j];
			p->vel[j] = dir[j] * 60 + crand () * 90;
		}

		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = -PARTICLE_GRAVITY * 2.5;
		p->alpha = 1;

		p->alphavel = -1.0 / (0.5 + frand () * 0.5);
	}


	if (!free_particles)
		return;
	p = free_particles;
	free_particles = p->next;
	p->next = active_particles;
	active_particles = p;
	VectorClear (p->accel);
	VectorClear (p->vel);
	p->orient = 0;
	p->flags = PARTICLE_DIRECTIONAL;
	p->flags |= PARTICLE_AIRONLY;
	p->flags |= PARTICLE_OVERBRIGHT;
	p->time = cl.time;
	p->endTime = cl.time + 20000;
	p->sFactor = GL_SRC_ALPHA;
	p->dFactor = GL_ONE;

	p->color[0] = 1;
	p->color[1] = 0.3;
	p->color[2] = 0;

	p->colorVel[0] = 0;
	p->colorVel[1] = 0;
	p->colorVel[2] = 0;

	p->alpha = 1;
	p->alphavel = -1.0 / (0.5 + frand () * 0.5);

	p->type = Particle::PT_WATERPLUME;
	p->size = 7;
	p->sizeVel = 0;
	p->len = 10;
	p->endLen = 800;


	for (j = 0; j < 3; j++) {
		p->origin[j] = origin[j];
		p->vel[j] = dir[j];
	}

	for (i = 0; i < 15; i++) {

		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		VectorClear (p->accel);
		VectorClear (p->vel);
		VectorCopy (dir, p->dir);
		VectorNormalize (p->dir);
		p->orient = 0;
		p->flags = PARTICLE_ALIGNED;
		p->flags |= PARTICLE_AIRONLY;
		p->flags |= PARTICLE_OVERBRIGHT;
		p->flags |= PARTICLE_NOFADE;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE;

		p->color[0] = 1;
		p->color[1] = 0.3;
		p->color[2] = 0;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		p->alpha = 1;
		p->alphavel = -1.0 / (0.5 + frand () * 0.5);

		p->type = Particle::PT_WATERCIRCLE;
		p->size = 10 + frand () * 6;
		p->sizeVel = 20;
		p->len = 0;
		p->endLen = 0;

		for (j = 0; j < 3; j++) {
			p->origin[j] = origin[j];
			p->vel[j] = p->dir[j];
		}
	}


}


void CL_ParticleGibBlood (vec3_t origin) {
	int j, i;
	Particle* p;
	vec3_t end, tmp;
	float d;

	if (!cl_blood->value) return;

	VectorCopy (origin, end);
	end[2] += 150;
	VectorSubtract (end, origin, tmp);
	VectorNormalize (tmp);

	for (i = 0; i < 5; i++) {
		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;


		VectorClear (p->accel);

		p->time = cl.time;
		p->endTime = cl.time + 5000;
		p->flags = PARTICLE_DIRECTIONAL;
		p->flags |= PARTICLE_NONSOLID;
		p->orient = 0;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE_MINUS_SRC_ALPHA;
		p->type = Particle::PT_BLOODDRIP;
		p->size = 1;
		p->sizeVel = 0;
		p->alpha = 1;
		p->alphavel = 0;

		p->color[0] = 0.1;
		p->color[1] = 0;
		p->color[2] = 0;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;
		p->len = 7;
		p->endLen = 0;
		d = rand () & 5;
		for (j = 0; j < 3; j++) {
			p->origin[j] = origin[j] + ((rand () & 2) - 7) + d * tmp[j];
			p->vel[j] = tmp[j] * 150 + crand () * 190;
		}
		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = -PARTICLE_GRAVITY * 6.5;

		VectorCopy (p->origin, p->previous_origin);

	}

	for (i = 0; i < 4; i++) {
		if (!free_particles)
			return;
		p = free_particles;

		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		VectorClear (p->vel);
		p->orient = 0;
		p->flags = PARTICLE_DEFAULT;
		p->time = cl.time;
		p->endTime = cl.time + 5000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE_MINUS_SRC_ALPHA;

		p->color[0] = 0.1;
		p->color[1] = 0;
		p->color[2] = 0;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		p->type = Particle::PT_BLOODMIST;
		p->size = 10 + (1.5 * crand ());
		p->sizeVel = 25 - (0.5 * crand ());
		origin[2] -= 20;
		d = rand () & 7;
		for (j = 0; j < 3; j++)
			p->origin[j] = origin[j] + ((rand () & 7) - 4) + d;


		p->accel[0] = p->accel[1] = p->accel[2] = 0;
		p->alpha = 1;

		p->alphavel = -1.0 / (0.5 + frand () * 0.3);
	}

}


void CL_Explosion(vec3_t origin) {
	Particle* p;
	int i, j;
	float d;
	int cont;

	if (!free_particles)
		return;
	p = free_particles;
	free_particles = p->next;
	p->next = active_particles;
	active_particles = p;
	p->orient = 0;
	//	p->flags = PARTICLE_OVERBRIGHT;
	p->flags = PARTICLE_LIGHTING;
	p->time = cl.time;
	p->endTime = cl.time + 700;
	p->size = 50;
	p->sizeVel = 0;
	p->alpha = 1.0f;
	p->alphavel = -0.2f;

	p->lightradius = 350;
	p->lcolor[0] = 1.0;
	p->lcolor[1] = 0.5;
	p->lcolor[2] = 0.5;


	p->sFactor = GL_ONE;
	p->dFactor = GL_ONE;

	p->color[0] = 1;
	p->color[1] = 1;
	p->color[2] = 1;

	p->colorVel[0] = 0;
	p->colorVel[1] = 0;
	p->colorVel[2] = 0;
	p->type = Particle::PT_EXPLODE;

	VectorCopy(origin, p->origin);
	VectorClear(p->vel);
	VectorClear(p->accel);
	//Sparks
	for (i = 0; i < 99; i++)
	{
		if (!free_particles)
			return;

		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;

		cont = (CL_PMpointcontents(p->origin) & MASK_WATER);
		if (cont)
		{
			p->orient = 0;
			p->flags = PARTICLE_UNDERWATER;
			p->type = Particle::PT_BUBBLE;
			p->sFactor = GL_SRC_ALPHA;
			p->dFactor = GL_ONE_MINUS_SRC_ALPHA;
			p->size = 0.8;
			p->sizeVel = 0;
			p->color[0] = 1;
			p->color[1] = 1;
			p->color[2] = 1;

			p->colorVel[0] = 0;
			p->colorVel[1] = 0;
			p->colorVel[2] = 0;
		}
		else
		{
			p->flags = PARTICLE_DIRECTIONAL;
			p->orient = 0;
			p->type = Particle::PT_DEFAULT;

			p->sFactor = GL_SRC_ALPHA;
			p->dFactor = GL_ONE;
			p->size = 0.30;
			p->sizeVel = 0.1;
			p->len = 10;
			p->endLen = 30;
			p->color[0] = 1;
			p->color[1] = 1;
			p->color[2] = 0;

			p->colorVel[0] = -1;
			p->colorVel[1] = -2;
			p->colorVel[2] = 0;
		}
		p->time = cl.time;
		p->endTime = cl.time + 700;



		for (j = 0; j < 3; j++)
		{
			p->origin[j] = origin[j] + ((rand() % 32) - 16);
			p->vel[j] = (rand() % 384) - 192;
		}

		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = -PARTICLE_GRAVITY;
		p->alpha = 1.0;

		p->alphavel = -0.8 / (0.5 + frand() * 0.1);
	}


	//Smoke     
	for (i = 0; i < 4; i++)
	{
		cont = (CL_PMpointcontents(p->origin) & MASK_WATER);

		if (cont)
			return;

		if (!free_particles)
			return;

		p = free_particles;

		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		p->orient = frand() * 360;
		p->flags = PARTICLE_AIRONLY;
		p->flags |= PARTICLE_VERTEXLIGHT;
		p->time = cl.time;
		p->endTime = cl.time + 20000;

		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE_MINUS_SRC_ALPHA;

		p->color[0] = 0.35;
		p->color[1] = 0.35;
		p->color[2] = 0.35;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		p->type = Particle::PT_SMOKE;
		p->size = 3;
		p->sizeVel = 50;
		d = rand() & 7;

		for (j = 0; j < 3; j++)
		{
			p->origin[j] = origin[j];
			p->vel[j] = crand();
		}

		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = 20;
		p->alpha = 1.0;

		p->alphavel = -1.0 / (1.5 + frand() * 0.5);

	}
}


/*
===============
CL_TeleporterParticles
===============
*/
void CL_TeleporterParticles (entity_state_t * ent) {
	int i, j;
	Particle* p;

	for (i = 0; i < 8; i++) {
		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		p->orient = 0;
		p->flags = PARTICLE_DEFAULT;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE;

		p->color[0] = 0.7;
		p->color[1] = 0.0;
		p->color[2] = 0.5;

		p->colorVel[0] = -0.4;
		p->colorVel[1] = 1;
		p->colorVel[2] = -0.5;
		p->type = Particle::PT_DEFAULT;
		p->size = 1;
		p->sizeVel = 0;
		for (j = 0; j < 2; j++) {
			p->origin[j] = ent->origin[j] - 16 + (rand () & 31);
			p->vel[j] = crand () * 14;
		}

		p->origin[2] = ent->origin[2] - 8 + (rand () & 7);
		p->vel[2] = 80 + (rand () & 7);

		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = -PARTICLE_GRAVITY;
		p->alpha = 1.0;

		p->alphavel = -0.5;
	}
}


/*
===============
CL_LogoutEffect

===============
*/
void CL_LogoutEffect (vec3_t origin, int type) {
	int i, j;
	Particle* p;

	for (i = 0; i < 500; i++) {
		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		p->orient = 0;
		p->flags = PARTICLE_DEFAULT;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE_MINUS_SRC_ALPHA;

		if (type == MZ_LOGIN) {
			p->color[0] = 0;
			p->color[1] = 1;
			p->color[2] = 0;
		}


		else if (type == MZ_LOGOUT) {
			p->color[0] = 1;
			p->color[1] = 0;
			p->color[2] = 0;
		}
		else {
			p->color[0] = 1;
			p->color[1] = 1;
			p->color[2] = 0;
		}

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		p->type = Particle::PT_DEFAULT;
		p->size = 1;
		p->sizeVel = 0;
		p->origin[0] = origin[0] - 16 + frand () * 32;
		p->origin[1] = origin[1] - 16 + frand () * 32;
		p->origin[2] = origin[2] - 24 + frand () * 56;

		for (j = 0; j < 3; j++)
			p->vel[j] = crand () * 20;

		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = -PARTICLE_GRAVITY;
		p->alpha = 1.0;

		p->alphavel = -1.0 / (1.0 + frand () * 0.3);
	}
}


/*
===============
CL_ItemRespawnParticles

===============
*/
void CL_ItemRespawnParticles (vec3_t origin) {
	int i, j;
	Particle* p;

	for (i = 0; i < 15; i++) {
		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		p->orient = 0;
		p->flags = PARTICLE_DEFAULT;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE;

		p->color[0] = 0;
		p->color[1] = 1;
		p->color[2] = 0;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		p->origin[0] = origin[0] + crand () * 15;
		p->origin[1] = origin[1] + crand () * 15;
		p->origin[2] = origin[2] + crand () * 15;
		p->type = Particle::PT_SMOKE;
		p->size = 0.7;
		p->sizeVel = 80;
		for (j = 0; j < 3; j++)
			p->vel[j] = crand () * 15;

		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = PARTICLE_GRAVITY*1.2;
		p->alpha = 0.5;

		p->alphavel = -1.0 / (1.0 + frand () * 0.3);
	}
}


/*
===============
CL_ExplosionParticles
===============
*/
void CL_ExplosionParticles (vec3_t origin) {
	int i, j;
	Particle* p;


	for (i = 0; i < 256; i++) {
		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		p->orient = 0;
		p->flags = PARTICLE_OVERBRIGHT;
		p->type = Particle::PT_BLASTER;

		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE_MINUS_SRC_ALPHA;

		p->time = cl.time;
		p->endTime = cl.time + 500;
		p->color[0] = 1;
		p->color[1] = 1;
		p->color[2] = 0;

		p->colorVel[0] = -0.3;
		p->colorVel[1] = -1;
		p->colorVel[2] = 0;

		p->size = 0.7;
		p->sizeVel = 0.1;

		for (j = 0; j < 3; j++) {
			p->origin[j] = origin[j] + ((rand () % 32) - 16);
			p->vel[j] = (rand () % 384) - 192;
		}

		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = -PARTICLE_GRAVITY;
		p->alpha = 1.0;

		p->alphavel = -0.8 / (0.5 + frand () * 0.3);


	}

}


/*
===============
CL_BigTeleportParticles
===============
*/
void CL_BigTeleportParticles (vec3_t origin) {
	int i;
	Particle* p;
	float angle, dist, color;

	for (i = 0; i < 4096; i++) {
		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		p->orient = 0;
		p->flags = PARTICLE_OVERBRIGHT;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE_MINUS_SRC_ALPHA;

		color = 0.1 + (0.2 * frand ());

		p->color[0] = color;
		p->color[1] = color;
		p->color[2] = color;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		angle = M_PI * 2 * (rand () & 1023) / 1023.0;
		dist = rand () & 31;
		p->origin[0] = origin[0] + cos (angle) * dist;
		p->vel[0] = cos (angle) * (70 + (rand () & 63));
		p->accel[0] = -cos (angle) * 100;

		p->origin[1] = origin[1] + sin (angle) * dist;
		p->vel[1] = sin (angle) * (70 + (rand () & 63));
		p->accel[1] = -sin (angle) * 100;

		p->origin[2] = origin[2] + 8 + (rand () % 90);
		p->vel[2] = -100 + (rand () & 31);
		p->accel[2] = PARTICLE_GRAVITY * 4;
		p->alpha = 1.0;
		p->type = Particle::PT_DEFAULT;
		p->size = 1;
		p->sizeVel = 1;
		p->alphavel = -0.3 / (0.5 + frand () * 0.3);
	}
}


/**
 * Wall impact sparks for blaster / hyper blaster
 */
void CL_BlasterParticles(vec3_t origin, vec3_t dir)
{
	Particle* p = nullptr;
	float d = 0.0f;

	for (int i = 0; i < BLASTER_SPARK_PARTICLES; i++)
	{
		if (!free_particles)
			return;

		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		p->type = Particle::PT_BLASTER;
		p->orient = 0;
		p->flags = PARTICLE_BOUNCE;
		p->flags |= PARTICLE_FRICTION;

		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE;

		p->size = 1;
		p->sizeVel = 0;
		p->time = cl.time;
		p->endTime = cl.time + 20000;

		p->color[0] = 0.97;
		p->color[1] = 0.46;
		p->color[2] = 0.14;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		p->alpha = 1;
		p->alphavel = -0.25 / (0.3 + frand() * 0.2);

		p->len = 2;
		p->endLen = 60;

		d = rand() & 15;
		for (int j = 0; j < 3; j++)
		{
			p->origin[j] = origin[j] + ((rand() & 7) - 4) + d * dir[j];
			p->vel[j] = dir[j] * 30 + crand() * 80;
		}

		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = -PARTICLE_GRAVITY * 3.0f;

		VectorCopy(p->origin, p->previous_origin);
	}
}


/*
===============
CL_BlasterTrail

===============
*/
void CL_BlasterTrail (vec3_t start, vec3_t end) {
	vec3_t move;
	vec3_t vec;
	float len;
	int j;
	Particle* p;
	int dec;


	VectorCopy (start, move);
	VectorSubtract (end, start, vec);
	len = VectorNormalize (vec);

	dec = 2;
	VectorScale (vec, dec, vec);

	// FIXME: this is a really silly way to have a loop
	while (len > 0) {
		len -= dec;

		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		VectorClear (p->accel);
		p->orient = 0;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE;
		p->flags = PARTICLE_DEFAULT;
		p->alpha = 1.0;
		p->alphavel = -1.0 / (0.3 + frand () * 0.2);
		p->color[0] = 0.97;
		p->color[1] = 0.46;
		p->color[2] = 0.14;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		p->type = Particle::PT_BLASTER;
		p->size = 2.6 + (1.2 * crand ());
		p->sizeVel = -2.4 + (1.2 * crand ());

		for (j = 0; j < 3; j++) {
			p->origin[j] = move[j] + crand ();
			p->vel[j] = crand () * 5;
			p->accel[j] = 0;
		}

		VectorAdd (move, vec, move);
	}
}


/*
===============
CL_FlagTrail

===============
*/
void CL_FlagTrail (vec3_t start, vec3_t end, float r, float g, float b) {
	vec3_t move;
	vec3_t vec;
	float len;
	int j;
	Particle* p;
	int dec;

	VectorCopy (start, move);
	VectorSubtract (end, start, vec);
	len = VectorNormalize (vec);

	dec = 5;
	VectorScale (vec, 5, vec);

	while (len > 0) {
		len -= dec;

		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		p->orient = 0;
		p->flags = PARTICLE_DEFAULT;
		VectorClear (p->accel);
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE_MINUS_SRC_ALPHA;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->type = Particle::PT_DEFAULT;
		p->size = 1;
		p->sizeVel = 1;
		p->alpha = 1.0;
		p->alphavel = -1.0 / (0.8 + frand () * 0.2);
		p->color[0] = r;
		p->color[1] = g;
		p->color[2] = b;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;
		for (j = 0; j < 3; j++) {
			p->origin[j] = move[j] + crand () * 16;
			p->vel[j] = crand () * 5;
			p->accel[j] = 0;
		}

		VectorAdd (move, vec, move);
	}
}

/*
===============
CL_DiminishingTrail

===============
*/
void CL_DiminishingTrail (vec3_t start, vec3_t end, centity_t * old,
	int flags) {
	vec3_t move;
	vec3_t vec;
	float len;
	int j, i;
	Particle* p;
	float dec;
	float orgscale;
	float velscale;
	int cont;
	trace_t trace;

	VectorCopy (start, move);
	VectorSubtract (end, start, vec);
	len = VectorNormalize (vec);

	trace = CL_Trace (start, end, 8.0, MASK_SOLID);
	VectorNormalize (trace.plane.normal);
	dec = 20;

	VectorScale (vec, dec, vec);

	if (old->trailcount > 900) {
		orgscale = 4;
		velscale = 15;
	}
	else if (old->trailcount > 800) {
		orgscale = 2;
		velscale = 10;
	}
	else {
		orgscale = 1;
		velscale = 5;
	}

	while (len > 0) {
		len -= dec;

		if (!free_particles)
			return;

		// drop less particles as it flies
		if ((rand () & 1023) < old->trailcount) {
			p = free_particles;
			free_particles = p->next;
			p->next = active_particles;
			active_particles = p;
			p->orient = frand () * 360;
			p->flags = PARTICLE_DEFAULT;
			VectorClear (p->accel);

			p->time = cl.time;
			p->endTime = cl.time + 20000;
			if (flags & EF_GIB) {
				p->orient = frand () * 360;
				p->alpha = 1;
				p->alphavel = -3.0 / (1 + frand () * 1.7);
				if (!cl_blood->value)
					p->alpha = 0;
				p->color[0] = 0.1;
				p->color[1] = 0.0;
				p->color[2] = 0.0;
				p->sFactor = GL_SRC_ALPHA;
				p->dFactor = GL_ONE_MINUS_SRC_ALPHA;

				p->colorVel[0] = 0;
				p->colorVel[1] = 0;
				p->colorVel[2] = 0;
				p->type = Particle::PT_BLOODMIST;
				p->size = 5 / (0.5 + frand () * 0.7);
				p->sizeVel = -5;

				for (j = 0; j < 3; j++) {
					p->origin[j] = move[j] + crand () * orgscale;
					p->vel[j] = crand () * velscale;
					p->accel[j] = 0;

				}
				p->vel[2] -= PARTICLE_GRAVITY;
				VectorCopy (p->origin, p->previous_origin);

				p->alpha = 0;
				if (cl_blood->value) {

					if (trace.fraction != 1.0) {

						for (i = 0; i < 1; i++)
							CL_AddDecalToScene (trace.endpos, trace.plane.normal,
							1, 1, 1, 1,
							1, 1, 1, 1,
							20, 12000,
							DECAL_BLOOD9, 0, frand () * 360,
							GL_ZERO, GL_ONE_MINUS_SRC_COLOR);
						VectorClear (trace.plane.normal);
						return;
					}
				}
			}
			else if (flags & EF_GREENGIB) {

				p->alpha = 0.55;
				p->alphavel = -1.0 / (1 + frand () * 1.7);

				if (!cl_blood->value)
					p->alpha = 0;

				p->sFactor = GL_SRC_ALPHA;
				p->dFactor = GL_ONE;
				p->color[0] = 1;
				p->color[1] = 1;
				p->color[2] = 0;

				p->colorVel[0] = 0;
				p->colorVel[1] = 0;
				p->colorVel[2] = 0;

				p->size = 10;
				p->sizeVel = -5;

				for (j = 0; j < 3; j++) {
					p->origin[j] = move[j] + crand () * orgscale;
					p->vel[j] = crand () * velscale;
					p->accel[j] = 0;
					p->type = Particle::PT_BLOOD2;

				}
				p->vel[2] -= PARTICLE_GRAVITY;

				if (cl_blood->value) {

					if (trace.fraction > 0 && trace.fraction < 1) {
						CL_AddDecalToScene (trace.endpos, trace.plane.normal,
							1, 1, 0, 1,
							1, 1, 0, 1,
							25, 12000,
							DECAL_ACIDMARK, DF_OVERBRIGHT,
							frand () * 360, GL_ONE, GL_ONE);
						CL_ParticleSmoke2 (trace.endpos, trace.plane.normal, 1,
							1, 0, 8, true);
						VectorClear (trace.plane.normal);
						p->alpha = 0;
						return;
					}
				}
			}
			else {
				p->alpha = 1.0;
				p->alphavel = -1.0 / (1 + frand () * 0.2);
				p->color[0] = 0.5;
				p->color[1] = 0.5;
				p->color[2] = 0.5;
				p->sFactor = GL_SRC_ALPHA;
				p->dFactor = GL_ONE_MINUS_SRC_ALPHA;
				p->colorVel[0] = 0;
				p->colorVel[1] = 0;
				p->colorVel[2] = 0;
				for (j = 0; j < 3; j++) {
					p->origin[j] = move[j] + crand () * orgscale;
					p->vel[j] = crand () * velscale;
				}
				p->accel[2] = 20;

				cont = (CL_PMpointcontents (p->origin) & MASK_WATER);
				if (cont)
				{
					p->flags = PARTICLE_UNDERWATER;
					p->type = Particle::PT_BUBBLE;
					p->size = 0.65;
					p->sizeVel = 0;
				}
				else
				{
					p->flags = PARTICLE_VERTEXLIGHT;
					p->type = Particle::PT_SMOKE;
					p->size = 2;
					p->sizeVel = 12;
				}
			}
		}

		old->trailcount -= 5;
		if (old->trailcount < 100)
			old->trailcount = 100;
		VectorAdd (move, vec, move);
	}
}


void CL_GunFire (vec3_t start, vec3_t end) {
	vec3_t move;
	vec3_t vec;
	float len;
	int j;
	Particle* p;
	int dec;


	VectorCopy (start, move);
	VectorSubtract (end, start, vec);
	len = VectorNormalize (vec);

	dec = 1;
	VectorScale (vec, dec, vec);

	// FIXME: this is a really silly way to have a loop
	while (len > 0) {
		len -= dec;

		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		VectorClear (p->accel);
		p->orient = 0;
		p->flags = PARTICLE_DEFAULT;

		p->time = cl.time;
		p->endTime = cl.time + 14000;
		p->sFactor = GL_ONE;
		p->dFactor = GL_ONE;
		p->alpha = 1.0;
		p->alphavel = -2.0 / (0.2 + frand () * 0.1);

		p->color[0] = 1;
		p->color[1] = 1;
		p->color[2] = 1;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		p->type = Particle::PT_FLAME;
		p->size = 4;
		p->sizeVel = -35;

		for (j = 0; j < 3; j++) {
			p->origin[j] = move[j] + crand ();
			p->accel[j] = 0;
			p->vel[j] = crand () * 5;
		}

		VectorAdd (move, vec, move);

	}

}

/*
===============
CL_RocketTrail

===============
*/

void CL_RocketFire (vec3_t start, vec3_t end) {
	vec3_t move;
	vec3_t vec;
	float len;
	int j;
	Particle* p;
	int dec, cont;


	VectorCopy (start, move);
	VectorSubtract (end, start, vec);
	len = VectorNormalize (vec);

	dec = 1;
	VectorScale (vec, dec, vec);

	// FIXME: this is a really silly way to have a loop
	while (len > 0) {
		len -= dec;

		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		VectorClear (p->accel);
		p->orient = 0;
		p->flags = PARTICLE_DEFAULT;

		p->time = cl.time;
		p->endTime = cl.time + 14000;
		p->sFactor = GL_ONE;
		p->dFactor = GL_ONE;
		p->alpha = 1.0;
		p->alphavel = -2.0 / (0.2 + frand () * 0.1);

		p->color[0] = 1;
		p->color[1] = 1;
		p->color[2] = 1;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		p->type = Particle::PT_FLAME;
		p->size = 2;
		p->sizeVel = -35;

		for (j = 0; j < 3; j++) {
			p->origin[j] = move[j] + crand ();
			p->accel[j] = 0;
			p->vel[j] = crand () * 5;
		}

		cont = (CL_PMpointcontents (p->origin) & MASK_WATER);


		if (cont) {
			CL_BubbleTrail (start, end);
		}

		VectorAdd (move, vec, move);

	}

}
void CL_RocketTrail (vec3_t start, vec3_t end, centity_t * old) {
	vec3_t move;
	vec3_t vec;
	float len;
	int j;
	Particle* p;
	float dec;

	VectorCopy (start, move);
	VectorSubtract (end, start, vec);
	len = VectorNormalize (vec);

	dec = 0.3;
	VectorScale (vec, dec, vec);

	while (len > 0) {
		len -= dec;

		if (!free_particles)
			return;

		if ((rand () & 7) == 0) {
			p = free_particles;
			free_particles = p->next;
			p->next = active_particles;
			active_particles = p;


			p->sFactor = GL_SRC_ALPHA;
			p->dFactor = GL_ONE_MINUS_SRC_ALPHA;
			p->flags = PARTICLE_VERTEXLIGHT;
			p->type = Particle::PT_SMOKE;
			p->orient = frand () * 360;
			p->size = 2;
			p->sizeVel = 40;
			VectorClear (p->accel);
			p->time = cl.time;
			p->endTime = cl.time + 20000;
			p->alpha = 0.5;
			p->alphavel = -1.0 / (0.1 + frand () * 0.9);
			p->color[0] = 0.4;
			p->color[1] = 0.4;
			p->color[2] = 0.4;

			p->colorVel[0] = 0.15;
			p->colorVel[1] = 0.15;
			p->colorVel[2] = 0.15;
			for (j = 0; j < 3; j++) {
				p->origin[j] = move[j] + crand ();
				p->vel[j] = crand ();
			}

			p->accel[0] = p->accel[1] = 0;
			p->accel[2] = 0;

		}
		VectorAdd (move, vec, move);
	}
}

/*
===============
CL_RailTrail

===============
*/

void CL_RailTrail (vec3_t start, vec3_t end) {
	vec3_t vec;
	int j;
	Particle* p;

	VectorSubtract (end, start, vec);

	if (!free_particles)
		return;

	p = free_particles;
	free_particles = p->next;
	p->next = active_particles;
	active_particles = p;
	p->orient = 0;
	p->flags = PARTICLE_SPIRAL;
	p->flags |= PARTICLE_OVERBRIGHT;
	p->time = cl.time;
	p->endTime = cl.time + 20000;
	VectorClear (p->accel);
	p->sFactor = GL_SRC_ALPHA;
	p->dFactor = GL_ONE_MINUS_SRC_ALPHA;
	p->alpha = 1.0;
	p->alphavel = -0.50 / (0.3 + frand () * 0.3);
	p->color[0] = cl_railspiral_red->value;
	p->color[1] = cl_railspiral_green->value;
	p->color[2] = cl_railspiral_blue->value;

	p->colorVel[0] = -0.5;
	p->colorVel[1] = -0.75;
	p->colorVel[2] = -1.0;
	p->type = Particle::PT_BLASTER;
	p->size = 2.8;
	p->sizeVel = 1;

	for (j = 0; j < 3; j++) {
		p->origin[j] = start[j];
		p->length[j] = vec[j];
		p->vel[j] = 0;
		p->accel[j] = 0;
	}


	if (!free_particles)
		return;

	p = free_particles;
	free_particles = p->next;
	p->next = active_particles;
	active_particles = p;

	p->time = cl.time;
	p->endTime = cl.time + 20000;
	VectorClear (p->accel);
	p->orient = 0;
	p->flags = PARTICLE_STRETCH;
	p->flags |= PARTICLE_OVERBRIGHT;
	p->alpha = 1.0;
	p->alphavel = -0.5 / (0.3 + frand () * 0.3);
	p->sFactor = GL_SRC_ALPHA;
	p->dFactor = GL_ONE_MINUS_SRC_ALPHA;
	p->color[0] = cl_railcore_red->value;
	p->color[1] = cl_railcore_green->value;
	p->color[2] = cl_railcore_blue->value;

	p->colorVel[0] = -0.5;
	p->colorVel[1] = -0.75;
	p->colorVel[2] = -1.0;
	p->type = Particle::PT_BLASTER;
	p->size = 3;
	p->sizeVel = 1;

	for (j = 0; j < 3; j++) {
		p->origin[j] = start[j];
		p->length[j] = vec[j];
		p->vel[j] = 0;
		p->accel[j] = 0;
	}

}

void CL_ParticleRick (vec3_t origin, vec3_t dir) {
	float d;
	int j, i;
	Particle* p;
	VectorNormalize (dir);

	for (i = 0; i < 24; i++) {

		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		VectorClear (p->accel);
		VectorClear (p->vel);
		p->orient = 0;
		p->flags = PARTICLE_DIRECTIONAL | PARTICLE_AIRONLY;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE;
		p->type = Particle::PT_BLASTER;
		p->size = 0.7;
		p->sizeVel = 0;
		p->alpha = 0.3;
		p->alphavel = -1.0 / (0.3 + frand () * 0.2);

		p->color[0] = 1;
		p->color[1] = 0.7;
		p->color[2] = 0;

		p->colorVel[0] = 0;
		p->colorVel[1] = -1;
		p->colorVel[2] = 0;
		p->len = 11;
		p->endLen = 36;
		d = rand () & 5;
		for (j = 0; j < 3; j++) {
			p->origin[j] = origin[j] + d * dir[j];
			p->vel[j] = dir[j] * 30 + crand () * 10;

		}
		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = -PARTICLE_GRAVITY;
	}
	
	//SMOKE
	for (i = 0; i < 6; i++) {
		if (!free_particles)
			return;
		p = free_particles;

		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		p->orient = frand () * 360;
		p->flags = PARTICLE_AIRONLY | PARTICLE_VERTEXLIGHT;
		p->time = cl.time;
		p->endTime = cl.time + 20000;

		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE_MINUS_SRC_ALPHA;

		p->color[0] = 0.1;
		p->color[1] = 0.1;
		p->color[2] = 0.1;

		p->colorVel[0] = 0.9;
		p->colorVel[1] = 0.9;
		p->colorVel[2] = 0.9;
		p->alpha = 1;
		p->alphavel = -1.5 / (0.5 + frand () * 0.5);

		p->type = Particle::PT_SMOKE;
		p->size = 5;
		p->sizeVel = 20;

		d = rand () & 15;
		for (j = 0; j < 3; j++) {
			p->origin[j] = origin[j] + ((rand () & 7) - 4) + d * dir[j];
			p->vel[j] = dir[j] * 30;
		}

		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = 10;


	}
	// Wall Aligned	Smoke Puff

	if (!free_particles)
		return;
	p = free_particles;
	free_particles = p->next;
	p->next = active_particles;
	active_particles = p;
	VectorClear (p->accel);
	VectorClear (p->vel);
	VectorCopy (dir, p->dir);
	VectorNormalize (p->dir);
	p->orient = 0;
	p->flags = PARTICLE_ALIGNED;
	p->flags |= PARTICLE_AIRONLY;
	p->flags |= PARTICLE_NOFADE;
	p->time = cl.time;
	p->endTime = cl.time + 20000;
	p->sFactor = GL_SRC_ALPHA;
	p->dFactor = GL_ONE_MINUS_SRC_ALPHA;

	p->color[0] = 0.1;
	p->color[1] = 0.1;
	p->color[2] = 0.1;

	p->colorVel[0] = 0.1;
	p->colorVel[1] = 0.1;
	p->colorVel[2] = 0.1;
	p->alpha = 1.0;
	p->alphavel = -1.0 / (0.5 + frand () * 0.5);

	p->type = Particle::PT_SMOKE;
	p->size = 5;
	p->sizeVel = 15;

	p->len = 0;
	p->endLen = 0;

	for (j = 0; j < 3; j++) {
		p->origin[j] = origin[j];
		p->vel[j] = p->dir[j] * 2;
	}

}

void CL_ParticleRailRick (vec3_t origin, vec3_t dir) {

	float d;
	int j, i;
	Particle* p;
	VectorNormalize (dir);

	for (i = 0; i < 25; i++) {

		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		VectorClear (p->accel);
		VectorClear (p->vel);
		p->orient = 0;
		p->flags = PARTICLE_DIRECTIONAL;
		p->flags |= PARTICLE_AIRONLY;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE;
		p->type = Particle::PT_BLASTER;
		p->size = 0.6;
		p->sizeVel = 0;
		p->alpha = 0.7;
		p->alphavel = -1.0 / (0.3 + frand () * 0.2);

		p->color[0] = 1;
		p->color[1] = 0.7;
		p->color[2] = 0;

		p->colorVel[0] = 0;
		p->colorVel[1] = -1;
		p->colorVel[2] = 0;
		p->len = 20;
		p->endLen = 200;
		d = rand () & 5;
		for (j = 0; j < 3; j++) {
			p->origin[j] = origin[j] + d * dir[j];
			p->vel[j] = dir[j] * 30 + crand () * 10;

		}
		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = -PARTICLE_GRAVITY;
	}
	//SMOKE
	for (i = 0; i < 11; i++) {
		if (!free_particles)
			return;
		p = free_particles;

		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		p->orient = frand () * 360;
		p->flags = PARTICLE_AIRONLY | PARTICLE_VERTEXLIGHT;
		p->time = cl.time;
		p->endTime = cl.time + 20000;

		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE_MINUS_SRC_ALPHA;

		p->color[0] = 0.1;
		p->color[1] = 0.1;
		p->color[2] = 0.1;

		p->colorVel[0] = 0.2;
		p->colorVel[1] = 0.2;
		p->colorVel[2] = 0.2;
		p->alpha = 1;
		p->alphavel = -1.0 / (0.5 + frand () * 0.5);

		p->type = Particle::PT_SMOKE;
		p->size = 5;
		p->sizeVel = 30;

		d = rand () & 15;
		for (j = 0; j < 3; j++) {
			p->origin[j] = origin[j] + ((rand () & 7) - 4) + d * dir[j];
			p->vel[j] = dir[j] * 30;
		}

		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = 10;


	}

	if (!free_particles)
		return;
	p = free_particles;
	free_particles = p->next;
	p->next = active_particles;
	active_particles = p;
	VectorClear (p->accel);
	VectorClear (p->vel);
	VectorCopy (dir, p->dir);
	VectorNormalize (p->dir);
	p->orient = 0;
	p->flags = PARTICLE_ALIGNED | PARTICLE_AIRONLY | PARTICLE_NOFADE | PARTICLE_VERTEXLIGHT;
	p->time = cl.time;
	p->endTime = cl.time + 20000;
	p->sFactor = GL_SRC_ALPHA;
	p->dFactor = GL_ONE_MINUS_SRC_ALPHA;

	p->color[0] = 0.1;
	p->color[1] = 0.1;
	p->color[2] = 0.1;

	p->colorVel[0] = 0.2;
	p->colorVel[1] = 0.2;
	p->colorVel[2] = 0.2;
	p->alpha = 0.7;
	p->alphavel = -1.0 / (0.5 + frand () * 0.5);

	p->type = Particle::PT_SMOKE;
	p->size = 5;
	p->sizeVel = 40;

	p->len = 0;
	p->endLen = 0;

	for (j = 0; j < 3; j++) {
		p->origin[j] = origin[j];
		p->vel[j] = p->dir[j] * 2;
	}

	for (i = 0; i < 35; i++) {
		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		p->type = Particle::PT_BLASTER;
		p->orient = 0;
		p->flags = PARTICLE_BOUNCE;
		p->flags |= PARTICLE_FRICTION;
		p->flags |= PARTICLE_DIRECTIONAL;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE;

		p->size = 0.5;
		p->sizeVel = 0;
		p->time = cl.time;
		p->endTime = cl.time + 20000;

		p->color[0] = 0.97;
		p->color[1] = 0.46;
		p->color[2] = 0.14;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		p->alpha = 1;
		p->alphavel = -0.25 / (0.3 + frand () * 0.2);

		p->len = 1;
		p->endLen = 130;

		d = rand () & 15;
		for (j = 0; j < 3; j++) {
			p->origin[j] = origin[j] + ((rand () & 7) - 4) + d*dir[j];
			p->vel[j] = dir[j] * 30 + crand () * 40;
		}

		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = -PARTICLE_GRAVITY*1.5;

		VectorCopy (p->origin, p->previous_origin);
	}


}





// RAFAEL
/*
===============
CL_IonripperTrail
===============
*/
void CL_IonripperTrail (vec3_t start, vec3_t ent) {
	vec3_t move;
	vec3_t vec;
	float len;
	int j;
	Particle* p;
	int dec;
	int left = 0;

	VectorCopy (start, move);
	VectorSubtract (ent, start, vec);
	len = VectorNormalize (vec);

	dec = 5;
	VectorScale (vec, 5, vec);

	while (len > 0) {
		len -= dec;

		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		VectorClear (p->accel);
		p->orient = 0;
		p->flags = PARTICLE_OVERBRIGHT;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE;
		p->type = Particle::PT_BLASTER;
		p->size = 1.0;
		p->sizeVel = 1.0;
		p->alpha = 0.5;
		p->alphavel = -1.0 / (0.3 + frand () * 0.2);

		p->color[0] = 0.8;
		p->color[1] = 0.4;
		p->color[2] = 0;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		for (j = 0; j < 3; j++) {
			p->origin[j] = move[j];
			p->accel[j] = 0;
		}
		if (left) {
			left = 0;
			p->vel[0] = 10;
		}
		else {
			left = 1;
			p->vel[0] = -10;
		}

		p->vel[1] = 0;
		p->vel[2] = 0;

		VectorAdd (move, vec, move);
	}
}


/*
===============
CL_BubbleTrail

===============
*/
void CL_BubbleTrail (vec3_t start, vec3_t end) {
	vec3_t move;
	vec3_t vec;
	float len;
	int i, j;
	Particle* p;
	float dec;

	VectorCopy (start, move);
	VectorSubtract (end, start, vec);
	len = VectorNormalize (vec);

	dec = 16;
	VectorScale (vec, dec, vec);

	for (i = 0; i < len; i += dec) {

		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		VectorClear (p->accel);
		p->flags = PARTICLE_UNDERWATER;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE_MINUS_SRC_ALPHA;
		p->alpha = 1.0;
		p->alphavel = -1.0 / (1 + frand () * 0.2);
		p->color[0] = 1;
		p->color[1] = 1;
		p->color[2] = 1;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;
		p->type = Particle::PT_BUBBLE;
		p->size = 0.35;
		p->sizeVel = 0;

		for (j = 0; j < 3; j++) {
			p->origin[j] = move[j] + crand () * 2;
			p->vel[j] = crand () * 5;
		}
		p->vel[2] += 6;

		p->accel[0] = p->accel[1] = p->accel[2] = 0;
		VectorAdd (move, vec, move);

	}
}


/*
===============
CL_FlyParticles
===============
*/

#define	BEAMLENGTH			16
void CL_FlyParticles (vec3_t origin, int count) {
	int i;
	Particle* p;
	float angle;
	float sr, sp, sy, cr, cp, cy;
	vec3_t forward;
	float dist = 64;
	float ltime;


	if (count > NUM_VERTEX_NORMALS)
		count = NUM_VERTEX_NORMALS;

	if (!avelocities[0][0]) {
		for (i = 0; i < NUM_VERTEX_NORMALS; i++) {
			avelocities[i][0] = (rand () & 255) * 0.01;
			avelocities[i][1] = (rand () & 255) * 0.01;
			avelocities[i][2] = (rand () & 255) * 0.01;
		}
	}



	ltime = (float)cl.time / 1000.0;
	for (i = 0; i < count; i += 2) {
		angle = ltime * avelocities[i][0];
		sy = sin (angle);
		cy = cos (angle);
		angle = ltime * avelocities[i][1];
		sp = sin (angle);
		cp = cos (angle);
		angle = ltime * avelocities[i][2];
		sr = sin (angle);
		cr = cos (angle);

		forward[0] = cp * cy;
		forward[1] = cp * sy;
		forward[2] = -sp;

		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		p->flags = PARTICLE_DEFAULT;
		p->orient = 0;
		p->time = cl.time;
		p->endTime = cl.time + 60000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE_MINUS_SRC_ALPHA;
		dist = sin (ltime + i) * 64;
		p->origin[0] =
			origin[0] + q_byteDirs[i][0] * dist + forward[0] * BEAMLENGTH;
		p->origin[1] =
			origin[1] + q_byteDirs[i][1] * dist + forward[1] * BEAMLENGTH;
		p->origin[2] =
			origin[2] + q_byteDirs[i][2] * dist + forward[2] * BEAMLENGTH;

		VectorClear (p->vel);
		VectorClear (p->accel);

		p->color[0] = 0;
		p->color[1] = 0;
		p->color[2] = 0;

		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;
		p->size = 0.75;
		p->sizeVel = 0;

		p->alpha = 1;
		p->alphavel = -100;
		p->type = Particle::PT_FLY;

	}
}


void CL_FlyEffect (centity_t * ent, vec3_t origin) {
	int n;
	int count;
	int starttime;

	if (ent->fly_stoptime < cl.time) {
		starttime = cl.time;
		ent->fly_stoptime = cl.time + 60000;
	}
	else {
		starttime = ent->fly_stoptime - 60000;
	}

	n = cl.time - starttime;
	if (n < 20000)
		count = n * 162 / 20000.0;
	else {
		n = ent->fly_stoptime - cl.time;
		if (n < 20000)
			count = n * 162 / 20000.0;
		else
			count = 162;
	}

	CL_FlyParticles (origin, count);
}


/*
===============
CL_BfgParticles
===============
*/

#define	BEAMLENGTH			16
void CL_BfgParticles (entity_t * ent) {
	int i;
	Particle* p;
	float angle;
	float sr, sp, sy, cr, cp, cy;
	vec3_t forward;
	float dist = 64;
	vec3_t v;
	float ltime;

	if (!avelocities[0][0]) {
		for (i = 0; i < NUM_VERTEX_NORMALS; i++) {
			avelocities[i][0] = (rand () & 255) * 0.01;
			avelocities[i][1] = (rand () & 255) * 0.01;
			avelocities[i][2] = (rand () & 255) * 0.01;
		}
	}


	ltime = (float)cl.time / 1000.0;
	for (i = 0; i < NUM_VERTEX_NORMALS; i++) {
		angle = ltime * avelocities[i][0];
		sy = sin (angle);
		cy = cos (angle);
		angle = ltime * avelocities[i][1];
		sp = sin (angle);
		cp = cos (angle);
		angle = ltime * avelocities[i][2];
		sr = sin (angle);
		cr = cos (angle);

		forward[0] = cp * cy;
		forward[1] = cp * sy;
		forward[2] = -sp;

		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		p->orient = 0;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE_MINUS_SRC_ALPHA;
		p->flags = PARTICLE_OVERBRIGHT;
		p->type = Particle::PT_BLASTER;
		p->size = 0.7;
		p->sizeVel = 2;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		dist = sin (ltime + i) * 64;
		p->origin[0] =
			ent->origin[0] + q_byteDirs[i][0] * dist +
			forward[0] * BEAMLENGTH;
		p->origin[1] =
			ent->origin[1] + q_byteDirs[i][1] * dist +
			forward[1] * BEAMLENGTH;
		p->origin[2] =
			ent->origin[2] + q_byteDirs[i][2] * dist +
			forward[2] * BEAMLENGTH;

		VectorClear (p->vel);
		VectorClear (p->accel);

		VectorSubtract (p->origin, ent->origin, v);
		dist = VectorLength (v) / 90.0;
		p->color[0] = 0.24;
		p->color[1] = 0.82;
		p->color[2] = 0.10;
		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		p->alpha = 1.0 - dist;
		p->alphavel = -100;

	}
}

/*
===============
CL_TrapParticles
===============
*/
// RAFAEL
void CL_TrapParticles (entity_t * ent) {
	vec3_t move;
	vec3_t vec;
	vec3_t start, end;
	float len;
	int j;
	Particle* p;
	int dec;

	ent->origin[2] -= 14;
	VectorCopy (ent->origin, start);
	VectorCopy (ent->origin, end);
	end[2] += 20;

	VectorCopy (start, move);
	VectorSubtract (end, start, vec);
	len = VectorNormalize (vec);

	dec = 1;
	VectorScale (vec, 5, vec);

	// FIXME: this is a really silly way to have a loop
	while (len > 0) {
		len -= dec;

		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;
		VectorClear (p->accel);
		p->orient = 0;
		p->flags = PARTICLE_DEFAULT;
		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE;
		p->type = Particle::PT_BLASTER;
		p->size = 6;
		p->sizeVel = -10;
		p->alpha = 1.0;
		p->alphavel = -1.0 / (0.3 + frand () * 0.2);
		p->color[0] = 1;
		p->color[1] = 0.67;
		p->color[2] = 0.4;
		p->colorVel[0] = 0;
		p->colorVel[1] = -3;
		p->colorVel[2] = -15;
		for (j = 0; j < 3; j++) {
			p->origin[j] = move[j] + crand ();
			p->vel[j] = crand () * 15;
			p->accel[j] = 0;
		}
		p->accel[2] = PARTICLE_GRAVITY;

		VectorAdd (move, vec, move);
	}

	{


		int i, j, k;
		Particle* p;
		float vel;
		vec3_t dir;
		vec3_t origin;


		ent->origin[2] += 14;
		VectorCopy (ent->origin, origin);


		for (i = -2; i <= 2; i += 4)
		for (j = -2; j <= 2; j += 4)
		for (k = -2; k <= 4; k += 4) {
			if (!free_particles)
				return;
			p = free_particles;
			free_particles = p->next;
			p->next = active_particles;
			active_particles = p;
			p->orient = 0;
			p->flags = PARTICLE_DEFAULT;
			p->time = cl.time;
			p->endTime = cl.time + 20000;
			p->sFactor = GL_SRC_ALPHA;
			p->dFactor = GL_ONE;
			p->color[0] = 1;
			p->color[1] = 0.5;
			p->color[2] = 0;
			p->colorVel[0] = 0;
			p->colorVel[1] = -0.5;
			p->colorVel[2] = 0;
			p->type = Particle::PT_BLASTER;
			p->size = 1;
			p->sizeVel = -2;
			p->alpha = 1.0;
			p->alphavel = -1.0 / (0.3 + (rand () & 7) * 0.02);

			p->origin[0] = origin[0] + i + ((rand () & 23) * crand ());
			p->origin[1] = origin[1] + j + ((rand () & 23) * crand ());
			p->origin[2] = origin[2] + k + ((rand () & 23) * crand ());

			dir[0] = j * 8;
			dir[1] = i * 8;
			dir[2] = k * 8;

			VectorNormalize (dir);
			vel = 50 + (rand () & 63);
			VectorScale (dir, vel, p->vel);

			p->accel[0] = p->accel[1] = 0;
			p->accel[2] = -PARTICLE_GRAVITY;
		}
	}
}


/*
===============
CL_BFGExplosionParticles
===============
*/
//FIXME combined with CL_ExplosionParticles
void CL_BFGExplosionParticles (vec3_t origin) {
	int i, j;
	Particle* p;

	for (i = 0; i < 256; i++) {
		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;

		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE_MINUS_SRC_ALPHA;
		p->flags = PARTICLE_OVERBRIGHT;
		p->flags |= PARTICLE_DIRECTIONAL;
		p->orient = 0;
		p->type = Particle::PT_BLASTER;
		p->size = 0.30;
		p->sizeVel = 0;
		p->color[0] = 0.24;
		p->color[1] = 0.82;
		p->color[2] = 0.10;
		p->colorVel[0] = -1;
		p->colorVel[1] = 1;
		p->colorVel[2] = -1;
		p->len = 10;
		p->endLen = 30;

		for (j = 0; j < 3; j++) {
			p->origin[j] = origin[j] + ((rand () % 32) - 16);
			p->vel[j] = (rand () % 384) - 192;
		}

		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = -PARTICLE_GRAVITY;
		p->alpha = 1.0;
		p->alphavel = -0.8 / (0.5 + frand () * 0.3);
	}
}


/*
===============
CL_TeleportParticles

===============
*/
void CL_TeleportParticles (vec3_t origin) {
	int i, j, k;
	Particle* p;
	float vel, color;
	vec3_t dir;

	color = 0.1 + (0.2 * frand ());
	for (i = -16; i <= 16; i += 4)
	for (j = -16; j <= 16; j += 4)
	for (k = -16; k <= 32; k += 4) {
		if (!free_particles)
			return;
		p = free_particles;
		free_particles = p->next;
		p->next = active_particles;
		active_particles = p;

		p->time = cl.time;
		p->endTime = cl.time + 20000;
		p->sFactor = GL_SRC_ALPHA;
		p->dFactor = GL_ONE;
		p->orient = 0;
		p->flags = PARTICLE_DEFAULT;
		p->color[0] = color;
		p->color[1] = color;
		p->color[2] = color;
		p->colorVel[0] = 0;
		p->colorVel[1] = 0;
		p->colorVel[2] = 0;

		p->alpha = 1.0;
		p->alphavel = -1.0 / (0.3 + (rand () & 7) * 0.02);

		p->origin[0] = origin[0] + i + (rand () & 3);
		p->origin[1] = origin[1] + j + (rand () & 3);
		p->origin[2] = origin[2] + k + (rand () & 3);

		dir[0] = j * 8;
		dir[1] = i * 8;
		dir[2] = k * 8;

		VectorNormalize (dir);
		vel = 50 + (rand () & 63);
		VectorScale (dir, vel, p->vel);
		p->type = Particle::PT_DEFAULT;
		p->size = 1;
		p->sizeVel = 1;
		p->accel[0] = p->accel[1] = 0;
		p->accel[2] = -PARTICLE_GRAVITY;
	}
}


/* BFG LASERS
*/
void CL_AddLasers()
{
	laser_t *l;
	int i;


	for (i = 0, l = cl_lasers; i < MAX_LASERS; i++, l++)
	{
		if (l->endtime >= cl.time)
		{
			Particle* p;
			if (!free_particles)
				return;

			p = free_particles;
			free_particles = p->next;
			p->next = active_particles;
			active_particles = p;
			p->orient = 0;
			p->flags = PARTICLE_OVERBRIGHT;
			p->flags |= PARTICLE_STRETCH;
			p->time = cl.time;
			p->endTime = cl.time + 20000;
			p->sFactor = GL_SRC_ALPHA;
			p->dFactor = GL_ONE;
			VectorClear(p->accel);
			VectorClear(p->vel);
			p->alpha = 0.3;
			p->alphavel = INSTANT_PARTICLE;

			/*
			p->color[0] = cl_indexPalette[index][0];
			p->color[1] = cl_indexPalette[index][1];
			p->color[2] = cl_indexPalette[index][2];
			*/
			/// \fixme	This may not be necessary as the BFG will eventually be removed anyway.
			p->color[0] = 50;
			p->color[1] = 200;
			p->color[2] = 20;

			p->colorVel[0] = 0;
			p->colorVel[1] = 0;
			p->colorVel[2] = 0;

			p->type = Particle::PT_BLASTER;
			p->size = 3;
			p->sizeVel = 3;
			VectorCopy(l->ent.origin, p->origin);
			VectorSubtract(l->ent.oldorigin, l->ent.origin, p->length);
		}
	}
}
