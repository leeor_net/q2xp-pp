#pragma once

#include "../common/math.h"


/// \fixme Make me an enumeration.
/// \fixme Move into Particle class.
#define PARTICLE_BOUNCE					1
#define PARTICLE_FRICTION				2
#define PARTICLE_DIRECTIONAL			4
#define PARTICLE_VERTEXLIGHT			8
#define PARTICLE_STRETCH				16
#define PARTICLE_UNDERWATER				32
#define PARTICLE_OVERBRIGHT				64
#define PARTICLE_SPIRAL					128
#define PARTICLE_AIRONLY				256
#define PARTICLE_LIGHTING				512
#define PARTICLE_ALIGNED				1024
#define PARTICLE_NONSOLID				2048
#define PARTICLE_STOPED					4096
#define PARTICLE_CLAMP					8192
#define PARTICLE_NOFADE					16384
#define PARTICLE_DEFAULT				32768


/**
 * Particle
 */
class Particle
{
public:
	/**
	 * 
	 */
	enum ParticleType
	{
		PT_DEFAULT,			/**<  */
		PT_BUBBLE,			/**<  */
		PT_FLY,				/**<  */
		PT_BLOOD,			/**<  */
		PT_BLOOD2,			/**<  */
		PT_BLASTER,			/**<  */
		PT_SMOKE,			/**<  */
		PT_SPLASH,			/**<  */
		PT_SPARK,			/**<  */
		PT_BEAM,			/**<  */
		PT_SPIRAL,			/**<  */
		PT_FLAME,			/**<  */
		PT_BLOODSPRAY,		/**<  */
		PT_EXPLODE,			/**<  */
		PT_WATERPLUME,		/**<  */
		PT_WATERCIRCLE,		/**<  */
		PT_BLOODDRIP,		/**<  */
		PT_BLOODMIST,		/**<  */
		PT_BLOOD_SPLAT,		/**<  */
		PT_BLASTER_BOLT,	/**<  */
		PT_MAX				/**<  */
	};

public:
	Particle* next;				/**< Next particle in list. */

	float time;					/**< Start time? */
	float endTime;				/**< Life? */

	vec3_t origin;				/**< Origin */
	vec3_t previous_origin;		/**< Previous Origin */
	vec3_t vel;					/**< Velocity */
	vec3_t accel;				/**< Acceleration */
	vec3_t mins;				/**< ? */
	vec3_t maxs;				/**< ? */
	vec3_t length;				/**< ? */
	vec3_t dir;					/**< Direction? */

	int type;					/**< ? */

	vec3_t color;				/**< Color of the particle. */
	vec3_t colorVel;			/**< Color of the particle change? */

	float alpha;				/**< Alpha value? */
	float alphavel;				/**< Alpha value change? */

	float size;					/**< Particle size */
	float sizeVel;				/**< Particle size change? */

	float lightradius;			/**< Radius of light cast? */

	vec3_t lcolor;				/**< Light color? */

	float orient;				/**< Orientation? */

	int flags;					/**< Particle behavior flags. */
	int sFactor;				/**< ? */
	int dFactor;				/**< ? */

	float len;					/**< ? */
	float endLen;				/**< ? */
};
