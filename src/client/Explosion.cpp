#include "Explosion.h"


/**
 * C'tor
 */
Explosion::Explosion()
{}


/**
 * D'tor
 */
Explosion::~Explosion()
{}


/**
 * Clears all Explosion values.
 */
void Explosion::clear()
{
	type = EX_FREE;
	//ent.clear();

	frames = 0;
	baseframe = 0;

	light = 0.0f;
	start = 0.0f;

	VectorClear(mLightColor);
}


/**
 * Indicates that the Explosion has a light effect.
 */
bool Explosion::hasLight() const
{
	return light != 0.0f;
}


/**
 * Sets the light color of the Explosion.
 * 
 * \param r	Red value of the light. Clamped to 0.0f - 1.0f;
 * \param g	Green value of the light. Clamped to 0.0f - 1.0f;
 * \param b	Blue value of the light. Clamped to 0.0f - 1.0f;
 */
void Explosion::lightColor(float r, float g, float b)
{
	mLightColor[0] = clamp(r, 0.0f, 1.0f);
	mLightColor[1] = clamp(r, 0.0f, 1.0f);
	mLightColor[2] = clamp(r, 0.0f, 1.0f);
}


/**
 * Gets the light color of the explosion.
 * 
 * \return	Returns a const refernece to the light color.
 */
const vec3_t& Explosion::lightColor() const
{
	return mLightColor;
}
