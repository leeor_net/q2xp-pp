/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "client.h"

Image* i_conback = nullptr;
Image* i_inventory = nullptr;
Image* i_net = nullptr;
Image* i_pause = nullptr;
Image* i_loading = nullptr;
Image* i_turtle = nullptr;
extern Image* r_notexture;


/**
 * Loads initial images such as the console background,
 * menu tags, etc.
 */
void CL_InitImages()
{
	i_conback = Draw_FindPic("gfx/conback.tga");

	i_inventory = Draw_FindPic("gfx/inventory.tga");
	i_net = Draw_FindPic("gfx/net.tga");
	i_pause = Draw_FindPic("gfx/pause.tga");
	i_loading = Draw_FindPic("gfx/loading.tga");

	i_turtle = Draw_FindPic("gfx/turtle.tga");
}
