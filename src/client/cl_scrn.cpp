/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
#include "client.h"
#include "cl_hud.h"
#include "music.h"

#include "../common/colordef.h"
#include "../common/string_util.h"

#include "../ref_gl/r_local.h"
#include "../ref_gl/r_postprocess.h"

#include "../win32/winquake.h"


// ===============================================================================
// = PROTOTYPES
// ===============================================================================
void Draw_LoadingScreen(int x, int y, int w, int h, char* pic);


// ===============================================================================
// = CVAR'S
// ===============================================================================
ConsoleVariable* SCR_VIEWSIZE;
ConsoleVariable* SCR_CONSPEED;
ConsoleVariable* SCR_CENTERTIME;
ConsoleVariable* SCR_SHOWTURTLE;
ConsoleVariable* SCR_SHOWPAUSE;
ConsoleVariable* SCR_DRAWALL;

ConsoleVariable* CROSSHAIR;
ConsoleVariable* CROSSHAIR_SCALE;

extern ConsoleVariable* cl_drawTime;
extern ConsoleVariable* cl_drawFPS;
extern ConsoleVariable* r_lightEditor;


// ===============================================================================
// = LOCAL VARIABLES
// ===============================================================================
char CROSSHAIR_PIC[MAX_QPATH];
char SCR_CENTER_STRING[1024];

float SCR_CENTERTIME_START;		/**< for slow victory printing */
float scr_centertime_off;		/**<  */

int CROSSHAIR_WIDTH;			/**<  */
int CROSSHAIR_HEIGHT;			/**<  */

int SCR_CENTER_LINES;			/**<  */
int scr_erase_center;			/**<  */

float scr_con_current;			/**< aproaches scr_conlines at scr_conspeed */
float scr_conlines;				/**< 0.0 to 1.0 lines of console to display */

bool scr_initialized;			/**< ready to draw */

int SCR_DRAW_LOADING;			/**< 0 = false, 1 = yes, 2 = clear to black first */

vrect_t SCR_VRECT;				/**< position of render window on screen */


bool loadingMessage;
char loadingMessages[5][96];
float loadingPercent;

// ===============================================================================
// = MODULE FUNCTIONS
// ===============================================================================

/**
 *
 */
static void _drawCenterString()
{
	int l;
	int j;
	int x, y;

	// the finale prints the characters one at a time
	int remaining = 9999;

	scr_erase_center = 0;
	char* start = SCR_CENTER_STRING;

	if (SCR_CENTER_LINES <= 4)
		y = videoHeight() * 0.35;
	else
		y = 48; ///\fixme Magic number

	Set_FontShader(true);

	do
	{
		// scan the width of the line
		for (l = 0; l < 40; l++)
		{
			if (start[l] == '\n' || !start[l])
				break;
		}

		x = (videoWidth() - l * 8 * cl_fontScale->value) * 0.5;
		for (j = 0; j < l; j++, x += 8 * cl_fontScale->value)
		{
			Draw_CharScaled(x, y, cl_fontScale->value, cl_fontScale->value, start[j]);

			if (!remaining--)
				return;
		}

		y += 8 * cl_fontScale->value;

		while (*start && *start != '\n')
			start++;

		if (!*start)
			break;

		start++;				// skip the \n
	} while (1);

	Set_FontShader(false);
}


/**
 * Sets scr_vrect, the coordinates of the rendered window
 */
static void _calcVrect()
{
	// bound viewsize
	std::string _val = std::to_string(clamp(static_cast<int>(SCR_VIEWSIZE->value), 40, 100));
	Cvar_Set("viewsize", _val.c_str());

	int size = SCR_VIEWSIZE->value;

	SCR_VRECT.width = videoWidth() * size / 100;
	SCR_VRECT.width &= ~7;

	SCR_VRECT.height = videoHeight() * size / 100;
	SCR_VRECT.height &= ~1;

	SCR_VRECT.x = (videoWidth() - SCR_VRECT.width) / 2;
	SCR_VRECT.y = (videoHeight() - SCR_VRECT.height) / 2;
}


/**
 * Set a specific sky and rotation speed
 */
static void _sky()
{
	float rotate = 0.0f;
	vec3_t axis = { 0.0f };

	if (Cmd_Argc() < 2)
	{
		Com_Printf("Usage: sky <basename> <rotate> <axis x y z>\n");
		return;
	}

	if (Cmd_Argc() > 2)
	{
		rotate = atof(Cmd_Argv(2));
	}

	if (Cmd_Argc() == 6)
	{
		axis[0] = atof(Cmd_Argv(3));
		axis[1] = atof(Cmd_Argv(4));
		axis[2] = atof(Cmd_Argv(5));
	}
	else
	{
		axis[0] = 0.0f;
		axis[1] = 0.0f;
		axis[2] = 1.0f;
	}

	R_SetSky(Cmd_Argv(1), rotate, axis);
}


/**
 *
 */
static void _timeRefresh()
{
	if (cls.state != ca_active)
	{
		return;
	}

	int start = Sys_Milliseconds();

	// run without page flipping
	if (Cmd_Argc() == 2)
	{
		R_BeginFrame();
		for (int i = 0; i < 128; i++)
		{
			cl.refdef.viewangles[1] = i / 128.0 * 360.0;
			R_RenderFrame(&cl.refdef);
		}
		GLimp_EndFrame();
	}
	else
	{
		for (int i = 0; i < 128; i++)
		{
			cl.refdef.viewangles[1] = i / 128.0 * 360.0;

			R_BeginFrame();
			R_RenderFrame(&cl.refdef);
			GLimp_EndFrame();
		}
	}

	float time = (Sys_Milliseconds() - start) / 1000.0;
	Com_Printf("%f seconds (" S_COLOR_YELLOW "%f" S_COLOR_WHITE " fps)\n", time, 128 / time); ///\fixme magic number
}


/**
 * Keybinding command
 */
static void _sizeUp()
{
	Cvar_SetValue("viewsize", SCR_VIEWSIZE->value + 10);
}


/**
 * Keybinding command
 */
static void _sizeDown()
{
	Cvar_SetValue("viewsize", SCR_VIEWSIZE->value - 10);
}


// ===============================================================================
// = EXPORT FUNCTIONS
// ===============================================================================

/**
 * 
 */
vrect_t* SCR_ViewRectangle()
{
	return &SCR_VRECT;
}


/**
 * 
 */
bool SCR_DrawingLoad()
{
	return (SCR_DRAW_LOADING != 0);
}


/**
 * Called for important messages that should stay in the center of the screen
 * for a few moments
 */
void SCR_CenterPrint(char* str)
{
	char line[64];
	int i, j, l;

	strncpy(SCR_CENTER_STRING, str, sizeof(SCR_CENTER_STRING) - 1);
	scr_centertime_off = SCR_CENTERTIME->value;
	SCR_CENTERTIME_START = cl.time;

	// count the number of lines for centering
	SCR_CENTER_LINES = 1;
	char* s = str;
	while (*s)
	{
		if (*s == '\n')
			SCR_CENTER_LINES++;
	
		s++;
	}

	// echo it to the console
	Com_Printf("\n\n\35\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\37\n\n");

	s = str;
	do
	{
		// scan the width of the line
		for (l = 0; l < 40; l++)
		{
			if (s[l] == '\n' || !s[l])
				break;
		}

		for (i = 0; i < (40 - l) * 0.5; i++)
			line[i] = ' ';

		for (j = 0; j < l; j++)
			line[i++] = s[j];

		line[i] = '\n';
		line[i + 1] = 0;

		Com_Printf("%s", line);

		while (*s && *s != '\n')
			s++;

		if (!*s)
			break;
		s++;					// skip the \n
	} while (1);

	Com_Printf("\n\n\35\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\37\n\n");
	Con_ClearNotify();
}


/**
 * 
 */
void SCR_CheckDrawCenterString()
{
	scr_centertime_off -= cls.frametime;

	if (scr_centertime_off <= 0)
		return;

	_drawCenterString();
}


/**
 * 
 */
void SCR_Init()
{
	SCR_VIEWSIZE = Cvar_Get("viewsize", "100", CVAR_ARCHIVE);
	SCR_CONSPEED = Cvar_Get("scr_conspeed", "2", 0);
	SCR_CENTERTIME = Cvar_Get("scr_centertime", "2.5", 0);
	SCR_SHOWTURTLE = Cvar_Get("scr_showturtle", "0", 0);
	SCR_SHOWPAUSE = Cvar_Get("scr_showpause", "1", 0);
	SCR_DRAWALL = Cvar_Get("scr_drawall", "0", 0);

	CROSSHAIR = Cvar_Get("crosshair", "0", CVAR_ARCHIVE);
	CROSSHAIR_SCALE = Cvar_Get("crosshairScale", "0.5", CVAR_ARCHIVE);

	// register commands
	Cmd_AddCommand("timerefresh", _timeRefresh);
	Cmd_AddCommand("loading", SCR_BeginLoadingPlaque);
	Cmd_AddCommand("sizeup", _sizeUp);
	Cmd_AddCommand("sizedown", _sizeDown);
	Cmd_AddCommand("sky", _sky);

	scr_initialized = true;
}


/**
 * 
 */
void SCR_DrawNet()
{
	if (cls.netchan.outgoing_sequence - cls.netchan.incoming_acknowledged < CMD_BACKUP - 1)
		return;

	Draw_Pic2(SCR_VRECT.x + 64, SCR_VRECT.y, i_net);
}


/**
 * 
 */
void SCR_DrawPause()
{
	if (!SCR_SHOWPAUSE->value || !cl_paused->value ||  cls.menuActive)
		return;

	int x = (videoWidth() - i_pause->width) * 0.5f;
	int y = (videoHeight() - i_pause->height) * 0.5;

	Draw_Pic(x, y, "gfx/pause.tga");
	Draw_PicBump(x, y, "gfx/pause.tga", "gfx/pause_n.tga");
}


/**
 * SCR_DrawLoading
 */
void SCR_DrawLoadingBar(float percent, float scale)
{
	Draw_BoxFilled(2, videoHeight() - scale * 10 + 3, videoWidth() * percent * 0.01, scale * 3 - 6, 0.0, 1.0, 0.0, 0.13);
}


/**
 * 
 */
void SCR_DrawLoading()
{
	int scaled, center;
	char mapfile[32];
	char* mapname;
	int fontscale = (int)cl_fontScale->value;

	if (!SCR_DrawingLoad())
		return;

	SCR_DRAW_LOADING = 0;

	if (loadingMessage && cl.configstrings[CS_MODELS + 1][0])
	{
		strcpy(mapfile, cl.configstrings[CS_MODELS + 1] + 5);	// skip "maps/"
		mapfile[strlen(mapfile) - 4] = 0;						// cut off ".bsp"

		Draw_LoadingScreen(0, 0, videoWidth(), videoHeight(), va("/levelshots/%s.jpg", mapfile));

		scaled = 8 * fontscale;
		SCR_DrawLoadingBar(loadingPercent, scaled);

		mapname = cl.configstrings[CS_NAME];

		Set_FontShader(true);
		GL_SetFontColor(GetColorFromIndex(COLOR_BLACK));

		center = videoWidth() / 2 - (int)strlen(mapname) * fontscale * 8;
		Draw_StringScaled(center - 3, fontscale * scaled + 3, fontscale * 2, fontscale * 2, mapname);
		GL_SetFontColor(GetColorFromIndex(COLOR_GREEN));
		Draw_StringScaled(center, fontscale * scaled, fontscale * 2, fontscale * 2, mapname);

		GL_SetFontColor(GetColorFromIndex(COLOR_YELLOW));
		Draw_StringScaled(0, 24 * fontscale * 2, fontscale, fontscale, va("%s", loadingMessages[0]));
		Draw_StringScaled(0, 32 * fontscale * 2, fontscale, fontscale, va("%s", loadingMessages[1]));
		Draw_StringScaled(0, 40 * fontscale * 2, fontscale, fontscale, va("%s", loadingMessages[2]));
		Draw_StringScaled(0, 48 * fontscale * 2, fontscale, fontscale, va("%s", loadingMessages[3]));
		GL_SetFontColor(GetColorFromIndex(COLOR_WHITE));
		Set_FontShader(false);
	}
}


/**
 * Scroll it up or down <-- ?
 */
void SCR_RunConsole()
{
	// decide on the height of the console
	if (cls.key_dest == KEY_CONSOLE)
		scr_conlines = 0.5;		// half screen
	else
		scr_conlines = 0;		// none visible

	if (scr_conlines < scr_con_current)
	{
		scr_con_current -= SCR_CONSPEED->value * cls.frametime;
		if (scr_conlines > scr_con_current)
			scr_con_current = scr_conlines;

	}
	else if (scr_conlines > scr_con_current)
	{
		scr_con_current += SCR_CONSPEED->value * cls.frametime;
		if (scr_conlines < scr_con_current)
			scr_con_current = scr_conlines;
	}
}


/**
 * 
 */
void SCR_DrawConsole()
{
	Con_CheckResize();

	// forced full screen console
	if (cls.state == ca_disconnected || cls.state == ca_connecting)
	{
		Con_DrawConsole(1.0);
		return;
	}

	// connected, but can't render
	if (cls.state != ca_active || !cl.refresh_prepped)
	{
		Con_DrawConsole(0.5);
		Draw_BoxFilled(0, videoHeight() / 2, videoWidth(), videoHeight() / 2, 0.0, 0.0, 0.0, 1.0);
		return;
	}

	if (scr_con_current)
	{
		Con_DrawConsole(scr_con_current);
	}
	else
	{
		if (cls.key_dest == KEY_GAME || cls.key_dest == KEY_MESSAGE)
			Con_DrawNotify();	// only draw notify in game
	}
}


/**
 * 
 */
void SCR_BeginLoadingPlaque()
{
	S_StopAllSounds();

	cl.sound_prepped = false;	// don't play ambients

	Music_Stop();

	if (cls.disable_screen)
		return;

	if (developer->value)
		return;

	if (cls.state == ca_disconnected)
		return;					// if at console, don't bring up the plaque

	if (cls.key_dest == KEY_CONSOLE)
		return;

	SCR_DRAW_LOADING = 1;

	SCR_UpdateScreen();

	cls.disable_screen = Sys_Milliseconds();
	cls.disable_servercount = cl.servercount;
}


/**
 * 
 */
void SCR_EndLoadingPlaque()
{
	cls.disable_screen = 0;
	SCR_DRAW_LOADING = 0;
	Con_ClearNotify();
}


/**
 * 
 */
int entitycmpfnc(const entity_t * a, const entity_t * b)
{
	// all other models are sorted by model then skin
	if (a->model == b->model)
		return ((int)a->skin - (int)b->skin);
	else
		return ((int)a->model - (int)b->model);
}


//===============================================================

#define STAT_MINUS		10	// num frame for '-' stats digit
/// \fixme	There has to be a better way to do this.
char* sb_nums[2][11] =	{{	"gfx/numbers/0.tga", "gfx/numbers/1.tga", "gfx/numbers/2.tga", "gfx/numbers/3.tga", "gfx/numbers/4.tga", "gfx/numbers/5.tga",
							"gfx/numbers/6.tga", "gfx/numbers/7.tga", "gfx/numbers/8.tga", "gfx/numbers/9.tga", "gfx/numbers/-.tga" },
						{	"gfx/numbers/r0.tga", "gfx/numbers/r1.tga", "gfx/numbers/r2.tga", "gfx/numbers/r3.tga", "gfx/numbers/r4.tga", "gfx/numbers/r5.tga",
							"gfx/numbers/r6.tga", "gfx/numbers/r7.tga", "gfx/numbers/r8.tga", "gfx/numbers/r9.tga", "gfx/numbers/r-.tga" }};

#define	ICON_WIDTH	24
#define	ICON_HEIGHT	24
#define	ICON_SPACE	8


int CHARACTER_WIDTH = 32;
int CHARACTER_HEIGHT = 64;

const int CHARACTER_MARGIN = 2;


/**
 * 
 * 
 * \note Allows embedded \n in the string
 */
void SizeHUDString(char* string, int *w, int *h)
{
	int lines, width, current;

	lines = 1;
	width = 0;

	current = 0;
	while (*string)
	{
		if (*string == '\n')
		{
			lines++;
			current = 0;
		}
		else
		{
			current++;
			if (current > width)
				width = current;
		}
		string++;
	}

	*w = width * 8;
	*h = lines * 8;
}


/**
 *
 */
void SCR_DrawHUDString(float x, float y, float scale_x, float scale_y, int centerwidth, int xor, char* string)
{
	char line[1024] = { '\0' };

	float margin = x;

	Set_FontShader(true);

	while (*string)
	{
		// scan out one line of text from the string
		int width = 0;
		
		while (*string && *string != '\n')
			line[width++] = *string++;

		line[width] = 0;

		x = margin;	///\fixme paramter shenanigans

		if (centerwidth)
			x = margin + (centerwidth - (width * 8)) * scale_x / 2;
		
		for (int i = 0; i < width; i++)
		{
			Draw_CharScaled(x, y, scale_x, scale_y, line[i] ^ xor);
			x += 8 * scale_x;
		}
		
		if (*string)
		{
			string++;	// skip the \n
			x = margin;
			y += 8 * scale_y;
		}
	}

	Set_FontShader(false);
}


/**
 * Draws the crosshair to the screen.
 */
void SCR_DrawCrosshair()
{
	if (!CROSSHAIR->value)
		return;

	if (CROSSHAIR->modified)
	{
		CROSSHAIR->modified = false;
		SCR_TouchPics();
	}

	if (!CROSSHAIR_PIC[0])
		return;

	int size_x = (CROSSHAIR_WIDTH * CROSSHAIR_SCALE->value) / 2;
	int size_y = (CROSSHAIR_HEIGHT * CROSSHAIR_SCALE->value) / 2;

	GL_Enable(GL_BLEND);
	GL_BlendFunc(GL_ONE, GL_ONE);

	Draw_PicScaled((videoWidth() / 2) - size_x, (videoHeight() / 2) - size_y, CROSSHAIR_SCALE->value, CROSSHAIR_SCALE->value, CROSSHAIR_PIC);

	GL_BlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}


/**
 * 
 */
void SCR_DrawField(int x, int y, int color, int width, int value)
{
	char num[16], *ptr;
	int l;
	int frame;

	if (width < 1)
		return;

	// draw number string
	width = clamp(width, 1, 5);

	Q_sprintf(num, sizeof(num), "%i", value);
	l = strlen(num);

	if (l > width)
		l = width;

	ptr = num;
	int _x = x;
	while (*ptr && l)
	{
		if (*ptr == '-') frame = STAT_MINUS;
		else frame = *ptr - '0';

		_x += CHARACTER_WIDTH + CHARACTER_MARGIN;

		Draw_Pic(_x, y, sb_nums[color][frame]);

		ptr++;
		l--;
	}
}


/**
 * Precache common images (HUD nubmers, crosshairs, etc.)
 */
void SCR_TouchPics()
{
	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 11; j++)
			Draw_FindPic(sb_nums[i][j]);
	}

	Draw_GetPicSize(CHARACTER_WIDTH, CHARACTER_HEIGHT, sb_nums[0][0]);

	Q_sprintf(CROSSHAIR_PIC, MAX_QPATH, "gfx/crosshair_%i.tga", (int)(CROSSHAIR->value));
	Draw_GetPicSize(CROSSHAIR_WIDTH, CROSSHAIR_HEIGHT, CROSSHAIR_PIC);
	
	if (CROSSHAIR_WIDTH == 0)
	{
		CROSSHAIR_PIC[0] = 0;
	}
}


/**
 * 
 */
void SCR_DrawFPS()
{
	static	char	avrfps[10], minfps[14], maxfps[14];
	static	int		fps = 0;
	static	int		lastUpdate;
	const	int		samPerSec = 4;
	static	float	fpsAvg = 0;
	const	float	fontscale = cl_fontScale->value;

	fps++;

	if (curtime - lastUpdate >= 1000 / samPerSec)
	{
		const float alpha = 0.45;

		if (cl.minFps == 0) // only one time per level
			cl.minFps = 999999;

		if (fpsAvg < cl.minFps)
			cl.minFps = fpsAvg;

		if (fpsAvg > cl.maxFps)
			cl.maxFps = fpsAvg;

		Q_sprintf(minfps, sizeof(minfps), "Min FPS %4d", cl.minFps);
		Q_sprintf(maxfps, sizeof(maxfps), "Max FPS %4d", cl.maxFps);

		Q_sprintf(avrfps, sizeof(avrfps), "%4d FPS", (int)fpsAvg);

		lastUpdate = curtime;
		fpsAvg = samPerSec * (alpha * fps + ((1 - alpha) * fpsAvg) / samPerSec);
		fps = 0;
	}


	if (cl_drawFPS->value && (cls.state == ca_active))
	{
		Set_FontShader(true);

		if (cl_drawFPS->value == 2)
		{
			Draw_StringScaled(videoWidth() - 65 * fontscale, videoHeight() * 0.65 - 40, fontscale, fontscale, avrfps);
			Draw_StringScaled(videoWidth() - 95 * fontscale, videoHeight() * 0.65 - 20, fontscale, fontscale, minfps);
			Draw_StringScaled(videoWidth() - 95 * fontscale, videoHeight() * 0.65, fontscale, fontscale, maxfps);
		}
		else
		{
			Draw_StringScaled(videoWidth() - 65 * fontscale, videoHeight() * 0.65, fontscale, fontscale, avrfps);
		}

		//RE_SetColor(colorWhite);
		Set_FontShader(false);
	}
}


/**
 * 
 */
void SCR_DrawClock()
{
	if (!cl_drawTime->value || (cls.state != ca_active))
		return;

	char timebuf[20];
	char tmpbuf[24];
	char datebuf[20];
	char tmpdatebuf[24];
	float fontscale = cl_fontScale->value;

#ifndef _WIN32
	struct tm *tm;
	time_t aclock;

	time(&aclock);
	tm = localtime(&aclock);
	strftime(timebuf, sizeof(timebuf), "%T", tm);
	strftime(datebuf, sizeof(datebuf), "%D", tm);
#else
	_strtime(timebuf);
	_strdate(datebuf);
#endif

	sprintf(tmpbuf, "Time %s", timebuf);
	sprintf(tmpdatebuf, "Date %s", datebuf);

	Set_FontShader(true);

	if (!cl_drawFPS->value)
	{
		Draw_StringScaled(videoWidth() - 105 * fontscale, videoHeight() * 0.65, fontscale, fontscale, tmpbuf);
		Draw_StringScaled(videoWidth() - 105 * fontscale, videoHeight() * 0.65 + 10 * fontscale, fontscale, fontscale, tmpdatebuf);
	}
	else
	{
		Draw_StringScaled(videoWidth() - 105 * fontscale, videoHeight() * 0.65 + 10 * fontscale, fontscale, fontscale, tmpbuf);
		Draw_StringScaled(videoWidth() - 105 * fontscale, videoHeight() * 0.65 + 20 * fontscale, fontscale, fontscale, tmpdatebuf);
	}

	Set_FontShader(false);
}


/**
 * This is called every frame, and can also be called explicitly to flush
 * text to the screen.
 */
void SCR_UpdateScreen()
{
	// Do nothing if the screen is disabled (loading plaque is up, or vid mode changing)
	if (cls.disable_screen)
	{
		if (cls.download) // Knightmare- don't time out on downloads
			cls.disable_screen = Sys_Milliseconds();

		if (Sys_Milliseconds() - cls.disable_screen > 120000 && cl.refresh_prepped)
		{
			cls.disable_screen = 0;
			Com_Printf("Loading plaque timed out.\n");
			return;
		}
		SCR_DRAW_LOADING = 2;
	}

	if (!scr_initialized || !Con_Console()->initialized) return; // not initialized yet

	cl_fontScale->value = clamp(cl_fontScale->value, 2.0, 2.0);

	R_BeginFrame();

	if (SCR_DRAW_LOADING == 2) // loading plaque over black screen
	{
		SCR_DrawLoading();
		if (cls.disable_screen) { SCR_DRAW_LOADING = 2; } // SCR_DRAW_LOADING is changed in SCR_DrawLoading()
	}
	else
	{
		_calcVrect();	// do 3D refresh drawing, and then update the screen

		V_RenderView();

		SCR_DrawStats();
		SCR_DrawLayout();
		CL_DrawInventory();

		SCR_DrawNet();
		SCR_CheckDrawCenterString();
		SCR_DrawPause();
		SCR_DrawFPS();

		SCR_DrawClock();

		SCR_DrawConsole();
		M_Draw();
		SCR_DrawLoading();
	}

	R_GammaRamp();
	GLimp_EndFrame();
}
