#pragma once

#include "ref.h"


/**
 * 
 */
class Explosion
{
public:
	/**
	 *
	 */
	enum ExplosionType
	{
		EX_FREE,		/**<  */
		EX_EXPLOSION,	/**<  */
		EX_MISC,		/**<  */
		EX_FLASH,		/**<  */
		EX_MFLASH,		/**<  */
		EX_POLY,		/**<  */
		EX_POLY2		/**<  */
	};

public:
	Explosion();
	~Explosion();

	void clear();

	bool hasLight() const;

	void lightColor(float r, float g, float b);
	const vec3_t& lightColor() const;

public:
	entity_t		entity;					/**<  */

	ExplosionType	type		= EX_FREE;	/**<  */

	int				frames		= 0;		/**<  */
	int				baseframe	= 0;		/**<  */

	float			light		= 0.0f;		/**<  */
	float			start		= 0.0f;		/**<  */


private:
	Explosion(const Explosion&) = delete;
	Explosion& operator=(const Explosion&) = delete;

private:
	vec3_t			mLightColor = { 0.0f };	/**<  */

};
