/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// snd_mem.c: sound caching

#include "client.h"
#include "snd_loc.h"

#include "../common/string_util.h"

#include <vector>


// ===============================================================================
// = DEFINES
// ===============================================================================
#define PCM_16BIT	1			// willow: do not touch this!!
#define PCM_STEREO	2			// willow: do not touch this!!


// ===============================================================================
// = IMPORTS
// ===============================================================================
extern std::vector<std::string> fastsound_name;


// ===============================================================================
// = LOCAL VARIABLES
// ===============================================================================
byte *data_p;			/**<  */
byte *iff_end;			/**<  */
byte *last_chunk;		/**<  */
byte *iff_data;			/**<  */
int iff_chunk_len;		/**<  */


// ===============================================================================
// = PRIVATE MODULE FUNCTIONS
// ===============================================================================

/**
 * 
 */
static std::string normalizeSoundName(const std::string& name)
{
	if (name.substr(0, 1) == "#")
	{
		return "sound/" + name.substr(1);
	}
	else
	{
		return "sound/" + name;
	}
}


/**
 * 
 */
static std::string translateAlError(int errorId)
{
	switch (errorId)
	{
	case AL_NO_ERROR:
		return "No Errors";

	case AL_INVALID_NAME:
		return "Invalid Name paramater passed to AL call.";

	case AL_ILLEGAL_ENUM:
		return "Invalid parameter passed to AL call.";

	case AL_INVALID_VALUE:
		return "Invalid enum parameter value.";

	case AL_ILLEGAL_COMMAND:
		return "Illegal call.";

	case AL_OUT_OF_MEMORY:
		return "Out of memory.";

	default:
		throw std::runtime_error("translateAlError():: Invalid error code passed.");
	}
}


/**
 * 
 */
short GetLittleShort()
{
	short val = *((short *)data_p);
	data_p += 2;
	return val;
}


/**
 * 
 */
long GetLittleLong()
{
	long val = *((long *)data_p);
	data_p += 4;
	return val;
}


/**
 * 
 */
static void FindNextChunk(char* name)
{
	while (1)
	{
		data_p = last_chunk;

		if (data_p >= iff_end) // didn't find the chunk
		{
			data_p = nullptr;
			return;
		}

		data_p += 4;
		iff_chunk_len = GetLittleLong();
		if (iff_chunk_len < 0)
		{
			data_p = nullptr;
			return;
		}
		data_p -= 8;
		last_chunk = data_p + 8 + ((iff_chunk_len + 1) & ~1);
		if (!strncmp(reinterpret_cast<const char*>(data_p), name, 4))
		{
			return;
		}
	}
}


/**
 * 
 */
static void FindChunk(char* name)
{
	last_chunk = iff_data;
	FindNextChunk(name);
}


/**
 * 
 */
static bool LoadWAV(const std::string& name, byte** wav, ALenum& format, ALsizei& rate, ALsizei& size)
{
	byte *buffer = nullptr;
	int length = FS_LoadFile(name.c_str(), reinterpret_cast<void**>(&buffer));

	if (!buffer) { return false; }

	iff_data = buffer;
	iff_end = buffer + length;

	// Find "RIFF" chunk
	FindChunk("RIFF");
	if (!(data_p && !memcmp((void *)(data_p + 8), "WAVE", 4)))
	{
		Com_DPrintf("S_LoadWAV():: missing 'RIFF/WAVE' chunks (%s)\n", name.c_str());
		FS_FreeFile(buffer);
		return false;
	}

	// Get "fmt " chunk
	iff_data = data_p + 12;

	FindChunk("fmt ");
	if (!data_p)
	{
		Com_DPrintf("S_LoadWAV():: missing 'fmt ' chunk (%s)\n", name.c_str());
		FS_FreeFile(buffer);
		return false;
	}

	data_p += 8;

	if (GetLittleShort() != 1)
	{
		Com_DPrintf("S_LoadWAV():: Microsoft PCM format only (%s)\n", name.c_str());
		FS_FreeFile(buffer);
		return false;
	}

	format = AL_FORMAT_MONO8;
	int channels = GetLittleShort();
	if (channels != 1 && channels != 2)
	{
		Com_DPrintf("S_LoadWAV():: only mono and stereo WAV files supported (%s)\n", name.c_str());
		FS_FreeFile(buffer);
		return false;
	}

	if (channels == 2) { format |= PCM_STEREO; }

	rate = GetLittleLong();

	data_p += 4 + 2;

	int width = GetLittleShort() >> 3;
	if (width != 1 && width != 2)
	{
		Com_DPrintf("S_LoadWAV():: only 8 and 16 bit WAV files supported (%s)\n", name.c_str());
		FS_FreeFile(buffer);
		return false;
	}

	if (width == 2) { format |= PCM_16BIT; }

	// Find data chunk
	FindChunk("data");
	if (!data_p)
	{
		Com_DPrintf("S_LoadWAV():: missing 'data' chunk (%s)\n", name.c_str());
		FS_FreeFile(buffer);
		return false;
	}

	data_p += 4;
	size = GetLittleLong();

	if (size == 0)
	{
		Com_DPrintf("S_LoadWAV: file with 0 samples (%s)\n", name.c_str());
		FS_FreeFile(buffer);
		return false;
	}

	// Load the data
	byte* out = nullptr;
	*wav = out = static_cast<byte*>(calloc(size, sizeof(byte*)));
	memcpy(out, buffer + (data_p - buffer), size);

	FS_FreeFile(buffer);

	return true;
}


/**
 * 
 */
static ALuint fastsoundCheck(const std::string& name)
{
	for (size_t idx = 0; idx < SOUND_ID_COUNT; ++idx)
	{
		if (fastsound_name[idx] == name)
		{
			Com_DPrintf("Legacy code requested fastsound sfx caching. Request ignored for '" S_COLOR_GREEN "%s" S_COLOR_WHITE "'\n", name.c_str());
			return fastsound_descriptor[idx];
		}
	}

	return 0;
}


// ===============================================================================
// = PUBLIC MODULE FUNCTIONS
// ===============================================================================
/**
 * Finds a sound ID by name.
 * 
 * \param name		Name of the sound effect.
 * \param generate	Make a new sound effect from a file if it hasn't already been loaded.
 * 
 * \return	Non-zero value if the sound was found or generated.
 * 
 * \note	A return value of 0 means that the sound was unable to be loaded. Check logs
 *			for possible reasons why.
 */
ALuint S_FindName(const std::string& name, bool generate)
{
	if (!s_initsound->value || openalStop) { return 0; }

	if (name.empty()) { Com_Error(ERR_FATAL, "S_FindName():: empty name\n"); }

	ALuint fastsound_Id = fastsoundCheck(name);
	if (fastsound_Id != 0)
	{
		Com_DPrintf("Legacy code requested fastsound sfx caching. Request ignored for '" S_COLOR_GREEN "%s" S_COLOR_WHITE "'\n", name.c_str());
		return fastsound_Id;
	}

	ALuint sfxId = findSfxID(name);
	if (sfxId != 0)
	{
		return sfxId;
	}

	if (!generate) { return 0; }

	if (sfxCount() >= MAX_SFX)
	{
		Com_Error(ERR_FATAL, "S_FindName():: Sound descriptor pool overflow.");
	}

	byte *data = nullptr;
	ALsizei rate = 0, size = 0;
	ALenum format = 0;

	if (LoadWAV(normalizeSoundName(name), &data, format, rate, size))
	{
		ALuint sfx_id = 0;
		alGenBuffers(1, &sfx_id);

		#ifdef _WITH_XRAM
		if (!eaxSetBufferMode(1, &sfx_id, alGetEnumValue("AL_STORAGE_HARDWARE")))
		{
			Com_DPrintf("S_FindName():: unable to set X-RAM mode\n");
		}

		if (alGetError() != AL_NO_ERROR)
		{
			Com_DPrintf("S_FindName():: unable to set X-RAM mode\n");
		}
		#endif

		alBufferData(sfx_id, format, data, size, rate);
		if (alGetError() != AL_NO_ERROR)
		{
			Com_DPrintf("S_FindName():: bad sample (" S_COLOR_GREEN "%s" S_COLOR_WHITE ")\n", translateAlError(alGetError()).c_str());
		}

		free(data);

		if (sfx_id != 0)
		{
			addNewSfxID(name, sfx_id);
		}

		return sfx_id;
	}
	else
	{
		Com_DPrintf("BAD WAV: '" S_COLOR_GREEN "%s" S_COLOR_WHITE "'\n", name);
		return 0;
	}
}


/**
 * \todo	Document this function.
 * \fixme	A quick glance suggests that this function is poorly named.
 */
void S_fastsound_get_descriptors_pool(unsigned count, ALuint* descriptors_pool)
{
	alGenBuffers(count, descriptors_pool);

#ifdef _WITH_XRAM
	if (!eaxSetBufferMode(count, descriptors_pool, alGetEnumValue("AL_STORAGE_HARDWARE")))
	{
		Com_DPrintf("S_fastsound: unable to set X-RAM mode\n");
	}

	if (alGetError() != AL_NO_ERROR)
	{
		Com_DPrintf("S_fastsound: unable to set X-RAM mode\n");
	}
#endif
}


/**
 * \todo	Document this function.
 * \fixme	A quick glance suggests that this function is poorly named.
 */
void S_fastsound_kill_descriptors_pool(unsigned count, ALuint* descriptors_pool)
{
	alDeleteBuffers(count, descriptors_pool);
	memset(descriptors_pool, 0, count * sizeof(ALuint));
}


/**
 * 
 */
void S_fastsound_load(const std::string& name, ALuint bufferNum)
{
	std::string _fname = normalizeSoundName(name);

	byte *data = nullptr;
	ALsizei rate = 0;
	ALenum format = 0;
	ALsizei size = 0;

	if (!FS_FileExists(_fname))
	{
		Com_DPrintf("S_fastsound_load():: File not found " S_COLOR_GREEN "\'%s\'" S_COLOR_WHITE "\n", _fname.c_str());
		return;
	}

	if (!LoadWAV(_fname, &data, format, rate, size))
	{
		Com_DPrintf("BAD WAV '%s'\n", _fname.c_str());
		return;
	}

	// Upload the sound
	alBufferData(bufferNum, format, data, size, rate);
	if (alGetError() != AL_NO_ERROR)
	{
		Com_DPrintf("S_fastsound_load():: bad sample\n");
	}

	free(data);
}
