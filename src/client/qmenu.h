/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
#pragma once

#include "../common/shared.h"

#define MAXMENUITEMS		64

#define MTYPE_SLIDER		0
#define MTYPE_LIST			1
#define MTYPE_ACTION		2
#define MTYPE_SPINCONTROL	3
#define MTYPE_SEPARATOR  	4
#define MTYPE_FIELD			5

#define SLIDER_RANGE		10

#define	K_TAB				9
#define	K_ENTER				13
#define	K_ESCAPE			27
#define	K_SPACE				32

// normal keys should be passed as lowercased ascii

#define	K_BACKSPACE			127
#define	K_UPARROW			128
#define	K_DOWNARROW			129
#define	K_LEFTARROW			130
#define	K_RIGHTARROW		131

#define QMF_LEFT_JUSTIFY	0x00000001
#define QMF_GRAYED			0x00000002
#define QMF_NUMBERSONLY		0x00000004


#define MENU_MAX_ITEMS		64
#define MENU_FIELD_BUFFER	80

/**
 * 
 */
typedef struct MenuFramework
{
	int x, y;
	int cursor;

	int nitems;
	int nslots;
	void *items[MENU_MAX_ITEMS];

	const char* statusbar;

	void (*cursordraw)(struct MenuFramework* m);

} menuframework_s;


/**
 * 
 */
typedef struct MenuCommon
{
	int type;
	const char* name;
	int x, y;
	menuframework_s* parent;
	int cursor_offset;
	int localdata[4];
	unsigned flags;

	const char* statusbar;

	void (*callback)(void *self);
	void (*statusbarfunc)(void *self);
	void (*ownerdraw)(void *self);
	void (*cursordraw)(void *self);
} menucommon_s;


/**
 * 
 */
typedef struct MenuField
{
	menucommon_s generic;

	char buffer[MENU_FIELD_BUFFER];
	int cursor;
	int length;
	int visible_length;
	int visible_offset;
} menufield_s;


/**
 * 
 */
typedef struct MenuSlider
{
	menucommon_s generic;

	float minvalue;
	float maxvalue;
	float curvalue;
	int intVal;
	float range;
} menuslider_s;


/**
 * 
 */
typedef struct MenuList
{
	menucommon_s generic;

	int curvalue;

	char* *itemnames;
} menulist_s;


/**
 * 
 */
typedef struct MenuAction
{
	menucommon_s generic;
} menuaction_s;


/**
 * 
 */
typedef struct MenuSeparator
{
	menucommon_s generic;
} menuseparator_s;


bool Field_Key(menufield_s* field, int key);

void Menu_AddItem(menuframework_s* menu, void *item);
void Menu_AdjustCursor(menuframework_s* menu, int dir);
void Menu_Center(menuframework_s* menu);
void Menu_Draw(menuframework_s* menu);
void *Menu_ItemAtCursor(menuframework_s* m);
bool Menu_SelectItem(menuframework_s* s);
void Menu_SetStatusBar(menuframework_s* s, const char* string);
void Menu_SlideItem(menuframework_s* s, int dir);
int Menu_TallySlots(menuframework_s* menu);

void Menu_DrawStringDark(int, int, const char*);
void Menu_DrawStringR2L(int, int, const char*);
void Menu_DrawStringR2LDark(int, int, const char*);
