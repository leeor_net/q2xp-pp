/*
Copyright (C)	1997-2001 Id Software, Inc.,
2004-2013 Quake2xp Team

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/


#include "cl_hud.h"
#include "client.h"


#include "../common/def_refdef.h"
#include "../common/def_renderfx.h"
#include "../common/string_util.h"

#include "../ref_gl/r_util.h"


#include <string>


const int	DISPLAY_ITEMS				= 	17;
const int	HEALTH_FLASH_THRESHOLD		=	25;
const float	AMMO_FLASH_THRESHOLD		=	0.10f;		// 10% of capacity


/**
 * 
 */
static void Inv_DrawString(int x, int y, char* string)
{
	while (*string)
	{
		Draw_CharScaled(x, y, cl_fontScale->value, cl_fontScale->value, *string);
		x += 8 * cl_fontScale->value;
		string++;
	}
}


/**
 * 
 */
static void SetStringHighBit(char* s)
{
	while (*s)
		*s++ |= 128;
}


/**
 * 
 */
static void DrawAltStringScaled(int x, int y, float scale_x, float scale_y, char* s)
{
	Set_FontShader(true);
	while (*s)
	{
		Draw_CharScaled(x, y, scale_x, scale_y, *s ^ 0x80);
		x += 8 * scale_x;
		s++;
	}
	Set_FontShader(false);
}


/**
 * 
 */
static void SCR_ExecuteLayoutString(char* s)
{
	if (cls.state != ca_active || !cl.refresh_prepped) { return; }
	if (!s[0]) { return; }
	if (cl_drawhud->value == 0.0f) { return; }


	int value = 0, index = 0;
	clientinfo_t* ci = nullptr;

	float screenAspect = static_cast<float>(videoWidth()) / static_cast<float>(videoHeight());
	float scaledHeight = 320.0 / screenAspect;
	
	float hud_sx = static_cast<float>(videoWidth()) / 320.0;
	float hud_sy = static_cast<float>(videoHeight()) / scaledHeight;

	int x = 0;
	int y = 0;
	int width = 3;

	while (s)
	{
		char* token = Com_Parse(&s);

		if (!strcmp(token, "xl"))
		{
			token = Com_Parse(&s);
			x = atoi(token)*hud_sx;
			continue;
		}

		if (!strcmp(token, "xr"))
		{
			token = Com_Parse(&s);
			x = videoWidth() + atoi(token)*hud_sx;
			continue;
		}

		if (!strcmp(token, "xv"))
		{
			token = Com_Parse(&s);
			x = videoWidth() / 2 - (160 - atoi(token))*hud_sx;
			continue;
		}

		if (!strcmp(token, "yt"))
		{
			token = Com_Parse(&s);
			y = atoi(token)*hud_sy;
			continue;
		}

		if (!strcmp(token, "yb"))
		{
			token = Com_Parse(&s);
			y = videoHeight() + atoi(token)*hud_sy;
			continue;
		}

		if (!strcmp(token, "yv"))
		{
			token = Com_Parse(&s);
			y = videoHeight() / 2 - (120 - atoi(token))*hud_sy;
			continue;
		}

		if (!strcmp(token, "pic")) // draw a pic from a stat number
		{
			token = Com_Parse(&s);
			value = cl.frame.playerstate.stats[atoi(token)];

			if (value >= MAX_IMAGES) { continue; }


			Draw_PicScaled(x, y, hud_sx, hud_sy, cl.configstrings[CS_IMAGES + value]);

			continue;
		}

		if (!strcmp(token, "client")) // draw a deathmatch client block
		{
			token = Com_Parse(&s);
			x = videoWidth() / 2 - (160 - atoi(token))*hud_sx;
			token = Com_Parse(&s);
			y = videoHeight() / 2 - (120 - atoi(token))*hud_sy;

			token = Com_Parse(&s);
			value = atoi(token);
			if (value >= MAX_CLIENTS || value < 0)
			{
				Com_Error(ERR_DROP, "client >= MAX_CLIENTS");
			}

			ci = &cl.clientinfo[value];

			token = Com_Parse(&s);
			int score = atoi(token);

			token = Com_Parse(&s);
			int ping = atoi(token);

			token = Com_Parse(&s);
			int time = atoi(token);

			DrawAltStringScaled(x + 32 * hud_sx, y, hud_sx, hud_sy, ci->name);

			Set_FontShader(true);
			Draw_StringScaled(x + 32 * hud_sx, y + 8 * hud_sy, hud_sx, hud_sy, va("Score:  %i", score));
			Draw_StringScaled(x + 32 * hud_sx, y + 16 * hud_sy, hud_sx, hud_sy, va("Ping:  %i", ping));
			Draw_StringScaled(x + 32 * hud_sx, y + 24 * hud_sy, hud_sx, hud_sy, va("Time:  %i", time));
			Set_FontShader(false);

			if (!ci->icon) { ci = &cl.baseclientinfo; }
			Draw_PicScaled(x, y, hud_sx, hud_sy, ci->iconname);
			continue;
		}

		if (!strcmp(token, "ctf"))// draw a ctf client block
		{
			char block[80];

			token = Com_Parse(&s);
			x = videoWidth() * 0.5 - 160 + atoi(token)*hud_sx;
			token = Com_Parse(&s);
			y = videoHeight() * 0.5 - 120 + atoi(token)*hud_sy;

			token = Com_Parse(&s);
			value = atoi(token);
			if (value >= MAX_CLIENTS || value < 0)
				Com_Error(ERR_DROP, "client >= MAX_CLIENTS");
			ci = &cl.clientinfo[value];

			token = Com_Parse(&s);
			int score = atoi(token);

			token = Com_Parse(&s);

			int ping = clamp(atoi(token), 0, 999);

			sprintf(block, "%3d %3d %-12.12s", score, ping, ci->name);

			if (value == cl.playernum)
			{
				DrawAltStringScaled(x, y, hud_sx, hud_sy, block);
			}
			else
			{
				Set_FontShader(true);
				Draw_StringScaled(x, y, hud_sx, hud_sy, block);
				Set_FontShader(false);
				continue;
			}
		}

		if (!strcmp(token, "picn"))
		{
			token = Com_Parse(&s);

			std::string bump("gfx/");
			bump = bump + token + ".tga";

			Draw_PicScaled(x, y, hud_sx, hud_sy, const_cast<char*>(bump.c_str()));	// EW!
			continue;
		}

		if (!strcmp(token, "num"))
		{	// draw a number
			token = Com_Parse(&s);
			width = atoi(token);
			token = Com_Parse(&s);
			value = cl.frame.playerstate.stats[atoi(token)];
			SCR_DrawField(x, y, 0, width, value);
			continue;
		}

		/*
		if (!strcmp(token, "hnum"))
		{	// health number
			int color = 0;

			width = 3;
			value = cl.frame.playerstate.stats[STAT_HEALTH];
			if (value > 25) { color = 0; }// green
			else if (value > 0) { color = (cl.frame.serverframe >> 2) & 1; }	// flash
			else { color = 1; }

			SCR_DrawField(x, y, color, width, value);
			continue;
		}

		if (!strcmp(token, "anum"))
		{	// ammo number
			int color;

			width = 3;
			value = cl.frame.playerstate.stats[STAT_AMMO];
			if (value > 5)
				color = 0;		// green
			else if (value >= 0)
				color = (cl.frame.serverframe >> 2) & 1;	// flash
			else
				continue;		// negative number = don't show

			SCR_DrawField(x, y, color, width, value);
			continue;
		}

		if (!strcmp(token, "rnum")) {	// armor number
			int color;

			width = 3;
			value = cl.frame.playerstate.stats[STAT_ARMOR];
			if (value < 1)
				continue;

			color = 0;			// green

			SCR_DrawField(x, y, color, width, value);
			continue;
		}
		*/

		if (!strcmp(token, "stat_string")) {

			token = Com_Parse(&s);
			index = atoi(token);
			if (index < 0 || index >= MAX_CONFIGSTRINGS)
				Com_Error(ERR_DROP, "Bad stat_string index");
			index = cl.frame.playerstate.stats[index];
			if (index < 0 || index >= MAX_CONFIGSTRINGS)
				Com_Error(ERR_DROP, "Bad stat_string index");
			Set_FontShader(true);
			Draw_StringScaled(x, y, hud_sx, hud_sy, cl.configstrings[index]);
			Set_FontShader(false);
			continue;
		}

		if (!strcmp(token, "cstring")) {
			token = Com_Parse(&s);
			SCR_DrawHUDString(x, y, hud_sx, hud_sy, 320, 0, token);
			continue;
		}

		if (!strcmp(token, "string")) {
			token = Com_Parse(&s);
			Set_FontShader(true);
			Draw_StringScaled(x, y, hud_sx, hud_sy, token);
			Set_FontShader(false);
			continue;
		}

		if (!strcmp(token, "cstring2")) {	// F1 messages upper block
			token = Com_Parse(&s);
			SCR_DrawHUDString(x, y, hud_sx, hud_sy, 320, 0x80, token);
			continue;
		}

		if (!strcmp(token, "string2")) {	// F1 messages lower block
			token = Com_Parse(&s);
			DrawAltStringScaled(x, y, hud_sx, hud_sy, token);
			continue;
		}

		if (!strcmp(token, "if")) {	// draw a number
			token = Com_Parse(&s);
			value = cl.frame.playerstate.stats[atoi(token)];
			if (!value) {		// skip to endif
				while (s && strcmp(token, "endif")) {
					token = Com_Parse(&s);
				}
			}

			continue;
		}


	}
}


/**
 * Draws a color modified bar based on a percentage.
 * 
 * \param	x			Screen X.
 * \param	y			Screen Y.
 * \param	width		Width of the bar in pixels.
 * \param	height		Height of the bar in pixels.
 * \param	percent		Percent of the bar full. Must be between 0.0f and 1.0f;
 * \param	alpha		Alpha value to draw the bar at. Default 1.0f. 0.0f - 1.0f.
 * \param	reversed	Bar is drawn Left to Right instead of the default Right to Left.
 * 
 * \throws	std::runtime_error	In debug builds will throw a std::runtime_error if
 *								percent is outside of 0.0f - 1.0f;
 */
static void drawBar(int x, int y, int width, int height, float percent, float alpha = 1.0f, bool reversed = false)
{
#if defined(_DEBUG)
	if (percent < 0.0f || percent > 1.0f)
	{
		throw std::runtime_error("drawBar():: percent parameter outside of bounds: " + std::to_string(percent));
	}
#endif

	float modifier = 1.0f - percent;
	if (reversed)
	{
		int _w = width * percent;
		Draw_BoxFilled(x + (width - _w), y, _w, height, 0.0f + modifier, 1.0f - modifier, 0.0f, alpha);
	}
	else
	{
		Draw_BoxFilled(x, y, width * percent, height, 0.0f + modifier, 1.0f - modifier, 0.0f, alpha);
	}
}


static void setStringDrawColor(int value, int threshold)
{
	if (value > threshold)
	{
		GL_SetFontColor(GetColorFromIndex(COLOR_WHITE));
	}
	else if (value)
	{
		((cl.frame.serverframe >> 2) & 1) != 0 ? GL_SetFontColor(GetColorFromIndex(COLOR_RED)) : GL_SetFontColor(GetColorFromIndex(COLOR_WHITE));
	}
	else
	{
		GL_SetFontColor(GetColorFromIndex(COLOR_RED));
	}
}


/**
 * The status bar is a small layout program that is based on the stats array
 * 
 * Yeah, useless commentary above. Anyway, this is where the primary HUD is drawn. The
 * configstrings[CS_STATUSBAR] comes from the Game module.
 */
void SCR_DrawStats()
{
	if (cl_drawhud->value == 0.0f) { return; }

	Draw_BoxFilled(10, videoHeight() - 75, 256, 64, 0.05f, 0.2f, 0.4f, 0.3f);
	Draw_BoxFilled(videoWidth() - 266, videoHeight() - 75, 256, 64, 0.05f, 0.2f, 0.4f, 0.3f);

	Draw_Pic(10, videoHeight() - 75, "hud/health.tga");

	Draw_Pic(videoWidth() - 266, videoHeight() - 75, "hud/weapons.tga");

	// ============================
	// BARS
	// ============================
	if (cl.frame.playerstate.stats[STAT_HEALTH] > 0)
	{
		drawBar(52, videoHeight() - 68, 200, 20, clamp(static_cast<float>(cl.frame.playerstate.stats[STAT_HEALTH]) / 100.0f, 0.0f, 1.0f), 0.85f);
	}

	if (cl.frame.playerstate.stats[STAT_HEALTH] > 0)
	{
		drawBar(52, videoHeight() - 37, 200, 20, clamp(static_cast<float>(cl.frame.playerstate.stats[STAT_ARMOR]) / 100.0f, 0.0f, 1.0f), 0.85f);
	}

	if (cl.frame.playerstate.stats[STAT_AMMO] >= 0 && cl.frame.playerstate.stats[STAT_AMMO_CAPACITY] > 0)
	{
		drawBar(videoWidth() - 259, videoHeight() - 68, 200, 20, clamp(static_cast<float>(cl.frame.playerstate.stats[STAT_AMMO]) / static_cast<float>(cl.frame.playerstate.stats[STAT_AMMO_CAPACITY]), 0.0f, 1.0f), 0.85f, true);
	}

	// ============================
	// STRINGS
	// ============================
	Set_FontShader(true);

	setStringDrawColor(cl.frame.playerstate.stats[STAT_HEALTH], HEALTH_FLASH_THRESHOLD);
	Draw_StringScaled(265, videoHeight() - 64, 2.0f, 2.0f, std::to_string(cl.frame.playerstate.stats[STAT_HEALTH]));

	//GL_SetFontColor(GetColorFromIndex(COLOR_WHITE));
	Draw_StringScaled(265, videoHeight() - 33, 2.0f, 2.0f, std::to_string(cl.frame.playerstate.stats[STAT_ARMOR]));

	if (cl.frame.playerstate.stats[STAT_AMMO] >= 0 && cl.frame.playerstate.stats[STAT_AMMO_CAPACITY] > 0)
	{
		setStringDrawColor(cl.frame.playerstate.stats[STAT_AMMO], cl.frame.playerstate.stats[STAT_AMMO_CAPACITY] * AMMO_FLASH_THRESHOLD);
		Draw_StringScaled(videoWidth() - (std::to_string(cl.frame.playerstate.stats[STAT_AMMO]).size() * 16) - 266, videoHeight() - 64, 2.0f, 2.0f, std::to_string(cl.frame.playerstate.stats[STAT_AMMO]));
	}

	GL_SetFontColor(GetColorFromIndex(COLOR_WHITE));

	Set_FontShader(false);
}


/**
 * 
 */
void SCR_DrawLayout()
{

}


/**
 * 
 */
void CL_ParseInventory ()
{
	for (int i = 0; i < MAX_ITEMS; i++)
	{
		cl.inventory[i] = MSG_ReadShort(Net_Message());
	}
}


/**
 * 
 */
void CL_DrawInventory()
{
	int i, j;
	int num, selected_num, item;
	int index[MAX_ITEMS];
	char string[1024];
	int x, y;
	char binding[1024];
	char* bind;
	int selected;
	int top;

	if (!(cl.frame.playerstate.stats[STAT_LAYOUTS] & 2))
		return;

	selected = cl.frame.playerstate.stats[STAT_SELECTED_ITEM];

	num = 0;
	selected_num = 0;
	for (i = 0; i < MAX_ITEMS; i++)
	{
		if (i == selected)
			selected_num = num;
		if (cl.inventory[i])
		{
			index[num] = i;
			num++;
		}
	}

	// determine scroll point
	top = selected_num - DISPLAY_ITEMS * 0.5;
	if (num - top < DISPLAY_ITEMS)
		top = num - DISPLAY_ITEMS;
	if (top < 0)
		top = 0;

	x = (videoWidth() - 256 * cl_fontScale->value) * 0.5;
	y = (videoHeight() - 240 * cl_fontScale->value) * 0.5;

	Draw_ScaledPic(x, y + 8, (float)cl_fontScale->value, (float)cl_fontScale->value, i_inventory);

	y += 24 * cl_fontScale->value;
	x += 24 * cl_fontScale->value;

	Set_FontShader(true);

	Inv_DrawString(x, y, "hotkey ### item");

	Inv_DrawString(x, y + 8 * cl_fontScale->value, "------ --- ----");

	y += 16 * cl_fontScale->value;

	for (i = top; i < num && i < top + DISPLAY_ITEMS; i++)
	{
		item = index[i];
		// search for a binding
		Q_sprintf(binding, sizeof(binding), "use %s",
			cl.configstrings[CS_ITEMS + item]);
		bind = "";
		for (j = 0; j < 256; j++)
			if (keybindings[j] && !_stricmp(keybindings[j], binding))
			{
				bind = Key_KeynumToString(j);
				break;
			}

		Q_sprintf(string, sizeof(string), "%6s %3i %s", bind, cl.inventory[item], cl.configstrings[CS_ITEMS + item]);
		if (item != selected)
		{
			SetStringHighBit(string);
		}
		else					// draw a blinky cursor by the selected item
		{
			if ((int)(cls.realtime * 10) & 1)
				Draw_CharScaled(x - 8, y, cl_fontScale->value, cl_fontScale->value, 15);

		}
		Inv_DrawString(x, y, string);
		y += 8 * cl_fontScale->value;
	}

	Set_FontShader(false);
}
