/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
#pragma once

#include "Particle.h"

#include "../common/common.h"

#include <string>

#define	API_VERSION				3

#define DECAL_BULLET			0
#define DECAL_BLASTER			1
#define DECAL_EXPLODE			2
#define DECAL_RAIL				3
#define DECAL_BLOOD1			4
#define DECAL_BLOOD2			5
#define DECAL_BLOOD3			6
#define DECAL_BLOOD4			7
#define DECAL_BLOOD5			8
#define DECAL_BLOOD6			9
#define DECAL_BLOOD7			10
#define DECAL_BLOOD8			11
#define DECAL_BLOOD9			12
#define DECAL_ACIDMARK			13
#define DECAL_BFG				14


#define DECAL_MAX				15

#define DF_SHADE				0x00000400	// 1024
#define DF_NOTIMESCALE			0x00000800	// 2048
#define INSTANT_DECAL			-10000.0
#define DF_OVERBRIGHT			1
#define DF_VERTEXLIGHT			2
#define DF_AREADECAL			4

#define	MAX_DLIGHTS				32
#define	MAX_ENTITIES			128
#define	MAX_PARTICLES			4096
#define	MAX_LIGHTSTYLES			256


#define MAX_RADAR_ENTS			512


#define	VERTEXSIZE	16

#define MAX_DECALS				8192
#define MAX_DECAL_VERTS			384
#define MAX_DECAL_FRAGMENTS		256

//
// msurface_t->flags
//
#define	MSURF_PLANEBACK		0x2
#define	MSURF_DRAWSKY		0x4
#define MSURF_DRAWTURB		0x10
//#define MSURF_DRAWBACKGROUND	0x40
#define MSURF_UNDERWATER		0x80
//#define MSURF_ENVMAP		0x100
//#define MSURF_DETAIL		0x200
#define MSURF_WATER      	0x400
#define MSURF_SLIME      	0x800
#define MSURF_LAVA       	0x1000

#define POWERSUIT_SCALE		4.0F

#define SHELL_RED_COLOR		0xF2
#define SHELL_GREEN_COLOR	0xD0
#define SHELL_BLUE_COLOR	0xF3

#define SHELL_RG_COLOR		0xDC
//#define SHELL_RB_COLOR        0x86
#define SHELL_RB_COLOR		0x68
#define SHELL_BG_COLOR		0x78

//ROGUE
#define SHELL_DOUBLE_COLOR	0xDF	// 223
#define	SHELL_HALF_DAM_COLOR	0x90
#define SHELL_CYAN_COLOR	0x72
//ROGUE

#define SHELL_WHITE_COLOR	0xD7

#define	GL_INDEX_TYPE		GL_UNSIGNED_SHORT

#define	MAX_FRAME_BUFFERS	32


typedef unsigned short		ushort;
typedef ushort				index_t;


extern vec3_t r_origin;
extern int NUM_VIS_LIGHTS;
extern float loadScreenColorFade;


void Set_FontShader (bool enable);

typedef struct entity_s
{
	struct Model* model;				// opaque type outside refresh

	float angles[3];
	mat3_3_t	axis;			// entity -> world space
	mat4_t	orMatrix, matrix;

	bool angleMod;
	/*
	 ** most recent data
	 */
	float	origin[3];				// also used as RF_BEAM's "from"
	int		frame;					// also used as RF_BEAM's diameter
	int		framecount;				// for vis calc

	/*
	 ** previous data for lerping
	 */
	float oldorigin[3];			// also used as RF_BEAM's "to"
	int oldframe;
	
	// iqm
	int iqmFrameTime;
	/*
	 ** misc
	 */
	float backlerp;				// 0.0 = current, 1.0 = old
	int skinnum;				// also used as RF_BEAM's palette index

	int lightstyle;				// for flashing entities
	float alpha;				// ignore if RF_TRANSLUCENT isn't set

	Image* skin;		// nullptr for inline skin
	int flags;
	vec3_t color;

	vec3_t lightvector;
	Image* bump;
	float shadelight[3];
	float minmax[6];
	vec3_t mins;
	vec3_t maxs;
	bool lightVised;
	byte vis[MAX_MAP_LEAFS / 8];

} entity_t;

#define ENTITY_FLAGS  68

typedef struct
{
	vec3_t origin, color, angles;
	float intensity, _cone;
	int filter;
	bool spotlight;
} dlight_t;


typedef struct
{
	float rgb[3];	// 0.0 - 2.0
	float white;	// highest of rgb
} lightstyle_t;


typedef struct
{
	float strength;
	vec3_t direction;
	vec3_t color;
} m_dlight_t;


typedef struct mtexInfo_s
{
	float vecs[2][4];			/**< Document Me */
	int flags;					/**< Document Me */
	int numFrames;				/**< Document Me */
	struct mtexInfo_s *next;	/**< Next texture in the animation chain. */

	Image* additive;			/**< Additive / Glow map texture. */
	Image* diffuse;			/**< Base diffuse texture. */
	Image* environment;		/**< Environment texture. */
	Image* normal;			/**< Normal map texture. */
	Image* roughness;			/**< Roughness map texture. */

	int value;					/**< Document Me */
} mtexInfo_t;


typedef struct glpoly_s
{
	struct	glpoly_s	*next;
	struct	glpoly_s	*chain;
	struct	glpoly_s	**neighbours;

	vec3_t	normal;
	vec3_t	center;
	int		lightTimestamp;
	int		shadowTimestamp;
	int		ShadowedFace;
	int		numVerts;
	int		flags;
	float	verts[4][VERTEXSIZE];	// variable sized (xyz s1t1 s2t2)
} glpoly_t;


/**
 * Temporary storage for polygons that use an edge.
 * 
 * \fixme	Is this really necessary?
 */
typedef struct
{
	byte		used;		/**< how many polygons use this edge */
	glpoly_t	*poly[2];	/**< pointer to the polygons who use this edge */
} temp_connect_t;


typedef struct msurface_s {
	int visframe;				// should be drawn when node is crossed

	cplane_t *plane;
	int flags;

	int firstedge;				// look up in model->surfEdges[], negative
	// numbers
	int numEdges;				// are backwards edges

	short texturemins[2];
	short extents[2];

	int light_s, light_t;		// gl lightmap coordinates
	int dlight_s, dlight_t;		// gl lightmap coordinates for dynamic
	// lightmaps

	glpoly_t *polys;			// multiple if warped

	struct msurface_s *texturechain;
	struct msurface_s *lightmapchain;

	mtexInfo_t *texInfo;

	float c_s, c_t;

	// lighting info
	int dlightframe;
	int dlightbits;

	int lightmaptexturenum;

	byte styles[MAXLIGHTMAPS];
	float cached_light[MAXLIGHTMAPS];	// values currently used in
	// lightmap
	byte *samples;				// [numstyles * surfsize * 3] for vanilla or [numstyles * 3 * surfsize * 3] for XP lightmaps

	int checkCount;
	vec3_t center;

	struct msurface_s *fogchain;
	int fragmentframe;
	entity_t *ent;
	vec3_t		mins, maxs;

	int	numIndices;
	int	numVertices;
	index_t	*indices;
	unsigned int sort;

	//vbo
	size_t vbo_pos;
	int	xyz_size;
	int st_size;
	int lm_size;

	int	baseIndex;
	int numIdx;

} msurface_t;


typedef struct mnode_s {
	// common with leaf
	int contents;				// -1, to differentiate from leafs
	int visframe;				// node needs to be traversed if current

	float minmaxs[6];			// for bounding box culling

	struct mnode_s *parent;

	// node specific
	cplane_t *plane;
	struct mnode_s *children[2];

	unsigned short firstsurface;
	unsigned short numsurfaces;
} mnode_t;


typedef struct decals_t {
	struct decals_t *prev, *next;
	mnode_t *node;

	float time, endTime;

	int numverts;
	int numIndices;
	index_t	*indices;

	vec3_t verts[MAX_DECAL_VERTS];
	vec2_t stcoords[MAX_DECAL_VERTS];
	vec3_t color;
	vec3_t endColor;
	float alpha;
	float endAlpha;
	float size;
	vec3_t org;
	int type;
	int flags;
	int sFactor;
	int dFactor;
} decals_t;



typedef struct
{
	mnode_t *node;
	msurface_t *surf;

	int firstvert;
	int numverts;
} fragment_t;


//================
// end decals
//================




typedef struct
{
	unsigned int	format;
	unsigned int	id;
	int				width;
	int				height;
} rbo_t;


typedef struct
{
	char			name[MAX_QPATH];
	int				index;	// in rg.fbs
	unsigned int	id;
} fbo_t;



class refdef_t
{
public:
	vec2_t	depthParms = { 0.0f };					/**<  */

	int				x = 0;							/**<  */
	int				y = 0;							/**<  */
	int				width = 0;						/**<  */
	int				height = 0;						/**< in virtual screen coordinates */

	float			fov_x = 0.0f;
	float			fov_y = 0.0f;					/**<  */
	float			vieworg[3] = { 0.0f };			/**<  */
	float			vieworg_old[3] = { 0.0f };		/**<  */
	float			viewangles[3] = { 0.0f };
	float			viewangles_old[3] = { 0.0f };	/**<  */

	vec4_t			blend = { 0.0f };				/**< rgba 0-1 full screen blend */
	float			time = 0.0f;					/**< time is uesed to auto animate */
	int				rdflags = 0;					/**< RDF_UNDERWATER, etc */
	bool			mirrorView = false;
	byte*			areabits = nullptr;				/**< if not nullptr, only areas with set bits will be drawn */

	// world bounds
	vec3_t			visMins = { 0.0f };				/**<  */
	vec3_t			visMaxs = { 0.0f };				/**<  */

	lightstyle_t*	lightstyles = nullptr;			/**< [MAX_LIGHTSTYLES] */

	// viewport
	int				viewport[4] = { 0 };			/**<  */
	vec3_t			cornerRays[4] = { 0.0f };		/**<  */
	mat3_3_t		axis = { 0.0f };				/**<  */
	mat4_t			projectionMatrix = { 0.0f };	/**<  */
	mat4_t			orthoMatrix = { 0.0f };			/**<  */
	mat4_t			modelViewMatrix = { 0.0f };		/**<  */
	mat4_t			modelViewProjectionMatrix = { 0.0f };			/**<  */
	mat4_t			modelViewProjectionMatrixTranspose = { 0.0f };	/**<  */
	mat4_t			prevMVP = { 0.0f };				/**<  */
	mat4_t			skyMatrix = { 0.0f };			/**<  */

	int				num_entities = 0;				/**<  */
	entity_t*		entities = nullptr;				/**<  */

	int				num_dlights = 0;				/**<  */
	dlight_t*		dlights = nullptr;				/**<  */

	int				num_particles = 0;				/**<  */
	Particle*		particles = nullptr;			/**<  */

	int				numDecals = 0;					/**<  */
	decals_t*		decals = nullptr;				/**<  */

	int				numFBs = 0;
	fbo_t*			fbs[MAX_FRAME_BUFFERS] = { 0 };	/**<  */
	fbo_t*			screenFB = nullptr;				/**<  */
	fbo_t*			hdrFB = nullptr;				/**<  */

	Image*			depthBufferImage = nullptr;		/**< depth24-stencil8 format */
	Image*			colorBufferImage = nullptr;		/**<  screen texture */

};


//
// these are the functions exported by the refresh module
//

int R_GetClippedFragments(vec3_t origin, float radius, mat3_3_t axis, int maxfverts, vec3_t * fverts, int maxfragments, fragment_t * fragments);
void Draw_Pic(int x, int y, char* name);
void Draw_Pic2(int x, int y, Image* gl);
void Draw_StretchPic(int x, int y, int w, int h, Image* gl);
void Draw_StretchPic(int x, int y, int w, int h, char* name);


void Draw_PicScaled(int x, int y, float scale_x, float scale_y, char* pic);
void Draw_PicBumpScaled(int x, int y, float scale_x, float scale_y, char* pic, char* pic2);
void Draw_PicBump(int x, int y, char* pic, char* pic2);
void Draw_ScaledPic(int x, int y, float scale_x, float scale_y, Image*  gl);

void Draw_Char(int x, int y, unsigned char num);
void Draw_CharScaled(int x, int y, float scale_x, float scale_y, unsigned char num);
void Draw_String(int x, int y, const std::string& str);
void Draw_StringScaled(int x, int y, float scale_x, float scale_y, const std::string& str);

void Draw_GetPicSize(int& w, int& h, const std::string& name);

void R_BeginRegistration(char* map);
void R_SetSky(char* name, float rotate, vec3_t axis);
void R_EndRegistration();
void R_RenderFrame(refdef_t * fd);


void R_Shutdown();
bool R_CullPoint(vec3_t org);
bool R_Init(void *hinstance, void *wndproc);
Image* Draw_FindPic(const std::string& name);
Model* R_RegisterModel(char* name);
void R_BeginFrame();
void GLimp_EndFrame();
void GLimp_AppActivate(bool active);
void VID_NewWindow(int width, int height);
bool VID_GetModeInfo(int *width, int *height, int mode);

//
// these are the functions imported by the refresh module
//
/// \fixme	Have the refresh module import these via header imports instead of having references to these everywhere, makes maintenance a nightmare.
void Con_Printf(int print_level, char* str, ...);
void Cmd_AddCommand(char* name, void(*cmd) ());
void Cmd_RemoveCommand(char* name);
int Cmd_Argc();
char* Cmd_Argv(int i);
void VID_Error(int err_level, char* str, ...);
void Cbuf_ExecuteText(int exec_when, char* text);
