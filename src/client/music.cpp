#include "music.h"

#include "client.h"
#include "snd_loc.h"

#include "../common/common.h"
#include "../common/shared.h"
#include "../common/filesystem.h"
#include "../common/string_util.h"

#include <string>

#include <vorbis/vorbisfile.h>

// ===============================================================================
// = FUNCTION POINTER PROTOTYPES
// ===============================================================================

/**
 * Function pointer declaration for read function.
 *
 * \param Pointer to an audio type interface structure.
 * \param Pointer to audio data buffer.
 * \param Data buffer size.
 */
typedef int(*readFunc_t)(void *, void *, int);


/**
 * Function pointer declaration for rewind function.
 */
typedef void(*rewindFunc_t)(void *);


/**
 * Function pointer declaration for close function.
 */
typedef void(*closeFunc_t)(void *);


// ===============================================================================
// = STRUCT'S
// ===============================================================================

/**
 *
 */
typedef struct
{
	int bits;
	int channels;
	int rate;
} MusicParameters;


/**
 * Provides a generic interface for working with
 * music audio files.
 */
typedef struct
{
	// public:
	readFunc_t read;		/**< Read audio buffer. */
	rewindFunc_t rewind;	/**< Reset audio position. */
	closeFunc_t close;		/**< Close audio buffer. */

	//private:
	void* f;				/**<  */
	const char* ext;		/**< Audio file extension. */
} AudioInterface;

typedef AudioInterface* (*openFunc_t)(const char*, MusicParameters*);


/**
 *
 */
typedef struct
{
	std::string name;
	openFunc_t open;
} AudioLoader;


/**
 * OGG format interface.
 */
typedef struct
{
	void* byte_stream;
	int pos;
	int size;

	OggVorbis_File ovFile;
	vorbis_info* info;
} OGG_t;


/**
 * WAV/RIFF format interface.
 */
typedef struct
{
	byte *data;
	byte *start;

	int pos;
	int size;
} WAV_t;


// ===============================================================================
// = FUNCTION PROTOTYPES
// ===============================================================================
AudioInterface* Gen_Open(const char* name, MusicParameters *sp);
AudioInterface* Gen_OpenAny(const char* name, MusicParameters *sp);
AudioInterface* Impl_Open_OGG(const char* name, MusicParameters *sp);

bool Music_PlayFile(const char* name, bool hasExt);


// ===============================================================================
// = ENUMERATORS
// ===============================================================================

/**
 * Music play status.
 */
typedef enum 
{
	STATUS_STOPPED,
	STATUS_PAUSED,
	STATUS_PLAYING
} MusicStatus;


// ===============================================================================
// = LOCAL VARIABLES
// ===============================================================================
static MusicStatus MUSIC_STATUS = STATUS_STOPPED;
static AudioInterface *MUSIC_HANDLE;
static byte MUSIC_BUFFER[MAX_STRBUF_SIZE];

static char** FS_LIST;
static int FS_INDEX = 0;
static int FS_NUM_FILES = 0;

static const std::string MUSIC_DIRECTORY = "music/";


AudioLoader SUPPORTED_TYPES[] =
{
	{ "ogg", (openFunc_t)Impl_Open_OGG }
};



// ===============================================================================
// = FUNCTIONS
// ===============================================================================

/**
 * Initializes music subsystem.
 * 
 * \fixme	Currently only plays a list of music files in order or randomly,
 *			should be allowed to specify a particular file for a given map
 *			or even any particular time in the code (e.g., menu music, level
 *			change, loading, etc.)
 */
void Music_Init()
{
	MUSIC_STATUS = STATUS_STOPPED;

	Com_Printf("\n" BAR_BOLD" \n");
	Com_Printf(S_COLOR_YELLOW "Initializing Music Subsystem\n");
	Com_Printf(BAR_BOLD "\n\n");

	FS_LIST = FS_ListFiles("/music", &FS_NUM_FILES, false);
	FS_INDEX = -1;

	if (FS_LIST != nullptr)
		Com_DPrintf(S_COLOR_YELLOW "found " S_COLOR_GREEN "%d " S_COLOR_YELLOW "music files\n\n", FS_NUM_FILES);

	Com_Printf(BAR_BOLD "\n\n");
}


/**
 *
 */
void Music_Shutdown()
{
	Com_Printf("** Music Shutdown **\n");
	Music_Stop();
	FS_FreeList(FS_LIST);
}


/**
 * 
 */
void Music_Play()
{
	Music_Stop();

	if (FS_NUM_FILES == 0)
	{
		return;
	}

	if (s_musicrandom->value != 0)
	{
		FS_INDEX = rand() % FS_NUM_FILES;
	}
	else
	{
		FS_INDEX = (FS_INDEX + 1) % FS_NUM_FILES;
	}

	int count = FS_NUM_FILES;
	while (count-- > 0)
	{
		if (Music_PlayFile(FS_LIST[FS_INDEX], true))
		{
			return;
		}

		FS_INDEX = (FS_INDEX + 1) % FS_NUM_FILES;
	}
}


/**
 * 
 */
void Music_Stop()
{
	if (MUSIC_STATUS == STATUS_STOPPED)
	{
		return;
	}

	Com_DPrintf(S_COLOR_GREEN "Stopped playing music\n");

	MUSIC_HANDLE->close(MUSIC_HANDLE->f);
	S_Streaming_Stop();

	MUSIC_STATUS = STATUS_STOPPED;
}


/**
 * 
 */
void Music_Pause()
{
	if (MUSIC_STATUS != STATUS_PLAYING)
	{
		return;
	}

	alSourcePause(source_name[CH_STREAMING]);
	MUSIC_STATUS = STATUS_PAUSED;
}


/**
 * 
 */
void Music_Resume()
{
	if (MUSIC_STATUS != STATUS_PAUSED)
	{
		return;
	}

	alSourcePlay(source_name[CH_STREAMING]);
	MUSIC_STATUS = STATUS_PLAYING;
}


/**
 * 
 */
void Music_Update()
{
	// if we are in the configuration menu, or paused we don't do anything
	if (MUSIC_STATUS == STATUS_PAUSED || MUSIC_STATUS == STATUS_STOPPED)
	{
		return;
	}

	// Check for configuration changes
	if (s_musicsrc->modified)
	{
		Music_Shutdown();
		Music_Init();
		Music_Play();
		s_musicsrc->modified = false;
		s_musicvolume->modified = false;
		s_musicrandom->modified = false;
		return;
	}

	if (s_musicrandom->modified)
	{
		s_musicrandom->modified = false;
		Music_Play();
		return;
	}

	if (s_musicvolume->modified)
	{
		alSourcef(source_name[CH_STREAMING], AL_GAIN, s_musicvolume->value);
		s_musicvolume->modified = false;
		return;
	}

	// Do the actual update
	if (MUSIC_STATUS != STATUS_PLAYING || S_Streaming_NumFreeBufs() == 0)
	{
		return;
	}

	// Play a portion of the current file
	int n = MUSIC_HANDLE->read(MUSIC_HANDLE->f, MUSIC_BUFFER, sizeof(MUSIC_BUFFER));
	if (n == 0)
	{
		Music_Play();
	}
	else // don't check return value as the buffer is guaranteed to fit        <---- We sure about that?
	{
		S_Streaming_Add(MUSIC_BUFFER, n);
	}
}


// ===============================================================================
// = INTERNAL IMPLEMENTATION FUNCTIONS
// ===============================================================================

/**
 * 
 */
static bool Music_PlayFile(const char* name, bool hasExt)
{
	MusicParameters sp;

	if (hasExt) { MUSIC_HANDLE = Gen_Open(name, &sp); }
	else { MUSIC_HANDLE = Gen_OpenAny(name, &sp); }

	if (MUSIC_HANDLE)
	{
		if (hasExt)
		{
			Com_DPrintf(S_COLOR_GREEN "Music_Play: playing \"%s\"\n", name);
		}
		else
		{
			Com_DPrintf(S_COLOR_GREEN "Music_Play: playing \"%s.%s\"\n", name, MUSIC_HANDLE->ext);
		}

		S_Streaming_Start(sp.bits, sp.channels, sp.rate, s_musicvolume->value);
		MUSIC_STATUS = cl_paused->value ? STATUS_PAUSED : STATUS_PLAYING;
		return true;
	}
	else
	{
		Com_Printf(S_COLOR_YELLOW "Music_Play: unable to load \"%s\"\n", name);
		return false;
	}
}


/**
 * Try all possible extensions of filename in sequence
 */
static AudioInterface* Gen_OpenAny(const char* name, MusicParameters *sp)
{
	for (int i = 0; i < 1; i++)	/// \fixme	Magic number 1.
	{
		char path[MAX_QPATH];
		Q_snprintfz(path, sizeof(path), "%s.%s", name, SUPPORTED_TYPES[i].name);

		return SUPPORTED_TYPES[i].open(path, sp);
	}

	return nullptr;
}


/**
 * Check given filename and call appropiate routine
 */
static AudioInterface* Gen_Open(const char* name, MusicParameters* sp)
{
	const std::string fname = name;
	const std::string extention = fname.substr(fname.find_last_of(".") + 1);

	if (extention.empty())
	{
		return nullptr;
	}

	Com_DPrintf("... Trying to load %s\n", name);

	const std::string _fpath = MUSIC_DIRECTORY + name;

	for (int i = 0; i < 1; ++i)	/// \fixme	Magic number 1.
	{
		if (toLowercase(extention) == SUPPORTED_TYPES[i].name)
		{
			return SUPPORTED_TYPES[i].open(_fpath.c_str(), sp);
		}
	}

	return nullptr;
}


/**
 * Implements read functionality for OGG format files.
 */
static int Impl_Read_OGG(OGG_t* f, char* buffer, int n)
{
	const int step = 1024 * 64;

	assert(step < n); /// \fixme Not sure I like asserts in code

	int total = 0;
	while (total + step < n)
	{
		/// \fixme check endianess
		int cur = ov_read(&f->ovFile, buffer + total, step, 0, 2, 1, &f->pos);
		if (cur < 0) { return 0; }
		if (cur == 0) { return total; }

		total += cur;
	}

	return total;
}


/**
 * Implements rewind functionality for OGG format files.
 */
static void Impl_Rewind_OGG(OGG_t* f)
{
	f->pos = 0;
}


/**
 * Implements close functionality for OGG format files.
 */
static void Impl_Close_OGG(OGG_t* f)
{
	ov_clear(&f->ovFile);
	FS_FreeFile(f->byte_stream);
}


/**
 * Implements open functionality for OGG format files.
 */
static AudioInterface* Impl_Open_OGG(const char* name, MusicParameters *sp)
{
	static OGG_t f;
	static AudioInterface res;

	f.size = FS_LoadFile(name, &f.byte_stream);
	if (f.size == FS_OPEN_FAILED)
	{
		return nullptr;
	}

	if (ov_open(nullptr, &f.ovFile, reinterpret_cast<char*>(f.byte_stream), f.size) == 0)
	{
		f.info = ov_info(&f.ovFile, 0);
		f.pos = 0;

		sp->bits = 16;	/// \fixme	MAGIC NUMBER
		sp->channels = f.info->channels;
		sp->rate = f.info->rate;

		res.read = (readFunc_t)Impl_Read_OGG;
		res.rewind = (rewindFunc_t)Impl_Rewind_OGG;
		res.close = (closeFunc_t)Impl_Close_OGG;
		res.f = &f;
		res.ext = "ogg";

		return &res;
	}
	else
	{
		FS_FreeFile(f.byte_stream);
	}

	return nullptr;
}
