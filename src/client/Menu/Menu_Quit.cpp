#include "menu.h"

#include "../client.h"
#include "../snd_loc.h"

// ===============================================================================
// = QUIT MENU
// ===============================================================================

#define HIGHLIGHT_YES		0
#define HIGHLIGHT_NO		1

static int HIGHLIGHT = NO_SELECTION;

Rectangle2D_t QUIT_MENU_RECTS[2] = 
{
	{ 0, 0, 64, 32 },
	{ 0, 0, 64, 32 }
};


/**
 * 
 */
int M_Quit_Key(int key)
{
	switch (key)
	{
	case K_ESCAPE:
	case 'n':
	case 'N':
		M_PopMenu();
		break;

	case 'Y':
	case 'y':
		cls.key_dest = KEY_CONSOLE;
		CL_Quit_f();
		break;

	case K_ENTER:
	case K_KP_ENTER:
	case K_MOUSE1:
		if (HIGHLIGHT == HIGHLIGHT_YES) { CL_Quit_f(); }
		else if (HIGHLIGHT == HIGHLIGHT_NO) { M_PopMenu(); }
		break;

	default:
		break;
	}

	return 0;
}


/**
 * 
 */
void M_Quit_Draw()
{
	Draw_Pic(videoCenterScreenX() - 256, 0, "gfx/logo.png");
	Draw_Pic(videoCenterScreenX() - 256, videoCenterScreenY() - 100, "gfx/leaving.tga");
	
	bool mouseInItem = false;

	int mx = 0, my = 0;
	getMousePosition(&mx, &my);


	for (int i = 0; i < 2; i++)
	{
		if (PointInRectangle(mx, my, &QUIT_MENU_RECTS[i]))
		{
			mouseInItem = true;
			if (i != HIGHLIGHT)
			{
				HIGHLIGHT = i;
				S_StartLocalSound(fastsound_descriptor[MENU_MOVE_SOUND]);
			}
		}
	}

	if (mouseInItem)
	{
		Draw_BoxFilled(QUIT_MENU_RECTS[HIGHLIGHT].x, QUIT_MENU_RECTS[HIGHLIGHT].y, QUIT_MENU_RECTS[HIGHLIGHT].w, QUIT_MENU_RECTS[HIGHLIGHT].h, HIGHLIGHT_COLOR[0], HIGHLIGHT_COLOR[1], HIGHLIGHT_COLOR[2], HIGHLIGHT_COLOR[3]);
	}
	else
	{
		HIGHLIGHT = NO_SELECTION;
	}

	Draw_Pic(QUIT_MENU_RECTS[0].x, QUIT_MENU_RECTS[0].y, "gfx/yes.tga");
	Draw_Pic(QUIT_MENU_RECTS[1].x, QUIT_MENU_RECTS[1].y, "gfx/no.tga");
}


/**
 * 
 */
void M_Menu_Quit_f()
{
	QUIT_MENU_RECTS[0].x = videoCenterScreenX() - 256;
	QUIT_MENU_RECTS[0].y = videoCenterScreenY() + 50;

	QUIT_MENU_RECTS[1].x = videoCenterScreenX() + 192;
	QUIT_MENU_RECTS[1].y = videoCenterScreenY() + 50;

	M_PushMenu(M_Quit_Draw, M_Quit_Key);
}
