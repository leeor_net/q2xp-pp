#include "menu.h"

#include "../client.h"
#include "../snd_loc.h"


#include <array>
#include <string>


static int M_MAIN_CURSOR = NO_SELECTION;

void M_Menu_Quit_f();


/**
 * Perform client side actions and prepare the game
 * to go into action.
 */
void gamePreStart()
{
	// disable updates and start the cinematic going
	cl.servercount = -1;
	M_ForceMenuOff();
	Cvar_SetValue("deathmatch", 0);
	Cvar_SetValue("coop", 0);
	Cvar_SetValue("gamerules", 0);	// PGM
	cls.key_dest = KEY_GAME;
}


void startMap00()
{
	gamePreStart();
	Cbuf_AddText("map floyd_test\n");
}


void startMap01()
{
	gamePreStart();
	Cbuf_AddText("map fdm1\n");
}


void startMap02()
{
	gamePreStart();
	Cbuf_AddText("map test3\n");
}


// ===============================================================================
// = MAIN MENU
// ===============================================================================
#define	MAIN_ITEMS	4

char* MAIN_MENU_IMAGES[MAIN_ITEMS] =
{
	"gfx/map_00.tga",
	"gfx/map_01.tga",
	"gfx/map_02.tga",
	"gfx/main_quit.tga"
};

Rectangle2D_t MAIN_MENU_OPTIONS[MAIN_ITEMS] =
{
	{ 0, 100, 256, 256 },
	{ 0, 100, 256, 256 },
	{ 0, 100, 256, 256 },
	{ 0, 100, 512, 64 },
};


std::array<const std::string, 3> TEST_MAP_DESCRIPTIONS = 
{
	"FLOYD_TEST: Floyd's first test map for Rogue Arena.",
	"FDM1: One of Floyd's deathmatch maps for Quake 2 ported to Rogue Arena and retextured.",
	"TEST3: 3rd developer test map demonstrating dynamic lighting and the PBR based texturing."
};


typedef void(*void_func_ptr)();

void_func_ptr MAIN_MENU_FUNCS[MAIN_ITEMS] =
{
	startMap00,
	startMap01,
	startMap02,
	M_Menu_Quit_f
};


/**
* Draws the main menu.
*
* Also handles basic mouse input for the menu.
*/
void M_Main_Draw()
{
	int mx = 0, my = 0;
	getMousePosition(&mx, &my);

	bool mouseInItem = false;

	for (int i = 0; i < MAIN_ITEMS; i++)
	{
		Draw_Pic(MAIN_MENU_OPTIONS[i].x, MAIN_MENU_OPTIONS[i].y, MAIN_MENU_IMAGES[i]);

		if (PointInRectangle(mx, my, &MAIN_MENU_OPTIONS[i]))
		{
			mouseInItem = true;
			if (i != M_MAIN_CURSOR)
			{
				M_MAIN_CURSOR = i;
				S_StartLocalSound(fastsound_descriptor[MENU_MOVE_SOUND]);
			}
		}
	}

	if (mouseInItem)
	{
		Draw_BoxFilled(MAIN_MENU_OPTIONS[M_MAIN_CURSOR].x, MAIN_MENU_OPTIONS[M_MAIN_CURSOR].y, MAIN_MENU_OPTIONS[M_MAIN_CURSOR].w, MAIN_MENU_OPTIONS[M_MAIN_CURSOR].h, HIGHLIGHT_COLOR[0], HIGHLIGHT_COLOR[1], HIGHLIGHT_COLOR[2], HIGHLIGHT_COLOR[3]);

		if (M_MAIN_CURSOR != 3)
		{
			Set_FontShader(true);
			Draw_String(videoCenterScreenX() - ((TEST_MAP_DESCRIPTIONS[M_MAIN_CURSOR].size() * 8) / 2), 50, TEST_MAP_DESCRIPTIONS[M_MAIN_CURSOR]);
			Set_FontShader(false);
		}
	}
	else
	{
		M_MAIN_CURSOR = NO_SELECTION;
	}
}


int M_Main_Key(int key)
{
	switch (key)
	{
	case K_ESCAPE:
		M_PopMenu();
		break;

	case K_KP_ENTER:
	case K_ENTER:
	case K_MOUSE1:
		if (M_MAIN_CURSOR != NO_SELECTION)
		{
			playMenuSound();
			MAIN_MENU_FUNCS[M_MAIN_CURSOR]();
		}
		break;
	}

	return 0;
}


void M_Menu_Main_f()
{
	MAIN_MENU_OPTIONS[1].x = videoCenterScreenX() - 128;

	MAIN_MENU_OPTIONS[0].x = MAIN_MENU_OPTIONS[1].x - (videoWidth() / 3);
	MAIN_MENU_OPTIONS[2].x = MAIN_MENU_OPTIONS[1].x + (videoWidth() / 3);

	// Quit option
	MAIN_MENU_OPTIONS[3].x = videoCenterScreenX() - 256;
	MAIN_MENU_OPTIONS[3].y = videoHeight() - 300;

	M_PushMenu(M_Main_Draw, M_Main_Key);
	showPointer();
}
