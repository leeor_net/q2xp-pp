#pragma once

#include "../qmenu.h"

#include "../../common/colordef.h"
#include "../../common/common.h"

#include "../../ref_gl/r_local.h"


#define NO_SELECTION	-1


void playMenuSound();

int Default_MenuKey(menuframework_s * m, int key);
void M_PushMenu(void(*draw) (), int(*key) (int k));
void M_ForceMenuOff();
void M_PopMenu();

void M_Banner(char* name);

extern color4f_t HIGHLIGHT_COLOR;
extern char* NAMES_YES_NO[];
