/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
#include <ctype.h>

#include "menu.h"

#include "../client.h"
#include "../qmenu.h"
#include "../snd_loc.h"

#include "../../common/common.h"
#include "../../common/filesystem.h"
#include "../../common/def_refdef.h"
#include "../../common/def_renderfx.h"
#include "../../common/string_util.h"

#include "../../ref_gl/r_local.h"

// ===============================================================================
// = DEFINES
// ===============================================================================
#define	MAX_MENU_DEPTH	8


// ===============================================================================
// = FUNCTION PROTOTYPES
// ===============================================================================
void M_Menu_Main_f();
void M_Menu_Quit_f();


// ===============================================================================
// = IMPORT FUNCTION PROTOTYPES
// ===============================================================================
void R_MenuBackGround();


// ===============================================================================
// = FUNCTION POINTER PROTOTYPES
// ===============================================================================
void(*m_drawfunc)();
int(*m_keyfunc)(int key);


// ===============================================================================
// = STRUCT'S
// ===============================================================================
/**
 * 
 */
typedef struct
{
	void(*draw)();
	int(*key)(int k);
} menulayer_t;


// ===============================================================================
// = LOCAL MODULE VARIABLES
// ===============================================================================
static int MENU_DEPTH;

static bool PLAY_ENTER_SOUND;	// play after drawing a frame so caching won't disrupt sound

Model* currentPlayerWeapon;

menulayer_t MENU_LAYERS[MAX_MENU_DEPTH];

color4f_t HIGHLIGHT_COLOR = { 0.65f, 0.39f, 0.06f, 0.35f };


char* NAMES_YES_NO[] =
{
	"No",
	"Yes",
	0
};


// ===============================================================================
// = EXPORT FUNCTIONS
// ===============================================================================
void playMenuSound()
{
	PLAY_ENTER_SOUND = true;
}


// ===============================================================================
// = SUPPORT FUNCTIONS
// ===============================================================================
/**
 * 
 */
void M_Banner(char* name)
{
	int w = 0, h = 0;
	Draw_GetPicSize(w, h, name);
	Draw_Pic((videoWidth() / 2) - (w / 2), h, name);
}


/**
 * \param	draw	Pointer to a drawing function for the menu.
 * \param	key		Pointer to a key handler function for the menu.
 */
void M_PushMenu(void(*draw)(), int(*key)(int k))
{
	if (Cvar_VariableValue("maxclients") == 1 && Com_ServerState())
		Cvar_Set("paused", "1");

	// if this menu is already present, drop back to that level
	// to avoid stacking menus by hotkeys
	int i = 0;
	for (i = 0; i < MENU_DEPTH; i++)
	{
		if (MENU_LAYERS[i].draw == draw && MENU_LAYERS[i].key == key)
			MENU_DEPTH = i;
	}

	if (i == MENU_DEPTH)
	{
		if (MENU_DEPTH >= MAX_MENU_DEPTH)
			Com_Error(ERR_FATAL, "M_PushMenu(): MAX_MENU_DEPTH");

		MENU_LAYERS[MENU_DEPTH].draw = m_drawfunc;
		MENU_LAYERS[MENU_DEPTH].key = m_keyfunc;
		MENU_DEPTH++;
	}

	m_drawfunc = draw;
	m_keyfunc = key;
	cls.menuActive = true;

	PLAY_ENTER_SOUND = true;

	cls.key_dest = KEY_MENU;
}


/**
 * 
 */
void M_ForceMenuOff()
{
	m_drawfunc = 0;
	m_keyfunc = 0;
	cls.key_dest = KEY_GAME;
	MENU_DEPTH = 0;
	Key_ClearStates();

	cls.menuActive = false;

	Cvar_Set("paused", "0");
	hidePointer();
}


/**
 * 
 */
void M_PopMenu()
{
	S_StartLocalSound(fastsound_descriptor[MENU_OUT_SOUND]);

	if (MENU_DEPTH < 1)
		Com_Error(ERR_FATAL, "M_PopMenu: depth < 1");

	MENU_DEPTH--;

	m_drawfunc = MENU_LAYERS[MENU_DEPTH].draw;
	m_keyfunc = MENU_LAYERS[MENU_DEPTH].key;

	if (!MENU_DEPTH)
		M_ForceMenuOff();
}


/**
 * 
 */
int Default_MenuKey(menuframework_s * m, int key)
{
	int sound = 0;
	menucommon_s* item;

	if (m)
	{
		if ((item = reinterpret_cast<menucommon_s*>(Menu_ItemAtCursor(m))) != 0)
		{
			if (item->type == MTYPE_FIELD)
			{
				if (Field_Key((menufield_s *)item, key))
					return 0;
			}
		}
	}

	switch (key)
	{
	case K_ESCAPE:
		M_PopMenu();
		return MENU_OUT_SOUND;

	case K_KP_UPARROW:
	case K_UPARROW:
		if (m)
		{
			m->cursor--;
			Menu_AdjustCursor(m, -1);
			sound = MENU_MOVE_SOUND;
		}
		break;

	case K_TAB:
		if (m)
		{
			m->cursor++;
			Menu_AdjustCursor(m, 1);
			sound = MENU_MOVE_SOUND;
		}
		break;

	case K_KP_DOWNARROW:
	case K_DOWNARROW:
		if (m)
		{
			m->cursor++;
			Menu_AdjustCursor(m, 1);
			sound = MENU_MOVE_SOUND;
		}
		break;

	case K_KP_LEFTARROW:
	case K_LEFTARROW:
		if (m)
		{
			Menu_SlideItem(m, -1);
			sound = MENU_MOVE_SOUND;
		}
		break;

	case K_KP_RIGHTARROW:
	case K_RIGHTARROW:
		if (m)
		{
			Menu_SlideItem(m, 1);
			sound = MENU_MOVE_SOUND;
		}
		break;

	case K_MOUSE1:
	case K_MOUSE2:
	case K_MOUSE3:
	case K_JOY1:
	case K_JOY2:
	case K_JOY3:
	case K_JOY4:
	case K_AUX1:
	case K_AUX2:
	case K_AUX3:
	case K_AUX4:
	case K_AUX5:
	case K_AUX6:
	case K_AUX7:
	case K_AUX8:
	case K_AUX9:
	case K_AUX10:
	case K_AUX11:
	case K_AUX12:
	case K_AUX13:
	case K_AUX14:
	case K_AUX15:
	case K_AUX16:
	case K_AUX17:
	case K_AUX18:
	case K_AUX19:
	case K_AUX20:
	case K_AUX21:
	case K_AUX22:
	case K_AUX23:
	case K_AUX24:
	case K_AUX25:
	case K_AUX26:
	case K_AUX27:
	case K_AUX28:
	case K_AUX29:
	case K_AUX30:
	case K_AUX31:
	case K_AUX32:

	case K_KP_ENTER:
	case K_ENTER:
		if (m) { Menu_SelectItem(m); }
		sound = MENU_MOVE_SOUND;
		break;
	}

	return sound;
}


/**
 * Draws one solid graphics character
 * cx and cy are in 320*240 coordinates, and will be centered on
 * higher res screens.
*/
void M_DrawCharacter(int cx, int cy, int num)
{
	float fontscale = cl_fontScale->value;
	Draw_CharScaled(cx + ((videoWidth() - 320) >> 1), cy + ((videoHeight() - 240) >> 1), fontscale, fontscale, num);
}


/**
 * 
 */
void M_Print(int cx, int cy, char* str)
{
	Set_FontShader(true);
	while (*str)
	{
		M_DrawCharacter(cx, cy, (*str) + 128);
		str++;
		cx += 8 * cl_fontScale->value;
	}
	Set_FontShader(false);
}


/**
 * 
 */
void M_PrintWhite(int cx, int cy, char* str)
{
	Set_FontShader(true);
	while (*str)
	{
		M_DrawCharacter(cx, cy, *str);
		str++;
		cx += 8 * cl_fontScale->value;
	}
	Set_FontShader(false);
}


/**
 * 
 */
void M_DrawTextBox(int x, int y, int width, int lines)
{
	int cx, cy;
	int n;

	Set_FontShader(true);
	// draw left side
	cx = x;
	cy = y;
	M_DrawCharacter(cx, cy, 1);
	for (n = 0; n < lines; n++)
	{
		cy += 8;
		M_DrawCharacter(cx, cy, 4);
	}
	M_DrawCharacter(cx, cy + 8, 7);

	// draw middle
	cx += 8;
	while (width > 0)
	{
		cy = y;
		M_DrawCharacter(cx, cy, 2);
		for (n = 0; n < lines; n++)
		{
			cy += 8;
			M_DrawCharacter(cx, cy, 5);
		}
		M_DrawCharacter(cx, cy + 8, 8);
		width -= 1;
		cx += 8;
	}

	// draw right side
	cy = y;
	M_DrawCharacter(cx, cy, 3);
	for (n = 0; n < lines; n++)
	{
		cy += 8;
		M_DrawCharacter(cx, cy, 6);
	}

	M_DrawCharacter(cx, cy + 8, 9);
	Set_FontShader(false);
}


// ===============================================================================
// = JOIN SERVER MENU
// ===============================================================================
#define MAX_LOCAL_SERVERS 8

static menuframework_s s_joinserver_menu;
static menuseparator_s s_joinserver_server_title;
static menuaction_s s_joinserver_search_action;
static menuaction_s s_joinserver_address_book_action;
static menuaction_s s_joinserver_server_actions[MAX_LOCAL_SERVERS];

int M_NUM_SERVERS;
#define	NO_SERVER_STRING	"<no server>"

// user readable information
static char local_server_names[MAX_LOCAL_SERVERS][80];

// network address
static netadr_t local_server_netadr[MAX_LOCAL_SERVERS];

void M_AddToServerList(netadr_t adr, char* info)
{
	if (M_NUM_SERVERS == MAX_LOCAL_SERVERS)
		return;

	while (*info == ' ')
		info++;

	// ignore if duplicated
	for (int i = 0; i < M_NUM_SERVERS; i++)
	{
		if (!strcmp(info, local_server_names[i]))
			return;
	}

	local_server_netadr[M_NUM_SERVERS] = adr;
	strncpy(local_server_names[M_NUM_SERVERS], info, sizeof(local_server_names[0]) - 1);
	M_NUM_SERVERS++;
}


void JoinServerFunc(void *self)
{
	char buffer[128] = { '\0' };

	int index = (menuaction_s *)self - s_joinserver_server_actions;

	if (_stricmp(local_server_names[index], NO_SERVER_STRING) == 0)
		return;

	if (index >= M_NUM_SERVERS)
		return;

	Q_sprintf(buffer, sizeof(buffer), "connect %s\n", NET_AdrToString(local_server_netadr[index]));
	Cbuf_AddText(buffer);
	M_ForceMenuOff();
}


void SearchLocalGames(void *self)
{
	float fontscale = cl_fontScale->value;

	M_NUM_SERVERS = 0;
	for (int i = 0; i < MAX_LOCAL_SERVERS; i++)
		strcpy(local_server_names[i], NO_SERVER_STRING);

	M_DrawTextBox(8, 120 - 48, 36 * fontscale, 3 * fontscale);

	M_Print(32, 120 - 48 + 8 * fontscale, "Searching for local servers, this");
	M_Print(32, 120 - 48 + 16 * fontscale, "could take up to a minute, so");
	M_Print(32, 120 - 48 + 24 * fontscale, "please be patient.");

	// the text box won't show up unless we do a buffer swap
	GLimp_EndFrame();

	// send out info packets
	CL_PingServers_f();
}


void JoinServer_MenuInit()
{
	int shift = 60 * (cl_fontScale->value - 1);

	s_joinserver_menu.x = videoWidth() * 0.50 - 120;

	s_joinserver_menu.nitems = 0;

	s_joinserver_search_action.generic.type = MTYPE_ACTION;
	s_joinserver_search_action.generic.name = "refresh server list";
	s_joinserver_search_action.generic.flags = QMF_LEFT_JUSTIFY;
	s_joinserver_search_action.generic.x = 0;
	s_joinserver_search_action.generic.y = shift + 10 * cl_fontScale->value;
	s_joinserver_search_action.generic.callback = SearchLocalGames;
	s_joinserver_search_action.generic.statusbar = "search for servers";

	s_joinserver_server_title.generic.type = MTYPE_SEPARATOR;
	s_joinserver_server_title.generic.name = "connect to...";
	s_joinserver_server_title.generic.x = 80;
	s_joinserver_server_title.generic.y = shift + 30 * cl_fontScale->value;

	for (int i = 0; i < MAX_LOCAL_SERVERS; i++)
	{
		s_joinserver_server_actions[i].generic.type = MTYPE_ACTION;
		strcpy(local_server_names[i], NO_SERVER_STRING);
		s_joinserver_server_actions[i].generic.name = local_server_names[i];
		s_joinserver_server_actions[i].generic.flags = QMF_LEFT_JUSTIFY;
		s_joinserver_server_actions[i].generic.x = 0;
		s_joinserver_server_actions[i].generic.y = shift + 40 * cl_fontScale->value + i * 10 * cl_fontScale->value;

		s_joinserver_server_actions[i].generic.callback = JoinServerFunc;
		s_joinserver_server_actions[i].generic.statusbar = "press ENTER to connect";
	}

	Menu_AddItem(&s_joinserver_menu, &s_joinserver_address_book_action);
	Menu_AddItem(&s_joinserver_menu, &s_joinserver_server_title);
	Menu_AddItem(&s_joinserver_menu, &s_joinserver_search_action);

	for (int i = 0; i < 8; i++)
		Menu_AddItem(&s_joinserver_menu, &s_joinserver_server_actions[i]);

	Menu_Center(&s_joinserver_menu);

	SearchLocalGames(nullptr);
}


void JoinServer_MenuDraw()
{
	M_Banner("gfx/m_banner_join_server.tga");
	Menu_Draw(&s_joinserver_menu);
}


int JoinServer_MenuKey(int key)
{
	return Default_MenuKey(&s_joinserver_menu, key);
}


void M_Menu_JoinServer_f(void*)
{
	JoinServer_MenuInit();
	M_PushMenu(JoinServer_MenuDraw, JoinServer_MenuKey);
}


// ===============================================================================
// = START SERVER MENU
// ===============================================================================
static menuframework_s s_startserver_menu;
static char** mapnames;
static int nummaps;

static menuaction_s s_startserver_start_action;
static menuaction_s s_startserver_dmoptions_action;
static menufield_s s_timelimit_field;
static menufield_s s_fraglimit_field;
static menufield_s s_maxclients_field;
static menufield_s s_hostname_field;
static menulist_s s_startmap_list;
static menulist_s s_rules_box;

void DMOptionsFunc(void *self)
{
	if (s_rules_box.curvalue == 1)
		return;
}


void RulesChangeFunc(void *self)
{
	// DM
	if (s_rules_box.curvalue == 0)
	{
		s_maxclients_field.generic.statusbar = nullptr;
		s_startserver_dmoptions_action.generic.statusbar = nullptr;
	}
	else if (s_rules_box.curvalue == 1)	// coop
	{
		s_maxclients_field.generic.statusbar = "4 maximum for cooperative";
		if (atoi(s_maxclients_field.buffer) > 4)
			strcpy(s_maxclients_field.buffer, "4");
		s_startserver_dmoptions_action.generic.statusbar = "N/A for cooperative";
	}
}


void StartServerActionFunc(void* self)
{
	char startmap[1024] = { '\0' };

	Q_sprintf(startmap, sizeof(startmap), strchr(mapnames[s_startmap_list.curvalue], '\n') + 1);

	int maxclients = atoi(s_maxclients_field.buffer);
	int timelimit = atoi(s_timelimit_field.buffer);
	int fraglimit = atoi(s_fraglimit_field.buffer);

	Cvar_SetValue("maxclients", clamp(maxclients, 0, maxclients));
	Cvar_SetValue("timelimit", clamp(timelimit, 0, timelimit));
	Cvar_SetValue("fraglimit", clamp(fraglimit, 0, fraglimit));
	Cvar_Set("hostname", s_hostname_field.buffer);
	Cvar_SetValue("deathmatch", !s_rules_box.curvalue);
	Cvar_SetValue("coop", s_rules_box.curvalue);
	Cvar_SetValue("gamerules", 0);

	Cbuf_AddText(va("map %s\n", startmap));

	M_ForceMenuOff();
}


/**
 * 
 */
static size_t _countMaps(const char* buffer, int length)
{
	const char* s = buffer;
	int _mapCount = 0;
	for (int i = 0; i < length; )
	{
		if (s[i] == '\r')
			_mapCount++;

		i++;
	}

	return _mapCount;
}


/**
 * 
 */
static char** _buildMapNamesList(const char* buffer, int mapCount)
{
	char** _mapNames = static_cast<char**>(calloc(mapCount + 1, sizeof(char*)));

	char* _buf = (char*)buffer; // const cast as Com_Parse below modifies the provided pointer.

	for (int i = 0; i < mapCount; i++)
	{
		char shortname[MAX_TOKEN_CHARS] = { '\0' };
		char longname[MAX_TOKEN_CHARS] = { '\0' };
		char scratch[200] = { '\0' };

		Q_sprintf(shortname, sizeof(shortname), "%s", Com_Parse(&_buf));

		int _shortNameLength = strlen(shortname);
		for (int j = 0; j < _shortNameLength; j++)
		{
			shortname[j] = toupper(shortname[j]);
		}

		strcpy(longname, Com_Parse(&_buf));
		Q_sprintf(scratch, sizeof(scratch), "%s\n%s", longname, shortname);

		int _len = strlen(scratch) + 1;
		_mapNames[i] = static_cast<char*>(calloc(_len, sizeof(char)));
		Q_sprintf(_mapNames[i], _len, scratch);
	}

	return _mapNames;
}


static char* dm_coop_names[] =
{
	"Deathmatch",
	"Cooperative",
	0
};

const char* MAP_LIST_NAME = "maps.lst";

/**
 * 
 */
void StartServer_MenuInit()
{
	char* buffer = nullptr;
	int length;

	// load the list of map names
	FS_File* f = FS_Open(MAP_LIST_NAME, FS_OPEN_READ);
	if (!f)
	{
		length = FS_LoadFile(MAP_LIST_NAME, reinterpret_cast<void**>(&buffer));
		if (length == FS_OPEN_FAILED)
			Com_Error(ERR_DROP, "couldn't find maps.lst\n");
	}
	else
	{
		FS_Seek(f, FS_FileLength(f));
		length = FS_Tell(f);
		FS_Seek(f, 0);

		buffer = static_cast<char*>(calloc(length, sizeof(char*)));
		FS_Read(buffer, length, 1, f);
	}

	nummaps = _countMaps(buffer, length);
	if (nummaps == 0)
		Com_Error(ERR_DROP, "no maps in %s\n", MAP_LIST_NAME);

	mapnames = _buildMapNamesList(buffer, nummaps);

	FS_Close(f);
	f = nullptr;
	FS_FreeFile(buffer);

	// =================================================
	// Initialize menu items.
	// =================================================
	s_startserver_menu.x = videoWidth() * 0.50;
	s_startserver_menu.nitems = 0;

	s_startmap_list.generic.type = MTYPE_SPINCONTROL;
	s_startmap_list.generic.x = 0;
	s_startmap_list.generic.y = 0;
	s_startmap_list.generic.name = "Start Map";
	s_startmap_list.itemnames = mapnames;

	// =================================================
	// Rules
	// =================================================
	s_rules_box.generic.type = MTYPE_SPINCONTROL;
	s_rules_box.generic.x = 0;
	s_rules_box.generic.y = 20 * cl_fontScale->value;
	s_rules_box.generic.name = "Rules";
	s_rules_box.itemnames = dm_coop_names;
	s_rules_box.curvalue = 0;
	if (Cvar_VariableValue("coop")) { s_rules_box.curvalue = 1; }

	s_rules_box.generic.callback = RulesChangeFunc;

	// =================================================
	// Time Limit
	// =================================================
	s_timelimit_field.generic.type = MTYPE_FIELD;
	s_timelimit_field.generic.name = "Time Limit";
	s_timelimit_field.generic.flags = QMF_NUMBERSONLY;
	s_timelimit_field.generic.x = 0;
	s_timelimit_field.generic.y = 36 * cl_fontScale->value;
	s_timelimit_field.generic.statusbar = "0 = no limit";
	s_timelimit_field.length = 3;
	s_timelimit_field.visible_length = 3;
	Q_sprintf(s_timelimit_field.buffer, MENU_FIELD_BUFFER, Cvar_VariableString("timelimit").c_str());


	// =================================================
	// Frag Limit
	// =================================================
	s_fraglimit_field.generic.type = MTYPE_FIELD;
	s_fraglimit_field.generic.name = "Frag Limit";
	s_fraglimit_field.generic.flags = QMF_NUMBERSONLY;
	s_fraglimit_field.generic.x = 0;
	s_fraglimit_field.generic.y = 54 * cl_fontScale->value;
	s_fraglimit_field.generic.statusbar = "0 = no limit";
	s_fraglimit_field.length = 3;
	s_fraglimit_field.visible_length = 3;
	Q_sprintf(s_fraglimit_field.buffer, MENU_FIELD_BUFFER, Cvar_VariableString("fraglimit").c_str());

	/**
	 * maxclients determines the maximum number of players that can join
	 * the game. If maxclients is only "1" then we should default the menu
	 * option to 8 players, otherwise use whatever its current value is.
	 * Clamping will be done when the server is actually started.
	 */
	s_maxclients_field.generic.type = MTYPE_FIELD;
	s_maxclients_field.generic.name = "Max Players";
	s_maxclients_field.generic.flags = QMF_NUMBERSONLY;
	s_maxclients_field.generic.x = 0;
	s_maxclients_field.generic.y = 72 * cl_fontScale->value;
	s_maxclients_field.generic.statusbar = nullptr;
	s_maxclients_field.length = 3;
	s_maxclients_field.visible_length = 3;
	if (Cvar_VariableValue("maxclients") == 1)
		Q_sprintf(s_fraglimit_field.buffer, MENU_FIELD_BUFFER, "8");
	else
		Q_sprintf(s_maxclients_field.buffer, MENU_FIELD_BUFFER, Cvar_VariableString("maxclients").c_str());

	// =================================================
	// Frag Limit
	// =================================================
	s_hostname_field.generic.type = MTYPE_FIELD;
	s_hostname_field.generic.name = "Hostname";
	s_hostname_field.generic.flags = 0;
	s_hostname_field.generic.x = 0;
	s_hostname_field.generic.y = 90 * cl_fontScale->value;
	s_hostname_field.generic.statusbar = nullptr;
	s_hostname_field.length = 12;
	s_hostname_field.visible_length = 12;
	strcpy(s_hostname_field.buffer, Cvar_VariableString("hostname").c_str());

	s_startserver_dmoptions_action.generic.type = MTYPE_ACTION;
	s_startserver_dmoptions_action.generic.name = " deathmatch flags";
	s_startserver_dmoptions_action.generic.flags = QMF_LEFT_JUSTIFY;
	s_startserver_dmoptions_action.generic.x = 24;
	s_startserver_dmoptions_action.generic.y = 108 * cl_fontScale->value;
	s_startserver_dmoptions_action.generic.statusbar = nullptr;
	s_startserver_dmoptions_action.generic.callback = DMOptionsFunc;

	s_startserver_start_action.generic.type = MTYPE_ACTION;
	s_startserver_start_action.generic.name = " begin";
	s_startserver_start_action.generic.flags = QMF_LEFT_JUSTIFY;
	s_startserver_start_action.generic.x = 24;
	s_startserver_start_action.generic.y = 128 * cl_fontScale->value;
	s_startserver_start_action.generic.callback = StartServerActionFunc;

	Menu_AddItem(&s_startserver_menu, &s_startmap_list);
	Menu_AddItem(&s_startserver_menu, &s_rules_box);
	Menu_AddItem(&s_startserver_menu, &s_timelimit_field);
	Menu_AddItem(&s_startserver_menu, &s_fraglimit_field);
	Menu_AddItem(&s_startserver_menu, &s_maxclients_field);
	Menu_AddItem(&s_startserver_menu, &s_hostname_field);
	Menu_AddItem(&s_startserver_menu, &s_startserver_dmoptions_action);
	Menu_AddItem(&s_startserver_menu, &s_startserver_start_action);

	Menu_Center(&s_startserver_menu);

	// call this now to set proper inital state
	RulesChangeFunc(nullptr);
}


void StartServer_MenuDraw()
{
	Menu_Draw(&s_startserver_menu);
}


int StartServer_MenuKey(int key)
{
	if (key == K_ESCAPE)
	{
		if (mapnames)
		{
			for (int i = 0; i < nummaps; i++)
				free(mapnames[i]);
		
			free(mapnames);
		}

		mapnames = 0;
		nummaps = 0;
	}

	return Default_MenuKey(&s_startserver_menu, key);
}


void M_Menu_StartServer_f(void *unused)
{
	StartServer_MenuInit();
	M_PushMenu(StartServer_MenuDraw, StartServer_MenuKey);
}





///\fixme yuck
#include "../m_frames.h"
#include <time.h>

/*
const char* pose_names[] = { "stand", '\0' };
const char* pose_rot_names[] = { "0 degrees", "30 degrees", "60 degrees", "90 degrees", "120 degrees", "150 degrees", "180 degrees", "210 degrees", "240 degrees", "270 degrees", "300 degrees", "330 degrees", "auto rotate", 0 };

struct image_s *R_RegisterPlayerBump (char* name);

const int divisor = 115;


void PlayerConfig_MenuDraw()
{
	extern float CalcFov(float fov_x, float w, float h);
	refdef_t refdef;
	char scratch[MAX_QPATH];
	int x2;

	memset(&refdef, 0, sizeof(refdef));

	refdef.x = videoWidth() / 2 + (cl_fontScale->value - 1) * 72;
	x2 = 320 * (cl_fontScale->value - 1);
	refdef.y = videoHeight() / 2 - 72 * cl_fontScale->value;
	refdef.width = 240 * cl_fontScale->value;
	refdef.height = 335 * cl_fontScale->value;
	refdef.fov_x = 35;
	refdef.fov_y = CalcFov(refdef.fov_x, refdef.width, refdef.height);
	refdef.time = cls.realtime * 0.001;


	if (s_pmi[s_player_model_box.curvalue].skindisplaynames)
	{
		entity_t entity[2];
		refdef.num_entities = 1;

		// player model
		memset(&entity[0], 0, sizeof(entity[0]));

		Q_sprintf(scratch, sizeof(scratch), "players/%s/tris.md2", s_pmi[s_player_model_box.curvalue].directory);
		entity[0].model = R_RegisterModel(scratch);

		Q_sprintf(scratch, sizeof(scratch), "players/%s/%s.tga", s_pmi[s_player_model_box.curvalue].directory, s_pmi[s_player_model_box.curvalue].skindisplaynames[s_player_skin_box.curvalue]);
		entity[0].skin = R_RegisterSkin(scratch);
		entity[0].bump = R_RegisterPlayerBump(scratch);
		entity[0].origin[0] = 90;
		entity[0].origin[1] = 0;
		entity[0].origin[2] = 0;
		entity[0].flags = RF_NOSHADOW | RF_DEPTHHACK;

		VectorCopy(entity[0].origin, entity[0].oldorigin);

		entity[0].angles[1] = 150.0f;

		entity[0].frame = ((cls.realtime / divisor) % 39);
		entity[0].oldframe = (((cls.realtime / divisor) - 1) % 39);
		entity[0].backlerp = 1.0 - ((float)(cls.realtime % divisor)) / divisor;

		// player weapon model
		memset(&entity[1], 0, sizeof(entity[1]));
		if (currentPlayerWeapon)
		{
			entity[1].model = currentPlayerWeapon;
			entity[1].skin = currentPlayerWeapon->skins[0];
			entity[1].bump = currentPlayerWeapon->skins_normal[0];
		}
		else
		{
			Q_sprintf(scratch, sizeof(scratch), "players/%s/w_sshotgun.md2", s_pmi[s_player_model_box.curvalue].directory); //force default player weapon
			entity[1].model = R_RegisterModel(scratch);
			entity[1].skin = R_RegisterSkin(scratch);
			entity[1].bump = R_RegisterPlayerBump(scratch);
		}

		if (entity[1].model)
		{
			entity[1].origin[0] = 90;
			entity[1].origin[1] = 0;
			entity[1].origin[2] = -8;

			VectorCopy(entity[1].origin, entity[1].oldorigin);

			entity[1].angles[1] = entity[0].angles[1];
			entity[1].frame = entity[0].frame;
			entity[1].oldframe = entity[0].oldframe;
			entity[1].backlerp = entity[0].backlerp;
			entity[1].flags = RF_FULLBRIGHT | RF_NOSHADOW | RF_DEPTHHACK;
			refdef.num_entities++;
		}

		refdef.areabits = 0;
		refdef.entities = &entity[0];
		refdef.rdflags = RDF_NOWORLDMODEL | RDF_NOCLEAR;

		Menu_Draw(&s_player_config_menu);

		refdef.height += 4;

		R_RenderFrame(&refdef);

		Q_sprintf(scratch, sizeof(scratch), "/players/%s/%s_i.tga", s_pmi[s_player_model_box.curvalue].directory, s_pmi[s_player_model_box.curvalue]. skindisplaynames[s_player_skin_box.curvalue]);
		Draw_Pic(s_player_config_menu.x - 40 * cl_fontScale->value, refdef.y, scratch);
	}
}


int PlayerConfig_MenuKey(int key)
{
	if (key == K_ESCAPE)
	{
		char scratch[1024] = { '\0' };

		Cvar_Set("name", s_player_name_field.buffer);
		Q_sprintf(scratch, sizeof(scratch), "%s/%s", s_pmi[s_player_model_box.curvalue].directory, s_pmi[s_player_model_box.curvalue]. skindisplaynames[s_player_skin_box.curvalue]);
		
		Cvar_Set("skin", scratch);
		for (int i = 0; i < s_numplayermodels; i++)
		{
			for (int j = 0; j < s_pmi[i].nskins; j++)
			{
				if (s_pmi[i].skindisplaynames[j])
					free(s_pmi[i].skindisplaynames[j]);

				s_pmi[i].skindisplaynames[j] = 0;
			}

			free(s_pmi[i].skindisplaynames);
			s_pmi[i].skindisplaynames = 0;
			s_pmi[i].nskins = 0;
		}
	}

	return Default_MenuKey(&s_player_config_menu, key);
}


void M_Menu_PlayerConfig_f(void* unused)
{
	if (!PlayerConfig_MenuInit())
	{
		Menu_SetStatusBar(&s_multiplayer_menu, "No valid player models found");
		return;
	}

	Menu_SetStatusBar(&s_multiplayer_menu, nullptr);
	M_PushMenu(PlayerConfig_MenuDraw, PlayerConfig_MenuKey);
}

*/





// ===============================================================================
// = 
// ===============================================================================


int MENU_BG_WIDTH = 0;
int MENU_BG_HEIGHT = 0;

/**
 * 
 */
void M_Init()
{
	Cmd_AddCommand("menu_main", M_Menu_Main_f);
	Cmd_AddCommand("menu_quit", M_Menu_Quit_f);

	Draw_GetPicSize(MENU_BG_WIDTH, MENU_BG_HEIGHT, "gfx/menuback.tga");
}


/**
 * 
 */
void M_Draw()
{
	if (cls.key_dest != KEY_MENU) { return; }

	//if (cls.state != ca_active || !cl.refresh_prepped)
	//{
		Draw_PicScaled(0, 0, (float)videoWidth() / (float)MENU_BG_WIDTH, (float)videoHeight() / (float)MENU_BG_HEIGHT, "gfx/menuback.tga");
		Draw_PicBumpScaled(0, 0, (float)videoWidth() / (float)MENU_BG_WIDTH, (float)videoHeight() / (float)MENU_BG_HEIGHT, "gfx/menuback.tga", "gfx/menuback_n.tga");
	//}

	R_MenuBackGround();

	m_drawfunc();
	
	// delay playing the enter sound until after the menu has been drawn, to avoid delay while caching images
	if (PLAY_ENTER_SOUND)
	{
		S_StartLocalSound(fastsound_descriptor[MENU_IN_SOUND]);
		PLAY_ENTER_SOUND = false;
	}
}


/**
 *
 */
void M_Keydown(int key)
{
	if (m_keyfunc)
	{
		int s = m_keyfunc(key);
		if (s == MENU_IN_SOUND)
			S_StartLocalSound(fastsound_descriptor[s]);
		if (s == MENU_MOVE_SOUND)
			S_StartLocalSound(fastsound_descriptor[s]);
		if (s == MENU_OUT_SOUND)
			S_StartLocalSound(fastsound_descriptor[s]);
	}
}
