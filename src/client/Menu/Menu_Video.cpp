/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "menu.h"

#include "../client.h"
#include "../snd_loc.h"

#include "../../ref_gl/r_local.h"

extern ConsoleVariable *vid_ref;

int refresh = 0;
int MENU_SIZE = 0;


// ====================================================================
// = LOCAL MODULE VARIABLES
// ====================================================================

static char* RESOLUTION_LIST[] =
{
#ifdef _WIN32
	"[Desktop]",
#endif
	"[1024 768][4:3]",
	"[1152 864][4:3]",
	"[1280 1024][5:4]",
	"[1600 1200][4:3]",
	"[2048 1536][4:3]",

	"[1280 720][720p HDTV]",
	"[1280 800][16:10]",
	"[1366 768][16:9 Plasma and LCD TV]",
	"[1440 900][16:10]",
	"[1600 900][16:9 LCD]",
	"[1680 1050][16:10]",
	"[1920 1080][1080p full HDTV]",
	"[1920 1200][16:10]",
	"[2560 1600][16:10]",
	"[Custom]", 0
};


// ====================================================================
// = MENU INTERACTION
// ====================================================================
static menuframework_s	s_opengl_menu;
static menuframework_s	s_opengl2_menu;
static menuframework_s* s_current_menu;

static menulist_s		s_mode_list;
static menuslider_s		s_aniso_slider;

static menuslider_s		s_brightness_slider;
static menuslider_s		s_contrast_slider;
static menuslider_s		s_saturation_slider;
static menuslider_s		s_gamma_slider;

static menuslider_s		s_bloomIntens_slider;
static menuslider_s		s_bloomThreshold_slider;
static menuslider_s		s_bloomWidth_slider;

static menulist_s  		s_fs_box;

static menuslider_s	    s_reliefScale_slider;
static menulist_s	    s_flare_box;
static menulist_s	    s_tc_box;
static menulist_s	    s_refresh_box;
static menulist_s	    s_parallax_box;
static menulist_s	    s_samples_list;

static menulist_s	    s_bloom_box;
static menulist_s	    s_dof_box;

static menulist_s  		s_finish_box;
static menuaction_s		s_apply_action;
static menuaction_s		s_defaults_action;

static	menulist_s		a_autoBump_list;
static	menulist_s		s_radBlur_box;
static	menulist_s		s_ssao;
static	menulist_s		s_fxaa_box;
static	menulist_s		s_film_grain;
static	menulist_s		s_mb_box;
static	menuslider_s	s_ambientLevel_slider;
static	menuaction_s	s_menuAction_color;


// ====================================================================
// = MENU CALLBACK FUNCTIONS
// ====================================================================

/**
 * 
 */
static void ambientLevelCallback(void* s)
{
	float ambient = s_ambientLevel_slider.curvalue / 20;
	Cvar_SetValue("r_lightmapScale", ambient);
}


/**
 * 
 */
static void filmCallback(void* s)
{
	menulist_s *box = (menulist_s*)s;
	Cvar_SetValue("r_filmFilterType", box->curvalue * 1);
}


/**
 * 
 */
static void ParallaxCallback(void* s)
{
	menulist_s *box = (menulist_s*)s;
	Cvar_SetValue("r_reliefMapping", box->curvalue * 1);
}


/**
 * 
 */
static void reliefScaleCallback(void* s)
{
	menuslider_s *slider = (menuslider_s*)s;
	Cvar_SetValue("r_reliefScale", slider->curvalue * 1);
}


/**
 * 
 */
static void FlareCallback(void* s)
{
	menulist_s *box = (menulist_s*)s;
	Cvar_SetValue("r_drawFlares", box->curvalue * 1);
}


/**
 * 
 */
static void AnisoCallback(void* s)
{
	menuslider_s *slider = (menuslider_s*)s;
	Cvar_SetValue("r_anisotropic", slider->curvalue * 1);
}


/**
 * 
 */
static void BrightnessCallback(void* s)
{
	float brt;
	brt = s_brightness_slider.curvalue / 10;
	Cvar_SetValue("r_brightness", brt);
}


/**
 * 
 */
static void ContrastCallback(void* s)
{
	float contr;
	contr = s_contrast_slider.curvalue / 10;
	Cvar_SetValue("r_contrast", contr);
}


/**
 * 
 */
static void SaturationCallback(void* s)
{
	float sat;
	sat = s_saturation_slider.curvalue / 10;
	Cvar_SetValue("r_saturation", sat);
}


/**
 * 
 */
static void GammaCallback(void* s)
{
	float gm;
	gm = s_gamma_slider.curvalue / 10;
	Cvar_SetValue("r_gamma", gm);
}


/**
 * 
 */
static void BloomCallback(void* s)
{
	menulist_s *box = (menulist_s*)s;
	Cvar_SetValue("r_bloom", box->curvalue * 1);
}


/**
 * 
 */
static void DofCallback(void* s)
{
	menulist_s *box = (menulist_s*)s;
	Cvar_SetValue("r_dof", box->curvalue * 1);
}


/**
 * 
 */
static void RBCallback(void* s)
{
	menulist_s *box = (menulist_s*)s;
	Cvar_SetValue("r_radialBlur", box->curvalue * 1);
}


/**
 * 
 */
static void ssaoCallback(void* s)
{
	menulist_s *box = (menulist_s*)s;
	Cvar_SetValue("r_ssao", box->curvalue * 1);
}


/**
 * 
 */
static void fxaaCallback(void* s)
{
	menulist_s *box = (menulist_s*)s;
	Cvar_SetValue("r_fxaa", box->curvalue * 1);
}


/**
 * 
 */
static void mbCallback(void* s)
{
	menulist_s *box = (menulist_s*)s;
	Cvar_SetValue("r_motionBlur", box->curvalue * 1);
}


/**
 * 
 */
static void bloomLevelCallback(void* s)
{
	float intens = s_bloomIntens_slider.curvalue / 10;
	Cvar_SetValue("r_bloomIntens", intens);
}


/**
 * 
 */
static void bloomThresholdCallback(void* s)
{
	float mins = s_bloomThreshold_slider.curvalue / 10;
	Cvar_SetValue("r_bloomThreshold", mins);
}


/**
 * 
 */
static void bloomWhidthCallback(void* s)
{
	float star = s_bloomWidth_slider.curvalue / 10;
	Cvar_SetValue("r_bloomWidth", star);
}


/**
 * 
 */
static void ResetDefaults(void* unused)
{
	VID_MenuInit();
}


static void ApplyChanges(void* unused)
{
	Cvar_SetValue("r_anisotropic", s_aniso_slider.curvalue);
	Cvar_SetValue("r_fullScreen", s_fs_box.curvalue);
	Cvar_SetValue("r_drawFlares", s_flare_box.curvalue);
	Cvar_SetValue("r_textureCompression", s_tc_box.curvalue);
	Cvar_SetValue("r_mode", s_mode_list.curvalue);
	Cvar_SetValue("r_reliefScale", s_reliefScale_slider.curvalue);
	Cvar_SetValue("r_reliefMapping", s_parallax_box.curvalue);
	Cvar_SetValue("r_bloom", s_bloom_box.curvalue);
	Cvar_SetValue("r_dof", s_dof_box.curvalue);
	Cvar_SetValue("r_radialBlur", s_radBlur_box.curvalue);
	Cvar_SetValue("r_ssao", s_ssao.curvalue);
	Cvar_SetValue("r_fxaa", s_fxaa_box.curvalue);
	Cvar_SetValue("r_vsync", s_finish_box.curvalue);
	Cvar_SetValue("r_filmFilter", s_film_grain.curvalue);
	Cvar_SetValue("r_motionBlur", s_mb_box.curvalue);

	// update appropriate stuff if we're running OpenGL and gamma has been modified
	if (r_brightness->modified) vid_ref->modified = true;
	if (r_contrast->modified) vid_ref->modified = true;
	if (r_saturation->modified) vid_ref->modified = true;
	if (r_gamma->modified) vid_ref->modified = true;
	if (r_anisotropic->modified) vid_ref->modified = true;
	if (r_reliefScale->modified) vid_ref->modified = true;
	if (r_bloom->modified) vid_ref->modified = true;
	if (r_dof->modified) vid_ref->modified = true;
	if (r_displayRefresh->modified) vid_ref->modified = true;
	if (r_drawFlares->modified) vid_ref->modified = true;
	if (r_reliefMapping->modified) vid_ref->modified = true;
	if (r_textureCompression->modified) vid_ref->modified = true;
	if (r_vsync->modified) vid_ref->modified = true;
	if (r_dof->modified) vid_ref->modified = true;
	if (r_radialBlur->modified) vid_ref->modified = true;
	if (r_ssao->modified) vid_ref->modified = true;
	if (r_fxaa->modified) vid_ref->modified = true;
	if (r_lightmapScale->modified) vid_ref->modified = true;
	if (r_motionBlur->modified) vid_ref->modified = true;
	if (r_bloomIntens->modified) vid_ref->modified = true;
	if (r_bloomThreshold->modified) vid_ref->modified = true;
	if (r_bloomWidth->modified) vid_ref->modified = true;

	M_ForceMenuOff();
}

static void CancelChanges(void* unused)
{
	M_PopMenu();
}


static void autoBumpCallBack(void* s)
{
	menulist_s *box = (menulist_s *)s;
	Cvar_SetValue("r_imageAutoBump", box->curvalue * 1);
}


static void vSyncCallBack(void* s)
{
	menulist_s *box = (menulist_s*)s;
	Cvar_SetValue("r_vsync", box->curvalue * 1);
}


/**
 * 
 */
void M_ColorInit()
{
	if (!r_gamma) r_gamma = Cvar_Get("r_gamma", "1.8", CVAR_ARCHIVE);
	if (!r_brightness) r_brightness = Cvar_Get("r_brightness", "1", CVAR_ARCHIVE);
	if (!r_contrast) r_contrast = Cvar_Get("r_contrast", "1", CVAR_ARCHIVE);
	if (!r_saturation) r_saturation = Cvar_Get("r_saturation", "1", CVAR_ARCHIVE);
	if (!r_bloomThreshold) r_bloomThreshold = Cvar_Get("r_bloomThreshold", "0.75", CVAR_ARCHIVE);
	if (!r_bloomIntens) r_bloomIntens = Cvar_Get("r_bloomIntens", "0.5", CVAR_ARCHIVE);
	if (!r_bloomWidth) r_bloomWidth = Cvar_Get("r_bloomWidth", "3.0", CVAR_ARCHIVE);

	r_gamma->value = clamp(r_gamma->value, 0.1, 2.5);
	r_brightness->value = clamp(r_brightness->value, 0.1, 2.0);
	r_contrast->value = clamp(r_contrast->value, 0.1, 2.0);
	r_saturation->value = clamp(r_saturation->value, 0.1, 2.0);

	r_bloomIntens->value = clamp(r_bloomIntens->value, 0.1, 2.0);
	r_bloomThreshold->value = clamp(r_bloomThreshold->value, 0.1, 1.0);
	r_bloomWidth->value = clamp(r_bloomWidth->value, 0.1, 3.0);

	s_opengl2_menu.x = videoWidth() / 2;
	s_opengl2_menu.nitems = 0;

	s_gamma_slider.generic.type = MTYPE_SLIDER;
	s_gamma_slider.generic.x = 0;
	s_gamma_slider.generic.y = 10 * cl_fontScale->value;
	s_gamma_slider.generic.name = "Gamma";
	s_gamma_slider.generic.callback = GammaCallback;
	s_gamma_slider.minvalue = 1;
	s_gamma_slider.maxvalue = 25;
	s_gamma_slider.curvalue = r_gamma->value * 10;
	s_gamma_slider.generic.statusbar = "Screen Gamma";

	s_brightness_slider.generic.type = MTYPE_SLIDER;
	s_brightness_slider.generic.x = 0;
	s_brightness_slider.generic.y = 20 * cl_fontScale->value;
	s_brightness_slider.generic.name = "Brightness";
	s_brightness_slider.generic.callback = BrightnessCallback;
	s_brightness_slider.minvalue = 1;
	s_brightness_slider.maxvalue = 20;
	s_brightness_slider.curvalue = r_brightness->value * 10;
	s_brightness_slider.generic.statusbar = "Screen Brightness";

	s_contrast_slider.generic.type = MTYPE_SLIDER;
	s_contrast_slider.generic.x = 0;
	s_contrast_slider.generic.y = 30 * cl_fontScale->value;
	s_contrast_slider.generic.name = "Contrast";
	s_contrast_slider.generic.callback = ContrastCallback;
	s_contrast_slider.minvalue = 1;
	s_contrast_slider.maxvalue = 20;
	s_contrast_slider.curvalue = r_contrast->value * 10;
	s_contrast_slider.generic.statusbar = "Screen Contrast";

	s_saturation_slider.generic.type = MTYPE_SLIDER;
	s_saturation_slider.generic.x = 0;
	s_saturation_slider.generic.y = 40 * cl_fontScale->value;
	s_saturation_slider.generic.name = "Saturation";
	s_saturation_slider.generic.callback = SaturationCallback;
	s_saturation_slider.minvalue = 1;
	s_saturation_slider.maxvalue = 20;
	s_saturation_slider.curvalue = r_saturation->value * 10;
	s_saturation_slider.generic.statusbar = "Screen Saturation";

	s_bloomIntens_slider.generic.type = MTYPE_SLIDER;
	s_bloomIntens_slider.generic.x = 0;
	s_bloomIntens_slider.generic.y = 60 * cl_fontScale->value;
	s_bloomIntens_slider.generic.name = "Bloom Intensity";
	s_bloomIntens_slider.generic.callback = bloomLevelCallback;
	s_bloomIntens_slider.minvalue = 1;
	s_bloomIntens_slider.maxvalue = 20;
	s_bloomIntens_slider.curvalue = r_bloomIntens->value * 10;
	s_bloomIntens_slider.generic.statusbar = "Bloom Intensity";

	s_bloomThreshold_slider.generic.type = MTYPE_SLIDER;
	s_bloomThreshold_slider.generic.x = 0;
	s_bloomThreshold_slider.generic.y = 70 * cl_fontScale->value;
	s_bloomThreshold_slider.generic.name = "Bloom Threshold";
	s_bloomThreshold_slider.generic.callback = bloomThresholdCallback;
	s_bloomThreshold_slider.minvalue = 1;
	s_bloomThreshold_slider.maxvalue = 10;
	s_bloomThreshold_slider.curvalue = r_bloomThreshold->value * 10;
	s_bloomThreshold_slider.generic.statusbar = "Bloom Threshold";

	s_bloomWidth_slider.generic.type = MTYPE_SLIDER;
	s_bloomWidth_slider.generic.x = 0;
	s_bloomWidth_slider.generic.y = 80 * cl_fontScale->value;
	s_bloomWidth_slider.generic.name = "Bloom Star Size";
	s_bloomWidth_slider.generic.callback = bloomWhidthCallback;
	s_bloomWidth_slider.minvalue = 1;
	s_bloomWidth_slider.maxvalue = 30;
	s_bloomWidth_slider.curvalue = r_bloomWidth->value * 10;
	s_bloomWidth_slider.generic.statusbar = "Bloom Star Size";

	MENU_SIZE = 90;

	Menu_AddItem(&s_opengl2_menu, (void*)&s_gamma_slider);
	Menu_AddItem(&s_opengl2_menu, (void*)&s_brightness_slider);
	Menu_AddItem(&s_opengl2_menu, (void*)&s_contrast_slider);
	Menu_AddItem(&s_opengl2_menu, (void*)&s_saturation_slider);

	Menu_AddItem(&s_opengl2_menu, (void*)&s_bloomIntens_slider);
	Menu_AddItem(&s_opengl2_menu, (void*)&s_bloomThreshold_slider);
	Menu_AddItem(&s_opengl2_menu, (void*)&s_bloomWidth_slider);

	Menu_Center(&s_opengl2_menu);
	s_opengl2_menu.x -= 8;
}


/**
 * 
 */
void Color_MenuDraw()
{
	MENU_SIZE = 190 * cl_fontScale->value;

	M_Banner("gfx/m_banner_video.tga");

	Menu_AdjustCursor(&s_opengl2_menu, 1);
	Menu_Draw(&s_opengl2_menu);
}


/**
 * 
 */
int Color_MenuKey(int key)
{
	return Default_MenuKey(&s_opengl2_menu, key);
}


/**
 * 
 */
void M_Menu_Color_f()
{
	M_ColorInit();
	M_PushMenu(Color_MenuDraw, Color_MenuKey);
}


/**
 * 
 */
static void ColorSettingsFunc(void* unused)
{
	M_Menu_Color_f();
}


/**
 * Initialize video options menu
 */
void VID_MenuInit()
{
	static char* adaptive_vc[] = { "off", "default", "adaptive", 0 };
	static char* film_filter[] = { "off", "TechniColor Sys 1", "TechniColor Sys 3", "Sepia", 0 };

	if (!r_mode) { r_mode = Cvar_Get("r_mode", "0", CVAR_ARCHIVE); }
	if (!r_anisotropic) { r_anisotropic = Cvar_Get("r_anisotropic", "1", CVAR_ARCHIVE); }
	if (!r_textureCompression) { r_textureCompression = Cvar_Get("r_textureCompression", "0", CVAR_ARCHIVE); }
	if (!r_drawFlares) { r_drawFlares = Cvar_Get("r_drawFlares", "0", CVAR_ARCHIVE); }
	if (!r_bloom) { r_bloom = Cvar_Get("r_bloom", "0", CVAR_ARCHIVE); }
	if (!r_reliefMapping) { r_reliefMapping = Cvar_Get("r_reliefMapping", "0", CVAR_ARCHIVE); }
	if (!r_dof) { r_dof = Cvar_Get("r_dof", "0", CVAR_ARCHIVE); }
	if (!r_imageAutoBump) { r_imageAutoBump = Cvar_Get("r_imageAutoBump", "0", CVAR_ARCHIVE); }
	if (!r_vsync) { r_vsync = Cvar_Get("r_vsync", "0", CVAR_ARCHIVE); }
	if (!cl_fontScale) { cl_fontScale = Cvar_Get("cl_fontScale", "1", CVAR_ARCHIVE); }

	if (r_radialBlur->value == 0.0f) { r_radialBlur = Cvar_Get("r_radialBlur", "0", CVAR_ARCHIVE); }
	if (r_ssao->value == 0.0f) { r_ssao = Cvar_Get("r_ssao", "0", CVAR_ARCHIVE); }
	if (r_fxaa->value == 0.0f) { r_fxaa = Cvar_Get("r_fxaa", "0", CVAR_ARCHIVE); }
	if (r_lightmapScale->value == 0.0f) { r_lightmapScale = Cvar_Get("r_lightmapScale", "0", CVAR_ARCHIVE); }
	if (r_motionBlur->value == 0.0f) { r_motionBlur = Cvar_Get("r_motionBlur", "0", CVAR_ARCHIVE); }
	if (r_reliefMapping->value > 1.0f) { r_reliefMapping = Cvar_Get("r_reliefMapping", "1", CVAR_ARCHIVE); }

	r_reliefScale->value = clamp(r_reliefScale->value, 1.0, 10.0);

	s_opengl_menu.x = videoWidth() * 0.50;
	s_opengl_menu.nitems = 0;

	s_mode_list.generic.type = MTYPE_SPINCONTROL;
	s_mode_list.generic.name = "Screen Resolution";
	s_mode_list.generic.x = 0;
	s_mode_list.generic.y = 10 * cl_fontScale->value;
	s_mode_list.itemnames = RESOLUTION_LIST;
	s_mode_list.curvalue = r_mode->value;
	s_mode_list.generic.statusbar = "Screen Resolution <Requires Restart Video Sub-System>";

	s_fs_box.generic.type = MTYPE_SPINCONTROL;
	s_fs_box.generic.x = 0;
	s_fs_box.generic.y = 20 * cl_fontScale->value;
	s_fs_box.generic.name = "Fullscreen";
	s_fs_box.itemnames = NAMES_YES_NO;
	s_fs_box.curvalue = r_fullScreen->value;
	s_fs_box.generic.statusbar = " Use Full Screen <Requires Restart Video Sub-System>";

	// -----------------------------------------------------------------------

	s_aniso_slider.generic.type = MTYPE_SLIDER;
	s_aniso_slider.generic.x = 0;
	s_aniso_slider.generic.y = 40 * cl_fontScale->value;
	s_aniso_slider.generic.name = "Anisotropy Filtering";
	s_aniso_slider.minvalue = 1;
	s_aniso_slider.maxvalue = 16;
	s_aniso_slider.curvalue = r_anisotropic->value;
	s_aniso_slider.generic.callback = AnisoCallback;
	s_aniso_slider.generic.statusbar = "Texture Anisotropy Level";

	s_tc_box.generic.type = MTYPE_SPINCONTROL;
	s_tc_box.generic.x = 0;
	s_tc_box.generic.y = 50 * cl_fontScale->value;
	s_tc_box.generic.name = "Texture Compression";
	s_tc_box.itemnames = NAMES_YES_NO;
	s_tc_box.curvalue = r_textureCompression->value;
	s_tc_box.generic.statusbar = "Use Compressed Textures <Requires Restart Video Sub-System>";

	// -----------------------------------------------------------------------

	a_autoBump_list.generic.type = MTYPE_SPINCONTROL;
	a_autoBump_list.generic.name = "Generate Normal Maps";
	a_autoBump_list.generic.x = 0;
	a_autoBump_list.generic.y = 70 * cl_fontScale->value;
	a_autoBump_list.itemnames = NAMES_YES_NO;
	a_autoBump_list.curvalue = r_imageAutoBump->value;
	a_autoBump_list.generic.callback = autoBumpCallBack;
	a_autoBump_list.generic.statusbar = "Realtime Normal Maps Generation For Old Textures";

	s_parallax_box.generic.type = MTYPE_SPINCONTROL;
	s_parallax_box.generic.x = 0;
	s_parallax_box.generic.y = 80 * cl_fontScale->value;
	s_parallax_box.generic.name = "Relief Mapping";
	s_parallax_box.itemnames = NAMES_YES_NO;
	s_parallax_box.curvalue = r_reliefMapping->value;
	s_parallax_box.generic.callback = ParallaxCallback;
	s_parallax_box.generic.statusbar = "Use High Quality Virtual Displasment Mapping";

	s_reliefScale_slider.generic.type = MTYPE_SLIDER;
	s_reliefScale_slider.generic.x = 0;
	s_reliefScale_slider.generic.y = 90 * cl_fontScale->value;
	s_reliefScale_slider.generic.name = "Relief Scale";
	s_reliefScale_slider.minvalue = 1;
	s_reliefScale_slider.maxvalue = 10;
	s_reliefScale_slider.curvalue = r_reliefScale->value;
	s_reliefScale_slider.generic.callback = reliefScaleCallback;
	s_reliefScale_slider.generic.statusbar = "Virtual Displasment Depth";

	s_ambientLevel_slider.generic.type = MTYPE_SLIDER;
	s_ambientLevel_slider.generic.x = 0;
	s_ambientLevel_slider.generic.y = 100 * cl_fontScale->value;
	s_ambientLevel_slider.generic.name = "Lightmap Scale";
	s_ambientLevel_slider.generic.callback = ambientLevelCallback;
	s_ambientLevel_slider.minvalue = 0;
	s_ambientLevel_slider.maxvalue = 20;
	s_ambientLevel_slider.curvalue = r_lightmapScale->value * 20;
	s_ambientLevel_slider.generic.statusbar = "Precomputed Lighting Level";

	s_flare_box.generic.type = MTYPE_SPINCONTROL;
	s_flare_box.generic.x = 0;
	s_flare_box.generic.y = 120 * cl_fontScale->value;
	s_flare_box.generic.name = "Light Flares";
	s_flare_box.itemnames = NAMES_YES_NO;
	s_flare_box.curvalue = r_drawFlares->value;
	s_flare_box.generic.callback = FlareCallback;
	s_flare_box.generic.statusbar = "Draw Lights Corona Effect";

	s_bloom_box.generic.type = MTYPE_SPINCONTROL;
	s_bloom_box.generic.x = 0;
	s_bloom_box.generic.y = 130 * cl_fontScale->value;
	s_bloom_box.generic.name = "Bloom";
	s_bloom_box.itemnames = NAMES_YES_NO;
	s_bloom_box.curvalue = r_bloom->value;
	s_bloom_box.generic.callback = BloomCallback;
	s_bloom_box.generic.statusbar = "Draw Bloom Effect";

	s_dof_box.generic.type = MTYPE_SPINCONTROL;
	s_dof_box.generic.x = 0;
	s_dof_box.generic.y = 140 * cl_fontScale->value;
	s_dof_box.generic.name = "Depth of Field";
	s_dof_box.itemnames = NAMES_YES_NO;
	s_dof_box.curvalue = r_dof->value;
	s_dof_box.generic.callback = DofCallback;
	s_dof_box.generic.statusbar = "Draw Depth of Field Effect";

	s_radBlur_box.generic.type = MTYPE_SPINCONTROL;
	s_radBlur_box.generic.x = 0;
	s_radBlur_box.generic.y = 150 * cl_fontScale->value;
	s_radBlur_box.generic.name = "Radial Blur";
	s_radBlur_box.itemnames = NAMES_YES_NO;
	s_radBlur_box.curvalue = r_radialBlur->value;
	s_radBlur_box.generic.callback = RBCallback;
	s_radBlur_box.generic.statusbar = "Draw Radial Blur Effect";

	s_mb_box.generic.type = MTYPE_SPINCONTROL;
	s_mb_box.generic.x = 0;
	s_mb_box.generic.y = 160 * cl_fontScale->value;
	s_mb_box.generic.name = "Motion Blur";
	s_mb_box.itemnames = NAMES_YES_NO;
	s_mb_box.curvalue = r_motionBlur->value;
	s_mb_box.generic.callback = mbCallback;
	s_mb_box.generic.statusbar = "Draw Motion Blur Effect";

	s_ssao.generic.type = MTYPE_SPINCONTROL;
	s_ssao.generic.x = 0;
	s_ssao.generic.y = 170 * cl_fontScale->value;
	s_ssao.generic.name = "SSAO";
	s_ssao.itemnames = NAMES_YES_NO;
	s_ssao.curvalue = r_ssao->value;
	s_ssao.generic.callback = ssaoCallback;
	s_ssao.generic.statusbar = "Draw Screen Space Ambient Occlusion Effect";

	s_film_grain.generic.type = MTYPE_SPINCONTROL;
	s_film_grain.generic.x = 0;
	s_film_grain.generic.y = 180 * cl_fontScale->value;
	s_film_grain.generic.name = "Cinematic filter";
	s_film_grain.itemnames = film_filter;
	s_film_grain.curvalue = r_filmFilterType->value;
	s_film_grain.generic.callback = filmCallback;
	s_film_grain.generic.statusbar = "Use Cinematic Film Effect <technicolor or sepia>";

	s_fxaa_box.generic.type = MTYPE_SPINCONTROL;
	s_fxaa_box.generic.x = 0;
	s_fxaa_box.generic.y = 190 * cl_fontScale->value;
	s_fxaa_box.generic.name = "FXAA";
	s_fxaa_box.itemnames = NAMES_YES_NO;
	s_fxaa_box.curvalue = r_fxaa->value;
	s_fxaa_box.generic.callback = fxaaCallback;
	s_fxaa_box.generic.statusbar = "Use Post-Process Anti-Aliasing";

	s_finish_box.generic.type = MTYPE_SPINCONTROL;
	s_finish_box.generic.x = 0;
	s_finish_box.generic.y = 200 * cl_fontScale->value;
	s_finish_box.generic.name = "Vertical Sync";
	s_finish_box.generic.callback = vSyncCallBack;
	s_finish_box.curvalue = r_vsync->value;
	if (r_vsync->value >= 3) Cvar_SetValue("r_vsync", 2);
	s_finish_box.itemnames = adaptive_vc;
	s_finish_box.generic.statusbar = "Standart Or Adaptive";

	s_menuAction_color.generic.type = MTYPE_ACTION;
	s_menuAction_color.generic.x = 0;
	s_menuAction_color.generic.y = 220 * cl_fontScale->value;
	s_menuAction_color.generic.name = "Post-Process Settings...";
	s_menuAction_color.generic.callback = ColorSettingsFunc;
	s_menuAction_color.generic.statusbar = "Color Balance and Bloom Settings";

	s_defaults_action.generic.type = MTYPE_ACTION;
	s_defaults_action.generic.name = "reset to defaults";
	s_defaults_action.generic.x = 0;
	s_defaults_action.generic.y = 240 * cl_fontScale->value;
	s_defaults_action.generic.callback = ResetDefaults;

	s_apply_action.generic.type = MTYPE_ACTION;
	s_apply_action.generic.name = "Apply Changes";
	s_apply_action.generic.x = 0;
	s_apply_action.generic.y = 250 * cl_fontScale->value;
	s_apply_action.generic.callback = ApplyChanges;

	MENU_SIZE = 260;

	Menu_AddItem(&s_opengl_menu, (void *)&s_mode_list);
	Menu_AddItem(&s_opengl_menu, (void *)&s_fs_box);

	Menu_AddItem(&s_opengl_menu, (void *)&s_aniso_slider);

	Menu_AddItem(&s_opengl_menu, (void *)&s_tc_box);

	Menu_AddItem(&s_opengl_menu, (void *)&a_autoBump_list);
	Menu_AddItem(&s_opengl_menu, (void *)&s_parallax_box);
	Menu_AddItem(&s_opengl_menu, (void *)&s_reliefScale_slider);
	Menu_AddItem(&s_opengl_menu, (void *)&s_ambientLevel_slider);
	Menu_AddItem(&s_opengl_menu, (void *)&s_flare_box);
	Menu_AddItem(&s_opengl_menu, (void *)&s_bloom_box);
	Menu_AddItem(&s_opengl_menu, (void *)&s_dof_box);
	Menu_AddItem(&s_opengl_menu, (void *)&s_radBlur_box);
	Menu_AddItem(&s_opengl_menu, (void *)&s_mb_box);
	Menu_AddItem(&s_opengl_menu, (void *)&s_ssao);
	Menu_AddItem(&s_opengl_menu, (void *)&s_film_grain);
	Menu_AddItem(&s_opengl_menu, (void *)&s_fxaa_box);
	Menu_AddItem(&s_opengl_menu, (void *)&s_finish_box);

	Menu_AddItem(&s_opengl_menu, (void *)&s_menuAction_color);
	Menu_AddItem(&s_opengl_menu, (void *)&s_defaults_action);
	Menu_AddItem(&s_opengl_menu, (void *)&s_apply_action);

	Menu_Center(&s_opengl_menu);
	s_opengl_menu.x -= 8;
}


/**
 *
 */
void VID_MenuDraw()
{
	s_current_menu = &s_opengl_menu;
	MENU_SIZE = 190 * cl_fontScale->value;

	M_Banner("gfx/m_banner_video.tga");

	// move cursor to a reasonable starting position
	Menu_AdjustCursor(s_current_menu, 1);
	Menu_Draw(s_current_menu);
}


/**
 *
 */
int VID_MenuKey(int key)
{
	menuframework_s *m = s_current_menu;

	switch (key)
	{
	case K_MOUSE2:
	case K_ESCAPE:
		CancelChanges(nullptr);
		return 0;
		break;

	case K_MWHEELUP:
	case K_KP_UPARROW:
	case K_UPARROW:
		m->cursor--;
		Menu_AdjustCursor(m, -1);
		break;

	case K_MWHEELDOWN:
	case K_KP_DOWNARROW:
	case K_DOWNARROW:
		m->cursor++;
		Menu_AdjustCursor(m, 1);
		break;

	case K_KP_LEFTARROW:
	case K_LEFTARROW:
		Menu_SlideItem(m, -1);
		break;

	case K_KP_RIGHTARROW:
	case K_RIGHTARROW:
		Menu_SlideItem(m, 1);
		break;

	case K_KP_ENTER:
	case K_ENTER:
		if (!Menu_SelectItem(m))
			ApplyChanges(nullptr);
		break;
	}

	return MENU_IN_SOUND;
}
