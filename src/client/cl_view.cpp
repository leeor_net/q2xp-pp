/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// cl_view.c -- player rendering positioning

#include "client.h"

#include "music.h"

#include "../common/def_refdef.h"
#include "../common/def_renderfx.h"
#include "../common/string_util.h"

#include "../ref_gl/r_local.h"


extern bool loadingMessage;
extern char loadingMessages[5][96];
extern float loadingPercent;

//=============
//
// development tools for weapons
//
int gun_frame;
Model* gun_model;

//=============

ConsoleVariable *cl_stats;


int r_numdlights;
dlight_t r_dlights[MAX_DLIGHTS];

int r_numentities;
entity_t r_entities[MAX_ENTITIES];

int r_numparticles;
Particle r_particles[MAX_PARTICLES];

lightstyle_t r_lightstyles[MAX_LIGHTSTYLES];

char cl_weaponmodels[MAX_CLIENTWEAPONMODELS][MAX_QPATH];
int num_cl_weaponmodels;


/*
====================
V_ClearScene

Specifies the model that will be used as the world
====================
*/

void V_ClearScene () {
	r_numdlights = 0;
	r_numentities = 0;
	r_numparticles = 0;
}

/*
=====================
V_AddEntity

=====================
*/
void V_AddEntity (entity_t * ent) {

	if (ent->flags & RF_VIEWERMODEL) {
		int i;

		for (i = 0; i < 3; i++)
			ent->oldorigin[i] = ent->origin[i] = cl.predicted_origin[i];

		if (cl_thirdPerson->value)
			ent->flags &= ~RF_VIEWERMODEL;
	}

	if (r_numentities >= MAX_ENTITIES)
		return;
	r_entities[r_numentities++] = *ent;
}

/*
=====================
V_AddParticle

=====================
*/
void V_AddParticle(vec3_t org, vec3_t length, vec3_t color, float alpha, int type, float size, int sFactor, int dFactor, int flags, int time, float orient, float len, vec3_t oldOrg, vec3_t dir)
{
	vec3_t lm;

	if (r_numparticles >= MAX_PARTICLES) { return; }

	Particle* p = &r_particles[r_numparticles++];
	VectorCopy(org, p->origin);
	VectorCopy(length, p->length);
	VectorCopy(color, p->color);
	VectorCopy(dir, p->dir);
	p->alpha = alpha;
	p->type = type;
	p->size = size;
	p->sFactor = sFactor;
	p->dFactor = dFactor;
	p->flags = flags;
	p->time = (float)time * 0.001f;
	p->orient = orient;
	p->len = len;
	VectorCopy(oldOrg, p->previous_origin);

	if (p->flags & PARTICLE_VERTEXLIGHT)
	{
		R_LightColor(org, lm);
		VectorMul(p->color, lm, p->color);
	}
}


/*
=====================
V_AddLight

=====================
*/
void V_AddLight(vec3_t org, float intensity, float r, float g, float b, vec3_t ang, float cone, int filter)
{
	if (r_numdlights >= MAX_DLIGHTS)
		return;

	dlight_t* dl = &r_dlights[r_numdlights++];
	VectorCopy(org, dl->origin);
	VectorCopy(ang, dl->angles);

	dl->intensity = intensity;
	dl->_cone = cone;
	dl->filter = filter;
	dl->color[0] = r;
	dl->color[1] = g;
	dl->color[2] = b;
}


/*
=====================
V_AddLightStyle

=====================
*/
void V_AddLightStyle (int style, float r, float g, float b) {
	lightstyle_t *ls;

	if (style < 0 || style > MAX_LIGHTSTYLES)
		Com_Error (ERR_DROP, "Bad light style %i", style);
	ls = &r_lightstyles[style];

	ls->white = r + g + b;
	ls->rgb[0] = r;
	ls->rgb[1] = g;
	ls->rgb[2] = b;
}


//===================================================================

/*
=================
CL_PrepRefresh

Call before entering a new level, or after changing dlls
=================
*/
void R_GenSkyCubeMap (char* name);

#ifdef _WIN32
extern int	xiActiveController;
void SetRumble(int inputDeviceNum, int rumbleLow, int rumbleHigh);
#endif

void CL_PrepRefresh()
{
	char mapname[32];
	int i = 0, start = 0, stop = 0;
	char name[MAX_QPATH];
	float rotate, sec;
	vec3_t axis;
	bool newPlaque = !cls.disable_screen || SCR_DrawingLoad();

	if (!cl.configstrings[CS_MODELS + 1][0]) return; // no map loaded

	if (newPlaque)
		SCR_BeginLoadingPlaque();

#ifdef _WIN32
	SetRumble(xiActiveController, 0, 0);
#endif

	start = Sys_Milliseconds();

	loadScreenColorFade = 0.1;

	loadingMessage = true;
	Q_sprintf(loadingMessages[0], sizeof(loadingMessages[0]), "Loading Map...");
	Q_sprintf(loadingMessages[1], sizeof(loadingMessages[1]), "Loading Models...");
	Q_sprintf(loadingMessages[2], sizeof(loadingMessages[2]), "Loading Pics...");
	Q_sprintf(loadingMessages[3], sizeof(loadingMessages[3]), "Loading Clients...");
	loadingPercent = 1;

	// let the render dll load the map
	strcpy(mapname, cl.configstrings[CS_MODELS + 1] + 5);	// skip "maps/"
	mapname[strlen(mapname) - 4] = 0;	// cut off ".bsp"

	// register models, pics, and skins
	Com_Printf("Map: %s\r", mapname);
	SCR_UpdateScreen();
	R_BeginRegistration(mapname);

	Com_Printf("                                     \r");
	Q_sprintf(loadingMessages[0], sizeof(loadingMessages[0]), "Loading Map... done");
	loadingPercent = 35;
	loadScreenColorFade = 0.35;

	// precache status bar pics
	Com_Printf("pics\r");
	SCR_UpdateScreen();
	SCR_TouchPics();
	Com_Printf("                                     \r");

	CL_RegisterTEntModels();

	num_cl_weaponmodels = 1;
	strcpy(cl_weaponmodels[0], "weapon.md2");

	for (i = 1; i < MAX_MODELS && cl.configstrings[CS_MODELS + i][0]; i++)
	{
		strcpy(name, cl.configstrings[CS_MODELS + i]);
		name[37] = 0;			// never go beyond one line
		if (name[0] != '*')
			Com_Printf("%s\r", name);
		SCR_UpdateScreen();
		Sys_SendKeyEvents();	// pump message loop
		if (name[0] == '#')
		{
			// special player weapon model
			if (num_cl_weaponmodels < MAX_CLIENTWEAPONMODELS)
			{
				strncpy(cl_weaponmodels[num_cl_weaponmodels],
					cl.configstrings[CS_MODELS + i] + 1,
					sizeof(cl_weaponmodels[num_cl_weaponmodels]) - 1);
				num_cl_weaponmodels++;
			}
		}
		else
		{
			Q_sprintf(loadingMessages[1], sizeof(loadingMessages[1]), "Loading Models... %s", cl.configstrings[CS_MODELS + i]);

			cl.model_draw[i] = R_RegisterModel(cl.configstrings[CS_MODELS + i]);
			if (name[0] == '*')
				cl.model_clip[i] = CM_InlineModel(cl.configstrings[CS_MODELS + i]);
			else
				cl.model_clip[i] = nullptr;
		}
		if (name[0] != '*')
			Com_Printf("                                     \r");
	}
	Q_sprintf(loadingMessages[1], sizeof(loadingMessages[1]), "Loading Models... done");
	loadingPercent = 55;
	loadScreenColorFade = 0.55;

	Com_Printf("images\r", i);
	SCR_UpdateScreen();
	for (i = 1; i < MAX_IMAGES && cl.configstrings[CS_IMAGES + i][0]; i++)
	{
		cl.image_precache[i] = Draw_FindPic(cl.configstrings[CS_IMAGES + i]);
		SCR_UpdateScreen();
		Sys_SendKeyEvents();	// pump message loop
	}

	Com_Printf("                                     \r");
	Q_sprintf(loadingMessages[2], sizeof(loadingMessages[2]), "Loading Pics... done");
	loadingPercent = 75;

	loadScreenColorFade = 0.75;

	for (i = 0; i < MAX_CLIENTS; i++)
	{
		if (!cl.configstrings[CS_PLAYERSKINS + i][0])
			continue;

		Q_sprintf(loadingMessages[3], sizeof(loadingMessages[3]),
			"Loading Clients...%i", i);

		Com_Printf("client %i\r", i);

		SCR_UpdateScreen();
		Sys_SendKeyEvents();	// pump message loop
		CL_ParseClientinfo(i);
		Com_Printf("                                     \r");
		Q_sprintf(loadingMessages[3], sizeof(loadingMessages[3]), "Loading Clients... done");
		loadingPercent = 85;

		loadScreenColorFade = 1.0;
	}
	loadingPercent = 100;

	CL_LoadClientinfo(&cl.baseclientinfo, "unnamed\\male/grunt");

	// set sky textures and speed
	Com_Printf("sky\r", i);
	SCR_UpdateScreen();
	rotate = atof(cl.configstrings[CS_SKYROTATE]);
	sscanf(cl.configstrings[CS_SKYAXIS], "%f %f %f", &axis[0], &axis[1], &axis[2]);
	R_SetSky(cl.configstrings[CS_SKY], rotate, axis);
	R_GenSkyCubeMap(cl.configstrings[CS_SKY]);
	Com_Printf("                                     \r");

	Con_ClearNotify();

	SCR_UpdateScreen();
	cl.refresh_prepped = true;
	cl.force_refdef = true;		// make sure we have a valid refdef

	Q_sprintf(loadingMessages[0], sizeof(loadingMessages[0]), "");
	Q_sprintf(loadingMessages[1], sizeof(loadingMessages[1]), "");
	Q_sprintf(loadingMessages[2], sizeof(loadingMessages[2]), "");
	Q_sprintf(loadingMessages[3], sizeof(loadingMessages[3]), "");

	// start the cd track
	Music_Play();

	loadingMessage = false;
	stop = Sys_Milliseconds();
	sec = (float)stop - (float)start;
	sec *= 0.001;
	Com_DPrintf("level loading time = %5.4f sec\n", sec);

	// the renderer can now free unneeded stuff
	R_EndRegistration();

	if (newPlaque)
		SCR_EndLoadingPlaque();
	else
		Cvar_Set("paused", "0");

	cl.minFps = cl.maxFps = 0;
}

//============================================================================

// gun frame debugging functions
void V_Gun_Next_f()
{
	gun_frame++;
	Com_Printf("frame %i\n", gun_frame);
}


void V_Gun_Prev_f()
{
	gun_frame--;
	if (gun_frame < 0)
		gun_frame = 0;
	Com_Printf("frame %i\n", gun_frame);
}


void V_Gun_Model_f()
{
	char name[MAX_QPATH];

	if (Cmd_Argc() != 2)
	{
		gun_model = nullptr;
		return;
	}
	Q_sprintf(name, sizeof(name), "models/%s/tris.md2", Cmd_Argv(1));
	gun_model = R_RegisterModel(name);
}

//============================================================================



#define AR_4x3	4.0f / 3.0f 

void CalcFovForScreen (float ingameFOV) {
	float	x, y, ratio_x, ratio_y;
	float	screenAspect = (float)videoWidth() / (float)videoHeight();

	if (ingameFOV < 1 || ingameFOV > 179)
		ingameFOV = 91;

	// calc FOV for 640x480 view (4x3 aspect ratio)
	x = 640.0f / tan (ingameFOV / 360.0f * M_PI);
	y = atan2 (480.0f, x);
	cl.refdef.fov_y = y * 360.0f / M_PI;

	if (screenAspect == AR_4x3) {
		cl.refdef.fov_x = ingameFOV;
		return;
	}
	// calc FOV for widescreen
	ratio_x = (float)videoWidth();
	ratio_y = (float)videoHeight();

	y = ratio_y / tan (cl.refdef.fov_y / 360.0f * M_PI);
	cl.refdef.fov_x = atan2 (ratio_x, y) * 360.0f / M_PI;

	if (cl.refdef.fov_x < ingameFOV) {
		cl.refdef.fov_x = ingameFOV;
		x = ratio_x / tan (cl.refdef.fov_x / 360.0f * M_PI);
		cl.refdef.fov_y = atan2 (ratio_y, x) * 360.0f / M_PI;
	}

}


/*
==================
V_RenderView

==================
*/

void V_RenderView() {
	extern int entitycmpfnc(const entity_t *, const entity_t *);

	if (cls.state != ca_active)
		return;

	if (!cl.refresh_prepped)
		return;					// still loading

	if (cl_timedemo->value) {
		if (!cl.timedemo_start)
			cl.timedemo_start = Sys_Milliseconds();
		cl.timedemo_frames++;
	}
	// an invalid frame will just use the exact previous refdef
	// we can't use the old frame if the video mode has changed, though...
	if (cl.frame.valid && (cl.force_refdef || !cl_paused->value)) {
		cl.force_refdef = false;

		V_ClearScene();

		// build a refresh entity list and calc cl.sim*
		// this also calls CL_CalcViewValues which loads
		// v_forward, etc.
		CL_AddEntities();

		// never let it sit exactly on a node line, because a water plane
		// can
		// dissapear when viewed with the eye exactly on it.
		// the server protocol only specifies to 1/8 pixel, so add 1/16 in 
		// each axis
		cl.refdef.vieworg[0] += 1.0 / 16;
		cl.refdef.vieworg[1] += 1.0 / 16;
		cl.refdef.vieworg[2] += 1.0 / 16;

		cl.refdef.x = SCR_ViewRectangle()->x;
		cl.refdef.y = SCR_ViewRectangle()->y;
		cl.refdef.width = SCR_ViewRectangle()->width;
		cl.refdef.height = SCR_ViewRectangle()->height;

		CalcFovForScreen(cl.refdef.fov_x);

		cl.refdef.time = cl.time * 0.001;

		if (cl_fontScale->value < 1)
			Cvar_Set("cl_fontScale", "1");

		// Warp if underwater ala q3a :-)
		if (cl.refdef.rdflags & RDF_UNDERWATER) {
			float f = sin(cl.time * 0.001 * 0.4 * (M_PI * 2.7));

			cl.refdef.fov_x += f;
			cl.refdef.fov_y -= f;
		}

#ifdef _WIN32
		// add xBox controller vibration 
		int value = cl.frame.playerstate.stats[STAT_HEALTH];

		if ((cl.refdef.rdflags & RDF_PAIN) && (value > 0))
			SetRumble(xiActiveController, 4096, 65535);
		else
			SetRumble(xiActiveController, 0, 0);
#endif

		cl.refdef.areabits = cl.frame.areabits;

		if (!cl_add_entities->value)
			r_numentities = 0;

		if (!cl_add_particles->value)
			r_numparticles = 0;
		if (!cl_add_lights->value)
			r_numdlights = 0;
		if (!cl_add_blend->value)
			VectorClear(cl.refdef.blend);



		cl.refdef.num_entities = r_numentities;
		cl.refdef.entities = r_entities;

		cl.refdef.num_particles = r_numparticles;
		cl.refdef.particles = r_particles;

		cl.refdef.num_dlights = r_numdlights;
		cl.refdef.dlights = r_dlights;

		cl.refdef.lightstyles = r_lightstyles;

		cl.refdef.rdflags = cl.frame.playerstate.rdflags;

		// sort entities for better cache locality
		qsort(cl.refdef.entities, cl.refdef.num_entities, sizeof(cl.refdef.entities[0]), (int(*)(const void*, const void*)) entitycmpfnc);
	}

	R_RenderFrame(&cl.refdef);

	if (cl_stats->value)
		Com_Printf("ent:%i  dlights:%i  part:%i \n", r_numentities, r_numdlights, r_numparticles);

	if (log_stats->value && LOG_STATS_FILE)
		FS_fprintf(LOG_STATS_FILE, "%i,%i,%i,", r_numentities, r_numdlights, r_numparticles);

	SCR_DrawCrosshair();
}


/*
=============
V_Viewpos_f
=============
*/
void V_Viewpos_f () {
	Com_Printf ("(%i %i %i) : %i\n", (int)cl.refdef.vieworg[0],
		(int)cl.refdef.vieworg[1], (int)cl.refdef.vieworg[2],
		(int)cl.refdef.viewangles[YAW]);
}


/**
 * \fixme	Give this a better name.
 */
void V_Init()
{
	Cmd_AddCommand("gun_next", V_Gun_Next_f);
	Cmd_AddCommand("gun_prev", V_Gun_Prev_f);
	Cmd_AddCommand("gun_model", V_Gun_Model_f);

	Cmd_AddCommand("viewpos", V_Viewpos_f);

	cl_stats = Cvar_Get("cl_stats", "0", 0);
}
