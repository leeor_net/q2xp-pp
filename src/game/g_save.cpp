/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "g_local.h"
#include "g_item_defs.h"

#include <stdio.h>

// ===============================================================================
// = IMPORTS
// ===============================================================================
extern ConsoleVariable* r_radialBlur;		///\fixme Find a better way to import this.
extern ConsoleVariable* weaponHitAccuracy;	///\fixme Find a better way to import this.


// ===============================================================================
// = MODULE VARIABLES
// ===============================================================================
mmove_t mmove_reloc;


// ===============================================================================
// = FIELD DEFINITIONS
// ===============================================================================
/**
 * 
 */
field_t fields[] =
{
	{ "classname", FOFS(classname), F_LSTRING },
	{ "model", FOFS(model), F_LSTRING },
	{ "spawnflags", FOFS(spawnflags), F_INT },
	{ "speed", FOFS(speed), F_FLOAT },
	{ "accel", FOFS(accel), F_FLOAT },
	{ "decel", FOFS(decel), F_FLOAT },
	{ "target", FOFS(target), F_LSTRING },
	{ "targetname", FOFS(targetname), F_LSTRING },
	{ "pathtarget", FOFS(pathtarget), F_LSTRING },
	{ "deathtarget", FOFS(deathtarget), F_LSTRING },
	{ "killtarget", FOFS(killtarget), F_LSTRING },
	{ "combattarget", FOFS(combattarget), F_LSTRING },
	{ "message", FOFS(message), F_LSTRING },
	{ "team", FOFS(team), F_LSTRING },
	{ "wait", FOFS(wait), F_FLOAT },
	{ "delay", FOFS(delay), F_FLOAT },
	{ "random", FOFS(random), F_FLOAT },
	{ "move_origin", FOFS(move_origin), F_VECTOR },
	{ "move_angles", FOFS(move_angles), F_VECTOR },
	{ "style", FOFS(style), F_INT },
	{ "count", FOFS(count), F_INT },
	{ "health", FOFS(health), F_INT },
	{ "sounds", FOFS(sounds), F_INT },
	{ "light", 0, F_IGNORE },
	{ "color", FOFS(color), F_VECTOR },
	{ "dmg", FOFS(dmg), F_INT },
	{ "mass", FOFS(mass), F_INT },
	{ "volume", FOFS(volume), F_FLOAT },
	{ "attenuation", FOFS(attenuation), F_FLOAT },
	{ "map", FOFS(map), F_LSTRING },
	{ "origin", FOFS(s.origin), F_VECTOR },
	{ "angles", FOFS(s.angles), F_VECTOR },
	{ "angle", FOFS(s.angles), F_ANGLEHACK },

	{ "goalentity", FOFS(goalentity), F_EDICT, FFL_NOSPAWN },
	{ "movetarget", FOFS(movetarget), F_EDICT, FFL_NOSPAWN },
	{ "enemy", FOFS(enemy), F_EDICT, FFL_NOSPAWN },
	{ "oldenemy", FOFS(oldenemy), F_EDICT, FFL_NOSPAWN },
	{ "activator", FOFS(activator), F_EDICT, FFL_NOSPAWN },
	{ "groundentity", FOFS(groundentity), F_EDICT, FFL_NOSPAWN },
	{ "teamchain", FOFS(teamchain), F_EDICT, FFL_NOSPAWN },
	{ "teammaster", FOFS(teammaster), F_EDICT, FFL_NOSPAWN },
	{ "owner", FOFS(owner), F_EDICT, FFL_NOSPAWN },
	{ "mynoise", FOFS(mynoise), F_EDICT, FFL_NOSPAWN },
	{ "mynoise2", FOFS(mynoise2), F_EDICT, FFL_NOSPAWN },
	{ "target_ent", FOFS(target_ent), F_EDICT, FFL_NOSPAWN },
	{ "chain", FOFS(chain), F_EDICT, FFL_NOSPAWN },

	{ "prethink", FOFS(prethink), F_FUNCTION, FFL_NOSPAWN },
	{ "think", FOFS(think), F_FUNCTION, FFL_NOSPAWN },
	{ "blocked", FOFS(blocked), F_FUNCTION, FFL_NOSPAWN },
	{ "touch", FOFS(touch), F_FUNCTION, FFL_NOSPAWN },
	{ "use", FOFS(use), F_FUNCTION, FFL_NOSPAWN },
	{ "pain", FOFS(pain), F_FUNCTION, FFL_NOSPAWN },
	{ "die", FOFS(die), F_FUNCTION, FFL_NOSPAWN },

	{ "stand", FOFS(monsterinfo.stand), F_FUNCTION, FFL_NOSPAWN },
	{ "idle", FOFS(monsterinfo.idle), F_FUNCTION, FFL_NOSPAWN },
	{ "search", FOFS(monsterinfo.search), F_FUNCTION, FFL_NOSPAWN },
	{ "walk", FOFS(monsterinfo.walk), F_FUNCTION, FFL_NOSPAWN },
	{ "run", FOFS(monsterinfo.run), F_FUNCTION, FFL_NOSPAWN },
	{ "dodge", FOFS(monsterinfo.dodge), F_FUNCTION, FFL_NOSPAWN },
	{ "attack", FOFS(monsterinfo.attack), F_FUNCTION, FFL_NOSPAWN },
	{ "melee", FOFS(monsterinfo.melee), F_FUNCTION, FFL_NOSPAWN },
	{ "sight", FOFS(monsterinfo.sight), F_FUNCTION, FFL_NOSPAWN },
	{ "checkattack", FOFS(monsterinfo.checkattack), F_FUNCTION, FFL_NOSPAWN },
	{ "currentmove", FOFS(monsterinfo.currentmove), F_MMOVE, FFL_NOSPAWN },

	{ "endfunc", FOFS(moveinfo.endfunc), F_FUNCTION, FFL_NOSPAWN },

	// temp spawn vars -- only valid when the spawn function is called
	{ "lip", STOFS(lip), F_INT, FFL_SPAWNTEMP },
	{ "distance", STOFS(distance), F_INT, FFL_SPAWNTEMP },
	{ "height", STOFS(height), F_INT, FFL_SPAWNTEMP },
	{ "noise", STOFS(noise), F_LSTRING, FFL_SPAWNTEMP },
	{ "pausetime", STOFS(pausetime), F_FLOAT, FFL_SPAWNTEMP },
	{ "item", STOFS(item), F_LSTRING, FFL_SPAWNTEMP },

	//need for item field in edict struct, FFL_SPAWNTEMP item will be skipped on saves
	{ "item", FOFS(item), F_ITEM },

	{ "gravity", STOFS(gravity), F_LSTRING, FFL_SPAWNTEMP },
	{ "sky", STOFS(sky), F_LSTRING, FFL_SPAWNTEMP },
	{ "skyrotate", STOFS(skyrotate), F_FLOAT, FFL_SPAWNTEMP },
	{ "skyaxis", STOFS(skyaxis), F_VECTOR, FFL_SPAWNTEMP },
	{ "minyaw", STOFS(minyaw), F_FLOAT, FFL_SPAWNTEMP },
	{ "maxyaw", STOFS(maxyaw), F_FLOAT, FFL_SPAWNTEMP },
	{ "minpitch", STOFS(minpitch), F_FLOAT, FFL_SPAWNTEMP },
	{ "maxpitch", STOFS(maxpitch), F_FLOAT, FFL_SPAWNTEMP },
	{ "nextmap", STOFS(nextmap), F_LSTRING, FFL_SPAWNTEMP },

	{ 0, 0, F_INT, 0 }
};


/**
 * 
 */
field_t levelfields[] =
{
	{ "changemap", LLOFS(changemap), F_LSTRING },

	{ "sight_client", LLOFS(sight_client), F_EDICT },
	{ "sight_entity", LLOFS(sight_entity), F_EDICT },
	{ "sound_entity", LLOFS(sound_entity), F_EDICT },
	{ "sound2_entity", LLOFS(sound2_entity), F_EDICT },

	{ nullptr, 0, F_INT }
};


/**
 * 
 */
field_t clientfields[] =
{
	{ "pers.weapon", CLOFS(pers.weapon), F_ITEM },
	{ "pers.lastweapon", CLOFS(pers.lastweapon), F_ITEM },
	{ "newweapon", CLOFS(newweapon), F_ITEM },

	{ nullptr, 0, F_INT }
};


/**
 * This will be called when the dll is first loaded, which
 * only happens when a new game is started or a save game
 * is loaded.
 */
void InitGame(void)
{
	gi.dprintf(BAR_BOLD "\n");
	gi.dprintf("Game Init\n");
	gi.dprintf(BAR_BOLD "\n\n");

	gun_x = gi.cvar("gun_x", "0", 0);
	gun_y = gi.cvar("gun_y", "0", 0);
	gun_z = gi.cvar("gun_z", "0", 0);

	//FIXME: sv_ prefix is wrong for these
	sv_rollspeed = gi.cvar("sv_rollspeed", "200", 0);
	sv_rollangle = gi.cvar("sv_rollangle", "2", 0);
	sv_maxvelocity = gi.cvar("sv_maxvelocity", "2000", 0);
	sv_gravity = gi.cvar("sv_gravity", "800", 0);
	dedicated = gi.cvar("dedicated", "0", CVAR_NOSET);

	// latched vars
	sv_cheats = gi.cvar("cheats", "0", CVAR_SERVERINFO | CVAR_LATCH);
	gi.cvar("gamename", GAMEVERSION, CVAR_SERVERINFO | CVAR_LATCH);
	gi.cvar("gamedate", __DATE__, CVAR_SERVERINFO | CVAR_LATCH);

	maxclients = gi.cvar("maxclients", "4", CVAR_SERVERINFO | CVAR_LATCH);
	maxspectators = gi.cvar("maxspectators", "4", CVAR_SERVERINFO);
	deathmatch = gi.cvar("deathmatch", "0", CVAR_LATCH);
	coop = gi.cvar("coop", "0", CVAR_LATCH);
	skill = gi.cvar("skill", "1", CVAR_LATCH);
	maxentities = gi.cvar("maxentities", "1024", CVAR_LATCH);

	// change anytime vars
	dmflags = gi.cvar("dmflags", "0", CVAR_SERVERINFO);
	fraglimit = gi.cvar("fraglimit", "0", CVAR_SERVERINFO);
	timelimit = gi.cvar("timelimit", "0", CVAR_SERVERINFO);
	password = gi.cvar("password", "", CVAR_USERINFO);
	spectator_password = gi.cvar("spectator_password", "", CVAR_USERINFO);
	needpass = gi.cvar("needpass", "0", CVAR_SERVERINFO);
	filterban = gi.cvar("filterban", "1", 0);

	g_select_empty = gi.cvar("g_select_empty", "0", CVAR_ARCHIVE);

	run_pitch = gi.cvar("run_pitch", "0.002", 0);
	run_roll = gi.cvar("run_roll", "0.005", 0);
	bob_up = gi.cvar("bob_up", "0.005", 0);
	bob_pitch = gi.cvar("bob_pitch", "0.002", 0);
	bob_roll = gi.cvar("bob_roll", "0.002", 0);

	// flood control
	flood_msgs = gi.cvar("flood_msgs", "4", 0);
	flood_persecond = gi.cvar("flood_persecond", "4", 0);
	flood_waitdelay = gi.cvar("flood_waitdelay", "10", 0);

	// dm map list
	sv_maplist = gi.cvar("sv_maplist", "", 0);

	g_monsterRespawn = gi.cvar("g_monsterRespawn", "0.5", CVAR_ARCHIVE);
	sv_solidcorpse = gi.cvar("sv_solidcorpse", "1", CVAR_ARCHIVE);
	r_radialBlur = gi.cvar("r_radialBlur", "1", CVAR_ARCHIVE);
	sv_stopClock = gi.cvar("sv_stopClock", "0", 0);
	weaponHitAccuracy = gi.cvar("weaponHitAccuracy", "1", CVAR_USERINFO | CVAR_ARCHIVE);

	// items
	InitItems();

	Q_sprintf(game.helpmessage1, sizeof(game.helpmessage1), "");
	Q_sprintf(game.helpmessage2, sizeof(game.helpmessage2), "");

	// initialize all entities for this game
	game.maxentities = maxentities->value;
	g_edicts = static_cast<edict_t*>(calloc(game.maxentities, sizeof(edict_t)));
	globals.edicts = g_edicts;
	globals.max_edicts = game.maxentities;

	// initialize all clients for this game
	game.maxclients = maxclients->value;
	game.clients = static_cast<gclient_t*>(calloc(game.maxclients, sizeof(gclient_t)));
	globals.num_edicts = game.maxclients + 1;

	gi.dprintf(S_COLOR_YELLOW "\nUsing Rogue Arena network protocol\n");
	gi.dprintf(BAR_LITE "\n\n");
}


/**
 * 
 */
void WriteField1(field_t* field, byte* base)
{
	int len = 0;
	int index = 0;

	if (field->flags & FFL_SPAWNTEMP)
		return;

	void* p = (void*)(base + field->ofs);
	switch (field->type)
	{
	case F_INT:
	case F_FLOAT:
	case F_ANGLEHACK:
	case F_VECTOR:
	case F_IGNORE:
		break;

	case F_LSTRING:
	case F_GSTRING:
		if (*(char**)p) { len = strlen(*(char**)p) + 1; }
		else { len = 0; }

		*(int*)p = len;
		break;

	case F_EDICT:
		if (*(edict_t **)p == nullptr) { index = -1; }
		else { index = *(edict_t **)p - g_edicts; }
		
		*(int*)p = index;
		break;

	case F_CLIENT:
		if (*(gclient_t **)p == nullptr) { index = -1; }
		else { index = *(gclient_t **)p - game.clients; }
		
		*(int*)p = index;
		break;

	case F_ITEM:
		if (*(edict_t **)p == nullptr) { index = -1; }
		else { index = *(gitem_t **)p - itemlist; }
		
		*(int*)p = index;
		break;

		//relative to code segment
	case F_FUNCTION:
		if (*(byte**)p == nullptr) { index = 0; }
		else { index = *(byte**)p - ((byte*)InitGame); }

		*(int*)p = index;
		break;

		//relative to data segment
	case F_MMOVE:
		if (*(byte**)p == nullptr) { index = 0; }
		else { index = *(byte**)p - (byte*)&mmove_reloc; }
		*(int*)p = index;
		break;

	default:
		gi.error("WriteEdict: unknown field type");
	}
}


/**
 * 
 */
void WriteField2(FS_File* f, field_t *field, byte *base)
{

	if (field->flags & FFL_SPAWNTEMP)
		return;

	void* p = (void*)(base + field->ofs);
	if (field->type == F_LSTRING)
	{
		if (*(char**)p)
		{
			int len = strlen(*(char**)p) + 1;
			gi.FS_Write(f, *(char**)p, len, 1);
		}
	}
}


/**
 * 
 */
void ReadField(FS_File* f, field_t* field, byte* base)
{
	int len = 0;
	int index = 0;

	if (field->flags & FFL_SPAWNTEMP)
		return;

	void* p = (void*)(base + field->ofs);
	switch (field->type)
	{
	case F_INT:
	case F_FLOAT:
	case F_ANGLEHACK:
	case F_VECTOR:
	case F_IGNORE:
		break;

	case F_LSTRING:
		len = *(int *)p;
		if (!len)
		{
			*(char* *)p = nullptr;
		}
		else
		{
			*(char**)p = static_cast<char*>(calloc(len, sizeof(char)));
			gi.FS_Read(*(char**)p, len, 1, f);
		}
		break;

	case F_EDICT:
		index = *(int *)p;
		if (index == -1) { *(edict_t **)p = nullptr; }
		else { *(edict_t **)p = &g_edicts[index]; }
		break;

	case F_CLIENT:
		index = *(int *)p;
		if (index == -1) { *(gclient_t **)p = nullptr; }
		else { *(gclient_t **)p = &game.clients[index]; }
		break;

	case F_ITEM:
		index = *(int*)p;
		if (index == -1) { *(gitem_t**)p = nullptr; }
		else { *(gitem_t **)p = &itemlist[index]; }
		break;

	//relative to code segment  <-- Hmm?
	case F_FUNCTION:
		index = *(int *)p;
		if (index == 0) { *(byte **)p = nullptr; }
		else { *(byte **)p = ((byte *)InitGame) + index; }
		break;

	//relative to data segment  <-- Hmm?
	case F_MMOVE:
		index = *(int *)p;
		if (index == 0) { *(byte **)p = nullptr; }
		else { *(byte **)p = (byte *)&mmove_reloc + index; }
		break;

	default:
		gi.error("ReadEdict: unknown field type");
	}
}


/**
 * All pointer variables (except function pointers) must be handled specially.
 */
void WriteClient(FS_File* f, gclient_t* client)
{
	// all of the ints, floats, and vectors stay as they are
	gclient_t temp = *client;

	// change the pointers to lengths or indexes
	for (field_t* field = clientfields; field->name; field++)
		WriteField1(field, (byte*)&temp);

	// write the block
	gi.FS_Write(f, &temp, sizeof(temp), 1);

	// now write any allocated data following the edict
	for (field_t* field = clientfields; field->name; field++)
		WriteField2(f, field, (byte*)client);
}


/**
 * All pointer variables (except function pointers) must be handled specially.
 */
void ReadClient(FS_File* f, gclient_t* client)
{
	gi.FS_Read(client, sizeof(*client), 1, f);

	for (field_t* field = clientfields; field->name; field++)
		ReadField(f, field, (byte *)client);
}


/**
 * This will be called whenever the game goes to a new level,
 * and when the user explicitly saves the game.
 *
 * Game information include cross level data, like multi level
 * triggers, help computer info, and all client states.
 *
 * A single player death will automatically restore from the
 * last save position.
 */
void WriteGame(const std::string& filename, bool autosave)
{
	if (!autosave) { SaveClientData(); }

	FS_File	*f = gi.FS_Open(filename, FS_OPEN_WRITE);
	if (!f) { gi.error("Couldn't open %s", filename.c_str()); }

	char str[16] = { '\0' };
	snprintf(str, sizeof(str), "%s", __DATE__);

	gi.FS_Write(f, str, sizeof(str), 1);

	game.autosaved = autosave;
	gi.FS_Write(f, &game, sizeof(game), 1);
	game.autosaved = false;

	for (int i = 0; i < game.maxclients; i++)
	{
		WriteClient(f, &game.clients[i]);
	}

	gi.FS_Close(f);
}


/**
 * 
 */
void ReadGame(const std::string& filename)
{
	FS_File* f = gi.FS_Open(filename, FS_OPEN_READ);

	if (!f)
		gi.error("Couldn't open %s", filename.c_str());

	char str[16] = { '\0' };
	gi.FS_Read(str, sizeof(str), 1, f);

	if (strcmp(str, __DATE__))
	{
		gi.FS_Close(f);
		gi.error("Save game from an older version.\n");
	}

	free(g_edicts);
	g_edicts = static_cast<edict_t*>(calloc(game.maxentities, sizeof(edict_t)));
	globals.edicts = g_edicts;

	gi.FS_Read(&game, sizeof(game), 1, f);

	free(game.clients);
	game.clients = static_cast<gclient_t*>(calloc(game.maxclients, sizeof(gclient_t)));

	for (int i = 0; i < game.maxclients; i++)
		ReadClient(f, &game.clients[i]);

	gi.FS_Close(f);
}


/**
 * All pointer variables (except function pointers) must be handled specially.
 */
void WriteEdict(FS_File* f, edict_t* ent)
{
	// all of the ints, floats, and vectors stay as they are
	edict_t temp = *ent;

	// change the pointers to lengths or indexes
	for (field_t* field = fields; field->name; field++)
		WriteField1(field, (byte *)&temp);

	// write the block
	gi.FS_Write(f, &temp, sizeof(temp), 1);

	// now write any allocated data following the edict
	for (field_t* field = fields; field->name; field++)
		WriteField2(f, field, (byte *)ent);
}


/**
 * All pointer variables (except function pointers) must be handled specially.
 */
void WriteLevelLocals(FS_File* f)
{
	// all of the ints, floats, and vectors stay as they are
	level_locals_t temp = level;

	// change the pointers to lengths or indexes
	for (field_t* field = levelfields; field->name; field++)
		WriteField1(field, (byte *)&temp);

	// write the block
	gi.FS_Write(f, &temp, sizeof(temp), 1);

	// now write any allocated data following the edict
	for (field_t* field = levelfields; field->name; field++)
		WriteField2(f, field, (byte *)&level);
}


/**
 * All pointer variables (except function pointers) must be handled specially.
 */
void ReadEdict(FS_File* f, edict_t* ent)
{
	gi.FS_Read(ent, sizeof(*ent), 1, f);

	for (field_t* field = fields; field->name; field++)
		ReadField(f, field, (byte *)ent);
}


/**
 * All pointer variables (except function pointers) must be handled specially.
 */
void ReadLevelLocals(FS_File* f)
{
	gi.FS_Read(&level, sizeof(level), 1, f);

	for (field_t* field = levelfields; field->name; field++)
		ReadField(f, field, (byte *)&level);
}


/**
 * 
 */
void WriteLevel(const char* filename)
{
	FS_File* f = gi.FS_Open(filename, FS_OPEN_WRITE);
	if (!f)
		gi.error("Couldn't open %s", filename);

	// write out edict size for checking
	int edictSize = sizeof(edict_t);

	gi.FS_Write(f, &edictSize, sizeof(edictSize), 1);

	// write out a function pointer for checking
	void* base = (void*)InitGame;
	gi.FS_Write(f, &base, sizeof(base), 1);

	// write out level_locals_t
	WriteLevelLocals(f);

	// write out all the entities
	for (int i = 0; i < globals.num_edicts; i++)
	{
		edict_t* ent = &g_edicts[i];
		if (!ent->inuse)
			continue;

		gi.FS_Write(f, &i, sizeof(i), 1);
		WriteEdict(f, ent);
	}

	int sentinel = -1;
	gi.FS_Write(f, &sentinel, sizeof(sentinel), 1);

	gi.FS_Close(f);
}


/**
 * SpawnEntities will allready have been called on the
 * level the same way it was when the level was saved.
 *
 * That is necessary to get the baselines
 * set up identically.
 *
 * The server will have cleared all of the world links before
 * calling ReadLevel.
 *
 * No clients are connected yet.
 */
void ReadLevel(const char* filename)
{
	FS_File* f = gi.FS_Open(filename, FS_OPEN_READ);
	if (!f)
		gi.error("Couldn't open %s", filename);

	// free any dynamic memory allocated by loading the level base state
	//gi.FreeTags (TAG_LEVEL);
	free(world);

	// wipe all the entities
	memset(g_edicts, 0, game.maxentities * sizeof(edict_t));
	globals.num_edicts = maxclients->value + 1;

	// check edict size
	int edictSize = 0;
	gi.FS_Read(&edictSize, sizeof(edictSize), 1, f);
	if (edictSize != sizeof(edict_t))
	{
		gi.FS_Close(f);
		gi.error(S_COLOR_RED "ReadLevel(): mismatched edict size");
	}

	// check function pointer base address
	void* base = nullptr;
	gi.FS_Read(&base, sizeof(base), 1, f);

	ReadLevelLocals(f);

	// load all the entities
	for (;;)
	{
		int entityNum = 0;
		if (gi.FS_Read(&entityNum, sizeof(entityNum), 1, f) != 1)
		{
			gi.FS_Close(f);
			gi.error("ReadLevel: failed to read entnum");
		}

		if (entityNum == -1)
			break;

		if (entityNum >= globals.num_edicts)
			globals.num_edicts = entityNum + 1;

		edict_t* ent = &g_edicts[entityNum];
		ReadEdict(f, ent);

		// let the server rebuild world links for this ent
		memset(&ent->area, 0, sizeof(ent->area));
		gi.linkentity(ent);
	}

	gi.FS_Close(f);

	// mark all clients as unconnected
	for (int i = 0; i < maxclients->value; i++)
	{
		edict_t* ent = &g_edicts[i + 1];
		ent->client = game.clients + i;
		ent->client->pers.connected = false;
	}

	// do any load time things at this point
	for (int i = 0; i < globals.num_edicts; i++)
	{
		edict_t* ent = &g_edicts[i];

		if (!ent->inuse)
			continue;

		// fire any cross-level triggers
		if (ent->classname)
		{
			if (strcmp(ent->classname, "target_crosslevel_target") == 0)
				ent->nextthink = level.time + ent->delay;
		}
	}
}
