#include "g_items.h"

void Drop_Ammo(edict_t *ent, gitem_t *item);
void Drop_General(edict_t* ent, gitem_t* item);
void Drop_PowerArmor(edict_t* ent, gitem_t* item);

bool Pickup_Rune(edict_t* ent, edict_t* other);
bool Pickup_Powerup(edict_t* ent, edict_t* other);
bool Pickup_Adrenaline(edict_t* ent, edict_t* other);
bool Pickup_Ammo(edict_t *ent, edict_t *other);
bool Pickup_Armor(edict_t* ent, edict_t* other);
bool Pickup_Bandolier(edict_t* ent, edict_t* other);
bool Pickup_Health(edict_t* ent, edict_t* other);
bool Pickup_Key(edict_t* ent, edict_t* other);
bool Pickup_Pack(edict_t* ent, edict_t* other);
bool Pickup_PowerArmor(edict_t* ent, edict_t* other);

void Use_Breather(edict_t* ent, gitem_t* item);
void Use_Envirosuit(edict_t* ent, gitem_t* item);
void Use_Invulnerability(edict_t* ent, gitem_t* item);
void Use_IR(edict_t *ent, gitem_t *item);
void Use_PowerArmor(edict_t* ent, gitem_t* item);
void Use_Rune(edict_t* ent, gitem_t* item);

extern gitem_armor_t jacketarmor_info;
extern gitem_armor_t combatarmor_info;
extern gitem_armor_t bodyarmor_info;


/**
 * List of sizes for items 
 */
gitem_size_t itemSizeList[] =
{
	{ nullptr }, // leave index 0 alone.
	// ======================================================
	// = RUNES
	// ======================================================
	{
		"rune_armor",
		{ -20.0f, -20.0f, -20.0f },
		{ 20.0f, 20.0f, 20.0f },
	},

	// ======================================================
	// = HEALTH ITEMS
	// ======================================================
	{
		"item_health_large",
		{ -7.5f, -10.f, 0.0f },
		{ 7.5f, 10.0f, 7.25f },
	},

	{
		"item_health",
		{ -7.5f, -10.f, 0.0f },
		{ 7.5f, 10.0f, 7.25f },
	},

	// ======================================================
	// = AMMO
	// ======================================================
	{
		"ammo_rockets",
		{ -10, -7.5, 0 },
		{ 10, 7.5, 15 },
	},


	{
		"ammo_grenades",
		{ -5.5f, -7, -4.0f },
		{ 5.5f, 3.5f, 4.0f },
	},

	{ nullptr } // sentinel.
};


/**
 * Item definitions
 */
gitem_t	itemlist[] =
{
	{ nullptr },	// leave index 0 alone

	// ======================================================
	// = RUNES
	// ======================================================
	{
		"rune_armor",
		Pickup_Rune,
		Use_Rune,
		Drop_General,
		nullptr,
		"runes/rune_pickup.wav",
		"models/runes/armor.md2",
		EF_ROTATE,
		nullptr,
		"rune_armor",		/* icon */
		"Rune of Armor",	/* pickup */	
		0,					/* width */		
		0,
		nullptr,
		IT_POWERUP,
		0,
		nullptr,
		0,
		/* precache */ "runes/rune_activate.wav"
	},


	// ======================================================
	// = ARMOR
	// ======================================================
	{
		"item_mask",
		Pickup_Powerup,
		Use_IR,
		Drop_General,
		nullptr,
		"items/pkup.wav",
		"models/items/predator/tris.md2", EF_ROTATE,
		nullptr,
		/* icon */		"p_mask",
		/* pickup */	"Rredator Mask",
		/* width */		2,
		60,
		nullptr,
		IT_POWERUP,
		0,
		nullptr,
		0,
		/* precache */ "misc/ir_start.wav"
	},

	{
		"item_armor_body",
		Pickup_Armor,
		nullptr,
		nullptr,
		nullptr,
		"misc/ar1_pkup.wav",
		"models/items/armor/body/tris.md2", EF_ROTATE,
		nullptr,
		/* icon */		"i_bodyarmor",
		/* pickup */	"Body Armor",
		/* width */		3,
		0,
		nullptr,
		IT_ARMOR,
		0,
		&bodyarmor_info,
		ARMOR_BODY,
		/* precache */ ""
	},

	{
		"item_armor_combat",
		Pickup_Armor,
		nullptr,
		nullptr,
		nullptr,
		"misc/ar1_pkup.wav",
		"models/items/armor/combat/tris.md2", EF_ROTATE,
		nullptr,
		/* icon */		"i_combatarmor",
		/* pickup */	"Combat Armor",
		/* width */		3,
		0,
		nullptr,
		IT_ARMOR,
		0,
		&combatarmor_info,
		ARMOR_COMBAT,
		/* precache */ ""
	},

	{
		"item_armor_jacket",
		Pickup_Armor,
		nullptr,
		nullptr,
		nullptr,
		"misc/ar1_pkup.wav",
		"models/items/armor/jacket/tris.md2", EF_ROTATE,
		nullptr,
		/* icon */		"i_jacketarmor",
		/* pickup */	"Jacket Armor",
		/* width */		3,
		0,
		nullptr,
		IT_ARMOR,
		0,
		&jacketarmor_info,
		ARMOR_JACKET,
		/* precache */ ""
	},

	{
		"item_armor_shard",
		Pickup_Armor,
		nullptr,
		nullptr,
		nullptr,
		"misc/ar2_pkup.wav",
		"models/armor/shard.md2", EF_ROTATE,
		nullptr,
		/* icon */		"i_shard",
		/* pickup */	"Armor Shard",
		/* width */		3,
		0,
		nullptr,
		IT_ARMOR,
		0,
		nullptr,
		ARMOR_SHARD,
		/* precache */ ""
	},

	{
		"item_power_screen",
		Pickup_PowerArmor,
		Use_PowerArmor,
		Drop_PowerArmor,
		nullptr,
		"misc/ar3_pkup.wav",
		"models/items/armor/screen/tris.md2", EF_ROTATE,
		nullptr,
		/* icon */		"i_powerscreen",
		/* pickup */	"Power Screen",
		/* width */		0,
		60,
		nullptr,
		IT_ARMOR,
		0,
		nullptr,
		0,
		/* precache */ ""
	},

	{
		"item_power_shield",
		Pickup_PowerArmor,
		Use_PowerArmor,
		Drop_PowerArmor,
		nullptr,
		"misc/ar3_pkup.wav",
		"models/items/armor/shield/tris.md2", EF_ROTATE,
		nullptr,
		/* icon */		"i_powershield",
		/* pickup */	"Power Shield",
		/* width */		0,
		60,
		nullptr,
		IT_ARMOR,
		0,
		nullptr,
		0,
		/* precache */ "misc/power2.wav misc/power1.wav"
	},


	// ======================================================
	// = WEAPONS 
	// ======================================================
	{
		"weapon_blaster",
		nullptr,
		Use_Weapon,
		nullptr,
		Weapon_Blaster,
		"misc/w_pkup.wav",
		"models/weapons/g_blast/tris.md2", EF_ROTATE,
		"models/weapons/v_blast/tris.md2",
		/* icon */		"w_blaster",
		/* pickup */	"Blaster",
		0,
		0,
		nullptr,
		IT_WEAPON | IT_STAY_COOP,
		WEAP_BLASTER,
		nullptr,
		0,
		/* precache */ "weapons/blastf1a.wav misc/lasfly.wav"
	},

	{
		"weapon_shotgun",
		Pickup_Weapon,
		Use_Weapon,
		Drop_Weapon,
		Weapon_Shotgun,
		"misc/w_pkup.wav",
		"models/shotgun/shotgun.md2", EF_ROTATE,
		"models/weapons/v_shotg/tris.md2",
		/* icon */		"w_shotgun",
		/* pickup */	"Shotgun",
		0,
		1,
		"Shells",
		IT_WEAPON | IT_STAY_COOP,
		WEAP_SHOTGUN,
		nullptr,
		0,
		/* precache */ "weapons/shotgun_1_fire.wav weapons/shotgun_1_reload.wav"
	},

	{
		"weapon_supershotgun",
		Pickup_Weapon,
		Use_Weapon,
		Drop_Weapon,
		Weapon_SuperShotgun,
		"misc/w_pkup.wav",
		"models/weapons/g_shotg2/tris.md2", EF_ROTATE,
		"models/weapons/v_shotg2/tris.md2",
		/* icon */		"w_sshotgun",
		/* pickup */	"Super Shotgun",
		0,
		2,
		"Shells",
		IT_WEAPON | IT_STAY_COOP,
		WEAP_SUPERSHOTGUN,
		nullptr,
		0,
		/* precache */ "weapons/sshotf1b.wav"
	},

	{
		"weapon_machinegun",
		Pickup_Weapon,
		Use_Weapon,
		Drop_Weapon,
		Weapon_Machinegun,
		"misc/w_pkup.wav",
		"models/weapons/g_machn/tris.md2", EF_ROTATE,
		"models/weapons/v_machn/tris.md2",
		/* icon */		"w_machinegun",
		/* pickup */	"Machinegun",
		0,
		1,
		"Bullets",
		IT_WEAPON | IT_STAY_COOP,
		WEAP_MACHINEGUN,
		nullptr,
		0,
		/* precache */ "weapons/gunshot1.wav weapons/gunshot2.wav weapons/gunshot3.wav weapons/gunshot4.wav"
	},

	{
		"weapon_chaingun",
		Pickup_Weapon,
		Use_Weapon,
		Drop_Weapon,
		Weapon_Chaingun,
		"misc/w_pkup.wav",
		"models/weapons/g_chain/tris.md2", EF_ROTATE,
		"models/weapons/v_chain/tris.md2",
		/* icon */		"w_chaingun",
		/* pickup */	"Chaingun",
		0,
		1,
		"Bullets",
		IT_WEAPON | IT_STAY_COOP,
		WEAP_CHAINGUN,
		nullptr,
		0,
		/* precache */ "weapons/chngnu1a.wav weapons/chngnl1a.wav weapons/machgf3b.wav` weapons/chngnd1a.wav"
	},

	{
		"weapon_grenadelauncher",
		Pickup_Weapon,
		Use_Weapon,
		Drop_Weapon,
		Weapon_GrenadeLauncher,
		"misc/w_pkup.wav",
		"models/weapons/g_launch/tris.md2", EF_ROTATE,
		"models/weapons/v_launch/tris.md2",
		/* icon */		"w_glauncher",
		/* pickup */	"Grenade Launcher",
		0,
		1,
		"Grenades",
		IT_WEAPON | IT_STAY_COOP,
		WEAP_GRENADELAUNCHER,
		nullptr,
		0,
		/* precache */ "models/objects/grenade/tris.md2 weapons/grenlf1a.wav weapons/grenlr1b.wav weapons/grenlb1b.wav"
	},

	{
		"weapon_rocketlauncher",
		Pickup_Weapon,
		Use_Weapon,
		Drop_Weapon,
		Weapon_RocketLauncher,
		"misc/w_pkup.wav",
		"models/rocket_launcher/launcher.md2", EF_ROTATE,
		"models/weapons/v_rocket/tris.md2",
		/* icon */		"w_rlauncher",
		/* pickup */	"Rocket Launcher",
		0,
		1,
		"Rockets",
		IT_WEAPON | IT_STAY_COOP,
		WEAP_ROCKETLAUNCHER,
		nullptr,
		0,
		/* precache */ "models/rocket_launcher/launcher.md2 weapons/rockfly.wav weapons/rocklf1a.wav weapons/rocklr1b.wav models/objects/debris2/tris.md2"
	},

	{
		"weapon_hyperblaster",
		Pickup_Weapon,
		Use_Weapon,
		Drop_Weapon,
		Weapon_HyperBlaster,
		"misc/w_pkup.wav",
		"models/weapons/g_hyperb/tris.md2", EF_ROTATE,
		"models/weapons/v_hyperb/tris.md2",
		/* icon */		"w_hyperblaster",
		/* pickup */	"HyperBlaster",
		0,
		1,
		"Cells",
		IT_WEAPON | IT_STAY_COOP,
		WEAP_HYPERBLASTER,
		nullptr,
		0,
		/* precache */ "weapons/hyprbu1a.wav weapons/hyprbl1a.wav weapons/hyprbf1a.wav weapons/hyprbd1a.wav misc/lasfly.wav"
	},

	{
		"weapon_railgun",
		Pickup_Weapon,
		Use_Weapon,
		Drop_Weapon,
		Weapon_Railgun,
		"misc/w_pkup.wav",
		"models/railgun/railgun.md2", EF_ROTATE,
		"models/railgun/railgun_v.md2",
		/* icon */		"w_railgun",
		/* pickup */	"Railgun",
		0,
		1,
		"Slugs",
		IT_WEAPON | IT_STAY_COOP,
		WEAP_RAILGUN,
		nullptr,
		0,
		/* precache */ "weapons/rg_hum.wav"
	},

	{
		"weapon_bfg",
		Pickup_Weapon,
		Use_Weapon,
		Drop_Weapon,
		Weapon_BFG,
		"misc/w_pkup.wav",
		"models/weapons/g_bfg/tris.md2", EF_ROTATE,
		"models/weapons/v_bfg/tris.md2",
		/* icon */		"w_bfg",
		/* pickup */	"BFG10K",
		0,
		50,
		"Cells",
		IT_WEAPON | IT_STAY_COOP,
		WEAP_BFG,
		nullptr,
		0,
		/* precache */ "sprites/s_bfg1.sp2 sprites/s_bfg2.sp2 sprites/s_bfg3.sp2 weapons/bfg__f1y.wav weapons/bfg__l1a.wav weapons/bfg__x1b.wav weapons/bfg_hum.wav"
	},

	// ======================================================
	// = AMMO ITEMS
	// ======================================================
	{
		"ammo_shells",
		Pickup_Ammo,
		nullptr,
		Drop_Ammo,
		nullptr,
		"misc/am_pkup.wav",
		"models/items/ammo/shells/medium/tris.md2", 0,
		nullptr,
		/* icon */		"a_shells",
		/* pickup */	"Shells",
		/* width */		3,
		10,
		nullptr,
		IT_AMMO,
		0,
		nullptr,
		AMMO_SHELLS,
		/* precache */ ""
	},

	{
		"ammo_bullets",
		Pickup_Ammo,
		nullptr,
		Drop_Ammo,
		nullptr,
		"misc/am_pkup.wav",
		"models/items/ammo/bullets/medium/tris.md2", 0,
		nullptr,
		/* icon */		"a_bullets",
		/* pickup */	"Bullets",
		/* width */		3,
		50,
		nullptr,
		IT_AMMO,
		0,
		nullptr,
		AMMO_BULLETS,
		/* precache */ ""
	},

	{
		"ammo_cells",
		Pickup_Ammo,
		nullptr,
		Drop_Ammo,
		nullptr,
		"misc/am_pkup.wav",
		"models/items/ammo/cells/medium/tris.md2", 0,
		nullptr,
		/* icon */		"a_cells",
		/* pickup */	"Cells",
		/* width */		3,
		50,
		nullptr,
		IT_AMMO,
		0,
		nullptr,
		AMMO_CELLS,
		/* precache */ ""
	},

	{
		"ammo_grenades",
		Pickup_Ammo,
		Use_Weapon,
		Drop_Ammo,
		Weapon_Grenade,
		"misc/am_pkup.wav",
		"models/ammo/grenade.md2", 0,
		"models/weapons/v_handgr/tris.md2",
		/* icon */		"a_grenades",
		/* pickup */	"Grenades",
		/* width */		3,
		5,
		"grenades",
		IT_AMMO | IT_WEAPON,
		WEAP_GRENADES,
		nullptr,
		AMMO_GRENADES,
		/* precache */ "weapons/hgrent1a.wav weapons/hgrena1b.wav weapons/hgrenc1b.wav weapons/hgrenb1a.wav weapons/hgrenb2a.wav "
	},

	{
		"ammo_rockets",
		Pickup_Ammo,
		nullptr,
		Drop_Ammo,
		nullptr,
		"misc/am_pkup.wav",
		"models/rocket_launcher/ammo.md2", 0,
		nullptr,
		/* icon */		"a_rockets",
		/* pickup */	"Rockets",
		/* width */		3,
		5,
		nullptr,
		IT_AMMO,
		0,
		nullptr,
		AMMO_ROCKETS,
		/* precache */ ""
	},

	{
		"ammo_slugs",
		Pickup_Ammo,
		nullptr,
		Drop_Ammo,
		nullptr,
		"misc/am_pkup.wav",
		"models/items/ammo/slugs/medium/tris.md2", 0,
		nullptr,
		/* icon */		"a_slugs",
		/* pickup */	"Slugs",
		/* width */		3,
		10,
		nullptr,
		IT_AMMO,
		0,
		nullptr,
		AMMO_SLUGS,
		/* precache */ ""
	},


	// ======================================================
	// = POWERUP ITEMS
	// ======================================================
	{
		"item_quad",
		Pickup_Powerup,
		Use_Quad,
		Drop_General,
		nullptr,
		"items/pkup.wav",
		"models/items/quaddama/tris.md2", EF_ROTATE | EF_QUAD,
		nullptr,
		/* icon */		"p_quad",
		/* pickup */	"Quad Damage",
		/* width */		2,
		60,
		nullptr,
		IT_POWERUP,
		0,
		nullptr,
		0,
		/* precache */ "items/damage.wav items/damage2.wav items/damage3.wav"
	},

	{
		"item_invulnerability",
		Pickup_Powerup,
		Use_Invulnerability,
		Drop_General,
		nullptr,
		"items/pkup.wav",
		"models/items/invulner/tris.md2", EF_ROTATE | EF_PENT,
		nullptr,
		/* icon */		"p_invulnerability",
		/* pickup */	"Invulnerability",
		/* width */		2,
		300,
		nullptr,
		IT_POWERUP,
		0,
		nullptr,
		0,
		/* precache */ "items/protect.wav items/protect2.wav items/protect4.wav"
	},

	{
		"item_breather",
		Pickup_Powerup,
		Use_Breather,
		Drop_General,
		nullptr,
		"items/pkup.wav",
		"models/items/breather/tris.md2", EF_ROTATE,
		nullptr,
		/* icon */		"p_rebreather",
		/* pickup */	"Rebreather",
		/* width */		2,
		60,
		nullptr,
		IT_STAY_COOP | IT_POWERUP,
		0,
		nullptr,
		0,
		/* precache */ "items/airout.wav"
	},

	{
		"item_enviro",
		Pickup_Powerup,
		Use_Envirosuit,
		Drop_General,
		nullptr,
		"items/pkup.wav",
		"models/items/enviro/tris.md2", EF_ROTATE,
		nullptr,
		/* icon */		"p_envirosuit",
		/* pickup */	"Environment Suit",
		/* width */		2,
		60,
		nullptr,
		IT_STAY_COOP | IT_POWERUP,
		0,
		nullptr,
		0,
		/* precache */ "items/airout.wav"
	},

	{
		"item_adrenaline",
		Pickup_Adrenaline,
		nullptr,
		nullptr,
		nullptr,
		"items/pkup.wav",
		"models/items/adrenal/tris.md2", EF_ROTATE,
		nullptr,
		/* icon */		"p_adrenaline",
		/* pickup */	"Adrenaline",
		/* width */		2,
		60,
		nullptr,
		0,
		0,
		nullptr,
		0,
		/* precache */ ""
	},

	{
		"item_bandolier",
		Pickup_Bandolier,
		nullptr,
		nullptr,
		nullptr,
		"items/pkup.wav",
		"models/items/band/tris.md2", EF_ROTATE,
		nullptr,
		/* icon */		"p_bandolier",
		/* pickup */	"Bandolier",
		/* width */		2,
		60,
		nullptr,
		0,
		0,
		nullptr,
		0,
		/* precache */ ""
	},

	{
		"item_pack",
		Pickup_Pack,
		nullptr,
		nullptr,
		nullptr,
		"items/pkup.wav",
		"models/items/pack/tris.md2", EF_ROTATE,
		nullptr,
		/* icon */		"i_pack",
		/* pickup */	"Ammo Pack",
		/* width */		2,
		180,
		nullptr,
		0,
		0,
		nullptr,
		0,
		/* precache */ ""
	},

	// ======================================================
	// = KEYS
	// ======================================================
	{
		"key_blue_key",
		Pickup_Key,
		nullptr,
		Drop_General,
		nullptr,
		"items/pkup.wav",
		"models/items/keys/key/tris.md2", EF_ROTATE,
		nullptr,
		"k_bluekey",
		"Blue Key",
		2,
		0,
		nullptr,
		IT_STAY_COOP | IT_KEY,
		0,
		nullptr,
		0,
		/* precache */ ""
	},

	{
		"key_red_key",
		Pickup_Key,
		nullptr,
		Drop_General,
		nullptr,
		"items/pkup.wav",
		"models/items/keys/red_key/tris.md2", EF_ROTATE,
		nullptr,
		"k_redkey",
		"Red Key",
		2,
		0,
		nullptr,
		IT_STAY_COOP | IT_KEY,
		0,
		nullptr,
		0,
		/* precache */ ""
	},

	// ======================================================
	// = HEALTH ITEMS
	// ======================================================
	{
		nullptr, // Why no class name?
		Pickup_Health,
		nullptr,
		nullptr,
		nullptr,
		"items/pkup.wav",
		nullptr, 0,
		nullptr,
		/* icon */		"i_health",
		/* pickup */	"Medium Health",
		/* width */		3,
		10,
		nullptr,
		0,
		0,
		nullptr,
		0,
		/* precache */ "items/n_health.wav"
	},

	{
		nullptr, // Why no class name?
		Pickup_Health,
		nullptr,
		nullptr,
		nullptr,
		"items/pkup.wav",
		nullptr, EF_ROTATE,
		nullptr,
		/* icon */		"i_health2",
		/* pickup */	"Stimpack",
		/* width */		3,
		2,
		nullptr,
		0,
		0,
		nullptr,
		0,
		/* precache */ "items/s_health.wav"
	},

	{
		nullptr, // Why no class name?
		Pickup_Health,
		nullptr,
		nullptr,
		nullptr,
		"items/pkup.wav",
		nullptr, 0,
		nullptr,
		/* icon */		"i_health3",
		/* pickup */	"Large Health",
		/* width */		3,
		25,
		nullptr,
		0,
		0,
		nullptr,
		0,
		/* precache */ "items/l_health.wav"
	},

	{
		nullptr, // Why no class name?
		Pickup_Health,
		nullptr,
		nullptr,
		nullptr,
		"items/pkup.wav",
		nullptr, 0,
		nullptr,
		/* icon */		"i_health4",
		/* pickup */	"Mega Health",
		/* width */		3,
		100,
		nullptr,
		0,
		0,
		nullptr,
		0,
		/* precache */ "items/m_health.wav"
	},

	// end of list marker
	{ nullptr }
};


/**
 * 
 */
void InitItems()
{
	game.num_items = sizeof(itemlist) / sizeof(itemlist[0]) - 1;
	game.num_item_sizes = sizeof(itemSizeList) / sizeof(itemSizeList[0]) - 1;
}
