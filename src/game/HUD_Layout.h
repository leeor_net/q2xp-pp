#pragma once

/**
 * Provides a default HUD layout string.
 * 
 * \todo	Integrate the below commenting to make this
 *			this much more sane.
 */

/*
// cursor positioning
xl <value>
xr <value>
yb <value>
yt <value>
xv <value>
yv <value>

// drawing
statpic <name>
pic <stat>
num <fieldwidth> <stat>
string <stat>

// control
if <stat>
ifeq <stat> <value>
ifbit <stat> <value>
endif
*/

extern char* single_statusbar;
extern char* dm_statusbar;

void LoadHUD();
