/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "server.h"

#include "../common/filesystem.h"
#include "../common/string_util.h"

#include "../win32/win_network.h"

#include <sstream>

bool RELIGHT_MAP = false;


/*
===============================================================================
OPERATOR CONSOLE ONLY COMMANDS

These commands can only be entered from stdin or by a remote operator datagram
===============================================================================
*/

/**
 * Specify a list of master servers
 */
void SV_SetMaster_f()
{
	// only dedicated servers send heartbeats
	if (!dedicated->value)
	{
		Com_Printf("Only dedicated servers use masters.\n");
		return;
	}

	// make sure the server is listed public
	Cvar_Set("public", "1");

	for (int i = 1; i < MAX_MASTERS; i++)
		memset(&master_adr[i], 0, sizeof(master_adr[i]));

	int slot = 1;	// slot 0 will always contain the id master
	for (int i = 1; i < Cmd_Argc(); i++)
	{
		if (slot == MAX_MASTERS)
			break;

		if (!NET_StringToAdr(Cmd_Argv(i), &master_adr[i]))
		{
			Com_Printf("Bad address: %s\n", Cmd_Argv(i));
			continue;
		}

		if (master_adr[slot].port == 0)
			master_adr[slot].port = BigShort(PORT_MASTER);

		Com_Printf("Master server at %s\n", NET_AdrToString(master_adr[slot]));
		Com_Printf("Sending a ping.\n");

		Netchan_OutOfBandPrint(NS_SERVER, master_adr[slot], "ping");

		slot++;
	}

	svs.last_heartbeat = -9999999;
}


/**
 * Sets sv_client and sv_player to the player with idnum Cmd_Argv(1)
 */
bool SV_SetPlayer()
{
	if (Cmd_Argc() < 2)
	{
		///\todo Display a usage message.
		return false;
	}

	// numeric values are just slot numbers
	char* s = Cmd_Argv(1);
	if (s[0] >= '0' && s[0] <= '9')
	{
		int idnum = atoi(Cmd_Argv(1));

		if (idnum < 0 || idnum >= maxclients->value)
		{
			Com_Printf("Bad client slot: %i\n", idnum);
			return false;
		}

		sv_client = &svs.clients[idnum];
		sv_player = sv_client->edict;
		if (!sv_client->state) {
			Com_Printf("Client %i is not active\n", idnum);
			return false;
		}
		return true;
	}

	// check for a name match
	client_t *cl = svs.clients;
	for (int i = 0; i < maxclients->value; i++, cl++)
	{
		if (!cl->state)
			continue;

		if (!strcmp(cl->name, s))
		{
			sv_client = cl;
			sv_player = sv_client->edict;
			return true;
		}
	}

	Com_Printf("Userid %s is not on the server\n", s);
	return false;
}


/*
===============================================================================
SAVEGAME FILES
===============================================================================
*/
/**
 * Delete save/<XXX>/
 */
void SV_WipeSavegame(char* savename)
{
/*
	char name[MAX_OSPATH] = { '\0' };

	Com_DPrintf("SV_WipeSaveGame(%s)\n", savename);

	Q_sprintf(name, MAX_OSPATH, "save/%s/server.ssv", savename);
	FS_Delete(name);
	Q_sprintf(name, MAX_OSPATH, "save/%s/game.ssv", savename);
	FS_Delete(name);

	Q_sprintf(name, MAX_OSPATH, "save/%s/*.sav", savename);

	/**
	 * \fixme	The following uses a set of functions that have since been removed.
	 *			The basics of it is that the functions find a series of files based
	 *			on pattern matching. There are better methods to do this using the
	 *			filesystem itself so these sections will need to be rewritten using
	 *			the rebuilt filesystem based on PhysicsFS.
	 */
	/*
	char* s = Sys_FindFirst(name, false);
	while (s)
	{
		remove(s);
		s = Sys_FindNext(false);
	}
	Sys_FindClose();

	Q_sprintf(name, MAX_OSPATH, "save/%s/*.sv2", savename);

	/*
	s = Sys_FindFirst(name, false);
	while (s)
	{
		remove(s);
		s = Sys_FindNext(false);
	}
	Sys_FindClose();
	*/
}


/**
 *
 */
void SV_CopySaveGame(char* src, char* dst)
{
/*
	char name[MAX_OSPATH] = { '\0' }, name2[MAX_OSPATH] = { '\0' };

	Com_DPrintf("SV_CopySaveGame(%s, %s)\n", src, dst);
	SV_WipeSavegame(dst);

	// copy the savegame over
	Q_sprintf(name, sizeof(name), "save/%s/server.ssv", src);
	Q_sprintf(name2, sizeof(name2), "save/%s/server.ssv", dst);
	FS_MakeDirectory(name2);
	CopyFile(name, name2);	///\fixme	This should be a filesystem function.

	Q_sprintf(name, sizeof(name), "save/%s/game.ssv", src);
	Q_sprintf(name2, sizeof(name2), "save/%s/game.ssv", dst);
	CopyFile(name, name2);	///\fixme	This should be a filesystem function.

	Q_sprintf(name, sizeof(name), "save/%s/", src);
	int len = strlen(name);
	Q_sprintf(name, sizeof(name), "save/%s/*.sav", src);

	/**
	* \fixme	The following uses a set of functions that have since been removed.
	*			The basics of it is that the functions find a series of files based
	*			on pattern matching. There are better methods to do this using the
	*			filesystem itself so these sections will need to be rewritten using
	*			the rebuilt filesystem based on PhysicsFS.
	*/
	/*
	char* found = Sys_FindFirst(name, true);
	while (found)
	{
		strcpy(name + len, found + len);

		Q_sprintf(name2, sizeof(name2), "%s/save/%s/%s", FS_GameDirectory(), dst, found + len);
		CopyFile(name, name2);

		// change sav to sv2
		int l = strlen(name);
		strcpy(name + l - 3, "sv2");
		l = strlen(name2);
		strcpy(name2 + l - 3, "sv2");
		CopyFile(name, name2);

		found = Sys_FindNext(false);
	}
	Sys_FindClose();
	*/
}


/**
 * 
 */
void SV_WriteLevelFile()
{
	char name[MAX_OSPATH] = { '\0' };

	Com_DPrintf("SV_WriteLevelFile()\n");
	Q_sprintf(name, sizeof(name), "save/current/%s.sv2", sv.name);
	
	FS_File* f = FS_Open(name, FS_OPEN_WRITE);
	if (!f)
	{
		Com_Printf("Failed to open %s\n", name);
		return;
	}
	
	FS_Write(f, sv.configstrings, sizeof(sv.configstrings), 1);
	CM_WritePortalState(f);
	FS_Close(f);

	Q_sprintf(name, sizeof(name), "save/current/%s.sav", sv.name);
	ge->WriteLevel(name);
}


/**
 * 
 */
void SV_ReadLevelFile()
{
	char name[MAX_OSPATH] = { '\0' };

	Com_DPrintf("SV_ReadLevelFile()\n");
	Q_sprintf(name, sizeof(name), "save/current/%s.sv2", sv.name);

	FS_File* f = FS_Open(name, FS_OPEN_READ);
	if (!f)
	{
		Com_Printf("Failed to open %s\n", name);
		return;
	}

	FS_Read(sv.configstrings, sizeof(sv.configstrings), 1, f);
	CM_ReadPortalState(f);
	FS_Close(f);

	Q_sprintf(name, sizeof(name), "save/current/%s.sav", sv.name);
	ge->ReadLevel(name);
}


const std::string SERVER_SAVE_FILE = "save/current/server.ssv";

/**
 * 
 */
void SV_WriteServerFile(bool autosave)
{
	ConsoleVariable *var;
	char name[MAX_OSPATH] = { '\0' };
	char string[MAX_OSPATH] = { '\0' };
	char comment[32] = { '\0' };
	time_t aclock;
	struct tm *newtime;

	Com_DPrintf("SV_WriteServerFile(%s)\n", autosave ? "true" : "false");
	Q_sprintf(name, sizeof(name), SERVER_SAVE_FILE.c_str());

	FS_File* f = FS_Open(name, FS_OPEN_WRITE);
	if (!f)
	{
		Com_Printf("Couldn't write %s\n", name);
		return;
	}

	if (!autosave)
	{
		time(&aclock);
		newtime = localtime(&aclock);
		Q_sprintf(comment, sizeof(comment), "%2i:%i%i %2i/%2i  ", newtime->tm_hour, newtime->tm_min / 10, newtime->tm_min % 10, newtime->tm_mon + 1, newtime->tm_mday);
		strncat(comment, sv.configstrings[CS_NAME], sizeof(comment) - 1 - strlen(comment));
	}
	else // autosaved
	{
		Q_sprintf(comment, sizeof(comment), "ENTERING %s", sv.configstrings[CS_NAME]);
	}

	FS_Write(f, comment, 1, sizeof(comment));
	FS_Write(f, svs.mapcmd, 1, sizeof(svs.mapcmd));

	// write all CVAR_LATCH cvars
	// these will be things like coop, skill, deathmatch, etc
	for (var = cvar_vars; var; var = var->next)
	{
		if (!(var->flags & CVAR_LATCH))
			continue;

		if (var->name.size() >= MAX_OSPATH - 1 || var->string.size() >= MAX_OSPATH - 1)
		{
			Com_Printf("Cvar too long: %s = %s\n", var->name.c_str(), var->string.c_str());
			continue;
		}

		Q_sprintf(name, sizeof(name), "%s", var->name.c_str());
		Q_sprintf(string, sizeof(string), "%s", var->string.c_str());
		FS_Write(f, name, 1, sizeof(name));
		FS_Write(f, string, 1, sizeof(string));
	}

	FS_Close(f);

	// write game state
	ge->WriteGame(SERVER_SAVE_FILE, autosave);
}


/**
 * 
 */
void SV_ReadServerFile()
{
	char name[MAX_OSPATH] = { '\0' }, string[128] = { '\0' };
	char comment[32] = { '\0' };
	char mapcmd[MAX_TOKEN_CHARS] = { '\0' };

	Com_DPrintf("SV_ReadServerFile()\n");
	Q_sprintf(name, sizeof(name), SERVER_SAVE_FILE.c_str());

	FS_File* f = FS_Open(name, FS_OPEN_READ);
	if (!f)
	{
		Com_Printf("Couldn't read %s\n", name);
		return;
	}

	FS_Read(comment, sizeof(comment), 1, f);
	FS_Read(mapcmd, sizeof(mapcmd), 1, f);

	// read all CVAR_LATCH cvars
	// these will be things like coop, skill, deathmatch, etc
	for (;;)
	{
		if (!FS_Read(name, 1, sizeof(name), f))
			break;

		FS_Read(string, sizeof(string), 1, f);
		Com_DPrintf("Set %s = %s\n", name, string);
		Cvar_ForceSet(name, string);
	}

	FS_Close(f);

	// start a new game fresh with new cvars
	SV_InitGame();

	Q_sprintf(svs.mapcmd, sizeof(svs.mapcmd), mapcmd);

	// read game state
	ge->ReadGame(SERVER_SAVE_FILE);
}


/**
 * Puts the server in demo mode on a specific map/cinematic
 */
void SV_DemoMap_f()
{
	SV_Map(true, Cmd_Argv(1), false);
}


/**
 * Saves the state of the map just being exited and goes to a new map.
 * 
 * If the initial character of the map string is '*', the next map is
 * in a new unit, so the current savegame directory is cleared of
 * map files.
 * 
 * Example:
 * 
 * *inter.cin+jail
 * 
 * Clears the archived maps, plays the inter.cin cinematic, then
 * goes to map jail.bsp.
 */
void SV_GameMap_f()
{
	if (Cmd_Argc() != 2)
	{
		Com_Printf("USAGE: gamemap <map>\n");
		return;
	}

	Com_DPrintf("SV_GameMap(%s)\n", Cmd_Argv(1));
	FS_MakeDirectory("save/current");

	// check for clearing the current savegame
	char* map = Cmd_Argv(1);
	if (map[0] == '*') // wipe all the *.sav files
	{
		SV_WipeSavegame("current");
	}
	else // save the map just exited
	{
		if (sv.state == ss_game)
		{
			// clear all the client inuse flags before saving so that
			// when the level is re-entered, the clients will spawn
			// at spawn points instead of occupying body shells
			bool* savedInuse = static_cast<bool*>(malloc(maxclients->value * sizeof(bool)));
			client_t* cl = svs.clients;
			for (int i = 0; i < maxclients->value; i++, cl++)
			{
				savedInuse[i] = cl->edict->inuse;
				cl->edict->inuse = false;
			}

			SV_WriteLevelFile();

			// we must restore these for clients to transfer over correctly
			cl = svs.clients;
			for (int i = 0; i < maxclients->value; i++, cl++)
				cl->edict->inuse = savedInuse[i];

			free(savedInuse);
		}
	}

	// start up the next map
	SV_Map(false, Cmd_Argv(1), false);

	// archive server state
	strncpy(svs.mapcmd, Cmd_Argv(1), sizeof(svs.mapcmd) - 1);

	// copy off the level to the autosave slot
	if (!dedicated->value)
	{
		SV_WriteServerFile(true);
		SV_CopySaveGame("current", "save0");
	}
}


/**
 * Goes directly to a given map without any savegame archiving.
 * 
 * For development work
 */
void SV_Map_f()
{
	std::stringstream map;
	map << "maps/" << Cmd_Argv(1) << ".bsp";
	
	if (!FS_FileExists(map.str()))
	{
		Com_Printf("Can't find %s\n", map.str().c_str());
		return;
	}

	sv.state = ss_dead; // don't save current level when changing
	SV_WipeSavegame("current");
	SV_GameMap_f();
}


void SV_ReLightMap_f()
{
	char* map;
	char expanded[MAX_QPATH] = { '\0' };

	RELIGHT_MAP = true;
	map = Cmd_Argv(1);
	if (!strstr(map, "."))
	{
		Q_sprintf(expanded, sizeof(expanded), "maps/%s.bsp", map);
		if (!FS_FileExists(expanded))
		{
			Com_Printf("Can't find %s\n", expanded);
			return;
		}
	}

	sv.state = ss_dead;			// don't save current level when changing
	SV_WipeSavegame("current");
	SV_GameMap_f();
}


/*
=====================================================================
SAVEGAMES
=====================================================================
*/
/**
 * 
 */
void SV_Loadgame_f()
{
	if (Cmd_Argc() != 2)
	{
		Com_Printf("USAGE: loadgame <directory>\n");
		return;
	}

	Com_Printf("Loading game...\n");

	char* dir = Cmd_Argv(1);
	if (strstr(dir, "..") || strstr(dir, "/") || strstr(dir, "\\"))
		Com_Printf("Bad savedir.\n");

	// make sure the server.ssv file exists
	char name[MAX_OSPATH] = { '\0' };
	Q_sprintf(name, sizeof(name), "save/%s/server.ssv", Cmd_Argv(1));

	if(!FS_FileExists(name))
	{
		Com_Printf("No such savegame: %s\n", name);
		return;
	}

	SV_CopySaveGame(Cmd_Argv(1), "current");
	SV_ReadServerFile();

	// go to the map
	sv.state = ss_dead; // don't save current level when changing
	SV_Map(false, svs.mapcmd, true);
}


/**
 * 
 */
void SV_Savegame_f()
{
	if (Cmd_Argc() != 2)
	{
		Com_Printf("USAGE: savegame <directory>\n");
		return;
	}

	if (sv.state != ss_game)
	{
		Com_Printf("You must be in a game to save.\n");
		return;
	}

	if (Cvar_VariableValue("deathmatch"))
	{
		Com_Printf("Can't save game in a deathmatch.\n");
		return;
	}


	if (!strcmp(Cmd_Argv(1), "current"))
	{
		Com_Printf("Can't save to 'current'\n");
		return;
	}

	if (maxclients->value == 1 && svs.clients[0].edict->client->ps.stats[STAT_HEALTH] <= 0)
	{
		Com_Printf("Can't save game while dead!\n");
		return;
	}

	char* dir = Cmd_Argv(1);
	if (strstr(dir, "..") || strstr(dir, "/") || strstr(dir, "\\"))
		Com_Printf("Bad savedir.\n");

	Com_Printf("Saving game...\n");

	// archive current level, including all client edicts.
	// when the level is reloaded, they will be shells awaiting
	// a connecting client
	SV_WriteLevelFile();

	// save server state
	SV_WriteServerFile(false);

	// copy it off
	SV_CopySaveGame("current", dir);

	Com_Printf("Done.\n");
}


/**
 * Kick a user off of the server
 */
void SV_Kick_f()
{
	if (Cmd_Argc() != 2)
	{
		Com_Printf("Usage: kick <userid>\n");
		return;
	}

	if (!svs.initialized)
	{
		Com_Printf("No server running.\n");
		return;
	}

	if (!SV_SetPlayer())
		return;

	SV_BroadcastPrintf(PRINT_HIGH, "%s was kicked\n", sv_client->name);

	// print directly, because the dropped client won't get the SV_BroadcastPrintf message
	SV_ClientPrintf(sv_client, PRINT_HIGH, "You were kicked from the game\n");
	SV_DropClient(sv_client);
	sv_client->lastmessage = svs.realtime;	// min case there is a funny zombie
}


/**
 * 
 */
void SV_Status_f()
{
	if (!svs.clients)
	{
		Com_Printf("No server running.\n");
		return;
	}

	Com_Printf("map              : %s\n", sv.name);
	Com_Printf("num score ping name            lastmsg address               qport \n");
	Com_Printf("--- ----- ---- --------------- ------- --------------------- ------\n");

	client_t *cl = svs.clients;
	for (int i = 0; i < maxclients->value; i++, cl++)
	{
		if (!cl->state)
			continue;

		Com_Printf("%3i ", i);
		Com_Printf("%5i ", cl->edict->client->ps.stats[STAT_FRAGS]);

		if (cl->state == cs_connected) Com_Printf("CNCT ");
		else if (cl->state == cs_zombie) Com_Printf("ZMBI ");
		else Com_Printf("%4i ", clamp(cl->ping, 0, 9999));

		Com_Printf("%s", cl->name);
		int l = 16 - strlen(cl->name);
		for (int j = 0; j < l; j++)
			Com_Printf(" ");

		Com_Printf("%7i ", svs.realtime - cl->lastmessage);

		char* s = NET_AdrToString(cl->netchan.remote_address);
		Com_Printf("%s", s);
		l = 22 - strlen(s);
		for (int j = 0; j < l; j++)
			Com_Printf(" ");

		Com_Printf("%5i", cl->netchan.qport);
		Com_Printf("\n");
	}
	Com_Printf("\n");
}


/**
 * 
 */
void SV_ConSay_f()
{
	if (Cmd_Argc() < 2)
		return;

	char text[1024] = { '\0' };
	strcpy(text, "console: ");
	char* p = Cmd_Args();

	if (*p == '"')
	{
		p++;
		p[strlen(p) - 1] = 0;
	}

	strcat(text, p);

	client_t *client = svs.clients;
	for (int j = 0; j < maxclients->value; j++, client++)
	{
		if (client->state != cs_spawned)
			continue;

		SV_ClientPrintf(client, PRINT_CHAT, "%s\n", text);
	}
}


/**
 * 
 */
void SV_Heartbeat_f()
{
	svs.last_heartbeat = -9999999;
}


/**
 * Examine or change the serverinfo string
 */
void SV_Serverinfo_f()
{
	Com_Printf("Server info settings:\n");
	Info_Print(Cvar_Serverinfo());
}


/**
 * Examine all a users info strings
 */
void SV_DumpUser_f()
{
	if (Cmd_Argc() != 2)
	{
		Com_Printf("Usage: info <userid>\n");
		return;
	}

	if (!SV_SetPlayer())
		return;

	Com_Printf("USER INFO:\n");
	Com_Printf(BAR_LITE "\n");
	Info_Print(sv_client->userinfo);
}


/**
 * Begins server demo recording.  Every entity and every message will be
 * recorded, but no playerinfo will be stored.  Primarily for demo merging.
 */
void SV_ServerRecord_f()
{
	if (Cmd_Argc() != 2)
	{
		Com_Printf("serverrecord <demoname>\n");
		return;
	}

	if (svs.demofile)
	{
		Com_Printf("Already recording.\n");
		return;
	}

	if (sv.state != ss_game)
	{
		Com_Printf("You must be in a level to record.\n");
		return;
	}

	char name[MAX_OSPATH] = { '\0' };
	char buf_data[32768] = { '\0' };
	sizebuf_t buf;

	if (!FS_FileExists("demos")) FS_MakeDirectory("demos");
	
	// open the demo file
	Q_sprintf(name, sizeof(name), "demos/%s.dm2", Cmd_Argv(1));
	Com_Printf("recording to %s.\n", name);
	
	svs.demofile = FS_Open(name, FS_OPEN_WRITE);
	if (!svs.demofile)
	{
		Com_Printf("SV_ServerRecord_f(): couldn't open \'%\' for writing.\n", name);
		return;
	}

	// setup a buffer to catch all multicasts
	SZ_Init(&svs.demo_multicast, svs.demo_multicast_buf, sizeof(svs.demo_multicast_buf));

	// write a single giant fake message with all the startup info
	SZ_Init(&buf, (byte*)buf_data, sizeof(buf_data));

	// serverdata needs to go over for all types of servers
	// to make sure the protocol is right, and to set the gamedir

	// send the serverdata
	MSG_WriteByte(&buf, svc_serverdata);
	MSG_WriteLong(&buf, PROTOCOL_VERSION);

	MSG_WriteLong(&buf, svs.spawncount);
	// 2 means server demo
	MSG_WriteByte(&buf, 2); // demos are always attract loops
	MSG_WriteString(&buf, (char*)Cvar_VariableString("gamedir").c_str()); /// \fixme Yuck
	MSG_WriteShort(&buf, -1);
	// send full levelname
	MSG_WriteString(&buf, sv.configstrings[CS_NAME]);

	for (int i = 0; i < MAX_CONFIGSTRINGS; i++)
	{
		if (sv.configstrings[i][0])
		{
			MSG_WriteByte(&buf, svc_configstring);
			MSG_WriteShort(&buf, i);
			MSG_WriteString(&buf, sv.configstrings[i]);
		}
	}

	// write it to the demo file
	Com_DPrintf("signon message length: %i\n", buf.cursize);
	int len = LittleLong(buf.cursize);
	FS_Write(svs.demofile, &len, 4, 1);
	FS_Write(svs.demofile, buf.data, buf.cursize, 1);

	// the rest of the demo file will be individual frames
}


/**
 * Ends server demo recording
 */
void SV_ServerStop_f()
{
	if (!svs.demofile)
	{
		Com_Printf("Not doing a serverrecord.\n");
		return;
	}
	
	FS_Close(svs.demofile);
	svs.demofile = nullptr;
	Com_Printf("Recording completed.\n");
}


/**
 * Kick everyone off, possibly in preparation for a new game
  */
void SV_KillServer_f()
{
	if (!svs.initialized)
		return;

	SV_Shutdown("Server was killed.\n", false);
	NET_Config(false);			// close network sockets
}


/**
 * Let the game dll handle a command
 */
void SV_ServerCommand_f()
{
	if (!ge)
	{
		Com_Printf("No game loaded.\n");
		return;
	}

	ge->ServerCommand();
}


/**
 *
 */
void SV_InitOperatorCommands()
{
	Cmd_AddCommand("heartbeat", SV_Heartbeat_f);
	Cmd_AddCommand("kick", SV_Kick_f);
	Cmd_AddCommand("status", SV_Status_f);
	Cmd_AddCommand("serverinfo", SV_Serverinfo_f);
	Cmd_AddCommand("dumpuser", SV_DumpUser_f);

	Cmd_AddCommand("map", SV_Map_f);
	Cmd_AddCommand("relightmap", SV_ReLightMap_f);
	Cmd_AddCommand("demomap", SV_DemoMap_f);
	Cmd_AddCommand("gamemap", SV_GameMap_f);
	Cmd_AddCommand("setmaster", SV_SetMaster_f);

	if (dedicated->value) Cmd_AddCommand("say", SV_ConSay_f);

	Cmd_AddCommand("serverrecord", SV_ServerRecord_f);
	Cmd_AddCommand("serverstop", SV_ServerStop_f);

	Cmd_AddCommand("save", SV_Savegame_f);
	Cmd_AddCommand("load", SV_Loadgame_f);

	Cmd_AddCommand("killserver", SV_KillServer_f);

	Cmd_AddCommand("sv", SV_ServerCommand_f);
}
