/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// cvar.c -- dynamic variable tracking

#include "common.h"

#include "cvar.h"
#include "filesystem.h"
#include "string_util.h"


ConsoleVariable *cvar_vars;

const std::string EMPTY_STRING = "";

static bool USERINFO_MODIFIED = false;


bool userInfoModified()
{
	return USERINFO_MODIFIED;
}

void userInfoModified(bool _b)
{
	USERINFO_MODIFIED = _b;
}


/**
 * 
 */
static bool Cvar_InfoValidate(const std::string& s)
{
	if (s == "\\") { return false; }
	if (s == "\"") { return false; }
	if (s == ";") { return false; }

	return true;
}


/**
 * 
 */
ConsoleVariable *Cvar_FindVar(const std::string& name)
{
	for (ConsoleVariable *var = cvar_vars; var; var = var->next)
	{
		if (name == var->name)
		{
			return var;
		}
	}

	return nullptr;
}


/**
 * 
 */
float Cvar_VariableValue(const std::string& name)
{
	ConsoleVariable *var = Cvar_FindVar(name);
	if (!var) { return 0.0f; }

	try
	{
		return std::stof(var->string);
	}
	catch (...)
	{
		return 0.0f;
	}
}


/**
 * 
 */
const std::string& Cvar_VariableString (const std::string& name)
{
	ConsoleVariable *var = Cvar_FindVar (name);
	if (!var) { return EMPTY_STRING; };

	return var->string;
}


/**
 * If the variable already exists, the value will not be set.
 * 
 * The flags will be or'ed in if the variable exists.
 */
ConsoleVariable* Cvar_Get(const std::string& name, const std::string& value, int flags)
{
	if (name.empty()) { return nullptr; }

	if (flags & (CVAR_USERINFO | CVAR_SERVERINFO))
	{
		if (!Cvar_InfoValidate(name))
		{
			Com_Printf("invalid info cvar name\n");
			return nullptr;
		}
	}

	ConsoleVariable* var = Cvar_FindVar(name);
	if (var)
	{
		var->flags |= flags;
		return var;
	}

	if (flags & (CVAR_USERINFO | CVAR_SERVERINFO))
	{
		if (!Cvar_InfoValidate(value))
		{
			Com_Printf("invalid info cvar value\n");
			return nullptr;
		}
	}

	var = new ConsoleVariable();
	var->name = name;
	var->string = value;
	var->modified = true;

	try { var->value = std::stof(var->string); }
	catch (std::invalid_argument&) { var->value = 0.0f; }

	// link the variable in
	var->next = cvar_vars;
	cvar_vars = var;

	var->flags = flags;
	var->help.clear();

	return var;
}

/**
 * 
 */
ConsoleVariable* Cvar_Set(const std::string& name, const std::string& value, bool force)
{
	if (name.empty()) { return nullptr; }

	ConsoleVariable *var = Cvar_FindVar(name);
	if (!var) { return Cvar_Get(name, value, 0); }

	if ((var->flags & CVAR_DEVELOPER))
	{
		Com_Printf("" S_COLOR_YELLOW "%s " S_COLOR_WHITE "is developer protected\n", name.c_str());
		return var;
	}

	if (var->flags & (CVAR_USERINFO | CVAR_SERVERINFO))
	{
		if (!Cvar_InfoValidate(value))
		{
			Com_Printf("invalid info cvar value\n");
			return var;
		}
	}

	if (!force)
	{
		if (var->flags & CVAR_NOSET)
		{
			Com_Printf("%s is write protected.\n", name.c_str());
			return var;
		}

		if (var->flags & CVAR_LATCH)
		{
			if (!var->latched_string.empty())
			{
				if (value == var->latched_string) { return var; }
				var->latched_string.clear();
			}
			else
			{
				if (value == var->string) { return var; }
			}

			if (Com_ServerState())
			{
				Com_Printf("%s will be changed for next game.\n", name.c_str());
				var->latched_string = value;
			}
			else
			{
				var->string = value;
				
				try { var->value = std::stof(var->string); }
				catch (...) { var->value = 0.0f; }

				if (var->name == "game") /// \fixme	Make this a named constant.
				{
					FS_SetGameDirectory(var->string);
				}
			}
			return var;
		}
	}
	else
	{
		if (!var->latched_string.empty())
		{
			var->latched_string.clear();
		}
	}

	if (value == var->string) { return var; } // not changed

	var->modified = true;

	if (var->flags & CVAR_USERINFO)
	{
		USERINFO_MODIFIED = true; // transmit at next oportunity
	}

	var->string.clear();

	var->string = value;

	try { var->value = std::stof(var->string); }
	catch (std::invalid_argument&) { var->value = 0.0f; }

	return var;
}


/**
 * 
 */
ConsoleVariable* Cvar_ForceSet(const std::string& name, const std::string& value)
{
	return Cvar_Set(name, value, true);
}


/**
 * Sets a variable with a given value.
 * 
 * \note	Creates the variable if it doesn't exist.
 */
ConsoleVariable* Cvar_Set(const std::string& name, const std::string& value)
{
	return Cvar_Set(name, value, false);
}


/**
 * 
 */
ConsoleVariable* Cvar_FullSet(const std::string& name, const std::string& value, int flags)
{
	ConsoleVariable* var = Cvar_FindVar(name);
	if (!var) { return Cvar_Get(name, value, flags); }

	var->modified = true;

	if (var->flags & CVAR_USERINFO) { USERINFO_MODIFIED = true; }

	var->string = value;
	try { var->value = std::stof(var->string); }
	catch (std::invalid_argument&) { var->value = 0.0f; }
	var->flags = flags;

	return var;
}


/**
 * 
 */
void Cvar_SetValue(const std::string& name, double value)
{
	Cvar_Set(name, std::to_string(value));
}


/**
 * 
 */
void Cvar_SetValue(const std::string& name, float value)
{
	Cvar_Set(name, std::to_string(value));
}


/**
 * 
 */
void Cvar_SetValue(const std::string& name, int value)
{
	Cvar_Set(name, std::to_string(value));
}


/**
 * 
 */
void Cvar_SetValue(const std::string& name, size_t value)
{
	Cvar_Set(name, std::to_string(value));
}


/**
 *
 */
void Cvar_ForceSetValue(const std::string& name, double value)
{
	Cvar_ForceSet(name, std::to_string(value));
}


/**
 *
 */
void Cvar_ForceSetValue(const std::string& name, float value)
{
	Cvar_ForceSet(name, std::to_string(value));
}


/**
 *
 */
void Cvar_ForceSetValue(const std::string& name, int value)
{
	Cvar_ForceSet(name, std::to_string(value));
}


/**
 *
 */
void Cvar_ForceSetValue(const std::string& name, size_t value)
{
	Cvar_ForceSet(name, std::to_string(value));
}


/**
 * Any variables with latched values will now be updated
 */
void Cvar_GetLatchedVars()
{
	for (ConsoleVariable* var = cvar_vars; var != nullptr; var = var->next)
	{
		if (var->latched_string.empty()) { continue; }

		var->string = var->latched_string;
		var->latched_string.clear();

		try { var->value = std::stof(var->string); }
		catch (...) { var->value = 0.0f; }

		if (var->name == "game") /// \fixme Make this a named constant.
		{
			FS_SetGameDirectory(var->string);
		}
	}
}


/**
 * Handles variable inspection and changing from the console
 */
bool Cvar_Command()
{
	ConsoleVariable* v = Cvar_FindVar(Cmd_Argv(0));
	if (!v) { return false; }

	// perform a variable print or set
	if (Cmd_Argc() == 1)
	{
		Com_Printf("\"%s\" is \"%s\"", v->name.c_str(), v->string.c_str());

		if (v->flags & CVAR_ARCHIVE) Com_Printf(S_COLOR_YELLOW " ARCHIVE");

		if (v->flags & CVAR_USERINFO) Com_Printf(" USERINFO");

		if (v->flags & CVAR_SERVERINFO) Com_Printf(S_COLOR_BLUE " SERVERINFO");

		if (v->flags & CVAR_NOSET) Com_Printf(S_COLOR_RED " NOSET");

		if (v->flags & CVAR_LATCH) Com_Printf(S_COLOR_CYAN " LATCH");

		Com_Printf("\n");
		Com_Printf("[" S_COLOR_YELLOW "%s" S_COLOR_WHITE "]\n", v->help.c_str());
		return true;
	}

	Cvar_Set(v->name, Cmd_Argv(1));
	return true;
}


/**
 * Allows setting and defining of arbitrary cvars from console
 */
void Cvar_Set_f()
{
	int flags;

	int c = Cmd_Argc();
	if (c != 3 && c != 4)
	{
		Com_Printf("usage: set <variable> <value> [u / s]\n");
		return;
	}

	if (c == 4)
	{
		if (!strcmp(Cmd_Argv(3), "u"))
		{
			flags = CVAR_USERINFO;
		}
		else if (!strcmp(Cmd_Argv(3), "s"))
		{
			flags = CVAR_SERVERINFO;
		}
		else
		{
			Com_Printf("flags can only be 'u' or 's'\n");
			return;
		}
		Cvar_FullSet(Cmd_Argv(1), Cmd_Argv(2), flags);
	}
	else
	{
		Cvar_Set(Cmd_Argv(1), Cmd_Argv(2));
	}
}


/**
 * cvar sorting from r1q2  <-- ??
 * 
 * \fixme	Find a better way to do this.
 */
int cvarsort(const void *_a, const void *_b)
{
	const ConsoleVariable* a = (const ConsoleVariable*)_a;
	const ConsoleVariable* b = (const ConsoleVariable*)_b;
	return strcmp(a->name.c_str(), b->name.c_str());
}


/**
 * Appends lines containing "set variable value" for all variables
 * with the archive flag set to true.
 */
void Cvar_WriteVariables(const std::string& path)
{
	ConsoleVariable *var, *sortedList, *cvar;
	char buffer[1024] = { '\0' };
	int	 i, num;

	for (var = cvar_vars, num = 0; var; var = var->next, num++)
	{ }

	sortedList = (ConsoleVariable *)calloc(num, sizeof(ConsoleVariable));

	if (sortedList)
	{
		for (var = cvar_vars, i = 0; var; var = var->next, i++)
		{
			sortedList[i] = *var;
		}

		qsort(sortedList, num, sizeof(sortedList[0]), cvarsort);
	}

	FS_File* f = FS_Open(path, FS_OPEN_APPEND);
	for (cvar = cvar_vars, i = 0; cvar; cvar = cvar->next, i++)
	{
		if (sortedList) { var = &sortedList[i]; }
		else { var = cvar; }

		if (var->flags & CVAR_ARCHIVE)
		{
			Q_sprintf(buffer, sizeof(buffer), "set %s \"%s\"\n", var->name.c_str(), var->string.c_str());
			FS_fprintf(f, "%s", buffer);
		}
	}
	FS_Close(f);

	if (sortedList)
	{
		free(sortedList);
	}
}


/**
 * 
 */
void Cvar_List_f()
{
	ConsoleVariable *var;
	int i, num;
	char *hlp;
	bool help = false;

	for (var = cvar_vars, num = 0; var; var = var->next, num++)
	{ }

	ConsoleVariable* sortedList = (ConsoleVariable *)calloc(num, sizeof(ConsoleVariable));

	if (sortedList)
	{
		for (var = cvar_vars, i = 0; var; var = var->next, i++)
			sortedList[i] = *var;

		qsort(sortedList, num, sizeof(sortedList[0]), cvarsort);
	}

	if (Cmd_Argc() == 2)
	{
		hlp = Cmd_Argv(1);
		if (!strcasecmp(hlp, "?") || !strcasecmp(hlp, "h") || !strcasecmp(hlp, "help"))
			help = true;
	}

	ConsoleVariable* cvar = cvar_vars;
	for (cvar, i = 0; cvar; cvar = cvar->next, i++)
	{
		if (sortedList) { var = &sortedList[i]; }
		else { var = cvar; }

		if (var->flags & CVAR_ARCHIVE)
			Com_Printf(S_COLOR_YELLOW "A");
		else
			Com_Printf(" ");

		if (var->flags & CVAR_USERINFO)
			Com_Printf("U");
		else
			Com_Printf(" ");
		
		if (var->flags & CVAR_SERVERINFO)
			Com_Printf(S_COLOR_BLUE "S");
		else
			Com_Printf(" ");

		if (var->flags & CVAR_DEVELOPER)
			Com_Printf(S_COLOR_RED "D");
		else
			Com_Printf(" ");

		if (var->flags & CVAR_NOSET)
			Com_Printf(S_COLOR_RED "-");
		else if (var->flags & CVAR_LATCH)
			Com_Printf(S_COLOR_CYAN "L");
		else
			Com_Printf(" ");
		Com_Printf(" %s \"%s\"", var->name, var->string.c_str());

		if (help || !var->help.empty())
			Com_Printf(" [" S_COLOR_YELLOW "%s" S_COLOR_WHITE "]", var->help.c_str());

		Com_Printf("\n");
	}

	Com_Printf("%i cvars\n", num);

	if (sortedList)
		free(sortedList);
}


/**
 * 
 */
char* Cvar_BitInfo(int bit)
{
	static char info[MAX_INFO_STRING];
	ConsoleVariable *var;

	info[0] = 0;

	for (var = cvar_vars; var; var = var->next)
	{
		if (var->flags & bit)
		{
			Info_SetValueForKey(info, (char*)var->name.c_str(), (char*)var->string.c_str());	/// \fixme	Yuck
		}
	}
	return info;
}


/**
 * returns an info string containing all the CVAR_USERINFO cvars
 */
char* Cvar_Userinfo()
{
	return Cvar_BitInfo(CVAR_USERINFO);
}


/**
 * returns an info string containing all the CVAR_SERVERINFO cvars
 */
char* Cvar_Serverinfo()
{
	return Cvar_BitInfo(CVAR_SERVERINFO);
}


/**
 * 
 */
void Cvar_Help_f()
{
	if (Cmd_Argc() != 2)
	{
		Com_Printf("Usage: cvarhelp <cvarName>\n");
		return;
	}

	ConsoleVariable* var = Cvar_FindVar(Cmd_Argv(1));
	if (!var)
	{
		Com_Printf("Cvar %s not found.\n", Cmd_Argv(1));
		return;
	}

	if (var->help.empty())
	{
		Com_Printf("No help available for %s.\n", Cmd_Argv(1));
		return;
	}

	char _help[4096] = { '\0' };
	Q_sprintf(_help, sizeof(_help), "%s: %s\n", var->name.c_str(), var->help.c_str());
	Com_Printf(_help, var->string.c_str());
}


/**
 * Reads in all archived cvars
 */
void Cvar_Init()
{
	Cmd_AddCommand("set", Cvar_Set_f);
	Cmd_AddCommand("cvarlist", Cvar_List_f);
	Cmd_AddCommand("cvarhelp", Cvar_Help_f);
}
