#pragma once

#include "shared.h"

// ===============================================================================
// = DEFINE'S
// ===============================================================================
#define M_PI		3.14159265358979323846	// matches value in gcc v2 math.h
#define	nanmask		(255<<23)

#define	IS_NAN(x)	(((*(int *)&x)&nanmask)==nanmask)

#define min(a,b)	(((a) < (b)) ? (a) : (b)) 
#define max(a,b)	(((a) > (b)) ? (a) : (b)) 

#define DEG2RAD(v)	((v) * (M_PI / 180.0f))
#define RAD2DEG(v)	((v) * (180.0f / M_PI))

#define BOX_ON_PLANE_SIDE(emins, emaxs, p) (((p)->type < 3) ? (((p)->dist <= (emins)[(p)->type]) ? 1 : (((p)->dist >= (emaxs)[(p)->type]) ? 2 :	3))	: BoxOnPlaneSide ((emins), (emaxs), (p)))


// ===============================================================================
// = TYPEDEF'S
// ===============================================================================
typedef float vec_t;
typedef vec_t vec2_t[2];
typedef vec_t vec3_t[3];
typedef vec_t vec4_t[4];
typedef vec_t vec5_t[5];

typedef vec_t quat_t[4];

typedef vec_t dualquat_t[8];

typedef uint8_t byte_vec4_t[4];


typedef vec3_t mat3_3_t[3];		// column-major (axis)
typedef vec_t mat3_t[9];
typedef vec4_t mat4_t[4];		// row-major


typedef	int	fixed4_t;
typedef	int	fixed8_t;
typedef	int	fixed16_t;

extern vec3_t vec3_origin;


// ===============================================================================
// = QUATERNION FUNCTIONS
// ===============================================================================
void Quat_Identity(quat_t q);
void Quat_Copy(const quat_t q1, quat_t q2);
void Quat_Quat3(const vec3_t in, quat_t out);
bool Quat_Compare(const quat_t q1, const quat_t q2);
void Quat_Conjugate(const quat_t q1, quat_t q2);
vec_t Quat_DotProduct(const quat_t q1, const quat_t q2);
vec_t Quat_Normalize(quat_t q);
vec_t Quat_Inverse(const quat_t q1, quat_t q2);
void Quat_FromMatrix3(const mat3_t m, quat_t q);
void Quat_Multiply(const quat_t q1, const quat_t q2, quat_t out);
static void Quat_LLerp(const quat_t q1, const quat_t q2, vec_t t, quat_t out);
void Quat_Lerp(const quat_t q1, const quat_t q2, vec_t t, quat_t out);
void Quat_Vectors(const quat_t q, vec3_t f, vec3_t r, vec3_t u);
void Quat_ToMatrix3(const quat_t q, mat3_t m);
void Quat_TransformVector(const quat_t q, const vec3_t v, vec3_t out);
void Quat_ConcatTransforms(const quat_t q1, const vec3_t v1, const quat_t q2, const vec3_t v2, quat_t q, vec3_t v);


// ===============================================================================
// = VECTOR3 FUNCTIONS
// ===============================================================================
void VectorScaleAndAdd(vec3_t vec1, vec3_t vec2, vec3_t out, float scale);
void VectorSubtract(vec3_t veca, vec3_t vecb, vec3_t out);
void VectorSubtract(short* veca, short* vecb, short* out);
void VectorAdd(vec3_t vec1, vec3_t vec2, vec3_t out);
void VectorCopy(const vec3_t in, vec3_t out);
void VectorCopy(short* in, short* out);
void VectorClear(vec3_t vec);
void VectorNegate(const vec3_t in, vec3_t out);
void VectorSet(vec3_t vec, float x, float y, float z);
void VectorMin(vec3_t vec1, vec3_t vec2, vec3_t out);
void VectorMul(vec3_t vec1, vec3_t vec2, vec3_t out);
void VectorAverage(vec3_t vec1, vec3_t vec2, vec3_t out);
void VectorInverse(vec3_t vec);
void VectorScale(vec3_t in, vec_t scale, vec3_t out);

vec_t VectorNormalize(vec3_t vec);
void VectorNormalizeFast(vec3_t vec);
vec_t VectorNormalizeCopy(const vec3_t vec, vec3_t out);
vec_t VectorLength(vec3_t vec);

bool VectorsEqual(vec3_t vec1, vec3_t vec2);

vec_t DotProduct(const vec3_t vec1, const vec3_t vec2);

void ClearBounds(vec3_t mins, vec3_t maxs);
void AddPointToBounds(vec3_t v, vec3_t mins, vec3_t maxs);
void BoundsAdd(vec3_t mins, vec3_t maxs, const vec3_t mins2, const vec3_t maxs2);
void ConcatRotations(float in1[3][3], float in2[3][3], float out[3][3]);
void CrossProduct(const vec3_t vec1, const vec3_t vec2, vec3_t cross);

void MakeNormalVectors(vec3_t forward, vec3_t right, vec3_t up);

void AngleVectors(vec3_t angles, vec3_t forward, vec3_t right, vec3_t up);
int BoxOnPlaneSide(vec3_t emins, vec3_t emaxs, struct cplane_s *plane);
float AngleMod(float a);
float LerpAngle(float a1, float a2, float frac);

void ProjectPointOnPlane(vec3_t dst, const vec3_t p, const vec3_t normal);
void PerpendicularVector(vec3_t dst, const vec3_t src);
void RotatePointAroundVector(vec3_t dst, const vec3_t dir, const vec3_t point, float degrees);
