#pragma once

#include "common.h"

// ===============================================================================
// = DEFINES
// ===============================================================================
#define	PORT_ANY		-1
#define	MAX_MSGLEN		1400
#define	PACKET_HEADER	10		// two ints and a short
#define	OLD_AVG			0.99
#define	MAX_LATENT		32


// ===============================================================================
// = ENUMERATIONS
// ===============================================================================

/**
 * 
 */
typedef enum
{
	NA_LOOPBACK,
	NA_BROADCAST,
	NA_IP
} netadrtype_t;


/**
 * 
 */
typedef enum
{
	NS_CLIENT,
	NS_SERVER
} netsrc_t;


// ===============================================================================
// = STRUCT'S
// ===============================================================================

/**
 * 
 */
typedef struct
{
	netadrtype_t type;

	byte ip[4];

	unsigned short port;
} netadr_t;


/**
 * 
 */
typedef struct
{
	bool fatal_error;

	netsrc_t sock;

	int dropped;				// between last packet and previous

	int last_received;			// for timeouts
	int last_sent;				// for retransmits

	netadr_t remote_address;
	int qport;					// qport value to write when transmitting

								// sequencing variables
	int incoming_sequence;
	int incoming_acknowledged;
	int incoming_reliable_acknowledged;	// single bit

	int incoming_reliable_sequence;	// single bit, maintained local

	int outgoing_sequence;
	int reliable_sequence;		// single bit
	int last_reliable_sequence;	// sequence number of last send

								// reliable staging and holding areas
	sizebuf_t message;			// writing buffer to send to server
	byte message_buf[MAX_MSGLEN - 16];	// leave space for header

										// message is copied to this buffer when it is first transfered
	int reliable_length;
	byte reliable_buf[MAX_MSGLEN - 16];	// unacked reliable message
} netchan_t;


void Netchan_Init();
void Netchan_Setup(netsrc_t sock, netchan_t * chan, netadr_t adr, int qport);

void Netchan_Transmit(netchan_t * chan, int length, byte * data);
void Netchan_OutOfBand(int net_socket, netadr_t adr, int length, byte * data);
void Netchan_OutOfBandPrint(int net_socket, netadr_t adr, char* format, ...);

bool Netchan_CanReliable(netchan_t * chan);
bool Netchan_NeedReliable(netchan_t * chan);
bool Netchan_Process(netchan_t * chan, sizebuf_t * msg);

netadr_t* Net_From();
sizebuf_t* Net_Message();
byte* Net_Message_Buffer();
