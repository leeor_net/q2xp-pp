/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "shared.h"

#include "common.h"
#include "string_util.h"

#include "math.h"

#include <math.h>
#include <stdio.h>

#define Q_COLOR_ESCAPE '^'

vec3_t vec3_origin = { 0.0f };


int com_parseLine;


/**
 * 
 */
float clamp(float value, float min, float max)
{
	float _val = value;

	if (value < min)
		_val = min;
	else if (value > max)
		_val = max;

	return _val;
}


/**
 * 
 */
int clamp(int value, int min, int max)
{
	int _val = value;

	if (value < min)
		_val = min;
	else if (value > max)
		_val = max;

	return _val;
}


/**
 * 
 */
float random()
{
	return (rand() & 0x7fff) / ((float)0x7fff);
}


/**
 * 
 */
float crandom()
{
	return (2.0f * (random() - 0.5f));
}


/**
 * 
 */
bool isColorString(const char *p)
{
	return p && *(p) == Q_COLOR_ESCAPE && *((p)+1) && *((p)+1) != Q_COLOR_ESCAPE;
}


//====================================================================================

/**
 * 
 */
char* Com_SkipPath(char* pathname)
{
	if (!pathname)
		return nullptr;

	char* last = pathname;
	while (*pathname)
	{
		if (*pathname == '/')
			last = pathname + 1;
		pathname++;
	}
	return last;
}


/**
 * 
 */
void Com_StripExtension (char* in, char* out)
{
	while (*in && *in != '.')
		*out++ = *in++;

	*out = 0;
}


/**
 * 
 */
char* COM_FileExtension(char* in)
{
	static char exten[8];

	while (*in && *in != '.')
		in++;

	if (!*in)
		return "";

	in++;
	int i = 0;
	for (i; i < 7 && *in; i++, in++) ///\fixme Magic number
		exten[i] = *in;

	exten[i] = 0;
	return exten;
}


/**
 * Gets the base name of the file with extension and path stripped.
 */
void Com_FileBase(char* in, char* out)
{
	char* s = in + strlen(in) - 1;

	while (s != in && *s != '.')
		s--;

	char* s2 = s;
	for (s2; s2 != in && *s2 != '/'; s2--)
	{}

	if (s - s2 < 2)
		out[0] = 0;
	else
	{
		s--;
		strncpy(out, s2 + 1, s - s2);
		out[s - s2] = 0;
	}
}


/**
 * Returns the path up to, but not including the last /
 */
void Com_FilePath(char* in, char* out)
{
	char* s = in + strlen(in) - 1;

	while (s != in && *s != '/')
		s--;

	strncpy(out, in, s - in);
	out[s - in] = 0;
}


/**
 * If path doesn't have a .EXT, append extension.
 * 
 * \note Extension should include the '.'
 */
void Com_DefaultExtension(char* path, int maxSize, const char* newExtension)
{
	char* s = path + strlen(path);
	while (*s != '/' && *s != '\\' && s != path)
	{
		if (*s == '.')
			return; // It has an extension

		s--;
	}

	Q_strncatz(path, maxSize, newExtension);
}


// ===============================================================================
// = BYTE ORDER FUNCTIONS
// ===============================================================================
bool	bigendien;

// can't just use function pointers, or dll linkage can
// mess up when qcommon is included in multiple places
short (*_BigShort) (short l);
short (*_LittleShort) (short l);
int (*_BigLong) (int l);
int (*_LittleLong) (int l);
float (*_BigFloat) (float l);
float (*_LittleFloat) (float l);


short BigShort(short l)
{
	return _BigShort(l);
}


short LittleShort(short l)
{
	return _LittleShort(l);
}


int BigLong(int l)
{
	return _BigLong(l);
}


int LittleLong(int l)
{
	return _LittleLong(l);
}


float BigFloat(float l)
{
	return _BigFloat(l);
}


float LittleFloat(float l)
{
	return _LittleFloat(l);
}


short ShortSwap(short l)
{
	byte b1 = l & 255;
	byte b2 = (l >> 8) & 255;

	return (b1 << 8) + b2;
}


short ShortNoSwap(short l)
{
	return l;
}


int LongSwap(int l)
{
	byte b1 = l & 255;
	byte b2 = (l >> 8) & 255;
	byte b3 = (l >> 16) & 255;
	byte b4 = (l >> 24) & 255;

	return ((int)b1 << 24) + ((int)b2 << 16) + ((int)b3 << 8) + b4;
}


int	LongNoSwap(int l)
{
	return l;
}


float FloatSwap(float f)
{
	union
	{
		float	f;
		byte	b[4];
	} dat1, dat2;


	dat1.f = f;
	dat2.b[0] = dat1.b[3];
	dat2.b[1] = dat1.b[2];
	dat2.b[2] = dat1.b[1];
	dat2.b[3] = dat1.b[0];
	return dat2.f;
}


float FloatNoSwap(float f)
{
	return f;
}


/**
 * 
 */
void Swap_Init()
{
	byte swaptest[2] = { 1, 0 };

	// set the byte swapping variables in a portable manner	
	if (*(short *)swaptest == 1)
	{
		bigendien = false;
		_BigShort = ShortSwap;
		_LittleShort = ShortNoSwap;
		_BigLong = LongSwap;
		_LittleLong = LongNoSwap;
		_BigFloat = FloatSwap;
		_LittleFloat = FloatNoSwap;
	}
	else
	{
		bigendien = true;
		_BigShort = ShortNoSwap;
		_LittleShort = ShortSwap;
		_BigLong = LongNoSwap;
		_LittleLong = LongSwap;
		_BigFloat = FloatNoSwap;
		_LittleFloat = FloatSwap;
	}
}


/**
 * does a varargs printf into a temp buffer, so I don't need to have
 * varargs versions of all text functions.
 */
char* va(char* format, ...)
{
	va_list argptr;
	static char string[1024] = { '\0' };

	va_start(argptr, format);
	vsnprintf(string, sizeof(string), format, argptr);
	va_end(argptr);

	return string;
}


char com_token[MAX_TOKEN_CHARS];


/**
 * Parse a token out of a string.
 * Keep it for old mods.
 */
char* Com_Parse(char* *data_p)
{
	static char	token[MAX_TOKEN_CHARS] = { '\0' };
	static int parseLine;
	int c;

	char* data = *data_p;
	int	len = 0;
	token[0] = 0;

	if (!data)
	{
		*data_p = nullptr;
		return "";
	}

skipWhite:
	// skip whitespace
	while ((c = *data) <= ' ')
	{
		if (c == 0)
		{
			*data_p = nullptr;
			return "";
		}

		data++;
	}

	// skip // comments
	if (c == '/' && data[1] == '/')
	{
		while (*data && *data != '\n')
			data++;

		goto skipWhite;
	}

	// handle quoted strings specially
	if (c == '\"')
	{
		data++;

		for (;;)
		{
			c = *data++;

			if (c == '\"' || !c)
			{
				token[len] = 0;
				*data_p = data;
				return token;
			}

			if (len < MAX_TOKEN_CHARS)
			{
				token[len] = c;
				len++;
			}
		}
	}

	// parse a regular word
	do
	{
		if (len < MAX_TOKEN_CHARS)
		{
			token[len] = c;
			len++;
		}

		data++;

		c = *data;
	} while (c > 32);

	if (len == MAX_TOKEN_CHARS)
	{
		Com_Printf("Com_Parse: token length exceeds MAX_TOKEN_CHARS ( %i )\n", MAX_TOKEN_CHARS);
		len = 0;
	}

	token[len] = 0;
	*data_p = data;

	return token;
}


/**
 * 
 */
int	paged_total;

void Com_PageInMemory(byte *buffer, int size)
{
	for (int i = size - 1; i > 0; i -= 4096)
		paged_total += buffer[i];
}


// ===============================================================================
// = INFO STRINGS
// ===============================================================================
/**
 * Searches the string for the given key and returns the associated value, or an empty string.
 */
char* Info_ValueForKey(char* s, char* key)
{
	char pkey[512];
	static char value[2][512];	// use two buffers so compares work without stomping on each other
	static int valueindex = 0;

	valueindex ^= 1;
	if (*s == '\\')
		s++;

	for(;;)
	{
		char* o = pkey;
		while (*s != '\\')
		{
			if (!*s)
				return "";
			*o++ = *s++;
		}

		*o = 0;
		s++;

		o = value[valueindex];

		while (*s != '\\' && *s)
		{
			if (!*s)
				return "";
			*o++ = *s++;
		}
		*o = 0;

		if (!strcmp(key, pkey))
			return value[valueindex];

		if (!*s)
			return "";
		s++;
	}
}


/**
 * 
 */
void Info_RemoveKey(char* s, char* key)
{
	char pkey[512] = { '\0' };
	char value[512] = { '\0' };

	if (strstr(key, "\\"))
	{
		//		Com_Printf ("Can't use a key with a \\\n");
		return;
	}

	for (;;)
	{
		char* start = s;
		if (*s == '\\')
			s++;

		char* o = pkey;
		while (*s != '\\')
		{
			if (!*s)
				return;
			*o++ = *s++;
		}

		*o = 0;
		s++;

		o = value;
		while (*s != '\\' && *s)
		{
			if (!*s)
				return;
			*o++ = *s++;
		}

		*o = 0;

		if (!strcmp(key, pkey))
		{
			strcpy(start, s);	// remove this part
			return;
		}

		if (!*s) return;
	}
}


/**
 * Some characters are illegal in info strings because they
 * can mess up the server's parsing
 */
bool Info_Validate(char* s)
{
	if (strstr(s, "\"") || strstr(s, ";"))
		return false;

	return true;
}


/**
 * 
 */
void Info_SetValueForKey(char* s, char* key, char* value)
{
	char newi[MAX_INFO_STRING];
	int maxsize = MAX_INFO_STRING;

	if (strstr(key, "\\") || strstr(value, "\\"))
	{
		Com_Printf("Can't use keys or values with a \\\n");
		return;
	}

	if (strstr(key, ";"))
	{
		Com_Printf("Can't use keys or values with a semicolon\n");
		return;
	}

	if (strstr(key, "\"") || strstr(value, "\""))
	{
		Com_Printf("Can't use keys or values with a \"\n");
		return;
	}

	if (strlen(key) > MAX_INFO_KEY - 1 || strlen(value) > MAX_INFO_KEY - 1)
	{
		Com_Printf("Keys and values must be < 64 characters.\n");
		return;
	}

	Info_RemoveKey(s, key);
	if (!value || !strlen(value))
		return;

	Q_sprintf(newi, sizeof(newi), "\\%s\\%s", key, value);

	if (strlen(newi) + strlen(s) > maxsize)
	{
		Com_Printf("Info string length exceeded\n");
		return;
	}

	// only copy ascii values
	s += strlen(s);
	char* v = newi;
	while (*v)
	{
		int c = *v++;
		c &= 127;		// strip high bits
		if (c >= 32 && c < 127)
			*s++ = c;
	}

	*s = 0;
}


/**
 * 
 */
void Com_SkipRestOfLine(char* *data_p)
{
	int c = 0;
	char* data = *data_p;

	while ((c = *data++) != 0)
	{
		if (c == '\n')
		{
			com_parseLine++;
			break;
		}
	}

	*data_p = data;
}




/**
 * 
 */
char* Com_ParseExt(char* *data_p, bool allowNewLines)
{
	int c, len = 0;
	char* data;
	bool hasNewLines = false;

	data = *data_p;
	com_token[0] = 0;

	// Make sure incoming data is valid 
	if (!data)
	{
		*data_p = nullptr;
		return com_token;
	}

	for (;;)
	{
		data = Com_SkipWhiteSpace(data, &hasNewLines);
		if (!data)
		{
			*data_p = nullptr;
			return com_token;
		}

		if (hasNewLines && !allowNewLines)
		{
			*data_p = data;
			return com_token;
		}

		c = *data;

		if (c == '/' && data[1] == '/')
		{
			while (*data && *data != '\n')
				data++;
		}

		// Skip /* */ comments 
		else if (c == '/' && data[1] == '*')
		{
			data += 2;

			while (*data && (*data != '*' || data[1] != '/'))
			{
				if (*data == '\n')
					com_parseLine++;

				data++;
			}

			if (*data)
				data += 2;
		}
		else // An actual token
			break;
	}

	// Handle quoted strings specially 
	if (c == '\"')
	{
		data++;
		for (;;)
		{
			c = *data++;
			if (c == '\n')
				com_parseLine++;

			if (c == '\"' || !c)
			{
				*data_p = data;
				com_token[len] = 0;
				return com_token;
			}

			if (len < MAX_TOKEN_CHARS)
				com_token[len++] = c;
		}
	}

	// Parse a regular word 
	do
	{
		if (len < MAX_TOKEN_CHARS)
			com_token[len++] = c;

		data++;
		c = *data;
	} while (c > 32);

	if (len == MAX_TOKEN_CHARS)
		len = 0;

	com_token[len] = 0;

	*data_p = data;
	return com_token;
}


/**
 * 
 */
char* Com_SkipWhiteSpace(char* data_p, bool *hasNewLines)
{
	int c = 0;

	while ((c = *data_p) <= ' ')
	{
		if (!c)
			return nullptr;

		if (c == '\n')
		{
			com_parseLine++;
			*hasNewLines = true;
		}
		data_p++;
	}
	return data_p;
}




/**
 * If path doesn't have a / or \\, insert newPath (newPath should not
 * include the /)
 */
void Com_DefaultPath(char* path, int maxSize, const char* newPath)
{
	char oldPath[MAX_OSPATH] = { '\0' };

	char*s = path;
	while (*s)
	{
		if (*s == '/' || *s == '\\')
			return;		// It has a path

		s++;
	}

	Q_strncpyz(oldPath, path, sizeof(oldPath));
	Q_snprintfz(path, maxSize, "%s/%s", newPath, oldPath);
}
