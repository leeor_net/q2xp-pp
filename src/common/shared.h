/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
#pragma once

#ifdef _WIN32
// unknown pragmas are SUPPOSED to be ignored, but....
#pragma warning(disable : 4244)     // MIPS
#pragma warning(disable : 4136)     // X86
#pragma warning(disable : 4051)     // ALPHA

#pragma warning(disable : 4018)     // signed/unsigned mismatch
#pragma warning(disable : 4305)		// truncation from const double to float
#pragma warning(disable : 4996)		// vs 2005 security warnings
#pragma warning(disable : 4133)

/* x64 warning dont need
   on win32 platform*/

#pragma warning(disable : 4267) 
#pragma warning(disable : 4311)
#pragma warning(disable : 4312)
#endif

#include <assert.h>
#include <math.h>
#include <stdarg.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>


// ===============================================================================
// = DEFINES
// ===============================================================================
// angle indexes
#define	PITCH				0		// up / down
#define	YAW					1		// left / right
#define	ROLL				2		// fall over

#define	MAX_STRING_CHARS	2048	// was 1024	max length of a string passed to Cmd_TokenizeString
#define	MAX_STRING_TOKENS	1024	// was 80	max tokens resulting from Cmd_TokenizeString
#define	MAX_TOKEN_CHARS		2048	// was 128	max length of an individual token

#define	MAX_QPATH			64		// max length of a quake game pathname
#define	MAX_OSPATH			128		// max length of a filesystem pathname

// per-level limits
#define	MAX_CLIENTS			256		// absolute limit
#define	MAX_EDICTS			1024	// must change protocol to increase more
#define	MAX_LIGHTSTYLES		256
#define	MAX_MODELS			256		// these are sent over the net as bytes
#define	MAX_SOUNDS			256		// so they cannot be blindly increased
#define	MAX_IMAGES			256
#define	MAX_ITEMS			256
#define MAX_LIGHTFLARES     256

// game print flags
#define	PRINT_LOW			0		// pickup messages
#define	PRINT_MEDIUM		1		// death messages
#define	PRINT_HIGH			2		// critical messages
#define	PRINT_CHAT			3		// chat messages

#define	ERR_FATAL			0		// exit the entire game with a popup window
#define	ERR_DROP			1		// print to console and disconnect from game
#define	ERR_DISCONNECT		2		// don't kill server

#define	PRINT_ALL			0
#define PRINT_DEVELOPER		1		// only print when "developer 1"
#define PRINT_ALERT			2

/** Consistent way of printing bars in console messages. */
#define BAR_BOLD  "============================================="
#define BAR_LITE  "---------------------------------------------"
#define BAR_GRFX  "\35\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\36\37"

/**
 * button bits
 * 
 * \fixme	These appear to be input bits, find a better place for these.
 */
#define	BUTTON_ATTACK		1
#define	BUTTON_USE			2
#define BUTTON_FLASHLIGHT	64
#define	BUTTON_ANY			128			// any key whatsoever

/**
 * Document these.
 * 
 * \fixme	Find a sane place for these.
 */
#define SPLASH_UNKNOWN		0
#define SPLASH_SPARKS		1
#define SPLASH_BLUE_WATER	2
#define SPLASH_BROWN_WATER	3
#define SPLASH_SLIME		4
#define	SPLASH_LAVA			5
#define SPLASH_BLOOD		6


// ===============================================================================
// = TYPEDEF'S
// ===============================================================================
typedef unsigned char byte;			/**< Basic Byte type. */


// ===============================================================================
// = ENUMERATIONS
// ===============================================================================
/**
 * destination class for gi.multicast()
 */
typedef enum
{
	MULTICAST_ALL,
	MULTICAST_PHS,
	MULTICAST_PVS,
	MULTICAST_ALL_R,
	MULTICAST_PHS_R,
	MULTICAST_PVS_R
} multicast_t;

int clamp(int value, int min, int max);
float clamp(float value, float min, float max);

float random();
float crandom();


// ===============================================================================
// = DON'T KNOW WHAT TO CALL THESE YET
// ===============================================================================
void Com_DefaultPath(char* path, int maxSize, const char* newPath);
void Com_DefaultExtension(char* path, int maxSize, const char* newExtension);

char* Com_SkipPath(char* pathname);

void Com_StripExtension(char* in, char* out);
void Com_FileBase(char* in, char* out);
void Com_FilePath(char* in, char* out);

char* Com_SkipWhiteSpace(char* data_p, bool *hasNewLines);
void Com_SkipRestOfLine(char* *data_p);
char* Com_ParseExt(char* *data_p, bool allowNewLines);

char* Com_Parse(char* *data_p); // data is an in/out parm, returns a parsed out token
void Com_PageInMemory(byte *buffer, int size);


// ===============================================================================
// = BIT FUCKERY FUNCTIONS, DECIDE IF THESE ARE STILL NEEDED
// ===============================================================================
short BigShort(short l);
short LittleShort(short l);
int BigLong(int l);
int LittleLong(int l);
float BigFloat(float l);
float LittleFloat(float l);

void Swap_Init();

char* va(char* format, ...);


// ===============================================================================
// = INFO? FUNCTIONS
// ===============================================================================

// key / value info strings
#define	MAX_INFO_KEY		64
#define	MAX_INFO_VALUE		64
#define	MAX_INFO_STRING		512

char* Info_ValueForKey(char* s, char* key);
void Info_RemoveKey(char* s, char* key);
void Info_SetValueForKey(char* s, char* key, char* value);
bool Info_Validate(char* s);


// ===============================================================================
// = SYSTEM SPECIFIC (THESE CAN BE MOVED ELSEWHERE AND ABSTRACTED AS NEEDED)
// ===============================================================================
extern int	curtime; // time returned by last Sys_Milliseconds

int Sys_Milliseconds();

// large block stack allocation routines
void* Hunk_Begin(int maxsize, char* name);
void* Hunk_Alloc(int size);
void Hunk_Free(void* buf);
int Hunk_End();


// usercmd_t is sent to the server each client frame
typedef struct usercmd_s
{
	byte	msec;
	byte	buttons;
	short	angles[3];
	short	forwardmove, sidemove, upmove;
	byte	impulse;		// remove?
	byte	lightlevel;		// light level the player is standing on
} usercmd_t;


// sound channels
// channel 0 never willingly overrides
// other channels (1-7) allways override a playing sound on that channel
#define	CHAN_AUTO               0
#define	CHAN_WEAPON             1
#define	CHAN_VOICE              2
#define	CHAN_ITEM               3
#define	CHAN_BODY               4
// modifier flags
#define	CHAN_NO_PHS_ADD			8	// send to all clients, not just ones in PHS (ATTN 0 will also do this)
#define	CHAN_RELIABLE			16	// send by reliable message, not datagram

#define	ATTN_NONE               0.075	//full volume the entire level
#define	ATTN_NORM               1.0
#define	ATTN_IDLE               1.1
#define	ATTN_STATIC             1.05
#define	ATTN_BIG_GUN            0.17
#define ATTN_MEDIUM				0.4
#define ATTN_WEAPON_LIGHT		0.2
#define ATTN_WEAPON_HEAVY		0.15

// player_state->stats[] indexes
#define STAT_HEALTH_ICON		0
#define	STAT_HEALTH				1
#define	STAT_AMMO_ICON			2
#define	STAT_AMMO				3
#define	STAT_ARMOR_ICON			4
#define	STAT_ARMOR				5
#define	STAT_SELECTED_ICON		6
#define	STAT_PICKUP_ICON		7
#define	STAT_PICKUP_STRING		8
#define	STAT_TIMER_ICON			9
#define	STAT_TIMER				10
#define	STAT_HELPICON			11
#define	STAT_SELECTED_ITEM		12
#define	STAT_LAYOUTS			13
#define	STAT_FRAGS				14
#define	STAT_FLASHES			15		// cleared each frame, 1 = health, 2 = armor
#define STAT_CHASE				16
#define STAT_SPECTATOR			17
#define STAT_HEALTH_MODEL       18

#define STAT_AMMO_BULLETS		25
#define STAT_AMMO_SHELLS		26
#define STAT_AMMO_GRENADES		27
#define STAT_AMMO_ROCKETS		28
#define STAT_AMMO_CELLS			29
#define	STAT_AMMO_SLUGS			30

#define STAT_AMMO_CAPACITY		31

#define	MAX_STATS				32

// dmflags->value flags
#define	DF_NO_HEALTH		0x00000001	// 1
#define	DF_NO_ITEMS			0x00000002	// 2
#define	DF_WEAPONS_STAY		0x00000004	// 4
#define	DF_NO_FALLING		0x00000008	// 8
#define	DF_INSTANT_ITEMS	0x00000010	// 16
#define	DF_SAME_LEVEL		0x00000020	// 32
#define DF_SKINTEAMS		0x00000040	// 64
#define DF_MODELTEAMS		0x00000080	// 128
#define DF_NO_FRIENDLY_FIRE	0x00000100	// 256
#define	DF_SPAWN_FARTHEST	0x00000200	// 512
#define DF_FORCE_RESPAWN	0x00000400	// 1024
#define DF_NO_ARMOR			0x00000800	// 2048
#define DF_ALLOW_EXIT		0x00001000	// 4096
#define DF_INFINITE_AMMO	0x00002000	// 8192
#define DF_QUAD_DROP		0x00004000	// 16384
#define DF_FIXED_FOV		0x00008000	// 32768

#define	DF_QUADFIRE_DROP	0x00010000	// 65536

#define DF_NO_MINES			0x00020000
#define DF_NO_STACK_DOUBLE	0x00040000
#define DF_NO_NUKES			0x00080000
#define DF_NO_SPHERES		0x00100000

#define DF_FLASHLIGHT		0x00800000
#define DF_UT_DOUBLE_JUMP	0x01000000


// ==========================================================
// = ELEMENTS COMMUNICATED ACROSS THE NET
// ==========================================================
#define	ANGLE2SHORT(x)	((int)((x)*65536/360) & 65535)
#define	SHORT2ANGLE(x)	((x)*(360.0f/65536))


/**
 * Config strings are a general means of communication from
 * the server to all connected clients.
 * 
 * Each config string can be at most MAX_QPATH characters.
 */
enum ConfigString
{
	CS_NAME				= 0,								/**<  */
	CS_CDTRACK			= 1,								/**<  */
	CS_SKY				= 2,								/**<  */
	CS_SKYAXIS			= 3,								/**< %f %f %f format */
	CS_SKYROTATE		= 4,								/**<  */
	CS_STATUSBAR		= 5,								/**< display program string */

	CS_AIRACCEL			= 29,								/**< air acceleration control */
	CS_MAXCLIENTS		= 30,								/**<  */
	CS_MAPCHECKSUM		= 31,								/**< for catching cheater maps */

	CS_MODELS			= 32,								/**<  */
	CS_SOUNDS			= CS_MODELS + MAX_MODELS,			/**<  */
	CS_IMAGES			= CS_SOUNDS + MAX_SOUNDS,			/**<  */
	CS_LIGHTS			= CS_IMAGES + MAX_IMAGES,			/**<  */
	CS_ITEMS			= CS_LIGHTS + MAX_LIGHTSTYLES,		/**<  */
	CS_PLAYERSKINS		= CS_ITEMS + MAX_ITEMS,				/**<  */

	CS_LIGHTFLARES		= CS_PLAYERSKINS + MAX_CLIENTS,		/**<  */
	CS_GENERAL			= CS_LIGHTFLARES + MAX_LIGHTFLARES,	/**<  */

	MAX_CONFIGSTRINGS	= CS_GENERAL + (MAX_CLIENTS * 2)	/**<  */
};
