#pragma once


// player_state_t->refdef flags
#define	RDF_UNDERWATER		1		// warp the screen as apropriate
#define RDF_NOWORLDMODEL	2		// used for player configuration screen
#define	RDF_IRGOGGLES		4
#define RDF_UVGOGGLES		8

#define RDF_NOCLEAR			16
#define RDF_PAIN            32
#define RDF_WATER			64
#define RDF_LAVA			128
#define RDF_SLIME			256
#define RDF_MASK			512
