#include "colordef.h"

static color4ub_t ColorTable[10] =
{
	{ 0, 0, 0, 255 },			// Black
	{ 255, 0, 0, 255 },			// Red
	{ 0, 185, 0, 255 },			// Green
	{ 255, 255, 0, 255 },		// Yellow
	{ 0, 0, 255, 255 },			// Blue
	{ 0, 255, 255, 255 },		// Cyan
	{ 255, 0, 255, 255 },		// Magenta
	{ 255, 255, 255, 255 },		// White
	{ 80, 80, 80, 255 },		// Grey
	{ 255, 192, 64, 255 }		// Gold
};


/**
 * 
 */
color4ub_t& GetColorFromIndex(ColorIndex index)
{
	return ColorTable[clamp(index, COLOR_TABLE_BEGIN, COLOR_TABLE_END)];
}
