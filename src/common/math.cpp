#include "math.h"
#include "collision.h"

/**
 * The infamous 'inverse square root' function that everybody tries to attribute to Carmack.
 */
static float InverseSquareRoot(float number)
{
	long i;
	float x2, y;
	const float threehalfs = 1.5F;

	x2 = number * 0.5F;
	y = number;
	i = *(long *)&y;						// evil floating point bit level hacking
	i = 0x5f3759df - (i >> 1);				// what the fuck?
	y = *(float *)&i;
	y = y * (threehalfs - (x2 * y * y));	// 1st iteration

	return y;
}


/**
 * 
 */
void Quat_Identity(quat_t q)
{
	q[0] = 0;
	q[1] = 0;
	q[2] = 0;
	q[3] = 1;
}


/**
 * 
 */
void Quat_Copy(const quat_t q1, quat_t q2)
{
	q2[0] = q1[0];
	q2[1] = q1[1];
	q2[2] = q1[2];
	q2[3] = q1[3];
}


/**
 * 
 */
void Quat_Quat3(const vec3_t in, quat_t out)
{
	out[0] = in[0];
	out[1] = in[1];
	out[2] = in[2];
	out[3] = -sqrt(max(1 - in[0] * in[0] - in[1] * in[1] - in[2] * in[2], 0.0f));
}


/**
 * 
 */
bool Quat_Compare(const quat_t q1, const quat_t q2)
{
	if (q1[0] != q2[0] || q1[1] != q2[1] || q1[2] != q2[2] || q1[3] != q2[3])
		return false;

	return true;
}


/**
 * 
 */
void Quat_Conjugate(const quat_t q1, quat_t q2)
{
	q2[0] = -q1[0];
	q2[1] = -q1[1];
	q2[2] = -q1[2];
	q2[3] = q1[3];
}


/**
 * 
 */
vec_t Quat_DotProduct(const quat_t q1, const quat_t q2)
{
	return (q1[0] * q2[0] + q1[1] * q2[1] + q1[2] * q2[2] + q1[3] * q2[3]);
}


/**
 * 
 */
vec_t Quat_Normalize(quat_t q)
{
	vec_t length;

	length = q[0] * q[0] + q[1] * q[1] + q[2] * q[2] + q[3] * q[3];
	if (length != 0)
	{
		vec_t ilength = 1.0 / sqrt(length);
		q[0] *= ilength;
		q[1] *= ilength;
		q[2] *= ilength;
		q[3] *= ilength;
	}

	return length;
}


/**
 * 
 */
vec_t Quat_Inverse(const quat_t q1, quat_t q2)
{
	Quat_Conjugate(q1, q2);
	return Quat_Normalize(q2);
}


/**
 * 
 */
void Quat_FromMatrix3(const mat3_t m, quat_t q)
{
	vec_t tr, s;

	tr = m[0] + m[4] + m[8];
	if (tr > 0.00001)
	{
		s = sqrt(tr + 1.0);
		q[3] = s * 0.5; s = 0.5 / s;
		q[0] = (m[7] - m[5]) * s;
		q[1] = (m[2] - m[6]) * s;
		q[2] = (m[3] - m[1]) * s;
	}
	else
	{
		int i, j, k;

		i = 0;
		if (m[4] > m[i * 3 + i])
			i = 1;

		if (m[8] > m[i * 3 + i])
			i = 2;

		j = (i + 1) % 3;
		k = (i + 2) % 3;

		s = sqrt(m[i * 3 + i] - (m[j * 3 + j] + m[k * 3 + k]) + 1.0);

		q[i] = s * 0.5; if (s != 0.0)
			s = 0.5 / s;

		q[j] = (m[j * 3 + i] + m[i * 3 + j]) * s;
		q[k] = (m[k * 3 + i] + m[i * 3 + k]) * s;
		q[3] = (m[k * 3 + j] - m[j * 3 + k]) * s;
	}

	Quat_Normalize(q);
}


/**
 * 
 */
void Quat_Multiply(const quat_t q1, const quat_t q2, quat_t out)
{
	out[0] = q1[3] * q2[0] + q1[0] * q2[3] + q1[1] * q2[2] - q1[2] * q2[1];
	out[1] = q1[3] * q2[1] + q1[1] * q2[3] + q1[2] * q2[0] - q1[0] * q2[2];
	out[2] = q1[3] * q2[2] + q1[2] * q2[3] + q1[0] * q2[1] - q1[1] * q2[0];
	out[3] = q1[3] * q2[3] - q1[0] * q2[0] - q1[1] * q2[1] - q1[2] * q2[2];
}


/**
 * 
 */
static void Quat_LLerp(const quat_t q1, const quat_t q2, vec_t t, quat_t out)
{
	vec_t scale0, scale1;

	scale0 = 1.0 - t;
	scale1 = t;

	out[0] = scale0 * q1[0] + scale1 * q2[0];
	out[1] = scale0 * q1[1] + scale1 * q2[1];
	out[2] = scale0 * q1[2] + scale1 * q2[2];
	out[3] = scale0 * q1[3] + scale1 * q2[3];
}


/**
 * 
 */
void Quat_Lerp(const quat_t q1, const quat_t q2, vec_t t, quat_t out)
{
	quat_t p1;
	vec_t omega, cosom, sinom, scale0, scale1, sinsqr;

	if (Quat_Compare(q1, q2))
	{
		Quat_Copy(q1, out);
		return;
	}

	cosom = q1[0] * q2[0] + q1[1] * q2[1] + q1[2] * q2[2] + q1[3] * q2[3];
	if (cosom < 0.0)
	{
		cosom = -cosom;
		p1[0] = -q1[0]; p1[1] = -q1[1];
		p1[2] = -q1[2]; p1[3] = -q1[3];
	}
	else {
		p1[0] = q1[0]; p1[1] = q1[1];
		p1[2] = q1[2]; p1[3] = q1[3];
	}

	if (cosom >= 1.0 - 0.0001)
	{
		Quat_LLerp(q1, q2, t, out);
		return;
	}

	sinsqr = 1.0 - cosom * cosom;
	sinom = InverseSquareRoot(sinsqr);
	omega = atan2(sinsqr * sinom, cosom);
	scale0 = sin((1.0 - t) * omega) * sinom;
	scale1 = sin(t * omega) * sinom;

	out[0] = scale0 * p1[0] + scale1 * q2[0];
	out[1] = scale0 * p1[1] + scale1 * q2[1];
	out[2] = scale0 * p1[2] + scale1 * q2[2];
	out[3] = scale0 * p1[3] + scale1 * q2[3];
}


/**
 * 
 */
void Quat_Vectors(const quat_t q, vec3_t f, vec3_t r, vec3_t u)
{
	vec_t wx, wy, wz, xx, yy, yz, xy, xz, zz, x2, y2, z2;

	x2 = q[0] + q[0]; y2 = q[1] + q[1]; z2 = q[2] + q[2];

	xx = q[0] * x2; yy = q[1] * y2; zz = q[2] * z2;
	f[0] = 1.0f - yy - zz; r[1] = 1.0f - xx - zz; u[2] = 1.0f - xx - yy;

	yz = q[1] * z2; wx = q[3] * x2;
	r[2] = yz - wx; u[1] = yz + wx;

	xy = q[0] * y2; wz = q[3] * z2;
	f[1] = xy - wz; r[0] = xy + wz;

	xz = q[0] * z2; wy = q[3] * y2;
	f[2] = xz + wy; u[0] = xz - wy;
}


/**
 * 
 */
void Quat_ToMatrix3(const quat_t q, mat3_t m)
{
	Quat_Vectors(q, &m[0], &m[3], &m[6]);
}


/**
 * 
 */
void Quat_TransformVector(const quat_t q, const vec3_t v, vec3_t out)
{
	vec3_t t;

	CrossProduct(&q[0], v, t); // 6 muls, 3 subs
	VectorScale(t, 2, t);      // 3 muls
	CrossProduct(&q[0], t, out);// 6 muls, 3 subs
	VectorScaleAndAdd(out, t, out, q[3]);// 3 muls, 3 adds
}


/**
 * 
 */
void Quat_ConcatTransforms(const quat_t q1, const vec3_t v1, const quat_t q2, const vec3_t v2, quat_t q, vec3_t v)
{
	Quat_Multiply(q1, q2, q);
	Quat_TransformVector(q1, v2, v);
	v[0] += v1[0]; v[1] += v1[1]; v[2] += v1[2];
}


/**
 * 
 */
void RotatePointAroundVector(vec3_t dst, const vec3_t dir, const vec3_t point, float degrees)
{
	float	m[3][3];
	float	im[3][3];
	float	zrot[3][3];
	float	tmpmat[3][3];
	float	rot[3][3];
	vec3_t vr, vup, vf;

	vf[0] = dir[0];
	vf[1] = dir[1];
	vf[2] = dir[2];

	PerpendicularVector(vr, dir);
	CrossProduct(vr, vf, vup);

	m[0][0] = vr[0];
	m[1][0] = vr[1];
	m[2][0] = vr[2];

	m[0][1] = vup[0];
	m[1][1] = vup[1];
	m[2][1] = vup[2];

	m[0][2] = vf[0];
	m[1][2] = vf[1];
	m[2][2] = vf[2];

	memcpy(im, m, sizeof(im));

	im[0][1] = m[1][0];
	im[0][2] = m[2][0];
	im[1][0] = m[0][1];
	im[1][2] = m[2][1];
	im[2][0] = m[0][2];
	im[2][1] = m[1][2];

	memset(zrot, 0, sizeof(zrot));
	zrot[0][0] = zrot[1][1] = 1.0F;
	zrot[2][2] = 1.0;

	zrot[0][0] = cos(DEG2RAD(degrees));
	zrot[0][1] = sin(DEG2RAD(degrees));
	zrot[1][0] = -sin(DEG2RAD(degrees));
	zrot[1][1] = cos(DEG2RAD(degrees));

	ConcatRotations(m, zrot, tmpmat);
	ConcatRotations(tmpmat, im, rot);

	for (int i = 0; i < 3; i++)
		dst[i] = rot[i][0] * point[0] + rot[i][1] * point[1] + rot[i][2] * point[2];
}


/**
 * 
 */
void AngleVectors(vec3_t angles, vec3_t forward, vec3_t right, vec3_t up)
{
	float angle;
	float sr, sp, sy, cr, cp, cy;

	angle = angles[YAW] * (M_PI * 2 / 360);
	sy = sin(angle);
	cy = cos(angle);
	angle = angles[PITCH] * (M_PI * 2 / 360);
	sp = sin(angle);
	cp = cos(angle);
	angle = angles[ROLL] * (M_PI * 2 / 360);
	sr = sin(angle);
	cr = cos(angle);

	if (forward)
	{
		forward[0] = cp*cy;
		forward[1] = cp*sy;
		forward[2] = -sp;
	}
	if (right)
	{
		right[0] = (-1 * sr*sp*cy + -1 * cr*-sy);
		right[1] = (-1 * sr*sp*sy + -1 * cr*cy);
		right[2] = -1 * sr*cp;
	}
	if (up)
	{
		up[0] = (cr*sp*cy + -sr*-sy);
		up[1] = (cr*sp*sy + -sr*cy);
		up[2] = cr*cp;
	}
}


/**
 * 
 */
void ProjectPointOnPlane(vec3_t dst, const vec3_t p, const vec3_t normal)
{
	float d;
	vec3_t n;
	float inv_denom;

	inv_denom = 1.0F / DotProduct(normal, normal);

	d = DotProduct(normal, p) * inv_denom;

	n[0] = normal[0] * inv_denom;
	n[1] = normal[1] * inv_denom;
	n[2] = normal[2] * inv_denom;

	dst[0] = p[0] - d * n[0];
	dst[1] = p[1] - d * n[1];
	dst[2] = p[2] - d * n[2];
}


/**
 * assumes "src" is normalized
 */
void PerpendicularVector(vec3_t dst, const vec3_t src)
{
	int	pos;
	int i;
	float minelem = 1.0F;
	vec3_t tempvec;

	// find the smallest magnitude axially aligned vector
	for (pos = 0, i = 0; i < 3; i++)
	{
		if (fabs(src[i]) < minelem)
		{
			pos = i;
			minelem = fabs(src[i]);
		}
	}
	tempvec[0] = tempvec[1] = tempvec[2] = 0.0F;
	tempvec[pos] = 1.0F;

	ProjectPointOnPlane(dst, tempvec, src);
	VectorNormalize(dst);
}


/**
 * 
 */
void ConcatRotations(float in1[3][3], float in2[3][3], float out[3][3])
{
	out[0][0] = in1[0][0] * in2[0][0] + in1[0][1] * in2[1][0] + in1[0][2] * in2[2][0];
	out[0][1] = in1[0][0] * in2[0][1] + in1[0][1] * in2[1][1] + in1[0][2] * in2[2][1];
	out[0][2] = in1[0][0] * in2[0][2] + in1[0][1] * in2[1][2] + in1[0][2] * in2[2][2];
	out[1][0] = in1[1][0] * in2[0][0] + in1[1][1] * in2[1][0] + in1[1][2] * in2[2][0];
	out[1][1] = in1[1][0] * in2[0][1] + in1[1][1] * in2[1][1] + in1[1][2] * in2[2][1];
	out[1][2] = in1[1][0] * in2[0][2] + in1[1][1] * in2[1][2] + in1[1][2] * in2[2][2];
	out[2][0] = in1[2][0] * in2[0][0] + in1[2][1] * in2[1][0] + in1[2][2] * in2[2][0];
	out[2][1] = in1[2][0] * in2[0][1] + in1[2][1] * in2[1][1] + in1[2][2] * in2[2][1];
	out[2][2] = in1[2][0] * in2[0][2] + in1[2][1] * in2[1][2] + in1[2][2] * in2[2][2];
}





/**
 * 
 */
float LerpAngle(float a2, float a1, float frac)
{
	if (a1 - a2 > 180)
		a1 -= 360;

	if (a1 - a2 < -180)
		a1 += 360;

	return a2 + frac * (a1 - a2);
}


/**
 * 
 */
float AngleMod(float a)
{
	a = (360.0 / 65536) * ((int)(a*(65536 / 360.0)) & 65535);
	return a;
}

vec3_t	corners[2];

/**
 * this is the slow, general version
 */
int BoxOnPlaneSide2(vec3_t emins, vec3_t emaxs, struct cplane_s *p)
{
	int		i;
	float	dist1, dist2;
	int		sides;
	vec3_t	corners[2];

	for (i = 0; i < 3; i++)
	{
		if (p->normal[i] < 0)
		{
			corners[0][i] = emins[i];
			corners[1][i] = emaxs[i];
		}
		else
		{
			corners[1][i] = emins[i];
			corners[0][i] = emaxs[i];
		}
	}

	dist1 = DotProduct(p->normal, corners[0]) - p->dist;
	dist2 = DotProduct(p->normal, corners[1]) - p->dist;
	sides = 0;

	if (dist1 >= 0)
		sides = 1;

	if (dist2 < 0)
		sides |= 2;

	return sides;
}


/**
 * Returns 1, 2, or 1 + 2
 */
int BoxOnPlaneSide(vec3_t emins, vec3_t emaxs, struct cplane_s *p)
{
	float dist1, dist2;
	int sides;

	// fast axial cases
	if (p->type < 3)
	{
		if (p->dist <= emins[p->type])
			return 1;
		if (p->dist >= emaxs[p->type])
			return 2;

		return 3;
	}

	// general case
	switch (p->signbits)
	{
	case 0:
		dist1 = p->normal[0] * emaxs[0] + p->normal[1] * emaxs[1] + p->normal[2] * emaxs[2];
		dist2 = p->normal[0] * emins[0] + p->normal[1] * emins[1] + p->normal[2] * emins[2];
		break;
	case 1:
		dist1 = p->normal[0] * emins[0] + p->normal[1] * emaxs[1] + p->normal[2] * emaxs[2];
		dist2 = p->normal[0] * emaxs[0] + p->normal[1] * emins[1] + p->normal[2] * emins[2];
		break;
	case 2:
		dist1 = p->normal[0] * emaxs[0] + p->normal[1] * emins[1] + p->normal[2] * emaxs[2];
		dist2 = p->normal[0] * emins[0] + p->normal[1] * emaxs[1] + p->normal[2] * emins[2];
		break;
	case 3:
		dist1 = p->normal[0] * emins[0] + p->normal[1] * emins[1] + p->normal[2] * emaxs[2];
		dist2 = p->normal[0] * emaxs[0] + p->normal[1] * emaxs[1] + p->normal[2] * emins[2];
		break;
	case 4:
		dist1 = p->normal[0] * emaxs[0] + p->normal[1] * emaxs[1] + p->normal[2] * emins[2];
		dist2 = p->normal[0] * emins[0] + p->normal[1] * emins[1] + p->normal[2] * emaxs[2];
		break;
	case 5:
		dist1 = p->normal[0] * emins[0] + p->normal[1] * emaxs[1] + p->normal[2] * emins[2];
		dist2 = p->normal[0] * emaxs[0] + p->normal[1] * emins[1] + p->normal[2] * emaxs[2];
		break;
	case 6:
		dist1 = p->normal[0] * emaxs[0] + p->normal[1] * emins[1] + p->normal[2] * emins[2];
		dist2 = p->normal[0] * emins[0] + p->normal[1] * emaxs[1] + p->normal[2] * emaxs[2];
		break;
	case 7:
		dist1 = p->normal[0] * emins[0] + p->normal[1] * emins[1] + p->normal[2] * emins[2];
		dist2 = p->normal[0] * emaxs[0] + p->normal[1] * emaxs[1] + p->normal[2] * emaxs[2];
		break;
	default:
		dist1 = dist2 = 0; // shut up compiler
		assert(0);
		break;
	}

	sides = 0;
	if (dist1 >= p->dist)
		sides = 1;
	if (dist2 < p->dist)
		sides |= 2;

	assert(sides != 0);

	return sides;
}


/**
 * 
 */
void ClearBounds (vec3_t mins, vec3_t maxs)
{
	VectorSet(mins, 99999, 99999, 99999);
	VectorSet(maxs, -99999, -99999, -99999);
}


/**
 * 
 */
void AddPointToBounds(vec3_t v, vec3_t mins, vec3_t maxs)
{
	for (int i = 0; i < 3; i++)
	{
		vec_t val = v[i];
		if (val < mins[i])
			mins[i] = val;
		if (val > maxs[i])
			maxs[i] = val;
	}
}


/**
 * 
 */
void BoundsAdd(vec3_t mins, vec3_t maxs, const vec3_t mins2, const vec3_t maxs2)
{
	if (mins2[0] < mins[0])
		mins[0] = mins2[0];

	if (mins2[1] < mins[1])
		mins[1] = mins2[1];

	if (mins2[2] < mins[2])
		mins[2] = mins2[2];

	if (maxs2[0] > maxs[0])
		maxs[0] = maxs2[0];

	if (maxs2[1] > maxs[1])
		maxs[1] = maxs2[1];

	if (maxs2[2] > maxs[2])
		maxs[2] = maxs2[2];
}


/**
 * 
 */
bool VectorsEqual(vec3_t v1, vec3_t v2)
{
	if (v1[0] != v2[0] || v1[1] != v2[1] || v1[2] != v2[2])
		return false;

	return true;
}


/**
 * Normalizes a vector.
 */
vec_t VectorNormalize(vec3_t vec)
{
	float length = sqrt(vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2]);

	if (length)
	{
		float ilength = 1.0f / length;
		vec[0] *= ilength;
		vec[1] *= ilength;
		vec[2] *= ilength;
	}

	return length;
}


/**
 * Normalizes a vector and copies the output to a different vector.
 * 
 * \param vec	Vector to normalize.
 * \param out	Vector to copy output to.
 */
vec_t VectorNormalizeCopy(const vec3_t vec, vec3_t out)
{

	float length = sqrt(vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2]);

	if (length)
	{
		float ilength = 1.0f / length;
		out[0] = vec[0] * ilength;
		out[1] = vec[1] * ilength;
		out[2] = vec[2] * ilength;
	}

	return length;
}


/**
 * Scale vec2 and add to vec1.
 * 
 * \param	vec1	Vector to add scale result to.
 * \param	vec2	Vector to scale.
 * \param	out		Vector to save result to.
 * \param	scale	Value to scale vec2 by.
 */
void VectorScaleAndAdd(vec3_t vec1, vec3_t vec2, vec3_t out, float scale)
{
	out[0] = vec1[0] + (scale * vec2[0]);
	out[1] = vec1[1] + (scale * vec2[1]);
	out[2] = vec1[2] + (scale * vec2[2]);
}


/**
 * 
 */
vec_t DotProduct(const vec3_t v1, const vec3_t v2)
{
	return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
}


/**
 * 
 */
void VectorSubtract(vec3_t veca, vec3_t vecb, vec3_t out)
{
	out[0] = veca[0] - vecb[0];
	out[1] = veca[1] - vecb[1];
	out[2] = veca[2] - vecb[2];
}


/**
 * 
 */
void VectorSubtract(short* veca, short* vecb, short* out)
{
	out[0] = veca[0] - vecb[0];
	out[1] = veca[1] - vecb[1];
	out[2] = veca[2] - vecb[2];
}


/**
 * 
 */
void VectorAdd(vec3_t veca, vec3_t vecb, vec3_t out)
{
	out[0] = veca[0] + vecb[0];
	out[1] = veca[1] + vecb[1];
	out[2] = veca[2] + vecb[2];
}


/**
 * 
 */
void VectorCopy(const vec3_t in, vec3_t out)
{
	out[0] = in[0];
	out[1] = in[1];
	out[2] = in[2];
}


/**
 * 
 */
void VectorCopy(short* in, short* out)
{
	out[0] = in[0];
	out[1] = in[1];
	out[2] = in[2];
}


/**
 * 
 */
void VectorClear(vec3_t vec)
{
	vec[0] = vec[1] = vec[2] = 0.0f;
}


/**
 * 
 */
void VectorNegate(const vec3_t in, vec3_t out)
{
	out[0] = -in[0];
	out[1] = -in[1];
	out[2] = -in[2];
}


/**
 * 
 */
void VectorSet(vec3_t vec, float x, float y, float z)
{
	vec[0] = x;
	vec[1] = y;
	vec[2] = z;
}


/**
 * 
 */
void VectorMin(vec3_t vec1, vec3_t vec2, vec3_t out)
{
	out[0] = vec1[0] < vec2[0] ? vec1[0] : vec2[0];
	out[1] = vec1[1] < vec2[1] ? vec1[1] : vec2[1];
	out[2] = vec1[2] < vec2[2] ? vec1[2] : vec2[2];
}


/**
 * 
 */
void VectorMul(vec3_t vec1, vec3_t vec2, vec3_t out)
{
	out[0] = vec1[0] * vec2[0];
	out[1] = vec1[1] * vec2[1];
	out[2] = vec1[2] * vec2[2];
}


/**
 * 
 */
void VectorAverage(vec3_t vec1, vec3_t vec2, vec3_t out)
{
	out[0] = (vec1[0] + vec2[0]) / 2;
	out[1] = (vec1[1] + vec2[1]) / 2;
	out[2] = (vec1[2] + vec2[2]) / 2;
}


/**
 * 
 */
void CrossProduct(const vec3_t vec1, const vec3_t vec2, vec3_t cross)
{
	cross[0] = vec1[1] * vec2[2] - vec1[2] * vec2[1];
	cross[1] = vec1[2] * vec2[0] - vec1[0] * vec2[2];
	cross[2] = vec1[0] * vec2[1] - vec1[1] * vec2[0];
}


/**
 * 
 */
void VectorNormalizeFast(vec3_t vec)
{
	float ilength = InverseSquareRoot(DotProduct(vec, vec));

	vec[0] *= ilength;
	vec[1] *= ilength;
	vec[2] *= ilength;
}


/**
 * 
 */
vec_t VectorLength(vec3_t vec)
{
	float length = 0;

	for (int i = 0; i < 3; i++)
		length += vec[i] * vec[i];

	length = sqrt(length);

	return length;
}


/**
 * 
 */
void VectorInverse(vec3_t vec)
{
	vec[0] = -vec[0];
	vec[1] = -vec[1];
	vec[2] = -vec[2];
}


/**
 * 
 */
void VectorScale(vec3_t in, vec_t scale, vec3_t out)
{
	out[0] = in[0] * scale;
	out[1] = in[1] * scale;
	out[2] = in[2] * scale;
}


/**
 * 
 */
void MakeNormalVectors(vec3_t forward, vec3_t right, vec3_t up)
{
	// this rotate and negat guarantees a vector not colinear with the original
	right[1] = -forward[0];
	right[2] = forward[1];
	right[0] = forward[2];

	VectorScaleAndAdd(right, forward, right, -DotProduct(right, forward));
	VectorNormalize(right);
	CrossProduct(right, forward, up);
}
