#pragma once

#include "shared.h"

// ===============================================================================
// = ENUMERATIONS
// ===============================================================================
enum ColorIndex
{
	COLOR_TABLE_BEGIN,

	COLOR_BLACK = COLOR_TABLE_BEGIN,
	COLOR_RED,
	COLOR_GREEN,
	COLOR_YELLOW,
	COLOR_BLUE,
	COLOR_CYAN,
	COLOR_MAGENTA,
	COLOR_WHITE,
	COLOR_GRAY,
	COLOR_GOLD,

	COLOR_TABLE_END = COLOR_GOLD,

	COLOR_DEFAULT = COLOR_WHITE
};


typedef byte color4ub_t[4];
typedef float color4f_t[4];

color4ub_t& GetColorFromIndex(ColorIndex index);
