#pragma once

#include "shared.h"

#include <string>

#define	S_COLOR_BLACK		"^0"
#define	S_COLOR_RED		    "^1"
#define	S_COLOR_GREEN		"^2"
#define	S_COLOR_YELLOW		"^3"
#define	S_COLOR_BLUE		"^4"
#define	S_COLOR_CYAN		"^5"
#define	S_COLOR_MAGENTA		"^6"
#define	S_COLOR_WHITE		"^7"


bool isColorString(const char *p);

int strncasecmp(const char* s1, const char* s2, int n);
int strcasecmp(const char* s1, const char* s2);

void Q_snprintfz(char* dst, int dstSize, const char* fmt, ...);
void Q_strncpyz(char* dst, const char* src, int dstSize);
void Q_strcat(char* dst, const char* src, int dstSize);
void Q_strncatz(char* dst, int dstSize, const char* src);
void Q_sprintf(char* dest, int size, const char* fmt, ...);

int Q_strnicmp(const char* string1, const char* string2, int n);


// String functions & types
std::string toLowercase(const std::string& str);
std::string toUppercase(const std::string& str);

/**
 * Simple helper function to provide a printf like function.
 */
template<typename ... Args>
std::string string_format(const std::string& format, Args ... args)
{
	size_t size = snprintf(nullptr, 0, format.c_str(), args ...) + 1;
	std::unique_ptr<char[]> buffer(new char[size]);
	snprintf(buffer.get(), size, format.c_str(), args ...);
	return std::string(buffer.get(), buffer.get() + size - 1);
}
