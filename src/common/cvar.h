#pragma once

#include "shared.h"

#include <string>

// ===============================================================================
// = CVARS (console variables)
// ===============================================================================

#define	CVAR_ARCHIVE	1	// set to cause it to be saved to vars.rc
#define	CVAR_USERINFO	2	// added to userinfo  when changed
#define	CVAR_SERVERINFO	4	// added to serverinfo when changed
#define	CVAR_NOSET		8	// don't allow change from console at all, but can be set from the command line
#define	CVAR_LATCH		16	// save changes until server restart
#define CVAR_READONLY	32	// dont allow setting from command line or console
#define CVAR_DEVELOPER	64


/**
 * nothing outside the Cvar_*() functions should modify these fields!
 */
class ConsoleVariable
{
public:

	//const std::string& name() const { return name; }
	//const std::string& str() const { return string; }

	std::string name;
	std::string string;
	std::string latched_string;	// for CVAR_LATCH vars
	std::string help;
	std::string default_string;

	int flags = 0;
	bool modified = false;	// set each time the cvar is changed

	float value = 0.0f;
	int integer = 0;
	bool boolean = false;

	ConsoleVariable* next = nullptr;
};


/**
 * ConsoleVariable variables are used to hold scalar or string variables that can be
 * changed or displayed at the console or prog code as well as accessed directly
 * in C code.
 * 
 * The user can access cvars from the console in three ways:
 * r_draworder			prints the current value
 * r_draworder 0		sets the current value to 0
 * set r_draworder 0	as above, but creates the cvar if not present
 * 
 * Cvars are restricted from having the same names as commands to keep this
 * interface from being ambiguous.
 */

extern ConsoleVariable *cvar_vars;

bool userInfoModified();
void userInfoModified(bool _b);

ConsoleVariable* Cvar_Get(const std::string& name, const std::string& value, int flags);
ConsoleVariable* Cvar_Set(const std::string& name, const std::string& value);
ConsoleVariable* Cvar_ForceSet(const std::string& name, const std::string& value);
ConsoleVariable* Cvar_FullSet(const std::string& name, const std::string& value, int flags);
ConsoleVariable *Cvar_FindVar(const std::string& name);

void Cvar_SetValue(const std::string& name, double value);
void Cvar_SetValue(const std::string& name, float value);
void Cvar_SetValue(const std::string& name, int value);
void Cvar_SetValue(const std::string& name, size_t value);

void Cvar_ForceSetValue(const std::string& name, double value);
void Cvar_ForceSetValue(const std::string& name, float value);
void Cvar_ForceSetValue(const std::string& name, int value);
void Cvar_ForceSetValue(const std::string& name, size_t value);

float Cvar_VariableValue(const std::string& name);
const std::string& Cvar_VariableString(const std::string& name);

void Cvar_GetLatchedVars();
bool Cvar_Command();
void Cvar_WriteVariables(const std::string& path);
void Cvar_Init();
char* Cvar_Userinfo();
char* Cvar_Serverinfo();
