/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
#include "filesystem.h"

#include "common.h"
#include "string_util.h"

#include <physfs.h>
#include <stdio.h>


// ===============================================================================
// = MODULE LEVEL VARIABLES
// ===============================================================================
const std::string FS_GAME_DIRECTORY = BASEDIRNAME;
std::string GAME_DLL;
std::string GAME_DLL_PATH;

#if defined(WIN32)
const std::string LIBRARY_EXTENSION = "dll";
#else
#error "Dynamic library support for this system needs to be implemented."
#endif


// ===============================================================================
// = CVAR'S
// ===============================================================================
ConsoleVariable* fs_basedir;
ConsoleVariable* fs_gamedirvar;



// ===============================================================================
// = MODULE FUNCTIONS
// ===============================================================================

/**
 * Sort comparison function ?
 */
static int _sortListPtrs(const void *data1, const void *data2)
{
	return _stricmp(*(char* const*)data1, *(char* const*)data2);
}


/**
 * Use ~/.roguearena/dir as fs_gamedir.
 */
static void _addHomeAsGameDirectory(char* dir)
{
#ifndef _WIN32
	char		gdir[MAX_OSPATH];	/* Game directory. */
	char           *homedir;		/* Home directory. */

	if ((homedir = getenv("HOME")) != nullptr) {
		Q_sprintf(gdir, sizeof(gdir), "%s/.roguearena/%s", homedir, dir);
		FS_AddGameDirectory(gdir);
	}
#else
	// TODO: add C:\Users\username\...\roguearena\dir to path here, to use for saving files
#endif
}


/**
 * 
 */
static void _listPath()
{
	Com_Printf("\n\nCurrent search path:\n" BAR_LITE "\n");
	for (char** i = PHYSFS_getSearchPath(); *i != nullptr; i++)
		Com_Printf("%s\n", *i);
}


/**
 * Searches for archive files and mounts them into the search path.
 */
static void _mountArchives()
{
	int _fCount = 0;
	char** _fList = FS_ListFiles("", &_fCount, false);

	char _path[MAX_OSPATH] = { '\0' };

	for (int i = 0; i < _fCount; ++i)
	{
		if (!PHYSFS_isDirectory(_fList[i]))
		{
			if (strlen(_fList[i]) > 4 && !strcmp(_fList[i] + strlen(_fList[i]) - 4, ".zip"))
			{
				Q_sprintf(_path, sizeof(_path), "%s/%s", *PHYSFS_getSearchPath(), _fList[i]);
				Com_Printf("Adding \'" S_COLOR_YELLOW "%s" S_COLOR_WHITE "\' to search path.\n", _path);

				PHYSFS_mount(_path, nullptr, 1);
			}
		}
	}

	FS_FreeList(_fList);
}



// ===============================================================================
// = PUBLICLY EXPOSED FUNCTIONS
// ===============================================================================

/**
 * Gets the legnth in bytes of a file.
 *
 * \param file	Pointer to a \c qFILE struct.
 *
 * \return	Size in bytes of a file.
 */
int FS_FileLength(FS_File* file)
{
	return PHYSFS_fileLength(reinterpret_cast<PHYSFS_File*>(file));
}


/**
 * Indicates that a given file has been found in the search path.
 * 
 * \param filename	File to check for.
 * 
 * \return	Returns \c true if file exists, \c false otherwise.
 */
bool FS_FileExists(const std::string& filename)
{
	return PHYSFS_exists(filename.c_str()) > 0;
}


/**
 * Creates a specified path.
 */
void FS_MakeDirectory(const std::string& path)
{
	PHYSFS_mkdir(path.c_str());
}


/**
 * Closes an open file.
 */
void FS_Close(FS_File* f)
{
	PHYSFS_close(reinterpret_cast<PHYSFS_File*>(f));
}


/**
 * Deletes a specified file.
 */
void FS_Delete(const std::string& filename)
{
	if (PHYSFS_delete(filename.c_str()) == 0)
	{
		Com_Printf("FS_Delete(): Can't delete '%s' : %s\n", filename.c_str(), PHYSFS_getLastError());
	}
}


/**
 * Finds the file in the search path.
 * 
 * Used for streaming data out of either a pak file or a seperate file.
 * 
 * \param filename	Name of the file to open.
 * \param mode		Mode to open the file for working with.
 * 
 * \return	FS_File handle. nullptr if unable to open the specified file.
 */
FS_File* FS_Open(const std::string& filename, fs_open_mode mode)
{
	switch(mode)
	{
	case FS_OPEN_APPEND:
		return reinterpret_cast<FS_File*>(PHYSFS_openAppend(filename.c_str()));
		break;

	case FS_OPEN_READ:
		return reinterpret_cast<FS_File*>(PHYSFS_openRead(filename.c_str()));
		break;

	case FS_OPEN_WRITE:
		return reinterpret_cast<FS_File*>(PHYSFS_openWrite(filename.c_str()));
		break;

	default:
		Com_Error(ERR_FATAL, "*** INVALID FILE OPEN MODE!\n");
		break;
	}

	return nullptr;
}


/**
 * Read bytes from an FS_File* into a buffer.
 */
size_t FS_Read(void* buffer, size_t size, size_t count, FS_File* stream)
{
	int bytesRead = PHYSFS_read((PHYSFS_File*)stream, buffer, size, count);
	return bytesRead;
}


/**
 * Renames a file.
 * 
 * \param oldname	Name of the file to be renamed.
 * \param newname	New name of the file.
 * 
 * \warning	This function is inefficient. Use sparingly.
 */
bool FS_Rename(const char* oldname, const char* newname)
{
	if (!FS_FileExists(oldname))
	{
		Com_Printf("FS_Rename(): File \'%s\' not found.\n", oldname);
		return false;
	}
	
	FS_File* _old = FS_Open(oldname, FS_OPEN_READ);
	FS_File* _new = FS_Open(newname, FS_OPEN_WRITE);
	int _length = FS_FileLength(_old);

	char* buffer = static_cast<char*>(calloc(_length, sizeof(byte)));

	FS_Read(buffer, sizeof(byte), _length, _old);
	FS_Write(_new, buffer, sizeof(byte), _length);

	FS_Close(_old);
	FS_Close(_new);
	FS_Delete(oldname);

	free(buffer);

	return true;
}


/**
 * Copies a file \c src to \c dst.
 * 
 * \param src	Name of the file to copy.
 * \param dst	Name of the copied file.
 */
bool FS_Copy(const char* src, const char* dst)
{
	if (!FS_FileExists(src))
	{
		Com_Printf("FS_Copy(): File \'%s\' not found.\n", src);
		return false;
	}

	FS_File* _old = FS_Open(src, FS_OPEN_READ);
	FS_File* _new = FS_Open(dst, FS_OPEN_WRITE);
	int _length = FS_FileLength(_old);

	char* buffer = static_cast<char*>(calloc(_length, sizeof(byte)));

	FS_Read(buffer, sizeof(byte), _length, _old);
	FS_Write(_new, buffer, sizeof(byte), _length);

	/* This writes byte for byte, perhaps makes sense to handle large files?
	for (;;)
	{
		int l = fread(buffer, 1, sizeof(buffer), f1);
		if (!l)
			break;

		fwrite(buffer, 1, l, f2);
	}
	*/

	FS_Close(_old);
	FS_Close(_new);

	free(buffer);

	return true;
}


/**
 * 
 */
int FS_Seek(FS_File* f, size_t pos)
{
	return PHYSFS_seek((PHYSFS_File*)f, pos);
}


/**
 * 
 */
size_t FS_Tell(FS_File* f)
{
	return PHYSFS_tell((PHYSFS_File*)f);
}


/**
 * File names are reletive to the quake search path.
 * 
 * \param path		Path of the file to load.
 * \param buffer	Buffer to store contents of the loaded file. Can be nullptr.
 * 
 * \return	Size of the file or FS_OPEN_FAILED if file couldn't be loaded.
 * 
 * \note	Don't forget to free the file with FS_FreeFile.
 * 
 * \see FS_FreeFile
 */
int FS_LoadFile(const char* path, void **buffer)
{
	PHYSFS_File* file = PHYSFS_openRead(path);
	if (!file)
		return FS_OPEN_FAILED;

	int fileLen = PHYSFS_fileLength(file);

	*buffer = calloc(fileLen + 1, sizeof(byte));
	
	int bytesRead = PHYSFS_read(file, *buffer, sizeof(byte), fileLen);

	const char* msg = PHYSFS_getLastError();
	if (bytesRead != fileLen)
		Com_Printf("FS_LoadFile(): Error reading \'%s\' -- %s", path, PHYSFS_getLastError());

	FS_Close(reinterpret_cast<FS_File*>(file));

	return bytesRead;
}


/**
 * 
 */
int FS_Write(FS_File* f, const void* buffer, size_t objSize, size_t count)
{
	return PHYSFS_write(reinterpret_cast<PHYSFS_File*>(f), buffer, objSize, count);
}


/**
 * 
 */
void FS_fprintf(FS_File* f, const char* fmt, ...)
{
	va_list argptr;
	char buffer[0x10000] = { '\0' };

	assert(f);

	va_start(argptr, fmt);
	int len = vsnprintf(buffer, sizeof(buffer), fmt, argptr);
	va_end(argptr);

	PHYSFS_write(reinterpret_cast<PHYSFS_File*>(f), buffer, sizeof(char), len);
}


void FS_Flush(FS_File* f)
{
	PHYSFS_flush(reinterpret_cast<PHYSFS_File*>(f));
}


/**
 * Frees a file buffer.
 * 
 * \param	buffer	Pointer to a buffer.
 * 
 * \note	Use only if buffer data was filled with FS_LoadFile()
 * 
 * \see FS_LoadFile
 */
void FS_FreeFile(void* buffer)
{
	free(buffer);
}


/**
 * Sets the gamedir and path to a different directory.
 */
void FS_SetGameDirectory(const std::string& path)
{
	GAME_DLL = path + "." + LIBRARY_EXTENSION;
	GAME_DLL_PATH = FS_GAME_DIRECTORY + "/" + GAME_DLL;
}


/**
 * Get a list of files within a specified directory.
 *
 * \param	directory		Directory name to search.
 * \param	fileCount		Pointer to an int. File count is saved here. Safe to pass nullptr.
 * \param	allowDirectory	Allow directories in returned list.
 * 
 * \note	Don't forget to free the returned list by using FS_FreeList.
 * 
 * \see		FS_FreeList
 */
char** FS_ListFiles(const char* directory, int* fileCount, bool allowDirectory)
{
	if (!directory)	// Guard against silly mistakes.
		return nullptr;

	char** _list = PHYSFS_enumerateFiles(directory);
	int _fileCount = 0, _fileCount_Pruned = 0;
	for (char** _ls = _list; *_ls != nullptr; ++_ls, ++_fileCount)
	{
		if (!PHYSFS_isDirectory(*_ls))
			_fileCount_Pruned++;
	}

	if (fileCount != nullptr)
		(*fileCount) = _fileCount_Pruned;

	char** _returnList = nullptr;
	if (!allowDirectory)
	{
		_returnList = static_cast<char**>(calloc(_fileCount_Pruned + 1, sizeof(char*)));

		for (int i = 0, j = 0; i < _fileCount; ++i)
		{
			if (!PHYSFS_isDirectory(_list[i]))
			{
				int _len = (strlen(_list[i]) + 1);
				_returnList[j] = static_cast<char*>(calloc(_len, sizeof(char)));
				Q_sprintf(_returnList[j], _len, _list[i]);
				++j;
			}
		}
		PHYSFS_freeList(_list);
	}
	else
	{
		_returnList = _list;
	}

	return _returnList;
}


/**
 * Get a list of files within a specified directory with a matching extension.
 * 
 * \param	directory		Directory name to search.
 * \param	extension		Exension to use when matching files. Should include the '.' or results may be inaccurate.
 * \param	fileCount		Pointer to an int. File count is saved here. Safe to pass nullptr.
 * 
 * \note	Don't forget to free the returned list by using FS_FreeList.
 *
 * \see		FS_FreeList
 */
char** FS_ListFilesByExtension(const char* directory, const char* extension, int* fileCount)
{
	if (!directory || !extension)	// Guard against silly mistakes.
		return nullptr;

	int _fileCount = 0;
	char** _list = FS_ListFiles(directory, &_fileCount, false);

	char** _matchedFiles = nullptr;

	int _extensionLength = strlen(extension);
	int _matchCount = 0;
	for (int i = 0; i < _fileCount; ++i)
	{
		if (strlen(_list[i]) > _extensionLength &&
			!strcmp(_list[i] + strlen(_list[i]) - _extensionLength, extension))
		{
			++_matchCount;
			_matchedFiles = static_cast<char**>(realloc(_matchedFiles, _matchCount * sizeof(char*)));
			_matchedFiles[_matchCount - 1] = static_cast<char*>(calloc(strlen(_list[i]) + 1, sizeof(char)));

			Q_sprintf(_matchedFiles[_matchCount - 1], strlen(_list[i]) + 1, _list[i]);
		}
	}
	
	if (_matchedFiles)
	{
		_matchedFiles = static_cast<char**>(realloc(_matchedFiles, (_matchCount + 1) * sizeof(char*)));
		_matchedFiles[_matchCount] = '\0';
	}

	if (fileCount)
		(*fileCount) = _matchCount;

	return _matchedFiles;
}


/**
 * Free list of files returned by FS_ListFiles().
 * 
 * \see	FS_ListFiles
 */
void FS_FreeList(char** list)
{
	if (!list) { return; }
	PHYSFS_freeList(list);
}


/**
 *
 */
void FS_StripExtension(const char* in, char* out, size_t size_out)
{
	char* last = nullptr;

	if (size_out == 0)
		return;

	while (*in && size_out > 1)
	{
		if (*in == '.')
			last = out;
		else if (*in == '/' || *in == '\\' || *in == ':')
			last = nullptr;

		*out++ = *in++;
		size_out--;
	}

	if (last)
		*last = 0;
	else
		*out = 0;
}


/**
 * Intiailizes the filesytem.
 * 
 * \param	argv0
 */
void FS_InitFilesystem(const char* argv0)
{
	Com_Printf("\n" BAR_BOLD "\n");
	Com_Printf(S_COLOR_YELLOW "Initializing Filesystem\n");
	Com_Printf(BAR_BOLD "\n\n");
	Cmd_AddCommand("path", _listPath);

	if(!PHYSFS_init(argv0))
		Com_Error(ERR_FATAL, "FS_InitFilesystem(): %s", PHYSFS_getLastError());

	PHYSFS_Version linked;
	PHYSFS_getLinkedVersion(&linked);
	Com_Printf("PhysicsFS Version: %d.%d.%d.\n\n", linked.major, linked.minor, linked.patch);

	/**
	 * Explicitly disallow symlinks, otherwise the link could take you outside the write and
	 * search paths potentially compromising security.
	 */
	PHYSFS_permitSymbolicLinks(0);

	// basedir <path>
	// allows the game to run from outside the data tree
	fs_basedir = Cvar_Get("basedir", ".", CVAR_NOSET);

	// start up with baseq2 by default
	PHYSFS_mount(BASEDIRNAME, nullptr, 1);
		
	_mountArchives();
	
	/**
	 * \fixme	Looking for the game DLL file is an inapporpriate function to perform
	 *			within the filesystem. This should be part of the game init code.
	 */
	fs_gamedirvar = Cvar_Get("game", "", CVAR_LATCH | CVAR_SERVERINFO);
	
	if (fs_gamedirvar->string.empty())
	{
		Cvar_Set("game", "game");
	}

	if (!fs_gamedirvar->string.empty())
	{
		FS_SetGameDirectory(fs_gamedirvar->string);
	}

	/* Create directory if it does not exist. */
	if (!FS_FileExists(BASEDIRNAME))
	{
		FS_MakeDirectory(BASEDIRNAME);
	}

	PHYSFS_setWriteDir(BASEDIRNAME);
	Com_Printf("\nUsing \'" S_COLOR_YELLOW "%s" S_COLOR_WHITE "\' for writing.\n\n", BASEDIRNAME);

	if (!FS_FileExists(GAME_DLL.c_str()))
	{
		Com_Error(ERR_FATAL, "Game DLL not found.\n");
		return;
	}

	Com_Printf(S_COLOR_YELLOW "Found game library at '%s'\n", GAME_DLL_PATH.c_str());

	Com_Printf("\n" BAR_LITE "\n");
	Com_Printf(S_COLOR_YELLOW "Filesystem initialized.\n");
	Com_Printf(BAR_LITE "\n\n");
}


void FS_Shutdown()
{
	PHYSFS_deinit();
}