#pragma once

#include "shared.h"

#include <string>

// ===============================================================================
// = FILESYSTEM
// ===============================================================================

#define FS_OPEN_FAILED		-1
#define FS_WRITE_FAILED		-1


typedef enum FileOpenMode
{
	FS_OPEN_APPEND,
	FS_OPEN_READ,
	FS_OPEN_WRITE
} fs_open_mode;


/**
 * A handle to a filesystem file.
 * 
 * \note	For use with the filesystem functions.
 * 
 * \warning	Don't try to manipulate the file handle, just pass the
 *			pointer you got, unmolested, to the filesystem functions.
 */
typedef struct
{
	void *opaque;
} FS_File;


void FS_InitFilesystem(const char* argv0);
void FS_Shutdown();

void FS_SetGameDirectory(const std::string& dir);

int FS_FileLength(FS_File* f);
int FS_LoadFile(const char* path, void** buffer);
FS_File* FS_Open(const std::string& filename, fs_open_mode mode);

void FS_Close(FS_File* f);
void FS_Delete(const std::string& filename);
bool FS_FileExists(const std::string& filename);
void FS_FreeFile(void* buffer);
void FS_MakeDirectory(const std::string& path);
size_t FS_Read(void* buffer, size_t size, size_t count, FS_File* stream);
void FS_StripExtension(const char* in, char* out, size_t size_out);

void FS_fprintf(FS_File* f, const char* fmt, ...);
int FS_Write(FS_File* f, const void* buffer, size_t objSize, size_t count);
void FS_Flush(FS_File* f);

int FS_Seek(FS_File*, size_t pos);
size_t FS_Tell(FS_File*);

bool FS_Rename(const char* oldname, const char* newname);
bool FS_Copy(const char* src, const char* dst);

char** FS_ListFiles(const char* directory, int* fileCount, bool allowDirectory);
char** FS_ListFilesByExtension(const char* directory, const char* extension, int* fileCount);

void FS_FreeList(char** list);
