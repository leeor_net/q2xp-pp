#pragma once

#include "GL/glew.h"

/**
 * \enum ImageType
 *
 * Skins will be outline flood filled and mip mapped.
 * Pics and sprites with alpha will be outline flood filled
 * Pics won't be mip mapped
 */
enum ImageType
{
	IT_UNDEFINED,
	IT_SKIN,		/**< Model skin */
	IT_SPRITE,		/**< Sprite frame */
	IT_WALL,		/**< Wall texture */
	IT_PIC,			/**< UI element */
	IT_SKY,			/**< Skybox */
	IT_AUTOBUMP,	/**<  */
	IT_PARALLAX,	/**<  */
	IT_BUMP,		/**< Normal map. */
};


#include <string>

/**
 * 
 */
struct Image
{
public:
	void clear();

	std::string	name;							// game path, including extension

	ImageType	type = IT_UNDEFINED;

	int			width, height;					// source image
	int			upload_width, upload_height;	// after power of two and picmip
	int			registration_sequence;			// 0 = free

	struct msurface_s* texturechain;			// for sort-by-texture world drawing

	GLuint		texnum;							// gl texture binding

	float		sl, tl, sh, th;					// 0.0 - 1.1 unless part of the scrap

	bool		scrap;
	bool		has_alpha;
	bool		envMap;

	float		parallaxScale;
	float		specularScale;
	float		specularExp;
	float		envScale;
	float		rghScale;

	float		alpha;
};
