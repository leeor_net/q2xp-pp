#include "string_util.h"

#include "common.h"

#include <algorithm>
#include <stdio.h>

#include <cctype>
#include <algorithm>

/**
 * Compares two strings.
 * 
 * \fixme	Document this properly.
 */
int strncasecmp(const char* s1, const char* s2, int n)
{
	int		c1, c2;

	do {
		c1 = *s1++;
		c2 = *s2++;

		if (!n--)
			return 0;		// strings are equal until end point

		if (c1 != c2) {
			if (c1 >= 'a' && c1 <= 'z')
				c1 -= ('a' - 'A');
			if (c2 >= 'a' && c2 <= 'z')
				c2 -= ('a' - 'A');
			if (c1 != c2)
				return -1;		// strings not equal
		}
	} while (c1);

	return 0;		// strings are equal
}


/**
 * 
 */
int strcasecmp(const char* s1, const char* s2)
{
	return strncasecmp(s1, s2, 99999);
}


/**
 * 
 */
void Q_strncpyz(char* dst, const char* src, int dstSize)
{
	if (!dst)
		Sys_Error(ERR_FATAL, "Q_strncpyz: nullptr dst");

	if (!src)
		Sys_Error(ERR_FATAL, "Q_strncpyz: nullptr src");

	if (dstSize < 1)
		Sys_Error(ERR_FATAL, "Q_strncpyz: dstSize < 1");

	strncpy(dst, src, dstSize - 1);
	dst[dstSize - 1] = 0;
}


/**
 * Safe strncat that ensures a trailing zero
 */
void Q_strncatz(char* dst, int dstSize, const char* src)
{
	if (!dst)
	{
		Com_Printf("Q_strncatz:� nullptr dst\n");
		return;
	}

	if (!src)
	{
		Com_Printf("Q_strncatz:� nullptr src\n");
		return;
	}

	if (dstSize < 1)
	{
		Com_Printf("Q_strncatz:� dstSize < 1\n");
		return;
	}

	while (--dstSize && *dst)
		dst++;

	if (dstSize > 0)
	{
		while (--dstSize && *src)
			*dst++ = *src++;

		*dst = 0;
	}
}


/**
 * Safe snprintf that ensures a trailing zero
 */
void Q_snprintfz(char* dst, int dstSize, const char* fmt, ...)
{
	va_list	argPtr;

	if (!dst)
		Sys_Error(ERR_FATAL, "Q_snprintfz: nullptr dst");

	if (dstSize < 1)
		Sys_Error(ERR_FATAL, "Q_snprintfz: dstSize < 1");

	va_start(argPtr, fmt);
	vsnprintf(dst, dstSize, fmt, argPtr);
	va_end(argPtr);

	dst[dstSize - 1] = 0;
}


/**
 * 
 */
int Q_strnicmp(const char* string1, const char* string2, int n)
{
	if (string1 == nullptr)
	{
		if (string2 == nullptr) { return 0; }
		else { return -1; }
	}
	else if (string2 == nullptr)
	{
		return 1;
	}

	int c1 = 0, c2 = 0;
	do
	{
		c1 = *string1++;
		c2 = *string2++;

		if (!n--)
			return 0; // Strings are equal until end point 

		if (c1 != c2)
		{
			if (c1 >= 'a' && c1 <= 'z')
				c1 -= ('a' - 'A');
			if (c2 >= 'a' && c2 <= 'z')
				c2 -= ('a' - 'A');

			if (c1 != c2)
				return c1 < c2 ? -1 : 1;
		}
	} while (c1);

	return 0; // Strings are equal 
}


/**
 * 
 */
void Q_strcat(char* dst, const char* src, int dstSize)
{
	int len = strlen(dst);

	if (len >= dstSize)
		Sys_Error(ERR_FATAL, "Q_strcat: already overflowed");

	Q_strncpyz(dst + len, src, dstSize - len);
}


/**
 * 
 */
void Q_sprintf(char* dest, int size, const char* fmt, ...)
{
	va_list argptr;
	char bigbuffer[0x10000] = { '\0' };  // cute -_-

	va_start(argptr, fmt);
	int len = vsnprintf(bigbuffer, sizeof(bigbuffer), fmt, argptr);
	va_end(argptr);

	if (len >= size)
		Com_Printf("Q_sprintf: overflow of %i in %i\n", len, size);

	strncpy(dest, bigbuffer, size - 1);
}


/**
 * \fn toLowercase(const std::string& str)
 *
 * Converts a string to lowercase.
 * 
 * \param str	Source string.
 * 
 * \return	Returns the converted string.
 */
std::string toLowercase(const std::string& str)
{
	std::string transformStr(str);
	std::transform(transformStr.begin(), transformStr.end(), transformStr.begin(), (int(*)(int))std::tolower);
	return transformStr;
}


/**
 * \fn toUppercase(const std::string& str)
 *
 * Converts a string to uppercase.
 * 
 * \param str	Source string.
 * 
 * \return	Returns the converted string.
 */
std::string toUppercase(const std::string& str)
{
	std::string transformStr(str);
	std::transform(transformStr.begin(), transformStr.end(), transformStr.begin(), (int(*)(int))std::toupper);
	return transformStr;
}