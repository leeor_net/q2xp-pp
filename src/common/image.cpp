#include "image.h"


void Image::clear()
{
	name.clear();

	type = IT_UNDEFINED;

	width = 0, height = 0;
	upload_width = 0, upload_height = 0;
	registration_sequence = 0;
	texturechain = nullptr;
	texnum = 0;
	sl = 0, tl = 0, sh = 0, th = 0;

	scrap = false;
	has_alpha = false;
	envMap = false;

	parallaxScale = 0.0f;
	specularScale = 0.0f;
	specularExp = 0.0f;
	envScale = 0.0f;
	rghScale = 0.0f;

	alpha = 0.0f;
}
