/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
#pragma once

#include <GL/glew.h>
#include <windows.h>

#include "Joystick.h"

#include "../common/common.h"


// ===============================================================================
// = FUNCTION PROTOTYPES
// ===============================================================================
HINSTANCE getMainInstance();

HWND getMainWindowHandle();

bool appActive();
bool appMinimized();

void setAppActive(bool _b);
void setAppMinimized(bool _b);

void setMainInstance(HINSTANCE _h);
HINSTANCE getMainInstance();

unsigned int SystemMessageTime(void);
void SetSystemMessageTime(unsigned int _t);

unsigned int SystemFrameTime(void);
void SetSystemFrameTime(unsigned int _t);

void IN_Activate(bool active);
void IN_StartupXInput();
void IN_ToggleXInput();
void IN_ControllerMove(usercmd_t *cmd);
void IN_ShutDownXinput();

void IN_ZoomDown();
void IN_ZoomUp();

void pollMouseButtonState(bool* left, bool* right);
void getMousePosition(int* x, int* y);

int windowCenterX();
int windowCenterY();

void showPointer();
void hidePointer();

const Point2D_t* videoResolution();
int videoWidth();
int videoHeight();
int videoCenterScreenX();
int videoCenterScreenY();

bool xInputActive();
bool looking();
bool mouseCaptured();
