/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
#include <GL/glew.h>

#include "../common/common.h"
#include "../common/string_util.h"

#include "winquake.h"

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <direct.h>
#include <io.h>
#include <conio.h>
#include <math.h>

int		hunkcount;

byte* membase;
int hunkmaxsize;
int cursize;
char hunk_name[MAX_OSPATH] = { '\0' };


void *Hunk_Begin(int maxsize, char* name)
{
	// reserve a huge chunk of memory, but don't commit any yet
	cursize = 0;
	hunkmaxsize = maxsize;
	memset(hunk_name, 0, strlen(name) + 1);
	strcpy(hunk_name, name);

	membase = static_cast<byte*>(VirtualAlloc(nullptr, maxsize, MEM_RESERVE, PAGE_NOACCESS));

	if (!membase)
	{
		Sys_Error("VirtualAlloc reserve failed %s", hunk_name);
	}

	return (void*)membase;
}


void *Hunk_Alloc(int size)
{
	void* buf;

	// round to cacheline
	size = (size + 31)&~31;

	// commit pages as needed
	buf = VirtualAlloc(membase, cursize + size, MEM_COMMIT, PAGE_READWRITE);
	if (!buf)
	{
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, nullptr, GetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&buf, 0, nullptr);
		Sys_Error("VirtualAlloc commit failed.\n%s", buf);
	}

	cursize += size;
	if (cursize > hunkmaxsize)
		Sys_Error("Hunk_Alloc overflow");

	return (void*)(membase + cursize - size);
}


int Hunk_End()
{
	int size = ceil(cursize / 4096.0f) * 4096;     // page size always is 4096, 8192, etc...

	if (hunkmaxsize > size)
		VirtualFree(membase + size, hunkmaxsize - size, MEM_RELEASE);

	hunkmaxsize = 0;

	///     hunkcount++;
	return cursize;
}



void Hunk_Free(void *base)
{
	if (base)
		VirtualFree(base, 0, MEM_RELEASE);

	hunkcount--;
}



///\fixme	The below functions _DO NOT_ belong in this file as they do not deal with memory at all.

/*
================
Sys_Milliseconds
================
*/

int	curtime;

int Sys_Milliseconds()
{
	static int base;
	static bool initialized = false;

	// let base retain 16 bits of effectively random data
	if (!initialized)
	{
		base = timeGetTime() & 0xffff0000;
		initialized = true;
	}

	curtime = timeGetTime() - base;

	return curtime;
}
