/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "win_glimp.h"

#include <GL/wglew.h>

#include "../common/string_util.h"


// ===============================================================================
// = DEFINES
// ===============================================================================
#define	WINDOW_NAME				"Rogue Arena"
#define	WINDOW_CLASS_NAME		"rogue-arena"

#define	WINDOW_STYLE			(WS_OVERLAPPED|WS_BORDER|WS_CAPTION|WS_VISIBLE)
#define	WINDOWBORDERLESS_STYLE	(WS_VISIBLE | WS_POPUP)

#define MAX_SUPPORTED_MONITORS  16

#define WIDTH					7


// ===============================================================================
// = FUNCTION PROTOTYPES
// ===============================================================================
LONG WINAPI MainWndProc(HWND hWnd, size_t uMsg, WPARAM wParam, LPARAM lParam);
bool GLW_InitDriver();
void setVideoResolution(int width, int height);


// ===============================================================================
// = FUNCTION IMPORTS
// ===============================================================================
void getMousePosition(int* x, int* y);


// ===============================================================================
// = FUNCTION POINTER PROTOTYPES
// ===============================================================================
typedef void (WINAPI *PGNSI)(LPSYSTEM_INFO);
typedef BOOL(WINAPI *PGPI)(DWORD, DWORD, DWORD, DWORD, PDWORD);


// ===============================================================================
// = EXPORTS
// ===============================================================================
// Enable High Performance Graphics while using Integrated Graphics.
__declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;        // Nvidia
__declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;  // AMD


// ===============================================================================
// = STRUCTS
// ===============================================================================

/**
 * 
 */
typedef struct
{
	bool	accelerated;
	bool	drawToWindow;
	bool	supportOpenGL;
	bool	doubleBuffer;
	bool	rgba;

	int		colorBits;
	int		alphaBits;
	int		depthBits;
	int		stencilBits;
	int		samples;
} glwPixelFormatDescriptor_t;


// ===============================================================================
// = LOCAL VARIABLES
// ===============================================================================
glwstate_t glw_state;

typedef struct
{
	MONITORINFO info;
	char name[16];
} _monitor_t;

_monitor_t MONITORS[MAX_SUPPORTED_MONITORS];

int MONITOR_COUNTER = 0;




/**
 * 
 */
glwstate_t* GLW_State()
{
	return &glw_state;
}


/**
 * 
 */
bool createWindow(int width, int height, bool fullscreen)
{
	glw_state.wndproc = MainWndProc;

	// Register the frame class
	WNDCLASS wc;
	memset(&wc, 0, sizeof(wc));
	wc.style = 0;
	wc.lpfnWndProc = (WNDPROC)glw_state.wndproc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = glw_state.hInstance;
	wc.hIcon = 0;
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)COLOR_GRAYTEXT;
	wc.lpszMenuName = 0;
	wc.lpszClassName = WINDOW_CLASS_NAME;

	if (!RegisterClass(&wc))
		VID_Error(ERR_FATAL, "Couldn't register window class");

	// Compute width and height
	DEVMODE dm;
	memset(&dm, 0, sizeof(dm));
	dm.dmSize = sizeof(dm);
	if (glw_state.desktopName[0])
	{
		if (!EnumDisplaySettings(glw_state.desktopName, ENUM_CURRENT_SETTINGS, &dm))
		{
			memset(&dm, 0, sizeof(dm));
			dm.dmSize = sizeof(dm);
		}
	}

	// save real monitor position in the virtual monitor
	glw_state.desktopPosX = dm.dmPosition.x;
	glw_state.desktopPosY = dm.dmPosition.y;

	ConsoleVariable* vid_BorderlessWindow = Cvar_Get("vid_BorderlessWindow", "0", CVAR_ARCHIVE);

	int exstyle = 0, stylebits = 0;
	if (fullscreen)
	{
		exstyle = WS_EX_TOPMOST;
		stylebits = WS_POPUP | WS_VISIBLE;
	}
	else
	{
		exstyle = 0;
		if (!vid_BorderlessWindow->integer)
			stylebits = WINDOW_STYLE;
		else
			stylebits = WINDOWBORDERLESS_STYLE;
	}

	glw_state.virtualX = GetSystemMetrics(SM_XVIRTUALSCREEN);
	glw_state.virtualY = GetSystemMetrics(SM_YVIRTUALSCREEN);
	glw_state.virtualWidth = GetSystemMetrics(SM_CXVIRTUALSCREEN);
	glw_state.virtualHeight = GetSystemMetrics(SM_CYVIRTUALSCREEN);
	glw_state.borderWidth = GetSystemMetrics(SM_CXBORDER) * 3;
	glw_state.borderHeight = GetSystemMetrics(SM_CYBORDER) * 3 + GetSystemMetrics(SM_CYCAPTION);
	
	ConsoleVariable* vid_xpos = Cvar_Get("vid_xpos", "0", 0);
	ConsoleVariable* vid_ypos = Cvar_Get("vid_ypos", "0", 0);

	int x = 0, y = 0;
	if (fullscreen)
	{
		x = glw_state.desktopPosX;
		y = glw_state.desktopPosY;
	}
	else
	{
		// adjust window coordinates if necessary so that the window is completely on screen
		x = clamp(vid_xpos->value, static_cast<float>(glw_state.virtualX), static_cast<float>(glw_state.virtualX + glw_state.virtualWidth - 64));
		y = clamp(vid_ypos->value, static_cast<float>(glw_state.virtualY), static_cast<float>(glw_state.virtualY + glw_state.virtualHeight - 64));
	}

	RECT _windowRect = { glw_state.desktopPosX, glw_state.desktopPosY, width, height };
	AdjustWindowRect(&_windowRect, stylebits, FALSE);

	glw_state.hWnd = CreateWindowExA(exstyle, WINDOW_CLASS_NAME, WINDOW_NAME, stylebits, x, y, _windowRect.right - _windowRect.left, _windowRect.bottom - _windowRect.top, nullptr, nullptr, glw_state.hInstance, nullptr);

	if (!glw_state.hWnd)
		VID_Error(ERR_FATAL, "Couldn't create window");

	ShowWindow(glw_state.hWnd, SW_SHOW);
	UpdateWindow(glw_state.hWnd);

	// init all the gl stuff for the window
	if (!GLW_InitDriver())
	{
		Com_Printf(S_COLOR_RED "createWindow(): GLimp_InitDriver failed\n");

		ShowWindow(glw_state.hWnd, SW_HIDE);
		DestroyWindow(glw_state.hWnd);
		glw_state.hWnd = nullptr;

		UnregisterClass(WINDOW_CLASS_NAME, glw_state.hInstance);
		return false;
	}

	SetForegroundWindow(glw_state.hWnd);
	SetFocus(glw_state.hWnd);

	SetCursor(wc.hCursor);

	// let the sound and input subsystems know about the new window
	VID_NewWindow(width, height);

	return true;
}


/**
 * 
 */
BOOL GetDisplayMonitorInfo(char* monitorName, char* monitorModel)
{
	DISPLAY_DEVICE dd;
	int i = 0;
	BOOL bRet = FALSE;

	monitorModel[0] = 0;
	ZeroMemory(&dd, sizeof(dd));
	dd.cb = sizeof(dd);

	while (EnumDisplayDevices(glw_state.desktopName, i, &dd, EDD_GET_DEVICE_INTERFACE_NAME))
	{
		if (dd.StateFlags & DISPLAY_DEVICE_ACTIVE)
		{
			char* p, *s;
			char deviceID[128];
			char regPath[128];
			byte edid[128];
			HKEY hKey;
			int j = 0;
			lstrcpy(deviceID, dd.DeviceID);
			p = strstr(deviceID, "DISPLAY");
			if (p)
			{
				s = p;
				while (1)
				{
					if (*s == 0)
					{
						j = -1; // not found
						break;
					}
					if (*s == '#')
					{
						j++;
						if (j == 3)
						{
							*s = 0;
							break;
						}
						else
							*s = '\\';
					}
					s++;
				}
				if (j != -1)
				{
					LSTATUS err;
					Q_sprintf(regPath, sizeof(regPath), "SYSTEM\\CurrentControlSet\\Enum\\%s\\Device Parameters\\", p);
					err = RegOpenKeyEx(HKEY_LOCAL_MACHINE, regPath, 0, KEY_READ, &hKey);
					if (err == ERROR_SUCCESS)
					{
						DWORD buflen = sizeof(edid);
						err = RegQueryValueEx(hKey, "EDID", nullptr, nullptr, edid, &buflen);
						RegCloseKey(hKey);
						if (err == ERROR_SUCCESS)
						{
							int k, m, n, descOffs[4] = { 54, 72, 90, 108 };
							for (k = 0; k < 4; k++)
							{
								byte *desc = &edid[descOffs[k]];
								if (desc[0] == 0 && desc[1] == 0 && desc[2] == 0 && desc[3] == 0xFC)
								{
									Q_strncpyz(monitorModel, reinterpret_cast<const char*>(&desc[5]), 13);
									n = strlen(monitorModel);
									for (m = 0; m < n; m++)
										if (monitorModel[m] == '\n')
											monitorModel[m] = 0;
									break;
								}
							}
						}
					}
				}
			}

			lstrcpy(monitorName, dd.DeviceString);
			bRet = TRUE;
			break;
		}
		i++;
	}

	return bRet;
}


/**
 * 
 */
BOOL CALLBACK enumerateMonitors(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData)
{
	MONITORS[MONITOR_COUNTER].info.cbSize = sizeof(MONITORINFO);
	if (GetMonitorInfo(hMonitor, &MONITORS[MONITOR_COUNTER].info))
	{
		++MONITOR_COUNTER;
		if (MONITOR_COUNTER == MAX_SUPPORTED_MONITORS)
			return FALSE;
	}
	else
		return FALSE;

	return TRUE;
}


/**
 * 
 */
BOOL CALLBACK printMonitorInfo(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData)
{
	MONITORS[MONITOR_COUNTER].info.cbSize = sizeof(MONITORINFO);
	if (GetMonitorInfo(hMonitor, &MONITORS[MONITOR_COUNTER].info))
	{
		int width = abs(MONITORS[MONITOR_COUNTER].info.rcMonitor.left - MONITORS[MONITOR_COUNTER].info.rcMonitor.right);
		int height = abs(MONITORS[MONITOR_COUNTER].info.rcMonitor.top - MONITORS[MONITOR_COUNTER].info.rcMonitor.bottom);
		Com_Printf("  " S_COLOR_GREEN "%i" S_COLOR_WHITE ": %i" S_COLOR_GREEN "x" S_COLOR_WHITE "%i", MONITOR_COUNTER + 1, width, height);

		if (MONITORS[MONITOR_COUNTER].name[0])
			Com_Printf(S_COLOR_YELLOW" %s", MONITORS[MONITOR_COUNTER].name);
		else
			Com_Printf(S_COLOR_YELLOW" Unknown model");

		if (MONITORS[MONITOR_COUNTER].info.dwFlags & MONITORINFOF_PRIMARY)
			Com_Printf(" (" S_COLOR_YELLOW "primary" S_COLOR_WHITE ")");

		Com_Printf("\n");
		++MONITOR_COUNTER;

		if (MONITOR_COUNTER == MAX_SUPPORTED_MONITORS)
			return FALSE;
	}
	else
		return FALSE;

	return TRUE;
}


/**
 * \fixme	As much as I hate this function it generally works so I'll leave it intact
 *			for now while I work on other higher priority parts of the code.
 */
rserr_t GLimp_SetMode(int _mode, bool _fullscreen)
{
	Com_Printf("\n" BAR_BOLD "\n");
	Com_Printf(S_COLOR_YELLOW"Initializing OpenGL\n");
	Com_Printf(BAR_BOLD"\n");

	const char* win_fs[] = { "Window", "Full Screen" };
	char monitorName[128] = { 0 }, monitorModel[16] = { 0 };

	DEVMODE dm;

	MONITOR_COUNTER = 0;
	EnumDisplayMonitors(nullptr, nullptr, enumerateMonitors, 0);

	ConsoleVariable* vid_monitor = Cvar_Get("vid_monitor", "0", CVAR_ARCHIVE);
	int cvm = clamp((int)vid_monitor->value - 1, 0, MAX_SUPPORTED_MONITORS);

	int idx = -1;
	for (int j = 0; j < MAX_SUPPORTED_MONITORS; j++)
	{
		MONITORS[j].name[0] = 0;
		for (int i = 1; i < 256; i++)
		{
			Q_sprintf(glw_state.desktopName, sizeof(glw_state.desktopName), "\\\\.\\Display%i", i);
			memset(&dm, 0, sizeof(dm));
			dm.dmSize = sizeof(dm);
			if (EnumDisplaySettings(glw_state.desktopName, ENUM_CURRENT_SETTINGS, &dm))
			{
				char tempMonitorName[128];
				glw_state.desktopPosX = dm.dmPosition.x;
				glw_state.desktopPosY = dm.dmPosition.y;
				if (GetDisplayMonitorInfo(tempMonitorName, monitorModel))
				{
					HDC _hdc = CreateDC(glw_state.desktopName, tempMonitorName, nullptr, nullptr);
					if (_hdc)
					{   /// monitor found, so compare positions in virtual desktop
						glw_state.desktopWidth = GetDeviceCaps(_hdc, HORZRES);
						glw_state.desktopHeight = GetDeviceCaps(_hdc, VERTRES);

						if (MONITORS[j].info.rcMonitor.left == glw_state.desktopPosX &&
							MONITORS[j].info.rcMonitor.top == glw_state.desktopPosY &&
							abs(MONITORS[j].info.rcMonitor.left - MONITORS[j].info.rcMonitor.right) == glw_state.desktopWidth &&
							abs(MONITORS[j].info.rcMonitor.top - MONITORS[j].info.rcMonitor.bottom) == glw_state.desktopHeight)
						{
							lstrcpy(MONITORS[j].name, monitorModel);
							if (j == cvm)
							{
								lstrcpy(monitorName, tempMonitorName);
								idx = i;
								i = 256;    /// break
							}
						}
					}

					DeleteDC(_hdc);
				}
			}
		}
	}

	if (idx == -1 || (int)vid_monitor->value <= 0 || (int)vid_monitor->value > MAX_SUPPORTED_MONITORS)
	{
		glw_state.desktopName[0] = 0;
		Com_Printf("\nUsing " S_COLOR_YELLOW "primary" S_COLOR_WHITE " monitor\n");

		glw_state.desktopPosX = 0;
		glw_state.desktopPosY = 0;
		
		HDC _hdc = GetDC(GetDesktopWindow());
		
		glw_state.desktopBitPixel = GetDeviceCaps(_hdc, BITSPIXEL);
		glw_state.desktopWidth = GetDeviceCaps(_hdc, HORZRES);
		glw_state.desktopHeight = GetDeviceCaps(_hdc, VERTRES);

		ReleaseDC(GetDesktopWindow(), _hdc);
	}
	else
	{
		Q_sprintf(glw_state.desktopName, sizeof(glw_state.desktopName), "\\\\.\\Display%i", idx);
		Com_Printf("\nCalling " S_COLOR_YELLOW "CreateDC" S_COLOR_WHITE "('" S_COLOR_GREEN "%s" S_COLOR_WHITE "','" S_COLOR_GREEN "%s" S_COLOR_WHITE "')\n", glw_state.desktopName, monitorName);
		memset(&dm, 0, sizeof(dm));
		dm.dmSize = sizeof(dm);
		EnumDisplaySettings(glw_state.desktopName, ENUM_CURRENT_SETTINGS, &dm);
		glw_state.desktopPosX = dm.dmPosition.x;
		glw_state.desktopPosY = dm.dmPosition.y;
		
		HDC _hdc = CreateDC(glw_state.desktopName, monitorName, nullptr, nullptr);
		glw_state.desktopBitPixel = GetDeviceCaps(_hdc, BITSPIXEL);
		glw_state.desktopWidth = GetDeviceCaps(_hdc, HORZRES);
		glw_state.desktopHeight = GetDeviceCaps(_hdc, VERTRES);
		DeleteDC(_hdc);

		if (MONITORS[cvm].name[0])
			Com_Printf("Using monitor " S_COLOR_GREEN "%i " S_COLOR_WHITE "(" S_COLOR_GREEN "%s" S_COLOR_WHITE ")\n", (int)vid_monitor->value, MONITORS[cvm].name);
		else
			Com_Printf("Using monitor " S_COLOR_GREEN "%i\n", (int)vid_monitor->value);

	}

	Com_Printf("\nAvailable monitors:\n");
	MONITOR_COUNTER = 0;
	EnumDisplayMonitors(nullptr, nullptr, printMonitorInfo, 0);

	Com_Printf("\n" BAR_LITE "\n\n");

	int width = 0, height = 0;
	if (!VID_GetModeInfo(&width, &height, _mode))
	{
		Com_Printf(S_COLOR_RED " invalid mode\n");
		return rserr_invalid_mode;
	}

	if (_mode == 0)
	{
		width = glw_state.desktopWidth;
		height = glw_state.desktopHeight;
	}

	Com_Printf("Setting mode " S_COLOR_YELLOW "%d" S_COLOR_WHITE ":" S_COLOR_YELLOW "[%ix%i]", _mode, width, height);

	if (width > glw_state.desktopWidth || height > glw_state.desktopHeight)
	{
		width = glw_state.desktopWidth;
		height = glw_state.desktopHeight;
		Com_Printf(S_COLOR_RED "\n*** INVALID RESOLUTION ***\n");
		Com_Printf(S_COLOR_MAGENTA "Set Current Desktop Resolution\n" S_COLOR_WHITE "%i" S_COLOR_GREEN "x" S_COLOR_WHITE "%i " S_COLOR_WHITE "%s\n", width, height, win_fs[_fullscreen]);
	}
	else
		Con_Printf(PRINT_ALL, " " S_COLOR_WHITE "%s\n", win_fs[_fullscreen]);

	// destroy the existing window
	if (glw_state.hWnd)
		GLimp_Shutdown();

	// Perform a 'change display settings' if necessary.
	if (_fullscreen)
	{
		Com_Printf("Attempting fullscreen... \n");

		memset(&dm, 0, sizeof(dm));
		dm.dmSize = sizeof(dm);

		dm.dmPelsWidth = width;
		dm.dmPelsHeight = height;
		dm.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT;

		/* display frequency */
		if (r_displayRefresh->value != 0)
		{
			gl_state.displayrefresh = r_displayRefresh->value;
			dm.dmDisplayFrequency = r_displayRefresh->value;
			dm.dmFields |= DM_DISPLAYFREQUENCY;
			Com_Printf("Refresh rate is " S_COLOR_GREEN "%d" S_COLOR_WHITE "hz\n", gl_state.displayrefresh);
		}
		else
		{
			HDC _hdc = GetDC(GetDesktopWindow());
			int displayref = GetDeviceCaps(_hdc, VREFRESH);
			dm.dmDisplayFrequency = displayref;
			dm.dmFields |= DM_DISPLAYFREQUENCY;
			Com_Printf("Using default refresh rate.\n");
			ReleaseDC(GetDesktopWindow(), _hdc);
		}

		// force set 32-bit color depth
		dm.dmBitsPerPel = 32;
		dm.dmFields |= DM_BITSPERPEL;

		Con_Printf(PRINT_ALL, "Changing display settings... ");
		int _cds_ret = 0;
		if (glw_state.desktopName[0])
			_cds_ret = ChangeDisplaySettingsEx(glw_state.desktopName, &dm, nullptr, CDS_FULLSCREEN, nullptr);
		else
			_cds_ret = ChangeDisplaySettings(&dm, CDS_FULLSCREEN);

		if (_cds_ret == DISP_CHANGE_SUCCESSFUL)
		{
			setVideoResolution(width, height);
			gl_state.fullscreen = true;

			Com_Printf("done.\n");

			if (!createWindow(width, height, true))
				return rserr_invalid_mode;

			return rserr_ok;
		}
		else
		{
			setVideoResolution(width, height);

			Com_Printf(S_COLOR_RED"failed!\n");

			Com_Printf("Assuming dual monitor setup... ");

			dm.dmPelsWidth = width * 2;
			dm.dmPelsHeight = height;
			dm.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT;

			// our first CDS failed, so maybe we're running on some weird dual monitor system 
			int _cds_ret = 0;
			if (glw_state.desktopName[0])
				_cds_ret = ChangeDisplaySettingsEx(glw_state.desktopName, &dm, nullptr, CDS_FULLSCREEN, nullptr);
			else
				_cds_ret = ChangeDisplaySettings(&dm, CDS_FULLSCREEN);

			if (_cds_ret != DISP_CHANGE_SUCCESSFUL)
			{
				Com_Printf(S_COLOR_RED"failed!\n");
				Com_Printf(S_COLOR_YELLOW"Setting windowed mode instead.\n");
				ChangeDisplaySettings(0, 0);

				setVideoResolution(width, height);
				gl_state.fullscreen = false;

				if (!createWindow(width, height, false))
					return rserr_invalid_mode;

				return rserr_invalid_fullscreen;
			}
			else
			{
				Com_Printf("done.\n");

				if (!createWindow(width, height, true))
					return rserr_invalid_mode;

				gl_state.fullscreen = true;
				return rserr_ok;
			}
		}
	}
	else
	{
		Com_Printf("Setting windowed mode\n");
		ChangeDisplaySettings(0, 0);

		setVideoResolution(width, height);
		gl_state.fullscreen = false;

		if (!createWindow(width, height, false))
			return rserr_invalid_mode;
	}

	return rserr_ok;
}


#pragma warning( push )
#pragma warning( disable : 4551)	// Silence the compiler warning about checking a function pointer.


/**
 * GLimp_Shutdown
 *
 * This routine does all OS specific shutdown procedures for the OpenGL
 * subsystem.
 */
void GLimp_Shutdown()
{
	if (!wglMakeCurrent(nullptr, nullptr))
		Com_Printf(S_COLOR_RED"GLimp_Shutdown(): wglMakeCurrent failed.\n");

	if (glw_state.hGLRC)
	{
		if (!wglDeleteContext(glw_state.hGLRC))
			Com_Printf(S_COLOR_RED"GLimp_Shutdown(): wglDeleteContext failed.\n");

		glw_state.hGLRC = nullptr;
	}
	if (glw_state.hDC)
	{
		if (!ReleaseDC(glw_state.hWnd, glw_state.hDC))
			Com_Printf(S_COLOR_RED"GLimp_Shutdown():: ReleaseDC failed.\n");

		glw_state.hDC = nullptr;
	}
	if (glw_state.hWnd)
	{
		DestroyWindow(glw_state.hWnd);
		glw_state.hWnd = nullptr;
	}

	UnregisterClass(WINDOW_CLASS_NAME, glw_state.hInstance);

	if (gl_state.fullscreen)
	{
		ChangeDisplaySettings(0, 0);
		gl_state.fullscreen = false;
	}
}

#pragma warning( pop ) 


/**
 * Returns true on success, false on failure.
 */
void GLW_InitExtensions()
{
	Com_Printf("\n" BAR_LITE "\n");
	Com_Printf(S_COLOR_GREEN"Initializing WGL\n");
	Com_Printf(BAR_LITE"\n\n");

	if (!wglewInit() == GLEW_OK)
	{
		Com_Printf(S_COLOR_RED"Unable to initialize WGL.");
		VID_Error(ERR_FATAL, "Unable to initialize WGL.");
	}

	if (WGLEW_EXT_swap_control)
		Com_Printf("Using WGL_EXT_swap_control.\n");
	else
		Com_Printf(S_COLOR_RED"WGL_EXT_swap_control not found.\n");

	
	gl_state.wgl_swap_control_tear = false;
	if (WGLEW_EXT_swap_control_tear)
	{
		Com_Printf("Using WGL_EXT_swap_control_tear.\n");
		gl_state.wgl_swap_control_tear = true;
	}
	else
	{
		Com_Printf(S_COLOR_RED"WGL_EXT_swap_control_tear not found.\n");
	}

	if (WGLEW_ARB_multisample)
	{
		if (r_multiSamples->value < 2)
			Com_Printf(S_COLOR_YELLOW "Ignoring WGL_ARB_multisample\n");
		else
			Com_Printf("Using WGL_ARB_multisample.\n");
	}

	if (WGLEW_ARB_create_context)
	{
		if (wglCreateContextAttribsARB)
		{
			Com_Printf("Using WGL_ARB_create_context.\n");
		}
		else
		{
			Com_Printf(S_COLOR_RED"WGL_ARB_create_context not found!\n");
			VID_Error(ERR_FATAL, "WGL_ARB_create_context not found!");
		}
	}

	if (WGLEW_ARB_create_context_profile)
		Com_Printf("Using WGL_ARB_create_context_profile.\n");
}


/**
 * Sets up a pixel format descriptor.
 */
bool initPixelFormat()
{
	Com_Printf("Setting pixel format... ");

	PIXELFORMATDESCRIPTOR _pfd;
	memset(&_pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));

	_pfd.cColorBits = 32;
	_pfd.cDepthBits = 24;
	_pfd.cStencilBits = 8;
	_pfd.cAlphaBits = 8;
	_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	_pfd.iLayerType = PFD_MAIN_PLANE;
	_pfd.iPixelType = PFD_TYPE_RGBA;
	_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	_pfd.nVersion = 1;

	gl_config.colorBits = _pfd.cColorBits;
	gl_config.depthBits = _pfd.cDepthBits;
	gl_config.stencilBits = _pfd.cAlphaBits;
	gl_config.alphaBits = _pfd.cAlphaBits;

	int _pixelFormat = ChoosePixelFormat(glw_state.hDC, &_pfd);
	if (_pixelFormat == 0)
	{
		Com_Printf(S_COLOR_RED "failed!\n");
		ReleaseDC(glw_state.hWnd, glw_state.hDC);
		glw_state.hDC = nullptr;
		return false;
	}

	// set the pixel format
	if (!SetPixelFormat(glw_state.hDC, _pixelFormat, &_pfd))
	{
		Com_Printf(S_COLOR_RED "failed!\n");
		ReleaseDC(glw_state.hWnd, glw_state.hDC);
		glw_state.hDC = nullptr;
		return false;
	}

	Com_Printf("done.\n");

	return true;
}


/**
 * 
 */
void setSamples()
{
	int samples = 0;
	if ((int)r_multiSamples->value == 1)
		samples = 0;
	else
		samples = (int)r_multiSamples->value;
	gl_config.samples = samples;
}


/**
 * 
 */
bool createDeviceContext()
{
	Com_Printf("Creating a device context... ");

	glw_state.hDC = GetDC(glw_state.hWnd);
	if (!glw_state.hDC)
	{
		Com_Printf(S_COLOR_RED "failed!\n");
		return false;
	}
	Com_Printf("done.\n");

	return true;
}


/**
 * 
 */
bool createContext()
{
	Com_Printf("Creating OpenGL " S_COLOR_GREEN "%i.%i" S_COLOR_WHITE " context... ", (int)r_glMajorVersion->value, (int)r_glMinorVersion->value);

	int	debugFlag = r_glDebugOutput->value ? WGL_CONTEXT_DEBUG_BIT_ARB : 0;
	int	contextMask = r_glCoreProfile->value ? WGL_CONTEXT_CORE_PROFILE_BIT_ARB : WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB;

	int	attribs[] =
	{
		WGL_CONTEXT_MAJOR_VERSION_ARB, static_cast<int>(r_glMajorVersion->value),
		WGL_CONTEXT_MINOR_VERSION_ARB, static_cast<int>(r_glMinorVersion->value),
		WGL_CONTEXT_FLAGS_ARB, debugFlag,
		WGL_CONTEXT_PROFILE_MASK_ARB, contextMask,
		0
	};

	if (wglCreateContextAttribsARB)
		glw_state.hGLRC = wglCreateContextAttribsARB(glw_state.hDC, nullptr, attribs);
	else if(&wglCreateContext != nullptr)
		glw_state.hGLRC = wglCreateContext(glw_state.hDC);
	else
		VID_Error(ERR_FATAL, "No available _wglCreateContext function.");

	if (!glw_state.hGLRC)
	{
		Com_Printf(S_COLOR_RED "failed!\n");

		ReleaseDC(glw_state.hWnd, glw_state.hDC);
		glw_state.hDC = nullptr;

		return false;
	}

	Com_Printf("done.\n");

	return true;
}


/**
 * 
 */
bool makeContextCurrent()
{
	// make it current
	Com_Printf("Setting context... ");

	if (!wglMakeCurrent(glw_state.hDC, glw_state.hGLRC))
	{
		Com_Printf(S_COLOR_RED "failed!\n");

		wglDeleteContext(glw_state.hGLRC);
		glw_state.hGLRC = nullptr;

		ReleaseDC(glw_state.hWnd, glw_state.hDC);
		glw_state.hDC = nullptr;

		return false;
	}

	Com_Printf("done.\n");

	return true;
}


/**
 * 
 */
bool GLW_InitDriver()
{
	if (!createDeviceContext())
		return false;
	
	setSamples();

	if (!initPixelFormat())
		return false;

	GLW_InitExtensions();
	
	if (!createContext())
		return false;

	if (!makeContextCurrent())
		return false;

	glGetIntegerv(GL_MAX_SAMPLES, &gl_config.maxSamples);
	gl_config.glMajorVersion = r_glMajorVersion->value;
	gl_config.glMinorVersion = r_glMinorVersion->value;

	return true;
}


/**
 * Responsible for doing a swapbuffers and possibly for other stuff
 * as yet to be determined.  Probably better not to make this a GLimp
 * function and instead do a call to GLimp_SwapBuffers.
 */

char scratch[100] = { '\0' };

void GLimp_EndFrame()
{
	/*
	int x = 0, y = 0;
	getMousePosition(&x, &y);

	memset(scratch, 0, sizeof(scratch));
	Q_sprintf(scratch, sizeof(scratch), "X(%i) Y(%i)", x, y);

	Set_FontShader(true);
	Draw_String(0, 0, scratch);
	Set_FontShader(false);
	*/

	if (!SwapBuffers(glw_state.hDC)) { VID_Error(ERR_FATAL, "GLimp_EndFrame(): SwapBuffers() failed!\n"); }

	r_newrefdef.time = Sys_Milliseconds() * 0.001f;
	ref_realtime = Sys_Milliseconds() * 0.0005f;
}


/**
 * 
 */
void GL_UpdateSwapInterval()
{
	if (r_vsync->modified)
		r_vsync->modified = false;

	if (gl_state.wgl_swap_control_tear)
	{
		if (wglSwapIntervalEXT)
		{
			if (r_vsync->value >= 2)
				wglSwapIntervalEXT(-1);
			else if (r_vsync->value >= 1)
				wglSwapIntervalEXT(1);
			else
				wglSwapIntervalEXT(0);
		}
	}
	else
		if (wglSwapIntervalEXT)
			wglSwapIntervalEXT(r_vsync->value);
}


/**
 * 
 */
void GLimp_AppActivate(bool active)
{
	if (active)
	{
		SetForegroundWindow(glw_state.hWnd);
		ShowWindow(glw_state.hWnd, SW_RESTORE);
	}
	else
	{
		if (r_fullScreen->value)
			ShowWindow(glw_state.hWnd, SW_MINIMIZE);
	}
}
