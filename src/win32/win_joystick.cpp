/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
// win_joystick.c -- windows 95 joystick code
// 02/21/97 JCB Added extended DirectInput code to support external controllers.

#include "../client/client.h"
#include "winquake.h"

#include "../common/string_util.h"


// ===============================================================================
// = DEFINES
// ===============================================================================
#define JOY_ABSOLUTE_AXIS	0x00000000		// control like a joystick
#define JOY_RELATIVE_AXIS	0x00000010		// control like a mouse, spinner, trackball
#define	JOY_MAX_AXES		6				// X, Y, Z, R, U, V
#define JOY_AXIS_X			0
#define JOY_AXIS_Y			1
#define JOY_AXIS_Z			2
#define JOY_AXIS_R			3
#define JOY_AXIS_U			4
#define JOY_AXIS_V			5


// ===============================================================================
// = ENUMERATORS
// ===============================================================================
enum _ControlList
{
	AxisNada = 0,
	AxisForward,
	AxisLook,
	AxisSide,
	AxisTurn,
	AxisUp
};


DWORD dwAxisFlags[JOY_MAX_AXES] =
{
	JOY_RETURNX,
	JOY_RETURNY,
	JOY_RETURNZ,
	JOY_RETURNR,
	JOY_RETURNU,
	JOY_RETURNV
};


// ===============================================================================
// = VARIABLES
// ===============================================================================

// None of these cvars are saved over a session.
// This means that advanced controller configuration needs to be executed each time.
// This avoids any problems with getting back to a default usage or when changing
// from one controller to another. This way at least something works.
ConsoleVariable*		joy_name;
ConsoleVariable*		joy_advanced;
ConsoleVariable*		joy_advaxisx;
ConsoleVariable*		joy_advaxisy;
ConsoleVariable*		joy_advaxisz;
ConsoleVariable*		joy_advaxisr;
ConsoleVariable*		joy_advaxisu;
ConsoleVariable*		joy_advaxisv;
ConsoleVariable*		joy_forwardthreshold;
ConsoleVariable*		joy_sidethreshold;
ConsoleVariable*		joy_pitchthreshold;
ConsoleVariable*		joy_yawthreshold;
ConsoleVariable*		joy_forwardsensitivity;
ConsoleVariable*		joy_sidesensitivity;
ConsoleVariable*		joy_pitchsensitivity;
ConsoleVariable*		joy_yawsensitivity;
ConsoleVariable*		joy_upthreshold;
ConsoleVariable*		joy_upsensitivity;
ConsoleVariable*		in_useJoystic;

int			joy_id;

DWORD		joy_flags;
DWORD		joy_numbuttons;
DWORD		joy_oldbuttonstate;
DWORD		joy_oldpovstate;

DWORD		dwAxisMap[JOY_MAX_AXES];
DWORD		dwControlMap[JOY_MAX_AXES];

PDWORD		pdwRawValue[JOY_MAX_AXES];

static JOYINFOEX ji;

bool		JOYSTIC_AVAILABLE;
bool		joy_advancedinit;
bool		joy_haspov;


// ===============================================================================
// = FUNCTIONS
// ===============================================================================

bool joystickAvailable()
{
	return JOYSTIC_AVAILABLE;
}


/**
 * Sets up joystick cvar's
 */
void initJoystick()
{
	// classic joystick stuff
	joy_name = Cvar_Get("joy_name", "joystick", 0);
	joy_advanced = Cvar_Get("joy_advanced", "0", 0);
	joy_advaxisx = Cvar_Get("joy_advaxisx", "0", 0);
	joy_advaxisy = Cvar_Get("joy_advaxisy", "0", 0);
	joy_advaxisz = Cvar_Get("joy_advaxisz", "0", 0);
	joy_advaxisr = Cvar_Get("joy_advaxisr", "0", 0);
	joy_advaxisu = Cvar_Get("joy_advaxisu", "0", 0);
	joy_advaxisv = Cvar_Get("joy_advaxisv", "0", 0);
	joy_forwardthreshold = Cvar_Get("joy_forwardthreshold", "0.15", 0);
	joy_sidethreshold = Cvar_Get("joy_sidethreshold", "0.15", 0);
	joy_upthreshold = Cvar_Get("joy_upthreshold", "0.15", 0);
	joy_pitchthreshold = Cvar_Get("joy_pitchthreshold", "0.15", 0);
	joy_yawthreshold = Cvar_Get("joy_yawthreshold", "0.15", 0);
	joy_forwardsensitivity = Cvar_Get("joy_forwardsensitivity", "-1", 0);
	joy_sidesensitivity = Cvar_Get("joy_sidesensitivity", "-1", 0);
	joy_upsensitivity = Cvar_Get("joy_upsensitivity", "-1", 0);
	joy_pitchsensitivity = Cvar_Get("joy_pitchsensitivity", "1", 0);
	joy_yawsensitivity = Cvar_Get("joy_yawsensitivity", "-1", 0);

	in_useJoystic = Cvar_Get("in_useJoystic", "0", CVAR_ARCHIVE);
}


/**
 * 
 */
void IN_StartupJoystick()
{
	initJoystick();

	int numdevs;
	JOYCAPS jc;
	MMRESULT mmr;

	if (xInputActive()) // found xInput controller
		return;

	// assume no joystick
	JOYSTIC_AVAILABLE = false;

	// abort startup if user requests no joystick
	ConsoleVariable* cv = Cvar_Get("in_initjoy", "1", CVAR_NOSET);
	if (!cv->value)
		return;

	Com_Printf("\n========== Init Joystick ==========\n\n");

	// verify joystick driver is present
	if ((numdevs = joyGetNumDevs()) == 0)
	{
		Com_Printf("" S_COLOR_RED "joystick not found -- driver not present\n");
		return;
	}

	// cycle through the joystick ids for the first valid one
	for (joy_id = 0; joy_id<numdevs; joy_id++)
	{
		memset(&ji, 0, sizeof(ji));
		ji.dwSize = sizeof(ji);
		ji.dwFlags = JOY_RETURNCENTERED;

		if ((mmr = joyGetPosEx(joy_id, &ji)) == JOYERR_NOERROR)
			break;
	}

	// abort startup if we didn't find a valid joystick
	if (mmr != JOYERR_NOERROR)
	{
		Com_Printf("..." S_COLOR_YELLOW "Joystick Not Found " S_COLOR_WHITE "(%x)\n", mmr);
		Com_Printf("\n-----------------------------------\n\n");
		return;
	}

	// get the capabilities of the selected joystick
	// abort startup if command fails
	memset(&jc, 0, sizeof(jc));
	if ((mmr = joyGetDevCaps(joy_id, &jc, sizeof(jc))) != JOYERR_NOERROR)
	{
		Com_Printf("..." S_COLOR_RED "Invalid Joystick Capabilities (%x)\n", mmr);
		Com_Printf("\n-----------------------------------\n\n");
		return;
	}

	// save the joystick's number of buttons and POV status
	joy_numbuttons = jc.wNumButtons;
	joy_haspov = (jc.wCaps & JOYCAPS_HASPOV) != 0;

	// old button and POV states default to no buttons pressed
	joy_oldbuttonstate = joy_oldpovstate = 0;

	// mark the joystick as available and advanced initialization not completed
	// this is needed as cvars are not available during initialization

	JOYSTIC_AVAILABLE = true;
	joy_advancedinit = false;

	Com_Printf("Found: " S_COLOR_GREEN "%s\n", jc.szPname);
	Com_Printf("Num Buttons: " S_COLOR_GREEN "%i / %i\n", jc.wNumButtons, jc.wMaxButtons);
	Com_Printf("Axis: " S_COLOR_GREEN "%i / %i\n", jc.wNumAxes, jc.wMaxAxes);
	Com_Printf("\n-----------------------------------\n\n");
}


/*
===========
RawValuePointer
===========
*/
PDWORD RawValuePointer(int axis)
{
	switch (axis)
	{
	case JOY_AXIS_X:
		return &ji.dwXpos;
	case JOY_AXIS_Y:
		return &ji.dwYpos;
	case JOY_AXIS_Z:
		return &ji.dwZpos;
	case JOY_AXIS_R:
		return &ji.dwRpos;
	case JOY_AXIS_U:
		return &ji.dwUpos;
	default:
	case JOY_AXIS_V:
		return &ji.dwVpos;
	}
}


/*
===========
Joy_AdvancedUpdate_f
===========
*/
void Joy_AdvancedUpdate_f()
{
	// called once by IN_ReadJoystick and by user whenever an update is needed
	// cvars are now available
	int	i;
	DWORD dwTemp;

	if (xInputActive())
		return;

	// initialize all the maps
	for (i = 0; i < JOY_MAX_AXES; i++)
	{
		dwAxisMap[i] = AxisNada;
		dwControlMap[i] = JOY_ABSOLUTE_AXIS;
		pdwRawValue[i] = RawValuePointer(i);
	}

	if (joy_advanced->value == 0.0)
	{
		// default joystick initialization
		// 2 axes only with joystick control
		dwAxisMap[JOY_AXIS_X] = AxisTurn;
		// dwControlMap[JOY_AXIS_X] = JOY_ABSOLUTE_AXIS;
		dwAxisMap[JOY_AXIS_Y] = AxisForward;
		// dwControlMap[JOY_AXIS_Y] = JOY_ABSOLUTE_AXIS;
	}
	else
	{
		if (joy_name->string == "joystick")
		{
			// notify user of advanced controller
			Com_Printf("\n%s configured\n\n", joy_name->string);
		}

		// advanced initialization here
		// data supplied by user via joy_axisn cvars
		dwTemp = (DWORD)joy_advaxisx->value;
		dwAxisMap[JOY_AXIS_X] = dwTemp & 0x0000000f;
		dwControlMap[JOY_AXIS_X] = dwTemp & JOY_RELATIVE_AXIS;
		dwTemp = (DWORD)joy_advaxisy->value;
		dwAxisMap[JOY_AXIS_Y] = dwTemp & 0x0000000f;
		dwControlMap[JOY_AXIS_Y] = dwTemp & JOY_RELATIVE_AXIS;
		dwTemp = (DWORD)joy_advaxisz->value;
		dwAxisMap[JOY_AXIS_Z] = dwTemp & 0x0000000f;
		dwControlMap[JOY_AXIS_Z] = dwTemp & JOY_RELATIVE_AXIS;
		dwTemp = (DWORD)joy_advaxisr->value;
		dwAxisMap[JOY_AXIS_R] = dwTemp & 0x0000000f;
		dwControlMap[JOY_AXIS_R] = dwTemp & JOY_RELATIVE_AXIS;
		dwTemp = (DWORD)joy_advaxisu->value;
		dwAxisMap[JOY_AXIS_U] = dwTemp & 0x0000000f;
		dwControlMap[JOY_AXIS_U] = dwTemp & JOY_RELATIVE_AXIS;
		dwTemp = (DWORD)joy_advaxisv->value;
		dwAxisMap[JOY_AXIS_V] = dwTemp & 0x0000000f;
		dwControlMap[JOY_AXIS_V] = dwTemp & JOY_RELATIVE_AXIS;
	}

	// compute the axes to collect from DirectInput
	joy_flags = JOY_RETURNCENTERED | JOY_RETURNBUTTONS | JOY_RETURNPOV;
	for (i = 0; i < JOY_MAX_AXES; i++)
	{
		if (dwAxisMap[i] != AxisNada)
		{
			joy_flags |= dwAxisFlags[i];
		}
	}
}

void IN_Commands()
{
	int		i, key_index;
	DWORD	buttonstate, povstate;

	if (xInputActive())
		return;

	if (!JOYSTIC_AVAILABLE)
	{
		return;
	}

	// loop through the joystick buttons
	// key a joystick event or auxillary event for higher number buttons for each state change
	buttonstate = ji.dwButtons;
	for (i = 0; i < joy_numbuttons; i++)
	{
		if ((buttonstate & (1 << i)) && !(joy_oldbuttonstate & (1 << i)))
		{
			key_index = (i < 4) ? K_JOY1 : K_AUX1;
			Key_Event(key_index + i, true, 0);
		}

		if (!(buttonstate & (1 << i)) && (joy_oldbuttonstate & (1 << i)))
		{
			key_index = (i < 4) ? K_JOY1 : K_AUX1;
			Key_Event(key_index + i, false, 0);
		}
	}
	joy_oldbuttonstate = buttonstate;

	if (joy_haspov)
	{
		// convert POV information into 4 bits of state information
		// this avoids any potential problems related to moving from one
		// direction to another without going through the center position
		povstate = 0;
		if (ji.dwPOV != JOY_POVCENTERED)
		{
			if (ji.dwPOV == JOY_POVFORWARD)
				povstate |= 0x01;
			if (ji.dwPOV == JOY_POVRIGHT)
				povstate |= 0x02;
			if (ji.dwPOV == JOY_POVBACKWARD)
				povstate |= 0x04;
			if (ji.dwPOV == JOY_POVLEFT)
				povstate |= 0x08;
		}
		// determine which bits have changed and key an auxillary event for each change
		for (i = 0; i < 4; i++)
		{
			if ((povstate & (1 << i)) && !(joy_oldpovstate & (1 << i)))
			{
				Key_Event(K_AUX29 + i, true, 0);
			}

			if (!(povstate & (1 << i)) && (joy_oldpovstate & (1 << i)))
			{
				Key_Event(K_AUX29 + i, false, 0);
			}
		}
		joy_oldpovstate = povstate;
	}
}


/*
===============
IN_ReadJoystick
===============
*/
bool IN_ReadJoystick()
{

	memset(&ji, 0, sizeof(ji));
	ji.dwSize = sizeof(ji);
	ji.dwFlags = joy_flags;

	if (joyGetPosEx(joy_id, &ji) == JOYERR_NOERROR)
	{
		return true;
	}
	else
	{
		// read error occurred
		// turning off the joystick seems too harsh for 1 read error,\
						// but what should be done?
// Com_Printf ("IN_ReadJoystick: no response\n");
// joy_avail = false;
		return false;
	}
}


/*
===========
IN_JoyMove
===========
*/
void IN_JoyMove(usercmd_t *cmd)
{
	float	speed, aspeed;
	float	fAxisValue;
	int		i;

	if (xInputActive())
	{
		IN_ControllerMove(cmd);
		return;
	}

	// complete initialization if first time in
	// this is needed as cvars are not available at initialization time
	if (joy_advancedinit != true)
	{
		Joy_AdvancedUpdate_f();
		joy_advancedinit = true;
	}

	// verify joystick is available and that the user wants to use it
	if (!joystickAvailable() || !in_useJoystic->value)
	{
		return;
	}

	// collect the joystick data, if possible
	if (IN_ReadJoystick() != true)
	{
		return;
	}

	if ((in_speed.state & 1) ^ (int)cl_run->value)
		speed = 2;
	else
		speed = 1;
	aspeed = speed * cls.frametime;

	// loop through the axes
	for (i = 0; i < JOY_MAX_AXES; i++)
	{
		// get the floating point zero-centered, potentially-inverted data for the current axis
		fAxisValue = (float)*pdwRawValue[i];
		// move centerpoint to zero
		fAxisValue -= 32768.0;

		// convert range from -32768..32767 to -1..1 
		fAxisValue /= 32768.0;

		switch (dwAxisMap[i])
		{
		case AxisForward:
			if ((joy_advanced->value == 0.0) && looking())
			{
				// user wants forward control to become look control
				if (fabs(fAxisValue) > joy_pitchthreshold->value)
				{
					// if mouse invert is on, invert the joystick pitch value
					// only absolute control support here (joy_advanced is false)
					if (m_pitch->value < 0.0)
					{
						cl.viewangles_PITCH -= (fAxisValue * joy_pitchsensitivity->value * (cl.refdef.fov_x / 90.0)) * aspeed * cl_pitchspeed->value;
					}
					else
					{
						cl.viewangles_PITCH += (fAxisValue * joy_pitchsensitivity->value * (cl.refdef.fov_x / 90.0)) * aspeed * cl_pitchspeed->value;
					}
				}
			}
			else
			{
				// user wants forward control to be forward control
				if (fabs(fAxisValue) > joy_forwardthreshold->value)
				{
					cmd->forwardmove += (fAxisValue * joy_forwardsensitivity->value) * speed * cl_forwardspeed->value;
				}
			}
			break;

		case AxisSide:
			if (fabs(fAxisValue) > joy_sidethreshold->value)
			{
				cmd->sidemove += (fAxisValue * joy_sidesensitivity->value) * speed * cl_sidespeed->value;
			}
			break;

		case AxisUp:
			if (fabs(fAxisValue) > joy_upthreshold->value)
			{
				cmd->upmove += (fAxisValue * joy_upsensitivity->value) * speed * cl_upspeed->value;
			}
			break;

		case AxisTurn:
			if ((in_strafe.state & 1) || (lookstrafe->value && looking()))
			{
				// user wants turn control to become side control
				if (fabs(fAxisValue) > joy_sidethreshold->value)
				{
					cmd->sidemove -= (fAxisValue * joy_sidesensitivity->value) * speed * cl_sidespeed->value;
				}
			}
			else
			{
				// user wants turn control to be turn control
				if (fabs(fAxisValue) > joy_yawthreshold->value)
				{
					if (dwControlMap[i] == JOY_ABSOLUTE_AXIS)
					{
						cl.viewangles_YAW += (fAxisValue * joy_yawsensitivity->value * (cl.refdef.fov_x / 90.0)) * aspeed * cl_yawspeed->value;
					}
					else
					{
						cl.viewangles_YAW += (fAxisValue * joy_yawsensitivity->value) * speed * 180.0;
					}

				}
			}
			break;

		case AxisLook:
			if (looking())
			{
				if (fabs(fAxisValue) > joy_pitchthreshold->value)
				{
					// pitch movement detected and pitch movement desired by user
					if (dwControlMap[i] == JOY_ABSOLUTE_AXIS)
					{
						cl.viewangles_PITCH += (fAxisValue * joy_pitchsensitivity->value * (cl.refdef.fov_x / 90.0)) * aspeed * cl_pitchspeed->value;
					}
					else
					{
						cl.viewangles_PITCH += (fAxisValue * joy_pitchsensitivity->value * (cl.refdef.fov_x / 90.0)) * speed * 180.0;

					}
				}
			}
			break;

		default:
			break;
		}
	}
}