#pragma once

#include "../common/shared.h"

// ===============================================================================
// = Publicly exposed functions and values for Joystick Input.
// ===============================================================================

bool joystickAvailable();

void IN_StartupJoystick();
void IN_JoyMove(usercmd_t *cmd);

void Joy_AdvancedUpdate_f();
