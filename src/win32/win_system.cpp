/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
#include <GL/glew.h>
#include "../common/common.h"
#include "conproc.h"
#include "winquake.h"

#include <iostream>

#include <errno.h>
#include <float.h>
#include <fcntl.h>
#include <stdio.h>
#include <direct.h>
#include <io.h>
#include <conio.h>


#define	MAX_NUM_ARGVS			128				/**<  */

bool APP_ACTIVE					= false;		/**< Indicates whether or not the application is active. */
bool APP_MINIMIZED				= false;		/**< Indicates whether or not the application is minimized. */

static HANDLE H_INPUT			= nullptr;		/**<  */
static HANDLE H_OUTPUT			= nullptr;		/**<  */

unsigned int SYS_MSG_TIME		= 0;			/**<  */
unsigned int SYS_FRAME_TIME		= 0;			/**<  */

int ARGC						= 0;			/**< Number of arguments passed to the game. */
char* ARGV[MAX_NUM_ARGVS]		= { '\0' };		/**< Array containing arguments passed to the game. */

static HINSTANCE GAME_LIBRARY	= nullptr;		/**< Handle to the active game DLL. */
HINSTANCE G_HINSTANCE			= nullptr;		/**< Handle to the game's window. */

static char console_text[256]	= { '\0' };		/**<  */
static int console_textlen		= 0;			/**<  */


/**
 * \fixme	More global variable nonsense. Replace this with a function call
 *			instead of an external import. Also need to find an appropriate place
 *			to call this. The Filesystem seems an inappropriate place to do this.
 */
extern std::string GAME_DLL_PATH;


/**
 * Gets application's activation status.
 */
bool appActive()
{
	return APP_ACTIVE;
}


/**
 * Sets application's activation status.
 */
void setAppActive(bool _b)
{
	APP_ACTIVE = _b;
}


/**
 * Gets whether or not the application is minimized.
 */
bool appMinimized()
{
	return APP_MINIMIZED;
}


/**
 * Sets whether or not the application is minimized.
 */
void setAppMinimized(bool _b)
{
	APP_MINIMIZED = _b;
}


/**
 * Sets the \c HINSTANCE for the game.
 */
void setMainInstance(HINSTANCE _h)
{
	G_HINSTANCE = _h;
}


/**
 * Gets the \c HINSTANCE for the game.
 */
HINSTANCE getMainInstance()
{
	return G_HINSTANCE;
}


/**
 * 
 */
unsigned int SystemMessageTime(void)
{
	return SYS_MSG_TIME;
}


/**
 * 
 */
void SetSystemMessageTime(unsigned int _t)
{
	SYS_MSG_TIME = _t;
}


/**
 * 
 */
unsigned int SystemFrameTime(void)
{
	return SYS_FRAME_TIME;
}


/**
 * 
 */
void SetSystemFrameTime(unsigned int _t)
{
	SYS_FRAME_TIME = _t;
}


/**
 * 
 */
void Sys_Quit()
{
	timeEndPeriod(1);

	CL_Shutdown();
	Qcommon_Shutdown();

	if (dedicated && dedicated->value)
	{
		FreeConsole();
	}

	// shut down QHOST hooks if necessary
	DeinitConProc();

	exit(0);
}


/**
 *
 */
void Sys_Error(const char* error, ...)
{
	va_list argptr;

	char text[1024] = { '\0' };

	CL_Shutdown();
	Qcommon_Shutdown();

	va_start(argptr, error);
	vsnprintf(text, sizeof(text), error, argptr);
	va_end(argptr);

	std::cout << text << std::endl;

	MessageBox(GetActiveWindow(), text, "Engine Error", 0);
	DeinitConProc();

	throw std::runtime_error(text);
}


/**
 * 
 */
void Sys_Init()
{
	timeBeginPeriod(1);

	if (dedicated->value)
	{
		if (!AllocConsole())
		{
			Sys_Error("Couldn't create dedicated server console.");
		}

		H_INPUT = GetStdHandle(STD_INPUT_HANDLE);
		H_OUTPUT = GetStdHandle(STD_OUTPUT_HANDLE);

		// let QHOST hook in
		InitConProc(ARGC, ARGV);
	}
}


/**
 * 
 */
char* Sys_ConsoleInput()
{
	INPUT_RECORD recs[1024] = { '\0' };
	int dummy = 0, numread = 0, numevents = 0;

	if (!dedicated || !dedicated->value)
		return nullptr;

	for (;;)
	{
		if (!GetNumberOfConsoleInputEvents(H_INPUT, reinterpret_cast<LPDWORD>(&numevents)))
		{
			Sys_Error("Error getting # of console events");
		}

		if (numevents <= 0)
		{
			break;
		}

		if (!ReadConsoleInput(H_INPUT, recs, 1, reinterpret_cast<LPDWORD>(&numread)))
		{
			Sys_Error("Error reading console input");
		}

		if (numread != 1)
		{
			Sys_Error("Couldn't read console input");
		}

		if (recs[0].EventType == KEY_EVENT)
		{
			if (!recs[0].Event.KeyEvent.bKeyDown)
			{
				int ch = recs[0].Event.KeyEvent.uChar.AsciiChar;

				switch (ch)
				{
				case '\r':
					WriteFile(H_OUTPUT, "\r\n", 2, reinterpret_cast<LPDWORD>(&dummy), nullptr);

					if (console_textlen)
					{
						console_text[console_textlen] = 0;
						console_textlen = 0;
						return console_text;
					}
					break;

				case '\b':
					if (console_textlen)
					{
						console_textlen--;
						WriteFile(H_OUTPUT, "\b \b", 3, reinterpret_cast<LPDWORD>(&dummy), nullptr);
					}
					break;

				default:
					if (ch >= ' ')
					{
						if (console_textlen < sizeof(console_text) - 2)
						{
							WriteFile(H_OUTPUT, &ch, 1, reinterpret_cast<LPDWORD>(&dummy), nullptr);
							console_text[console_textlen] = ch;
							console_textlen++;
						}
					}
					break;
				}
			}
		}
	}

	return nullptr;
}


/**
 * Print text to the dedicated console
 */
void Sys_ConsoleOutput(char* string)
{
	char text[256] = { '\0' };

	if (!dedicated || !dedicated->value) return;

	int dummy = 0;
	if (console_textlen)
	{
		text[0] = '\r';
		memset(&text[1], ' ', console_textlen);
		text[console_textlen + 1] = '\r';
		text[console_textlen + 2] = 0;
		WriteFile(H_OUTPUT, text, console_textlen + 2, reinterpret_cast<LPDWORD>(&dummy), nullptr);
	}

	WriteFile(H_OUTPUT, string, strlen(string), reinterpret_cast<LPDWORD>(&dummy), nullptr);

	if (console_textlen)
		WriteFile(H_OUTPUT, console_text, console_textlen, reinterpret_cast<LPDWORD>(&dummy), nullptr);
}


/**
 * Send Key_Event calls
 */
void Sys_SendKeyEvents()
{
	MSG msg;
	while (PeekMessage(&msg, nullptr, 0, 0, PM_NOREMOVE))
	{
		if (!GetMessage(&msg, nullptr, 0, 0))
			Sys_Quit();

		SetSystemMessageTime(msg.time);
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	// grab frame time 
	SetSystemFrameTime(timeGetTime());	// FIXME: should this be at start?
}


/**
 * 
 */
char* Sys_GetClipboardData()
{
	char* data = nullptr;

	if (OpenClipboard(nullptr) != 0)
	{
		HANDLE hClipboardData = GetClipboardData(CF_TEXT);
		if (hClipboardData)
		{
			char* cliptext = static_cast<char*>(GlobalLock(hClipboardData));
			if (cliptext)
			{
				data = static_cast<char*>(malloc(GlobalSize(hClipboardData) + 1));
				if (data)
				{
					strcpy(data, cliptext);
					GlobalUnlock(hClipboardData);
				}
			}
		}
		CloseClipboard();
	}

	return data;
}


/**
 * 
 */
void Sys_AppActivate()
{
	ShowWindow(getMainWindowHandle(), SW_RESTORE);
	SetForegroundWindow(getMainWindowHandle());
}


/**
 *
 */
void Sys_UnloadGame()
{
	if (GAME_LIBRARY == nullptr)
	{
		throw std::runtime_error("Trying to unload a library without first having loaded a library.");
	}

	if (!FreeLibrary(GAME_LIBRARY))
	{
		Com_Error(ERR_FATAL, "FreeLibrary():: Failed to unload game library.");
	}

	GAME_LIBRARY = nullptr;
}


/**
 * Loads the game DLL
 */
void* Sys_GetGameAPI(void* parms)
{
	if (GAME_LIBRARY)
	{
		Com_Error(ERR_FATAL, "Sys_GetGameAPI() called without Sys_UnloadingGame().");
	}

	GAME_LIBRARY = LoadLibrary(GAME_DLL_PATH.c_str());
	if (GAME_LIBRARY)
	{
		Com_Printf("LoadLibrary (%s)\n", GAME_DLL_PATH.c_str());
	}

	void* (*GetGameAPI)(void*) = (void*(__cdecl*)(void*)) GetProcAddress(GAME_LIBRARY, "GetGameAPI");
	if (!GetGameAPI)
	{
		Sys_UnloadGame();
		return nullptr;
	}

	return GetGameAPI(parms);
}


/**
 * 
 */
void ParseCommandLine(LPSTR lpCmdLine)
{
	ARGC = 1;
	ARGV[0] = "exe";

	while (*lpCmdLine && (ARGC < MAX_NUM_ARGVS))
	{
		while (*lpCmdLine && ((*lpCmdLine <= 32) || (*lpCmdLine > 126)))
			lpCmdLine++;

		if (*lpCmdLine)
		{
			ARGV[ARGC] = lpCmdLine;
			ARGC++;

			while (*lpCmdLine && ((*lpCmdLine > 32) && (*lpCmdLine <= 126)))
				lpCmdLine++;

			if (*lpCmdLine)
			{
				*lpCmdLine = 0;
				lpCmdLine++;
			}

		}
	}
}

#include <iostream>
#include <fstream>

/**
 * 
 */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	std::ofstream out("log.txt");
	std::streambuf *coutbuf = std::cout.rdbuf();
	std::cout.rdbuf(out.rdbuf());

	std::cout << "ROGUE ARENA " << VERSION << std::endl << std::endl;

	int time = 0, oldtime = 0, newtime = 0;

	/* previous instances do not exist in Win32 */
	if (hPrevInstance)
	{
		return 0;
	}

	setMainInstance(hInstance);
	hidePointer();

	//try
	{
		ParseCommandLine(lpCmdLine);
		Qcommon_Init(ARGC, ARGV);
		oldtime = Sys_Milliseconds();

		srand(Sys_Milliseconds());

		/* main window message loop */
		for (;;)
		{
			// if at a full screen console, don't update unless needed
			if (APP_MINIMIZED || (dedicated && dedicated->value))
			{
				Sleep(1);
			}

			MSG msg;
			while (PeekMessage(&msg, nullptr, 0, 0, PM_NOREMOVE))
			{
				if (!GetMessage(&msg, nullptr, 0, 0))
				{
					Com_Quit();
				}

				SetSystemMessageTime(msg.time);
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

			do
			{
				newtime = Sys_Milliseconds();
				time = newtime - oldtime;
			} while (time < 1);

			_controlfp(_PC_24, _MCW_PC);
			Qcommon_Frame(time);
			oldtime = newtime;
		}
	}
	//catch (std::exception& e)
	{
		//std::cout << e.what() << std::endl;
		//MessageBoxA(nullptr, e.what(), "Exception", 0);
	}
	//catch (...)
	{
		//std::cout << "Unknown error." << std::endl;
		//MessageBoxA(nullptr, "Unknown error.", "Exception", 0);
	}

	std::cout << "Program termination" << std::endl;

	// never gets here
	return TRUE;
}
