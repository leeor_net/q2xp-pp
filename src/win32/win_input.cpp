/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "winquake.h"

#include "../client/client.h"
#include "../common/string_util.h"


// ============================================================
// = VARIABLES
// ============================================================
ConsoleVariable	*m_inversion;
ConsoleVariable	*v_centermove;
ConsoleVariable	*v_centerspeed;

int mouse_buttons;
int mouse_oldbuttonstate;
int originalmouseparms[3], newmouseparms[3] = { 0, 0, 1 };
int window_center_x, window_center_y;
int mouse_x, mouse_y, old_mouse_x, old_mouse_y, mx_accum, my_accum;
int old_x, old_y;

POINT current_pos;
RECT window_rect;

bool MOUSE_CAPTURED = false;
bool in_appactive = false;
bool mlooking = false;
bool restore_spi = false;
bool mouseinitialized = false;
bool mouseparmsvalid = false;

extern ConsoleVariable* r_fullScreen;


static POINT mousePt;

static int __MOUSE_X = 0;
static int __MOUSE_Y = 0;


/**
 * 
 */
void showPointer()
{
	while (ShowCursor(TRUE) < 0);
}


/**
 * 
 */
void hidePointer()
{
	while (ShowCursor(FALSE) >= 0);
}


/**
 * Gets mouse coordinates.
 */
void getMousePosition(int* x, int* y)
{
	if (x) { (*x) = __MOUSE_X; }
	if (y) { (*y) = __MOUSE_Y; }
}



/**
 * Gets the center point of the game window.
 */
int windowCenterX()
{
	return window_center_x;
}


/**
 * Gets the center point of the game window.
 */
int windowCenterY()
{
	return window_center_y;
}


/**
 * \fixme find a better name for this.
 */
bool looking()
{
	return mlooking;
}


/**
 * 
 */
bool mouseCaptured()
{
	return MOUSE_CAPTURED;
}


/**
 * 
 */
void IN_MLookDown()
{
	mlooking = true;
}


/**
 * 
 */
void IN_MLookUp()
{
	mlooking = false;
	if (!freelook->value && lookspring->value)
		IN_CenterView();
}


/**
 * Called when the window gains focus or changes in some way.
 */
void IN_ActivateMouse()
{
	if (mouseCaptured())
		return;

	/// Berserker's fix
	int width = GetSystemMetrics(SM_CXSCREEN);
	int height = GetSystemMetrics(SM_CYSCREEN);

	GetWindowRect(getMainWindowHandle(), &window_rect);
	if (window_rect.left < 0)
		window_rect.left = 0;
	if (window_rect.top < 0)
		window_rect.top = 0;
	if (window_rect.right >= width)
		window_rect.right = width - 1;
	if (window_rect.bottom >= height - 1)
		window_rect.bottom = height - 1;

	window_center_x = (window_rect.right + window_rect.left) / 2;
	window_center_y = (window_rect.top + window_rect.bottom) / 2;

	old_x = window_center_x;
	old_y = window_center_y;

	SetCursorPos(windowCenterX(), windowCenterY());
	SetCapture(getMainWindowHandle());
	ClipCursor(&window_rect);

	MOUSE_CAPTURED = true;
	hidePointer();
}


/**
 * Called when the window loses focus
 */
void IN_DeactivateMouse()
{
	if (!mouseCaptured())
		return;

	ClipCursor(nullptr);
	ReleaseCapture();

	MOUSE_CAPTURED = false;
	showPointer();
}


/**
 *
 */
void IN_StartupMouse()
{
	if (!Cvar_Get("in_initmouse", "1", CVAR_NOSET)->value)
	{
		return;
	}

	mouseinitialized = true;
	mouseparmsvalid = SystemParametersInfo(SPI_GETMOUSE, 0, originalmouseparms, 0) != FALSE;
	mouse_buttons = 5;
}


/**
 * 
 */
bool FindRawDevices()
{
	PRAWINPUTDEVICELIST g_pRawInputDeviceList;
	size_t nDevices;

	Com_Printf("====== Initialize Raw Input Devices ======\n\n");

	// Get Number of devices attached
	if (GetRawInputDeviceList(nullptr, &nDevices, sizeof(RAWINPUTDEVICELIST)) != 0)
	{
		Com_Printf("\n" S_COLOR_RED "No Raw Input devices attached\n");
		return false;
	}
	else
		Com_Printf("" S_COLOR_YELLOW "... Found " S_COLOR_GREEN "%i" S_COLOR_YELLOW " RAW input devices.\n", nDevices);

	// Create list large enough to hold all RAWINPUTDEVICE structs
	if ((g_pRawInputDeviceList = (PRAWINPUTDEVICELIST)calloc(nDevices, sizeof(RAWINPUTDEVICELIST))) == nullptr)
	{
		Com_Printf("" S_COLOR_RED "Error mallocing RAWINPUTDEVICELIST\n");
		return false;
	}
	// Now get the data on the attached devices
	if (GetRawInputDeviceList(g_pRawInputDeviceList, &nDevices, sizeof(RAWINPUTDEVICELIST)) == -1)
	{
		Com_Printf("" S_COLOR_RED "1Error from GetRawInputDeviceList\n");
		free(g_pRawInputDeviceList);
		return false;
	}

	for (size_t i = 0; i < nDevices; i++)
	{
		if (g_pRawInputDeviceList[i].dwType == RIM_TYPEMOUSE)
		{
			size_t nchars = 300;
			TCHAR deviceName[300];
			if (GetRawInputDeviceInfo(g_pRawInputDeviceList[i].hDevice, RIDI_DEVICENAME, deviceName, &nchars) > 0)
			{
				Com_DPrintf("Device[%d]:\n handle=0x%x\n name = %s\n\n", i, g_pRawInputDeviceList[i].hDevice, deviceName);
			}

			RID_DEVICE_INFO dinfo;
			size_t sizeofdinfo = sizeof(dinfo);
			dinfo.cbSize = sizeofdinfo;
			if (GetRawInputDeviceInfo(g_pRawInputDeviceList[i].hDevice, RIDI_DEVICEINFO, &dinfo, &sizeofdinfo) > 0)
			{
				if (dinfo.dwType == RIM_TYPEMOUSE)
				{
					RID_DEVICE_INFO_MOUSE *pMouseInfo = &dinfo.mouse;
					Com_DPrintf("ID = 0x%x\n", pMouseInfo->dwId);
					Com_DPrintf("Number of buttons = %i\n", pMouseInfo->dwNumberOfButtons);
					Com_DPrintf("Sample Rate = %i\n", pMouseInfo->dwSampleRate);
					Com_DPrintf("Has Horizontal Wheel: %s\n", pMouseInfo->fHasHorizontalWheel ? "Yes" : "No");
				}
			}
		}
	}

	free(g_pRawInputDeviceList);

	Com_Printf("\n" BAR_LITE "\n");

	return true;
}


/**
 * 
 */
void IN_Init()
{
	// mouse variables
	m_inversion = Cvar_Get("m_inversion", "0", CVAR_ARCHIVE);

	// centering
	v_centermove = Cvar_Get("v_centermove", "0.15", 0);
	v_centerspeed = Cvar_Get("v_centerspeed", "500", 0);

	// xinput stuff
	Cmd_AddCommand("+mlook", IN_MLookDown);
	Cmd_AddCommand("-mlook", IN_MLookUp);
	Cmd_AddCommand("joy_advancedupdate", Joy_AdvancedUpdate_f);

	FindRawDevices();
	IN_StartupXInput();
	IN_StartupJoystick();
}

/**
 * 
 */
void IN_Shutdown()
{
	IN_DeactivateMouse();
	IN_ShutDownXinput();
}


/**
 * Called when the main window gains or loses focus. The window may have
 * been destroyed and recreated between a deactivate and an activate.
 */
void IN_Activate(bool active)
{
	in_appactive = active;
	MOUSE_CAPTURED = !active;		// force a new window check or turn off
}


/**
 * Called every frame, even if not generating commands
 */
void IN_Frame()
{
	GetCursorPos(&mousePt);
	if (ScreenToClient(getMainWindowHandle(), &mousePt))
	{
		__MOUSE_X = clamp(mousePt.x, 0, videoWidth());
		__MOUSE_Y = clamp(mousePt.y, 0, videoHeight());
	}

	if (!in_appactive)
	{
		IN_DeactivateMouse();
		return;
	}

	if (!cl.refresh_prepped || cls.key_dest == KEY_CONSOLE || cls.key_dest == KEY_MENU)
	{
		// temporarily deactivate if in fullscreen
		//if (Cvar_VariableValue("r_fullScreen") == 0)
		//{
			IN_DeactivateMouse();
			return;
		//}
	}

	IN_ActivateMouse();
}


/**
 *
 */
void IN_Move(usercmd_t *cmd)
{
	IN_ToggleXInput();

	if (appActive())
		IN_JoyMove(cmd);
}


/**
 * 
 */
void IN_ClearStates()
{
	mx_accum = 0;
	my_accum = 0;
	mouse_oldbuttonstate = 0;
}
