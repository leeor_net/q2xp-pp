/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

// Main windowed and fullscreen graphics interface module. This module
// is used for both the software and OpenGL rendering versions of the
// Quake refresh engine.

#include <assert.h>
#include <float.h>
#include <stdio.h>

#include "winquake.h"
#include "xinput.h"

#include "../client/client.h"
#include "../client/music.h"
#include "../client/snd_loc.h" //for experimental OpenAL suspend feature.

#include "../common/common.h"

#include <windowsx.h>

// ===============================================================================
// = DEFINES
// ===============================================================================
#ifndef MK_XBUTTON3
# define MK_XBUTTON3         0x0080
# define MK_XBUTTON4         0x0100
#endif

#ifndef MK_XBUTTON5
# define MK_XBUTTON5         0x0200
#endif

/// Added by Willow: new mouse support
#ifndef HID_USAGE_PAGE_GENERIC
#define HID_USAGE_PAGE_GENERIC	((USHORT) 0x01)
#endif
#ifndef HID_USAGE_GENERIC_MOUSE
#define HID_USAGE_GENERIC_MOUSE	((USHORT) 0x02)
#endif
///#define mouse_buttons	5

#ifndef WM_MOUSEWHEEL
#define WM_MOUSEWHEEL (WM_MOUSELAST+1)  // message that will be supported by the OS
#endif


// ===============================================================================
// = STRUCT'S
// ===============================================================================
/**
 *
 */
typedef struct vidmode_s
{
	const char* description;
	int         width, height;
	int         mode;
} vidmode_t;


// ===============================================================================
// = VARIABLES
// ===============================================================================
static size_t MSH_MOUSEWHEEL = 0;

HWND CL_HWND = nullptr; // Main window handle for life of program

/**
 * Console variables that we need to access from this module
 * 
 * \fixme	Find a better way to access these.
 */
extern ConsoleVariable* r_brightness;		/**< */
extern ConsoleVariable* vid_ref;				/**< Name of Refresh DLL loaded */
extern ConsoleVariable* r_fullScreen;		/**< */
extern ConsoleVariable* r_customWidth;		/**< */
extern ConsoleVariable* r_customHeight;		/**< */
extern ConsoleVariable* m_inversion;			/**< */


/**
 * \fixme	These are accessible within this module but are also defined in menu.c. Find
 *			a way to unify these.
 */
ConsoleVariable* win_noalttab = nullptr;		/**<  */
ConsoleVariable* vid_xpos = nullptr;			/**< X coordinate of window position */
ConsoleVariable* vid_ypos = nullptr;			/**< Y coordinate of window position */


// Global variables used internally by this module
///\fixme Wrap these in get/set type functions.

static Point2D_t	VIDEO_RESOLUTION;		/**< global video state; used by other modules */
static bool		REFLIB_ACTIVE = false;	/**<  */
static bool		ALT_TAB_DISABLED;		/**<  */


static bool __MOUSE_BUTTON_LEFT_DOWN = false;
static bool __MOUSE_BUTTON_RIGHT_DOWN = false;


void pollMouseButtonState(bool* left, bool* right)
{
	if (left) { *left = __MOUSE_BUTTON_LEFT_DOWN; }
	if (right) { *right = __MOUSE_BUTTON_RIGHT_DOWN; }
}


/**
 * 
 */
const Point2D_t* videoResolution()
{
	return &VIDEO_RESOLUTION;
}


void setVideoResolution(int width, int height)
{
	VIDEO_RESOLUTION.x = width;
	VIDEO_RESOLUTION.y = height;
}


/**
 * Gets the width of the screen.
 */
int videoWidth()
{
	return VIDEO_RESOLUTION.x;
}


/**
 * Gets the height of the screen.
 */
int videoHeight()
{
	return VIDEO_RESOLUTION.y;
}


/**
 * Gets the center point of the screen.
 */
int videoCenterScreenX()
{
	return VIDEO_RESOLUTION.x / 2;
}


/**
 * Gets the center point of the screen.
 */
int videoCenterScreenY()
{
	return VIDEO_RESOLUTION.y / 2;
}


/**
 * 
 */
bool refreshLibraryActive()
{
	return REFLIB_ACTIVE;
}


/**
 * 
 */
bool altTabDisabled()
{
	return ALT_TAB_DISABLED;
}


/**
 * Video Modes
 * 
 * \fixme	Have this automatically generated with actual
 *			supported modes from the system.
 */
static vidmode_t vid_modes[] = {
	{ "Desktop",	-1, -1, 0 },		// desktop native
										// generic screen
	{ "1024x768",	1024, 768, 1 },     // 4:3
	{ "1152x864",	1152, 864, 2 },     // 4:3
	{ "1280x1024",	1280, 1024, 3 },    // 5:4
	{ "1600x1200",	1600, 1200, 4 },    // 4:3
	{ "2048x1536",	2048, 1536, 5 },    // 4:3
										// wide screen
	{ "1280x720",	1280, 720, 6 },     // 16:9 720p HDTV
	{ "1280x800",	1280, 800, 7 },     // 16:10
	{ "1366x768",	1366, 768, 8 },     // 16:9, plasma & LCD TV
	{ "1440x900",	1440, 900, 9 },     // 16:10
	{ "1600x900",	1600, 900, 10 },    // 16:9 TV
	{ "1680x1050",	1680, 1050, 11 },   // 16:10
	{ "1920x1080",	1920, 1080, 12 },   // 16:9 1080p full HDTV
	{ "1920x1200",	1920, 1200, 13 },   // 16:10
	{ "2560x1600",	2560, 1600, 14 },   // 16:10
	{ "Custom",		-1, -1, 15 }		// custom
};


/**
 * 
 */
byte scantokey[128] =
{
	//  0           1       2       3       4       5       6       7
	//  8           9       A       B       C       D       E       F
	0, 27, '1', '2', '3', '4', '5', '6',
	'7', '8', '9', '0', '-', '=', K_BACKSPACE, 9, // 0
	'q', 'w', 'e', 'r', 't', 'y', 'u', 'i',
	'o', 'p', '[', ']', 13, K_CTRL, 'a', 's',      // 1
	'd', 'f', 'g', 'h', 'j', 'k', 'l', ';',
	'\'', '`', K_SHIFT, '\\', 'z', 'x', 'c', 'v',      // 2
	'b', 'n', 'm', ',', '.', '/', K_SHIFT, '*',
	K_ALT, ' ', 0, K_F1, K_F2, K_F3, K_F4, K_F5,   // 3
	K_F6, K_F7, K_F8, K_F9, K_F10, K_PAUSE, 0, K_HOME,
	K_UPARROW, K_PGUP, K_KP_MINUS, K_LEFTARROW, K_KP_5, K_RIGHTARROW, K_KP_PLUS, K_END, //4
	K_DOWNARROW, K_PGDN, K_INS, K_DEL, 0, 0, 0, K_F11,
	K_F12, 0, 0, 0, 0, 0, 0, 0,        // 5
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,        // 6
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0         // 7
};


HWND getMainWindowHandle()
{
	return CL_HWND;
}


/**
 * 
 */
static void WIN_DisableAltTab()
{
	if (altTabDisabled())
		return;

	BOOL old;
	SystemParametersInfo(SPI_SCREENSAVERRUNNING, 1, &old, 0);

	ALT_TAB_DISABLED = true;
}


/**
 * 
 */
static void WIN_EnableAltTab()
{
	if (altTabDisabled())
	{
		BOOL old;
		SystemParametersInfo(SPI_SCREENSAVERRUNNING, 0, &old, 0);
		ALT_TAB_DISABLED = false;
	}
}


/** 
 * \todo Update documentation to explain how to properly use it.
 * 
 * \fixme	This function doesn't belong here. Find a better place.
 */
void Con_Printf(int print_level, char* fmt, ...)
{
	va_list argptr;
	char msg[MAXPRINTMSG];
	static bool	inupdate;

	va_start(argptr, fmt);
	vsnprintf(msg, sizeof(msg), fmt, argptr);
	va_end(argptr);

	if (print_level == PRINT_ALL)
	{
		Com_Printf("%s", msg);
	}
	else if (print_level == PRINT_DEVELOPER)
	{
		Com_DPrintf("%s", msg);
	}
	else if (print_level == PRINT_ALERT)
	{
		MessageBox(0, msg, "PRINT_ALERT", MB_ICONWARNING);
		OutputDebugString(msg);
	}
}


/**
 * Eliminate this function, totally not needed.
 * 
 * \see Com_Error
 * 
 * \fixme	This function doesn't belong here. Find a better place.
 */
void VID_Error(int err_level, char* fmt, ...)
{
	va_list argptr;
	char msg[MAXPRINTMSG];
	static bool inupdate;

	va_start(argptr, fmt);
	vsnprintf(msg, sizeof(msg), fmt, argptr);
	va_end(argptr);

	Com_Error(err_level, "%s", msg);
}


/**
 * 
 */
int MapKey(int key)
{
	int result;
	int modified = (key >> 16) & 255;
	bool is_extended = false;

	if (modified > 127)
		return 0;

	if (key & (1 << 24))
		is_extended = true;

	result = scantokey[modified];

	if (!is_extended)
	{
		switch (result)
		{
		case K_HOME:
			return K_KP_HOME;
		case K_UPARROW:
			return K_KP_UPARROW;
		case K_PGUP:
			return K_KP_PGUP;
		case K_LEFTARROW:
			return K_KP_LEFTARROW;
		case K_RIGHTARROW:
			return K_KP_RIGHTARROW;
		case K_END:
			return K_KP_END;
		case K_DOWNARROW:
			return K_KP_DOWNARROW;
		case K_PGDN:
			return K_KP_PGDN;
		case K_INS:
			return K_KP_INS;
		case K_DEL:
			return K_KP_DEL;
		default:
			return result;
		}
	}
	else
	{
		switch (result)
		{
		case 0x0D:
			return K_KP_ENTER;
		case 0x2F:
			return K_KP_SLASH;
		case 0xAF:
			return K_KP_PLUS;
		}
		return result;
	}
}


/**
 * 
 */
void AppActivate(BOOL fActive, BOOL minimize)
{
	extern alConfig_t alConfig;

	setAppMinimized(minimize != FALSE);	// yuck

	Key_ClearStates();

	// we don't want to act like we're active if we're minimized
	// minimize/restore mouse-capture on demand
	if (!fActive || appMinimized())
	{
		setAppActive(false);
		IN_Activate(false);
		Music_Pause();
		if (win_noalttab->value)
			WIN_EnableAltTab();

		if (alConfig.hALC)
			alcSuspendContext(alConfig.hALC); //willow: Have no success??
	}
	else
	{
		setAppActive(true);
		IN_Activate(true);
		Music_Resume();
		if (win_noalttab->value)
			WIN_DisableAltTab();

		if (alConfig.hALC) alcProcessContext(alConfig.hALC); //willow: Have no success??
	}
}


/**
 * main window procedure
 */
LONG WINAPI MainWndProc(HWND hWnd, size_t uMsg, WPARAM wParam, LPARAM lParam)
{
	if (uMsg == WM_INPUT)
	{
		size_t dwSize = 40;
		static BYTE lpb[40];

		if (GetRawInputData((HRAWINPUT)lParam, RID_INPUT, lpb, &dwSize, sizeof(RAWINPUTHEADER)) != -1)
		{
			PRAWINPUT raw = (RAWINPUT*)lpb;

			if (raw->header.dwType == RIM_TYPEMOUSE)
			{
				if (raw->data.mouse.lLastX || raw->data.mouse.lLastY)
				{
					float sens = 1.0f;
					if(sensitivity)
						sens = sensitivity->value * cl.refdef.fov_x * 0.00066f;

					if (raw->data.mouse.lLastX)
						cl.viewangles_YAW -= sens * raw->data.mouse.lLastX;

					if (raw->data.mouse.lLastY)
					{
						if (m_inversion && m_inversion->value)
							cl.viewangles_PITCH -= sens * raw->data.mouse.lLastY;
						else
							cl.viewangles_PITCH += sens * raw->data.mouse.lLastY;
					}

					if (!(r_fullScreen && r_fullScreen->value) && mouseCaptured())
						SetCursorPos(windowCenterX(), windowCenterY());
				}

				// perform button actions
				if (raw->data.mouse.usButtonFlags)
				{
					//RI_MOUSE_LEFT_BUTTON
					if (raw->data.mouse.usButtonFlags & RI_MOUSE_LEFT_BUTTON_DOWN)
					{
						Key_Event(K_MOUSE1, true, SystemMessageTime());
						__MOUSE_BUTTON_LEFT_DOWN = true;
					}
					if (raw->data.mouse.usButtonFlags & RI_MOUSE_LEFT_BUTTON_UP)
					{
						Key_Event(K_MOUSE1, false, SystemMessageTime());
						__MOUSE_BUTTON_LEFT_DOWN = false;
					}

					//RI_MOUSE_RIGHT_BUTTON
					if (raw->data.mouse.usButtonFlags & RI_MOUSE_RIGHT_BUTTON_DOWN)
					{
						Key_Event(K_MOUSE2, true, SystemMessageTime());
						__MOUSE_BUTTON_RIGHT_DOWN = true;
					}
					if (raw->data.mouse.usButtonFlags & RI_MOUSE_RIGHT_BUTTON_UP)
					{
						Key_Event(K_MOUSE2, false, SystemMessageTime());
						__MOUSE_BUTTON_RIGHT_DOWN = false;
					}

					//RI_MOUSE_MIDDLE_BUTTON
					if (raw->data.mouse.usButtonFlags & RI_MOUSE_MIDDLE_BUTTON_DOWN) Key_Event(K_MOUSE3, true, SystemMessageTime());
					if (raw->data.mouse.usButtonFlags & RI_MOUSE_MIDDLE_BUTTON_UP) Key_Event(K_MOUSE3, false, SystemMessageTime());
					//RI_MOUSE_BUTTON_4
					if (raw->data.mouse.usButtonFlags & RI_MOUSE_BUTTON_4_DOWN) Key_Event(K_MOUSE4, true, SystemMessageTime());
					if (raw->data.mouse.usButtonFlags & RI_MOUSE_BUTTON_4_UP) Key_Event(K_MOUSE4, false, SystemMessageTime());
					//RI_MOUSE_BUTTON_5
					if (raw->data.mouse.usButtonFlags & RI_MOUSE_BUTTON_5_DOWN) Key_Event(K_MOUSE5, true, SystemMessageTime());
					if (raw->data.mouse.usButtonFlags & RI_MOUSE_BUTTON_5_UP) Key_Event(K_MOUSE5, false, SystemMessageTime());
					//RI_MOUSE_WHEEL
					if (raw->data.mouse.usButtonFlags & RI_MOUSE_WHEEL)
					{
						int i = (short)raw->data.mouse.usButtonData; //wheel delta, signed
						if (i < 0)
						{
							Key_Event(K_MWHEELDOWN, true, SystemMessageTime());
							Key_Event(K_MWHEELDOWN, false, SystemMessageTime());
						}
						else if (i > 0)
						{
							Key_Event(K_MWHEELUP, true, SystemMessageTime());
							Key_Event(K_MWHEELUP, false, SystemMessageTime());
						}
					}
				}
				return 0;
			}
			else
			{
				return DefRawInputProc(&raw, 1, sizeof(RAWINPUTHEADER)) ? 1 : 0;
			}
		}
		return 1;
	} //WM_INPUT


	switch (uMsg)
	{
	case WM_SYSKEYDOWN:
		if (wParam == 13)		// Alt+Enter
		{
			if (r_fullScreen)
				Cvar_SetValue("r_fullScreen", !r_fullScreen->value);

			return 0;
		}
		// fall through
	case WM_KEYDOWN:
		Key_Event(MapKey(lParam), true, SystemMessageTime());
		break;

	case WM_SYSKEYUP:
	case WM_KEYUP:
		Key_Event(MapKey(lParam), false, SystemMessageTime());
		break;

	case WM_HOTKEY:
		return 0;
		break;

	case WM_DESTROY:
		// let sound and input know about this?
		CL_HWND = nullptr;
		break;

	case WM_ACTIVATE:
	{
		// KJB: Watch this for problems in fullscreen modes with Alt-tabbing.
		bool fActive = LOWORD(wParam) != 0;	// yuck
		bool fMinimized = HIWORD(wParam) != 0; // yuck
		AppActivate(fActive != WA_INACTIVE, fMinimized);
		if (REFLIB_ACTIVE)
			GLimp_AppActivate(!(fActive == WA_INACTIVE));
	}
	break;

	case WM_MOVE:
		if (!r_fullScreen->value)
		{
			RECT r;
			int xPos = (short)LOWORD(lParam);    // horizontal position 
			int yPos = (short)HIWORD(lParam);    // vertical position 

			r.left = 0;
			r.top = 0;
			r.right = 1;
			r.bottom = 1;

			int style = GetWindowLong(hWnd, GWL_STYLE);
			AdjustWindowRect(&r, style, FALSE);

			Cvar_SetValue("vid_xpos", xPos + r.left);
			Cvar_SetValue("vid_ypos", yPos + r.top);
			vid_xpos->modified = false;
			vid_ypos->modified = false;

			if (appActive())
				IN_Activate(true);
		}
		break;

	case WM_SYSCOMMAND:
		if (wParam == SC_SCREENSAVE || wParam == SC_MONITORPOWER)
			return 0;

		return DefWindowProc(hWnd, uMsg, wParam, lParam);
		break;

	case WM_CREATE:
		CL_HWND = hWnd;

		RAWINPUTDEVICE Rid[1];
		Rid[0].usUsagePage = HID_USAGE_PAGE_GENERIC;
		Rid[0].usUsage = HID_USAGE_GENERIC_MOUSE;
		Rid[0].dwFlags =
			(r_fullScreen && r_fullScreen->value) ?
			//If set, the mouse button click does not activate the other window
			RIDEV_CAPTUREMOUSE
			//If set, this enables the caller to receive the input even when the caller is not in the foreground.
			//Note that hwndTarget must be specified
			| RIDEV_INPUTSINK
			//If set, this prevents any devices specified by usUsagePage or usUsage from generating legacy messages.
			//This is only for the mouse and keyboard.
			| RIDEV_NOLEGACY
			:
		0;
		Rid[0].hwndTarget = CL_HWND;
		RegisterRawInputDevices(Rid, 1, sizeof(RAWINPUTDEVICE));

		break;

	case WM_CLOSE:
		return 0;
	}

	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}


/**
 * Console command to re-start the video mode and refresh DLL. We do this
 * simply by setting the modified flag for the vid_ref variable, which will
 * cause the entire video mode and refresh DLL to be reset on the next frame.
 */
void VID_Restart_f()
{
	vid_ref->modified = true;
}


/**
 * 
 */
void VID_Front_f()
{
	SetWindowLong(CL_HWND, GWL_EXSTYLE, WS_EX_TOPMOST);
	SetForegroundWindow(CL_HWND);
}


/**
 * 
 */
unsigned int numVideoModes()
{
	return sizeof(vid_modes) / sizeof(vid_modes[0]);
}


/**
 * 
 */
bool VID_GetModeInfo(int *width, int *height, int mode)
{
	if (mode < 0 || mode >= numVideoModes()) return false;

	if (mode == 15)
	{
		*width = r_customWidth->value;
		*height = r_customHeight->value;
	}
	else
	{
		*width = vid_modes[mode].width;
		*height = vid_modes[mode].height;
	}

	return true;
}


/**
 * 
 */
void VID_UpdateWindowPosAndSize(int x, int y)
{
	RECT r;
	int		style;
	int		w, h;

	r.left = 0;
	r.top = 0;
	r.right = VIDEO_RESOLUTION.x;
	r.bottom = VIDEO_RESOLUTION.y;

	style = GetWindowLong(CL_HWND, GWL_STYLE);
	AdjustWindowRect(&r, style, FALSE);

	w = r.right - r.left;
	h = r.bottom - r.top;

	MoveWindow(CL_HWND, vid_xpos->value, vid_ypos->value, w, h, TRUE);
}


/**
 * 
 */
void VID_NewWindow(int width, int height)
{
	VIDEO_RESOLUTION.x = width;
	VIDEO_RESOLUTION.y = height;

	cl.force_refdef = true;		// can't use a paused refdef
}


/**
 * 
 */
void VID_FreeReflib()
{
	//	memset (&re, 0, sizeof(re));
	REFLIB_ACTIVE = false;
}


/**
 * 
 */
bool VID_StartRefresh()
{
	if (REFLIB_ACTIVE)
	{
		R_Shutdown();
		VID_FreeReflib();
	}

	Swap_Init();

	if (!R_Init(getMainInstance(), MainWndProc))
	{
		R_Shutdown();
		VID_FreeReflib();
		return false;
	}

	REFLIB_ACTIVE = true;
	return true;
}


/**
 * This function gets called once just before drawing each frame, and it's sole purpose in life
 * is to check to see if any of the video mode parameters have changed, and if they have to
 * update the rendering DLL and/or video mode to match.
 */
void VID_CheckChanges()
{
	if (win_noalttab->modified)
	{
		if (win_noalttab->value)
			WIN_DisableAltTab();
		else
			WIN_EnableAltTab();

		win_noalttab->modified = false;
	}

	while (vid_ref->modified)
	{
		cl.force_refdef = true;		// can't use a paused refdef
		S_StopAllSounds();

		vid_ref->modified = false;
		r_fullScreen->modified = true;
		cl.refresh_prepped = 0.0f;
		cls.disable_screen = 1.0f;
		CL_ClearDecals();

		if (!VID_StartRefresh())
			Com_Error(ERR_FATAL, "Error during initialization video");

		cls.disable_screen = false;
		CL_InitImages();
	}

	if (vid_xpos->modified || vid_ypos->modified)
	{
		if (!r_fullScreen->value) VID_UpdateWindowPosAndSize(vid_xpos->value, vid_ypos->value);

		vid_xpos->modified = false;
		vid_ypos->modified = false;
	}
}


/**
 * 
 */
void VID_Init()
{
	/* Create the video variables so we know how to start the graphics drivers */
	vid_ref = Cvar_Get("vid_ref", "gl", CVAR_ARCHIVE);
	
	vid_xpos = Cvar_Get("vid_xpos", "0", CVAR_ARCHIVE);
	vid_ypos = Cvar_Get("vid_ypos", "0", CVAR_ARCHIVE);
	
	r_fullScreen = Cvar_Get("r_fullScreen", "1", CVAR_ARCHIVE);
	r_customWidth = Cvar_Get("r_customWidth", "1024", CVAR_ARCHIVE);
	r_customHeight = Cvar_Get("r_customHeight", "768", CVAR_ARCHIVE);
	win_noalttab = Cvar_Get("win_noalttab", "0", CVAR_ARCHIVE);

	/* Add some console commands that we want to handle */
	Cmd_AddCommand("vid_restart", VID_Restart_f);
	Cmd_AddCommand("vid_front", VID_Front_f);

	/* Start the graphics mode and load refresh DLL */
	VID_CheckChanges();
}


/**
 * 
 */
void VID_Shutdown()
{
	if (REFLIB_ACTIVE)
	{
		R_Shutdown();
		VID_FreeReflib();
	}
}
