#pragma once

#include <GL/glew.h>

#include <windows.h>

#include "../common/shared.h"
#include "../ref_gl/r_local.h"

typedef struct
{
	HINSTANCE	hInstance;
	void		*wndproc;

	HDC			hDC;			// handle to device context
	HWND		hWnd;			// handle to window
	HGLRC		hGLRC;			// handle to GL rendering context

	HINSTANCE	hinstOpenGL;	// HINSTANCE for the OpenGL library

	int			desktopWidth;
	int			desktopHeight;
	int			desktopBitPixel;

	int			desktopPosX;
	int			desktopPosY;

	int			virtualX;
	int			virtualY;

	int			virtualWidth;
	int			virtualHeight;

	int			borderWidth;
	int			borderHeight;

	bool		pixelFormatSet;

	char		desktopName[32];	// no monitor specified if empty, drawing on primary display
} glwstate_t;

glwstate_t* GLW_State();

void GLimp_AppActivate(bool active);
void GLimp_EndFrame();
rserr_t GLimp_SetMode(int mode, bool fullscreen);
void GLimp_Shutdown();
