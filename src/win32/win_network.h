#pragma once

#include "../common/net_chan.h"

void NET_Init();
void NET_Shutdown();

void NET_Config(bool multiplayer);

bool NET_GetPacket(netsrc_t sock, netadr_t * net_from, sizebuf_t * net_message);
void NET_SendPacket(netsrc_t sock, int length, void *data, netadr_t to);

bool NET_CompareAdr(netadr_t a, netadr_t b);
bool NET_CompareBaseAdr(netadr_t a, netadr_t b);
bool NET_IsLocalAddress(netadr_t adr);
char* NET_AdrToString(netadr_t a);
bool NET_StringToAdr(char* s, netadr_t * a);
void NET_Sleep(int msec);
