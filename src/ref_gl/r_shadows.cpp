/*
Copyright (C) 2004-2013 Quake2xp Team, Berserker.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// shadows.c: shadow functions
#include "r_local.h"

#include "r_math.h"
#include "r_program.h"

int			num_shadow_surfaces, shadowTimeStamp;
vec4_t		s_lerped[MAX_VERTS];
vec3_t		vcache[MAX_MAP_TEXINFO * MAX_POLY_VERT];
vec4_t		vcache4[MAX_VERTS * 3];
size_t		icache[MAX_MAP_TEXINFO * MAX_POLY_VERT];
msurface_t	*shadow_surfaces[MAX_MAP_FACES];
char		triangleFacingLight[MAX_INDICES / 3];
void		*dynamicVertex, *dynamicIndex;
/*
=====================
Alias Shadow Volumes
=====================
*/

void R_MarkShadowTriangles (dmd2header_t *paliashdr, dmd2vertex_t *tris, vec3_t lightOrg) {

	vec3_t	r_triangleNormals[MAX_INDICES / 3];
	vec3_t	temp, dir0, dir1;
	int		i;
	float	f;
	float	*v0, *v1, *v2;

	for (i = 0; i < paliashdr->num_tris; i++, tris++) {

		v0 = (float*)s_lerped[tris->index_xyz[0]];
		v1 = (float*)s_lerped[tris->index_xyz[1]];
		v2 = (float*)s_lerped[tris->index_xyz[2]];

		//Calculate shadow volume triangle normals
		VectorSubtract (v0, v1, dir0);
		VectorSubtract (v2, v1, dir1);

		CrossProduct (dir0, dir1, r_triangleNormals[i]);

		// Find front facing triangles
		VectorSubtract (lightOrg, v0, temp);
		f = DotProduct (temp, r_triangleNormals[i]);

		triangleFacingLight[i] = f > 0;
	}
}

void BuildShadowVolumeTriangles(dmd2header_t * hdr, vec3_t lightOrg) {
	dmd2vertex_t		*ot, *tris;
	neighbors_t		*neighbours;
	vec4_t			v0, v1, v2;
	dmd2frame_t	*frame;
	dtrivertx_t		*verts;
	int				i, j, numVerts = 0, id = 0;

	frame = (dmd2frame_t *)((byte *)hdr + hdr->ofs_frames + CURRENT_ENTITY->frame * hdr->framesize);
	verts = frame->verts;
	ot = tris = (dmd2vertex_t *)((unsigned char* )hdr + hdr->ofs_tris);

	R_MarkShadowTriangles(hdr, tris, lightOrg);

	for (i = 0, tris = ot, neighbours = CURRENT_MODEL->neighbours; i < hdr->num_tris; i++, tris++, neighbours++) {

		if (!triangleFacingLight[i])
			continue;

		if (neighbours->n[0] < 0 || !triangleFacingLight[neighbours->n[0]]) {

			for (j = 0; j < 3; j++) {
				v0[j] = s_lerped[tris->index_xyz[1]][j];
				v1[j] = s_lerped[tris->index_xyz[0]][j];
			}

			//  transforms points with w = 1 normally and sends points with w = 0 to infinity away from the light.
			VA_SetElem4(vcache4[numVerts + 0], v0[0], v0[1], v0[2], v0[3] = 1.0);
			VA_SetElem4(vcache4[numVerts + 1], v1[0], v1[1], v1[2], v1[3] = 1.0);
			VA_SetElem4(vcache4[numVerts + 2], v1[0], v1[1], v1[2], v1[3] = 0.0);
			VA_SetElem4(vcache4[numVerts + 3], v0[0], v0[1], v0[2], v0[3] = 0.0);
			
			icache[id++] = numVerts + 0;
			icache[id++] = numVerts + 1;
			icache[id++] = numVerts + 3;
			icache[id++] = numVerts + 3;
			icache[id++] = numVerts + 1;
			icache[id++] = numVerts + 2;
			numVerts += 4;
		}

		if (neighbours->n[1] < 0 || !triangleFacingLight[neighbours->n[1]]) {

			for (j = 0; j < 3; j++) {
				v0[j] = s_lerped[tris->index_xyz[2]][j];
				v1[j] = s_lerped[tris->index_xyz[1]][j];
			}

			VA_SetElem4(vcache4[numVerts + 0], v0[0], v0[1], v0[2], v0[3] = 1.0);
			VA_SetElem4(vcache4[numVerts + 1], v1[0], v1[1], v1[2], v1[3] = 1.0);
			VA_SetElem4(vcache4[numVerts + 2], v1[0], v1[1], v1[2], v1[3] = 0.0);
			VA_SetElem4(vcache4[numVerts + 3], v0[0], v0[1], v0[2], v0[3] = 0.0);

			icache[id++] = numVerts + 0;
			icache[id++] = numVerts + 1;
			icache[id++] = numVerts + 3;
			icache[id++] = numVerts + 3;
			icache[id++] = numVerts + 1;
			icache[id++] = numVerts + 2;
			numVerts += 4;
		}

		if (neighbours->n[2] < 0 || !triangleFacingLight[neighbours->n[2]]) {

			for (j = 0; j < 3; j++) {
				v0[j] = s_lerped[tris->index_xyz[0]][j];
				v1[j] = s_lerped[tris->index_xyz[2]][j];
			}

			VA_SetElem4(vcache4[numVerts + 0], v0[0], v0[1], v0[2], v0[3] = 1.0);
			VA_SetElem4(vcache4[numVerts + 1], v1[0], v1[1], v1[2], v1[3] = 1.0);
			VA_SetElem4(vcache4[numVerts + 2], v1[0], v1[1], v1[2], v1[3] = 0.0);
			VA_SetElem4(vcache4[numVerts + 3], v0[0], v0[1], v0[2], v0[3] = 0.0);

			icache[id++] = numVerts + 0;
			icache[id++] = numVerts + 1;
			icache[id++] = numVerts + 3;
			icache[id++] = numVerts + 3;
			icache[id++] = numVerts + 1;
			icache[id++] = numVerts + 2;
			numVerts += 4;
		}
	}

	// build shadows caps
	for (i = 0, tris = ot; i < hdr->num_tris; i++, tris++) {
		if (!triangleFacingLight[i])
			continue;

		for (j = 0; j < 3; j++) {
			v0[j] = s_lerped[tris->index_xyz[0]][j];
			v1[j] = s_lerped[tris->index_xyz[1]][j];
			v2[j] = s_lerped[tris->index_xyz[2]][j];
		}

		VA_SetElem4(vcache4[numVerts + 0], v0[0], v0[1], v0[2], v0[3] = 1.0);
		VA_SetElem4(vcache4[numVerts + 1], v1[0], v1[1], v1[2], v1[3] = 1.0);
		VA_SetElem4(vcache4[numVerts + 2], v2[0], v2[1], v2[2], v2[3] = 1.0);


		icache[id++] = numVerts + 0;
		icache[id++] = numVerts + 1;
		icache[id++] = numVerts + 2;
		numVerts += 3;

		// rear cap (with flipped winding order)

		for (j = 0; j < 3; j++) {
			v0[j] = s_lerped[tris->index_xyz[0]][j];
			v1[j] = s_lerped[tris->index_xyz[1]][j];
			v2[j] = s_lerped[tris->index_xyz[2]][j];
		}

		VA_SetElem4(vcache4[numVerts + 0], v0[0], v0[1], v0[2], v0[3] = 0.0);
		VA_SetElem4(vcache4[numVerts + 1], v1[0], v1[1], v1[2], v1[3] = 0.0);
		VA_SetElem4(vcache4[numVerts + 2], v2[0], v2[1], v2[2], v2[3] = 0.0);

		icache[id++] = numVerts + 2;
		icache[id++] = numVerts + 1;
		icache[id++] = numVerts + 0;
		numVerts += 3;
	}

	glBufferSubData(GL_ARRAY_BUFFER, 0, numVerts * sizeof(vec4_t), vcache4);
	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, id * sizeof(size_t), icache);

	glDrawElements (GL_TRIANGLES, id, GL_UNSIGNED_INT, nullptr);
}

bool R_EntityInLightBounds () {

	int		i;
	vec3_t	mins, maxs;

	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		return false;

	if (CURRENT_ENTITY->flags & (RF_SHELL_HALF_DAM | RF_SHELL_GREEN | RF_SHELL_RED |
		RF_SHELL_BLUE | RF_SHELL_DOUBLE | RF_SHELL_GOD |
		RF_TRANSLUCENT | RF_BEAM | RF_WEAPONMODEL | RF_NOSHADOW | RF_DISTORT))
		return false;

	if (!r_playerShadow->value && (CURRENT_ENTITY->flags & RF_VIEWERMODEL))
		return false;

	if (CURRENT_ENTITY->angles[0] || CURRENT_ENTITY->angles[1] || CURRENT_ENTITY->angles[2]) {
		for (i = 0; i < 3; i++) {
			mins[i] = CURRENT_ENTITY->origin[i] - CURRENT_MODEL->radius;
			maxs[i] = CURRENT_ENTITY->origin[i] + CURRENT_MODEL->radius;
		}
	}
	else {
		VectorAdd (CURRENT_ENTITY->origin, CURRENT_ENTITY->model->maxs, maxs);
		VectorAdd (CURRENT_ENTITY->origin, CURRENT_ENTITY->model->mins, mins);
	}

	if (currentShadowLight->_cone) {

		if(R_CullConeLight(mins, maxs, currentShadowLight->frust))
			return false;

	}else if (currentShadowLight->spherical) {

		if (!BoundsAndSphereIntersect (mins, maxs, currentShadowLight->origin, currentShadowLight->radius[0]))
			return false;
	}
	else {

		if (!BoundsIntersect (mins, maxs, currentShadowLight->mins, currentShadowLight->maxs))
			return false;
	}

	if (VectorsEqual(CURRENT_ENTITY->origin, currentShadowLight->origin)) // skip shadows from shell lights
		return false;

	if (!InLightVISEntity())
		return false;

	return true;
}


void GL_LerpShadowVerts (int nverts, dtrivertx_t *v, dtrivertx_t *ov, float *lerp, float move[3], float frontv[3], float backv[3]) {
	int i;

	if (CURRENT_MODEL->numFrames < 1)
		return;

	for (i = 0; i < nverts; i++, v++, ov++, lerp += 4) {
		lerp[0] = move[0] + ov->v[0] * backv[0] + v->v[0] * frontv[0];
		lerp[1] = move[1] + ov->v[1] * backv[1] + v->v[1] * frontv[1];
		lerp[2] = move[2] + ov->v[2] * backv[2] + v->v[2] * frontv[2];
	}
}

void R_DeformShadowVolumeMD3()
{

}

void R_DeformShadowVolumeMD2()
{
	dmd2header_t			*paliashdr;
	dmd2frame_t	*frame, *oldframe;
	dtrivertx_t		*v, *ov, *verts;
	int				*order, i;
	float			frontlerp;
	vec3_t			move, delta, vectors[3], frontv, backv, light, temp;

	if (!R_EntityInLightBounds ())
		return;

	paliashdr = (dmd2header_t *)CURRENT_MODEL->extraData;

	frame = (dmd2frame_t *)((byte *)paliashdr + paliashdr->ofs_frames
		+ CURRENT_ENTITY->frame * paliashdr->framesize);

	verts = v = frame->verts;

	oldframe = (dmd2frame_t *)((byte *)paliashdr + paliashdr->ofs_frames +
		CURRENT_ENTITY->oldframe * paliashdr->framesize);

	ov = oldframe->verts;

	order = (int *)((byte *)paliashdr + paliashdr->ofs_glcmds);

	frontlerp = 1.0 - CURRENT_ENTITY->backlerp;

	// move should be the delta back to the previous frame * backlerp
	VectorSubtract (CURRENT_ENTITY->oldorigin, CURRENT_ENTITY->origin, delta);
	AngleVectors (CURRENT_ENTITY->angles, vectors[0], vectors[1],
		vectors[2]);

	move[0] = DotProduct (delta, vectors[0]);	// forward
	move[1] = -DotProduct (delta, vectors[1]);	// left
	move[2] = DotProduct (delta, vectors[2]);	// up

	VectorAdd (move, oldframe->translate, move);

	for (i = 0; i < 3; i++) {
		move[i] = CURRENT_ENTITY->backlerp * move[i] + frontlerp * frame->translate[i];
		frontv[i] = frontlerp * frame->scale[i];
		backv[i] = CURRENT_ENTITY->backlerp * oldframe->scale[i];
	}

	GL_LerpShadowVerts (paliashdr->num_xyz, v, ov, s_lerped[0], move, frontv, backv);

	R_SetupEntityMatrix (CURRENT_ENTITY);

	VectorSubtract (currentShadowLight->origin, CURRENT_ENTITY->origin, temp);
	Mat3_TransposeMultiplyVector (CURRENT_ENTITY->axis, temp, light);

	glUniformMatrix4fv(sv_mvp, 1, false, (const float *)CURRENT_ENTITY->orMatrix);
	glUniform3fv(sv_lightOrg, 1, light);

	BuildShadowVolumeTriangles (paliashdr, light);
}

void R_CastAliasShadowVolumes (bool player) {
	int	i;

	if (!r_shadows->value || !r_drawEntities->value)
		return;

	if (!currentShadowLight->isShadow || currentShadowLight->isAmbient || currentShadowLight->isFog)
		return;

	// setup program
	GL_BindProgram (shadowProgram, 0);

	GL_StencilMask (255);
	GL_StencilFuncSeparate (GL_FRONT_AND_BACK, GL_ALWAYS, 128, 255);
	GL_StencilOpSeparate (GL_BACK, GL_KEEP, GL_INCR_WRAP, GL_KEEP);
	GL_StencilOpSeparate (GL_FRONT, GL_KEEP, GL_DECR_WRAP, GL_KEEP);

	GL_Disable (GL_CULL_FACE);
	GL_DepthFunc (GL_LESS);
	GL_Enable (GL_POLYGON_OFFSET_FILL);
	GL_PolygonOffset (0.1, 1);
	GL_ColorMask (0, 0, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, getVbo().vbo_Dynamic);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, getVbo().ibo_Dynamic);

	glEnableVertexAttribArray (ATT_POSITION);
	glVertexAttribPointer (ATT_POSITION, 4, GL_FLOAT, false, 0, 0);

	if (player) {
		for (i = 0; i < r_newrefdef.num_entities; i++) {
			CURRENT_ENTITY = &r_newrefdef.entities[i];
			CURRENT_MODEL = CURRENT_ENTITY->model;

			if (!CURRENT_MODEL)
				continue;
			
			if (!(CURRENT_ENTITY->flags & RF_VIEWERMODEL))
				continue;

			if (CURRENT_MODEL->type == mod_alias_md2)
				R_DeformShadowVolumeMD2();
			else if (CURRENT_MODEL->type == mod_alias_md3)
				R_DeformShadowVolumeMD3();

		}
	}
	else
	{
		for (i = 0; i < r_newrefdef.num_entities; i++)
		{
			CURRENT_ENTITY = &r_newrefdef.entities[i];
			CURRENT_MODEL = CURRENT_ENTITY->model;

			if (!CURRENT_MODEL)
				continue;

			if (CURRENT_ENTITY->flags & RF_VIEWERMODEL)
				continue;

			if (CURRENT_MODEL->type == mod_alias_md2)
				R_DeformShadowVolumeMD2();
			else if(CURRENT_MODEL->type == mod_alias_md3)
				R_DeformShadowVolumeMD3();
		}
	}
	glDisableVertexAttribArray (ATT_POSITION);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	GL_Disable (GL_POLYGON_OFFSET_FILL);
	GL_PolygonOffset (0, 0);
	GL_Enable (GL_CULL_FACE);
	GL_ColorMask (1, 1, 1, 1);
	GL_BindNullProgram ();
}


/*
======================================
BSP SHADOW VOLUMES
EASY VERSION FROM TENEBRAE AND BERS@Q2
======================================
*/

bool R_MarkShadowSurf (msurface_t *surf) {
	cplane_t	*plane;
	glpoly_t	*poly;
	float		dist, lbbox[6], pbbox[6];

	if (surf->texInfo->flags & (SURF_NODRAW)) // rogue hack
		return false;

	// add sky surfaces to shadow marking
	if (surf->texInfo->flags & (SURF_SKY))
		goto hack;

	if ((surf->texInfo->flags & (SURF_ALPHA | SURF_WARP)) || (surf->flags & MSURF_DRAWTURB))
		return false;
hack:
	plane = surf->plane;
	poly = surf->polys;

	if (poly->shadowTimestamp == shadowTimeStamp)
		return false;

	switch (plane->type) {
		case PLANE_X:
			dist = currentShadowLight->origin[0] - plane->dist;
			break;
		case PLANE_Y:
			dist = currentShadowLight->origin[1] - plane->dist;
			break;
		case PLANE_Z:
			dist = currentShadowLight->origin[2] - plane->dist;
			break;
		default:
			dist = DotProduct (currentShadowLight->origin, plane->normal) - plane->dist;
			break;
	}

	//the normals are flipped when surf_planeback is 1
	if (((surf->flags & MSURF_PLANEBACK) && (dist > 0)) ||
		(!(surf->flags & MSURF_PLANEBACK) && (dist < 0)))
		return false;

	//the normals are flipped when surf_planeback is 1
	if (fabsf (dist) > currentShadowLight->maxRad)
		return false;

	lbbox[0] = currentShadowLight->origin[0] - currentShadowLight->radius[0];
	lbbox[1] = currentShadowLight->origin[1] - currentShadowLight->radius[1];
	lbbox[2] = currentShadowLight->origin[2] - currentShadowLight->radius[2];
	lbbox[3] = currentShadowLight->origin[0] + currentShadowLight->radius[0];
	lbbox[4] = currentShadowLight->origin[1] + currentShadowLight->radius[1];
	lbbox[5] = currentShadowLight->origin[2] + currentShadowLight->radius[2];

	// surface bounding box
	pbbox[0] = surf->mins[0];
	pbbox[1] = surf->mins[1];
	pbbox[2] = surf->mins[2];
	pbbox[3] = surf->maxs[0];
	pbbox[4] = surf->maxs[1];
	pbbox[5] = surf->maxs[2];

	if (currentShadowLight->_cone && R_CullConeLight(&pbbox[0], &pbbox[3], currentShadowLight->frust))
		return false;

	if (!BoundsIntersect (&lbbox[0], &lbbox[3], &pbbox[0], &pbbox[3]))
		return false;

	poly->shadowTimestamp = shadowTimeStamp;

	return true;
}

void R_MarkBrushModelShadowSurfaces () {
	int			i;
	msurface_t	*psurf;
	Model		*clmodel;

	clmodel = CURRENT_ENTITY->model;
	psurf = &clmodel->surfaces[clmodel->firstModelSurface];

	for (i = 0; i < clmodel->numModelSurfaces; i++, psurf++) {

		if (R_MarkShadowSurf (psurf))
			shadow_surfaces[num_shadow_surfaces++] = psurf;
	}
}

void R_DrawBrushModelVolumes () {
	int			i, j, vb = 0, ib = 0, surfBase = 0;
	float		scale;
	msurface_t	*surf;
	Model		*clmodel;
	glpoly_t	*poly;
	vec3_t		v1, temp, oldLight;
	bool	shadow;

	clmodel = CURRENT_ENTITY->model;
	surf = &clmodel->surfaces[clmodel->firstModelSurface];

	if (!R_EntityInLightBounds ())
		return;

	R_SetupEntityMatrix (CURRENT_ENTITY);

	VectorCopy (currentShadowLight->origin, oldLight);
	VectorSubtract (currentShadowLight->origin, CURRENT_ENTITY->origin, temp);
	Mat3_TransposeMultiplyVector (CURRENT_ENTITY->axis, temp, currentShadowLight->origin);

	glUniformMatrix4fv(null_mvp, 1, false, (const float *)CURRENT_ENTITY->orMatrix);

	shadowTimeStamp++;
	num_shadow_surfaces = 0;
	R_MarkBrushModelShadowSurfaces ();

	scale = currentShadowLight->maxRad * 2;

	// generate vertex buffer
	for (i = 0; i < num_shadow_surfaces; i++) {
		surf = shadow_surfaces[i];
		poly = surf->polys;

		if (surf->polys->shadowTimestamp != shadowTimeStamp)
			continue;

		for (j = 0; j < surf->numEdges; j++) {

			VectorCopy (poly->verts[j], vcache[vb * 2 + 0]);
			VectorSubtract (poly->verts[j], currentShadowLight->origin, v1);
			vcache[vb * 2 + 1][0] = v1[0] * scale + poly->verts[j][0];
			vcache[vb * 2 + 1][1] = v1[1] * scale + poly->verts[j][1];
			vcache[vb * 2 + 1][2] = v1[2] * scale + poly->verts[j][2];
			vb++;
		}
	}

	// generate index buffer
	for (i = 0; i < num_shadow_surfaces; i++) {
		surf = shadow_surfaces[i];
		poly = surf->polys;

		if (surf->polys->shadowTimestamp != shadowTimeStamp)
			continue;

		for (j = 0; j < surf->numEdges; j++) {
			shadow = false;

			if (poly->neighbours[j] != nullptr) {

				if (poly->neighbours[j]->shadowTimestamp != poly->shadowTimestamp)
					shadow = true;
			}
			else
				shadow = true;

			if (shadow) {
				int jj = (j + 1) % poly->numVerts;

				icache[ib++] = j  * 2 + 0 + surfBase;
				icache[ib++] = j  * 2 + 1 + surfBase;
				icache[ib++] = jj * 2 + 1 + surfBase;

				icache[ib++] = j  * 2 + 0 + surfBase;
				icache[ib++] = jj * 2 + 1 + surfBase;
				icache[ib++] = jj * 2 + 0 + surfBase;
			}
		}

		//Draw near light cap
		for (j = 0; j < surf->numEdges - 2; j++) {
			icache[ib++] = 0 + surfBase;
			icache[ib++] = (j + 1) * 2 + 0 + surfBase;
			icache[ib++] = (j + 2) * 2 + 0 + surfBase;
		}

		//Draw extruded cap
		for (j = 0; j < surf->numEdges - 2; j++) {
			icache[ib++] = 1 + surfBase;
			icache[ib++] = (j + 2) * 2 + 1 + surfBase;
			icache[ib++] = (j + 1) * 2 + 1 + surfBase;
		}
		surfBase += surf->numEdges * 2;
	}

	if (ib) {
		glBufferSubData(GL_ARRAY_BUFFER, 0, surfBase * sizeof(vec3_t), vcache);
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, ib * sizeof(size_t), icache);

		glDrawElements	(GL_TRIANGLES, ib, GL_UNSIGNED_INT, nullptr);
	}

	VectorCopy(oldLight, currentShadowLight->origin);
}


void R_MarkShadowCasting (mnode_t *node) {
	cplane_t	*plane;
	int			c, cluster;
	float		dist;
	msurface_t	**surf;
	mleaf_t		*leaf;

	if (node->contents != -1) {

		//we are in a leaf
		leaf = (mleaf_t *)node;
		cluster = leaf->cluster;

		if (!(currentShadowLight->vis[cluster >> 3] & (1 << (cluster & 7))))
			return;

		surf = leaf->firstmarksurface;

		for (c = 0; c < leaf->numMarkSurfaces; c++, surf++) {

			if (R_MarkShadowSurf ((*surf))) {

				shadow_surfaces[num_shadow_surfaces++] = (*surf);
			}
		}
		return;
	}

	plane = node->plane;
	dist = DotProduct (currentShadowLight->origin, plane->normal) - plane->dist;

	if (dist > currentShadowLight->maxRad) {
		R_MarkShadowCasting (node->children[0]);
		return;
	}

	if (dist < -currentShadowLight->maxRad) {
		R_MarkShadowCasting (node->children[1]);
		return;
	}

	R_MarkShadowCasting (node->children[0]);
	R_MarkShadowCasting (node->children[1]);
}

extern Model* LOAD_MODEL;
int numPreCachedLights = 0;

void R_DrawBspModelVolumes (bool precalc, worldShadowLight_t *light) {
	int			i, j, vb = 0, ib = 0, surfBase = 0;
	float		scale;
	msurface_t	*surf;
	glpoly_t	*poly;
	vec3_t		v1;
	bool	shadow;

	if (precalc)
		currentShadowLight = light;
	else
		light = nullptr;

	shadowTimeStamp++;
	num_shadow_surfaces = 0;
	R_MarkShadowCasting (r_worldmodel->nodes);

	scale = currentShadowLight->maxRad * 10;

	// generate vertex buffer
	for (i = 0; i < num_shadow_surfaces; i++) {
		surf = shadow_surfaces[i];
		poly = surf->polys;

		if (surf->polys->shadowTimestamp != shadowTimeStamp)
			continue;

		for (j = 0; j < surf->numEdges; j++) {

			VectorCopy (poly->verts[j], vcache[vb * 2 + 0]);
			VectorSubtract (poly->verts[j], currentShadowLight->origin, v1);

			vcache[vb * 2 + 1][0] = v1[0] * scale + poly->verts[j][0];
			vcache[vb * 2 + 1][1] = v1[1] * scale + poly->verts[j][1];
			vcache[vb * 2 + 1][2] = v1[2] * scale + poly->verts[j][2];
			vb++;
		}
	}

	// generate index buffer
	for (i = 0; i < num_shadow_surfaces; i++) {
		surf = shadow_surfaces[i];
		poly = surf->polys;

		if (surf->polys->shadowTimestamp != shadowTimeStamp)
			continue;

		for (j = 0; j < surf->numEdges; j++) {
			shadow = false;

			if (poly->neighbours[j] != nullptr) {

				if (poly->neighbours[j]->shadowTimestamp != poly->shadowTimestamp)
					shadow = true;
			}
			else
				shadow = true;

			if (shadow) {
				int jj = (j + 1) % poly->numVerts;

				icache[ib++] = j  * 2 + 0 + surfBase;
				icache[ib++] = j  * 2 + 1 + surfBase;
				icache[ib++] = jj * 2 + 1 + surfBase;

				icache[ib++] = j  * 2 + 0 + surfBase;
				icache[ib++] = jj * 2 + 1 + surfBase;
				icache[ib++] = jj * 2 + 0 + surfBase;
			}
		}

		//Draw near light cap
		for (j = 0; j < surf->numEdges - 2; j++) {
			icache[ib++] = 0 + surfBase;
			icache[ib++] = (j + 1) * 2 + 0 + surfBase;
			icache[ib++] = (j + 2) * 2 + 0 + surfBase;
		}

		//Draw extruded cap
		for (j = 0; j < surf->numEdges - 2; j++) {
			icache[ib++] = 1 + surfBase;
			icache[ib++] = (j + 2) * 2 + 1 + surfBase;
			icache[ib++] = (j + 1) * 2 + 1 + surfBase;
		}
		surfBase += surf->numEdges * 2;
	}

	if (precalc) {

		if (currentShadowLight->vboId)
			glDeleteBuffers(1, &currentShadowLight->vboId);

		glGenBuffers(1, &currentShadowLight->vboId);
		glBindBuffer(GL_ARRAY_BUFFER, currentShadowLight->vboId);
		glBufferData(GL_ARRAY_BUFFER, surfBase * sizeof(vec3_t), vcache, GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		if (currentShadowLight->iboId)
			glDeleteBuffers(1, &currentShadowLight->iboId);

		glGenBuffers(1, &currentShadowLight->iboId);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, currentShadowLight->iboId);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, ib * sizeof(size_t), icache, GL_STATIC_DRAW);
		currentShadowLight->iboNumIndices = ib;
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		numPreCachedLights++;
	}
	else {
		if (ib) {
			glBufferSubData(GL_ARRAY_BUFFER, 0, surfBase * sizeof(vec3_t), vcache);
			glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, ib * sizeof(size_t), icache);
			
			glDrawElements	(GL_TRIANGLES, ib, GL_UNSIGNED_INT, nullptr);
		}
	}
}


void R_CastBspShadowVolumes () {
	int	i;

	if (!r_shadows->value)
		return;

	if (!currentShadowLight->isShadow || currentShadowLight->isAmbient)
		return;

	// setup program
	GL_BindProgram (nullProgram, 0);
	glUniformMatrix4fv(null_mvp, 1, false, (const float *)r_newrefdef.modelViewProjectionMatrix);

	GL_StencilMask (255);
	GL_StencilFuncSeparate (GL_FRONT_AND_BACK, GL_ALWAYS, 128, 255);
	GL_StencilOpSeparate (GL_BACK, GL_KEEP, GL_INCR_WRAP, GL_KEEP);
	GL_StencilOpSeparate (GL_FRONT, GL_KEEP, GL_DECR_WRAP, GL_KEEP);

	GL_Disable (GL_CULL_FACE);
	GL_DepthFunc (GL_LESS);
	GL_Enable (GL_POLYGON_OFFSET_FILL);
	GL_PolygonOffset (0.1, 1);
	GL_ColorMask (0, 0, 0, 0);
	glEnableVertexAttribArray (ATT_POSITION);

	if (currentShadowLight->vboId && currentShadowLight->iboId && currentShadowLight->isStatic) { // draw vbo shadow

		glBindBuffer(GL_ARRAY_BUFFER, currentShadowLight->vboId);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, currentShadowLight->iboId);
		
		glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, 0);
		glDrawElements	(GL_TRIANGLES, currentShadowLight->iboNumIndices, GL_UNSIGNED_INT, nullptr);
	}

	glBindBuffer(GL_ARRAY_BUFFER, getVbo().vbo_Dynamic);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, getVbo().ibo_Dynamic);
	
	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, 0);

	if (!currentShadowLight->isStatic)	
		R_DrawBspModelVolumes(false, nullptr); 
	

	for (i = 0; i < r_newrefdef.num_entities; i++) {
		CURRENT_ENTITY = &r_newrefdef.entities[i];
		CURRENT_MODEL = CURRENT_ENTITY->model;

		if (!CURRENT_MODEL)
			continue;

		if (CURRENT_MODEL->type == mod_brush)
			R_DrawBrushModelVolumes ();
	}

	glDisableVertexAttribArray (ATT_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	GL_Disable (GL_POLYGON_OFFSET_FILL);
	GL_Enable (GL_CULL_FACE);
	GL_ColorMask (1, 1, 1, 1);
	GL_BindNullProgram ();
}