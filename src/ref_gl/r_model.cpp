﻿/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/
#include "r_local.h"

#include "r_model.h"
#include "r_model_md3.h"

#include "r_bsp.h"
#include "r_image.h"
#include "r_util.h"

#include "../client/client.h"

#include "../common/filesystem.h"
#include "../common/string_util.h"


// ===============================================================================
// = DEFINES
// ===============================================================================
#define	MAX_MOD_KNOWN	512
#define SMOOTH_COSINE cos(DEG2RAD(45.0))


// ===============================================================================
// = MODULE VARIABLES
// ===============================================================================
int MODELS_NUM_KNOWN;

int r_numIgnoreAutoLights = 0;
int r_numAutoLights = 0;

extern int BSP_SIZE;
extern int ALIAS_SIZE;
extern int SPRITE_SIZE;


Model MODELS_KNOWN[MAX_MOD_KNOWN];
Model MODELS_INLINE[MAX_MOD_KNOWN]; // the inline models from the current map are kept seperate

Model* LOAD_MODEL = nullptr;



// ===============================================================================
// = FUNCTION PROTOTYPES
// ===============================================================================
byte Normal2Index(const vec3_t vec);

void Mod_LoadSpriteModel(Model*  mod, void *buffer, int file_length);
void Mod_LoadAliasMD2Model(Model*  mod, void *buffer);


// ===============================================================================
// = INTERNAL FUNCTIONS
// ===============================================================================

/**
 *
 */
static void _freeModel(Model* mod)
{
	Hunk_Free(mod->extraData);

	if (mod->type == mod_alias_md2)
	{
		if (mod->neighbours)
			free(mod->neighbours);

		glDeleteBuffers(1, &mod->vboId);
	}
	else if (mod->type == mod_alias_md3)
	{
		Com_Printf("NI: MD3 Free - %s", mod->name);
	}

	memset(mod, 0, sizeof(Model));
}



/**
 * 
 */
static Model* _modelForName(char* name, bool crash)
{
	int i = 0, wasMD5 = 0;

	if (!name[0]) VID_Error(ERR_DROP, "Mod_ForName: nullptr name");

	// inline models are grabbed only from worldmodel
	if (name[0] == '*')
	{
		i = atoi(name + 1);
		if (i < 1 || !r_worldmodel || i >= r_worldmodel->numSubModels)
			VID_Error(ERR_DROP, "bad inline model number");

		return &MODELS_INLINE[i];
	}

	// search the currently loaded models
	Model* mod = MODELS_KNOWN;
	for (i = 0; i < MODELS_NUM_KNOWN; i++, mod++)
	{
		if (!mod->name[0]) continue;

		if (!strcmp(mod->name, name))
		{
			if (mod->type == mod_alias_md2 || mod->type == mod_alias_md3)
			{
				int i = 0;	// Wtf?       ↓  ← That makes no sense, why not just stick '0' in there?
				Image* img = mod->skins[i];
			}

			return mod;
		}
	}

	// find a free model slot spot
	for (i = 0, mod = MODELS_KNOWN; i < MODELS_NUM_KNOWN; i++, mod++)
	{
		if (!mod->name[0])
			break;				// free spot
	} /// \fixme variable reuse

	if (i == MODELS_NUM_KNOWN)
	{
		if (MODELS_NUM_KNOWN == MAX_MOD_KNOWN) VID_Error(ERR_DROP, "MODELS_NUM_KNOWN == MAX_MOD_KNOWN");
		MODELS_NUM_KNOWN++;
	}

	memset(mod, 0, sizeof(Model));
	strcpy(mod->name, name);

	// load the file
	unsigned int* buf = nullptr;
	int len = FS_LoadFile(mod->name, reinterpret_cast<void**>(&buf));
	if (!buf)
	{
		if (crash) { Com_Error(ERR_DROP, S_COLOR_MAGENTA "_modelForName(): " S_COLOR_WHITE "%s not found", mod->name); }

		memset(mod->name, 0, sizeof(mod->name));
		return nullptr;
	}

	LOAD_MODEL = mod;

	// fill it in
	switch (LittleLong(*buf))
	{
	case IDMD3HEADER:
		//LOAD_MODEL->extraData = (maliasmodel_t*)malloc(sizeof(maliasmodel_t)); ///\fixme Magic Number  <- Set within LoadAliasMD3Model()
		Mod_LoadAliasMD3Model(mod, nullptr, buf);
		break;

	case IDALIASMD2:											// 1MB buffer?
		LOAD_MODEL->extraData = Hunk_Begin(hunk_model->value * 1048576, name); ///\fixme Magic Number
		Mod_LoadAliasMD2Model(mod, buf);
		break;

	case IDSPRITEHEADER:
		LOAD_MODEL->extraData = Hunk_Begin(hunk_sprite->value * 1048576, name); ///\fixme Magic Number
		Mod_LoadSpriteModel(mod, buf, len);
		break;

	case IDBSPHEADER:
		LOAD_MODEL->extraData = Hunk_Begin(hunk_bsp->value * 1048576, name); ///\fixme Magic Number
		BSP_LoadBrushModel(mod, buf);
		break;

	default:
		VID_Error(ERR_DROP, "Mod_ForName(): unknown fileId for %s", mod->name);
		break;
	}

	LOAD_MODEL->extraDataSize = Hunk_End();
	FS_FreeFile(buf);

	return mod;
}


// ===============================================================================
// = PUBLIC FUNCTIONS
// ===============================================================================

Model* Mod_KnownModels()
{
	 return MODELS_KNOWN;
}


Model* Mod_InlineModels()
{
	return MODELS_INLINE;
}


/**
 * 
 */
Model* Mod_LoadingModel()
{
	return LOAD_MODEL;
}


/**
 * 
 */
void Mod_ModelList()
{
	int total = 0;
	Com_Printf("Loaded models:\n");
	Model* mod = MODELS_KNOWN;
	for (int i = 0; i < MODELS_NUM_KNOWN; i++, mod++)
	{
		if (!mod->name[0]) continue;

		Com_Printf("%8i : %s\n", mod->extraDataSize, mod->name);
		total += mod->extraDataSize;
	}
	Com_Printf("Total resident: %i\n", total);
}


/*
==============================================================================
MD2 MODELS
==============================================================================
*/

/**
 * 
 */
byte Normal2Index(const vec3_t vec)
{
	int best = 0;
	float bestd = 0;
	for (int i = 0; i < NUM_VERTEX_NORMALS; i++)
	{
		float d = DotProduct(vec, q_byteDirs[i]);
		if (d > bestd)
		{
			bestd = d;
			best = i;
		}
	}

	return best;
}


/**
 * Shadow volumes stuff
 * 
 * \note	Called only by Mod_BuildTriangleNeighbors (e.g., MD2 loading code).
 */
static int Mod_FindTriangleWithEdge(neighbors_t* neighbors, dmd2vertex_t* tris, int numtris, int triIndex, int edgeIndex)
{
	int i, j, found = -1, foundj = 0;
	dmd2vertex_t *current = &tris[triIndex];
	bool dup = false;

	for (i = 0; i < numtris; i++)
	{
		if (i == triIndex)
			continue;

		for (j = 0; j < 3; j++)
		{
			if (((current->index_xyz[edgeIndex] == tris[i].index_xyz[j]) &&
				(current->index_xyz[(edgeIndex + 1) % 3] == tris[i].index_xyz[(j + 1) % 3])) ||
				((current->index_xyz[edgeIndex] == tris[i].index_xyz[(j + 1) % 3]) &&
				(current->index_xyz[(edgeIndex + 1) % 3] == tris[i].index_xyz[j])))
			{
				// no edge for this model found yet?
				if (found == -1)
				{
					found = i;
					foundj = j;
				}
				else
					dup = true;	// the three edges story
			}
		}
	}

	// normal edge, setup neighbour pointers
	if (!dup && found != -1)
	{
		neighbors[found].n[foundj] = triIndex;
		return found;
	}

	// naughty egde let no-one have the neighbour <-- ?
	return -1;
}


/**
 * \note	Seems to only be used by the MD2 loading code.
 */
static void Mod_BuildTriangleNeighbors(neighbors_t * neighbors, dmd2vertex_t * tris, int numtris)
{
	int i, j;

	// set neighbours to -1
	for (i = 0; i < numtris; i++) {
		for (j = 0; j < 3; j++)
			neighbors[i].n[j] = -1;
	}

	// generate edges information (for shadow volumes)
	// NOTE: We do this with the original vertices not the reordered onces 
	// since reordering them
	// duplicates vertices and we only compare indices
	for (i = 0; i < numtris; i++) {
		for (j = 0; j < 3; j++) {
			if (neighbors[i].n[j] == -1)
				neighbors[i].n[j] = Mod_FindTriangleWithEdge(neighbors, tris, numtris, i, j);
		}
	}
}


/**
 * \note	Seems to only be loaded by MD2 -- might make sense to have
 *			MD3 loading code also take advantage of this as well.
 */
void Mod_LoadAliasModelFx(Model* mod, char* s)
{
	char* token;

	while (s)
	{
		token = Com_Parse(&s);

		if (!strcasecmp(token, "monster"))
		{
			mod->flags |= RF_MONSTER;
			continue;
		}

		if (!strcasecmp(token, "distort"))
		{
			mod->flags |= RF_DISTORT;
			continue;
		}

		if (!strcasecmp(token, "noshadow"))
		{
			mod->flags |= RF_NOSHADOW;
			continue;
		}
		if (!strcasecmp(token, "fullbright"))
		{
			mod->flags |= RF_FULLBRIGHT;
			continue;
		}

		if (!strcasecmp(token, "glow"))
		{
			mod->glowCfg[0] = atof(Com_Parse(&s)); // alpha min
			mod->glowCfg[1] = atof(Com_Parse(&s)); // alpha max
			mod->glowCfg[2] = atof(Com_Parse(&s)); // time scale
			continue;
		}
		if (!strcasecmp(token, "noSelfShadow"))
		{
			mod->noSelfShadow = true;
			continue;
		}
		if (!strcasecmp(token, "envMap"))
		{
			mod->envMap = true;
			mod->envScale = atof(Com_Parse(&s));
			continue;
		}

		if (!strcasecmp(token, "scale"))
		{
			mod->modelScale = atof(Com_Parse(&s));
			continue;
		}
	}
}


/**
 * \note	Seems to only be used in MD2 Loading Code
 */
static void _calcTangentVectors(float *v0, float *v1, float *v2, float *st0, float *st1, float *st2, vec3_t Tangent, vec3_t Binormal)
{
	vec3_t	vec1, vec2;
	vec3_t	planes[3];
	float	tmp;
	int		i;

	for (i = 0; i < 3; i++)
	{
		vec1[0] = v1[i] - v0[i];
		vec1[1] = st1[0] - st0[0];
		vec1[2] = st1[1] - st0[1];
		vec2[0] = v2[i] - v0[i];
		vec2[1] = st2[0] - st0[0];
		vec2[2] = st2[1] - st0[1];
		VectorNormalize(vec1);
		VectorNormalize(vec2);
		CrossProduct(vec1, vec2, planes[i]);
	}

	for (i = 0; i < 3; i++) {
		tmp = 1.0 / planes[i][0];
		Tangent[i] = -planes[i][1] * tmp;
		Binormal[i] = -planes[i][2] * tmp;
	}
	VectorNormalize(Tangent);
	VectorNormalize(Binormal);
}


/**
 * 
 */
void Mod_BuildMD2Tangents(Model*  mod, dmd2header_t *pheader, fstvert_t *poutst)
{
	int				i, j, k, l;
	dmd2frame_t	*frame;
	dtrivertx_t		*verts, *v;
	dmd2vertex_t		*tris = (dmd2vertex_t *)((byte *)pheader + pheader->ofs_tris);
	int				cx = pheader->num_xyz * pheader->num_frames * sizeof(byte);
	vec3_t			binormals_[MAX_VERTS];
	vec3_t			tangents_[MAX_VERTS];
	vec3_t			normals_[MAX_VERTS];
	byte			*tangents = nullptr, *binormals = nullptr;

	mod->binormals = binormals = (byte*)Hunk_Alloc(cx);
	mod->tangents = tangents = (byte*)Hunk_Alloc(cx);

	mod->memorySize += cx;
	mod->memorySize += cx;

	//for all frames
	for (i = 0; i < pheader->num_frames; i++)
	{

		//set temp to zero
		memset(tangents_, 0, pheader->num_xyz * sizeof(vec3_t));
		memset(binormals_, 0, pheader->num_xyz * sizeof(vec3_t));
		memset(normals_, 0, pheader->num_xyz * sizeof(vec3_t));

		tris = (dmd2vertex_t *)((byte *)pheader + pheader->ofs_tris);
		frame = (dmd2frame_t *)((byte *)pheader + pheader->ofs_frames + i * pheader->framesize);
		verts = frame->verts;

		//for all tris
		for (j = 0; j < pheader->num_tris; j++)
		{
			vec3_t	edge0, edge1, edge2;
			vec3_t	triangle[3], dir0, dir1;
			vec3_t	tangent, binormal, normal, cross;

			for (k = 0; k < 3; k++)
			{
				l = tris[j].index_xyz[k];
				v = &verts[l];
				for (l = 0; l < 3; l++)
					triangle[k][l] = v->v[l];
			}

			//calc normals
			VectorSubtract(triangle[0], triangle[1], dir0);
			VectorSubtract(triangle[2], triangle[1], dir1);
			CrossProduct(dir1, dir0, normal);
			VectorInverse(normal);
			VectorNormalize(normal);

			// calc tangents
			edge0[0] = (float)verts[tris[j].index_xyz[0]].v[0];
			edge0[1] = (float)verts[tris[j].index_xyz[0]].v[1];
			edge0[2] = (float)verts[tris[j].index_xyz[0]].v[2];
			edge1[0] = (float)verts[tris[j].index_xyz[1]].v[0];
			edge1[1] = (float)verts[tris[j].index_xyz[1]].v[1];
			edge1[2] = (float)verts[tris[j].index_xyz[1]].v[2];
			edge2[0] = (float)verts[tris[j].index_xyz[2]].v[0];
			edge2[1] = (float)verts[tris[j].index_xyz[2]].v[1];
			edge2[2] = (float)verts[tris[j].index_xyz[2]].v[2];

			_calcTangentVectors(edge0, edge1, edge2, &poutst[tris[j].index_st[0]].s, &poutst[tris[j].index_st[1]].s, &poutst[tris[j].index_st[2]].s, tangent, binormal);

			// invert if needed
			CrossProduct(binormal, tangent, cross);
			if (DotProduct(cross, normal) < 0.0)
			{
				VectorInverse(tangent);
				VectorInverse(binormal);
			}

			for (k = 0; k < 3; k++)
			{
				l = tris[j].index_xyz[k];
				VectorAdd(tangents_[l], tangent, tangents_[l]);
				VectorAdd(binormals_[l], binormal, binormals_[l]);
				VectorAdd(normals_[l], normal, normals_[l]);
			}
		}

		for (j = 0; j < pheader->num_xyz; j++)
		{
			for (k = j + 1; k < pheader->num_xyz; k++)
			{
				if (verts[j].v[0] == verts[k].v[0] && verts[j].v[1] == verts[k].v[1] && verts[j].v[2] == verts[k].v[2])
				{
					float *jnormal = q_byteDirs[verts[j].lightnormalindex];
					float *knormal = q_byteDirs[verts[k].lightnormalindex];

					if (DotProduct(jnormal, knormal) >= SMOOTH_COSINE)
					{
						VectorAdd(tangents_[j], tangents_[k], tangents_[j]);
						VectorCopy(tangents_[j], tangents_[k]);

						VectorAdd(binormals_[j], binormals_[k], binormals_[j]);
						VectorCopy(binormals_[j], binormals_[k]);

						VectorAdd(normals_[j], normals_[k], normals_[j]);
						VectorCopy(normals_[j], normals_[k]);
					}
				}
			}
		}

		//normalize averages
		for (j = 0; j < pheader->num_xyz; j++)
		{
			VectorNormalize(tangents_[j]);
			VectorNormalize(binormals_[j]);
			VectorNormalize(normals_[j]);

			tangents[i * pheader->num_xyz + j] = Normal2Index(tangents_[j]);
			binormals[i * pheader->num_xyz + j] = Normal2Index(binormals_[j]);
			verts[j].lightnormalindex = Normal2Index(normals_[j]);
		}
	}
}


/**
 * Verifies that the MD2 header is correct.
 * 
 * \note	If an issue is detected, this function will push a video error
 *			and halt the game from loading further.
 */
static void _verifyMD2Header(dmd2header_t* header, Model* mod, void* buffer)
{
	// byte swap the header fields and sanity check
	for (int i = 0; i < sizeof(dmd2header_t) * 0.25; i++)
		((int *)header)[i] = LittleLong(((int *)buffer)[i]);

	if (header->num_xyz <= 0)
		VID_Error(ERR_DROP, "model %s has no vertices", mod->name);

	if (header->num_xyz > MAX_VERTS)
		VID_Error(ERR_DROP, "model %s has too many vertices", mod->name);

	if (header->num_st <= 0)
		VID_Error(ERR_DROP, "model %s has no st vertices", mod->name);

	if (header->num_tris <= 0)
		VID_Error(ERR_DROP, "model %s has no triangles", mod->name);

	if (header->num_frames <= 0)
		VID_Error(ERR_DROP, "model %s has no frames", mod->name);
}


void Mod_LoadAliasMD2Model(Model*  mod, void *buffer)
{
	int				i, j, indexST;
	dmd2header_t	*pinmodel, *pheader;
	fstvert_t		*poutst;
	dstvert_t		*pinst;
	dmd2vertex_t	*pintri, *pouttri, *tris;
	dmd2frame_t		*pinframe, *poutframe;
	dmd2frame_t		*frame;
	dtrivertx_t		*verts;

	float			s, t;
	float			iw, ih;

	vec3_t			tempr, tempv;
	int				k, l;
	char			nam[MAX_OSPATH];

	mod->memorySize = 0;

	pinmodel = (dmd2header_t*)buffer;

	if (LittleLong(pinmodel->version) != ALIAS_MD2_VERSION)
	{
		VID_Error(ERR_DROP, "** Bad version number for " S_COLOR_GREEN "\'%s\'", mod->name);
	}

	mod->type = mod_alias_md2;
	mod->radius = 0;

	pheader = (dmd2header_t*)Hunk_Alloc(LittleLong(pinmodel->ofs_end));
	mod->memorySize += LittleLong(pinmodel->ofs_end);
	ALIAS_SIZE += mod->memorySize;

	_verifyMD2Header(pheader, mod, buffer);

	mod->flags = 0;

	// set default render fx values
	mod->glowCfg[0] = 0.3;
	mod->glowCfg[1] = 1.0;
	mod->glowCfg[2] = 5.666;
	mod->noSelfShadow = false;
	mod->modelScale = 1.0;
	mod->envMap = false;
	mod->envScale = 0.1;
	i = strlen(mod->name);
	memcpy(nam, mod->name, i);
	nam[i - 3] = 'r';
	nam[i - 2] = 'f';
	nam[i - 1] = 'x';
	nam[i] = 0;
	// load the .rfx

	char* buff = nullptr;
	i = FS_LoadFile(nam, reinterpret_cast<void**>(&buff));
	if (buff)
	{
		char bak = buff[i];
		buff[i] = 0;
		Mod_LoadAliasModelFx(mod, buff);
		buff[i] = bak;
		FS_FreeFile(buff);
	}

	// load triangle lists
	pintri = (dmd2vertex_t *)((byte *)pinmodel + pheader->ofs_tris);
	pouttri = (dmd2vertex_t *)((byte *)pheader + pheader->ofs_tris);

	for (i = 0, tris = pouttri; i < pheader->num_tris; i++, tris++)
	{
		for (j = 0; j < 3; j++)
		{
			tris->index_xyz[j] = LittleShort(pintri[i].index_xyz[j]);
			tris->index_st[j] = LittleShort(pintri[i].index_st[j]);
		}
	}

	// find neighbours
	mod->neighbours = (neighbors_t*)malloc(pheader->num_tris * sizeof(neighbors_t));
	Mod_BuildTriangleNeighbors(mod->neighbours, pouttri, pheader->num_tris);
	mod->memorySize += pheader->num_tris * sizeof(neighbors_t);

	// load the frames
	for (i = 0; i < pheader->num_frames; i++)
	{
		pinframe = (dmd2frame_t *)((byte *)pinmodel + pheader->ofs_frames + i * pheader->framesize);
		poutframe = (dmd2frame_t *)((byte *)pheader + pheader->ofs_frames + i * pheader->framesize);

		memcpy(poutframe->name, pinframe->name, sizeof(poutframe->name));
		for (j = 0; j < 3; j++)
		{
			poutframe->scale[j] = LittleFloat(pinframe->scale[j]) * mod->modelScale;
			poutframe->translate[j] = LittleFloat(pinframe->translate[j]) * mod->modelScale;
		}
		// verts are all 8 bit, so no swapping needed
		memcpy(poutframe->verts, pinframe->verts, pheader->num_xyz * sizeof(dtrivertx_t));
	}

	// register all skins
	memcpy((char*)pheader + pheader->ofs_skins, (char*)pinmodel + pheader->ofs_skins, pheader->num_skins * MAX_SKINNAME);
	for (i = 0; i < pheader->num_skins; i++)
	{
		char* pname = (char*)pheader + pheader->ofs_skins + i * MAX_SKINNAME;

		// If no directory separator, assume model only specifies skin file name and
		// not full path. Add the base path of the model to the skin name.
		if (!strchr(pname, '/'))
		{
			char _basePath[MAX_QPATH] = { '\0' };
			Com_FilePath(mod->name, _basePath);
			Q_sprintf(pname, MAX_QPATH, "%s/%s", _basePath, pname);
		}

		mod->skins[i] = GL_FindImage(pname, IT_SKIN, true);

		// GlowMaps loading
		char gl[128] = { 0 };
		strcpy(gl, pname);
		gl[strlen(gl) - 4] = 0;
		strcat(gl, "_light.tga");
		mod->glowtexture[i] = GL_FindImage(gl, IT_SKIN, true);

		if (!mod->glowtexture[i])
		{
			strcpy(gl, pname);
			gl[strlen(gl) - 4] = 0;
			strcat(gl, "_light.dds");
			mod->glowtexture[i] = GL_FindImage(gl, IT_SKIN, true);
		}

		if (!mod->glowtexture[i])
			mod->glowtexture[i] = r_notexture;

		// Loading Normal maps
		strcpy(gl, pname);
		gl[strlen(gl) - 4] = 0;
		strcat(gl, "_n.tga");
		mod->skins_normal[i] = GL_FindImage(gl, IT_BUMP, true);

		if (!mod->skins_normal[i])
		{
			strcpy(gl, pname);
			gl[strlen(gl) - 4] = 0;
			strcat(gl, "_n.dds");
			mod->skins_normal[i] = GL_FindImage(gl, IT_BUMP, true);
		}

		if (!mod->skins_normal[i])
			mod->skins_normal[i] = r_defBump;

		// Loading roughness maps
		strcpy(gl, pname);
		gl[strlen(gl) - 4] = 0;
		strcat(gl, "_rgh.tga");
		mod->skins_roughness[i] = GL_FindImage(gl, IT_BUMP, true);

		if (!mod->skins_roughness[i]) {
			strcpy(gl, pname);
			gl[strlen(gl) - 4] = 0;
			strcat(gl, "_rgh.dds");
			mod->skins_roughness[i] = GL_FindImage(gl, IT_BUMP, true);
		}

		if (!mod->skins_roughness[i])
			mod->skins_roughness[i] = r_notexture;
	}

	// Calculate texcoords for triangles (for compute tangents and binormals)
	mod->memorySize += pheader->num_st * sizeof(fstvert_t);
	pinst = (dstvert_t *)((byte *)pinmodel + pheader->ofs_st);
	poutst = (fstvert_t*)Hunk_Alloc(pheader->num_st * sizeof(fstvert_t));
	iw = 1.0 / pheader->skinwidth;
	ih = 1.0 / pheader->skinheight;
	for (i = 0; i < pheader->num_st; i++) {
		s = LittleShort(pinst[i].s);
		t = LittleShort(pinst[i].t);
		poutst[i].s = (s - 0.5) * iw;
		poutst[i].t = (t - 0.5) * ih;
	}

	// build tangents vectors
	Mod_BuildMD2Tangents(mod, pheader, poutst);

	ClearBounds(mod->mins, mod->maxs);
	VectorClear(mod->center);
	frame = (dmd2frame_t *)((byte *)pheader + pheader->ofs_frames);
	verts = frame->verts;

	for (k = 0; k < pheader->num_xyz; k++)
		for (l = 0; l < 3; l++) {
			if (mod->mins[l] > verts[k].v[l])	mod->mins[l] = verts[k].v[l];
			if (mod->maxs[l] < verts[k].v[l])	mod->maxs[l] = verts[k].v[l];
		}

	for (l = 0; l < 3; l++) {
		mod->mins[l] = mod->mins[l] * frame->scale[l] + frame->translate[l];
		mod->maxs[l] = mod->maxs[l] * frame->scale[l] + frame->translate[l];
		mod->center[l] = (mod->mins[l] + mod->maxs[l]) * 0.5;
	}


	tempr[0] = mod->maxs[0] - mod->mins[0];
	tempr[1] = mod->maxs[1] - mod->mins[1];
	tempr[2] = 0;
	tempv[0] = 0;
	tempv[1] = 0;
	tempv[2] = mod->maxs[2] - mod->mins[2];
	mod->radius = max(VectorLength(tempr), VectorLength(tempv));

	for (i = 0; i < 3; i++)
		mod->center[i] = (mod->maxs[i] + mod->mins[i]) * 0.5;

	// generate st cache for fast md2 rendering
	tris = (dmd2vertex_t *)((byte *)pheader + pheader->ofs_tris);
	mod->st = (float*)malloc(pheader->num_tris * 3 * sizeof(float) * 2);

	if (!mod->st)
	{
		throw std::runtime_error("Mod_LoadAliasMD2Model():: Memory allocation failure.");
	}

	for (l = 0, i = 0; i < pheader->num_tris; i++)
	{
		for (j = 0; j < 3; j++)
		{
			indexST = tris[i].index_st[j];
			mod->st[l++] = poutst[indexST].s;
			mod->st[l++] = poutst[indexST].t;
		}
	}

	glGenBuffers(1, &mod->vboId);
	glBindBuffer(GL_ARRAY_BUFFER, mod->vboId);
	glBufferData(GL_ARRAY_BUFFER, l * sizeof(float), mod->st, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}





/*
==============================================================================

SPRITE MODELS

==============================================================================
*/
/**
 *
 */
void Mod_LoadSpriteModel(Model* mod, void* buffer, int file_length)
{
	dsprite_t *sprin, *sprout;
	int i;

	mod->memorySize = 0;

	sprin = (dsprite_t *)buffer;
	sprout = (dsprite_t*)Hunk_Alloc(file_length);

	mod->memorySize += file_length;

	SPRITE_SIZE += mod->memorySize;

	sprout->ident = LittleLong(sprin->ident);
	sprout->version = LittleLong(sprin->version);
	sprout->numFrames = LittleLong(sprin->numFrames);

	if (sprout->version != SPRITE_VERSION)
		VID_Error(ERR_DROP, "%s has wrong version number (%i should be %i)", mod->name, sprout->version, SPRITE_VERSION);

	if (sprout->numFrames > MAX_MD2SKINS)
		VID_Error(ERR_DROP, "%s has too many frames (%i > %i)", mod->name, sprout->numFrames, MAX_MD2SKINS);

	// byte swap everything
	for (i = 0; i < sprout->numFrames; i++) {
		sprout->frames[i].width = LittleLong(sprin->frames[i].width);
		sprout->frames[i].height = LittleLong(sprin->frames[i].height);
		sprout->frames[i].origin_x = LittleLong(sprin->frames[i].origin_x);
		sprout->frames[i].origin_y = LittleLong(sprin->frames[i].origin_y);
		memcpy(sprout->frames[i].name, sprin->frames[i].name,
			MAX_SKINNAME);
		mod->skins[i] = GL_FindImage(sprout->frames[i].name, IT_SPRITE, true);
	}

	mod->type = mod_sprite;
}


/**
 * Specifies the model that will be used as the world.
 */
void R_BeginRegistration(char* model)
{
	char fullname[MAX_QPATH];

	R_IncrementRegistrationSequence();
	r_oldviewcluster = -1; // force markleafs

	Q_sprintf(fullname, sizeof(fullname), "maps/%s.bsp", model);

	// explicitly free the old map if different
	// this guarantees that mod_known[0] is the world map
	ConsoleVariable* flushmap = Cvar_Get("flushmap", "0", 0);
	if (strcmp(MODELS_KNOWN[0].name, fullname) || flushmap->value)
		_freeModel(&MODELS_KNOWN[0]);

	r_worldmodel = _modelForName(fullname, true);
	r_viewcluster = -1;
	numPreCachedLights = 0;
	flareEdit = (bool)false;
}


/**
 * 
 */
Model* R_RegisterModel(char* name)
{
	int len = strlen(name);

	Model* mod = _modelForName(name, false);
	if (!mod)
		return nullptr;

	mod->registration_sequence = R_RegistrationSequence();

	// register any images used by the models
	if (mod->type == mod_sprite)
	{
		dsprite_t* sprite = (dsprite_t*)mod->extraData;
		for (int i = 0; i < sprite->numFrames; i++)
		{
			mod->skins[i] = GL_FindImage(sprite->frames[i].name, IT_SPRITE, true);
			if (!mod->skins[i])
				mod->skins[i] = r_missingTexture;
		}
	}
	else if (mod->type == mod_alias_md3)	// MD3 Models
	{
		Com_Printf("MD3 Model: %s", mod->name);
	}
	else if (mod->type == mod_alias_md2)	// MD2 Models
	{
		dmd2header_t* pheader = (dmd2header_t*)mod->extraData;

		for (int i = 0; i < pheader->num_skins; i++)
		{
			char gl[128];
			char* pname = (char*)pheader + pheader->ofs_skins + i * MAX_SKINNAME;
			mod->skins[i] = GL_FindImage(pname, IT_SKIN, true);

			// GlowMaps loading
			strcpy(gl, pname);
			gl[strlen(gl) - 4] = 0;
			strcat(gl, "_light.tga");
			mod->glowtexture[i] = GL_FindImage(gl, IT_SKIN, true);

			if (!mod->glowtexture[i])
			{
				strcpy(gl, pname);
				gl[strlen(gl) - 4] = 0;
				strcat(gl, "_light.dds");
				mod->glowtexture[i] = GL_FindImage(gl, IT_SKIN, true);
			}

			if (!mod->glowtexture[i])
				mod->glowtexture[i] = r_notexture;

			// Loading Normal maps
			strcpy(gl, pname);
			gl[strlen(gl) - 4] = 0;
			strcat(gl, "_n.tga");
			mod->skins_normal[i] = GL_FindImage(gl, IT_BUMP, true);

			if (!mod->skins_normal[i])
			{
				strcpy(gl, pname);
				gl[strlen(gl) - 4] = 0;
				strcat(gl, "_n.dds");
				mod->skins_normal[i] = GL_FindImage(gl, IT_BUMP, true);
			}

			if (!mod->skins_normal[i])
				mod->skins_normal[i] = r_defBump;

			// Loading roughness maps
			strcpy(gl, pname);
			gl[strlen(gl) - 4] = 0;
			strcat(gl, "_rgh.tga");
			mod->skins_roughness[i] = GL_FindImage(gl, IT_BUMP, true);

			if (!mod->skins_roughness[i])
			{
				strcpy(gl, pname);
				gl[strlen(gl) - 4] = 0;
				strcat(gl, "_rgh.dds");
				mod->skins_roughness[i] = GL_FindImage(gl, IT_BUMP, true);
			}

			if (!mod->skins_roughness[i]) mod->skins_roughness[i] = r_notexture;
		}
		mod->numFrames = pheader->num_frames;
	}

	if (mod->type == mod_brush)
	{
		for (int i = 0; i < mod->numTexInfo; i++)
		{
			mod->texInfo[i].diffuse->registration_sequence = R_RegistrationSequence();

			if (mod->texInfo[i].normal) mod->texInfo[i].normal->registration_sequence = R_RegistrationSequence();
			if (mod->texInfo[i].additive) mod->texInfo[i].additive->registration_sequence = R_RegistrationSequence();
			if (mod->texInfo[i].environment) mod->texInfo[i].environment->registration_sequence = R_RegistrationSequence();
			if (mod->texInfo[i].roughness) mod->texInfo[i].roughness->registration_sequence = R_RegistrationSequence();
		}
	}

	return mod;
}


/**
 * 
 */
void R_EndRegistration()
{
	Model* mod = MODELS_KNOWN;
	for (int i = 0; i < MODELS_NUM_KNOWN; i++, mod++)
	{
		if (!mod->name[0])
			continue;
		
		if (mod->registration_sequence != R_RegistrationSequence())	// don't need this model
			_freeModel(mod);
	}

	GL_FreeUnusedImages();

	int total = BSP_SIZE + ALIAS_SIZE + SPRITE_SIZE;
	
	Com_DPrintf(S_COLOR_YELLOW "Model Memory Allocation\n");
	Com_DPrintf(BAR_BOLD "\n");
	Com_DPrintf("Size of Bsp model memory    " S_COLOR_GREEN "%i" S_COLOR_WHITE " Bytes (" S_COLOR_GREEN "%i" S_COLOR_WHITE " Mb)\n", BSP_SIZE, BSP_SIZE >> 20);
	Com_DPrintf("Size of Alias model memory  " S_COLOR_GREEN "%i" S_COLOR_WHITE " Bytes (" S_COLOR_GREEN "%i" S_COLOR_WHITE " Mb)\n", ALIAS_SIZE, ALIAS_SIZE >> 20);
	Com_DPrintf("Size of Sprite model memory " S_COLOR_GREEN "%i" S_COLOR_WHITE " Bytes (" S_COLOR_GREEN "%i" S_COLOR_WHITE " Mb)\n", SPRITE_SIZE, SPRITE_SIZE >> 20);
	Com_DPrintf("Size of Total model memory  " S_COLOR_GREEN "%i" S_COLOR_WHITE " Bytes (" S_COLOR_GREEN "%i" S_COLOR_WHITE " Mb)\n", total, total >> 20);
	Com_DPrintf(BAR_BOLD "\n");

	BSP_SIZE = 0;
	ALIAS_SIZE = 0;
	SPRITE_SIZE = 0;
	glClear(GL_COLOR_BUFFER_BIT);
	glClearColor(0.0, 0.0, 0.0, 0.0);

	GL_SetDefaultState();
}


/**
 * \todo	Find a sane place for this.
 */
void Mod_FreeAll()
{
	for (int i = 0; i < MODELS_NUM_KNOWN; i++)
	{
		if (MODELS_KNOWN[i].extraDataSize)
			_freeModel(&MODELS_KNOWN[i]);
	}

}
