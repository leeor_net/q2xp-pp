// ==============================================================================
// = Copyright (C) 1997-2001 Id Software, Inc.
// ==============================================================================
// = This program is free software; you can redistribute it and/or modify it under
// = the terms of the GNU General Public License as published by the Free Software
// = Foundation; either version 2 of the License, or (at your option) any later
// = version.
// = 
// = This program is distributed in the hope that it will be useful, but WITHOUT
// = ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// = FOR A PARTICULAR PURPOSE.
// = 
// = See the GNU General Public License for more details.
// = 
// = You should have received a copy of the GNU General Public License along with
// = this program; if not, write to the Free Software Foundation, Inc., 59 Temple
// = Place - Suite 330, Boston, MA  02111-1307, USA.
// ==============================================================================

#pragma once

#include "../client/ref.h"

/*
d*_t structures are on-disk representations
m*_t structures are in-memory
*/

// ==============================================================================
// = CONSTANTS / IMPORTS / VARIABLES
// ==============================================================================
#define Q_INFINITY				1e30f
#define MAX_WORLD_SHADOW_LIGHTS	1024
#define EQUAL_EPSILON			0.000001f
#define MAX_FLARES_VERTEX		(MAX_WORLD_SHADOW_LIGHTS * 4)

#define	SIDE_FRONT				0
#define	SIDE_BACK				1
#define	SIDE_ON					2
#define SIDE_CLIP				3

#define CONTENTS_NODE			-1

#define SHELL_SCALE		        0.5F
#define WEAPON_SHELL_SCALE		0.2F

extern int numPreCachedLights;

extern int r_numIgnoreAutoLights;
extern int r_numAutoLights;


/**
 * 
 */
typedef void(*mod_touch_t)(Model* model);


// ==============================================================================
// = WORLD LIGHTS
// ==============================================================================

/**
 * 
 */
typedef struct worldShadowLight_s
{
	vec3_t		origin;
	vec3_t		angles;
	vec3_t		speed;
	vec3_t		color, startColor;
	vec3_t		mins, maxs;
	vec3_t		corners[8];
	vec2_t		fov;
	mat3_3_t	axis;
	mat4_t		attenMatrix;
	mat4_t		spotMatrix;
	mat4_t		cubeMapMatrix;

	vec3_t		flareOrigin;
	float		flareSize;
	int			flare;

	vec3_t		radius;
	float		_cone;
	float		coneExp;
	float		hotSpot;
	float		distance;
	vec2_t		depthBounds;
	float		maxRad;
	float		fogDensity;

	int			filter, style, area;
	int			isShadow;
	int			isStatic;
	int			isNoWorldModel;
	int			isAmbient;
	int			isFog;
	int			isCone;
	int			scissor[4];
	int			start_off;

	bool	spherical;
	bool	castCaustics;

	cplane_t	frust[6];
	msurface_t	*interaction[MAX_MAP_FACES];
	int			numInteractionSurfs;

	char		targetname[MAX_QPATH];

	byte		vis[MAX_MAP_LEAFS / 8];

	GLuint		vboId;
	GLuint		iboId;
	int			iboNumIndices;

	bool		directVisibled, visible;
	GLuint		occID;
	int			framecount;

	struct worldShadowLight_s *next;
	struct worldShadowLight_s *s_next;
} worldShadowLight_t;


extern worldShadowLight_t* currentShadowLight;


// ==============================================================================
// = BRUSH MODELS
// ==============================================================================

/**
 * 
 */
typedef struct
{
	vec3_t position;
} mvertex_t;


/**
 * 
 */
typedef struct
{
	vec3_t mins, maxs;
	vec3_t origin;				// for sounds or lights
	float radius;
	int headnode;
	int visleafs;				// not including the solid leaf 0
	int firstface, numfaces;
} mmodel_t;


/**
 * 
 */
typedef struct
{
	unsigned short v[2];
	unsigned int cachededgeoffset;
} medge_t;


/**
 * 
 */
typedef struct mleaf_s
{
	// common with node
	int contents;				// wil be a negative contents number
	int visframe;				// node needs to be traversed if current

	float minmaxs[6];			// for bounding box culling

	struct mnode_s *parent;

	// leaf specific
	int cluster;
	int area;

	msurface_t **firstmarksurface;
	int numMarkSurfaces;
} mleaf_t;


// ===================================================================
// Whole model


/**
 * 
 */
typedef enum
{
	mod_bad,
	mod_brush,
	mod_sprite,
	mod_alias_md2,
	mod_alias_md3
} modtype_t;


/**
 * 
 */
typedef struct
{
	float s, t;
} fstvert_t;


/**
 * 
 */
typedef struct
{
	int n[3];
} neighbors_t;


/**
 * 
 */
struct Model
{
	mod_touch_t touch;          // touching a model updates registration sequence, images and VBO's

	char		name[MAX_QPATH];
	int			registration_sequence;
	modtype_t	type;
	int			numFrames;
	int			flags;

	// volume occupied by the model graphics
	vec3_t		mins, maxs, center;
	vec3_t		bbox[8];
	float		radius;

	// solid volume for clipping
	bool		clipbox;
	vec3_t		clipmins, clipmaxs;

	// brush model
	int			firstModelSurface, numModelSurfaces, lightmap;

	int			numSubModels;
	mmodel_t	*subModels;

	int			numPlanes;
	cplane_t	*planes;

	int			numLeafs; /**< Number of visible leafs, not counting 0. */
	mleaf_t		*leafs;

	int			numVertexes;
	mvertex_t	*vertexes;

	int			numEdges;
	medge_t		*edges;

	int			numNodes;
	int			firstNode;
	mnode_t		*nodes;

	int			numTexInfo;
	mtexInfo_t	*texInfo;

	int			numSurfaces;
	msurface_t	*surfaces;

	int			numSurfEdges;
	int			*surfEdges;

	int			numMarkSurfaces;
	msurface_t	**markSurfaces;

	dvis_t		*vis;

	int			lightmap_scale;
	byte		*lightData;
	bool		useXPLM;	/**< 3-vector basis lightmap. */
	bool		useXpVis;


	// for alias models and skins
	Image*		skins[MAX_MD2SKINS];
	Image*		skins_normal[MAX_MD2SKINS];
	Image*		skins_specular[MAX_MD2SKINS];
	Image*		skins_roughness[MAX_MD2SKINS];
	Image*		glowtexture[MAX_MD2SKINS];
	Image*		skin_env[MAX_MD2SKINS];

	int			extraDataSize;
	void*		extraData;

	int			triangles[MAX_TRIANGLES];
	float		*st;
	neighbors_t *neighbours;

	float		ambient;
	float		diffuse;
	float		specular;
	float		alphaShift;
	float		glowCfg[3];
	float		envScale;
	float		modelScale;
	bool		noSelfShadow;
	bool		envMap;

	byte		*normals;
	byte		*binormals;
	byte		*tangents;

	int			*indexArray;
	int			numIndices;

	GLuint		vboId;
	int			memorySize;

	mat3_3_t	axis;
};


// ==============================================================================
// = FUNCTION PROTOTYPES
// ==============================================================================

void Mod_ModelList();

void *Hunk_Begin(int maxsize, char* name);
void *Hunk_Alloc(int size);
int Hunk_End();
void Hunk_Free(void* base);

void Mod_FreeAll();

Model* Mod_LoadingModel();
Model* Mod_KnownModels();
Model* Mod_InlineModels();

int R_RegistrationSequence();
void R_SetRegistrationSequence(int _value);
void R_ModelBounds(Model* model, vec3_t mins, vec3_t maxs);
void R_ModelRadius(Model* model, vec3_t rad);
void R_ModelCenter(Model* model, vec3_t center);
float R_RadiusFromBounds(vec3_t mins, vec3_t maxs);
