/*
Copyright (C) 1997-2001 Id Software, Inc.
Copyright (C) 2004-2015 Quake2xp Team.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// gl_warp.c -- sky and water polygons

#include "r_local.h"

#include "r_image.h"
#include "r_program.h"

#include "../common/string_util.h"

#include "../win32/winquake.h"


extern Model* LOAD_MODEL;
char SKY_NAME[MAX_QPATH];
float skyrotate;
vec3_t skyaxis;
Image* sky_images[6];
static float shadelight[3];
unsigned int	skyCube = -1;

/*
===============
CreateDSTTex

Create the texture which warps texture shaders
===============
*/
void R_DrawWaterPolygons(msurface_t *fa, bool bmodel)
{
	glpoly_t	*p;
	float		*v, ambient, alpha;
	int			i, nv = fa->polys->numVerts;
	int			numIdx = 0, numVerts = 0;

	GL_BindProgram(waterProgram, 0);

	if (fa->texInfo->flags & (SURF_ALPHA))
	{
		alpha = fa->texInfo->diffuse->alpha;
		glUniform1i(water_trans, 1);
	}
	else
	{
		glUniform1i(water_trans, 0);
		alpha = 1.0f;
	}

	ambient = min(r_lightmapScale->value, 0.33f);

	GL_MBind(GL_TEXTURE0, fa->texInfo->diffuse->texnum);
	GL_MBind(GL_TEXTURE1, r_waterNormals[((int)(r_newrefdef.time * 30)) % (MAX_WATER_NORMALS - 1)]->texnum); // magic number, frames per second?
	GL_MBindRect(GL_TEXTURE2, ScreenMap->texnum);
	GL_MBindRect(GL_TEXTURE3, depthMap->texnum);

	glUniform1f(water_deformMul, 0.75);
	glUniform1f(water_thickness, 150.0);
	glUniform2f(water_screenSize, videoWidth(), videoHeight());
	glUniform2f(water_depthParams, r_newrefdef.depthParms[0], r_newrefdef.depthParms[1]);
	glUniform1f(water_colorModulate, r_textureColorScale->value);
	glUniform1f(water_ambient, ambient);

	if (!bmodel)
		glUniformMatrix4fv(water_mvp, 1, false, (const float *)r_newrefdef.modelViewProjectionMatrix);
	else
		glUniformMatrix4fv(water_mvp, 1, false, (const float *)CURRENT_ENTITY->orMatrix);

	if (r_newrefdef.rdflags & RDF_UNDERWATER)
		glUniform1i(water_mirror, 0);
	else
		glUniform1i(water_mirror, 1);

	glUniformMatrix4fv(water_mv, 1, false, (const float *)r_newrefdef.modelViewMatrix);
	glUniformMatrix4fv(water_pm, 1, false, (const float *)r_newrefdef.projectionMatrix);

	glEnableVertexAttribArray(ATT_POSITION);
	glEnableVertexAttribArray(ATT_TEX0);
	glEnableVertexAttribArray(ATT_NORMAL);
	glEnableVertexAttribArray(ATT_TANGENT);
	glEnableVertexAttribArray(ATT_BINORMAL);
	glEnableVertexAttribArray(ATT_COLOR);

	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, wVertexArray);
	glVertexAttribPointer(ATT_TEX0, 2, GL_FLOAT, false, 0, wTexArray);
	glVertexAttribPointer(ATT_COLOR, 4, GL_FLOAT, false, 0, wColorArray);
	glVertexAttribPointer(ATT_NORMAL, 3, GL_FLOAT, false, 0, nTexArray);
	glVertexAttribPointer(ATT_TANGENT, 3, GL_FLOAT, false, 0, tTexArray);
	glVertexAttribPointer(ATT_BINORMAL, 3, GL_FLOAT, false, 0, bTexArray);

	p = fa->polys;
	v = p->verts[0];

	for (i = 0; i < nv - 2; i++)
	{
		indexArray[numIdx++] = numVerts;
		indexArray[numIdx++] = numVerts + i + 1;
		indexArray[numIdx++] = numVerts + i + 2;
	}

	for (i = 0; i < p->numVerts; i++, v += VERTEXSIZE)
	{
		VectorCopy(v, wVertexArray[i]);

		wTexArray[i][0] = v[3];
		wTexArray[i][1] = v[4];

		// normals
		nTexArray[i][0] = v[7];
		nTexArray[i][1] = v[8];
		nTexArray[i][2] = v[9];

		tTexArray[i][0] = v[10];
		tTexArray[i][1] = v[11];
		tTexArray[i][2] = v[12];

		bTexArray[i][0] = v[13];
		bTexArray[i][1] = v[14];
		bTexArray[i][2] = v[15];

		wColorArray[i][3] = alpha;
	}

	glDrawElements(GL_TRIANGLES, numIdx, GL_UNSIGNED_INT, indexArray);

	glDisableVertexAttribArray(ATT_POSITION);
	glDisableVertexAttribArray(ATT_TEX0);
	glDisableVertexAttribArray(ATT_NORMAL);
	glDisableVertexAttribArray(ATT_TANGENT);
	glDisableVertexAttribArray(ATT_BINORMAL);
	glDisableVertexAttribArray(ATT_COLOR);

	GL_BindNullProgram();
}


//===================================================================


vec3_t skyclip[6] =
{
	{  1, 1, 0 },
	{  1,-1, 0 },
	{  0,-1, 1 },
	{  0, 1, 1 },
	{  1, 0, 1 },
	{ -1, 0, 1 }
};
int c_sky;


// 1 = s, 2 = t, 3 = 2048
int st_to_vec[6][3] =
{
	{ 3, -1, 2 },
	{ -3, 1, 2 },

	{ 1, 3, 2 },
	{ -1, -3, 2 },

	{ -2, -1, 3 },				// 0 degrees yaw, look straight up
	{ 2, -1, -3 }					// look straight down
};


int vec_to_st[6][3] =
{
	{ -2, 3, 1 },
	{ 2, 3, -1 },

	{ 1, 3, 2 },
	{ -1, 3, -2 },

	{ -2, -1, 3 },
	{ -2, 1, -3 }
};


float skymins[2][6], skymaxs[2][6];
float sky_min, sky_max;

void DrawSkyPolygon(int nump, vec3_t vecs)
{
	int i, j;
	vec3_t v, av;
	float s, t, dv;
	int axis;
	float *vp;

	c_sky++;

	// decide which face it maps to
	VectorCopy(vec3_origin, v);
	for (i = 0, vp = vecs; i < nump; i++, vp += 3)
	{
		VectorAdd(vp, v, v);
	}
	av[0] = fabs(v[0]);
	av[1] = fabs(v[1]);
	av[2] = fabs(v[2]);
	if (av[0] > av[1] && av[0] > av[2])
	{
		if (v[0] < 0)
			axis = 1;
		else
			axis = 0;
	}
	else if (av[1] > av[2] && av[1] > av[0])
	{
		if (v[1] < 0)
			axis = 3;
		else
			axis = 2;
	}
	else
	{
		if (v[2] < 0)
			axis = 5;
		else
			axis = 4;
	}

	// project new texture coords
	for (i = 0; i < nump; i++, vecs += 3)
	{
		j = vec_to_st[axis][2];
		if (j > 0)
			dv = vecs[j - 1];
		else
			dv = -vecs[-j - 1];
		if (dv < 0.001)
			continue;			// don't divide by zero
		j = vec_to_st[axis][0];
		if (j < 0)
			s = -vecs[-j - 1] / dv;
		else
			s = vecs[j - 1] / dv;
		j = vec_to_st[axis][1];
		if (j < 0)
			t = -vecs[-j - 1] / dv;
		else
			t = vecs[j - 1] / dv;

		if (s < skymins[0][axis])
			skymins[0][axis] = s;
		if (t < skymins[1][axis])
			skymins[1][axis] = t;
		if (s > skymaxs[0][axis])
			skymaxs[0][axis] = s;
		if (t > skymaxs[1][axis])
			skymaxs[1][axis] = t;
	}
}


#define	ON_EPSILON		0.1		// point on plane side epsilon
#define	MAX_CLIP_VERTS	64

void ClipSkyPolygon(int nump, vec3_t vecs, int stage)
{
	float *norm;
	float *v;
	bool front, back;
	float d, e;
	float dists[MAX_CLIP_VERTS];
	int sides[MAX_CLIP_VERTS];
	vec3_t newv[2][MAX_CLIP_VERTS];
	int newc[2];
	int i, j;

	if (nump > MAX_CLIP_VERTS - 2) VID_Error(ERR_DROP, "ClipSkyPolygon: MAX_CLIP_VERTS");

	if (stage == 6) // fully clipped, so draw it
	{
		DrawSkyPolygon(nump, vecs);
		return;
	}

	front = back = false;
	norm = skyclip[stage];
	for (i = 0, v = vecs; i < nump; i++, v += 3)
	{
		d = DotProduct(v, norm);
		if (d > ON_EPSILON)
		{
			front = true;
			sides[i] = SIDE_FRONT;
		}
		else if (d < -ON_EPSILON)
		{
			back = true;
			sides[i] = SIDE_BACK;
		}
		else
			sides[i] = SIDE_ON;
		dists[i] = d;
	}

	if (!front || !back) // not clipped
	{
		ClipSkyPolygon(nump, vecs, stage + 1);
		return;
	}
	// clip it
	sides[i] = sides[0];
	dists[i] = dists[0];
	VectorCopy(vecs, (vecs + (i * 3)));
	newc[0] = newc[1] = 0;

	for (i = 0, v = vecs; i < nump; i++, v += 3)
	{
		switch (sides[i])
		{
		case SIDE_FRONT:
			VectorCopy(v, newv[0][newc[0]]);
			newc[0]++;
			break;
		case SIDE_BACK:
			VectorCopy(v, newv[1][newc[1]]);
			newc[1]++;
			break;
		case SIDE_ON:
			VectorCopy(v, newv[0][newc[0]]);
			newc[0]++;
			VectorCopy(v, newv[1][newc[1]]);
			newc[1]++;
			break;
		}

		if (sides[i] == SIDE_ON || sides[i + 1] == SIDE_ON || sides[i + 1] == sides[i])
			continue;

		d = dists[i] / (dists[i] - dists[i + 1]);
		for (j = 0; j < 3; j++)
		{
			e = v[j] + d * (v[j + 3] - v[j]);
			newv[0][newc[0]][j] = e;
			newv[1][newc[1]][j] = e;
		}

		newc[0]++;
		newc[1]++;
	}

	ClipSkyPolygon(newc[0], newv[0][0], stage + 1);
	ClipSkyPolygon(newc[1], newv[1][0], stage + 1);
}


/*
=================
R_AddSkySurface
=================
*/
void R_AddSkySurface(msurface_t* fa)
{
	int i;
	vec3_t verts[MAX_CLIP_VERTS];
	glpoly_t *p;

	// calculate vertex values for sky box
	for (p = fa->polys; p; p = p->next)
	{
		for (i = 0; i < p->numVerts; i++)
			VectorSubtract(p->verts[i], r_origin, verts[i]);

		ClipSkyPolygon(p->numVerts, verts[0], 0);
	}
}


/*
==============
R_ClearSkyBox
==============
*/
void R_ClearSkyBox()
{
	int i;

	for (i = 0; i < 6; i++)
	{
		skymins[0][i] = skymins[1][i] = 9999;
		skymaxs[0][i] = skymaxs[1][i] = -9999;
	}
}


vec2_t		SkyTexCoordArray[2 * MAX_TRIANGLES];
vec3_t		SkyVertexArray[3 * MAX_TRIANGLES];
vec4_t		SkyColorArray[4 * MAX_TRIANGLES];
index_t		skyIndex[MAX_INDICES];
static int	numVerts, idx;


void MakeSkyVec(float s, float t, int axis)
{
	vec3_t v, b;
	int j, k;

	b[0] = s * 2300;
	b[1] = t * 2300;
	b[2] = 2300;

	for (j = 0; j < 3; j++)
	{
		k = st_to_vec[axis][j];
		if (k < 0)
			v[j] = -b[-k - 1];
		else
			v[j] = b[k - 1];
	}

	// avoid bilerp seam
	s = (s + 1) * 0.5;
	t = (t + 1) * 0.5;

	if (s < sky_min) s = sky_min;
	else if (s > sky_max) s = sky_max;

	if (t < sky_min) t = sky_min;
	else if (t > sky_max) t = sky_max;

	t = 1.0 - t;

	VA_SetElem3(SkyVertexArray[numVerts], v[0], v[1], v[2]);
	VA_SetElem2(SkyTexCoordArray[numVerts], s, t);
	VA_SetElem4(SkyColorArray[numVerts], 1, 1, 1, 1);

	skyIndex[idx++] = numVerts + 0;
	skyIndex[idx++] = numVerts + 1;
	skyIndex[idx++] = numVerts + 3;
	skyIndex[idx++] = numVerts + 3;
	skyIndex[idx++] = numVerts + 1;
	skyIndex[idx++] = numVerts + 2;

	numVerts++;
}


/*
==============
R_DrawSkyBox
==============
*/
int skytexorder[6] = { 0, 2, 1, 3, 4, 5 };
void R_DrawSkyBox(bool color)
{
	if (color)
	{
		GL_BindProgram(genericProgram, 0);
		glUniform1i(gen_sky, 1);
		glUniform1i(gen_3d, 0);
		glUniform1i(gen_attribColors, 0);
		glUniform1f(gen_colorModulate, 1.0f);

		glEnableVertexAttribArray(ATT_TEX0);
		glEnableVertexAttribArray(ATT_COLOR);
		glVertexAttribPointer(ATT_TEX0, 2, GL_FLOAT, false, 0, SkyTexCoordArray);
		glVertexAttribPointer(ATT_COLOR, 4, GL_FLOAT, false, 0, SkyColorArray);
		GL_Enable(GL_POLYGON_OFFSET_FILL);
		GL_PolygonOffset(-1, -1);
	}

	glEnableVertexAttribArray(ATT_POSITION);
	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, SkyVertexArray);

	// check for no sky at all
	if (skyrotate)
	{
		int index = 0;
		for (index; index < 6; index++)
		{
			if (skymins[0][index] < skymaxs[0][index] && skymins[1][index] < skymaxs[1][index])
			{
				break;
			}
		}

		if (index == 6)
			return;	// nothing visible
	}

	if (color) glUniformMatrix4fv(gen_mvp, 1, false, (const float *)r_newrefdef.skyMatrix);
	else glUniformMatrix4fv(null_mvp, 1, false, (const float *)r_newrefdef.skyMatrix);

	for (int i = 0; i < 6; i++)
	{
		if (skyrotate) // hack, forces full sky to draw when rotating
		{
			skymins[0][i] = -1;
			skymins[1][i] = -1;
			skymaxs[0][i] = 1;
			skymaxs[1][i] = 1;
		}

		if (skymins[0][i] >= skymaxs[0][i] || skymins[1][i] >= skymaxs[1][i]) { continue; }

		if (color) { GL_MBind(GL_TEXTURE0, sky_images[skytexorder[i]]->texnum); }

		numVerts = idx = 0;

		MakeSkyVec(skymins[0][i], skymins[1][i], i);
		MakeSkyVec(skymins[0][i], skymaxs[1][i], i);
		MakeSkyVec(skymaxs[0][i], skymaxs[1][i], i);
		MakeSkyVec(skymaxs[0][i], skymins[1][i], i);

		glDrawElements(GL_TRIANGLES, idx, GL_UNSIGNED_SHORT, skyIndex);
	}

	glDisableVertexAttribArray(ATT_POSITION);

	if (color)
	{
		glDisableVertexAttribArray(ATT_TEX0);
		glDisableVertexAttribArray(ATT_COLOR);
		GL_BindNullProgram();
		GL_Disable(GL_POLYGON_OFFSET_FILL);
	}
}


/**
 * 
 * Original suffix's used for skybox:
 * char* suf[6] = { "rt", "bk", "lf", "ft", "up", "dn" };
 */
void R_SetSky(char* name, float rotate, vec3_t axis)
{
	char pathname[MAX_QPATH] = { '\0' };

	strncpy(SKY_NAME, name, sizeof(SKY_NAME) - 1);
	skyrotate = rotate;
	VectorCopy(axis, skyaxis);

	for (int i = 0; i < 6; i++)
	{
		Q_sprintf(pathname, sizeof(pathname), "env/%s%i.tga", SKY_NAME, i);

		sky_images[i] = GL_FindImage(pathname, IT_SKY, true);
		if (!sky_images[i]) sky_images[i] = r_missingTexture;

		sky_min = 0.001953125f;	/// \fixme magic number
		sky_max = 0.998046875f;	/// \fixme magic number
	}
}


/**
 * Loads a sky cube map from disk.
 * 
 * \fixme	Should probably verify that at least one skybox texture
 *			exists before making calls to opengl.
 */
void R_GenSkyCubeMap(char* name)
{
	char _fullpath[MAX_QPATH];

	strncpy(SKY_NAME, name, sizeof(SKY_NAME) - 1);

	glGenTextures(1, &skyCube);
	glBindTexture(GL_TEXTURE_CUBE_MAP, skyCube);

	for (int i = 0; i < 6; i++)	/// \fixme	MAGIC NUMBER
	{
		Q_sprintf(_fullpath, sizeof(_fullpath), "env/%s%i.tga", SKY_NAME, i);
		int w = 0, h = 0;
		byte *pic = nullptr;
		IL_LoadImage(_fullpath, &pic, &w, &h, IL_TGA);
		if (pic)
		{
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, pic);
			free(pic);
		}
	}

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
}
