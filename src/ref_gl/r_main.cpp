/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// r_main.c
#include "GL/glew.h"

#include "r_local.h"

#include "r_bsp.h"
#include "r_image.h"
#include "r_math.h"

#include "../common/common.h"
#include "../common/filesystem.h"
#include "../common/string_util.h"

#include "../win32/win_glimp.h"
#include "../win32/winquake.h"

#include "r_light.h"
#include "r_postprocess.h"
#include "r_program.h"


// ===============================================================================
// = DEFINES
// ===============================================================================
#define MAX_GL_DEBUG_MESSAGES							16

#define GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX			0x9047
#define GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX		0x9048
#define GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX	0x9049
#define GPU_MEMORY_INFO_EVICTION_COUNT_NVX				0x904A
#define GPU_MEMORY_INFO_EVICTED_MEMORY_NVX				0x904B

#define VBO_FREE_MEMORY_ATI								0x87FB
#define TEXTURE_FREE_MEMORY_ATI							0x87FC
#define RENDERBUFFER_FREE_MEMORY_ATI					0x87FD


// ===============================================================================
// = FUNCTION PROTOTYPES
// ===============================================================================
void GL_Strings_f();
void R_GLSLinfo_f();


// ===============================================================================
// = EXTERNALS
// ===============================================================================
extern unsigned d_8to24table[256];	/// \fixme Make this accessible via get/set type functions.


// ===============================================================================
// = LOCAL VARIABLES
// ===============================================================================
bool GL_INITIALIZED = false;
bool OUT_MAP = false;

ConsoleVariable *r_noRefresh;
ConsoleVariable *r_drawEntities;
ConsoleVariable *r_drawWorld;
ConsoleVariable *r_noVis;
ConsoleVariable *r_noCull;
ConsoleVariable *r_leftHand;
ConsoleVariable *r_lightLevel;
ConsoleVariable *r_mode;
ConsoleVariable *r_noBind;
ConsoleVariable *r_cull;
ConsoleVariable *r_vsync;
ConsoleVariable *r_textureMode;

ConsoleVariable *r_imageAutoBump;
ConsoleVariable *r_imageAutoBumpScale;
ConsoleVariable *r_imageAutoSpecularScale;

ConsoleVariable *r_lockPvs;
ConsoleVariable *r_fullScreen;

ConsoleVariable *r_brightness;
ConsoleVariable *r_contrast;
ConsoleVariable *r_saturation;
ConsoleVariable *r_gamma;

ConsoleVariable *vid_ref;

ConsoleVariable	*r_causticIntens;

ConsoleVariable	*r_displayRefresh;

ConsoleVariable	*r_textureColorScale;
ConsoleVariable	*r_textureCompression;
ConsoleVariable	*r_anisotropic;
ConsoleVariable	*r_maxAnisotropy;

ConsoleVariable	*r_shadows;
ConsoleVariable	*r_playerShadow;

ConsoleVariable	*r_multiSamples;
ConsoleVariable	*r_nvSamplesCoverange;
ConsoleVariable	*r_fxaa;
ConsoleVariable	*deathmatch;

ConsoleVariable	*r_drawFlares;
ConsoleVariable	*r_scaleAutoLightColor;
ConsoleVariable	*r_lightWeldThreshold;

ConsoleVariable	*r_customWidth;
ConsoleVariable	*r_customHeight;

ConsoleVariable	*r_bloom;
ConsoleVariable	*r_bloomThreshold;
ConsoleVariable	*r_bloomIntens;
ConsoleVariable	*r_bloomWidth;

ConsoleVariable	*r_ssao;
ConsoleVariable	*r_ssaoIntensity;
ConsoleVariable	*r_ssaoScale;
ConsoleVariable	*r_ssaoBlur;

ConsoleVariable	*r_skipStaticLights;
ConsoleVariable	*r_lightmapScale;
ConsoleVariable	*r_lightsWeldThreshold;
ConsoleVariable	*r_debugLights;
ConsoleVariable	*r_useLightScissors;
ConsoleVariable	*r_useDepthBounds;
ConsoleVariable	*r_specularScale;
ConsoleVariable	*r_ambientSpecularScale;
ConsoleVariable	*r_useRadiosityBump;
ConsoleVariable	*r_zNear;
ConsoleVariable	*r_zFar;

ConsoleVariable	*hunk_bsp;
ConsoleVariable	*hunk_model;
ConsoleVariable	*hunk_sprite;

//ConsoleVariable	*r_vbo;
ConsoleVariable	*r_maxTextureSize;

ConsoleVariable	*r_reliefMapping;
ConsoleVariable	*r_reliefScale;

ConsoleVariable	*r_dof;
ConsoleVariable	*r_dofBias;
ConsoleVariable	*r_dofFocus;

ConsoleVariable	*r_motionBlur;
ConsoleVariable	*r_motionBlurSamples;
ConsoleVariable	*r_motionBlurFrameLerp;

ConsoleVariable	*r_radialBlur;
ConsoleVariable	*r_radialBlurFov;

ConsoleVariable	*r_tbnSmoothAngle;

ConsoleVariable	*r_glDebugOutput;
ConsoleVariable	*r_glMinorVersion;
ConsoleVariable	*r_glMajorVersion;
ConsoleVariable	*r_glCoreProfile;

ConsoleVariable	*r_lightEditor;
ConsoleVariable	*r_cameraSpaceLightMove;

ConsoleVariable	*r_hudLighting;
ConsoleVariable	*r_bump2D;

ConsoleVariable	*r_filmFilter;
ConsoleVariable	*r_filmFilterType; // 0 - technicolor; 1 - sepia
ConsoleVariable	*r_filmFilterNoiseIntens;
ConsoleVariable	*r_filmFilterScratchIntens;
ConsoleVariable	*r_filmFilterVignetIntens;

ConsoleVariable	*r_fixFovStrength; // 0.0 = no hi-fov perspective correction
ConsoleVariable	*r_fixFovDistroctionRatio; // 0.0 = cylindrical distortion ratio. 1.0 = spherical


VBO vbo;
VAO vao;


/**
 * Gets a reference to the VBO.
 */
VBO& getVbo()
{
	return vbo;
}


/**
 * Gets a reference to the VAO.
 */
VAO& getVao()
{
	return vao;
}


gllightmapstate_t gl_lms;

Model* r_worldmodel;

float gldepthmin, gldepthmax;

glconfig_t gl_config;
glstate_t gl_state;
entity_t *CURRENT_ENTITY;
Model* CURRENT_MODEL;

cplane_t frustum[6];

int r_visframecount;			// bumped when going to a new PVS
int r_framecount;				// used for dlight push checking

float v_blend[4];				// final blending color

// view origin
vec3_t vup;
vec3_t vpn;
vec3_t vright;
vec3_t r_origin;


// screen size info
refdef_t r_newrefdef;

int r_viewcluster, r_viewcluster2, r_oldviewcluster, r_oldviewcluster2;
int	occ_framecount;

float SinTableByte[256]; /// \fixme find a sane place for this.

/**
 * convert from Q2 coordinate system (screen depth is X)
 * to OpenGL's coordinate system (screen depth is -Z)
 */
static mat4_t r_flipMatrix = {
	{ 0.f, 0.f, -1.f, 0.f },	// world +X -> screen -Z
	{ -1.f, 0.f,  0.f, 0.f },	// world +Y -> screen -X
	{ 0.f, 1.f,  0.f, 0.f },	// world +Z -> screen +Y (Y=0 being the bottom of the screen)
	{ 0.f, 0.f,  0.f, 1.f }
};





/**
 * 
 */
void DebugOutput(unsigned source, unsigned type, unsigned id, unsigned severity, const char* message)
{
	char debSource[64], debType[64], debSev[64];

	if (severity == GL_DEBUG_SEVERITY_NOTIFICATION)
		return;

	if (source == GL_DEBUG_SOURCE_API_ARB)
		strcpy(debSource, "OpenGL");
	else if (source == GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB)
		strcpy(debSource, "Windows");
	else if (source == GL_DEBUG_SOURCE_SHADER_COMPILER_ARB)
		strcpy(debSource, "Shader Compiler");
	else if (source == GL_DEBUG_SOURCE_THIRD_PARTY_ARB)
		strcpy(debSource, "Third Party");
	else if (source == GL_DEBUG_SOURCE_APPLICATION_ARB)
		strcpy(debSource, "Application");
	else if (source == GL_DEBUG_SOURCE_OTHER_ARB)
		strcpy(debSource, "Other");
	else
		strcpy(debSource, va("Source 0x%X", source));

	if (type == GL_DEBUG_TYPE_ERROR_ARB)
		strcpy(debType, "Error");
	else if (type == GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB)
		strcpy(debType, "Deprecated behavior");
	else if (type == GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB)
		strcpy(debType, "Undefined behavior");
	else if (type == GL_DEBUG_TYPE_PORTABILITY_ARB)
		strcpy(debType, "Portability");
	else if (type == GL_DEBUG_TYPE_PERFORMANCE_ARB)
		strcpy(debType, "Performance");
	else if (type == GL_DEBUG_TYPE_OTHER_ARB)
		strcpy(debType, "Other");
	else
		strcpy(debType, va("Type 0x%X", type));

	if (severity == GL_DEBUG_SEVERITY_HIGH_ARB)
		strcpy(debSev, "High");
	else if (severity == GL_DEBUG_SEVERITY_MEDIUM_ARB)
		strcpy(debSev, "Medium");
	else if (severity == GL_DEBUG_SEVERITY_LOW_ARB)
		strcpy(debSev, "Low");
	else
		strcpy(debSev, va("0x%X", severity));

	Com_Printf("GL_DEBUG: %s %s\nSeverity '%s': '%s'\nID: '%d'\n", debSource, debType, debSev, message, id);
}


/**
 * 
 */
void GL_CheckError(const char* fileName, int line, const char* subr)
{
	int         err;
	char        s[128];

	if (!r_glDebugOutput->value)
		return;

	err = glGetError();
	if (err == GL_NO_ERROR)
		return;

	if (gl_state.debugOutput)
	{
		unsigned        sources[MAX_GL_DEBUG_MESSAGES];
		unsigned        types[MAX_GL_DEBUG_MESSAGES];
		unsigned        ids[MAX_GL_DEBUG_MESSAGES];
		unsigned        severities[MAX_GL_DEBUG_MESSAGES];
		int             lengths[MAX_GL_DEBUG_MESSAGES];
		char            messageLog[2048];
		unsigned        count = MAX_GL_DEBUG_MESSAGES;
		int             bufsize = 2048;

		unsigned retVal = glGetDebugMessageLogARB(count, bufsize, sources, types, ids, severities, lengths, messageLog);
		if (retVal > 0)
		{
			unsigned pos = 0;
			for (unsigned i = 0; i < retVal; i++)
			{
				DebugOutput(sources[i], types[i], ids[i], severities[i], &messageLog[pos]);
				pos += lengths[i];
			}
		}
	}

	switch (err)
	{
	case GL_INVALID_ENUM:
		strcpy(s, "GL_INVALID_ENUM");
		break;
	case GL_INVALID_VALUE:
		strcpy(s, "GL_INVALID_VALUE");
		break;
	case GL_INVALID_OPERATION:
		strcpy(s, "GL_INVALID_OPERATION");
		break;
	case GL_STACK_OVERFLOW:
		strcpy(s, "GL_STACK_OVERFLOW");
		break;
	case GL_STACK_UNDERFLOW:
		strcpy(s, "GL_STACK_UNDERFLOW");
		break;
	case GL_OUT_OF_MEMORY:
		strcpy(s, "GL_OUT_OF_MEMORY");
		break;
	case GL_INVALID_FRAMEBUFFER_OPERATION:
		strcpy(s, "GL_INVALID_FRAMEBUFFER_OPERATION_EXT");
		break;
	default:
		Q_sprintf(s, sizeof(s), "0x%X", err);
		break;
	}

	Com_Printf("GL_CheckErrors: %s in file '%s' subroutine '%s' line %i\n", s, fileName, subr, line);
}


/**
 * \fixme	Hrmm...
 */
void R_DrawSpriteModel(entity_t* e)
{
	return;
}


/**
 * 
 */
static void R_DrawDistortSpriteModel(entity_t* e)
{
	dsprframe_t *frame;
	float		*up, *right;
	dsprite_t	*psprite;
	int			vert = 0;
	int		len;

	psprite = (dsprite_t *)CURRENT_MODEL->extraData;
	e->frame %= psprite->numFrames;
	frame = &psprite->frames[e->frame];

	len = frame->width;

	// normal sprite
	up = vup;
	right = vright;

	if (CURRENT_MODEL->skins[0] == 0)
		VID_Error(ERR_DROP, "nullptr SKIN");

	GL_MBind(GL_TEXTURE1, CURRENT_MODEL->skins[e->frame]->texnum);

	glUniform1f(ref_alpha, e->alpha);
	glUniform1f(ref_thickness, len * 0.5);

	glUniform1f(ref_thickness2, len * 0.5);

	VectorScaleAndAdd(e->origin, up, wVertexArray[vert + 0], -frame->origin_y);
	VectorScaleAndAdd(wVertexArray[vert + 0], right, wVertexArray[vert + 0], -frame->origin_x);
	VA_SetElem2(wTexArray[vert + 0], 0, 1);

	VectorScaleAndAdd(e->origin, up, wVertexArray[vert + 1], frame->height - frame->origin_y);
	VectorScaleAndAdd(wVertexArray[vert + 1], right, wVertexArray[vert + 1], -frame->origin_x);
	VA_SetElem2(wTexArray[vert + 1], 0, 0);

	VectorScaleAndAdd(e->origin, up, wVertexArray[vert + 2], frame->height - frame->origin_y);
	VectorScaleAndAdd(wVertexArray[vert + 2], right, wVertexArray[vert + 2], frame->width - frame->origin_x);
	VA_SetElem2(wTexArray[vert + 2], 1, 0);

	VectorScaleAndAdd(e->origin, up, wVertexArray[vert + 3], -frame->origin_y);
	VectorScaleAndAdd(wVertexArray[vert + 3], right, wVertexArray[vert + 3], frame->width - frame->origin_x);
	VA_SetElem2(wTexArray[vert + 3], 1, 1);

	vert += 4;

	if (vert)
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, nullptr);
}


/**
 * \fixme	Hrmm...
 */
void R_DrawNullModel()
{
	return;
}


/**
 * 
 */
void R_SetupFrame()
{
	mleaf_t* leaf;

	r_framecount++;
	occ_framecount++;

	// build the transformation matrix for the given view angles
	VectorCopy(r_newrefdef.vieworg, r_origin);

	AngleVectors(r_newrefdef.viewangles, vpn, vright, vup);

	// current viewcluster
	if (!(r_newrefdef.rdflags & RDF_NOWORLDMODEL))
	{
		r_oldviewcluster = r_viewcluster;
		r_oldviewcluster2 = r_viewcluster2;
		leaf = BSP_PointInLeaf(r_origin, r_worldmodel);
		r_viewcluster = r_viewcluster2 = leaf->cluster;

		// check above and below so crossing solid water doesn't draw wrong
		if (!leaf->contents) // look down a bit
		{
			vec3_t temp;

			VectorCopy(r_origin, temp);
			temp[2] -= 16;
			leaf = BSP_PointInLeaf(temp, r_worldmodel);
			if (!(leaf->contents & CONTENTS_SOLID) && (leaf->cluster != r_viewcluster2))
				r_viewcluster2 = leaf->cluster;
		}
		else // look up a bit
		{
			vec3_t temp;

			VectorCopy(r_origin, temp);
			temp[2] += 16;
			leaf = BSP_PointInLeaf(temp, r_worldmodel);
			if (!(leaf->contents & CONTENTS_SOLID) && (leaf->cluster != r_viewcluster2))
				r_viewcluster2 = leaf->cluster;
		}
	}

	if (CL_PMpointcontents(r_origin) & CONTENTS_SOLID)
		OUT_MAP = true;
	else
		OUT_MAP = false;

	for (int i = 0; i < 4; i++)
		v_blend[i] = r_newrefdef.blend[i];

	// clear out the portion of the screen that the NOWORLDMODEL defines
	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
	{
		GL_Enable(GL_SCISSOR_TEST);
		GL_Scissor(r_newrefdef.viewport[0], r_newrefdef.viewport[1], r_newrefdef.viewport[2], r_newrefdef.viewport[3]);

		if (!(r_newrefdef.rdflags & RDF_NOCLEAR))
		{
			glClearColor(0.0, 0.0, 0.0, 0.0);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		}
		else
			glClear(GL_DEPTH_BUFFER_BIT);

		GL_Disable(GL_SCISSOR_TEST);
	}
}


/**
 * 
 */
static void R_SetupViewMatrices()
{
	mat4_t tmpMatrix;

	// setup perspective projection matrix
	r_newrefdef.projectionMatrix[0][0] = 1.f / tan(DEG2RAD(r_newrefdef.fov_x) * 0.5f);
	r_newrefdef.projectionMatrix[0][1] = 0.f;
	r_newrefdef.projectionMatrix[0][2] = 0.f;
	r_newrefdef.projectionMatrix[0][3] = 0.f;

	r_newrefdef.projectionMatrix[1][0] = 0.f;
	r_newrefdef.projectionMatrix[1][1] = 1.f / tan(DEG2RAD(r_newrefdef.fov_y) * 0.5f);
	r_newrefdef.projectionMatrix[1][2] = 0.f;
	r_newrefdef.projectionMatrix[1][3] = 0.f;

	r_newrefdef.projectionMatrix[2][0] = 0.f;
	r_newrefdef.projectionMatrix[2][1] = 0.f;
	r_newrefdef.projectionMatrix[2][2] = -0.999f; // infinite
	r_newrefdef.projectionMatrix[2][3] = -1.f;

	r_newrefdef.projectionMatrix[3][0] = 0.f;
	r_newrefdef.projectionMatrix[3][1] = 0.f;
	r_newrefdef.projectionMatrix[3][2] = -2.f * r_zNear->value; // infinite
	r_newrefdef.projectionMatrix[3][3] = 0.f;

	r_newrefdef.depthParms[0] = r_zNear->value;
	r_newrefdef.depthParms[1] = 0.9995f;

	// setup view matrix
	AnglesToMat3(r_newrefdef.viewangles, r_newrefdef.axis);
	Mat4_SetupTransform(r_newrefdef.modelViewMatrix, r_newrefdef.axis, r_newrefdef.vieworg);
	Mat4_AffineInvert(r_newrefdef.modelViewMatrix, tmpMatrix);
	Mat4_Multiply(tmpMatrix, r_flipMatrix, r_newrefdef.modelViewMatrix);

	// scissors transform
	Mat4_Multiply(r_newrefdef.modelViewMatrix, r_newrefdef.projectionMatrix, r_newrefdef.modelViewProjectionMatrix);
	Mat4_Transpose(r_newrefdef.modelViewProjectionMatrix, r_newrefdef.modelViewProjectionMatrixTranspose);

	// set sky matrix
	Mat4_Copy(r_newrefdef.modelViewMatrix, tmpMatrix);
	Mat4_Translate(tmpMatrix, r_newrefdef.vieworg[0], r_newrefdef.vieworg[1], r_newrefdef.vieworg[2]);
	if (skyrotate)
		Mat4_Rotate(tmpMatrix, r_newrefdef.time * skyrotate, skyaxis[0], skyaxis[1], skyaxis[2]);

	Mat4_Multiply(tmpMatrix, r_newrefdef.projectionMatrix, r_newrefdef.skyMatrix);
}


/**
 * 
 */
void R_SetupEntityMatrix(entity_t* e)
{
	AnglesToMat3(e->angles, e->axis);
	Mat4_SetOrientation(e->matrix, e->axis, e->origin);
	Mat4_TransposeMultiply(e->matrix, r_newrefdef.modelViewProjectionMatrix, e->orMatrix);

	if ((e->flags & RF_WEAPONMODEL) && (r_leftHand->value == 1.0F)) // Flip player weapon
	{
		Mat4_Scale(e->orMatrix, 1.0, -1.0, 1.0);
		GL_CullFace(GL_BACK);
	}
}


/**
 * Set up a 2D projection matrix.
 */
void R_SetupOrthoMatrix()
{
	// set 2D virtual screen size
	glViewport(0, 0, videoWidth(), videoHeight());

	r_newrefdef.orthoMatrix[0][0] = 2.0f / (float)videoWidth();
	r_newrefdef.orthoMatrix[0][1] = 0.0f;
	r_newrefdef.orthoMatrix[0][2] = 0.0f;
	r_newrefdef.orthoMatrix[0][3] = 0.0f;

	r_newrefdef.orthoMatrix[1][0] = 0.0f;
	r_newrefdef.orthoMatrix[1][1] = -2.0f / (float)videoHeight();
	r_newrefdef.orthoMatrix[1][2] = 0.0f;
	r_newrefdef.orthoMatrix[1][3] = 0.0f;

	r_newrefdef.orthoMatrix[2][0] = 0.0f;
	r_newrefdef.orthoMatrix[2][1] = 0.0f;
	r_newrefdef.orthoMatrix[2][2] = -1.0f;
	r_newrefdef.orthoMatrix[2][3] = 0.0f;

	r_newrefdef.orthoMatrix[3][0] = -1.0f;
	r_newrefdef.orthoMatrix[3][1] = 1.0f;
	r_newrefdef.orthoMatrix[3][2] = 0.0f;
	r_newrefdef.orthoMatrix[3][3] = 1.0f;
	
	GL_Disable(GL_DEPTH_TEST);
	GL_Disable(GL_CULL_FACE);
}


/**
 * 
 */
void R_SetupGL()
{
	// set drawing parms
	GL_CullFace(GL_FRONT);
	GL_Enable(GL_CULL_FACE);
	GL_Disable(GL_BLEND);
	GL_Enable(GL_DEPTH_TEST);
	GL_DepthMask(1);
}


/**
 * 
 */
void R_Clear()
{
	glClear(GL_DEPTH_BUFFER_BIT|GL_COLOR_BUFFER_BIT);
	gldepthmin = 0.0;
	gldepthmax = 1.0;

	GL_DepthFunc(GL_LEQUAL); 
	GL_DepthRange(gldepthmin, gldepthmax);
}


/**
 * 
 */
void R_DrawPlayerWeaponLightPass()
{
	if (!r_drawEntities->value)
		return;

	GL_DepthFunc(GL_LEQUAL);
	GL_StencilFunc(GL_EQUAL, 128, 255);
	GL_StencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	GL_StencilMask(0);

	for (int i = 0; i < r_newrefdef.num_entities; i++)	// weapon model
	{
		CURRENT_ENTITY = &r_newrefdef.entities[i];
		CURRENT_MODEL = CURRENT_ENTITY->model;
		if (CURRENT_ENTITY->flags & RF_TRANSLUCENT)
			continue;

		if (!CURRENT_MODEL)
			continue;
		if (CURRENT_MODEL->type != mod_alias_md2)
			continue;
		if (CURRENT_MODEL->type != mod_alias_md3)
			continue;
		if (!(CURRENT_ENTITY->flags & RF_WEAPONMODEL))
			continue;

		//R_DrawAliasModelLightPass(true);
	}
}


/**
 * 
 */
void R_DrawPlayerWeaponAmbient()
{
	if (!r_drawEntities->value)
		return;

	// draw non-transparent first
	for (int i = 0; i < r_newrefdef.num_entities; i++)
	{
		CURRENT_ENTITY = &r_newrefdef.entities[i];

		if (CURRENT_ENTITY->flags & RF_TRANSLUCENT)
			continue;

		CURRENT_MODEL = CURRENT_ENTITY->model;

		if (!(CURRENT_ENTITY->flags & RF_WEAPONMODEL))
			continue;

		if (CURRENT_MODEL->type == mod_alias_md2)
			R_DrawAliasMD2Model(CURRENT_ENTITY);
		else if (CURRENT_MODEL->type == mod_alias_md3)
			R_DrawAliasMD3Model(CURRENT_ENTITY);
	}

	// draw transluscent shells
	GL_Enable(GL_BLEND);
	GL_BlendFunc(GL_ONE, GL_ONE);
	GL_DepthMask(0);
	for (int i = 0; i < r_newrefdef.num_entities; i++)
	{
		CURRENT_ENTITY = &r_newrefdef.entities[i];

		if (!(CURRENT_ENTITY->flags & RF_TRANSLUCENT)) continue;

		CURRENT_MODEL = CURRENT_ENTITY->model;

		if (!(CURRENT_ENTITY->flags & RF_WEAPONMODEL)) continue;

		if (CURRENT_MODEL->type == mod_alias_md2)
			R_DrawAliasMD2Model(CURRENT_ENTITY);
		else if (CURRENT_MODEL->type == mod_alias_md3)
			R_DrawAliasMD3Model(CURRENT_ENTITY);
	}

	GL_Disable(GL_BLEND);
	GL_DepthMask(1);
}


/**
 * 
 */
void R_DrawLightScene()
{
	NUM_VIS_LIGHTS = 0;

	GL_DepthMask(0);
	GL_Enable(GL_BLEND);
	GL_BlendFunc(GL_ONE, GL_ONE /*GL_DST_COLOR, GL_ZERO*/);

	if (!(r_newrefdef.rdflags & RDF_NOWORLDMODEL))
	{
		if (r_useLightScissors->value)
			GL_Enable(GL_SCISSOR_TEST);

		if (gl_state.depthBoundsTest && r_useDepthBounds->value)
			GL_Enable(GL_DEPTH_BOUNDS_TEST_EXT);

		if (r_shadows->value)
			GL_Enable(GL_STENCIL_TEST);
	}

	R_PrepareShadowLightFrame(false);

	if (shadowLight_frame)
	{
		for (currentShadowLight = shadowLight_frame; currentShadowLight; currentShadowLight = currentShadowLight->next)
		{
			if (r_skipStaticLights->value && currentShadowLight->isStatic /*&& currentShadowLight->style == 0*/) continue;

			UpdateLightEditor();

			R_SetViewLightScreenBounds();

			if (r_useLightScissors->value)
				GL_Scissor(currentShadowLight->scissor[0], currentShadowLight->scissor[1], currentShadowLight->scissor[2], currentShadowLight->scissor[3]);

			if (gl_state.depthBoundsTest && r_useDepthBounds->value)
				GL_DepthBoundsTest(currentShadowLight->depthBounds[0], currentShadowLight->depthBounds[1]);

			if (!(r_newrefdef.rdflags & RDF_NOWORLDMODEL))
			{
				glClearStencil(128);
				GL_StencilMask(255);
				glClear(GL_STENCIL_BUFFER_BIT);
			}

			R_CastBspShadowVolumes();		// bsp and bmodels shadows
			R_CastAliasShadowVolumes(true);	// player models shadows

			// only bsp shadows for entity!!! 
			for (int i = 0; i < r_newrefdef.num_entities; i++)
			{
				CURRENT_ENTITY = &r_newrefdef.entities[i];

				if (CURRENT_ENTITY->flags & RF_WEAPONMODEL) continue;
				if (CURRENT_ENTITY->flags & RF_TRANSLUCENT) continue;
				if (CURRENT_ENTITY->flags & RF_DISTORT) continue;

				CURRENT_MODEL = CURRENT_ENTITY->model;

				if (!CURRENT_MODEL)
				{
					R_DrawNullModel();
					continue;
				}
				if (CURRENT_MODEL->type == mod_alias_md2)
					R_DrawAliasMD2ModelLightPass(false);
				else if (CURRENT_MODEL->type == mod_alias_md3)
					R_DrawAliasMD3ModelLightPass(false);
			}

			R_CastAliasShadowVolumes(false);	// alias models shadows
			R_DrawLightWorld();					// light world

			NUM_VIS_LIGHTS++;

			//brush models light pass
			for (int i = 0; i < r_newrefdef.num_entities; i++)
			{
				CURRENT_ENTITY = &r_newrefdef.entities[i];

				if (CURRENT_ENTITY->flags & RF_WEAPONMODEL) continue;
				if (CURRENT_ENTITY->flags & RF_TRANSLUCENT) continue;
				if (CURRENT_ENTITY->flags & RF_DISTORT) continue;

				CURRENT_MODEL = CURRENT_ENTITY->model;

				if (!CURRENT_MODEL)
				{
					R_DrawNullModel();
					continue;
				}
				if (CURRENT_MODEL->type == mod_brush)
					R_DrawLightBrushModel();
			}

			R_DrawLightFlare();				// light flare
			R_DrawLightBounds();			// debug stuff
		}
	}

	GL_DepthMask(1);
	GL_Disable(GL_STENCIL_TEST);
	GL_Disable(GL_SCISSOR_TEST);
	if (gl_state.depthBoundsTest && r_useDepthBounds->value)
		GL_Disable(GL_DEPTH_BOUNDS_TEST_EXT);
	GL_Disable(GL_BLEND);
}


/**
 * 
 */
void R_DrawPlayerWeapon()
{
	if (!r_drawEntities->value || r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		return;

	R_DrawPlayerWeaponAmbient();

	GL_DepthMask(0);
	GL_Enable(GL_BLEND);
	GL_BlendFunc(GL_ONE, GL_ONE);

	if (r_useLightScissors->value)
		GL_Enable(GL_SCISSOR_TEST);

	if (gl_state.depthBoundsTest && r_useDepthBounds->value)
		GL_Enable(GL_DEPTH_BOUNDS_TEST_EXT);

	if (r_shadows->value)
		GL_Enable(GL_STENCIL_TEST);

	R_PrepareShadowLightFrame(true);

	if (shadowLight_frame)
	{
		for (currentShadowLight = shadowLight_frame; currentShadowLight; currentShadowLight = currentShadowLight->next)
		{
			if (r_skipStaticLights->value && currentShadowLight->isStatic /*&& currentShadowLight->style == 0*/)
				continue;

			R_SetViewLightScreenBounds();

			if (r_useLightScissors->value)
				GL_Scissor(currentShadowLight->scissor[0], currentShadowLight->scissor[1], currentShadowLight->scissor[2], currentShadowLight->scissor[3]);

			if (gl_state.depthBoundsTest && r_useDepthBounds->value)
				GL_DepthBoundsTest(currentShadowLight->depthBounds[0], currentShadowLight->depthBounds[1]);

			glClearStencil(128);
			GL_StencilMask(255);
			glClear(GL_STENCIL_BUFFER_BIT);

			R_CastBspShadowVolumes();
			R_DrawPlayerWeaponLightPass();
		}
	}

	GL_DepthMask(1);
	GL_Disable(GL_STENCIL_TEST);
	GL_Disable(GL_SCISSOR_TEST);
	if (gl_state.depthBoundsTest && r_useDepthBounds->value)
		GL_Disable(GL_DEPTH_BOUNDS_TEST_EXT);
	GL_Disable(GL_BLEND);
}


/**
 * 
 */
void R_RenderSprites()
{
	GL_DepthMask(0);
	GL_Enable(GL_BLEND);
	GL_BlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo.ibo_quadTris);
	glEnableVertexAttribArray(ATT_POSITION);
	glEnableVertexAttribArray(ATT_TEX0);

	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, wVertexArray);
	glVertexAttribPointer(ATT_TEX0, 2, GL_FLOAT, false, 0, wTexArray);

	// setup program
	GL_BindProgram(refractProgram, 0);

	GL_MBind(GL_TEXTURE0, r_distort->texnum);
	GL_MBindRect(GL_TEXTURE2, ScreenMap->texnum);
	GL_MBindRect(GL_TEXTURE3, depthMap->texnum);

	glUniform1f(ref_deformMul, 9.5);
	glUniformMatrix4fv(ref_mvp, 1, false, (const float *)r_newrefdef.modelViewProjectionMatrix);
	glUniformMatrix4fv(ref_mvm, 1, false, (const float *)r_newrefdef.modelViewMatrix);
	glUniformMatrix4fv(ref_pm, 1, false, (const float *)r_newrefdef.projectionMatrix);

	glUniform2f(ref_viewport, videoWidth(), videoHeight());
	glUniform2f(ref_depthParams, r_newrefdef.depthParms[0], r_newrefdef.depthParms[1]);
	glUniform2f(ref_mask, 0.0, 1.0);
	glUniform1i(ref_alphaMask, 1);

	for (int i = 0; i < r_newrefdef.num_entities; i++)
	{
		CURRENT_ENTITY = &r_newrefdef.entities[i];
		CURRENT_MODEL = CURRENT_ENTITY->model;

		if (!CURRENT_MODEL) continue;
		if (r_newrefdef.rdflags & RDF_IRGOGGLES) continue;
		if (CURRENT_MODEL->type == mod_sprite) R_DrawDistortSpriteModel(CURRENT_ENTITY);
	}

	glDisableVertexAttribArray(ATT_POSITION);
	glDisableVertexAttribArray(ATT_TEX0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	GL_BindNullProgram();
	GL_Disable(GL_BLEND);
	GL_DepthMask(1);
}


/**
 * draws ambient opaque entities
 */
static void R_DrawEntitiesOnList()
{
	if (!r_drawEntities->value)
		return;

	// draw non-transparent first
	for (int i = 0; i < r_newrefdef.num_entities; i++)
	{
		CURRENT_ENTITY = &r_newrefdef.entities[i];

		if (CURRENT_ENTITY->flags & (RF_TRANSLUCENT | RF_WEAPONMODEL) || ((CURRENT_ENTITY->flags & RF_DISTORT) && !(r_newrefdef.rdflags & RDF_IRGOGGLES)))
			continue;
		if (CURRENT_ENTITY->flags & (RF_SHELL_RED | RF_SHELL_GREEN | RF_SHELL_BLUE | RF_SHELL_DOUBLE | RF_SHELL_HALF_DAM | RF_SHELL_GOD))
			continue;
		if (CURRENT_ENTITY->flags & RF_BEAM)
		{
			R_DrawBeam();
			continue;
		}

		CURRENT_MODEL = CURRENT_ENTITY->model;

		if (!CURRENT_MODEL)
		{
			R_DrawNullModel();
			continue;
		}

		switch (CURRENT_MODEL->type)
		{
		case mod_alias_md2:
			R_DrawAliasMD2Model(CURRENT_ENTITY);
			break;
		case mod_alias_md3:
			R_DrawAliasMD3Model(CURRENT_ENTITY);
			break;
		case mod_brush:
			R_DrawBrushModel();
			break;
		case mod_sprite:
			R_DrawSpriteModel(CURRENT_ENTITY);
			break;
		default:
			VID_Error(ERR_DROP, "Bad modeltype");
			break;
		}
	}

	GL_Enable(GL_BLEND);
	GL_BlendFunc(GL_ONE, GL_ONE);

	for (int i = 0; i < r_newrefdef.num_entities; i++)
	{
		CURRENT_ENTITY = &r_newrefdef.entities[i];

		if (!(CURRENT_ENTITY->flags & RF_TRANSLUCENT)) continue;
		if (CURRENT_ENTITY->flags & (RF_WEAPONMODEL)) continue;

		if (CURRENT_ENTITY->flags & RF_BEAM)
		{
			R_DrawBeam();
			continue;
		}

		CURRENT_MODEL = CURRENT_ENTITY->model;

		if (!CURRENT_MODEL)
		{
			R_DrawNullModel();
			continue;
		}

		if (CURRENT_MODEL->type == mod_alias_md2)
			R_DrawAliasMD2Model(CURRENT_ENTITY);
		else if (CURRENT_MODEL->type == mod_alias_md3)
			R_DrawAliasMD3Model(CURRENT_ENTITY);
	}

	GL_Disable(GL_BLEND);
}


/**
 * draw all opaque, non-reflective stuff.
 * fills reflective & alpha chains from world model
 */
static void R_DrawAmbientScene()
{
	if (!(r_newrefdef.rdflags & RDF_NOWORLDMODEL))
		GL_DepthMask(0);

	R_DrawBSP();
	R_DrawEntitiesOnList();

	if (!(r_newrefdef.rdflags & RDF_NOWORLDMODEL))
		GL_DepthMask(1);
}


/**
 * Draws reflective & alpha chains from world model.
 * 
 * Draws reflective surfaces from brush models.
 */
static void R_DrawRAScene()
{
	GL_DepthMask(0);
	GL_PolygonOffset(-1.0, 1.0);

	R_DrawChainsRA(false);

	GL_DepthMask(1);
	GL_PolygonOffset(0.0, 1.0);

	R_CaptureColorBuffer();

	if (!r_drawEntities->value)
		return;

	for (int i = 0; i < r_newrefdef.num_entities; i++)
	{
		CURRENT_ENTITY = &r_newrefdef.entities[i];
		CURRENT_MODEL = CURRENT_ENTITY->model;

		if (CURRENT_MODEL && CURRENT_MODEL->type == mod_brush)
		{
			R_DrawBrushModelRA();
			continue;
		}
	}
}


/**
 * r_newrefdef must be set before the first call.
 */
void R_RenderView(refdef_t *fd)
{
	if (r_noRefresh->value)
		return;

	r_newrefdef = *fd;
	
	r_newrefdef.viewport[0] = fd->x;
	r_newrefdef.viewport[1] = videoHeight() - fd->height - fd->y;
	r_newrefdef.viewport[2] = fd->width;
	r_newrefdef.viewport[3] = fd->height;
	glViewport(r_newrefdef.viewport[0], r_newrefdef.viewport[1], r_newrefdef.viewport[2], r_newrefdef.viewport[3]);

	if (!r_worldmodel && !(r_newrefdef.rdflags & RDF_NOWORLDMODEL))
		VID_Error(ERR_DROP, "R_RenderView: nullptr worldmodel.");

	R_SetupFrame();
	R_SetFrustum();
	R_SetupViewMatrices();
	R_SetupGL();
	R_MarkLeaves(); // done here so we know if we're in water

	R_DrawDepthScene();
	R_CaptureDepthBuffer();

	if (r_ssao->value && !(r_newrefdef.rdflags & (RDF_NOWORLDMODEL | RDF_IRGOGGLES)))
	{
		R_SetupOrthoMatrix();
		GL_DepthMask(0);
		R_DownsampleDepth();
		R_SSAO();

		GL_Enable(GL_CULL_FACE);
		GL_Enable(GL_DEPTH_TEST);
		GL_DepthMask(1);
		glViewport(r_newrefdef.viewport[0], r_newrefdef.viewport[1], r_newrefdef.viewport[2], r_newrefdef.viewport[3]);
	}

	R_DrawAmbientScene();
	R_DrawLightScene();
	R_RenderDecals();
	R_DrawParticles();
	R_CaptureColorBuffer();
	R_DrawRAScene();
	R_DrawParticles();

	if (r_motionBlur->value)
	{
		R_SetupOrthoMatrix();
		GL_DepthMask(0);

		R_MotionBlur();

		GL_Enable(GL_CULL_FACE);
		GL_Enable(GL_DEPTH_TEST);
		GL_DepthMask(1);
		glViewport(r_newrefdef.viewport[0], r_newrefdef.viewport[1], r_newrefdef.viewport[2], r_newrefdef.viewport[3]);
	}

	R_DrawPlayerWeapon();
	R_CaptureColorBuffer();
	R_RenderSprites();
	R_CaptureColorBuffer();

	GL_CheckError(__FILE__, __LINE__, "end frame");
}


/**
 * 
 */
void R_SetLightLevel()
{
	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		return;

	// save off light value for server to look at (BIG HACK!)
	vec3_t amb;
	R_LightPoint(r_newrefdef.vieworg, amb);

	float mid = max(max(amb[0], amb[1]), amb[2]);
	if (mid <= 0.1)
		mid = 0.15;

	mid *= 2.0;
	r_lightLevel->value = 150 * mid; // convert to byte
}


static void drawLightInfo()
{
	Set_FontShader(true);
	Draw_String(8, videoHeight() / 2, string_format("Origin: %i %i %i", (int)getCurrentSelectedLight()->origin[0], (int)getCurrentSelectedLight()->origin[1], (int)getCurrentSelectedLight()->origin[2]));
	Draw_String(8, videoHeight() / 2 + 10, string_format("Color: %.3f %.3f %.3f", getCurrentSelectedLight()->color[0], getCurrentSelectedLight()->color[1], getCurrentSelectedLight()->color[2]));
	Draw_String(8, videoHeight() / 2 + 20, string_format("Radius: %i %i %i", (int)getCurrentSelectedLight()->radius[0], (int)getCurrentSelectedLight()->radius[1], (int)getCurrentSelectedLight()->radius[2]));
	Draw_String(8, videoHeight() / 2 + 30, string_format("Style: %i", getCurrentSelectedLight()->style));
	Draw_String(8, videoHeight() / 2 + 40, string_format("Filter: %i", getCurrentSelectedLight()->filter));
	Draw_String(8, videoHeight() / 2 + 50, string_format("Angles: %i %i %i", (int)getCurrentSelectedLight()->angles[0], (int)getCurrentSelectedLight()->angles[1], (int)getCurrentSelectedLight()->angles[2]));
	Draw_String(8, videoHeight() / 2 + 60, string_format("Speed: %.3f %.3f %.3f", getCurrentSelectedLight()->speed[0], getCurrentSelectedLight()->speed[1], getCurrentSelectedLight()->speed[2]));
	Draw_String(8, videoHeight() / 2 + 70, string_format("Shadow: %i", getCurrentSelectedLight()->isShadow));
	Draw_String(8, videoHeight() / 2 + 80, string_format("Ambient: %i", getCurrentSelectedLight()->isAmbient));
	Draw_String(8, videoHeight() / 2 + 90, string_format("Cone: %.2f", getCurrentSelectedLight()->_cone));
	Draw_String(8, videoHeight() / 2 + 100, string_format("Flare: %i; Flare Editing is %i", getCurrentSelectedLight()->flare, (int)flareEdit));
	Draw_String(8, videoHeight() / 2 + 110, string_format("Flare Size: %i", (int)getCurrentSelectedLight()->flareSize));
	Draw_String(8, videoHeight() / 2 + 120, string_format("Target Name: %s", getCurrentSelectedLight()->targetname));
	Draw_String(8, videoHeight() / 2 + 130, string_format("Start Off: %i", getCurrentSelectedLight()->start_off));
	Draw_String(8, videoHeight() / 2 + 140, string_format("Fog Light: %i", getCurrentSelectedLight()->isFog));
	Draw_String(8, videoHeight() / 2 + 150, string_format("Fog Density: %5f", getCurrentSelectedLight()->fogDensity));
	Set_FontShader(false);
}


/**
 * 
 */
void R_RenderFrame(refdef_t * fd)
{
	R_RenderView(fd);
	R_SetupOrthoMatrix();
	R_SetLightLevel();

	// post processing - cut off if player camera is out of map bounds
	if (!OUT_MAP)
	{
		R_FixFov();
		R_FXAA();
		R_RadialBlur();
		R_ThermalVision();
		R_DofBlur();
		R_Bloom();
		R_FilmFilter();
		R_ScreenBlend();
	}

	// set alpha blend for 2D mode
	GL_Enable(GL_BLEND);
	GL_BlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	if (r_lightEditor->value && R_LightSelected())
	{
		drawLightInfo();
	}
}


/**
 * 
 */
void AutoLightsStatsList_f()
{
	Com_Printf("%i raw auto lights\n", r_numAutoLights);
	Com_Printf("%i clean auto lights\n", r_numIgnoreAutoLights);
	Com_Printf("%i total auto lights\n", r_numAutoLights - r_numIgnoreAutoLights);
}


/**
 * 
 */
void Dump_EntityString()
{
	if (!r_worldmodel->name[0])
	{
		Com_Printf(S_COLOR_RED"You must be in a game to dump entity string.\n");
		return;
	}

	char name[MAX_OSPATH] = { '\0' };
	Q_sprintf(name, sizeof(name), "%s", r_worldmodel->name);

	name[strlen(name) - 4] = 0;
	strcat(name, ".ent");

	Com_Printf("Dump entity string to " S_COLOR_GREEN "%s\n", name);
	FS_MakeDirectory(name);

	char* buf = CM_EntityString();

	FS_File* f = FS_Open(name, FS_OPEN_WRITE);
	if (!f)
	{
		Com_Printf(S_COLOR_RED"ERROR: couldn't open.\n");
		return;
	}

	FS_Write(f, buf, sizeof(byte), strlen(buf) - 1);
	FS_Close(f);
}


/**
 * 
 */
void R_VideoInfo_f()
{
	int mem[4] = { 0 };
	if (GLEW_NVX_gpu_memory_info)
	{
		Com_Printf("\nNvidia specific memory info:\n");
		Com_Printf("\n");
		glGetIntegerv(GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX, mem);
		Com_Printf("dedicated video memory %i MB\n", mem[0] >> 10);

		glGetIntegerv(GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX, mem);
		Com_Printf("total available memory %i MB\n", mem[0] >> 10);

		glGetIntegerv(GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX, mem);
		Com_Printf("currently unused GPU memory %i MB\n", mem[0] >> 10);

		glGetIntegerv(GPU_MEMORY_INFO_EVICTION_COUNT_NVX, mem);
		Com_Printf("count of total evictions seen by system %i MB\n", mem[0] >> 10);

		glGetIntegerv(GPU_MEMORY_INFO_EVICTED_MEMORY_NVX, mem);
		Com_Printf("total video memory evicted %i MB\n", mem[0] >> 10);
	}
	else
	{
		Com_Printf("Memory info not availabled for your video card or driver.\n");
	}
}


/**
 * 512 mb vram
 */
void R_LowSpecMachine_f()
{
	Cvar_Set("r_textureCompression", "1");
	Cvar_Set("r_maxTextureSize", "512");
	Cvar_Set("r_anisotropic", "4");
	Cvar_Set("r_textureMode", "GL_LINEAR_MIPMAP_LINEAR");

	Cvar_Set("r_shadows", "0");
	Cvar_Set("r_drawFlares", "0");
	Cvar_Set("r_reliefMapping", "0");
	Cvar_Set("r_skipStaticLights", "1");
	Cvar_Set("r_lightmapScale", "1.0");
	Cvar_Set("r_bloom", "0");
	Cvar_Set("r_dof", "0");
	Cvar_Set("r_radialBlur", "1");
	Cvar_Set("r_softParticles", "1");
	Cvar_Set("r_motionBlur", "0");
	Cvar_Set("r_ssao", "0");

	vid_ref->modified = true;
}


/**
 * 1gb vram
 */
void R_MediumSpecMachine_f()
{
	Cvar_Set("r_textureCompression", "0");
	Cvar_Set("r_maxTextureSize", "512");
	Cvar_Set("r_anisotropic", "8");
	Cvar_Set("r_textureMode", "GL_LINEAR_MIPMAP_LINEAR");

	Cvar_Set("r_shadows", "1");
	Cvar_Set("r_drawFlares", "1");
	Cvar_Set("r_reliefMapping", "0");
	Cvar_Set("r_skipStaticLights", "0");
	Cvar_Set("r_lightmapScale", "0.5");
	Cvar_Set("r_bloom", "1");
	Cvar_Set("r_dof", "0");
	Cvar_Set("r_radialBlur", "1");
	Cvar_Set("r_softParticles", "1");
	Cvar_Set("r_motionBlur", "0");
	Cvar_Set("r_ssao", "0");

	vid_ref->modified = true;
}


/**
 * 
 */
void R_HiSpecMachine_f()
{
	Cvar_Set("r_textureCompression", "0");
	Cvar_Set("r_maxTextureSize", "0");
	Cvar_Set("r_anisotropic", "16");
	Cvar_Set("r_textureMode", "GL_LINEAR_MIPMAP_LINEAR");

	Cvar_Set("r_shadows", "1");
	Cvar_Set("r_drawFlares", "1");
	Cvar_Set("r_reliefMapping", "1");
	Cvar_Set("r_skipStaticLights", "0");
	Cvar_Set("r_lightmapScale", "0.5");
	Cvar_Set("r_bloom", "1");
	Cvar_Set("r_dof", "1");
	Cvar_Set("r_radialBlur", "1");
	Cvar_Set("r_softParticles", "1");
	Cvar_Set("r_fxaa", "1");
	Cvar_Set("r_motionBlur", "1");
	Cvar_Set("r_ssao", "1");

	vid_ref->modified = true;
}


void R_RegisterCvars()
{
	r_leftHand =						Cvar_Get("hand", "0", CVAR_USERINFO | CVAR_ARCHIVE);
	r_noRefresh =						Cvar_Get("r_noRefresh", "0", 0);
	r_drawEntities =					Cvar_Get("r_drawEntities", "1", 0);
	r_drawWorld =						Cvar_Get("r_drawWorld", "1", 0);
	r_noVis =							Cvar_Get("r_noVis", "0", 0);
	r_noCull =							Cvar_Get("r_noCull", "0", 0);

	r_lightLevel =						Cvar_Get("r_lightLevel", "0", 0);

	r_mode =							Cvar_Get("r_mode", "0", CVAR_ARCHIVE);
	r_noBind =							Cvar_Get("r_noBind", "0", 0);
	r_lockPvs =							Cvar_Get("r_lockPvs", "0", 0);

	r_vsync =							Cvar_Get("r_vsync", "1", CVAR_ARCHIVE);
	
	r_fullScreen =						Cvar_Get("r_fullScreen", "1", CVAR_ARCHIVE);
	
	r_brightness =						Cvar_Get("r_brightness", "1", CVAR_ARCHIVE);
	r_contrast	=						Cvar_Get("r_contrast", "1", CVAR_ARCHIVE);
	r_saturation =						Cvar_Get("r_saturation", "1", CVAR_ARCHIVE);
	r_gamma =							Cvar_Get("r_gamma", "1", CVAR_ARCHIVE);

	vid_ref =							Cvar_Get("vid_ref", "gl", CVAR_ARCHIVE);
	r_displayRefresh =					Cvar_Get("r_displayRefresh", "0", CVAR_ARCHIVE);

	r_anisotropic =						Cvar_Get("r_anisotropic", "16", CVAR_ARCHIVE);
	r_maxAnisotropy =					Cvar_Get("r_maxAnisotropy", "0", 0);
	r_maxTextureSize=					Cvar_Get("r_maxTextureSize", "0", CVAR_ARCHIVE);
	r_textureColorScale =				Cvar_Get("r_textureColorScale", "2", CVAR_ARCHIVE);
	r_textureCompression =				Cvar_Get("r_textureCompression", "0", CVAR_ARCHIVE);			
	r_causticIntens =					Cvar_Get("r_causticIntens", "5.0", CVAR_ARCHIVE);
	r_textureMode =						Cvar_Get("r_textureMode", "GL_LINEAR_MIPMAP_LINEAR", CVAR_ARCHIVE);
	
	r_imageAutoBump	=					Cvar_Get("r_imageAutoBump", "1", CVAR_ARCHIVE);
	r_imageAutoBumpScale =				Cvar_Get("r_imageAutoBumpScale", "6.0", CVAR_ARCHIVE);
	r_imageAutoSpecularScale =			Cvar_Get("r_imageAutoSpecularScale", "1", CVAR_ARCHIVE);

	r_multiSamples =					Cvar_Get("r_multiSamples", "0", CVAR_ARCHIVE);
	r_fxaa =							Cvar_Get("r_fxaa", "1", CVAR_ARCHIVE);

	deathmatch =						Cvar_Get("deathmatch", "0", CVAR_SERVERINFO);
	
	r_drawFlares =						Cvar_Get("r_drawFlares", "1", CVAR_ARCHIVE);
	r_scaleAutoLightColor =				Cvar_Get("r_scaleAutoLightColor", "3", CVAR_ARCHIVE);
	r_lightWeldThreshold =				Cvar_Get("r_lightWeldThreshold", "32", CVAR_ARCHIVE);

	r_customWidth =						Cvar_Get("r_customWidth", "1024", CVAR_ARCHIVE);
	r_customHeight =					Cvar_Get("r_customHeight", "768", CVAR_ARCHIVE);
		
	hunk_bsp =							Cvar_Get("hunk_bsp", "70", CVAR_ARCHIVE);
	hunk_model =						Cvar_Get("hunk_model", "2.4", CVAR_ARCHIVE);
	hunk_sprite =						Cvar_Get("hunk_sprite", "0.08", CVAR_ARCHIVE);

	r_reliefMapping =					Cvar_Get("r_reliefMapping", "1", CVAR_ARCHIVE);
	r_reliefScale=						Cvar_Get("r_reliefScale", "2.0", CVAR_ARCHIVE);

	r_shadows =							Cvar_Get("r_shadows", "1", CVAR_DEVELOPER);
	r_playerShadow =					Cvar_Get("r_playerShadow", "1", CVAR_ARCHIVE);

	r_skipStaticLights =				Cvar_Get("r_skipStaticLights", "0", CVAR_DEVELOPER);
	r_lightmapScale =					Cvar_Get("r_lightmapScale", "0.5", CVAR_ARCHIVE);
	r_useLightScissors = 				Cvar_Get("r_useLightScissors", "1", 0);
	r_useDepthBounds =					Cvar_Get("r_useDepthBounds", "1", 0);
	r_tbnSmoothAngle =					Cvar_Get("r_tbnSmoothAngle", "45", CVAR_ARCHIVE);
	r_lightsWeldThreshold =				Cvar_Get("r_lightsWeldThreshold", "40", CVAR_ARCHIVE);
	r_debugLights =						Cvar_Get("r_debugLights", "0", 0);
	r_specularScale =					Cvar_Get("r_specularScale", "1", CVAR_ARCHIVE);
	r_ambientSpecularScale =			Cvar_Get("r_ambientSpecularScale", "0.3", CVAR_ARCHIVE);
	r_useRadiosityBump =				Cvar_Get("r_useRadiosityBump", "1", CVAR_ARCHIVE);
	r_zNear =							Cvar_Get("r_zNear", "3", CVAR_ARCHIVE);
	r_zFar =							Cvar_Get("r_zFar", "4096", CVAR_ARCHIVE);

	r_bloom =							Cvar_Get("r_bloom", "1", CVAR_ARCHIVE);
	r_bloomThreshold =					Cvar_Get("r_bloomThreshold", "0.75", CVAR_ARCHIVE);
	r_bloomIntens =						Cvar_Get("r_bloomIntens", "0.5", CVAR_ARCHIVE);
	r_bloomWidth =						Cvar_Get("r_bloomWidth", "3.0", CVAR_ARCHIVE);

	r_ssao =							Cvar_Get ("r_ssao", "1", CVAR_ARCHIVE);
	r_ssaoIntensity =					Cvar_Get ("r_ssaoIntensity", "2.0", CVAR_ARCHIVE);
	r_ssaoScale =						Cvar_Get ("r_ssaoScale", "80.0", CVAR_ARCHIVE);
	r_ssaoBlur	=						Cvar_Get ("r_ssaoBlur", "2", CVAR_ARCHIVE);

	r_dof =								Cvar_Get("r_dof", "1", CVAR_ARCHIVE);
	r_dofBias =							Cvar_Get("r_dofBias", "0.002", CVAR_ARCHIVE);
	r_dofFocus =						Cvar_Get("r_dofFocus", "0.0", CVAR_ARCHIVE);

	r_motionBlur =						Cvar_Get("r_motionBlur", "0", CVAR_ARCHIVE);
	r_motionBlurSamples	=				Cvar_Get("r_motionBlurSamples", "32", CVAR_ARCHIVE);
	r_motionBlurFrameLerp =				Cvar_Get("r_motionBlurFrameLerp", "20", CVAR_ARCHIVE);

	r_radialBlur =						Cvar_Get("r_radialBlur", "1", CVAR_ARCHIVE);
	r_radialBlurFov =                   Cvar_Get("r_radialBlurFov", "30", CVAR_ARCHIVE);
	
	r_filmFilter = 						Cvar_Get("r_filmFilter", "0", CVAR_ARCHIVE);
	r_filmFilterType =					Cvar_Get("r_filmFilterType", "0", CVAR_ARCHIVE);
	r_filmFilterNoiseIntens =			Cvar_Get("r_filmFilterNoiseIntens", "0.03", CVAR_ARCHIVE);
	r_filmFilterScratchIntens =			Cvar_Get("r_filmFilterScratchIntens", "0.4", CVAR_ARCHIVE);
	r_filmFilterVignetIntens =			Cvar_Get("r_filmFilterVignetIntens", "0.35", CVAR_ARCHIVE);

	r_glDebugOutput =					Cvar_Get("r_glDebugOutput", "0", 0);
	r_glMajorVersion =					Cvar_Get("r_glMajorVersion", "4", CVAR_ARCHIVE);
	r_glMinorVersion =					Cvar_Get("r_glMinorVersion", "5", CVAR_ARCHIVE);
	r_glCoreProfile =					Cvar_Get("r_glCoreProfile", "0", 0);

	r_lightEditor =						Cvar_Get("r_lightEditor", "0", 0);
	r_cameraSpaceLightMove =			Cvar_Get("r_cameraSpaceLightMove", "0", CVAR_ARCHIVE);

	r_hudLighting =						Cvar_Get("r_hudLighting", "1.5", CVAR_ARCHIVE);
	r_hudLighting->help =				"Intensity of HUD light pass";
	r_bump2D =							Cvar_Get("r_bump2D", "1", CVAR_ARCHIVE);
	r_bump2D->help =					"Draw 2D bump maps";

	r_fixFovStrength =					Cvar_Get("r_fixFovStrength", "0", CVAR_ARCHIVE);
	r_fixFovStrength->help =			"0.0 no perspective correction";
	r_fixFovDistroctionRatio =			Cvar_Get("r_fixFovDistroctionRatio", "5", CVAR_ARCHIVE);
	r_fixFovDistroctionRatio->help =	"Cylindrical distortion ratio";

	Cmd_AddCommand("screenshot",		GL_ScreenShot_f);
	Cmd_AddCommand("modellist",			Mod_ModelList);
	Cmd_AddCommand("openglInfo",		GL_Strings_f);
	Cmd_AddCommand("autoLightsStats",	AutoLightsStatsList_f);
	Cmd_AddCommand("dumpEntityString",	Dump_EntityString);
	Cmd_AddCommand("glslInfo",			R_ListPrograms_f);
	Cmd_AddCommand("r_meminfo",			R_VideoInfo_f);
	Cmd_AddCommand("low_spec",			R_LowSpecMachine_f);
	Cmd_AddCommand("medium_spec",		R_MediumSpecMachine_f);
	Cmd_AddCommand("hi_spec",			R_HiSpecMachine_f);
	Cmd_AddCommand("glsl",				R_GLSLinfo_f);

	Cmd_AddCommand("saveLights",				R_SaveLights_f);
	Cmd_AddCommand("spawnLight",				R_Light_Spawn_f);
	Cmd_AddCommand("spawnLightAtPoint",			R_Light_SpawnAtPoint_f);
	Cmd_AddCommand("spawnLightToCamera",		R_Light_SpawnToCamera_f);
	Cmd_AddCommand("removeLight",				R_Light_Delete_f);
	Cmd_AddCommand("editLight",					R_EditSelectedLight_f);
	Cmd_AddCommand("moveLight_right",			R_MoveLightToRight_f);
	Cmd_AddCommand("moveLight_forward",			R_MoveLightForward_f);
	Cmd_AddCommand("moveLight_z",				R_MoveLightUpDown_f);
	Cmd_AddCommand("changeLightRadius",			R_ChangeLightRadius_f);
	Cmd_AddCommand("cloneLight",				R_Light_Clone_f);
	Cmd_AddCommand("changeLightCone",			R_ChangeLightCone_f);
	Cmd_AddCommand("clearWorldLights",          R_ClearWorldLights);
	Cmd_AddCommand("unselectLight",				R_Light_UnSelect_f);
	Cmd_AddCommand("editFlare",					R_FlareEdit_f);
	Cmd_AddCommand("resetFlarePos",				R_ResetFlarePos_f);
	Cmd_AddCommand("copy",						R_Copy_Light_Properties_f);
	Cmd_AddCommand("paste",						R_Paste_Light_Properties_f);
}


/**
 * 
 */
bool R_SetMode()
{
	const bool fullscreen = r_fullScreen->value != 0;

	r_fullScreen->modified = false;
	r_mode->modified = false;

	rserr_t err = GLimp_SetMode(r_mode->value, fullscreen);

	if (err == rserr_ok)
	{
		Cvar_SetValue("r_fullScreen", gl_state.fullscreen);
		r_fullScreen->modified = false;
		gl_state.prev_mode = r_mode->value;
		return true;
	}
	else if (err == rserr_invalid_fullscreen)
	{
		Com_Printf(S_COLOR_RED "R_SetMode(): Fullscreen unavailable in this mode.\n");
		if ((err = GLimp_SetMode(r_mode->value, false)) == rserr_ok)
		{
			Cvar_SetValue("r_fullScreen", 0);
			r_fullScreen->modified = false;
			gl_state.prev_mode = r_mode->value;
			return true;
		}

	}
	else if (err == rserr_invalid_mode)
	{
		Com_Printf(S_COLOR_RED"R_SetMode(): Invalid mode.\n");
	}

	// revert to previous mode
	if (GLimp_SetMode(gl_state.prev_mode, false) == rserr_ok)
	{
		Cvar_SetValue("r_mode", gl_state.prev_mode);
		r_mode->modified = false;
		Cvar_SetValue("r_fullScreen", 0);
		r_fullScreen->modified = false;
		return true;
	}
	else
	{
		Com_Printf(S_COLOR_RED"R_SetMode(): Could not revert to safe mode.\n");
		return false;
	}
}


/**
 * \fixme	Find a better place for this.
 */
static void DevIL_Init()
{
	///\fixme Not thrilled with this approach.
	static bool init = false;

	if (init) { return; }

	Com_Printf("\n===" S_COLOR_YELLOW "OpenIL library initiation..." S_COLOR_WHITE "===\n\n");

	ilInit();
	iluInit();
	ilutInit();

	ilutRenderer(ILUT_OPENGL);
	ilEnable(IL_ORIGIN_SET);
	ilSetInteger(IL_ORIGIN_MODE, IL_ORIGIN_UPPER_LEFT);

	Con_Printf(PRINT_ALL, "OpenIL VENDOR: " S_COLOR_GREEN " %s\n", ilGetString(IL_VENDOR));
	Con_Printf(PRINT_ALL, "OpenIL Version: " S_COLOR_GREEN "%i\n", ilGetInteger(IL_VERSION_NUM));
	Com_Printf("\n==================================\n\n");

	init = true;
}


/**
 * 
 */
bool R_Init(void *hinstance, void *hWnd)
{
	R_RegisterCvars();

	// create the window and set up the context
	if (!R_SetMode())
	{
		Com_Printf(S_COLOR_RED "R_Init(): Unable to set up video mode.\n");
		return false;
	}

	// Must have a context before attempting to initialize GLEW.
	GLenum _init = glewInit();
	if (_init != GLEW_OK)
	{
		Com_Printf(S_COLOR_RED "R_Init(): %s\n", glewGetErrorString(_init));
		return false;
	}

	// set our "safe" modes
	gl_state.prev_mode = 0;

	Com_Printf("\n" BAR_LITE "\n");
	Com_Printf(S_COLOR_GREEN "OpenGL Initialized\n");
	Com_Printf(BAR_LITE "\n\n");

	gl_config.vendor_string = (const char*)glGetString(GL_VENDOR);
	gl_config.renderer_string = (const char*)glGetString(GL_RENDERER);
	gl_config.version_string = (const char*)glGetString(GL_VERSION);

	Com_Printf(S_COLOR_WHITE "GL_VENDOR:" S_COLOR_GREEN "    %s\n", gl_config.vendor_string);
	Com_Printf(S_COLOR_WHITE "GL_RENDERER:" S_COLOR_GREEN "  %s\n", gl_config.renderer_string);
	Com_Printf(S_COLOR_WHITE "GL_VERSION:" S_COLOR_GREEN "   %s\n", gl_config.version_string);

	Com_Printf("\n" BAR_LITE "\n");
	Com_Printf(S_COLOR_GREEN "Checking OpenGL Extensions\n");
	Com_Printf(BAR_LITE"\n\n");

	// Multitexture is required.
	if (GLEW_ARB_multitexture)
	{
		Com_Printf("Using GL_ARB_multitexture\n");
	}
	else
	{
		Com_Printf(S_COLOR_RED"GL_ARB_multitexture not found\n");
		VID_Error(ERR_FATAL, "GL_ARB_multitexture not found!");
	}

	int max_aniso = 0;
	glGetIntegerv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &max_aniso);
	Cvar_SetValue("r_maxAnisotropy", max_aniso);
	if (r_anisotropic->value >= r_maxAnisotropy->value)
		Cvar_SetValue("r_anisotropic", r_maxAnisotropy->value);

	int aniso_level = r_anisotropic->value;
	if (GLEW_EXT_texture_filter_anisotropic)
	{
		if (r_anisotropic->value <= 1)
		{
			r_anisotropic = Cvar_Set("r_anisotropic", "1");
			Com_Printf(S_COLOR_MAGENTA"Ignoring GL_EXT_texture_filter_anisotropic\n");
		}
		else
		{
			Com_Printf("Using GL_EXT_texture_filter_anisotropic\n");
			Com_Printf("Anisotropic filter: SELECTED (" S_COLOR_GREEN "%i" S_COLOR_WHITE ") MAX: (" S_COLOR_GREEN "%i" S_COLOR_WHITE ")\n", aniso_level, max_aniso);
		}
	}

	gl_state.texture_compression_bptc = false;
	if (GLEW_ARB_texture_compression_bptc)
	{
		if (!r_textureCompression->value)
		{
			Com_Printf(S_COLOR_MAGENTA"Not using GL_ARB_texture_compression_bptc\n");
			gl_state.texture_compression_bptc = false;
		}
		else
		{
			Com_Printf("Using GL_ARB_texture_compression_bptc\n");
			gl_state.texture_compression_bptc = true;
		}
	}
	else
	{
		Com_Printf(S_COLOR_RED"GL_ARB_texture_compression_bptc not found\n");
		gl_state.texture_compression_bptc = false;
	}

	if (GLEW_ARB_seamless_cube_map)
	{
		Com_Printf("Using GL_ARB_seamless_cube_map\n");
		glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
	}
	else
		Com_Printf(S_COLOR_RED"GL_ARB_seamless_cube_map not found\n");

	if (glStencilFuncSeparate && glStencilOpSeparate && glStencilMaskSeparate)
		Com_Printf("Using GL_EXT_stencil_two_side\n");
	else
		Com_Printf(S_COLOR_RED"GL_EXT_stencil_two_side not found\n");

	gl_state.depthBoundsTest = false;
	if (GLEW_EXT_depth_bounds_test)
	{
		Com_Printf("Using GL_EXT_depth_bounds_test\n");
		gl_state.depthBoundsTest = true;
	}
	else
	{
		Com_Printf(S_COLOR_RED"GL_EXT_depth_bounds_test not found\n");
		gl_state.depthBoundsTest = false;
	}

	gl_state.debugOutput = false;

	if (glDebugMessageControlARB && glDebugMessageInsertARB && glDebugMessageCallbackARB && glGetDebugMessageLogARB)
	{
		Com_Printf("Using GL_ARB_debug_output\n");
		gl_state.debugOutput = true;
	}
	else
		Com_Printf(S_COLOR_RED"GL_ARB_debug_output not found\n");

	if (glGenVertexArrays && glDeleteVertexArrays && glBindVertexArray)
		Com_Printf("Using GL_ARB_vertex_array_object\n");

	if (GLEW_ARB_vertex_buffer_object)
	{
		if (glGenBuffers && glBindBuffer && glBufferData && glDeleteBuffers && glBufferSubData && glMapBufferRange)
		{
			vec2_t		tmpVerts[4];
			index_t		iCache[6 * MAX_DRAW_STRING_LENGTH];
			int			idx = 0, i;
			void		*vmap, *imap;
			vmap = nullptr;
			imap = nullptr;

			Com_Printf("Creating GL_ARB_vertex_buffer_object... ");
			// precalc screen quads for postprocessing
			// full quad
			VA_SetElem2(tmpVerts[0], 0, videoHeight());
			VA_SetElem2(tmpVerts[1], videoWidth(), videoHeight());
			VA_SetElem2(tmpVerts[2], videoWidth(), 0);
			VA_SetElem2(tmpVerts[3], 0, 0);
			glGenBuffers(1, &vbo.vbo_fullScreenQuad);
			glBindBuffer(GL_ARRAY_BUFFER, vbo.vbo_fullScreenQuad);
			glBufferData(GL_ARRAY_BUFFER, sizeof(vec2_t) * 4, tmpVerts, GL_STATIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, 0);

			// half quad
			VA_SetElem2(tmpVerts[0], 0, videoHeight());
			VA_SetElem2(tmpVerts[1], videoWidth() * 0.5f, videoHeight());
			VA_SetElem2(tmpVerts[2], videoWidth() * 0.5f, videoHeight() * 0.5f);
			VA_SetElem2(tmpVerts[3], 0, videoHeight() * 0.5f);
			glGenBuffers(1, &vbo.vbo_halfScreenQuad);
			glBindBuffer(GL_ARRAY_BUFFER, vbo.vbo_halfScreenQuad);
			glBufferData(GL_ARRAY_BUFFER, sizeof(vec2_t) * 4, tmpVerts, GL_STATIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, 0);

			// quarter quad
			VA_SetElem2(tmpVerts[0], 0, videoHeight());
			VA_SetElem2(tmpVerts[1], videoWidth() * 0.25f, videoHeight());
			VA_SetElem2(tmpVerts[2], videoWidth() * 0.25f, videoHeight() * 0.25f);
			VA_SetElem2(tmpVerts[3], 0, videoHeight() * 0.25f);
			glGenBuffers(1, &vbo.vbo_quarterScreenQuad);
			glBindBuffer(GL_ARRAY_BUFFER, vbo.vbo_quarterScreenQuad);
			glBufferData(GL_ARRAY_BUFFER, sizeof(vec2_t) * 4, tmpVerts, GL_STATIC_DRAW);
			glBindBuffer(GL_ARRAY_BUFFER, 0);

			for (i = 0, idx = 0; i < MAX_DRAW_STRING_LENGTH * 4; i += 4)
			{
				iCache[idx++] = i + 0;
				iCache[idx++] = i + 1;
				iCache[idx++] = i + 2;
				iCache[idx++] = i + 0;
				iCache[idx++] = i + 2;
				iCache[idx++] = i + 3;
			}

			glGenBuffers(1, &vbo.ibo_quadTris);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo.ibo_quadTris);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, idx * sizeof(ushort), iCache, GL_STATIC_DRAW);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

			glGenBuffers(1, &vbo.vbo_Dynamic);
			glBindBuffer(GL_ARRAY_BUFFER, vbo.vbo_Dynamic);
			glBufferData(GL_ARRAY_BUFFER, MAX_VERTICES * sizeof(vec4_t), 0, GL_DYNAMIC_DRAW);

			glBindBuffer(GL_ARRAY_BUFFER, 0);

			glGenBuffers(1, &vbo.ibo_Dynamic);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo.ibo_Dynamic);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, MAX_INDICES * sizeof(size_t), 0, GL_DYNAMIC_DRAW);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			Com_Printf("done.\n");
		}
	}
	else
	{
		Com_Printf(S_COLOR_RED "GL_ARB_vertex_buffer_object not found\n");
	}

	if (GLEW_ARB_draw_buffers)
	{
		if (glDrawBuffers)
			Com_Printf("Using GL_ARB_draw_buffers\n");
		else
			Com_Printf(S_COLOR_RED"GL_ARB_draw_buffers not found\n");
	}

	if (GLEW_ARB_framebuffer_object)
	{
		Com_Printf("Using GL_ARB_framebuffer_object\n");

		glGetIntegerv(GL_MAX_RENDERBUFFER_SIZE, &gl_state.maxRenderBufferSize);
		glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &gl_state.maxColorAttachments);
		glGetIntegerv(GL_MAX_SAMPLES, &gl_state.maxSamples);


		Com_Printf("   Max Render Buffer Size:  " S_COLOR_YELLOW "%i\n", gl_state.maxRenderBufferSize);
		Com_Printf("   Max Color Attachments:   " S_COLOR_YELLOW "%i\n", gl_state.maxColorAttachments);
		Com_Printf("   Max Buffer Samples:      " S_COLOR_YELLOW "%i\n", gl_state.maxSamples);
		Com_Printf("\n");
		CreateSSAOBuffer();
		Com_Printf("\n");
	}
	else
	{
		Com_Printf(S_COLOR_RED"GL_ARB_framebuffer_object not found\n");
	}

	if (GLEW_ARB_fragment_shader)
		Com_Printf("Using GL_ARB_fragment_shader\n");
	else
		VID_Error(ERR_FATAL, "GL_ARB_fragment_shader not found");

	if (GLEW_ARB_vertex_shader)
		Com_Printf("Using GL_ARB_vertex_shader\n");
	else
		VID_Error(ERR_FATAL, "GL_ARB_vertex_shader not found");

	gl_state.bindlessTexture = false;
	gl_config.shadingLanguageVersionString = (const char*)glGetString(GL_SHADING_LANGUAGE_VERSION);
	glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_COMPONENTS, &gl_config.maxFragmentUniformComponents);
	glGetIntegerv(GL_MAX_VERTEX_UNIFORM_COMPONENTS, &gl_config.maxVertexUniformComponents);
	glGetIntegerv(GL_MAX_VARYING_FLOATS, &gl_config.maxVaryingFloats);
	glGetIntegerv(GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS, &gl_config.maxVertexTextureImageUnits);
	glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &gl_config.maxCombinedTextureImageUnits);
	glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_COMPONENTS, &gl_config.maxFragmentUniformComponents);
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &gl_config.maxVertexAttribs);
	glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &gl_config.maxTextureImageUnits);

	Com_Printf("\n");
	Com_Printf("   GLSL Version:               "	S_COLOR_YELLOW	"   %s\n", gl_config.shadingLanguageVersionString);
	Com_Printf("   maxFragmentUniformComponents:"	S_COLOR_YELLOW	"  %i\n", gl_config.maxFragmentUniformComponents);
	Com_Printf("   maxVertexUniformComponents: "	S_COLOR_YELLOW	"   %i\n", gl_config.maxVertexUniformComponents);
	Com_Printf("   maxVertexAttribs:           "	S_COLOR_YELLOW	"   %i\n", gl_config.maxVertexAttribs);
	Com_Printf("   maxVaryingFloats:           "	S_COLOR_YELLOW	"   %i\n", gl_config.maxVaryingFloats);
	Com_Printf("   maxVertexTextureImageUnits: "	S_COLOR_YELLOW	"   %i\n", gl_config.maxVertexTextureImageUnits);
	Com_Printf("   maxTextureImageUnits:       "	S_COLOR_YELLOW	"   %i\n", gl_config.maxTextureImageUnits);
	Com_Printf("   maxCombinedTextureImageUnits: "	S_COLOR_YELLOW	" %i\n", gl_config.maxCombinedTextureImageUnits);
	Com_Printf("   maxFragmentUniformComponents: "	S_COLOR_YELLOW	" %i\n", gl_config.maxFragmentUniformComponents);

	R_InitPrograms();
	Com_Printf(BAR_BOLD"\n\n");

	DevIL_Init();

	VID_MenuInit();

	GL_SetDefaultState();
	GL_InitImages();
	BSP_Init();
	R_InitEngineTextures();
	R_LoadFont();

	flareEdit = false;
	R_Init_AliasArrays();
	
	for (int i = 0; i < 256; i++)
		SinTableByte[i] = sin((float)i / 255.0 * M_TWOPI);

	GL_INITIALIZED = true;
	return true;
}


/**
 * 
 */
void R_Shutdown()
{
	Cmd_RemoveCommand("modellist");
	Cmd_RemoveCommand("screenshot");
	Cmd_RemoveCommand("imagelist");
	Cmd_RemoveCommand("autoLightsStats");
	Cmd_RemoveCommand("dumpEntityString");
	Cmd_RemoveCommand("r_meminfo");
	Cmd_RemoveCommand("low_spec");
	Cmd_RemoveCommand("medium_spec");
	Cmd_RemoveCommand("hi_spec");
	Cmd_RemoveCommand("gpuInfo");

	Cmd_RemoveCommand("saveLights");
	Cmd_RemoveCommand("spawnLight");
	Cmd_RemoveCommand("removeLight");
	Cmd_RemoveCommand("editLight");
	Cmd_RemoveCommand("spawnLightToCamera");
	Cmd_RemoveCommand("changeLightRadius");
	Cmd_RemoveCommand("cloneLight");
	Cmd_RemoveCommand("changeLightCone");
	Cmd_RemoveCommand("clearWorldLights");
	Cmd_RemoveCommand("unselectLight");
	Cmd_RemoveCommand("editFlare");
	Cmd_RemoveCommand("resetFlarePos");
	Cmd_RemoveCommand("copy");
	Cmd_RemoveCommand("paste");
	Cmd_RemoveCommand("moveLight_right");
	Cmd_RemoveCommand("moveLight_forward");
	Cmd_RemoveCommand("moveLight_z");

	Cmd_RemoveCommand("glsl");
	Cmd_RemoveCommand("glslInfo");
	Cmd_RemoveCommand("openglInfo");

	if (GL_INITIALIZED)
	{
		glDeleteFramebuffers(1, &fboId);
		glDeleteFramebuffers(1, &fbo_weaponMask);

		DeleteShadowVertexBuffers();

		glDeleteBuffers(1, &vbo.vbo_fullScreenQuad);
		glDeleteBuffers(1, &vbo.vbo_halfScreenQuad);
		glDeleteBuffers(1, &vbo.vbo_quarterScreenQuad);
		glDeleteBuffers(1, &vbo.ibo_quadTris);
		glDeleteBuffers(1, &vbo.vbo_Dynamic);
		glDeleteBuffers(1, &vbo.ibo_Dynamic);
		glDeleteBuffers(1, &vbo.vbo_BSP);

		glDeleteVertexArrays(1, &getVao().bsp_a);
		glDeleteVertexArrays(1, &getVao().bsp_l);

		GL_ShutdownImages();
		R_ShutdownPrograms();
	}

	R_ClearWorldLights();
	Mod_FreeAll();
	ilShutDown();

	R_Clean_AliasArrays();

	GLimp_Shutdown();
}


/**
 * 
 */
void R_BeginFrame()
{

#ifndef _WIN32
	// there is no need to restart video mode with SDL
	if (r_fullScreen->modified)
	{
		R_SetMode();
		r_fullScreen->modified = false;
	}
#endif

	/*
	 ** change modes if necessary
	 */
	if (r_mode->modified || r_fullScreen->modified)
		vid_ref->modified = true;

	if (r_dof->modified)	r_dof->modified = false;
	if (r_lightmapScale->modified) r_lightmapScale->modified = false;
	if (r_lightmapScale->value > 1) Cvar_SetValue("r_lightmapScale", 1);
	if (r_lightmapScale->value < 0) Cvar_SetValue("r_lightmapScale", 0);
	if (r_ssao->modified) r_ssao->modified = false;
	if (r_reliefMapping->modified) r_reliefMapping->modified = false;
	if (r_reliefMapping->value > 1) Cvar_SetValue("r_reliefMapping", 1);

	if (r_textureMode->modified || r_anisotropic->modified)
	{
		GL_TextureMode(r_textureMode->string);

		// Do these actually need to be tested to be set to false?
		if (r_textureMode->modified) r_textureMode->modified = false;
		if (r_anisotropic->modified) r_anisotropic->modified = false;
	}

	R_SetupOrthoMatrix();

	GL_Enable(GL_BLEND); // alpha blend for chars
	GL_BlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glDrawBuffer(GL_BACK);

	GL_UpdateSwapInterval();

	R_Clear();
}


/**
 * \fixme	What is this?
 */
void R_DrawBeam()
{
	return;
}
