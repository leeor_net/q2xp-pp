#pragma once

#include "r_local.h"

int R_RegistrationSequence();
void R_IncrementRegistrationSequence();
void R_SetRegistrationSequence(int _value);

void R_ModelBounds(Model* model, vec3_t mins, vec3_t maxs);
void R_ModelRadius(Model* model, vec3_t rad);
void R_ModelCenter(Model* model, vec3_t center);

bool R_HasSharedLeafs(byte* v1, byte* v2);
