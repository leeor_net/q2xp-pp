#pragma once

#include "r_local.h"

#include "../common/image.h"

#include <array>

size_t glGetFreeImageIndex();

int glFilterMin();
int glFilterMax();
int numGlTextures();
void incrementGlTextures();
bool texturesAtMax();

void IL_LoadImage(const char* filename, byte* *pic, int *width, int *height, ILenum type);

Image* GL_LoadPic(const char* name, byte * pic, int width, int height, ImageType type, int bits);
Image* GL_FindImage(const std::string& name, ImageType type, bool load);

void GL_TextureMode(const std::string&);

void GL_InitImages();
void GL_ShutdownImages();

void GL_FreeUnusedImages();

void R_CaptureDepthBuffer();
void R_CaptureColorBuffer();

Image* R_RegisterPlayerBump(const std::string& name);
Image* R_RegisterSkin(const std::string& name);

extern std::array<Image, MAX_GLTEXTURES> gltextures;
