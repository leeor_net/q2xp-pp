#include "r_material.h"

#include "r_image.h"

#include "../common/filesystem.h"
#include "../common/string_util.h"


/**
 * 
 */
void Material_SetDefault(mtexInfo_t* texture)
{
	texture->additive = r_notexture;
	texture->diffuse = r_missingTexture;
	texture->environment = nullptr;
	texture->normal = r_defBump;
	texture->roughness = r_notexture;
}


/**
 * \fixme	This doesn't do a lot of checking for correctness and I doubt
 *			it handles error conditions well.
 */
bool Material_Load(mtexInfo_t* texture, char* path)
{
	//Com_Printf("Loading material for " S_COLOR_GREEN "%s\n", path);

	char _full_path[MAX_QPATH] = { '\0' };
	char _working_dir[MAX_QPATH] = { '\0' };
	char _texture_path[MAX_QPATH] = { '\0' };

	Q_sprintf(_full_path, sizeof(_full_path), "%s.material", path);
	if (!FS_FileExists(_full_path))
	{
		Com_DPrintf("** Material \'" S_COLOR_GREEN "%s" S_COLOR_WHITE "\' was not found.\n", _full_path);
		return false;
	}

	char *stream = nullptr;
	int stream_len = FS_LoadFile(_full_path, reinterpret_cast<void**>(&stream));
	if (!stream)
	{
		Com_Printf("** Failed to load material \'" S_COLOR_GREEN "%s" S_COLOR_WHITE "\'.\n", _full_path);
		return false;
	}

	char *stream_addr = stream;

	float texture_scale_x = 1.0f, texture_scale_y = 1.0f;
	float parallax_scale = 0.0f, specular_scale = 0.0f, specular_exponent = 0.0f;
	float roughness_scale = 0.0f, environment_scale = 0.0f;
	float alpha_blend = 1.0f;
	bool surface_sky = false;
	bool noload = false;

	Com_FilePath(_full_path, _working_dir);

	char* token = nullptr;
	while (stream)
	{
		token = Com_Parse(&stream);

		// ========================================================
		// = TEXTURE PARAMETERS
		// ========================================================
		if (!strcasecmp(token, "parallax_scale"))
		{
			parallax_scale = atof(Com_Parse(&stream));
			continue;
		}

		if (!strcasecmp(token, "specular_scale"))
		{
			specular_scale = atof(Com_Parse(&stream));
			continue;
		}

		if (!strcasecmp(token, "specular_exponent"))
		{
			specular_exponent = atof(Com_Parse(&stream));
			continue;
		}

		if (!strcasecmp(token, "roughness_scale"))
		{
			roughness_scale = atof(Com_Parse(&stream));
			continue;
		}

		if (!strcasecmp(token, "env_scale"))
		{
			environment_scale = atof(Com_Parse(&stream));
			continue;
		}

		if (!strcasecmp(token, "surface_sky"))
		{
			surface_sky = true;
			texture->flags |= MSURF_DRAWSKY;
			return true;	// sky material, forget about anything else.
		}

		if (!strcasecmp(token, "alpha"))
		{
			alpha_blend = atof(Com_Parse(&stream));
			continue;
		}

		if (!strcasecmp(token, "noload"))
		{
			noload = true;
			continue;
		}

		if (!strcasecmp(token, "texture_scale"))
		{
			texture_scale_x = atof(Com_Parse(&stream));
			texture_scale_y = atof(Com_Parse(&stream));

			if (texture_scale_x == 0.0f) texture_scale_x = 1.0f;
			if (texture_scale_y == 0.0f) texture_scale_y = 1.0f;

			continue;
		}

		// ========================================================
		// = TEXTURES
		// ========================================================
		if (!strcasecmp(token, "texture_additive"))
		{
			Q_sprintf(_texture_path, sizeof(_texture_path), "%s/%s", _working_dir, Com_Parse(&stream));
			texture->additive = GL_FindImage(_texture_path, IT_WALL, true);
			if (!texture->additive) texture->additive = r_notexture;

			continue;
		}

		if (!strcasecmp(token, "texture_diffuse"))
		{
			Q_sprintf(_texture_path, sizeof(_texture_path), "%s/%s", _working_dir, Com_Parse(&stream));
			texture->diffuse = GL_FindImage(_texture_path, IT_WALL, true);
			if (!texture->diffuse)
			{
				Com_DPrintf("** Diffuse texture \'token\' defined in \'" S_COLOR_GREEN "%s" S_COLOR_WHITE "\' not found.\n", token, _full_path);
				texture->diffuse = r_missingTexture;
			}
		
			continue;
		}

		if (!strcasecmp(token, "texture_environment"))
		{
			Q_sprintf(_texture_path, sizeof(_texture_path), "%s/%s", _working_dir, Com_Parse(&stream));
			texture->environment = GL_FindImage(_texture_path, IT_WALL, true); // nullptr environment textures are acceptable.

			if (texture->environment)
			{
				texture->diffuse->envMap = true;
				texture->environment->envMap = true;
			}

			continue;
		}

		if (!strcasecmp(token, "texture_normal"))
		{
			Q_sprintf(_texture_path, sizeof(_texture_path), "%s/%s", _working_dir, Com_Parse(&stream));
			texture->normal = GL_FindImage(_texture_path, IT_BUMP, true);
			if (!texture->normal) texture->normal = r_defBump;
			continue;
		}

		if (!strcasecmp(token, "texture_roughness"))
		{
			Q_sprintf(_texture_path, sizeof(_texture_path), "%s/%s", _working_dir, Com_Parse(&stream));
			texture->roughness = GL_FindImage(_texture_path, IT_WALL, true);
			if (!texture->roughness) texture->roughness = r_notexture;
			continue;
		}

		// ========================================================
		// = UNKNOWN TOKEN
		// ========================================================
		else
		{
			if(token && strlen(token))
				Con_Printf(PRINT_DEVELOPER, "** Unknown command \'%s\' found in \'" S_COLOR_GREEN "%s" S_COLOR_WHITE "\'\n", token, _full_path);
		}
	}

	if (texture->diffuse == r_missingTexture)
	{
		if (noload == false)
		{
			Com_DPrintf("** Diffuse texture not set for \'" S_COLOR_GREEN "%s" S_COLOR_WHITE "\'\n", _full_path);
			return false;
		}
	}

	// Set texture parameters here
	texture->diffuse->width = texture->diffuse->upload_width * texture_scale_x;
	texture->diffuse->height = texture->diffuse->upload_height * texture_scale_y;
	texture->diffuse->parallaxScale = parallax_scale;
	texture->diffuse->specularScale = specular_scale;
	texture->diffuse->specularExp = specular_exponent;
	texture->diffuse->envScale = environment_scale;
	texture->diffuse->rghScale = roughness_scale;
	texture->diffuse->alpha = alpha_blend;

	if (alpha_blend != 1.0f)
	{
		texture->flags |= SURF_ALPHA;
	}

	if (texture->additive != r_notexture)
	{
		texture->additive->width = texture->additive->upload_width * texture_scale_x;
		texture->additive->height = texture->additive->upload_height * texture_scale_y;
	}

	if (texture->normal != r_defBump)
	{
		texture->normal->width = texture->normal->upload_width * texture_scale_x;
		texture->normal->height = texture->normal->upload_height * texture_scale_y;
	}

	if (texture->roughness != r_notexture)
	{
		texture->roughness->width = texture->roughness->upload_width * texture_scale_x;
		texture->roughness->height = texture->roughness->upload_height * texture_scale_y;
	}

	FS_FreeFile(stream_addr);

	return true;
}