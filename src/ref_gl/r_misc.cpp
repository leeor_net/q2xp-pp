/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "r_local.h"
#include "noTexture.h"

#include "r_image.h"

#include "../common/filesystem.h"
#include "../common/string_util.h"

#include "../win32/winquake.h"


#ifndef _WIN32
#include <dlfcn.h>
#define qwglGetProcAddress( a ) dlsym( glw_state.hinstOpenGL, a )
#elif defined(_WIN32)
#include "../win32/win_glimp.h"
#endif


#define SCREENSHOTS_DIR			"screenshots"
#define SCREENSHOTS_MAX_COUNT	999


/**
 * 
 */
void IL_LoadImageBuf(char* buffer, int buff_len, byte* *pic, int* width, int* height, ILenum type);


/**
 * R_InitEngineTextures
 */
Image* r_defBump;
Image* r_whiteMap;
Image* r_notexture;
Image* r_missingTexture;
Image* r_caustic[MAX_CAUSTICS];
Image* r_waterNormals[MAX_WATER_NORMALS];
Image* fly[MAX_FLY];
Image* flameanim[MAX_FLAMEANIM];
Image* r_flare;
Image* r_blood[MAX_BLOOD];
Image* r_explode[MAX_EXPLODE];
Image* r_xblood[MAX_BLOOD];
Image* r_distort;
Image* r_texshell[MAX_SHELLS];
Image* r_DSTTex;
Image* r_scanline;
Image* r_envTex;
Image* r_randomNormalTex;
Image* r_lightCubeMap[MAX_FILTERS];
Image* fbo_color0;
Image* skinBump;

Image* depthMap;
Image* ScreenMap;
Image* shadowMask;

Image* fboDN, *fboColor[2];


// TODO use image array
size_t thermaltex;
size_t bloomtex;
size_t fxaatex;
size_t fovCorrTex;

size_t fboId;
size_t fbo_weaponMask;
size_t fboDps;
size_t rboDps;
byte fboColorIndex;


void CreateDSTTex_ARB()
{
	unsigned char dist[16][16][4];
	int x, y;

	for (x = 0; x < 16; x++)
	{
		for (y = 0; y < 16; y++)
		{
			dist[x][y][0] = rand() % 255;
			dist[x][y][1] = rand() % 255;
			dist[x][y][2] = rand() % 48;
			dist[x][y][3] = rand() % 48;
		}
	}

	r_DSTTex = GL_LoadPic("***r_DSTTex***", (byte *)dist, 16, 16, IT_PIC, 24);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, glFilterMin());
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, glFilterMax());
	glGenerateMipmap(GL_TEXTURE_2D);
}


void CreateDepthTexture()
{
	size_t index = glGetFreeImageIndex();
	Image* image = &gltextures[index];

	image->name = "***DepthTex***";

	image->width = videoWidth();
	image->height = videoHeight();
	image->upload_width = videoWidth();
	image->upload_height = videoHeight();
	image->type = IT_PIC;
	image->texnum = TEXNUM_IMAGES + index;

	depthMap = image;

	// create depth texture
	glBindTexture(GL_TEXTURE_RECTANGLE, depthMap->texnum);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_NEAREST); // rectangle!
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_NEAREST); // rectangle!
	glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_DEPTH_COMPONENT24, videoWidth(), videoHeight(), 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, nullptr);
}


void CreateScreenRect()
{
	size_t index = glGetFreeImageIndex();
	Image* image = &gltextures[index];

	image->name = "***ScreenMap***";

	image->width = videoWidth();
	image->height = videoHeight();
	image->upload_width = videoWidth();
	image->upload_height = videoHeight();
	image->type = IT_PIC;
	image->texnum = TEXNUM_IMAGES + TEXNUM_IMAGES + index;

	ScreenMap = image;

	// create screen texture
	glBindTexture(GL_TEXTURE_RECTANGLE, ScreenMap->texnum);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGB, videoWidth(), videoHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
}


/*
=============
R_FB_Check

Framebuffer must be bound.
=============
*/
static void FB_Check(const char* file, const int line)
{
	const char* s = nullptr;
	GLenum		code;

	code = glCheckFramebufferStatus(GL_FRAMEBUFFER);

	// an error occured
	switch (code)
	{
	case GL_FRAMEBUFFER_COMPLETE:
		return;

	case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
		s = "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT";
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
		s = "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT";
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
		s = "GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER";
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
		s = "GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER";
		break;

	case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
		s = "GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE";
		break;

	case GL_FRAMEBUFFER_UNSUPPORTED:
		s = "GL_FRAMEBUFFER_UNSUPPORTED";
		break;

	case GL_FRAMEBUFFER_UNDEFINED:
		s = "GL_FRAMEBUFFER_UNDEFINED";
		break;
	}

	if (s) { Com_Printf("R_FB_Check: %s, line %i: %s\n", file, line, s); }
}

#define _R_FB_Check();		FB_Check(__FILE__, __LINE__);

Image* fboScreen;

void CreateFboBuffer()
{
	size_t index = glGetFreeImageIndex();
	Image* image = &gltextures[index];

	image->name = "***fboScreen***";

	image->width = videoWidth();
	image->height = videoHeight();
	image->upload_width = videoWidth();
	image->upload_height = videoHeight();
	image->type = IT_PIC;
	image->texnum = TEXNUM_IMAGES + index;

	fboScreen = image;
	// attach screen texture
	glBindTexture(GL_TEXTURE_RECTANGLE, fboScreen->texnum);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RG, videoWidth(), videoHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);

	size_t rb = 0;
	glGenRenderbuffers(1, &rb);
	glBindRenderbuffer(GL_RENDERBUFFER, rb);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, videoWidth(), videoHeight());
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	glGenFramebuffers(1, &fboDps);
	glBindFramebuffer(GL_FRAMEBUFFER, fboDps);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rb);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rb);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_RECTANGLE, fboScreen->texnum, 0);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE)
	{
		Com_Printf(S_COLOR_YELLOW "... Create depth-stencil FBO\n");
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


void CreateMiniDepth()
{
	size_t index = glGetFreeImageIndex();
	Image* image = &gltextures[index];

	image->name = "***fboDN***";

	image->width = videoWidth() / 2;
	image->height = videoHeight() / 2;
	image->upload_width = videoWidth() / 2;
	image->upload_height = videoHeight() / 2;
	image->type = IT_PIC;
	image->texnum = TEXNUM_IMAGES + index;

	fboDN = image;

	glBindTexture(GL_TEXTURE_RECTANGLE, fboDN->texnum);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_R16F, videoWidth() / 2, videoHeight() / 2, 0, GL_RED, GL_FLOAT, nullptr);
}


void CreateSsaoColorTextures()
{
	for (int i = 0; i < 2; ++i)
	{
		size_t index = glGetFreeImageIndex();
		Image* image = &gltextures[index];

		image->name = "***fboColor***";

		image->width = videoWidth() / 2;
		image->height = videoHeight() / 2;
		image->upload_width = videoWidth() / 2;
		image->upload_height = videoHeight() / 2;
		image->type = IT_PIC;
		image->texnum = TEXNUM_IMAGES + index;

		fboColor[i] = image;

		glBindTexture(GL_TEXTURE_RECTANGLE, fboColor[i]->texnum);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGB8, videoWidth() / 2, videoHeight() / 2, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
	}
}


void CreateSSAOBuffer()
{
	CreateMiniDepth();
	CreateSsaoColorTextures();

	fboColorIndex = 0;

	glGenFramebuffers(1, &fboId);
	glBindFramebuffer(GL_FRAMEBUFFER, fboId);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_RECTANGLE, fboColor[0]->texnum, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_RECTANGLE, fboColor[1]->texnum, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_RECTANGLE, fboDN->texnum, 0);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		Com_Printf(S_COLOR_RED, "Couldn't create SSAO Frame Buffer");
	else
		Com_Printf("Created SSAO Frame Buffer\n");

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


void CreateShadowMask ()
{
	size_t index = glGetFreeImageIndex();
	Image* image = &gltextures[index];

	image->name = "***shadowMask***";

	image->width = videoWidth();
	image->height = videoHeight();
	image->upload_width = videoWidth();
	image->upload_height = videoHeight();
	image->type = IT_PIC;
	image->texnum = TEXNUM_IMAGES + index;

	shadowMask = image;
	// create shadow mask texture
	glBindTexture (GL_TEXTURE_RECTANGLE, shadowMask->texnum);
	glTexParameteri (GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri (GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri (GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri (GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexImage2D (GL_TEXTURE_RECTANGLE, 0, GL_RGBA, videoWidth(), videoHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
}


char* lsuf[6] = { "ft", "bk", "lf", "rt", "up", "dn" };
unsigned	trans[4096 * 4096];

struct img
{
	byte* pixels = nullptr;
	int width = 0;
	int height = 0;
};


void R_FlipImage(int idx, img* pix, byte* dst)
{
	byte *from;
	byte *src = pix->pixels;
	int	width = pix->width;
	int	height = pix->height;
	int	x, y;

	if (idx == 1)
	{
		for (y = height - 1; y >= 0; y--)
		{
			for (x = width - 1; x >= 0; x--) // copy rgb components
			{
				from = src + (x*height + y) * 4;
				dst[0] = from[0];
				dst[1] = from[1];
				dst[2] = from[2];
				dst[3] = 255;
				dst += 4;
			}
		}

		return;
	}

	if (idx == 2)		// lf
	{
		for (y = height - 1; y >= 0; y--)
		{
			for (x = 0; x < width; x++) // copy rgb components
			{
				from = src + (y*width + x) * 4;
				dst[0] = from[0];
				dst[1] = from[1];
				dst[2] = from[2];
				dst[3] = 255;
				dst += 4;
			}
		}
		return;
	}

	if (idx == 3)		// rt
	{
		for (y = 0; y < height; y++)
		{
			for (x = width - 1; x >= 0; x--) // copy rgb components
			{
				from = src + (y*width + x) * 4;
				dst[0] = from[0];
				dst[1] = from[1];
				dst[2] = from[2];
				dst[3] = 255;
				dst += 4;
			}
		}
		return;
	}

	// ft, up, dn
	for (y = 0; y < height; y++)
	{
		for (x = 0; x < width; x++) // copy rgb components
		{
			from = src + (x*height + y) * 4;
			dst[0] = from[0];
			dst[1] = from[1];
			dst[2] = from[2];
			dst[3] = 255;
			dst += 4;
		}
	}
}


Image* R_LoadLightFilter(int id)
{
	int		i, minw, minh, maxw, maxh;
	char	checkname[MAX_OSPATH];
	img		pix[6];
	byte	*nullpixels;
	bool	allNull = true;

	size_t index = glGetFreeImageIndex();
	Image* image = &gltextures[index];

	image->name = "***Filter" + std::to_string(id + 1) + "***";

	image->registration_sequence = R_RegistrationSequence();
	image->type = IT_PIC;
	image->texnum = TEXNUM_IMAGES + index;

	glBindTexture(GL_TEXTURE_CUBE_MAP, image->texnum);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	minw = minh = 0;
	maxw = maxh = 9999999;
	for (i = 0; i < 6; i++)
	{
		pix[i].pixels = nullptr;
		pix[i].width = pix[i].height = 0;
		Q_sprintf(checkname, sizeof(checkname), "gfx/lights/%i_%s.tga", id + 1, lsuf[i]);

		// Berserker: stop spam
		if (FS_FileExists(checkname))
		{
			IL_LoadImage(checkname, &pix[i].pixels, &pix[i].width, &pix[i].height, IL_TGA);
			if (pix[i].width)
			{
				if (minw < pix[i].width)	minw = pix[i].width;
				if (maxw > pix[i].width)	maxw = pix[i].width;
			}

			if (pix[i].height)
			{
				if (minh < pix[i].height)	minh = pix[i].height;
				if (maxh > pix[i].height)	maxh = pix[i].height;
			}
		}
	}

	if ((minw == 0) || (minh == 0))
	{
		//Com_DPrintf("R_LoadLightFilter: filter %i does not exist\n", id+1);	// Berserker: stop spam
		minw = minh = maxw = maxh = 1;
	}

	if ((minw != maxw) || (minh != maxh) || (minw != minh))
		Com_Error(ERR_DROP, "R_LoadLightFilter: (%i) all images must be quadratic with equal sizes", id + 1);

	for (i = 0; i < 6; i++)
	{
		if (pix[i].pixels)
		{
			allNull = false;
			R_FlipImage(i, &pix[i], (byte*)trans);
			free(pix[i].pixels);
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, minw, minh, 0, GL_RGBA, GL_UNSIGNED_BYTE, /*pix[i].pixels*/ trans);
		}
		else
		{
			nullpixels = (byte*)calloc((minw * minh) * 4, sizeof(byte));
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, minw, minh, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullpixels);
			free(nullpixels);
		}
	}

	image->width = minw;
	image->height = minh;
	image->upload_width = image->width * 6;
	image->upload_height = image->height * 6;

	if (allNull)
		image->registration_sequence = -1;	// free

	return image;
}


void R_InitEngineTextures()
{
	int		i;
	byte	notex[1][1][4] = { 0x00, 0x00, 0x00, 0x00 };
	byte	bump[1][1][4] = { 0x80, 0x80, 0xff, 0x10 };
	byte	white[1][1][4] = { 0xff, 0xff, 0xff, 0xff };

	r_defBump = GL_LoadPic("***r_defBump***", (byte*)bump, 1, 1, IT_BUMP, 32);
	r_whiteMap = GL_LoadPic("***r_whiteMap***", (byte*)white, 1, 1, IT_BUMP, 32);
	r_notexture = GL_LoadPic("***r_notexture***", (byte*)notex, 1, 1, IT_WALL, 32);


	byte* pic = nullptr;
	int width = 0, height = 0;
	IL_LoadImageBuf(reinterpret_cast<char*>(noTexture), NOTEXTURE_LEN, &pic, &width, &height, NOTEXTURE_IMG_TYPE);
	if (!pic)
	{
		VID_Error(ERR_DROP, "R_InitEngineTextures(): failure initializing default texture.");
		return;
	}
	else r_missingTexture = GL_LoadPic("***r_missingTexture***", pic, width, height, IT_WALL, 32);

	r_particletexture[Particle::PT_DEFAULT] = GL_FindImage("gfx/particles/pt_blast.tga", IT_WALL, true);
	r_particletexture[Particle::PT_BUBBLE] = GL_FindImage("gfx/particles/bubble.png", IT_WALL, true);
	r_particletexture[Particle::PT_FLY] = GL_FindImage("gfx/fly/fly0.png", IT_WALL, true);
	r_particletexture[Particle::PT_BLOOD] = GL_FindImage("gfx/particles/blood_trail1.tga", IT_WALL, true);
	r_particletexture[Particle::PT_BLOOD2] = GL_FindImage("gfx/particles/blood_trail2.tga", IT_WALL, true);
	r_particletexture[Particle::PT_BLASTER] = GL_FindImage("gfx/particles/pt_blast.tga", IT_WALL, true);
	r_particletexture[Particle::PT_SMOKE] = GL_FindImage("gfx/particles/smoke.png", IT_WALL, true);
	r_particletexture[Particle::PT_SPLASH] = GL_FindImage("gfx/particles/drop.tga", IT_WALL, true);
	r_particletexture[Particle::PT_SPARK] = GL_FindImage("gfx/particles/spark.tga", IT_WALL, true);

	r_particletexture[Particle::PT_BEAM] = GL_FindImage("gfx/particles/pt_beam.png", IT_WALL, true);
	r_particletexture[Particle::PT_SPIRAL] = GL_FindImage("gfx/particles/pt_blast.tga", IT_WALL, true);
	r_particletexture[Particle::PT_FLAME] = GL_FindImage("gfx/flame/fire_00.tga", IT_WALL, true);
	r_particletexture[Particle::PT_BLOODSPRAY] = GL_FindImage("gfx/particles/blood_hit0.tga", IT_WALL, true);

	r_particletexture[Particle::PT_EXPLODE] = GL_FindImage("gfx/explode/0.png", IT_WALL, true);
	r_particletexture[Particle::PT_WATERPLUME] = GL_FindImage("gfx/particles/water_plume.tga", IT_WALL, true);
	r_particletexture[Particle::PT_WATERCIRCLE] = GL_FindImage("gfx/particles/water_ripples.png", IT_WALL, true);
	r_particletexture[Particle::PT_BLOODDRIP] = GL_FindImage("gfx/particles/blood_drip.tga", IT_WALL, true);
	r_particletexture[Particle::PT_BLOODMIST] = GL_FindImage("gfx/particles/blood_mist.tga", IT_WALL, true);
	r_particletexture[Particle::PT_BLOOD_SPLAT] = GL_FindImage("gfx/decals/decal_splat.tga", IT_WALL, true);
	r_particletexture[Particle::PT_BLASTER_BOLT] = GL_FindImage("gfx/particles/blaster_bolt.tga", IT_WALL, true);

	for (i = 0; i < Particle::PT_MAX; i++)
	{
		if (!r_particletexture[i])
		{
			r_particletexture[i] = r_missingTexture;
		}
	}

	r_decaltexture[DECAL_RAIL] = GL_FindImage("gfx/decals/decal_railgun.tga", IT_WALL, true);
	r_decaltexture[DECAL_BULLET] = GL_FindImage("gfx/decals/decal_bullet2.tga", IT_WALL, true);
	r_decaltexture[DECAL_BLASTER] = GL_FindImage("gfx/decals/decal_blaster2.tga", IT_WALL, true);
	r_decaltexture[DECAL_EXPLODE] = GL_FindImage("gfx/decals/decal_explode.tga", IT_WALL, true);
	r_decaltexture[DECAL_BLOOD1] = GL_FindImage("gfx/decals/decal_blood1.tga", IT_WALL, true);
	r_decaltexture[DECAL_BLOOD2] = GL_FindImage("gfx/decals/decal_blood2.tga", IT_WALL, true);
	r_decaltexture[DECAL_BLOOD3] = GL_FindImage("gfx/decals/decal_blood3.tga", IT_WALL, true);
	r_decaltexture[DECAL_BLOOD4] = GL_FindImage("gfx/decals/decal_blood4.tga", IT_WALL, true);
	r_decaltexture[DECAL_BLOOD5] = GL_FindImage("gfx/decals/decal_blood5.tga", IT_WALL, true);
	r_decaltexture[DECAL_BLOOD6] = GL_FindImage("gfx/decals/decal_blood6.tga", IT_WALL, true);
	r_decaltexture[DECAL_BLOOD7] = GL_FindImage("gfx/decals/decal_blood7.tga", IT_WALL, true);
	r_decaltexture[DECAL_BLOOD8] = GL_FindImage("gfx/decals/decal_blood8.tga", IT_WALL, true);
	r_decaltexture[DECAL_BLOOD9] = GL_FindImage("gfx/decals/decal_splat.tga", IT_WALL, true);
	r_decaltexture[DECAL_ACIDMARK] = GL_FindImage("gfx/decals/decal_acidmark.tga", IT_WALL, true);
	r_decaltexture[DECAL_BFG] = GL_FindImage("gfx/decals/decal_bfg.tga", IT_WALL, true);

	for (i = 0; i < DECAL_MAX; i++)
		if (!r_decaltexture[i])
			r_decaltexture[i] = r_missingTexture;

	// ====================================================
	// = WATER CAUSTICS
	// ====================================================
	for (i = 0; i < MAX_CAUSTICS; i++)
	{
		char name[MAX_QPATH];

		Q_sprintf(name, sizeof(name), "gfx/caust/caust_%i.tga", i);
		r_caustic[i] = GL_FindImage(name, IT_WALL, true);
		if (!r_caustic[i])
			r_caustic[i] = r_missingTexture;
	}

	// ====================================================
	// = WATER NORMALS
	// ====================================================
	for (i = 0; i < MAX_WATER_NORMALS; i++)
	{
		char name[MAX_QPATH];
		Q_sprintf(name, sizeof(name), "water/%i.tga", i);

		r_waterNormals[i] = GL_FindImage(name, IT_WALL, true);
		if (!r_waterNormals[i])
			r_waterNormals[i] = r_notexture;
	}

	// ====================================================
	// = FLY PARTICLE TEXTURES
	// ====================================================
	for (i = 0; i < MAX_FLY; i++)
	{
		char frame[MAX_QPATH];
		Q_sprintf(frame, sizeof(frame), "gfx/fly/fly%i.png", i);
		fly[i] = GL_FindImage(frame, IT_WALL, true);
		if (!fly[i])
			fly[i] = r_missingTexture;
	}

	// ====================================================
	// = FIRE TEXTURES
	// ====================================================
	for (i = 0; i < MAX_FLAMEANIM; i++)
	{
		char frame2[MAX_QPATH];
		Q_sprintf(frame2, sizeof(frame2), "gfx/flame/fire_0%i.tga", i);
		flameanim[i] = GL_FindImage(frame2, IT_WALL, true);
		if (!flameanim[i])
			flameanim[i] = r_missingTexture;
	}

	// ====================================================
	// = BLOOD TEXTURES
	// ====================================================
	for (i = 0; i < MAX_BLOOD; i++)
	{
		char bloodspr[MAX_QPATH];
		Q_sprintf(bloodspr, sizeof(bloodspr), "gfx/particles/blood_hit%i.tga", i);
		r_blood[i] = GL_FindImage(bloodspr, IT_WALL, true);
		if (!r_blood[i])
			r_blood[i] = r_missingTexture;

	}

	// ====================================================
	// = EXPLOSION TEXTURES
	// ====================================================
	for (i = 0; i < MAX_EXPLODE; i++)
	{
		char expl[MAX_QPATH];
		Q_sprintf(expl, sizeof(expl), "gfx/explode/%i.png", i);
		r_explode[i] = GL_FindImage(expl, IT_WALL, true);
		if (!r_explode[i])
			r_explode[i] = r_missingTexture;
	}
	
	// ====================================================
	// = SHELL TEXTURES
	// ====================================================
	for (i = 0; i < MAX_SHELLS; i++)
	{
		char shell[MAX_QPATH];
		Q_sprintf(shell, sizeof(shell), "gfx/shells/shell%i.tga", i);
		r_texshell[i] = GL_FindImage(shell, IT_WALL, true);
		if (!r_texshell[i])
			r_texshell[i] = r_missingTexture;
	}

	r_flare = GL_FindImage("gfx/flares/flare0.tga", IT_WALL, true);
	if (!r_flare) r_flare = r_missingTexture;

	r_distort = GL_FindImage("gfx/distort/explosion.tga", IT_WALL, true);
	if (!r_distort) r_distort = r_missingTexture;

	r_scanline = GL_FindImage("gfx/conback_add.tga", IT_WALL, true);
	if (!r_scanline) r_scanline = r_missingTexture;

	r_envTex = GL_FindImage("gfx/tinfx.png", IT_WALL, true);
	if (!r_envTex) r_envTex = r_missingTexture;

	r_randomNormalTex = GL_FindImage("gfx/random_normal.tga", IT_PIC, true);
	if (!r_randomNormalTex) r_randomNormalTex = r_notexture;

	for (i = 0; i < MAX_GLOBAL_FILTERS; i++)
		r_lightCubeMap[i] = R_LoadLightFilter(i);

	skinBump = GL_FindImage("gfx/skinBlend_n.tga", IT_PIC, true);
	if (!skinBump) skinBump = r_notexture;

	bloomtex = 0;
	thermaltex = 0;
	fxaatex = 0;
	fovCorrTex = 0;

	CreateDSTTex_ARB();
	CreateDepthTexture();
	CreateScreenRect();
}


/**
 * Saves a screenshot.
 */
void GL_ScreenShot_f()
{
	char picname[80] = { '\0' };			// Filename for the screenshot.
	char filepath[MAX_OSPATH] = { '\0' };	// Temp string for checking against existing files.

	FS_MakeDirectory(SCREENSHOTS_DIR);

	/// \fixme This is a naive way of doing this.
	int i = 0;
	for (i = 0; i <= SCREENSHOTS_MAX_COUNT; i++)
	{
		Q_sprintf(picname, sizeof(picname), "%04i.png", i);
		Q_sprintf(filepath, sizeof(filepath), SCREENSHOTS_DIR "/%s", picname);

		if (!FS_FileExists(filepath)) { break; }
	}

	if (i > SCREENSHOTS_MAX_COUNT)
	{
		Com_Printf("GL_ScreenShot_f: Couldn't create a file\n");
		return;
	}

	Q_sprintf(filepath, sizeof(filepath), "data/%s", filepath);

	ILuint images[1] = { 0 };
	ilGenImages(1, images);
	ilBindImage(images[0]);

	if (ilutGLScreen())
	{
		ilSave(IL_PNG, filepath);
		Com_Printf("Wrote %s\n", picname);
	}

	ilDeleteImages(1, images);
}


/**
 * 
 */
void GL_Strings_f()
{
	Com_Printf("\n");
	Com_Printf("GL_VENDOR:    " S_COLOR_GREEN "%s\n", gl_config.vendor_string);
	Com_Printf("GL_RENDERER:  " S_COLOR_GREEN "%s\n", gl_config.renderer_string);
	Com_Printf("GL_VERSION:   " S_COLOR_GREEN "%s\n", gl_config.version_string);

	GLint n = 0;
	glGetIntegerv(GL_NUM_EXTENSIONS, &n);
	Com_Printf("GL_EXTENSIONS:\n");

	for (int i = 0; i < n; i++)
	{
		gl_config.extensions3_string = reinterpret_cast<const char*>(glGetStringi(GL_EXTENSIONS, i));
		Com_Printf(S_COLOR_YELLOW"%s\n", gl_config.extensions3_string);
	}
}


/**
 * GL_SetDefaultState
 */
void GL_SetDefaultState()
{
	glDisable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset(0.0f, 1.0f);
	gl_state.polygonOffsetFill = false;
	gl_state.polygonOffsetFactor = 0.0f;
	gl_state.polygonOffsetUnits = 1.0f;

	glDepthRange(0.f, 1.f);
	gl_state.depthRange[0] = 0.0f;
	gl_state.depthRange[1] = 1.0f;

	// scissor
	glDisable(GL_SCISSOR_TEST);
	glScissor(0, 0, videoWidth(), videoHeight());
	gl_state.scissorTest = false;
	gl_state.scissor[0] = 0;
	gl_state.scissor[1] = 0;
	gl_state.scissor[2] = videoWidth();
	gl_state.scissor[3] = videoHeight();

	// color mask
	glColorMask(1, 1, 1, 1);
	gl_state.colorMask[0] = GL_TRUE;
	gl_state.colorMask[1] = GL_TRUE;
	gl_state.colorMask[2] = GL_TRUE;
	gl_state.colorMask[3] = GL_TRUE;

	// depth test
	glDisable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDepthMask(1);
	gl_state.depthTest = false;
	gl_state.depthFunc = GL_LEQUAL;
	gl_state.depthMask = true;

	// stencil test
	glDisable(GL_STENCIL_TEST);
	glStencilMask(255);
	glStencilFunc(GL_ALWAYS, 128, 255);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	glStencilOpSeparate(GL_FRONT_AND_BACK, GL_KEEP, GL_KEEP, GL_KEEP);
	gl_state.stencilTest = false;
	gl_state.stencilMask = 255;
	gl_state.stencilFunc = GL_ALWAYS;
	gl_state.stencilRef = 128;
	gl_state.stencilRefMask = 255;
	gl_state.stencilFace = GL_FRONT_AND_BACK;
	gl_state.stencilFail = GL_KEEP;
	gl_state.stencilZFail = GL_KEEP;
	gl_state.stencilZPass = GL_KEEP;

	// blending
	glDisable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	gl_state.blend = false;
	gl_state.blendSrc = GL_SRC_ALPHA;
	gl_state.blendDst = GL_ONE_MINUS_SRC_ALPHA;

	// face culling
	glDisable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
	glFrontFace(GL_CCW);
	gl_state.cullFace = false;
	gl_state.cullMode = GL_FRONT;
	gl_state.frontFace = GL_CCW;

	// depth bounds test
	if (gl_state.depthBoundsTest)
	{
		gl_state.glDepthBoundsTest = false;
		glDisable(GL_DEPTH_BOUNDS_TEST_EXT);
		glDepthBoundsEXT(0.f, 1.f);
		gl_state.depthBoundsMins = 0.f;
		gl_state.depthBoundsMax = 1.f;
	}

	glHint(GL_TEXTURE_COMPRESSION_HINT, GL_NICEST);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	GL_TextureMode(r_textureMode->string);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, glFilterMin());
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, glFilterMax());

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	GL_UpdateSwapInterval();
}
