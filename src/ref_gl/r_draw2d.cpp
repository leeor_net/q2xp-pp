/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "r_local.h"
#include "r_image.h"
#include "r_program.h"

#include "../common/string_util.h"
#include "../win32/winquake.h"


// ===============================================================================
// = CONSTANTS
// ===============================================================================

const float	GLPYH_TEXTURE_SIZE = 0.0625f;


// ===============================================================================
// = FUNCTION PROTOTYPES
// ===============================================================================
void Scrap_Upload();


// ===============================================================================
// = LOCAL MODULE VARIABLES
// ===============================================================================
vec2_t	texCoord[MAX_VERTEX_ARRAY];
vec2_t	texCoord1[MAX_VERTEX_ARRAY];
vec3_t	vertCoord[MAX_VERTEX_ARRAY];
vec4_t	colorCoord[MAX_VERTEX_ARRAY];

Image* draw_chars = nullptr;


/**
 * 
 */
void R_LoadFont()
{
	draw_chars = GL_FindImage("gfx/conchars.tga", IT_PIC, true);
	if (!draw_chars)
		VID_Error(ERR_FATAL, "couldn't load gfx/conchars.tga");

	GL_MBind(GL_TEXTURE0, draw_chars->texnum);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
}


/**
 * 
 */
void Set_FontShader(bool enable)
{
	if (enable)
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, getVbo().ibo_quadTris);
		glEnableVertexAttribArray(ATT_POSITION);
		glEnableVertexAttribArray(ATT_TEX0);
		glEnableVertexAttribArray(ATT_COLOR);

		glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, vertCoord);
		glVertexAttribPointer(ATT_TEX0, 2, GL_FLOAT, false, 0, texCoord);
		glVertexAttribPointer(ATT_COLOR, 4, GL_FLOAT, false, 0, colorCoord);

		GL_BindProgram(genericProgram, 0);
		glUniform1i(gen_attribColors, 1);
		glUniform1i(gen_sky, 0);
		glUniform1i(gen_3d, 0);
		glUniform1f(gen_colorModulate, r_textureColorScale->value);
		glUniformMatrix4fv(gen_orthoMatrix, 1, false, (const float *)r_newrefdef.orthoMatrix);
	}
	else
	{
		GL_BindNullProgram();
		glDisableVertexAttribArray(ATT_POSITION);
		glDisableVertexAttribArray(ATT_TEX0);
		glDisableVertexAttribArray(ATT_COLOR);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}
}


/**
 * 
 */
void Draw_Char(int x, int y, unsigned char c)
{
	Draw_CharScaled(x, y, 1.0f, 1.0f, c);
}


/**
 * 
 */
void Draw_CharScaled(int x, int y, float scale_x, float scale_y, unsigned char c)
{
	float frow, fcol, size;

	c &= 255;

	if ((c & 127) == 32) return;	// space

	if (y <= -8 * scale_y) return;	// totally off screen

	int row = c >> 4;
	int col = c & 15;

	frow = row * 0.0625;
	fcol = col * 0.0625;
	size = 0.0625;

	GL_MBind(GL_TEXTURE0, draw_chars->texnum);

	VA_SetElem2(texCoord[0], fcol, frow);
	VA_SetElem2(texCoord[1], fcol + size, frow);
	VA_SetElem2(texCoord[2], fcol + size, frow + size);
	VA_SetElem2(texCoord[3], fcol, frow + size);

	VA_SetElem2(vertCoord[0], x, y);
	VA_SetElem2(vertCoord[1], x + 8 * scale_x, y);
	VA_SetElem2(vertCoord[2], x + 8 * scale_x, y + 8 * scale_y);
	VA_SetElem2(vertCoord[3], x, y + 8 * scale_y);

	VA_SetElem4(colorCoord[0], gl_state.fontColor[0], gl_state.fontColor[1], gl_state.fontColor[2], gl_state.fontColor[3]);
	VA_SetElem4(colorCoord[1], gl_state.fontColor[0], gl_state.fontColor[1], gl_state.fontColor[2], gl_state.fontColor[3]);
	VA_SetElem4(colorCoord[2], gl_state.fontColor[0], gl_state.fontColor[1], gl_state.fontColor[2], gl_state.fontColor[3]);
	VA_SetElem4(colorCoord[3], gl_state.fontColor[0], gl_state.fontColor[1], gl_state.fontColor[2], gl_state.fontColor[3]);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, nullptr);
}


void Draw_String(int x, int y, const std::string& str)
{
	Draw_StringScaled(x, y, 1.0f, 1.0f, str);
}


void Draw_StringScaled(int x, int y, float scale_x, float scale_y, const std::string& str)
{
	if (str.empty()) { return; }

	if (gl_state.currenttextures[gl_state.currenttmu] != draw_chars->texnum)
	{
		GL_MBind(GL_TEXTURE0, draw_chars->texnum);
	}

	int px = x;

	int glyph_index = 0, quadCounter = 0;
	int row = 0, col = 0;
	float u = 0.0f, v = 0.0f;

	const unsigned char* s = reinterpret_cast<const unsigned char*>(str.c_str());

	int glyph_count = 0;
	for (glyph_count; glyph_count < str.size() && glyph_count <= MAX_DRAW_STRING_LENGTH; ++glyph_count)
	{
		glyph_index = *s++;

		row = glyph_index / 16;
		col = glyph_index % 16;

		v = row * GLPYH_TEXTURE_SIZE;
		u = col * GLPYH_TEXTURE_SIZE;

		quadCounter = glyph_count * 4;

		VA_SetElem2(texCoord[quadCounter + 0], u, v);
		VA_SetElem2(texCoord[quadCounter + 1], u + GLPYH_TEXTURE_SIZE, v);
		VA_SetElem2(texCoord[quadCounter + 2], u + GLPYH_TEXTURE_SIZE, v + GLPYH_TEXTURE_SIZE);
		VA_SetElem2(texCoord[quadCounter + 3], u, v + GLPYH_TEXTURE_SIZE);

		VA_SetElem2(vertCoord[quadCounter + 0], px, y);
		VA_SetElem2(vertCoord[quadCounter + 1], px + 8 * scale_x, y);
		VA_SetElem2(vertCoord[quadCounter + 2], px + 8 * scale_x, y + 8 * scale_y);
		VA_SetElem2(vertCoord[quadCounter + 3], px, y + 8 * scale_y);

		VA_SetElem4(colorCoord[quadCounter + 0], gl_state.fontColor[0], gl_state.fontColor[1], gl_state.fontColor[2], gl_state.fontColor[3]);
		VA_SetElem4(colorCoord[quadCounter + 1], gl_state.fontColor[0], gl_state.fontColor[1], gl_state.fontColor[2], gl_state.fontColor[3]);
		VA_SetElem4(colorCoord[quadCounter + 2], gl_state.fontColor[0], gl_state.fontColor[1], gl_state.fontColor[2], gl_state.fontColor[3]);
		VA_SetElem4(colorCoord[quadCounter + 3], gl_state.fontColor[0], gl_state.fontColor[1], gl_state.fontColor[2], gl_state.fontColor[3]);

		px += 8 * scale_x;
	}

	if (glyph_count > 0)
	{
		glDrawElements(GL_TRIANGLES, 6 * glyph_count, GL_UNSIGNED_SHORT, nullptr);
	}
}


/**
 * 
 */
Image* Draw_FindPic(const std::string& name)
{
	Image* image = nullptr;

	if (name[0] != '/' && name[0] != '\\')
	{
		image = GL_FindImage(name, IT_PIC, true);
	}
	else
	{
		image = GL_FindImage(name.substr(1, std::string::npos), IT_PIC, true);
	}

	return image;
}

/**
 * Gets size in pixels of a specified image.
 * 
 * \param w		Reference to an int to write width to.
 * \param h		Reference to an int to write height to.
 * \param pic	Name of the pic to get size information from.
 * 
 * \note	w / h paramters will be 0 if pic wasn't found.
 */
void Draw_GetPicSize(int& w, int& h, const std::string& name)
{
	Image* gl = Draw_FindPic(name);
	if (!gl)
	{
		w = h = 0;
		return;
	}

	w = gl->width;
	h = gl->height;
}




/**
 * \todo	Draws an Image fit within a given rectangular area.
 */
void Draw_StretchPic(int x, int y, int w, int h, Image* gl)
{
	if (!gl)
	{
		Com_DPrintf(S_COLOR_MAGENTA "** Draw_StretchPic2(): null pic passed.\n");
		return;
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, getVbo().ibo_quadTris);

	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, vertCoord);
	glVertexAttribPointer(ATT_TEX0, 2, GL_FLOAT, false, 0, texCoord);
	glVertexAttribPointer(ATT_TEX2, 2, GL_FLOAT, false, 0, texCoord1);
	glVertexAttribPointer(ATT_COLOR, 4, GL_FLOAT, false, 0, colorCoord);

	glEnableVertexAttribArray(ATT_POSITION);
	glEnableVertexAttribArray(ATT_TEX0);
	glEnableVertexAttribArray(ATT_TEX2);
	glEnableVertexAttribArray(ATT_COLOR);

	VA_SetElem2(vertCoord[0], x, y);
	VA_SetElem2(vertCoord[1], x + w, y);
	VA_SetElem2(vertCoord[2], x + w, y + h);
	VA_SetElem2(vertCoord[3], x, y + h);

	VA_SetElem4(colorCoord[0], 1.0, 1.0, 1.0, 1.0);
	VA_SetElem4(colorCoord[1], 1.0, 1.0, 1.0, 1.0);
	VA_SetElem4(colorCoord[2], 1.0, 1.0, 1.0, 1.0);
	VA_SetElem4(colorCoord[3], 1.0, 1.0, 1.0, 1.0);

	GL_BindProgram(genericProgram, 0);

	glUniform1i(gen_attribColors, 1);
	glUniform1i(gen_sky, 0);
	glUniform1i(gen_3d, 0);

	glUniform1f(gen_colorModulate, r_textureColorScale->value);

	glUniformMatrix4fv(gen_orthoMatrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	Scrap_Upload();

	GL_MBind(GL_TEXTURE0, gl->texnum);
	VA_SetElem2(texCoord[0], gl->sl, gl->tl);
	VA_SetElem2(texCoord[1], gl->sh, gl->tl);
	VA_SetElem2(texCoord[2], gl->sh, gl->th);
	VA_SetElem2(texCoord[3], gl->sl, gl->th);


	float scroll = -16 * (r_newrefdef.time / 32.0);

	GL_MBind(GL_TEXTURE1, r_scanline->texnum);
	VA_SetElem2(texCoord1[0], gl->sl, gl->tl - scroll);
	VA_SetElem2(texCoord1[1], gl->sh, gl->tl - scroll);
	VA_SetElem2(texCoord1[2], gl->sh, gl->th - scroll);
	VA_SetElem2(texCoord1[3], gl->sl, gl->th - scroll);


	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, nullptr);

	GL_BindNullProgram();
	GL_SelectTexture(GL_TEXTURE0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glDisableVertexAttribArray(ATT_POSITION);
	glDisableVertexAttribArray(ATT_TEX0);
	glDisableVertexAttribArray(ATT_TEX2);
	glDisableVertexAttribArray(ATT_COLOR);
}


void Draw_StretchPic(int x, int y, int w, int h, char* pic)
{
	bool cons = 0;
	Image* gl;

	gl = Draw_FindPic(pic);

	if (!gl)
	{
		Com_Printf("Draw_StretchPic(): Can't find pic: %s\n", pic);
		return;
	}

	Draw_StretchPic(x, y, w, h, gl);
}

float loadScreenColorFade;

#define WIDTH_FHD 1920.0
#define HEIGHT_FHD 1080.0
#define WIDE_SCREEN_16x9  WIDTH_FHD / HEIGHT_FHD


/**
 * 
 */
void Draw_LoadingScreen(int x, int y, int w, int h, char* pic)
{
	Image* gl;
	gl = Draw_FindPic(pic);
	if (!gl)
	{
		Draw_BoxFilled(0, 0, videoWidth(), videoHeight(), 0.33, 0.33, 0.33, 1.0);
		return;
	}

	float offsX, offsY;
	float woh = (float)videoWidth() / (float)videoHeight();

	if (!gl)
	{
		Com_Printf("Draw_LoadingScreen():: nullptr pic\n");
		return;
	}

	if (woh < WIDE_SCREEN_16x9)	// quad screen
	{
		offsX = (WIDTH_FHD - (HEIGHT_FHD * woh)) / (WIDTH_FHD * 2.0);
		offsY = 0;
	}
	else if (woh > WIDE_SCREEN_16x9) // super wide screen (21 x 9)
	{
		offsX = 0;
		offsY = (HEIGHT_FHD - (WIDTH_FHD / woh)) / (HEIGHT_FHD * 2.0);
	}
	else
	{
		offsX = offsY = 0;
	}

	GL_BindProgram(loadingProgram, 0);

	glUniformMatrix4fv(ls_orthoMatrix, 1, false, (const float *)r_newrefdef.orthoMatrix);
	glUniform1f(ls_fade, loadScreenColorFade);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, getVbo().ibo_quadTris);

	glEnableVertexAttribArray(ATT_POSITION);
	glEnableVertexAttribArray(ATT_TEX0);
	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, vertCoord);
	glVertexAttribPointer(ATT_TEX0, 2, GL_FLOAT, false, 0, texCoord);

	GL_MBind(GL_TEXTURE0, gl->texnum);

	VA_SetElem2(texCoord[0], gl->sl + offsX, gl->tl + offsY);
	VA_SetElem2(texCoord[1], gl->sh - offsX, gl->tl + offsY);
	VA_SetElem2(texCoord[2], gl->sh - offsX, gl->th - offsY);
	VA_SetElem2(texCoord[3], gl->sl + offsX, gl->th - offsY);

	VA_SetElem2(vertCoord[0], x, y);
	VA_SetElem2(vertCoord[1], x + w, y);
	VA_SetElem2(vertCoord[2], x + w, y + h);
	VA_SetElem2(vertCoord[3], x, y + h);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, nullptr);

	GL_BindNullProgram();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(ATT_POSITION);
	glDisableVertexAttribArray(ATT_TEX0);
}

/*
=============
Draw_Pic
=============
*/
void Draw_Pic2(int x, int y, Image* gl)
{
	int w = gl->width;
	int h = gl->height;

	if (!gl->has_alpha)
		GL_Disable(GL_BLEND);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, getVbo().ibo_quadTris);
	glEnableVertexAttribArray(ATT_POSITION);
	glEnableVertexAttribArray(ATT_TEX0);
	glEnableVertexAttribArray(ATT_COLOR);

	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, vertCoord);
	glVertexAttribPointer(ATT_TEX0, 2, GL_FLOAT, false, 0, texCoord);
	glVertexAttribPointer(ATT_COLOR, 4, GL_FLOAT, false, 0, colorCoord);

	GL_BindProgram(genericProgram, 0);
	glUniform1i(gen_attribColors, 1);
	glUniform1i(gen_sky, 0);
	glUniform1i(gen_3d, 0);
	glUniform1f(gen_colorModulate, r_textureColorScale->value);
	glUniformMatrix4fv(gen_orthoMatrix, 1, false, (const float*)r_newrefdef.orthoMatrix);

	Scrap_Upload();

	GL_MBind(GL_TEXTURE0, gl->texnum);

	VA_SetElem2(texCoord[0], gl->sl, gl->tl);
	VA_SetElem2(texCoord[1], gl->sh, gl->tl);
	VA_SetElem2(texCoord[2], gl->sh, gl->th);
	VA_SetElem2(texCoord[3], gl->sl, gl->th);

	VA_SetElem2(vertCoord[0], x, y);
	VA_SetElem2(vertCoord[1], x + gl->width, y);
	VA_SetElem2(vertCoord[2], x + gl->width, y + gl->height);
	VA_SetElem2(vertCoord[3], x, y + gl->height);

	VA_SetElem4(colorCoord[0], 1.0, 1.0, 1.0, 1.0);
	VA_SetElem4(colorCoord[1], 1.0, 1.0, 1.0, 1.0);
	VA_SetElem4(colorCoord[2], 1.0, 1.0, 1.0, 1.0);
	VA_SetElem4(colorCoord[3], 1.0, 1.0, 1.0, 1.0);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, nullptr);

	if (!gl->has_alpha)
		GL_Enable(GL_BLEND);

	GL_BindNullProgram();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(ATT_POSITION);
	glDisableVertexAttribArray(ATT_TEX0);
	glDisableVertexAttribArray(ATT_COLOR);
}


void Draw_ScaledPic(int x, int y, float sX, float sY, Image* gl)
{
	int w = gl->width * sX;
	int h = gl->height * sY;

	if (!gl->has_alpha)
		GL_Disable(GL_BLEND);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, getVbo().ibo_quadTris);
	glEnableVertexAttribArray(ATT_POSITION);
	glEnableVertexAttribArray(ATT_TEX0);
	glEnableVertexAttribArray(ATT_COLOR);
	
    glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, vertCoord);
	glVertexAttribPointer(ATT_TEX0, 2, GL_FLOAT, false, 0, texCoord);
	glVertexAttribPointer(ATT_COLOR, 4, GL_FLOAT, false, 0, colorCoord);
	
	GL_BindProgram(genericProgram, 0);
	glUniform1i(gen_attribColors, 1);
	glUniform1i(gen_sky, 0);
	glUniform1i(gen_3d, 0);

	glUniform1f(gen_colorModulate, r_bump2D->value ? 1.0 : r_textureColorScale->value);
	glUniformMatrix4fv(gen_orthoMatrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	Scrap_Upload();

	GL_MBind(GL_TEXTURE0, gl->texnum);
				
	VA_SetElem2(texCoord[0],gl->sl, gl->tl);
	VA_SetElem2(texCoord[1],gl->sh, gl->tl);
	VA_SetElem2(texCoord[2],gl->sh, gl->th);
	VA_SetElem2(texCoord[3],gl->sl, gl->th);
		
	VA_SetElem2(vertCoord[0],x, y);
	VA_SetElem2(vertCoord[1],x + w, y);
	VA_SetElem2(vertCoord[2],x + w, y + h);
	VA_SetElem2(vertCoord[3],x, y + h);
		
	VA_SetElem4(colorCoord[0], 1.0, 1.0, 1.0, 1.0);
	VA_SetElem4(colorCoord[1], 1.0, 1.0, 1.0, 1.0);
	VA_SetElem4(colorCoord[2], 1.0, 1.0, 1.0, 1.0);
	VA_SetElem4(colorCoord[3], 1.0, 1.0, 1.0, 1.0);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, nullptr);

	if (!gl->has_alpha)
		GL_Enable(GL_BLEND);
	
	GL_BindNullProgram();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(ATT_POSITION);
	glDisableVertexAttribArray(ATT_TEX0);
	glDisableVertexAttribArray(ATT_COLOR);
}


void Draw_ScaledBumpPic(int x, int y, float sX, float sY, Image* gl, Image* gl2)
{
	float lightShift;

	int w = gl->width * sX;
	int h = gl->height * sY;

	GL_BlendFunc(GL_SRC_ALPHA, GL_ONE); // use addative alpha blending

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, getVbo().ibo_quadTris);
	glEnableVertexAttribArray(ATT_POSITION);
	glEnableVertexAttribArray(ATT_TEX0);

	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, vertCoord);
	glVertexAttribPointer(ATT_TEX0, 2, GL_FLOAT, false, 0, texCoord);

	GL_BindProgram(light2dProgram, 0);

	lightShift = 66.6 * sin(Sys_Milliseconds() * 0.001f);
	glUniform2f(light2d_params, lightShift, r_hudLighting->value);
	glUniformMatrix4fv(light2d_orthoMatrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	GL_MBind(GL_TEXTURE0, gl->texnum);
	GL_MBind(GL_TEXTURE1, gl2->texnum);

	VA_SetElem2(texCoord[0], gl->sl, gl->tl);
	VA_SetElem2(texCoord[1], gl->sh, gl->tl);
	VA_SetElem2(texCoord[2], gl->sh, gl->th);
	VA_SetElem2(texCoord[3], gl->sl, gl->th);

	VA_SetElem3(vertCoord[0], x, y, 1.0);
	VA_SetElem3(vertCoord[1], x + w, y, 1.0);
	VA_SetElem3(vertCoord[2], x + w, y + h, 1.0);
	VA_SetElem3(vertCoord[3], x, y + h, 1.0);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, nullptr);


	GL_BlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	GL_BindNullProgram();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(ATT_POSITION);
	glDisableVertexAttribArray(ATT_TEX0);
}


void Draw_Pic(int x, int y, char* pic)
{
	Image* gl;

	gl = Draw_FindPic(pic);
	if (!gl)
	{
		Com_Printf("Draw_Pic(): Can't find pic: %s\n", pic);
		return;
	}

	Draw_Pic2(x, y, gl);
}


void Draw_PicScaled(int x, int y, float scale_x, float scale_y, char* pic)
{
	Image* gl;

	gl = Draw_FindPic(pic);
	if (!gl)
	{
		Com_Printf("Draw_PicScaled(): Can't find pic: %s\n", pic);
		return;
	}

	Draw_ScaledPic(x, y, scale_x, scale_y, gl);
}


void Draw_PicBump(int x, int y, char* pic, char* pic2)
{
	Draw_PicBumpScaled(x, y, 1.0f, 1.0f, pic, pic2);
}


void Draw_PicBumpScaled(int x, int y, float scale_x, float scale_y, char* pic, char* pic2)
{
	Image* gl;
	Image* gl2;

	if (!r_bump2D->value)
		return;

	gl = Draw_FindPic(pic);
	if (!gl)
	{
		Com_Printf("Draw_PicBumpScaled(): Can't find pic: %s\n", pic);
		return;
	}

	gl2 = Draw_FindPic(pic2);
	if (!gl2)
	{
		Com_Printf("Draw_PicBumpScaled(): Can't find pic: %s\n", pic2);
		return;
	}

	Draw_ScaledBumpPic(x, y, scale_x, scale_y, gl, gl2);
}


/**
 * Draws a box outline in a given color.
 * 
 * \param x	X-Coordinate.
 * \param y	Y-Coordinate.
 * \param w	Width of the box in pixels.
 * \param h	Height of the box in pixels.
 * \param r	Red value. Valid range is 0.0 - 1.0.
 * \param g	Green value. Valid range is 0.0 - 1.0.
 * \param b	Blue value. Valid range is 0.0 - 1.0.
 * \param a	Alpha value. Valid range is 0.0 - 1.0.
 *  
 * \warning	In the interest of efficiency, color values are not clamped so
 *			values outside of 0.0 - 1.0 will yield undefined results.
 */
void Draw_Box(int x, int y, int w, int h, float r, float g, float b, float a)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, getVbo().ibo_quadTris);
	glEnableVertexAttribArray(ATT_POSITION);
	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, vertCoord);

	GL_BindProgram(genericProgram, 0);
	glUniform1i(gen_attribColors, 0);
	glUniform1i(gen_sky, 0);
	glUniform1i(gen_3d, 0);
	glUniform4f(gen_color, r, g, b, a);
	glUniform1f(gen_colorModulate, r_textureColorScale->value);
	glUniformMatrix4fv(gen_orthoMatrix, 1, false, (const float*)r_newrefdef.orthoMatrix);

	VA_SetElem2(vertCoord[0], x, y);
	VA_SetElem2(vertCoord[1], x + w, y);
	glDrawElements(GL_LINES, 2, GL_UNSIGNED_SHORT, nullptr);

	VA_SetElem2(vertCoord[0], x + w, y);
	VA_SetElem2(vertCoord[1], x + w, y + h);
	glDrawElements(GL_LINES, 2, GL_UNSIGNED_SHORT, nullptr);

	VA_SetElem2(vertCoord[0], x, y + h);
	VA_SetElem2(vertCoord[1], x + w, y + h);
	glDrawElements(GL_LINES, 2, GL_UNSIGNED_SHORT, nullptr);

	VA_SetElem2(vertCoord[0], x, y);
	VA_SetElem2(vertCoord[1], x, y + h);
	glDrawElements(GL_LINES, 2, GL_UNSIGNED_SHORT, nullptr);

	GL_BindNullProgram();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(ATT_POSITION);
}


/**
 * Draws a filled box in a given color.
 * 
 * \param x	X-Coordinate.
 * \param y	Y-Coordinate.
 * \param w	Width of the box in pixels.
 * \param h	Height of the box in pixels.
 * \param r	Red value. Valid range is 0.0 - 1.0.
 * \param g	Green value. Valid range is 0.0 - 1.0.
 * \param b	Blue value. Valid range is 0.0 - 1.0.
 * \param a	Alpha value. Valid range is 0.0 - 1.0.
 * 
 * \note	Color values are not clamped so values outside of 0.0 - 1.0
 *			will yield undefined results.
 */
void Draw_BoxFilled(int x, int y, int w, int h, float r, float g, float b, float a)
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, getVbo().ibo_quadTris);
	glEnableVertexAttribArray(ATT_POSITION);
	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, vertCoord);
	
	GL_BindProgram(genericProgram, 0);
	glUniform1i(gen_attribColors, 0);
	glUniform1i(gen_sky, 0);
	glUniform1i(gen_3d, 0);
	glUniform4f(gen_color, r, g, b, a);
	glUniform1f(gen_colorModulate, r_textureColorScale->value);
	glUniformMatrix4fv(gen_orthoMatrix, 1, false, (const float*)r_newrefdef.orthoMatrix);

	VA_SetElem2(vertCoord[0], x, y);
	VA_SetElem2(vertCoord[1], x + w, y);
	VA_SetElem2(vertCoord[2], x + w, y + h);
	VA_SetElem2(vertCoord[3], x, y + h);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, nullptr);
	
	GL_BindNullProgram();
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(ATT_POSITION);
}
