#include "r_model_md3.h"
#include "r_image.h"
#include "r_util.h"

#include "../common/math.h"
#include "../common/mem.h"
#include "../common/string_util.h"


#define MOD_MAX_LODS 4

enum
{
	AXIS_FORWARD = 0,
	AXIS_RIGHT = 3,
	AXIS_UP = 6
};


/**
 * \fixme	Find a sane place for this.
 */
void R_BuildTangentVectors(int numVertexes, vec4_t *xyzArray, vec4_t *normalsArray, vec2_t *stArray, int numTris, elem_t *elems, vec4_t *sVectorsArray)
{
	int i, j;
	float d, *v[3], *tc[3];
	vec_t *s, *t, *n;
	vec3_t stvec[3], cross;
	vec3_t stackTVectorsArray[128];
	vec3_t* tVectorsArray;

	if (numVertexes > sizeof(stackTVectorsArray) / sizeof(stackTVectorsArray[0]))
	{
		tVectorsArray = static_cast<vec3_t*>(malloc(sizeof(vec3_t) * numVertexes));
	}
	else
	{
		tVectorsArray = stackTVectorsArray;
	}

	// assuming arrays have already been allocated this also does some nice precaching
	memset(sVectorsArray, 0, numVertexes * sizeof(*sVectorsArray));
	memset(tVectorsArray, 0, numVertexes * sizeof(*tVectorsArray));

	for (i = 0; i < numTris; i++, elems += 3)
	{
		for (j = 0; j < 3; j++)
		{
			v[j] = (float *)(xyzArray + elems[j]);
			tc[j] = (float *)(stArray + elems[j]);
		}

		// calculate two mostly perpendicular edge directions
		VectorSubtract(v[1], v[0], stvec[0]);
		VectorSubtract(v[2], v[0], stvec[1]);

		// we have two edge directions, we can calculate the normal then
		CrossProduct(stvec[1], stvec[0], cross);

		for (j = 0; j < 3; j++)
		{
			stvec[0][j] = ((tc[1][1] - tc[0][1]) * (v[2][j] - v[0][j]) - (tc[2][1] - tc[0][1]) * (v[1][j] - v[0][j]));
			stvec[1][j] = ((tc[1][0] - tc[0][0]) * (v[2][j] - v[0][j]) - (tc[2][0] - tc[0][0]) * (v[1][j] - v[0][j]));
		}

		// inverse tangent vectors if their cross product goes in the opposite direction to triangle normal
		CrossProduct(stvec[1], stvec[0], stvec[2]);
		if (DotProduct(stvec[2], cross) < 0)
		{
			VectorInverse(stvec[0]);
			VectorInverse(stvec[1]);
		}

		for (j = 0; j < 3; j++)
		{
			VectorAdd(sVectorsArray[elems[j]], stvec[0], sVectorsArray[elems[j]]);
			VectorAdd(tVectorsArray[elems[j]], stvec[1], tVectorsArray[elems[j]]);
		}
	}

	// normalize
	for (i = 0, s = *sVectorsArray, t = *tVectorsArray, n = *normalsArray; i < numVertexes; i++, s += 4, t += 3, n += 4)
	{
		// keep s\t vectors perpendicular
		d = -DotProduct(s, n);
		VectorScaleAndAdd(s, n, s, d);
		VectorNormalize(s);

		d = -DotProduct(t, n);
		VectorScaleAndAdd(t, n, t, d);

		// store polarity of t-vector in the 4-th coordinate of s-vector
		CrossProduct(n, s, cross);
		if (DotProduct(cross, t) < 0)
			s[3] = -1;
		else
			s[3] = 1;
	}

	if (tVectorsArray != stackTVectorsArray)
		//R_Free(tVectorsArray);
		free(tVectorsArray);
}



extern float SinTableByte[256]; /// \fixme Defined in r_main.c, find a better place for this.


/**
 * \fixme	Find a sane place for this.
 */
void R_LatLongToNorm4(const uint8_t latlong[2], vec4_t out)
{
	static float* const sinTable = SinTableByte;
	float sin_a, sin_b, cos_a, cos_b;

	cos_a = sinTable[(latlong[0] + 64) & 255];
	sin_a = sinTable[latlong[0]];
	cos_b = sinTable[(latlong[1] + 64) & 255];
	sin_b = sinTable[latlong[1]];

	Vector4Set(out, cos_b * sin_a, sin_b * sin_a, cos_a, 0);
}


/**
 * 
 */
void Mod_StripLODSuffix(char *name)
{
	size_t len;

	len = strlen(name);
	if (len <= 2)
		return;

	if (name[len - 2] != '_')
		return;

	if (name[len - 1] >= '0' && name[len - 1] <= '0' + MOD_MAX_LODS)
		name[len - 2] = 0;
}


static void Mod_AliasBuildMeshesForFrame0(Model* mod)
{
	int i, j, k;
	size_t size;
	maliasframe_t *frame;
	maliasmodel_t *aliasmodel = (maliasmodel_t*)mod->extraData;

	frame = &aliasmodel->frames[0];
	for (k = 0; k < aliasmodel->nummeshes; k++)
	{
		maliasmesh_t *mesh = &aliasmodel->meshes[k];

		size = sizeof(vec4_t) + sizeof(vec4_t); // xyz and normals
		size += sizeof(vec4_t);       // s-vectors
		size *= mesh->numverts;

		mesh->xyzArray = (vec4_t*)malloc(size);
		mesh->normalsArray = (vec4_t*)((uint8_t*)mesh->xyzArray + mesh->numverts * sizeof(vec4_t));
		mesh->sVectorsArray = (vec4_t*)((uint8_t*)mesh->normalsArray + mesh->numverts * sizeof(vec4_t));

		for (i = 0; i < mesh->numverts; i++)
		{
			for (j = 0; j < 3; j++)
				mesh->xyzArray[i][j] = frame->translate[j] + frame->scale[j] * mesh->vertexes[i].point[j];

			mesh->xyzArray[i][3] = 1;
			R_LatLongToNorm4(mesh->vertexes[i].latlong, mesh->normalsArray[i]);
		}

		R_BuildTangentVectors(mesh->numverts, mesh->xyzArray, mesh->normalsArray, mesh->stArray, mesh->numtris, mesh->elems, mesh->sVectorsArray);

		/*
		if (gl_config.ext.vertex_buffer_object)
			Mod_AliasBuildStaticVBOForMesh(mesh);
		*/
	}
}


/*
 * Same as R_RegisterModel(char*)?
 */
/*
static void Mod_TouchAliasModel(Model* mod)
{
	int i, j;
	maliasmesh_t *mesh;
	maliasskin_t *skin;
	maliasmodel_t *aliasmodel = (maliasmodel_t *)mod->extradata;

	mod->registration_sequence = rsh.registrationSequence;

	for (i = 0, mesh = aliasmodel->meshes; i < aliasmodel->nummeshes; i++, mesh++)
	{
		// register needed skins and images
		for (j = 0, skin = mesh->skins; j < mesh->numskins; j++, skin++)
		{
			if (skin->shader)
				R_TouchShader(skin->shader);
		}

		if (mesh->vbo)
			R_TouchMeshVBO(mesh->vbo);
	}
}
*/


/**
 * 
 */
void Mod_LoadAliasMD3Model(Model* mod, Model* parent, void* buffer)
{
	int version, i, j, l;
	int bufsize, numverts;
	uint8_t *buf;
	dmd3header_t *pinmodel;
	dmd3frame_t *pinframe;
	dmd3tag_t *pintag;
	dmd3mesh_t *pinmesh;
	dmd3skin_t *pinskin;
	dmd3coord_t *pincoord;
	dmd3vertex_t *pinvert;
	unsigned int *pinelem;
	elem_t *poutelem;
	maliasvertex_t *poutvert;
	vec2_t *poutcoord;
	maliasskin_t *poutskin;
	maliasmesh_t *poutmesh;
	maliastag_t *pouttag;
	maliasframe_t *poutframe;
	maliasmodel_t *poutmodel;
	drawSurfaceAlias_t *drawSurf;

	mod->memorySize = 0;

	pinmodel = (dmd3header_t*)buffer;

	version = LittleLong(pinmodel->version);
	if (version != MD3_ALIAS_VERSION)
		Com_Error(ERR_DROP, "%s has wrong version number (%i should be %i)", mod->name, version, MD3_ALIAS_VERSION);

	mod->type = mod_alias_md3;
	mod->radius = 0;

	poutmodel = (maliasmodel_t*)malloc(sizeof(maliasmodel_t));

	if (!poutmodel)
	{
		throw std::runtime_error("Mod_LoadAliasMD3Model():: Memory allocation failure.");
	}

	mod->extraData = poutmodel;

	//mod->registration_sequence = rsh.registrationSequence; // Set in R_RegisterModel(char*)
	//mod->touch = &Mod_TouchAliasModel;

	ClearBounds(mod->mins, mod->maxs);

	// byte swap the header fields and sanity check
	poutmodel->numframes = LittleLong(pinmodel->num_frames);
	poutmodel->numtags = LittleLong(pinmodel->num_tags);
	poutmodel->nummeshes = LittleLong(pinmodel->num_meshes);
	poutmodel->numskins = 0;
	poutmodel->numverts = 0;
	poutmodel->numtris = 0;

	if (poutmodel->numframes <= 0)
		Com_Error(ERR_DROP, "model %s has no frames", mod->name);
	else if (poutmodel->numframes > MD3_MAX_FRAMES)
		Com_Error(ERR_DROP, "model %s has too many frames", mod->name);

	if (poutmodel->numtags > MD3_MAX_TAGS)
		Com_Error(ERR_DROP, "model %s has too many tags", mod->name);
	else if (poutmodel->numtags < 0)
		Com_Error(ERR_DROP, "model %s has invalid number of tags", mod->name);

	if (poutmodel->nummeshes < 0)
		Com_Error(ERR_DROP, "model %s has invalid number of meshes", mod->name);
	else if (!poutmodel->nummeshes && !poutmodel->numtags)
		Com_Error(ERR_DROP, "model %s has no meshes and no tags", mod->name);
	//	else if( poutmodel->nummeshes > MD3_MAX_MESHES )
	//		ri.Com_Error( ERR_DROP, "model %s has too many meshes", mod->name );


	bufsize = poutmodel->numframes * (sizeof(maliasframe_t) + (sizeof(maliastag_t) * poutmodel->numtags)) +
		poutmodel->nummeshes * sizeof(maliasmesh_t) +
		poutmodel->nummeshes * sizeof(drawSurfaceAlias_t);

	buf = (uint8_t*)malloc(bufsize);

	if (!buf)
	{
		throw std::runtime_error("Mod_LoadAliasMD3Model():: Memory allocation failure.");
	}

	// load the frames
	pinframe = (dmd3frame_t *)((uint8_t *)pinmodel + LittleLong(pinmodel->ofs_frames));
	poutframe = poutmodel->frames = (maliasframe_t *)buf;
	buf += sizeof(maliasframe_t) * poutmodel->numframes;
	for (i = 0; i < poutmodel->numframes; i++, pinframe++, poutframe++)
	{
		memcpy(poutframe->translate, pinframe->translate, sizeof(vec3_t));
		for (j = 0; j < 3; j++)
		{
			poutframe->scale[j] = MD3_XYZ_SCALE;
			poutframe->translate[j] = LittleFloat(poutframe->translate[j]);
		}

		// never trust the modeler utility and recalculate bbox and radius
		ClearBounds(poutframe->mins, poutframe->maxs);
	}

	// load the tags
	pintag = (dmd3tag_t*)((uint8_t*)pinmodel + LittleLong(pinmodel->ofs_tags));
	pouttag = poutmodel->tags = (maliastag_t*)buf;
	buf += sizeof(maliastag_t) * poutmodel->numframes * poutmodel->numtags;
	for (i = 0; i < poutmodel->numframes; i++)
	{
		for (l = 0; l < poutmodel->numtags; l++, pintag++, pouttag++)
		{
			dmd3tag_t intag;
			mat3_t axis;

			memcpy(&intag, pintag, sizeof(dmd3tag_t));

			for (j = 0; j < 3; j++)
			{
				axis[AXIS_FORWARD + j] = LittleFloat(intag.axis[0][j]);
				axis[AXIS_RIGHT + j] = LittleFloat(intag.axis[1][j]);
				axis[AXIS_UP + j] = LittleFloat(intag.axis[2][j]);
				pouttag->origin[j] = LittleFloat(intag.origin[j]);
			}

			Quat_FromMatrix3(axis, pouttag->quat);
			Quat_Normalize(pouttag->quat);

			Q_strncpyz(pouttag->name, intag.name, MAX_QPATH);
		}
	}


	// allocate drawSurfs
	drawSurf = poutmodel->drawSurfs = (drawSurfaceAlias_t*)buf;
	buf += sizeof(drawSurfaceAlias_t) * poutmodel->nummeshes;
	for (i = 0; i < poutmodel->nummeshes; i++, drawSurf++)
	{
		drawSurf->type = ST_ALIAS;
		drawSurf->model = mod;
		drawSurf->mesh = poutmodel->meshes + i;
	}


	// load meshes
	pinmesh = (dmd3mesh_t*)((uint8_t*)pinmodel + LittleLong(pinmodel->ofs_meshes));
	poutmesh = poutmodel->meshes = (maliasmesh_t *)buf;
	buf += sizeof(maliasmesh_t) * poutmodel->nummeshes;
	for (i = 0; i < poutmodel->nummeshes; i++, poutmesh++)
	{
		dmd3mesh_t inmesh;
		memcpy(&inmesh, pinmesh, sizeof(dmd3mesh_t));

		/*	Checked in Mod_ForName()
		if (strncmp((const char *)inmesh.id, IDMD3HEADER, 4))
			Com_Error(ERR_DROP, "mesh %s in model %s has wrong id (%s should be %s)", inmesh.name, mod->name, inmesh.id, IDMD3HEADER);
		*/

		if (LittleLong(*(unsigned*)inmesh.id) != IDMD3HEADER)
			Com_Error(ERR_DROP, "mesh %s in model %s has wrong id (%s should be %s)", inmesh.name, mod->name, inmesh.id, IDMD3HEADER);

		Q_strncpyz(poutmesh->name, inmesh.name, MAX_QPATH);

		Mod_StripLODSuffix(poutmesh->name);

		poutmesh->numtris = LittleLong(inmesh.num_tris);
		poutmesh->numskins = LittleLong(inmesh.num_skins);
		poutmesh->numverts = numverts = LittleLong(inmesh.num_verts);

		poutmodel->numverts += poutmesh->numverts;
		poutmodel->numtris += poutmesh->numtris;

		/*		if( poutmesh->numskins <= 0 )
		ri.Com_Error( ERR_DROP, "mesh %i in model %s has no skins", i, mod->name );
		else*/
		if (poutmesh->numskins > MD3_MAX_SHADERS)
			Com_Error(ERR_DROP, "mesh %i in model %s has too many skins", i, mod->name);

		if (poutmesh->numtris <= 0)
			Com_Error(ERR_DROP, "mesh %i in model %s has no elements", i, mod->name);
		else if (poutmesh->numtris > MD3_MAX_TRIANGLES)
			Com_Error(ERR_DROP, "mesh %i in model %s has too many triangles", i, mod->name);

		if (poutmesh->numverts <= 0)
			Com_Error(ERR_DROP, "mesh %i in model %s has no vertices", i, mod->name);
		else if (poutmesh->numverts > MD3_MAX_VERTS)
			Com_Error(ERR_DROP, "mesh %i in model %s has too many vertices", i, mod->name);

		bufsize = ALIGN(sizeof(maliasskin_t) * poutmesh->numskins, sizeof(vec_t)) +
			numverts * (sizeof(vec2_t) + sizeof(maliasvertex_t) * poutmodel->numframes) +
			poutmesh->numtris * sizeof(elem_t) * 3;
		buf = (uint8_t*)malloc(bufsize);

		// load the skins
		pinskin = (dmd3skin_t *)((uint8_t *)pinmesh + LittleLong(inmesh.ofs_skins));
		poutskin = poutmesh->skins = (maliasskin_t *)buf;
		buf += sizeof(maliasskin_t) * poutmesh->numskins, sizeof(vec_t);
		for (j = 0; j < poutmesh->numskins; j++, pinskin++, poutskin++)
		{
			Q_strncpyz(poutskin->name, pinskin->name, sizeof(poutskin->name));
			strcat(poutskin->name, ".tga");
			poutskin->skin = GL_FindImage(poutskin->name, IT_SKIN, true);
			if (!poutskin->skin)
				poutskin->skin = r_missingTexture;
		}

		// load the texture coordinates
		pincoord = (dmd3coord_t *)((uint8_t *)pinmesh + LittleLong(inmesh.ofs_tcs));
		poutcoord = poutmesh->stArray = (vec2_t *)buf; buf += poutmesh->numverts * sizeof(vec2_t);
		for (j = 0; j < poutmesh->numverts; j++, pincoord++)
		{
			memcpy(poutcoord[j], pincoord->st, sizeof(vec2_t));
			poutcoord[j][0] = LittleFloat(poutcoord[j][0]);
			poutcoord[j][1] = LittleFloat(poutcoord[j][1]);
		}

		// load the vertexes and normals
		pinvert = (dmd3vertex_t *)((uint8_t *)pinmesh + LittleLong(inmesh.ofs_verts));
		poutvert = poutmesh->vertexes = (maliasvertex_t *)buf;
		buf += poutmesh->numverts * sizeof(maliasvertex_t) * poutmodel->numframes;
		for (l = 0, poutframe = poutmodel->frames; l < poutmodel->numframes; l++, poutframe++, pinvert += poutmesh->numverts, poutvert += poutmesh->numverts)
		{
			vec3_t v;

			for (j = 0; j < poutmesh->numverts; j++)
			{
				dmd3vertex_t invert;

				memcpy(&invert, &(pinvert[j]), sizeof(dmd3vertex_t));

				poutvert[j].point[0] = LittleShort(invert.point[0]);
				poutvert[j].point[1] = LittleShort(invert.point[1]);
				poutvert[j].point[2] = LittleShort(invert.point[2]);

				poutvert[j].latlong[0] = invert.norm[0];
				poutvert[j].latlong[1] = invert.norm[1];

				v[0] = poutvert[j].point[0];
				v[1] = poutvert[j].point[1];
				v[2] = poutvert[j].point[2];

				AddPointToBounds(v, poutframe->mins, poutframe->maxs);
			}
		}

		// load the elems
		pinelem = (unsigned int *)((uint8_t *)pinmesh + LittleLong(inmesh.ofs_elems));
		poutelem = poutmesh->elems = (elem_t *)buf;
		for (j = 0; j < poutmesh->numtris; j++, pinelem += 3, poutelem += 3)
		{
			unsigned int inelem[3];

			memcpy(inelem, pinelem, sizeof(int) * 3);

			poutelem[0] = (elem_t)LittleLong(inelem[0]);
			poutelem[1] = (elem_t)LittleLong(inelem[1]);
			poutelem[2] = (elem_t)LittleLong(inelem[2]);
		}

		pinmesh = (dmd3mesh_t*)((uint8_t*)pinmesh + LittleLong(inmesh.meshsize));
	}

	// setup drawSurfs
	for (i = 0; i < poutmodel->nummeshes; i++)
	{
		drawSurf = poutmodel->drawSurfs + i;
		drawSurf->type = ST_ALIAS;
		drawSurf->model = mod;
		drawSurf->mesh = poutmodel->meshes + i;
	}

	// build S and T vectors for frame 0
	Mod_AliasBuildMeshesForFrame0(mod);

	// calculate model bounds
	poutframe = poutmodel->frames;
	for (i = 0; i < poutmodel->numframes; i++, poutframe++)
	{
		VectorScaleAndAdd(poutframe->translate, poutframe->mins, poutframe->mins, MD3_XYZ_SCALE);
		VectorScaleAndAdd(poutframe->translate, poutframe->maxs, poutframe->maxs, MD3_XYZ_SCALE);
		poutframe->radius = R_RadiusFromBounds(poutframe->mins, poutframe->maxs);

		AddPointToBounds(poutframe->mins, mod->mins, mod->maxs);
		AddPointToBounds(poutframe->maxs, mod->mins, mod->maxs);
		mod->radius = max(mod->radius, poutframe->radius);
	}
}
