#pragma once

void R_DeslectLights();
bool R_LightSelected();

void R_DrawLightFlare();
void R_DrawLightBounds();

void R_SetViewLightScreenBounds();
void R_PrepareShadowLightFrame(bool weapon);

void R_CalcStaticLightInteraction();

worldShadowLight_t* getCurrentSelectedLight();
