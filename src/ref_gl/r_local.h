/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
#pragma once

#include <string>

#include <stdio.h>
#include <math.h>

#include <GL/glew.h>

#ifdef _WIN32
#include <windows.h>
#include "imagelib/il.h"
#include "imagelib/ilu.h"
#include "imagelib/ilut.h"
#else
#include "glcorearb.h"
#include <IL/il.h>
#include <IL/ilu.h>
#include <IL/ilut.h>

typedef void ILvoid;
#define _inline inline
#endif

#include "r_model.h"

#include "../client/Particle.h"
#include "../client/ref.h"

#include "../common/colordef.h"
#include "../common/def_refdef.h"
#include "../common/def_renderfx.h"
#include "../common/files.h"


#define	PITCH						0		/**< Up / Down */
#define	YAW							1		/**< Left / Right */
#define	ROLL						2		/**< Fall Over */

#define M_TWOPI						6.28318530717958647692

#define	TEXNUM_SCRAPS				4096
#define	TEXNUM_IMAGES				4097

#define	TEXNUM_LIGHTMAPS			16384
#define	MAX_GLTEXTURES				16384 

#define BUFFER_OFFSET(i)			((byte *)nullptr + (i))

#define MAX_DRAW_STRING_LENGTH		256
#define MAX_IDX						(MAX_VERTICES * 4)


/**
 * 
 */
typedef enum
{
	rserr_ok,

	rserr_invalid_fullscreen,
	rserr_invalid_mode,

	rserr_unknown
} rserr_t;


void GL_SetDefaultState();
void GL_UpdateSwapInterval();

extern float gldepthmin, gldepthmax;

typedef struct
{
	float x, y, z;
	float s, t;
	float r, g, b;
} glvert_t;


#define BACKFACE_EPSILON	0.01


//====================================================

#define		MAX_CAUSTICS		32
extern Image* r_caustic[MAX_CAUSTICS];

#define		MAX_WATER_NORMALS		32
extern Image* r_waterNormals[MAX_WATER_NORMALS];

#define		MAX_FLY		2
extern Image* fly[MAX_FLY];

#define		MAX_FLAMEANIM		5
extern Image* flameanim[MAX_FLAMEANIM];

#define		MAX_BLOOD 6
extern Image* r_blood[MAX_BLOOD];

#define	MAX_SHELLS 6
extern	Image* r_texshell[MAX_SHELLS];

#define MAX_EXPLODE 30
extern Image* r_explode[MAX_EXPLODE];

extern bool drawFlares;
extern Image* r_missingTexture;
extern Image* r_notexture;
extern Image* r_distort;
extern Image* r_predator;
extern Image* depthMap;

extern Image* r_particletexture[Particle::PT_MAX];
extern Image* r_decaltexture[DECAL_MAX];

extern	Image* r_radarmap;
extern	Image* r_around;
extern	Image* r_flare;

extern	Image* draw_chars;
extern	Image* r_DSTTex;

extern	Image* r_defBump;
extern	Image* ScreenMap;
extern	Image* r_envTex;
extern	Image* r_randomNormalTex;
extern	Image* shadowMask;
extern	Image* r_scanline;
extern	Image* weaponHack;
extern	Image* fxaaMap;
extern	Image* fboScreen;
extern	Image* r_whiteMap;
extern	Image* skinBump;

#define MAX_FILTERS 256
extern	Image* r_lightCubeMap[MAX_FILTERS];
#define	MAX_GLOBAL_FILTERS	37

extern Image* fboDN;
extern Image* fboColor[2];

extern size_t bloomtex;
extern size_t thermaltex;
extern size_t fxaatex;
extern size_t fovCorrTex;

extern size_t fboId, fbo_weaponMask;
extern byte fboColorIndex;

extern entity_t *CURRENT_ENTITY;
extern Model* CURRENT_MODEL;
extern int r_visframecount;
extern int r_framecount;
extern cplane_t frustum[6];

extern	int flareQueries[MAX_WORLD_SHADOW_LIGHTS];

//
// view origin
//
extern vec3_t vup;
extern vec3_t vpn;
extern vec3_t vright;
extern vec3_t r_origin;
extern entity_t r_worldentity;

//
// screen size info
//
extern refdef_t r_newrefdef;
extern int r_viewcluster, r_viewcluster2, r_oldviewcluster, r_oldviewcluster2;

extern ConsoleVariable *r_noRefresh;
extern ConsoleVariable *r_drawEntities;
extern ConsoleVariable *r_drawWorld;
extern ConsoleVariable *r_noVis;
extern ConsoleVariable *r_noCull;
extern ConsoleVariable *r_leftHand;
extern ConsoleVariable *r_lightLevel;
extern ConsoleVariable *r_mode;
extern ConsoleVariable *r_noBind;
extern ConsoleVariable *r_cull;
extern ConsoleVariable *r_vsync;
extern ConsoleVariable *r_textureMode;

extern ConsoleVariable *r_imageAutoBump;
extern ConsoleVariable *r_imageAutoBumpScale;
extern ConsoleVariable *r_imageAutoSpecularScale;

extern ConsoleVariable *r_lockPvs;
extern ConsoleVariable *r_fullScreen;

extern ConsoleVariable *r_brightness;
extern ConsoleVariable *r_contrast;
extern ConsoleVariable *r_saturation;
extern ConsoleVariable *r_gamma;

extern ConsoleVariable *vid_ref;

extern ConsoleVariable *r_causticIntens;

extern ConsoleVariable *r_displayRefresh;

extern ConsoleVariable *r_textureColorScale;
extern ConsoleVariable *r_textureCompression;
extern ConsoleVariable *r_anisotropic;
extern ConsoleVariable *r_maxAnisotropy;

extern ConsoleVariable *r_shadows;
extern ConsoleVariable *r_playerShadow;

extern ConsoleVariable *r_multiSamples;
extern ConsoleVariable *r_nvSamplesCoverange;
extern ConsoleVariable *r_fxaa;
extern ConsoleVariable *deathmatch;

extern ConsoleVariable *r_drawFlares;
extern ConsoleVariable *r_scaleAutoLightColor;
extern ConsoleVariable *r_lightWeldThreshold;

extern ConsoleVariable *r_customWidth;
extern ConsoleVariable *r_customHeight;

extern ConsoleVariable *r_bloom;
extern ConsoleVariable *r_bloomThreshold;
extern ConsoleVariable *r_bloomIntens;
extern ConsoleVariable *r_bloomWidth;

extern ConsoleVariable *r_ssao;
extern ConsoleVariable *r_ssaoIntensity;
extern ConsoleVariable *r_ssaoScale;
extern ConsoleVariable *r_ssaoBlur;

extern ConsoleVariable *r_skipStaticLights;
extern ConsoleVariable *r_lightmapScale;
extern ConsoleVariable *r_lightsWeldThreshold;
extern ConsoleVariable *r_debugLights;
extern ConsoleVariable *r_useLightScissors;
extern ConsoleVariable *r_useDepthBounds;
extern ConsoleVariable *r_specularScale;
extern ConsoleVariable *r_ambientSpecularScale;
extern ConsoleVariable *r_useRadiosityBump;
extern ConsoleVariable *r_zNear;
extern ConsoleVariable *r_zFar;

extern ConsoleVariable *hunk_bsp;
extern ConsoleVariable *hunk_model;
extern ConsoleVariable *hunk_sprite;

extern ConsoleVariable *r_maxTextureSize;

extern ConsoleVariable *r_reliefMapping;
extern ConsoleVariable *r_reliefScale;

extern ConsoleVariable *r_dof;
extern ConsoleVariable *r_dofBias;
extern ConsoleVariable *r_dofFocus;

extern ConsoleVariable *r_motionBlur;
extern ConsoleVariable *r_motionBlurSamples;
extern ConsoleVariable *r_motionBlurFrameLerp;

extern ConsoleVariable *r_radialBlur;
extern ConsoleVariable *r_radialBlurFov;

extern ConsoleVariable *r_tbnSmoothAngle;

extern ConsoleVariable *r_glDebugOutput;
extern ConsoleVariable *r_glMinorVersion;
extern ConsoleVariable *r_glMajorVersion;
extern ConsoleVariable *r_glCoreProfile;

extern ConsoleVariable *r_lightEditor;
extern ConsoleVariable *r_cameraSpaceLightMove;

extern ConsoleVariable *r_hudLighting;
extern ConsoleVariable *r_bump2D;

extern ConsoleVariable *r_filmFilter;
extern ConsoleVariable *r_filmFilterType; // 0 - technicolor; 1 - sepia
extern ConsoleVariable *r_filmFilterNoiseIntens;
extern ConsoleVariable *r_filmFilterScratchIntens;
extern ConsoleVariable *r_filmFilterVignetIntens;

extern ConsoleVariable *r_fixFovStrength; // 0.0 = no hi-fov perspective correction
extern ConsoleVariable *r_fixFovDistroctionRatio; // 0.0 = cylindrical distortion ratio. 1.0 = spherical

extern float ref_realtime;

extern int r_visframecount;

extern vec3_t	*vertexArray;
extern vec3_t	*normalArray;
extern vec3_t	*tangentArray;
extern vec3_t	*binormalArray;
extern vec4_t	*colorArray;


extern int lightsQueries[MAX_WORLD_SHADOW_LIGHTS];
extern int numLightQ;
extern int numFlareOcc;
extern bool FoundReLight;


// ===============================================================================
// = FUNCTION PROTOTYPES
// ===============================================================================
int CL_PMpointcontents(vec3_t point);

void R_Init_AliasArrays();
void R_Clean_AliasArrays();

void GL_Bind (int texnum);
void GL_MBind (GLenum target, int texnum);
void GL_SelectTexture (GLenum);
void GL_MBindCube (GLenum target, int texnum);

void GL_SetFontColor(const color4ub_t& color);

void R_LightPoint(vec3_t p, vec3_t color);

void R_InitLightgrid();

/**
 * HOLY SHIT!
 * FIX THIS FIX THIS FIX THIS!
 */
worldShadowLight_t *R_AddNewWorldLight (vec3_t origin, vec3_t color, float radius[3], int style, int filter, vec3_t angles, vec3_t speed,
	bool isStatic, int isShadow, int isAmbient, float cone, bool ingame, int flare, vec3_t flareOrg,
	float flareSize, char target[MAX_QPATH], int start_off, int fog, float fogDensity);


void R_DrawParticles();
void R_RenderDecals();
void R_LightColor(vec3_t org, vec3_t color);
bool R_CullAliasModel(vec3_t bbox[8], entity_t *e);
int CL_PMpointcontents2(vec3_t point, Model*  ignore);
void VID_MenuInit();
void R_ShutdownPrograms();
void GL_BindNullProgram();
void GL_BindRect(int texnum);
void GL_MBindRect(GLenum target, int texnum);
void R_Bloom();
void R_ThermalVision();
void R_RadialBlur();
void R_DofBlur();
void R_FXAA();
void R_FilmFilter();
void R_ListPrograms_f();
void R_InitPrograms();
void R_ClearWorldLights();
bool R_CullSphere(const vec3_t centre, const float radius);
void R_CastBspShadowVolumes();
void R_CastAliasShadowVolumes(bool player);
void R_DrawAliasMD2ModelLightPass(bool weapon_model);
void R_DrawAliasMD3ModelLightPass(bool weapon_model);
void R_SetupEntityMatrix(entity_t * e);
void GL_MBind3d(GLenum target, int texnum);
void R_SSAO();
void R_DrawDepthScene();
void R_DownsampleDepth();
void R_ScreenBlend();

void R_SaveLights_f();
void R_Light_Spawn_f();
void R_Light_SpawnAtPoint_f();
void R_Light_SpawnToCamera_f();
void R_Light_Delete_f();
void R_EditSelectedLight_f();
void R_MoveLightToRight_f();
void R_MoveLightForward_f();
void R_MoveLightUpDown_f();
void R_ChangeLightRadius_f();
void R_Light_Clone_f();
void R_ChangeLightCone_f();
void R_Light_UnSelect_f();
void R_FlareEdit_f();
void R_ResetFlarePos_f();
void R_Copy_Light_Properties_f();
void R_Paste_Light_Properties_f();

extern bool flareEdit;

void R_CalcCubeMapMatrix(bool model);
void DeleteShadowVertexBuffers();

void MakeFrustum4Light(worldShadowLight_t *light, bool ingame);
bool R_CullConeLight(vec3_t mins, vec3_t maxs, cplane_t *frust);
void GL_DrawAliasFrameLerpLight(dmd2header_t *paliashdr);

bool SurfInFrustum(msurface_t *s);
bool InLightVISEntity();

void R_DrawLightBrushModel();
void UpdateLightEditor();
void Load_LightFile();
bool BoundsIntersectsPoint(vec3_t mins, vec3_t maxs, vec3_t p);

bool PF_inPVS(vec3_t p1, vec3_t p2);



/**
 * GL config stuff
 */

/**
 *
 */
typedef struct
{
	int renderer;
	const char* renderer_string;
	const char* vendor_string;
	const char* version_string;
	const char* extensions3_string;

	int			screenTextureSize;
	const char* wglExtensionsString;

	const char* shadingLanguageVersionString;
	int			maxVertexUniformComponents;		// GLSL info
	int			maxVaryingFloats;
	int			maxVertexTextureImageUnits;
	int			maxCombinedTextureImageUnits;
	int			maxFragmentUniformComponents;
	int			maxVertexAttribs;
	int			maxTextureImageUnits;

	int			glMajorVersion;
	int			glMinorVersion;

	int			colorBits;
	int			alphaBits;
	int			depthBits;
	int			stencilBits;
	int			samples;
	int			maxSamples;
} glconfig_t;


/**
 *
 */
typedef struct
{
	bool		fullscreen;

	int				prev_mode;

	int				lightmap_textures;
	int				currenttextures[32]; // max gl_texturesXX
	int				currenttmu;

	bool			texture_compression_bptc;
	int				displayrefresh;

	bool			wgl_swap_control_tear;
	bool			depthBoundsTest;
	bool			shader5;
	bool			debugOutput;
	bool			bindlessTexture;
	bool			bufferStorage;

	int				programId;
	GLenum			matrixMode;

	mat4_t			projectionMatrix;
	mat4_t			modelViewMatrix;		// ready to load

										// frame buffer
	int				maxRenderBufferSize;
	int				maxColorAttachments;
	int				maxSamples;
	int				maxDrawBuffers;

	// gl state cache
	bool			cullFace;
	GLenum			cullMode;
	GLenum			frontFace;

	bool			blend;
	GLenum			blendSrc;
	GLenum			blendDst;

	GLboolean		colorMask[4];

	bool			depthTest;
	GLenum			depthFunc;
	GLboolean		depthMask;
	GLclampd		depthRange[2];

	bool			polygonOffsetFill;
	GLfloat			polygonOffsetFactor;
	GLfloat			polygonOffsetUnits;

	bool			stencilTest;
	GLenum			stencilFunc;
	GLenum			stencilFace;
	GLuint			stencilMask;
	GLint			stencilRef;
	GLuint			stencilRefMask;
	GLenum			stencilFail;
	GLenum			stencilZFail;
	GLenum			stencilZPass;

	bool			scissorTest;
	GLint			scissor[4];

	bool			glDepthBoundsTest;
	GLfloat			depthBoundsMins;
	GLfloat			depthBoundsMax;

	vec4_t			fontColor;
} glstate_t;


/**
*
*/
struct VBO
{
	GLuint	vbo_fullScreenQuad;
	GLuint	vbo_halfScreenQuad;
	GLuint	vbo_quarterScreenQuad;
	GLuint	ibo_quadTris;
	GLuint	vbo_Dynamic;
	GLuint	ibo_Dynamic;
	GLuint	vbo_BSP;
	GLuint	ibo_BSP;

	int xyz_offset;
	int st_offset;
	int lm_offset;
	int nm_offset;
	int tg_offset;
	int bn_offset;
};


/**
 *
 */
struct VAO
{
	GLuint bsp_a;
	GLuint bsp_l;
};


VBO& getVbo();
VAO& getVao();



// r_cull.c
void R_SetFrustum();

bool BoundsIntersect(const vec3_t mins1, const vec3_t maxs1, const vec3_t mins2, const vec3_t maxs2);
bool R_EntityInLightBounds();
bool R_CullBox(vec3_t mins, vec3_t maxs);
bool R_CullOrigin(vec3_t origin);
bool BoundsAndSphereIntersect(const vec3_t mins, const vec3_t maxs, const vec3_t origin, float radius);


// cl_clients.c
trace_t CL_PMTraceWorld (vec3_t start, vec3_t mins, vec3_t maxs, vec3_t end, int mask, bool checkAliases);


// r_surf.c
#define MAX_POLY_VERT		128
#define	MAX_BATCH_SURFS		21845

extern vec3_t wVertexArray[MAX_BATCH_SURFS];
extern float wTexArray[MAX_BATCH_SURFS][2];
extern float wLMArray[MAX_BATCH_SURFS][2];
extern vec4_t wColorArray[MAX_BATCH_SURFS];

extern float wTmu0Array[MAX_BATCH_SURFS][2];
extern float wTmu1Array[MAX_BATCH_SURFS][2];
extern float wTmu2Array[MAX_BATCH_SURFS][2];

extern size_t indexArray[MAX_MAP_VERTS * 3];
extern vec3_t nTexArray[MAX_BATCH_SURFS];
extern vec3_t tTexArray[MAX_BATCH_SURFS];
extern vec3_t bTexArray[MAX_BATCH_SURFS];

void R_DrawChainsRA(bool bmodel);
void R_DrawBrushModelRA();
void R_DrawBrushModel();
void R_DrawBSP();
void R_DrawLightWorld();
void R_MarkLeaves();


// r_warp.c
extern float skyrotate;
extern vec3_t skyaxis;

void R_DrawWaterPolygons(msurface_t* fa, bool bmodel);
void R_AddSkySurface(msurface_t* fa);
void R_ClearSkyBox();
void R_DrawSkyBox(bool color);


// r_misc.c
void GL_ScreenShot_f();
void R_InitEngineTextures();

void CreateSSAOBuffer();
void CreateFboBuffer();


// r_drawAlias.c
void R_DrawAliasMD2Model(entity_t * e);
void R_DrawAliasMD3Model(entity_t * e);

// r_main.c
extern int	occ_framecount;
extern Model* r_worldmodel;

extern glconfig_t gl_config;
extern glstate_t gl_state;

extern int g_numGlLights;

extern vec3_t lightspot;

bool R_Init(void *hinstance, void *hWnd);
void R_Shutdown();
void R_RenderView (refdef_t * fd);
void R_DrawSpriteModel(entity_t * e);
void R_DrawBeam();
void R_BeginFrame();
void R_SetupOrthoMatrix();


// r_draw2d.c
void R_LoadFont();

void Draw_GetPicSize(int& w, int& h, const std::string& name);
void Draw_Pic(int x, int y, char* name);
void Draw_StretchPic(int x, int y, int w, int h, char* name);
void Draw_Box(int x, int y, int w, int h, float r, float g, float b, float a);
void Draw_BoxFilled(int x, int y, int w, int h, float r, float g, float b, float a);

// r_light.c
extern worldShadowLight_t *shadowLight_static, *shadowLight_frame; /// \fixme	Move these to a sane place


// r_glstate.c
void GL_CullFace(GLenum mode);
void GL_FrontFace(GLenum mode);

void GL_DepthFunc(GLenum func);
void GL_DepthMask(GLboolean flag);
void GL_BlendFunc(GLenum src, GLenum dst);
void GL_ColorMask(GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha);

void GL_StencilMask(GLuint mask);
void GL_StencilFunc(GLenum func, GLint ref, GLuint mask);
void GL_StencilOp(GLenum fail, GLenum zFail, GLenum zPass);
void GL_StencilFuncSeparate(GLenum face, GLenum func, GLint ref, GLuint mask);
void GL_StencilOpSeparate(GLenum face, GLenum fail, GLenum zFail, GLenum zPass);

void GL_Scissor(GLint x, GLint y, GLint width, GLint height);
void GL_DepthRange(GLclampd n, GLclampd f);
void GL_PolygonOffset(GLfloat factor, GLfloat units);
void GL_DepthBoundsTest(GLfloat mins, GLfloat maxs);

void GL_Enable(GLenum cap);
void GL_Disable(GLenum cap);



#ifndef BIT
#define BIT(num) (1 << (num))
#endif

#define	MAX_LIGHTMAPS		4		// max number of atlases
#define	LIGHTMAP_SIZE		2048
#define GL_LIGHTMAP_FORMAT	GL_RGB
#define XPLM_NUMVECS		3

#define MAX_VERTICES				16384
#define MAX_INDICES					(MAX_VERTICES * 4)
#define MAX_VERTEX_ARRAY			8192
#define MAX_SHADOW_VERTS			16384

#define VA_SetElem2(v,a,b)			((v)[0]=(a),(v)[1]=(b))
#define VA_SetElem3(v,a,b,c)		((v)[0]=(a),(v)[1]=(b),(v)[2]=(c))
#define VA_SetElem4(v,a,b,c,d)		((v)[0]=(a),(v)[1]=(b),(v)[2]=(c),(v)[3]=(d))

#define VA_SetElem2v(v,a)			((v)[0]=(a)[0],(v)[1]=(a)[1])
#define VA_SetElem3v(v,a)			((v)[0]=(a)[0],(v)[1]=(a)[1],(v)[2]=(a)[2])
#define VA_SetElem4v(v,a)			((v)[0]=(a)[0],(v)[1]=(a)[1],(v)[2]=(a)[2],(v)[3]=(a)[3])


#define Vector4Set(v, a, b, c, d)	((v)[0]=(a),(v)[1]=(b),(v)[2]=(c),(v)[3]=(d))
#define Vector4Copy(a,b)			((b)[0]=(a)[0],(b)[1]=(a)[1],(b)[2]=(a)[2],(b)[3]=(a)[3])
#define PlaneDiff(point,plane)		(((plane)->type < 3 ? (point)[(plane)->type] : DotProduct((point), (plane)->normal)) - (plane)->dist)

#define Vector4Scale(in,scale,out)	((out)[0]=(in)[0]*scale,(out)[1]=(in)[1]*scale,(out)[2]=(in)[2]*scale,(out)[3]=(in)[3]*scale)
#define Vector4Add(a,b,c)			((c)[0]=(((a[0])+(b[0]))),(c)[1]=(((a[1])+(b[1]))),(c)[2]=(((a[2])+(b[2]))),(c)[3]=(((a[3])+(b[3]))))
#define Vector4Sub(a,b,c)			((c)[0]=(((a[0])-(b[0]))),(c)[1]=(((a[1])-(b[1]))),(c)[2]=(((a[2])-(b[2]))),(c)[3]=(((a[3])-(b[3]))))



/**
 * 
 */
typedef struct
{
	int internal_format;
	int current_lightmap_texture;

	msurface_t *lightmap_surfaces[MAX_LIGHTMAPS];

	int allocated[LIGHTMAP_SIZE];

	// the lightmap texture data needs to be kept in
	// main memory so texsubimage can update properly
	byte lightmap_buffer[3][LIGHTMAP_SIZE * LIGHTMAP_SIZE * 3];
} gllightmapstate_t;

extern gllightmapstate_t gl_lms;

// r_lightMaps.c
extern const vec3_t r_xplmBasisVecs[XPLM_NUMVECS];

/**
 * 
 */
typedef struct bonepose_s
{
	dualquat_t dualquat;
} bonepose_t;

#define	MAX_VERTEX_CACHES	4096

void R_DrawFullScreenQuad();
