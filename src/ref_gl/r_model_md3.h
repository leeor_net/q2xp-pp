#pragma once

#include <stdint.h>

#include "r_surface.h"

// ========================================================================
// = .MD3 model file format
// ========================================================================

#define IDMD3HEADER         (('3' << 24) + ('P' << 16) + ('D' << 8) + 'I')

#define MD3_ALIAS_VERSION   15

#define MD3_MAX_TRIANGLES   8192    // per mesh
#define MD3_MAX_VERTS       4096    // per mesh
#define MD3_MAX_SHADERS     256     // per mesh
#define MD3_MAX_FRAMES      1024    // per model
#define MD3_MAX_MESHES      32      // per model
#define MD3_MAX_TAGS        16      // per frame

#define MAX_SKIN_PATH			64

// vertex scales
#define MD3_XYZ_SCALE       ( 1.0 / 64 )


/**
 * \fixme	Find a sane place for this.
 */
typedef struct
{
	const char *header;
	const int *versions;
	int lightmapWidth;
	int lightmapHeight;
	int flags;
	int entityLumpNum;
} bspFormatDesc_t;


typedef struct
{
	float st[2];
} dmd3coord_t;


typedef struct
{
	short point[3];
	unsigned char norm[2];
} dmd3vertex_t;


typedef struct
{
	float mins[3];
	float maxs[3];
	float translate[3];
	float radius;
	char creator[16];
} dmd3frame_t;


typedef struct
{
	char name[MAX_SKIN_PATH];            // tag name
	float origin[3];
	float axis[3][3];
} dmd3tag_t;


typedef struct
{
	char name[MAX_SKIN_PATH];
	int unused;                         // shader
} dmd3skin_t;


typedef struct
{
	char id[4];

	char name[MAX_SKIN_PATH];

	int flags;

	int num_frames;
	int num_skins;
	int num_verts;
	int num_tris;

	int ofs_elems;
	int ofs_skins;
	int ofs_tcs;
	int ofs_verts;

	int meshsize;
} dmd3mesh_t;


typedef struct
{
	int id;
	int version;

	char filename[MAX_SKIN_PATH];

	int flags;

	int num_frames;
	int num_tags;
	int num_meshes;
	int num_skins;

	int ofs_frames;
	int ofs_tags;
	int ofs_meshes;
	int ofs_end;
} dmd3header_t;


// ==============================================================================
// = ALIAS MODELS
// ==============================================================================

// in memory representation
typedef struct
{
	short point[3];
	uint8_t latlong[2];                     // use bytes to keep 8-byte alignment
} maliasvertex_t;


typedef struct
{
	vec3_t mins, maxs;
	vec3_t scale;
	vec3_t translate;
	float radius;
} maliasframe_t;


typedef struct
{
	char name[MAX_SKIN_PATH];
	quat_t quat;
	vec3_t origin;
} maliastag_t;


typedef struct
{
	char name[MAX_SKIN_PATH];
	//shader_t        *shader;
	Image*		skin;
} maliasskin_t;


typedef struct maliasmesh_s
{
	char name[MAX_SKIN_PATH];

	int numverts;
	maliasvertex_t *vertexes;
	vec2_t          *stArray;

	vec4_t          *xyzArray;
	vec4_t          *normalsArray;
	vec4_t          *sVectorsArray;

	int numtris;
	elem_t          *elems;

	int numskins;
	maliasskin_t    *skins;

	struct mesh_vbo_s *vbo;
} maliasmesh_t;


struct maliasmodel_t
{
	int numframes;
	maliasframe_t   *frames;

	int numtags;
	maliastag_t     *tags;

	int nummeshes;
	maliasmesh_t    *meshes;
	drawSurfaceAlias_t *drawSurfs;

	int numskins;
	maliasskin_t    *skins;

	int numverts;             // sum of numverts for all meshes
	int numtris;             // sum of numtris for all meshes
};


void Mod_LoadAliasMD3Model(Model* mod, Model* parent, void* buffer);
