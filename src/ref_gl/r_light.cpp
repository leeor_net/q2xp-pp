/*
Copyright (C) 2004-2013 Quake2xp Team.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// r_lightMan.c
// Per-pixel light manager and editor basic idea based on Discoloda work

#include "r_local.h"

#include "r_math.h"
#include "r_program.h"
#include "r_util.h"

#include "../common/filesystem.h"
#include "../common/string_util.h"


// ===============================================
// = CONSTANTS
// ===============================================
#define MAX_LIGHT_PLANE_VERTICES	64
#define ON_EPSILON					0.1 // point on plane side epsilon


extern byte viewvis[MAX_MAP_LEAFS / 8];


// ===============================================
// = MODULE VAR'S
// ===============================================
worldShadowLight_t *currentShadowLight = nullptr;
worldShadowLight_t *shadowLight_static = nullptr, *shadowLight_frame = nullptr, *SELECTED_SHADOW_LIGHT = nullptr;
worldShadowLight_t	shadowLightsBlock[MAX_WORLD_SHADOW_LIGHTS];

static int num_dlits = 0;
int num_nwmLights = 0;
int NUM_VIS_LIGHTS = 0;

int r_numWorldShadowLights;

vec3_t vCache[MAX_VERTEX_ARRAY] = { 0.0f };

vec3_t player_org = { 0.0f };
vec3_t v_forward = { 0.0f };
vec3_t v_right = { 0.0f };
vec3_t v_up = { 0.0f };

bool flareEdit = false;


// ===============================================
// = EXTERNALS
// ===============================================
extern Model* LOAD_MODEL;
extern  int lightSurfSort(const msurface_t **a, const msurface_t **b);
extern int r_lightTimestamp;


// ===============================================
// = FUNCTION PROTOTYPES
// ===============================================
void R_DrawBspModelVolumes(bool precalc, worldShadowLight_t *light);
void R_LightFlareOutLine();
void R_AddLightInteraction(worldShadowLight_t *light);
void R_MarkLightCasting(mnode_t *node, bool precalc, worldShadowLight_t *light);
bool R_MarkLightLeaves(worldShadowLight_t *light);


/**
 * Gets the currently selected world light when in light editor mode.
 * 
 * \return	A pointer to the currently selected world light or \c nullptr
 *			if no light is selected.
 */
worldShadowLight_t* getCurrentSelectedLight()
{
	return SELECTED_SHADOW_LIGHT;
}


void R_DeslectLights()
{
	currentShadowLight = nullptr;
	shadowLight_static = nullptr;
	shadowLight_frame = nullptr;
	SELECTED_SHADOW_LIGHT = nullptr;
}


/**
 * Gets whether or not there is a light object selected.
 */
bool R_LightSelected()
{
	return SELECTED_SHADOW_LIGHT != nullptr;
}


/**
 * 
 */
bool R_AddLightToFrame(worldShadowLight_t *light, bool weapon)
{

	if (r_newrefdef.areabits)
	{
		if (!(r_newrefdef.areabits[light->area >> 3] & (1 << (light->area & 7))))
			return false;
	}

	if (light->startColor[0] <= 0.01 && light->startColor[1] <= 0.01 && light->startColor[2] <= 0.01 && !r_lightEditor->value)
		return false;

	if (!light->radius[0] && !light->radius[1] && !light->radius[2])
		return false;

	if (light->isCone)
	{
		if (R_CullConeLight(light->mins, light->maxs, light->frust))
			return false;
	}
	else if (light->spherical)
	{
		if (R_CullSphere(light->origin, light->radius[0]))
			return false;
	}
	else
	{
		if (R_CullBox(light->mins, light->maxs))
			return false;
	}

	if (weapon)
	{
		if (!BoundsAndSphereIntersect(light->mins, light->maxs, r_origin, 25.0))
			return false;
	}

	if (!R_HasSharedLeafs(light->vis, viewvis))
		return false;

	return true;
}


/**
 * 
 */
void UpdateLightBounds(worldShadowLight_t *light)
{
	int i;
	float x, y;
	mat3_3_t lightAxis;
	mat4_t tmpMatrix, mvMatrix;
	vec3_t tmp;

	if (light->radius[0] == light->radius[1] && light->radius[0] == light->radius[2])
		light->spherical = true;
	else
		light->spherical = false;

	for (i = 0; i < 3; i++)
	{
		light->mins[i] = light->origin[i] - light->radius[i];
		light->maxs[i] = light->origin[i] + light->radius[i];
	}

	if (light->spherical)
		light->maxRad = light->radius[0];
	else
		light->maxRad = max(max(light->radius[0], light->radius[1]), light->radius[2]);

	if (light->_cone)
	{
		light->isCone = 1;
		light->spherical = false;
	}

	light->distance = light->maxRad;

	for (i = 0; i < 8; i++)
	{
		tmp[0] = (i & 1) ? -light->radius[0] : light->radius[0];
		tmp[1] = (i & 2) ? -light->radius[1] : light->radius[1];
		tmp[2] = (i & 4) ? -light->radius[2] : light->radius[2];

		AnglesToMat3(light->angles, lightAxis);
		Mat3_TransposeMultiplyVector(lightAxis, tmp, light->corners[i]);
		VectorAdd(light->corners[i], light->origin, light->corners[i]);
	}

	AnglesToMat3(light->angles, light->axis);
	Mat4_SetupTransform(tmpMatrix, light->axis, light->origin);
	Mat4_AffineInvert(tmpMatrix, mvMatrix);

	// setup unit space conversion matrix
	if (light->isCone)
	{
		light->fov[0] = light->fov[1] = light->_cone * 0.5;

		x = tanf(light->fov[0] * 0.5f);
		y = tanf(light->fov[1] * 0.5f);

		tmpMatrix[0][0] = 1.f / light->distance;
		tmpMatrix[0][1] = 0.f;
		tmpMatrix[0][2] = 0.f;
		tmpMatrix[0][3] = 0.f;
		tmpMatrix[1][0] = 0.f;
		tmpMatrix[1][1] = 1.f / (light->distance * x);
		tmpMatrix[1][2] = 0.f;
		tmpMatrix[1][3] = 0.f;
		tmpMatrix[2][0] = 0.f;
		tmpMatrix[2][1] = 0.f;
		tmpMatrix[2][2] = 1.f / (light->distance * y);
		tmpMatrix[2][3] = 0.f;
		tmpMatrix[3][0] = 0.f;
		tmpMatrix[3][1] = 0.f;
		tmpMatrix[3][2] = 0.f;
		tmpMatrix[3][3] = 1.f;

		Mat4_Multiply(mvMatrix, tmpMatrix, light->spotMatrix);
	}

	if (light->isFog)
		tmpMatrix[0][0] = 0.f;
	else
		tmpMatrix[0][0] = 1.f / light->radius[0];

	tmpMatrix[0][1] = 0.f;
	tmpMatrix[0][2] = 0.f;
	tmpMatrix[0][3] = 0.f;
	tmpMatrix[1][0] = 0.f;

	if (light->isFog)
		tmpMatrix[1][1] = 0.f;
	else
		tmpMatrix[1][1] = 1.f / light->radius[1];

	tmpMatrix[1][2] = 0.f;
	tmpMatrix[1][3] = 0.f;
	tmpMatrix[2][0] = 0.f;
	tmpMatrix[2][1] = 0.f;
	tmpMatrix[2][2] = 1.f / light->radius[2];
	tmpMatrix[2][3] = 0.f;
	tmpMatrix[3][0] = 0.f;
	tmpMatrix[3][1] = 0.f;
	tmpMatrix[3][2] = 0.f;
	tmpMatrix[3][3] = 1.f;

	Mat4_Multiply(mvMatrix, tmpMatrix, light->attenMatrix);
}


/**
 * 
 */
void R_AddDynamicLight(dlight_t *dl)
{
	worldShadowLight_t *light;
	mat3_3_t			lightAxis;
	mat4_t				tmpMatrix, mvMatrix;
	vec3_t				tmp;
	int					i;

	if (R_CullSphere(dl->origin, dl->intensity))
		return;

	light = &shadowLightsBlock[num_dlits++];
	memset(light, 0, sizeof(worldShadowLight_t));
	light->next = shadowLight_frame;
	shadowLight_frame = light;

	VectorCopy(dl->origin, light->origin);
	VectorCopy(dl->color, light->startColor);
	VectorCopy(dl->angles, light->angles);
	VectorSet(light->radius, dl->intensity, dl->intensity, dl->intensity);

	for (i = 0; i < 3; i++)
	{
		light->mins[i] = light->origin[i] - dl->intensity;
		light->maxs[i] = light->origin[i] + dl->intensity;
	}

	light->style = 0;
	light->filter = dl->filter;
	light->isStatic = 0;
	light->_cone = dl->_cone;
	light->isNoWorldModel = 0;
	light->isShadow = 1;
	light->spherical = true;
	light->maxRad = dl->intensity;

	for (i = 0; i < 8; i++)
	{
		tmp[0] = (i & 1) ? -light->radius[0] : light->radius[0];
		tmp[1] = (i & 2) ? -light->radius[1] : light->radius[1];
		tmp[2] = (i & 4) ? -light->radius[2] : light->radius[2];

		AnglesToMat3(light->angles, lightAxis);
		Mat3_TransposeMultiplyVector(lightAxis, tmp, light->corners[i]);
		VectorAdd(light->corners[i], light->origin, light->corners[i]);
	}

	AnglesToMat3(light->angles, light->axis);
	Mat4_SetupTransform(tmpMatrix, light->axis, light->origin);
	Mat4_AffineInvert(tmpMatrix, mvMatrix);

	// setup unit space conversion matrix
	tmpMatrix[0][0] = 1.f / light->radius[0];
	tmpMatrix[0][1] = 0.f;
	tmpMatrix[0][2] = 0.f;
	tmpMatrix[0][3] = 0.f;
	tmpMatrix[1][0] = 0.f;
	tmpMatrix[1][1] = 1.f / light->radius[1];
	tmpMatrix[1][2] = 0.f;
	tmpMatrix[1][3] = 0.f;
	tmpMatrix[2][0] = 0.f;
	tmpMatrix[2][1] = 0.f;
	tmpMatrix[2][2] = 1.f / light->radius[2];
	tmpMatrix[2][3] = 0.f;
	tmpMatrix[3][0] = 0.f;
	tmpMatrix[3][1] = 0.f;
	tmpMatrix[3][2] = 0.f;
	tmpMatrix[3][3] = 1.f;

	Mat4_Multiply(mvMatrix, tmpMatrix, light->attenMatrix);
}


/**
 * 
 */
void R_AddNoWorldModelLight()
{
	worldShadowLight_t *light;
	mat4_t				tmpMatrix, mvMatrix;
	int					i;

	light = &shadowLightsBlock[num_nwmLights++];
	memset(light, 0, sizeof(worldShadowLight_t));
	light->next = shadowLight_frame;
	shadowLight_frame = light;
	VectorCopy(r_origin, light->origin);
	light->origin[2] -= 20.0;
	VectorSet(light->startColor, 1.0, 0.87, 0.66);
	VectorSet(light->color, 1.0, 0.87, 0.66);
	VectorSet(light->angles, 0, 0, 0);
	VectorSet(light->radius, 2048.0, 2048.0, 2048.0);

	for (i = 0; i < 3; i++)
	{
		light->mins[i] = light->origin[i] - 512.0;
		light->maxs[i] = light->origin[i] + 512.0;
	}

	light->style = 0;
	light->filter = 0;
	light->isStatic = 1;
	light->isShadow = 0;
	light->_cone = 0;
	light->isNoWorldModel = 1;
	light->flare = 0;
	light->isAmbient = 0;
	light->isCone = 0;
	light->spherical = true;
	light->maxRad = light->radius[0];

	AnglesToMat3(light->angles, light->axis);
	Mat4_SetupTransform(tmpMatrix, light->axis, light->origin);
	Mat4_AffineInvert(tmpMatrix, mvMatrix);

	// setup unit space conversion matrix
	tmpMatrix[0][0] = 1.f / light->radius[0];
	tmpMatrix[0][1] = 0.f;
	tmpMatrix[0][2] = 0.f;
	tmpMatrix[0][3] = 0.f;
	tmpMatrix[1][0] = 0.f;
	tmpMatrix[1][1] = 1.f / light->radius[1];
	tmpMatrix[1][2] = 0.f;
	tmpMatrix[1][3] = 0.f;
	tmpMatrix[2][0] = 0.f;
	tmpMatrix[2][1] = 0.f;
	tmpMatrix[2][2] = 1.f / light->radius[2];
	tmpMatrix[2][3] = 0.f;
	tmpMatrix[3][0] = 0.f;
	tmpMatrix[3][1] = 0.f;
	tmpMatrix[3][2] = 0.f;
	tmpMatrix[3][3] = 1.f;

	Mat4_Multiply(mvMatrix, tmpMatrix, light->attenMatrix);
}


void R_PrepareShadowLightFrame(bool weapon)
{
	int i;
	worldShadowLight_t *light;

	num_dlits = 0;
	num_nwmLights = 0;
	shadowLight_frame = nullptr;

	// add pre computed lights
	if (shadowLight_static)
	{
		for (light = shadowLight_static; light; light = light->s_next)
		{
			if (!R_AddLightToFrame(light, weapon))
				continue;

			light->next = shadowLight_frame;
			shadowLight_frame = light;
		}
	}

	// add tempory lights
	for (i = 0; i < r_newrefdef.num_dlights; i++)
	{
		if (num_dlits > MAX_WORLD_SHADOW_LIGHTS)
			break;

		R_AddDynamicLight(&r_newrefdef.dlights[i]);
	}

	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		R_AddNoWorldModelLight();

	if (!shadowLight_frame)
		return;

	for (light = shadowLight_frame; light; light = light->next)
	{
		if (!light->isStatic)
		{
			if (!R_MarkLightLeaves(light))
				continue;
		}

		MakeFrustum4Light(light, true);

		if (CL_PMpointcontents(light->origin) & MASK_WATER)
			light->castCaustics = false;
		else
			light->castCaustics = true;

		VectorCopy(light->startColor, light->color);

		if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
			continue;

		light->color[0] *= r_newrefdef.lightstyles[light->style].rgb[0];
		light->color[1] *= r_newrefdef.lightstyles[light->style].rgb[1];
		light->color[2] *= r_newrefdef.lightstyles[light->style].rgb[2];
	}
}


/**
 * 
 */
void R_SaveLights_f()
{
	char name[MAX_QPATH] = { '\0' }, path[MAX_QPATH] = { '\0' };
	int	 i = 0;

	if (!r_lightEditor->value)
	{
		Com_Printf("Type r_lightEditor 1 to enable light editing.\n");
		return;
	}

	if (!shadowLight_static)
		return;

	FS_StripExtension(r_worldmodel->name, name, sizeof(name));
	Q_sprintf(path, sizeof(path), "%s.xplit", name);
	remove(path); //remove prev version

	FS_File* f = FS_Open(path, FS_OPEN_WRITE);
	if (!f)
	{
		Com_Printf("Could not open %s.\n", path);
		return;
	}

	FS_fprintf(f, "//ReLight for %s\n//Generated by Rogue Arena\n\n", r_worldmodel->name);

	for (currentShadowLight = shadowLight_static; currentShadowLight; currentShadowLight = currentShadowLight->s_next)
	{
		FS_fprintf(f, "//Light %i\n", i);
		FS_fprintf(f, "{\n");
		FS_fprintf(f, "\"classname\" \"light\"\n");
		FS_fprintf(f, "\"origin\" \"%i %i %i\"\n", (int)currentShadowLight->origin[0], (int)currentShadowLight->origin[1], (int)currentShadowLight->origin[2]);
		FS_fprintf(f, "\"radius\" \"%i %i %i\"\n", (int)currentShadowLight->radius[0], (int)currentShadowLight->radius[1], (int)currentShadowLight->radius[2]);
		FS_fprintf(f, "\"color\" \"%.3f %.3f %.3f\"\n", currentShadowLight->startColor[0], currentShadowLight->startColor[1], currentShadowLight->startColor[2]);
		FS_fprintf(f, "\"style\" \"%i\"\n", (int)currentShadowLight->style);
		FS_fprintf(f, "\"filter\" \"%i\"\n", (int)currentShadowLight->filter);
		FS_fprintf(f, "\"angles\" \"%i %i %i\"\n", (int)currentShadowLight->angles[0], (int)currentShadowLight->angles[1], (int)currentShadowLight->angles[2]);
		FS_fprintf(f, "\"speed\" \"%.3f %.3f %.3f\"\n", currentShadowLight->speed[0], currentShadowLight->speed[1], currentShadowLight->speed[2]);
		FS_fprintf(f, "\"shadow\" \"%i\"\n", currentShadowLight->isShadow);
		FS_fprintf(f, "\"ambient\" \"%i\"\n", currentShadowLight->isAmbient);
		FS_fprintf(f, "\"_cone\" \"%.1f\"\n", currentShadowLight->_cone);

		if (currentShadowLight->targetname[0])
			FS_fprintf(f, "\"targetname\" \"%s\"\n", currentShadowLight->targetname);

		FS_fprintf(f, "\"spawnflags\" \"%i\"\n", currentShadowLight->start_off);
		FS_fprintf(f, "\"flareSize\" \"%i\"\n", (int)currentShadowLight->flareSize);
		FS_fprintf(f, "\"flareOrigin\" \"%i %i %i\"\n", (int)currentShadowLight->flareOrigin[0], (int)currentShadowLight->flareOrigin[1], (int)currentShadowLight->flareOrigin[2]);
		FS_fprintf(f, "\"flare\" \"%i\"\n", currentShadowLight->flare);
		if (currentShadowLight->isFog)
		{
			FS_fprintf(f, "\"fogLight\" \"%i\"\n", currentShadowLight->isFog);
			FS_fprintf(f, "\"fogDensity\" \"%.6f\"\n", currentShadowLight->fogDensity);
		}

		FS_fprintf(f, "}\n");
		i++;
	}

	FS_Close(f);
	Com_Printf(S_COLOR_MAGENTA "SaveLights_f: " S_COLOR_WHITE "Save lights to " S_COLOR_GREEN "%s.xplit\n" S_COLOR_WHITE "Save " S_COLOR_GREEN "%i" S_COLOR_WHITE " lights\n", name, i);
}


/**
 * 
 */
static void DeleteCurrentLight(worldShadowLight_t *l)
{
	worldShadowLight_t* light = nullptr;

	if (l == shadowLight_static)
	{
		shadowLight_static = l->s_next;
	}
	else
	{
		for (light = shadowLight_static; light; light = light->s_next)
		{
			if (light->s_next == l)
			{
				light->s_next = l->s_next;
				break;
			}
		}
	}
	free(l);
}


/**
 * Spawns a new light.
 * 
 * \fixme	Frequently doesn't work as expected and instead spawns a light at the world origin { 0, 0, 0 }
 */
void R_Light_Spawn_f()
{
	vec3_t	color = { 1.0, 1.0, 1.0 }, end, spawn, radius = { 300, 300, 300 };
	trace_t trace;
	char target[MAX_QPATH] = { '\0' };

	if (!r_lightEditor->value)
	{
		Com_Printf("Type r_lightEditor 1 to enable light editing.\n");
		return;
	}

	VectorScaleAndAdd(player_org, v_forward, end, 1024);
	trace = CL_PMTraceWorld(player_org, vec3_origin, vec3_origin, end, MASK_SOLID, false);

	if (trace.fraction != 1.0)
	{
		VectorScaleAndAdd(trace.endpos, v_forward, spawn, -10);
		R_AddNewWorldLight(spawn, color, radius, 0, 0, vec3_origin, vec3_origin, true, 1, 0, 0, true, 0, spawn, 10.0, target, 0, 0, 0.0);
	}
}


/**
 * Spawns a new light at a specified coordinate.
 */
void R_Light_SpawnAtPoint_f()
{
	vec3_t color = { 1.0, 1.0, 1.0 }, spawn = { 0.0f, 0.0f, 1.0f }, radius = { 300, 300, 300 };
	char target[MAX_QPATH] = { '\0' };

	if (!r_lightEditor->value)
	{
		Com_Printf("Type r_lightEditor 1 to enable light editing.\n");
		return;
	}

	if (Cmd_Argc() != 4)
	{
		Com_Printf("usage: spawnLightAtPoint X Y Z");
		return;
	}

	spawn[0] = atof(Cmd_Argv(1));
	spawn[1] = atof(Cmd_Argv(2));
	spawn[2] = atof(Cmd_Argv(3));

	R_AddNewWorldLight(spawn, color, radius, 0, 0, vec3_origin, vec3_origin, true, 1, 0, 0, true, 0, spawn, 10.0, target, 0, 0, 0.0);
}


/**
 * Spawns a new light in the position of the camera.
 * 
 * \fixme	Doesn't always work as expected.
 */
void R_Light_SpawnToCamera_f()
{
	vec3_t	color = { 1.0, 1.0, 1.0 }, radius = { 300, 300, 300 };
	char target[MAX_QPATH] = { '\0' };

	if (!r_lightEditor->value)
	{
		Com_Printf("Type r_lightEditor 1 to enable light editing.\n");
		return;
	}

	R_AddNewWorldLight(player_org, color, radius, 0, 0, vec3_origin, vec3_origin, true, 1, 0, 0, true, 0, player_org, 10.0, target, 0, 0, 0.0);
}


/**
 * Creates a clone of the currently selected light.
 */
void R_Light_Clone_f()
{
	vec3_t color, spawn, origin, angles, speed, radius, flareOrg, end;
	float _cone, flareSize, fogDensity;
	int style, filter, shadow, ambient, flare, flag, fog;
	char target[MAX_QPATH] = { '\0' };
	trace_t trace;

	if (!r_lightEditor->value)
	{
		Com_Printf("Type r_lightEditor 1 to enable light editing.\n");
		return;
	}

	if (!SELECTED_SHADOW_LIGHT)
	{
		Com_Printf("No selected light.\n");
		return;
	}

	VectorCopy(SELECTED_SHADOW_LIGHT->origin, origin);
	VectorCopy(SELECTED_SHADOW_LIGHT->color, color);
	VectorCopy(SELECTED_SHADOW_LIGHT->angles, angles);
	VectorCopy(SELECTED_SHADOW_LIGHT->speed, speed);
	VectorCopy(SELECTED_SHADOW_LIGHT->radius, radius);
	VectorCopy(SELECTED_SHADOW_LIGHT->flareOrigin, flareOrg);

	if (SELECTED_SHADOW_LIGHT->targetname[0])
		memcpy(target, SELECTED_SHADOW_LIGHT->targetname, sizeof(target));

	style = SELECTED_SHADOW_LIGHT->style;
	filter = SELECTED_SHADOW_LIGHT->filter;
	shadow = SELECTED_SHADOW_LIGHT->isShadow;
	ambient = SELECTED_SHADOW_LIGHT->isAmbient;
	_cone = SELECTED_SHADOW_LIGHT->_cone;
	flare = SELECTED_SHADOW_LIGHT->flare;
	flareSize = SELECTED_SHADOW_LIGHT->flareSize;
	flag = SELECTED_SHADOW_LIGHT->start_off;
	fog = SELECTED_SHADOW_LIGHT->isFog;
	fogDensity = SELECTED_SHADOW_LIGHT->fogDensity;
	VectorScaleAndAdd(player_org, v_forward, end, 1024);

	trace = CL_PMTraceWorld(player_org, vec3_origin, vec3_origin, end, MASK_SOLID, false);
	if (trace.fraction != 1.0)
	{
		VectorScaleAndAdd(trace.endpos, v_forward, spawn, -10);
		SELECTED_SHADOW_LIGHT = R_AddNewWorldLight(spawn, color, radius, style, filter, angles, vec3_origin, true, shadow, ambient, _cone, true, flare, flareOrg, flareSize, target, flag, fog, fogDensity);
	}
}


/*===========================
Copy - Paste Light Properties
===========================*/

/**
 * 
 */
typedef struct
{
	vec3_t	angles;
	vec3_t	speed;
	vec3_t	color;
	vec3_t	radius;

	float	fogDensity;
	float	flareSize;

	int		flare;
	int		style;

	float	cone;

	int		filter;
	int		isShadow;
	int		isAmbient;
	int		isFog;
	int		start_off;

	char	targetname[MAX_QPATH];
}lightClipboard_t;

lightClipboard_t LIGHT_CLIPBOARD;


/**
 * 
 */
void clearLightProperties(lightClipboard_t* l_clip)
{
	if (!l_clip)
		return;

	memset(&l_clip, 0, sizeof(lightClipboard_t));
}


/**
 * Copies currently selected light properties to the clipboard.
 */
void R_Copy_Light_Properties_f()
{
	if (!r_lightEditor->value)
	{
		Com_Printf("Type r_lightEditor 1 to enable light editing.\n");
		return;
	}

	if (!SELECTED_SHADOW_LIGHT)
	{
		Com_Printf("No selected light.\n");
		return;
	}

	clearLightProperties(&LIGHT_CLIPBOARD);

	VectorCopy(SELECTED_SHADOW_LIGHT->color, LIGHT_CLIPBOARD.color);
	VectorCopy(SELECTED_SHADOW_LIGHT->angles, LIGHT_CLIPBOARD.angles);
	VectorCopy(SELECTED_SHADOW_LIGHT->speed, LIGHT_CLIPBOARD.speed);
	VectorCopy(SELECTED_SHADOW_LIGHT->radius, LIGHT_CLIPBOARD.radius);

	memset(LIGHT_CLIPBOARD.targetname, 0, sizeof(LIGHT_CLIPBOARD.targetname));
	if (SELECTED_SHADOW_LIGHT->targetname[0])
		memcpy(LIGHT_CLIPBOARD.targetname, SELECTED_SHADOW_LIGHT->targetname, sizeof(LIGHT_CLIPBOARD.targetname));

	LIGHT_CLIPBOARD.style = SELECTED_SHADOW_LIGHT->style;
	LIGHT_CLIPBOARD.filter = SELECTED_SHADOW_LIGHT->filter;
	LIGHT_CLIPBOARD.isShadow = SELECTED_SHADOW_LIGHT->isShadow;
	LIGHT_CLIPBOARD.isAmbient = SELECTED_SHADOW_LIGHT->isAmbient;
	LIGHT_CLIPBOARD.cone = SELECTED_SHADOW_LIGHT->_cone;
	LIGHT_CLIPBOARD.flare = SELECTED_SHADOW_LIGHT->flare;
	LIGHT_CLIPBOARD.flareSize = SELECTED_SHADOW_LIGHT->flareSize;
	LIGHT_CLIPBOARD.start_off = SELECTED_SHADOW_LIGHT->start_off;
	LIGHT_CLIPBOARD.isFog = SELECTED_SHADOW_LIGHT->isFog;
	LIGHT_CLIPBOARD.fogDensity = SELECTED_SHADOW_LIGHT->fogDensity;

	//Com_Printf("Copy light properties to clipboard.\n");
}


/**
 * Pastes light properties from the clipboard to the currently selected light.
 */
void R_Paste_Light_Properties_f()
{
	if (!r_lightEditor->value)
	{
		Com_Printf("Type r_lightEditor 1 to enable light editing.\n");
		return;
	}

	if (!LIGHT_CLIPBOARD.radius[0])
	{
		//Com_Printf("No light clipboard data.\n");
		return;
	}

	if (SELECTED_SHADOW_LIGHT == currentShadowLight)
		return;

	VectorCopy(LIGHT_CLIPBOARD.color, SELECTED_SHADOW_LIGHT->startColor);
	VectorCopy(LIGHT_CLIPBOARD.angles, SELECTED_SHADOW_LIGHT->angles);
	VectorCopy(LIGHT_CLIPBOARD.speed, SELECTED_SHADOW_LIGHT->speed);
	VectorCopy(LIGHT_CLIPBOARD.radius, SELECTED_SHADOW_LIGHT->radius);

	if (LIGHT_CLIPBOARD.targetname[0])
		memcpy(SELECTED_SHADOW_LIGHT->targetname, LIGHT_CLIPBOARD.targetname, sizeof(SELECTED_SHADOW_LIGHT->targetname));

	SELECTED_SHADOW_LIGHT->style		= LIGHT_CLIPBOARD.style;
	SELECTED_SHADOW_LIGHT->filter		= LIGHT_CLIPBOARD.filter;
	SELECTED_SHADOW_LIGHT->isShadow		= LIGHT_CLIPBOARD.isShadow;
	SELECTED_SHADOW_LIGHT->isAmbient	= LIGHT_CLIPBOARD.isAmbient;
	SELECTED_SHADOW_LIGHT->_cone		= LIGHT_CLIPBOARD.cone;
	SELECTED_SHADOW_LIGHT->flare		= LIGHT_CLIPBOARD.flare;
	SELECTED_SHADOW_LIGHT->flareSize	= LIGHT_CLIPBOARD.flareSize;
	SELECTED_SHADOW_LIGHT->start_off	= LIGHT_CLIPBOARD.start_off;
	SELECTED_SHADOW_LIGHT->isFog		= LIGHT_CLIPBOARD.isFog;
	SELECTED_SHADOW_LIGHT->fogDensity	= LIGHT_CLIPBOARD.fogDensity;

	UpdateLightBounds(SELECTED_SHADOW_LIGHT);
	R_MarkLightLeaves(SELECTED_SHADOW_LIGHT);
	R_DrawBspModelVolumes(true, SELECTED_SHADOW_LIGHT);
	R_AddLightInteraction(SELECTED_SHADOW_LIGHT);

	//Com_Printf("Paste light properties from clipboard.\n");
}


/**
 * Parses and processes light edit commands.
 */
void R_EditSelectedLight_f()
{
	vec3_t color, origin, angles, speed, radius, fOrg;
	float _cone, fSize, fogDensity;
	int style, /*filter,*/ shadow, ambient, flare, start_off, fog;
	char target[MAX_QPATH];

	if (!r_lightEditor->value)
	{
		Com_Printf("Type r_lightEditor 1 to enable light editing.\n");
		return;
	}

	if (!SELECTED_SHADOW_LIGHT)
	{
		Com_Printf("No selected light.\n");
		return;
	}

	VectorCopy(SELECTED_SHADOW_LIGHT->origin, origin);
	VectorCopy(SELECTED_SHADOW_LIGHT->color, color);
	VectorCopy(SELECTED_SHADOW_LIGHT->angles, angles);
	VectorCopy(SELECTED_SHADOW_LIGHT->radius, radius);
	VectorCopy(SELECTED_SHADOW_LIGHT->flareOrigin, fOrg);

	if (SELECTED_SHADOW_LIGHT->targetname[0])
	{
		memset(target, 0, sizeof(target));
		memcpy(target, SELECTED_SHADOW_LIGHT->targetname, sizeof(target));
	}

	style = SELECTED_SHADOW_LIGHT->style;
	//filter = SELECTED_SHADOW_LIGHT->filter;
	shadow = SELECTED_SHADOW_LIGHT->isShadow;
	ambient = SELECTED_SHADOW_LIGHT->isAmbient;
	_cone = SELECTED_SHADOW_LIGHT->_cone;
	flare = SELECTED_SHADOW_LIGHT->flare;
	fSize = SELECTED_SHADOW_LIGHT->flareSize;
	start_off = SELECTED_SHADOW_LIGHT->start_off;
	fog = SELECTED_SHADOW_LIGHT->isFog;
	fogDensity = SELECTED_SHADOW_LIGHT->fogDensity;

	if (!strcmp(Cmd_Argv(1), "origin"))
	{
		if (Cmd_Argc() != 5)
		{
			Com_Printf("usage: editLight: %s X Y Z\nCurrent Origin: %.4f %.4f %.4f\n", Cmd_Argv(0), SELECTED_SHADOW_LIGHT->origin[0], SELECTED_SHADOW_LIGHT->origin[1], SELECTED_SHADOW_LIGHT->origin[2]);
			return;
		}
		origin[0] = atof(Cmd_Argv(2));
		origin[1] = atof(Cmd_Argv(3));
		origin[2] = atof(Cmd_Argv(4));
		VectorCopy(origin, SELECTED_SHADOW_LIGHT->origin);
		UpdateLightBounds(SELECTED_SHADOW_LIGHT);
		R_MarkLightLeaves(SELECTED_SHADOW_LIGHT);
		R_DrawBspModelVolumes(true, SELECTED_SHADOW_LIGHT);
		R_AddLightInteraction(SELECTED_SHADOW_LIGHT);
	}
	else if (!strcmp(Cmd_Argv(1), "color"))
	{
		if (Cmd_Argc() != 5)
		{
			Com_Printf("usage: editLight: %s Red Green Blue\nCurrent Color: %.4f %.4f %.4f\n", Cmd_Argv(0), SELECTED_SHADOW_LIGHT->color[0], SELECTED_SHADOW_LIGHT->color[1], SELECTED_SHADOW_LIGHT->color[2]);
			return;
		}
		color[0] = atof(Cmd_Argv(2));
		color[1] = atof(Cmd_Argv(3));
		color[2] = atof(Cmd_Argv(4));
		VectorCopy(color, SELECTED_SHADOW_LIGHT->startColor);
	}
	else if (!strcmp(Cmd_Argv(1), "speed"))
	{
		if (Cmd_Argc() != 5)
		{
			Com_Printf("usage: editLight: %s X rotate speed Y rotate speed Z rotate speed\nCurrent speed rotations: %.4f %.4f %.4f\n", Cmd_Argv(0), SELECTED_SHADOW_LIGHT->speed[0], SELECTED_SHADOW_LIGHT->speed[1], SELECTED_SHADOW_LIGHT->speed[2]);
			return;
		}
		speed[0] = atof(Cmd_Argv(2));
		speed[1] = atof(Cmd_Argv(3));
		speed[2] = atof(Cmd_Argv(4));
		VectorCopy(speed, SELECTED_SHADOW_LIGHT->speed);
	}
	else if (!strcmp(Cmd_Argv(1), "radius"))
	{
		if (Cmd_Argc() != 5)
		{
			Com_Printf("usage: editLight: %s value\nCurrent Radius: %.1f %.1f %.1f\n", Cmd_Argv(0), SELECTED_SHADOW_LIGHT->radius[0], SELECTED_SHADOW_LIGHT->radius[1], SELECTED_SHADOW_LIGHT->radius[2]);
			return;
		}
		radius[0] = atof(Cmd_Argv(2));
		radius[1] = atof(Cmd_Argv(3));
		radius[2] = atof(Cmd_Argv(4));
		VectorCopy(radius, SELECTED_SHADOW_LIGHT->radius);
		UpdateLightBounds(SELECTED_SHADOW_LIGHT);
		R_MarkLightLeaves(SELECTED_SHADOW_LIGHT);
		R_DrawBspModelVolumes(true, SELECTED_SHADOW_LIGHT);
		R_AddLightInteraction(SELECTED_SHADOW_LIGHT);
	}
	else if (!strcmp(Cmd_Argv(1), "cone"))
	{
		if (Cmd_Argc() != 3)
		{
			Com_Printf("usage: editLight: %s value\nCurrent Light Cone: %.1f\n", Cmd_Argv(0), SELECTED_SHADOW_LIGHT->_cone);
			return;
		}
		_cone = atof(Cmd_Argv(2));
		SELECTED_SHADOW_LIGHT->_cone = _cone;
		UpdateLightBounds(SELECTED_SHADOW_LIGHT);
		R_MarkLightLeaves(SELECTED_SHADOW_LIGHT);
		R_DrawBspModelVolumes(true, SELECTED_SHADOW_LIGHT);
		R_AddLightInteraction(SELECTED_SHADOW_LIGHT);
	}
	else if (!strcmp(Cmd_Argv(1), "style"))
	{
		if (Cmd_Argc() != 3)
		{
			Com_Printf("usage: editLight %s value\nCurrent Style %i\n", Cmd_Argv(0), SELECTED_SHADOW_LIGHT->style);
			return;
		}
		style = atoi(Cmd_Argv(2));
		SELECTED_SHADOW_LIGHT->style = style;
	}
	else if (!strcmp(Cmd_Argv(1), "filter"))
	{
		if (Cmd_Argc() != 3)
		{
			Com_Printf("usage: editLight %s value\nCurrent Cube Filter %i\n", Cmd_Argv(0), SELECTED_SHADOW_LIGHT->filter);
			return;
		}

		int filter = clamp(atoi(Cmd_Argv(2)), 0, 255);

		if (r_lightCubeMap[filter] != nullptr)
		{
			SELECTED_SHADOW_LIGHT->filter = filter;
		}
		else
		{
			Com_Printf("Unavailable light filter \'%s\'. Current light filter %i\n", Cmd_Argv(2), SELECTED_SHADOW_LIGHT->filter);
		}
	}
	else if (!strcmp(Cmd_Argv(1), "angles"))
	{
		if (Cmd_Argc() != 5)
		{
			Com_Printf("usage: editLight: %s X Y Z \nCurrent Angles: %.4f %.4f %.4f\n", Cmd_Argv(0), SELECTED_SHADOW_LIGHT->angles[0], SELECTED_SHADOW_LIGHT->angles[1], SELECTED_SHADOW_LIGHT->angles[2]);
			return;
		}
		
		angles[0] = atof(Cmd_Argv(2));
		angles[1] = atof(Cmd_Argv(3));
		angles[2] = atof(Cmd_Argv(4));

		VectorCopy(angles, SELECTED_SHADOW_LIGHT->angles);
		UpdateLightBounds(SELECTED_SHADOW_LIGHT);
		R_AddLightInteraction(SELECTED_SHADOW_LIGHT);
	}
	else if (!strcmp(Cmd_Argv(1), "shadow"))
	{
		if (Cmd_Argc() != 3)
		{
			Com_Printf("usage: editLight %s value\nCurrent Shadow Flag is %i\n", Cmd_Argv(0), SELECTED_SHADOW_LIGHT->isShadow);
			return;
		}
		shadow = atoi(Cmd_Argv(2));
		SELECTED_SHADOW_LIGHT->isShadow = shadow;
	}
	else if (!strcmp(Cmd_Argv(1), "ambient"))
	{
		if (Cmd_Argc() != 3)
		{
			Com_Printf("usage: editLight %s value\nCurrent Ambient Flag is %i\n", Cmd_Argv(0), SELECTED_SHADOW_LIGHT->isAmbient);
			return;
		}
		ambient = atoi(Cmd_Argv(2));
		SELECTED_SHADOW_LIGHT->isAmbient = ambient;
	}
	else if (!strcmp(Cmd_Argv(1), "flare"))
	{
		if (Cmd_Argc() != 3)
		{
			Com_Printf("usage: editLight %s value\nCurrent Flare Flag is %i\n", Cmd_Argv(0), SELECTED_SHADOW_LIGHT->flare);
			return;
		}
		flare = atoi(Cmd_Argv(2));
		SELECTED_SHADOW_LIGHT->flare = flare;
		VectorCopy(SELECTED_SHADOW_LIGHT->origin, SELECTED_SHADOW_LIGHT->flareOrigin);
	}
	else if (!strcmp(Cmd_Argv(1), "flareSize"))
	{
		if (Cmd_Argc() != 3)
		{
			Com_Printf("usage: editLight %s value\nCurrent Flare Size is %i\n", Cmd_Argv(0), SELECTED_SHADOW_LIGHT->flareSize);
			return;
		}
		fSize = atoi(Cmd_Argv(2));
		SELECTED_SHADOW_LIGHT->flareSize = fSize;
	}
	else if (!strcmp(Cmd_Argv(1), "flareOrigin"))
	{
		if (Cmd_Argc() != 5)
		{
			Com_Printf("usage: editLight: %s X Y Z\nCurrent Flare Origin: %.4f %.4f %.4f\n", Cmd_Argv(0), SELECTED_SHADOW_LIGHT->flareOrigin[0], SELECTED_SHADOW_LIGHT->flareOrigin[1], SELECTED_SHADOW_LIGHT->flareOrigin[2]);
			return;
		}
		fOrg[0] = atof(Cmd_Argv(2));
		fOrg[1] = atof(Cmd_Argv(3));
		fOrg[2] = atof(Cmd_Argv(4));
		VectorCopy(fOrg, SELECTED_SHADOW_LIGHT->flareOrigin);
	}
	else if (!strcmp(Cmd_Argv(1), "target"))
	{
		if (Cmd_Argc() != 3)
		{
			Com_Printf("usage: editLight: %s value\nCurrent Light Target: %s\n", Cmd_Argv(0), SELECTED_SHADOW_LIGHT->targetname);
			return;
		}
		memcpy(SELECTED_SHADOW_LIGHT->targetname, Cmd_Argv(2), sizeof(SELECTED_SHADOW_LIGHT->targetname));
	}
	else if (!strcmp(Cmd_Argv(1), "start_off"))
	{
		if (Cmd_Argc() != 3)
		{
			Com_Printf("usage: editLight %s value\nCurrent Start Off Flag is %i\n", Cmd_Argv(0), SELECTED_SHADOW_LIGHT->start_off);
			return;
		}
		start_off = atoi(Cmd_Argv(2));
		SELECTED_SHADOW_LIGHT->start_off = start_off;
	}
	else if (!strcmp(Cmd_Argv(1), "fog"))
	{
		if (Cmd_Argc() != 3)
		{
			Com_Printf("usage: editLight %s value\nCurrent Fog Light Flag is %i\n", Cmd_Argv(0), SELECTED_SHADOW_LIGHT->isFog);
			return;
		}
		fog = atoi(Cmd_Argv(2));
		SELECTED_SHADOW_LIGHT->isFog = fog;
	}
	else if (!strcmp(Cmd_Argv(1), "fogDensity"))
	{
		if (Cmd_Argc() != 3)
		{
			Com_Printf("usage: editLight %s value\nCurrent Fog Light Flag is %f\n", Cmd_Argv(0), SELECTED_SHADOW_LIGHT->fogDensity);
			return;
		}
		fogDensity = atof(Cmd_Argv(2));
		SELECTED_SHADOW_LIGHT->fogDensity = fogDensity;
	}
}


/**
 * 
 */
void R_FlareEdit_f()
{
	int mode;

	if (Cmd_Argc() != 2)
	{
		Com_Printf("Toggle Flare Editing Mode.\nUsage: editFlare: 0 or 1\n");
		return;
	}

	mode = atoi(Cmd_Argv(1));
	flareEdit = mode > 0;
}


/**
 * 
 */
void R_ResetFlarePos_f()
{
	if (!r_lightEditor->value)
	{
		Com_Printf("Type r_lightEditor 1 to enable light editing.\n");
		return;
	}

	if (!SELECTED_SHADOW_LIGHT)
	{
		Com_Printf("No selected light.\n");
		return;
	}

	VectorCopy(SELECTED_SHADOW_LIGHT->origin, SELECTED_SHADOW_LIGHT->flareOrigin);
}


/**
 * 
 */
void R_MoveLightToRight_f()
{
	vec3_t	origin, origin2;
	float	offset;

	if (!r_lightEditor->value)
	{
		Com_Printf("Type r_lightEditor 1 to enable light editing.\n");
		return;
	}

	if (!SELECTED_SHADOW_LIGHT)
	{
		Com_Printf("No selected light.\n");
		return;
	}

	if (Cmd_Argc() != 2)
	{
		Com_Printf("Usage: moveLight_right: <value>\n");
		return;
	}

	offset = atof(Cmd_Argv(1));

	if (!flareEdit)
	{
		VectorCopy(SELECTED_SHADOW_LIGHT->origin, origin);

		if (r_cameraSpaceLightMove->value)
			VectorScaleAndAdd(origin, v_right, origin, offset);
		else
			origin[0] += offset;

		VectorCopy(origin, SELECTED_SHADOW_LIGHT->origin);
		UpdateLightBounds(SELECTED_SHADOW_LIGHT);
		R_MarkLightLeaves(SELECTED_SHADOW_LIGHT);
		R_DrawBspModelVolumes(true, SELECTED_SHADOW_LIGHT);
		R_AddLightInteraction(SELECTED_SHADOW_LIGHT);
	}

	// move flare
	VectorCopy(SELECTED_SHADOW_LIGHT->flareOrigin, origin2);

	if (r_cameraSpaceLightMove->value)
		VectorScaleAndAdd(origin2, v_right, origin2, offset);
	else
		origin2[0] += offset;

	VectorCopy(origin2, SELECTED_SHADOW_LIGHT->flareOrigin);
}


/**
 * 
 */
void R_MoveLightForward_f()
{
	vec3_t origin, origin2;
	float  offset, fix;

	if (!r_lightEditor->value)
	{
		Com_Printf("Type r_lightEditor 1 to enable light editing.\n");
		return;
	}

	if (!SELECTED_SHADOW_LIGHT)
	{
		Com_Printf("No selected light.\n");
		return;
	}

	if (Cmd_Argc() != 2)
	{
		Com_Printf("Usage: moveLight_forward: <value>\n");
		return;
	}

	offset = atof(Cmd_Argv(1));

	if (!flareEdit)
	{
		VectorCopy(SELECTED_SHADOW_LIGHT->origin, origin);

		if (r_cameraSpaceLightMove->value)
		{
			fix = origin[2];
			VectorScaleAndAdd(origin, v_forward, origin, offset);
			origin[2] = fix;
		}
		else
			origin[1] += offset;

		VectorCopy(origin, SELECTED_SHADOW_LIGHT->origin);
		UpdateLightBounds(SELECTED_SHADOW_LIGHT);
		R_MarkLightLeaves(SELECTED_SHADOW_LIGHT);
		R_DrawBspModelVolumes(true, SELECTED_SHADOW_LIGHT);
		R_AddLightInteraction(SELECTED_SHADOW_LIGHT);
	}

	// move flare	
	VectorCopy(SELECTED_SHADOW_LIGHT->flareOrigin, origin2);
	if (r_cameraSpaceLightMove->value)
	{
		fix = origin2[2];
		VectorScaleAndAdd(origin2, v_forward, origin2, offset);
		origin2[2] = fix;
	}
	else
		origin2[1] += offset;

	VectorCopy(origin2, SELECTED_SHADOW_LIGHT->flareOrigin);
}


/**
 * 
 */
void R_MoveLightUpDown_f()
{
	vec3_t	origin, origin2;
	float	offset;

	if (!r_lightEditor->value)
	{
		Com_Printf("Type r_lightEditor 1 to enable light editing.\n");
		return;
	}

	if (!SELECTED_SHADOW_LIGHT)
	{
		Com_Printf("No selected light.\n");
		return;
	}


	if (Cmd_Argc() != 2)
	{
		Com_Printf("Usage: moveSelectedLight_right: <value>\n");
		return;
	}

	if (!flareEdit)
	{
		VectorCopy(SELECTED_SHADOW_LIGHT->origin, origin);

		offset = atof(Cmd_Argv(1));
		origin[2] += offset;

		VectorCopy(origin, SELECTED_SHADOW_LIGHT->origin);
		UpdateLightBounds(SELECTED_SHADOW_LIGHT);
		R_MarkLightLeaves(SELECTED_SHADOW_LIGHT);
		R_DrawBspModelVolumes(true, SELECTED_SHADOW_LIGHT);
		R_AddLightInteraction(SELECTED_SHADOW_LIGHT);
	}

	// move flare
	VectorCopy(SELECTED_SHADOW_LIGHT->flareOrigin, origin2);
	offset = atof(Cmd_Argv(1));
	origin2[2] += offset;
	VectorCopy(origin2, SELECTED_SHADOW_LIGHT->flareOrigin);
}


/**
 * 
 */
void R_ChangeLightRadius_f()
{
	float	offset, fRad;
	vec3_t	rad;

	if (!r_lightEditor->value)
	{
		Com_Printf("Type r_lightEditor 1 to enable light editing.\n");
		return;
	}

	if (!SELECTED_SHADOW_LIGHT)
	{
		Com_Printf("No selected light.\n");
		return;
	}

	if (Cmd_Argc() != 2)
	{
		Com_Printf("Usage: ajustSelectedLightRadius: X Y Z\n");
		return;
	}
	if (flareEdit)
	{
		fRad = SELECTED_SHADOW_LIGHT->flareSize;
		offset = atof(Cmd_Argv(1));
		fRad += offset;
		SELECTED_SHADOW_LIGHT->flareSize = fRad;
	}
	else
	{
		VectorCopy(SELECTED_SHADOW_LIGHT->radius, rad);

		offset = atof(Cmd_Argv(1));

		rad[0] += offset;
		rad[1] += offset;
		rad[2] += offset;

		if (rad[0] < 10)
			rad[0] = 10;

		if (rad[1] < 10)
			rad[1] = 10;

		if (rad[2] < 10)
			rad[2] = 10;

		VectorCopy(rad, SELECTED_SHADOW_LIGHT->radius);
		UpdateLightBounds(SELECTED_SHADOW_LIGHT);
		R_MarkLightLeaves(SELECTED_SHADOW_LIGHT);
		R_DrawBspModelVolumes(true, SELECTED_SHADOW_LIGHT);
		R_AddLightInteraction(SELECTED_SHADOW_LIGHT);
	}
}


/**
 * 
 */
void R_ChangeLightCone_f()
{
	float cone, offset;

	if (!r_lightEditor->value)
	{
		Com_Printf("Type r_lightEditor 1 to enable light editing.\n");
		return;
	}

	if (!SELECTED_SHADOW_LIGHT)
	{
		Com_Printf("No selected light.\n");
		return;
	}

	if (Cmd_Argc() != 2)
	{
		Com_Printf("Usage: ajustSelectedLightRadius: <value>\n");
		return;
	}

	cone = SELECTED_SHADOW_LIGHT->_cone;
	offset = atof(Cmd_Argv(1));

	cone += offset;
	if (cone > 999999)
		cone = 999999;

	if (cone < 0)
		cone = 0;

	SELECTED_SHADOW_LIGHT->_cone = cone;
	R_DrawBspModelVolumes(true, SELECTED_SHADOW_LIGHT);
	R_MarkLightLeaves(SELECTED_SHADOW_LIGHT);
	UpdateLightBounds(SELECTED_SHADOW_LIGHT);
	R_AddLightInteraction(SELECTED_SHADOW_LIGHT);
}


/**
 * 
 */
void R_Light_Delete_f()
{
	if (!r_lightEditor->value)
	{
		Com_Printf("Type r_lightEditor 1 to enable light editing.\n");
		return;
	}

	if (!SELECTED_SHADOW_LIGHT)
	{
		Com_Printf("No selected light.\n");
		return;
	}

	DeleteCurrentLight(SELECTED_SHADOW_LIGHT);
	SELECTED_SHADOW_LIGHT = nullptr;
}


/**
 * 
 */
void R_Light_UnSelect_f()
{

	if (!r_lightEditor->value)
	{
		Com_Printf("Type r_lightEditor 1 to enable light editing.\n");
		return;
	}

	if (!SELECTED_SHADOW_LIGHT)
	{
		Com_Printf("No selected light.\n");
		return;
	}
	SELECTED_SHADOW_LIGHT = nullptr;
}


/**
 * 
 */
void UpdateLightEditor()
{
	vec3_t		end_trace, mins = { -5.0f, -5.0f, -5.0f }, maxs = { 5.0f, 5.0f, 5.0f };
	vec3_t		corners[8], tmp;
	int			j;
	float		fraction = 1.0;
	vec3_t		v[8];
	trace_t		trace_light, trace_bsp;
	unsigned	headNode;
	vec3_t		tmpOrg, tmpRad;

	if (!r_lightEditor->value)
		return;

	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		return;

	if (!currentShadowLight->isStatic)
		return;

	GL_Disable(GL_SCISSOR_TEST);

	if (gl_state.depthBoundsTest && r_useDepthBounds->value)
		GL_Disable(GL_DEPTH_BOUNDS_TEST_EXT);

	GL_Disable(GL_BLEND);
	GL_Disable(GL_STENCIL_TEST);
	GL_Disable(GL_CULL_FACE);

	// stupid player camera and angles corruption, fixed
	VectorCopy(r_origin, player_org);
	AngleVectors(r_newrefdef.viewangles, v_forward, v_right, v_up);

	// create a temp hull from bounding box
	headNode = CM_HeadnodeForBox(mins, maxs);
	VectorScaleAndAdd(r_origin, v_forward, end_trace, 1024);

	trace_bsp = CL_PMTraceWorld(r_origin, vec3_origin, vec3_origin, end_trace, MASK_SOLID, false); //bsp collision with bmodels

	// light in focus?
	trace_light = CM_TransformedBoxTrace(r_origin, trace_bsp.endpos, vec3_origin, vec3_origin, headNode, MASK_ALL,
		currentShadowLight->origin, vec3_origin); // find light

	if (trace_light.fraction < fraction)
	{
		SELECTED_SHADOW_LIGHT = currentShadowLight;
		fraction = trace_light.fraction;
	}

	// setup program
	GL_BindProgram(genericProgram, 0);
	glUniform1i(gen_attribColors, 0);
	glUniform1i(gen_sky, 0);
	glUniform1i(gen_3d, 1);
	glUniformMatrix4fv(gen_mvp, 1, false, (const float *)r_newrefdef.modelViewProjectionMatrix);

	glEnableVertexAttribArray(ATT_POSITION);
	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, vCache);

	if (currentShadowLight != SELECTED_SHADOW_LIGHT)
	{

		for (j = 0; j < 8; j++)
		{
			tmp[0] = (j & 1) ? -5.0f : 5.0f;
			tmp[1] = (j & 2) ? -5.0f : 5.0f;
			tmp[2] = (j & 4) ? -5.0f : 5.0f;

			VectorAdd(tmp, currentShadowLight->origin, corners[j]);
		}

		glUniform4f(gen_color, currentShadowLight->color[0], currentShadowLight->color[1], currentShadowLight->color[2], 1.0);
		glEnable(GL_LINE_SMOOTH);
		glLineWidth(3.0);

		// top quad
		VA_SetElem3(vCache[0], corners[0][0], corners[0][1], corners[0][2]);
		VA_SetElem3(vCache[1], corners[1][0], corners[1][1], corners[1][2]);
		VA_SetElem3(vCache[2], corners[2][0], corners[2][1], corners[2][2]);
		VA_SetElem3(vCache[3], corners[3][0], corners[3][1], corners[3][2]);
		VA_SetElem3(vCache[4], corners[3][0], corners[3][1], corners[3][2]);
		VA_SetElem3(vCache[5], corners[1][0], corners[1][1], corners[1][2]);
		VA_SetElem3(vCache[6], corners[2][0], corners[2][1], corners[2][2]);
		VA_SetElem3(vCache[7], corners[0][0], corners[0][1], corners[0][2]);
		// bottom quad
		VA_SetElem3(vCache[8], corners[4][0], corners[4][1], corners[4][2]);
		VA_SetElem3(vCache[9], corners[5][0], corners[5][1], corners[5][2]);
		VA_SetElem3(vCache[10], corners[6][0], corners[6][1], corners[6][2]);
		VA_SetElem3(vCache[11], corners[7][0], corners[7][1], corners[7][2]);
		VA_SetElem3(vCache[12], corners[7][0], corners[7][1], corners[7][2]);
		VA_SetElem3(vCache[13], corners[5][0], corners[5][1], corners[5][2]);
		VA_SetElem3(vCache[14], corners[6][0], corners[6][1], corners[6][2]);
		VA_SetElem3(vCache[15], corners[4][0], corners[4][1], corners[4][2]);
		// connectors
		VA_SetElem3(vCache[16], corners[0][0], corners[0][1], corners[0][2]);
		VA_SetElem3(vCache[17], corners[4][0], corners[4][1], corners[4][2]);
		VA_SetElem3(vCache[18], corners[1][0], corners[1][1], corners[1][2]);
		VA_SetElem3(vCache[19], corners[5][0], corners[5][1], corners[5][2]);
		VA_SetElem3(vCache[20], corners[2][0], corners[2][1], corners[2][2]);
		VA_SetElem3(vCache[21], corners[6][0], corners[6][1], corners[6][2]);
		VA_SetElem3(vCache[22], corners[3][0], corners[3][1], corners[3][2]);
		VA_SetElem3(vCache[23], corners[7][0], corners[7][1], corners[7][2]);
		glDrawArrays(GL_LINES, 0, 24);

		glDisable(GL_LINE_SMOOTH);
	}

	if (SELECTED_SHADOW_LIGHT)
	{
		glUniform4f(gen_color, SELECTED_SHADOW_LIGHT->color[0], SELECTED_SHADOW_LIGHT->color[1], SELECTED_SHADOW_LIGHT->color[2], 1.0);

		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glEnable(GL_LINE_SMOOTH);
		glLineWidth(3.0);
		VectorCopy(SELECTED_SHADOW_LIGHT->origin, tmpOrg);
		VectorCopy(SELECTED_SHADOW_LIGHT->radius, tmpRad);

		VectorSet(v[0], tmpOrg[0] - tmpRad[0], tmpOrg[1] - tmpRad[1], tmpOrg[2] - tmpRad[2]);
		VectorSet(v[1], tmpOrg[0] - tmpRad[0], tmpOrg[1] - tmpRad[1], tmpOrg[2] + tmpRad[2]);
		VectorSet(v[2], tmpOrg[0] - tmpRad[0], tmpOrg[1] + tmpRad[1], tmpOrg[2] - tmpRad[2]);
		VectorSet(v[3], tmpOrg[0] - tmpRad[0], tmpOrg[1] + tmpRad[1], tmpOrg[2] + tmpRad[2]);
		VectorSet(v[4], tmpOrg[0] + tmpRad[0], tmpOrg[1] - tmpRad[1], tmpOrg[2] - tmpRad[2]);
		VectorSet(v[5], tmpOrg[0] + tmpRad[0], tmpOrg[1] - tmpRad[1], tmpOrg[2] + tmpRad[2]);
		VectorSet(v[6], tmpOrg[0] + tmpRad[0], tmpOrg[1] + tmpRad[1], tmpOrg[2] - tmpRad[2]);
		VectorSet(v[7], tmpOrg[0] + tmpRad[0], tmpOrg[1] + tmpRad[1], tmpOrg[2] + tmpRad[2]);

		//front
		VA_SetElem3(vCache[0], v[0][0], v[0][1], v[0][2]);
		VA_SetElem3(vCache[1], v[1][0], v[1][1], v[1][2]);
		VA_SetElem3(vCache[2], v[3][0], v[3][1], v[3][2]);
		VA_SetElem3(vCache[3], v[0][0], v[0][1], v[0][2]);
		VA_SetElem3(vCache[4], v[2][0], v[2][1], v[2][2]);
		VA_SetElem3(vCache[5], v[3][0], v[3][1], v[3][2]);
		//right
		VA_SetElem3(vCache[6], v[0][0], v[0][1], v[0][2]);
		VA_SetElem3(vCache[7], v[4][0], v[4][1], v[4][2]);
		VA_SetElem3(vCache[8], v[5][0], v[5][1], v[5][2]);
		VA_SetElem3(vCache[9], v[0][0], v[0][1], v[0][2]);
		VA_SetElem3(vCache[10], v[1][0], v[1][1], v[1][2]);
		VA_SetElem3(vCache[11], v[5][0], v[5][1], v[5][2]);
		//bottom
		VA_SetElem3(vCache[12], v[0][0], v[0][1], v[0][2]);
		VA_SetElem3(vCache[13], v[4][0], v[4][1], v[4][2]);
		VA_SetElem3(vCache[14], v[6][0], v[6][1], v[6][2]);
		VA_SetElem3(vCache[15], v[0][0], v[0][1], v[0][2]);
		VA_SetElem3(vCache[16], v[2][0], v[2][1], v[2][2]);
		VA_SetElem3(vCache[17], v[6][0], v[6][1], v[6][2]);
		//top
		VA_SetElem3(vCache[18], v[1][0], v[1][1], v[1][2]);
		VA_SetElem3(vCache[19], v[5][0], v[5][1], v[5][2]);
		VA_SetElem3(vCache[20], v[7][0], v[7][1], v[7][2]);
		VA_SetElem3(vCache[21], v[1][0], v[1][1], v[1][2]);
		VA_SetElem3(vCache[22], v[3][0], v[3][1], v[3][2]);
		VA_SetElem3(vCache[23], v[7][0], v[7][1], v[7][2]);
		//left
		VA_SetElem3(vCache[24], v[2][0], v[2][1], v[2][2]);
		VA_SetElem3(vCache[25], v[3][0], v[3][1], v[3][2]);
		VA_SetElem3(vCache[26], v[7][0], v[7][1], v[7][2]);
		VA_SetElem3(vCache[27], v[2][0], v[2][1], v[2][2]);
		VA_SetElem3(vCache[28], v[6][0], v[6][1], v[6][2]);
		VA_SetElem3(vCache[29], v[7][0], v[7][1], v[7][2]);
		//back
		VA_SetElem3(vCache[30], v[4][0], v[4][1], v[4][2]);
		VA_SetElem3(vCache[31], v[5][0], v[5][1], v[5][2]);
		VA_SetElem3(vCache[32], v[7][0], v[7][1], v[7][2]);
		VA_SetElem3(vCache[33], v[4][0], v[4][1], v[4][2]);
		VA_SetElem3(vCache[34], v[6][0], v[6][1], v[6][2]);
		VA_SetElem3(vCache[35], v[7][0], v[7][1], v[7][2]);

		glDrawArrays(GL_TRIANGLES, 0, 36);

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glDisable(GL_LINE_SMOOTH);

		if (!flareEdit || !SELECTED_SHADOW_LIGHT->flare) // skip filled box in flare editing mode
		{
			// draw small light box
			VectorSet(v[0], tmpOrg[0] - 5, tmpOrg[1] - 5, tmpOrg[2] - 5);
			VectorSet(v[1], tmpOrg[0] - 5, tmpOrg[1] - 5, tmpOrg[2] + 5);
			VectorSet(v[2], tmpOrg[0] - 5, tmpOrg[1] + 5, tmpOrg[2] - 5);
			VectorSet(v[3], tmpOrg[0] - 5, tmpOrg[1] + 5, tmpOrg[2] + 5);
			VectorSet(v[4], tmpOrg[0] + 5, tmpOrg[1] - 5, tmpOrg[2] - 5);
			VectorSet(v[5], tmpOrg[0] + 5, tmpOrg[1] - 5, tmpOrg[2] + 5);
			VectorSet(v[6], tmpOrg[0] + 5, tmpOrg[1] + 5, tmpOrg[2] - 5);
			VectorSet(v[7], tmpOrg[0] + 5, tmpOrg[1] + 5, tmpOrg[2] + 5);

			//front
			VA_SetElem3(vCache[0], v[0][0], v[0][1], v[0][2]);
			VA_SetElem3(vCache[1], v[1][0], v[1][1], v[1][2]);
			VA_SetElem3(vCache[2], v[3][0], v[3][1], v[3][2]);
			VA_SetElem3(vCache[3], v[0][0], v[0][1], v[0][2]);
			VA_SetElem3(vCache[4], v[2][0], v[2][1], v[2][2]);
			VA_SetElem3(vCache[5], v[3][0], v[3][1], v[3][2]);
			//right
			VA_SetElem3(vCache[6], v[0][0], v[0][1], v[0][2]);
			VA_SetElem3(vCache[7], v[4][0], v[4][1], v[4][2]);
			VA_SetElem3(vCache[8], v[5][0], v[5][1], v[5][2]);
			VA_SetElem3(vCache[9], v[0][0], v[0][1], v[0][2]);
			VA_SetElem3(vCache[10], v[1][0], v[1][1], v[1][2]);
			VA_SetElem3(vCache[11], v[5][0], v[5][1], v[5][2]);
			//bottom
			VA_SetElem3(vCache[12], v[0][0], v[0][1], v[0][2]);
			VA_SetElem3(vCache[13], v[4][0], v[4][1], v[4][2]);
			VA_SetElem3(vCache[14], v[6][0], v[6][1], v[6][2]);
			VA_SetElem3(vCache[15], v[0][0], v[0][1], v[0][2]);
			VA_SetElem3(vCache[16], v[2][0], v[2][1], v[2][2]);
			VA_SetElem3(vCache[17], v[6][0], v[6][1], v[6][2]);
			//top
			VA_SetElem3(vCache[18], v[1][0], v[1][1], v[1][2]);
			VA_SetElem3(vCache[19], v[5][0], v[5][1], v[5][2]);
			VA_SetElem3(vCache[20], v[7][0], v[7][1], v[7][2]);
			VA_SetElem3(vCache[21], v[1][0], v[1][1], v[1][2]);
			VA_SetElem3(vCache[22], v[3][0], v[3][1], v[3][2]);
			VA_SetElem3(vCache[23], v[7][0], v[7][1], v[7][2]);
			//left
			VA_SetElem3(vCache[24], v[2][0], v[2][1], v[2][2]);
			VA_SetElem3(vCache[25], v[3][0], v[3][1], v[3][2]);
			VA_SetElem3(vCache[26], v[7][0], v[7][1], v[7][2]);
			VA_SetElem3(vCache[27], v[2][0], v[2][1], v[2][2]);
			VA_SetElem3(vCache[28], v[6][0], v[6][1], v[6][2]);
			VA_SetElem3(vCache[29], v[7][0], v[7][1], v[7][2]);
			//back
			VA_SetElem3(vCache[30], v[4][0], v[4][1], v[4][2]);
			VA_SetElem3(vCache[31], v[5][0], v[5][1], v[5][2]);
			VA_SetElem3(vCache[32], v[7][0], v[7][1], v[7][2]);
			VA_SetElem3(vCache[33], v[4][0], v[4][1], v[4][2]);
			VA_SetElem3(vCache[34], v[6][0], v[6][1], v[6][2]);
			VA_SetElem3(vCache[35], v[7][0], v[7][1], v[7][2]);

			glDrawArrays(GL_TRIANGLES, 0, 36);
		}
	}

	glDisableVertexAttribArray(ATT_POSITION);
	GL_BindNullProgram();
	GL_Enable(GL_CULL_FACE);
	GL_Enable(GL_BLEND);

	if (r_shadows->value)
		GL_Enable(GL_STENCIL_TEST);

	if (r_useLightScissors->value)
		GL_Enable(GL_SCISSOR_TEST);

	if (gl_state.depthBoundsTest && r_useDepthBounds->value)
		GL_Enable(GL_DEPTH_BOUNDS_TEST_EXT);

	R_LightFlareOutLine();
}


/**
 * 
 */
void CreateNormal(vec3_t dst, vec3_t xyz0, vec3_t xyz1, vec3_t xyz2)
{
	float x10, y10, z10;
	float x20, y20, z20;

	x10 = xyz1[0] - xyz0[0];
	x20 = xyz2[0] - xyz0[0];
	y10 = xyz1[1] - xyz0[1];
	y20 = xyz2[1] - xyz0[1];
	z10 = xyz1[2] - xyz0[2];
	z20 = xyz2[2] - xyz0[2];

	dst[0] = y10 * z20 - y20 * z10;
	dst[1] = z10 * x20 - z20 * x10;
	dst[2] = x10 * y20 - x20 * y10;

	VectorNormalize(dst);
}


/**
 * 
 */
void MakeFrustum4Light(worldShadowLight_t *light, bool ingame)
{
	vec3_t		v0, v1, v2, v3, v4;
	vec3_t		forward, right, up;
	vec3_t		angles, rspeed;

	if (!light->_cone)
		return;	// �����, ���� ������� �� ���� ������������ (�� ����� �������� _cone)

	if (ingame)
		VectorCopy(light->speed, rspeed);
	else
		VectorClear(rspeed);

	angles[0] = light->angles[0] + rspeed[0] * r_newrefdef.time * 1000;
	angles[1] = light->angles[1] + rspeed[1] * r_newrefdef.time * 1000;
	angles[2] = light->angles[2] + rspeed[2] * r_newrefdef.time * 1000;

	AngleVectors(angles, forward, right, up);

	VectorScale(right, light->_cone, right);
	VectorScale(up, light->_cone, up);
	VectorCopy(light->origin, v0);

	v1[0] = v0[0] + (forward[0] - right[0] - up[0]);
	v1[1] = v0[1] + (forward[1] - right[1] - up[1]);
	v1[2] = v0[2] + (forward[2] - right[2] - up[2]);

	v2[0] = v0[0] + (forward[0] - right[0] + up[0]);
	v2[1] = v0[1] + (forward[1] - right[1] + up[1]);
	v2[2] = v0[2] + (forward[2] - right[2] + up[2]);

	v3[0] = v0[0] + (forward[0] + right[0] + up[0]);
	v3[1] = v0[1] + (forward[1] + right[1] + up[1]);
	v3[2] = v0[2] + (forward[2] + right[2] + up[2]);

	v4[0] = v0[0] + (forward[0] + right[0] - up[0]);
	v4[1] = v0[1] + (forward[1] + right[1] - up[1]);
	v4[2] = v0[2] + (forward[2] + right[2] - up[2]);

	CreateNormal(light->frust[0].normal, v1, v2, v0);
	light->frust[0].dist = DotProduct(light->frust[0].normal, v0);

	CreateNormal(light->frust[1].normal, v2, v3, v0);
	light->frust[1].dist = DotProduct(light->frust[1].normal, v0);

	CreateNormal(light->frust[2].normal, v3, v4, v0);
	light->frust[2].dist = DotProduct(light->frust[2].normal, v0);

	CreateNormal(light->frust[3].normal, v4, v1, v0);
	light->frust[3].dist = DotProduct(light->frust[3].normal, v0);
}


/**
 * Adds a new world light into the map.
 * 
 * \fixme	This uses a ridiculous number of arguments. This is better done
 *			with a structure and passing that in by reference/pointer.
 */
worldShadowLight_t *R_AddNewWorldLight(vec3_t origin, vec3_t color, float radius[3], int style,
	int filter, vec3_t angles, vec3_t speed, bool isStatic,
	int isShadow, int isAmbient, float cone, bool ingame,
	int flare, vec3_t flareOrg, float flareSize, char target[MAX_QPATH],
	int flags, int fog, float fogDensity)
{
	worldShadowLight_t	*light;
	int					i;
	float				x, y;
	vec3_t				tmp;
	mat4_t				tmpMatrix, mvMatrix;

	light = (worldShadowLight_t*)malloc(sizeof(worldShadowLight_t));
	light->s_next = shadowLight_static;
	shadowLight_static = light;

	VectorClear(tmp);
	VectorClear(light->frust[0].normal);
	VectorClear(light->origin);
	VectorClear(light->startColor);
	VectorClear(light->angles);
	VectorClear(light->speed);
	VectorClear(light->radius);
	VectorClear(light->flareOrigin);
	memset(light->targetname, 0, sizeof(light->targetname));
	light->start_off = 0;

	for (i = 0; i < 8; i++)
		VectorClear(light->corners[i]);

	VectorCopy(origin, light->origin);
	VectorCopy(color, light->startColor);
	VectorCopy(angles, light->angles);
	VectorCopy(speed, light->speed);
	VectorCopy(radius, light->radius);
	VectorCopy(flareOrg, light->flareOrigin);

	if (light->radius[0] == light->radius[1] && light->radius[0] == light->radius[2])
		light->spherical = true;
	else
		light->spherical = false;

	light->startColor[0] = clamp(light->startColor[0], 0.0f, 1.0f);
	light->startColor[1] = clamp(light->startColor[1], 0.0f, 1.0f);
	light->startColor[2] = clamp(light->startColor[2], 0.0f, 1.0f);

	light->_cone = cone;
	light->isStatic = isStatic;
	light->isShadow = isShadow;
	light->isAmbient = isAmbient;
	light->isFog = fog;
	light->fogDensity = fogDensity;
	light->isNoWorldModel = 0;
	light->next = nullptr;
	light->style = style;
	light->filter = filter;
	light->flareSize = flareSize;
	light->flare = flare;
	light->vboId = light->iboId = light->iboNumIndices = 0;
	light->depthBounds[0] = 0.0;
	light->depthBounds[1] = 1.0;
	light->maxRad = 0;

	memcpy(light->targetname, target, sizeof(light->targetname));

	for (i = 0; i < 3; i++)
	{
		light->mins[i] = light->origin[i] - light->radius[i];
		light->maxs[i] = light->origin[i] + light->radius[i];
	}

	if (light->spherical)
		light->maxRad = light->radius[0];
	else
		light->maxRad = max(max(light->radius[0], light->radius[1]), light->radius[2]);

	for (i = 0; i < 8; i++)
	{
		tmp[0] = (i & 1) ? -light->radius[0] : light->radius[0];
		tmp[1] = (i & 2) ? -light->radius[1] : light->radius[1];
		tmp[2] = (i & 4) ? -light->radius[2] : light->radius[2];

		AnglesToMat3(light->angles, light->axis);
		Mat3_TransposeMultiplyVector(light->axis, tmp, light->corners[i]);
		VectorAdd(light->corners[i], light->origin, light->corners[i]);
	}

	MakeFrustum4Light(light, ingame);

	if (ingame) // new light
	{
		R_MarkLightLeaves(light);
		R_DrawBspModelVolumes(true, light);
		R_AddLightInteraction(light);
	}


#define START_OFF 1
	light->start_off = (flags & START_OFF);

	AnglesToMat3(light->angles, light->axis);
	Mat4_SetupTransform(tmpMatrix, light->axis, light->origin);
	Mat4_AffineInvert(tmpMatrix, mvMatrix);

	if (light->_cone)
	{
		light->isCone = 1;
		light->spherical = false;
	}
	else
		light->isCone = 0;

	light->hotSpot = 0.8f;
	light->coneExp = 1.f;
	light->distance = light->maxRad;

	// setup unit space conversion matrix
	if (light->isCone)
	{
		light->fov[0] = light->fov[1] = light->_cone * 0.5;

		x = tanf(light->fov[0] * 0.5f);
		y = tanf(light->fov[1] * 0.5f);

		tmpMatrix[0][0] = 1.f / light->distance;
		tmpMatrix[0][1] = 0.f;
		tmpMatrix[0][2] = 0.f;
		tmpMatrix[0][3] = 0.f;
		tmpMatrix[1][0] = 0.f;
		tmpMatrix[1][1] = 1.f / (light->distance * x);
		tmpMatrix[1][2] = 0.f;
		tmpMatrix[1][3] = 0.f;
		tmpMatrix[2][0] = 0.f;
		tmpMatrix[2][1] = 0.f;
		tmpMatrix[2][2] = 1.f / (light->distance * y);
		tmpMatrix[2][3] = 0.f;
		tmpMatrix[3][0] = 0.f;
		tmpMatrix[3][1] = 0.f;
		tmpMatrix[3][2] = 0.f;
		tmpMatrix[3][3] = 1.f;

		Mat4_Multiply(mvMatrix, tmpMatrix, light->spotMatrix);
	}

	if (light->isFog)
		tmpMatrix[0][0] = 0.f;
	else
		tmpMatrix[0][0] = 1.f / light->radius[0];

	tmpMatrix[0][1] = 0.f;
	tmpMatrix[0][2] = 0.f;
	tmpMatrix[0][3] = 0.f;
	tmpMatrix[1][0] = 0.f;

	if (light->isFog)
		tmpMatrix[1][1] = 0.f;
	else
		tmpMatrix[1][1] = 1.f / light->radius[1];

	tmpMatrix[1][2] = 0.f;
	tmpMatrix[1][3] = 0.f;
	tmpMatrix[2][0] = 0.f;
	tmpMatrix[2][1] = 0.f;
	tmpMatrix[2][2] = 1.f / light->radius[2];
	tmpMatrix[2][3] = 0.f;
	tmpMatrix[3][0] = 0.f;
	tmpMatrix[3][1] = 0.f;
	tmpMatrix[3][2] = 0.f;
	tmpMatrix[3][3] = 1.f;

	Mat4_Multiply(mvMatrix, tmpMatrix, light->attenMatrix);

	r_numWorldShadowLights++;
	return light;
}


/**
 * 
 */
void Load_BspLights()
{
	int addLight, style, numlights, flag;
	char* c, *token, key[256], *value, target[MAX_QPATH];
	float color[3], origin[3], radius[3], cone;

	if (!LOAD_MODEL)
	{
		Com_Printf("No map loaded.\n");
		return;
	}

	c = CM_EntityString();
	numlights = 0;

	for (;;)
	{
		token = Com_Parse(&c);
		if (!c) break;

		memset(target, 0, sizeof(target));
		VectorClear(origin);
		VectorSet(color, 1, 1, 1);
		VectorClear(radius);
		style = 0;
		cone = 0;
		flag = 0;

		addLight = false;

		for (;;)
		{
			token = Com_Parse(&c);
			if (token[0] == '}') break;

			strncpy(key, token, sizeof(key) - 1);

			value = Com_Parse(&c);
			if (!_stricmp(key, "classname"))
			{
				if (!_stricmp(value, "light"))
					addLight = true;
			}

			if (!_stricmp(key, "light"))
				radius[0] = atoi(value);
			if (!_stricmp(key, "origin"))
				sscanf(value, "%f %f %f", &origin[0], &origin[1], &origin[2]);
			if (!_stricmp(key, "_color"))
				sscanf(value, "%f %f %f", &color[0], &color[1], &color[2]);
			if (!_stricmp(key, "style"))
				style = atoi(value);
			if (!_stricmp(key, "_cone"))
				cone = atof(value);
			if (!_stricmp(key, "targetname"))
				Q_strncpyz(target, value, sizeof(target));
			if (!_stricmp(key, "spawnflags"))
				flag = atoi(value);
		}

		if (addLight && style > 0)
		{
			VectorSet(radius, radius[0], radius[0], radius[0]);
			R_AddNewWorldLight(origin, color, radius, style, 0, vec3_origin, vec3_origin, true, 1, 0, cone, false, 0, origin, 10.0, target, flag, 0, 0.0);
			numlights++;
		}
	}
	Com_Printf(S_COLOR_MAGENTA "Loaded " S_COLOR_GREEN "%i" S_COLOR_WHITE " bsp lights\n", numlights);
}

/**
 * \fixme	This could stand to use a bit of cleaning up.
 */
void Load_LightFile()
{
	int style = 0, numLights = 0, filter = 0, shadow = 0, ambient = 0, flare = 0, flag = 0, fog = 0;
	float cone = 0.0f, fSize = 0.0f, fogDensity = 0.0f;

	vec3_t radius = { 0.0f }, angles = { 0.0f }, speed = { 0.0f }, color = { 0.0f }, origin = { 0.0f }, lOrigin = { 0.0f }, fOrg = { 0.0f };

	char* token = nullptr;
	char* value = nullptr;

	char name[MAX_QPATH] = { '\0' }, path[MAX_QPATH] = { '\0' };
	char key[256] = { '\0' }, target[MAX_QPATH] = { '\0' };

	if (!r_worldmodel)
	{
		Com_Printf("No map loaded.\n");
		return;
	}

	FS_StripExtension(r_worldmodel->name, name, sizeof(name));
	Q_sprintf(path, sizeof(path), "%s.xplit", name);

	char* c = nullptr;
	FS_LoadFile(path, reinterpret_cast<void**>(&c));
	if (!c)
	{
		Load_BspLights();
		return;
	}

	Com_Printf("Loaded lights from " S_COLOR_GREEN "%s" S_COLOR_WHITE ".\n", path);

	for (;;)
	{
		token = Com_Parse(&c);

		if (!c) { break; }

		style = 0;
		filter = 0;
		shadow = 1;
		ambient = 0;
		cone = 0;
		fSize = 0;
		flare = 0;
		flag = 0;
		fog = 0;
		fogDensity = 0.0;

		memset(target, 0, sizeof(target));
		VectorClear(radius);
		VectorClear(angles);
		VectorClear(speed);
		VectorClear(origin);
		VectorClear(lOrigin);
		VectorClear(color);
		VectorClear(fOrg);

		for (;;)
		{
			token = Com_Parse(&c);
			if (token[0] == '}') { break; }

			strncpy(key, token, sizeof(key) - 1);

			value = Com_Parse(&c);

			if (!_stricmp(key, "radius"))
			{
				sscanf(value, "%f %f %f", &radius[0], &radius[1], &radius[2]);
			}
			else if (!_stricmp(key, "origin"))
			{
				sscanf(value, "%f %f %f", &origin[0], &origin[1], &origin[2]);
			}
			else if (!_stricmp(key, "color"))
			{
				sscanf(value, "%f %f %f", &color[0], &color[1], &color[2]);
			}
			else if (!_stricmp(key, "style"))
			{
				style = atoi(value);
			}
			else if (!_stricmp(key, "filter"))
			{
				filter = atoi(value);
			}
			else if (!_stricmp(key, "angles"))
			{
				sscanf(value, "%f %f %f", &angles[0], &angles[1], &angles[2]);
			}
			else if (!_stricmp(key, "speed"))
			{
				sscanf(value, "%f %f %f", &speed[0], &speed[1], &speed[2]);
			}
			else if (!_stricmp(key, "shadow"))
			{
				shadow = atoi(value);
			}
			else if (!_stricmp(key, "ambient"))
			{
				ambient = atoi(value);
			}
			else if (!_stricmp(key, "_cone"))
			{
				cone = atof(value);
			}
			else if (!_stricmp(key, "flare"))
			{
				flare = atoi(value);
			}
			else if (!_stricmp(key, "flareOrigin"))
			{
				sscanf(value, "%f %f %f", &fOrg[0], &fOrg[1], &fOrg[2]);
			}
			else if (!_stricmp(key, "flareSize"))
			{
				fSize = atoi(value);
			}
			else if (!_stricmp(key, "targetname"))
			{
				Q_strncpyz(target, value, sizeof(target));
			}
			else if (!_stricmp(key, "spawnflags"))
			{
				flag = atoi(value);
			}
			else if (!_stricmp(key, "fogLight"))
			{
				fog = atof(value);
			}
			else if (!_stricmp(key, "fogDensity"))
			{
				fogDensity = atof(value);
			}

		}

		R_AddNewWorldLight(origin, color, radius, style, filter, angles, speed, true, shadow, ambient, cone, false, flare, fOrg, fSize, target, flag, fog, fogDensity);
		numLights++;
	}
	Com_Printf(S_COLOR_MAGENTA "Load_LightFile:" S_COLOR_WHITE " add " S_COLOR_GREEN "%i" S_COLOR_WHITE " world lights\n", numLights);
}


/**
 * Marks nodes from the light, this is used for gross culling
 * during svbsp creation.
 */
bool R_MarkLightLeaves (worldShadowLight_t *light)
{
	int contents, leafnum, cluster;
	int		leafs[MAX_MAP_LEAFS];
	int		i, count;
	vec3_t	mins, maxs;
	byte	vis[MAX_MAP_LEAFS / 8];

	contents = CM_PointContents(light->origin, 0);

	if (contents & CONTENTS_SOLID)
		Com_DPrintf("Out of BSP, rejected light at %f %f %f\n", light->origin[0], light->origin[1], light->origin[2]);

	leafnum = CM_PointLeafnum (light->origin);
	cluster = CM_LeafCluster (leafnum);
	light->area = CM_LeafArea (leafnum);

	if (!light->area)
	{
		Com_DPrintf ("Out of BSP, rejected light at %f %f %f\n", light->origin[0], light->origin[1], light->origin[2]);
		return false;
	}

	// build vis-data
	memcpy (&light->vis, CM_ClusterPVS (cluster), (((CM_NumClusters () + 31) >> 5) << 2));

	for (i = 0; i < 3; i++)
	{
		mins[i] = light->origin[i] - light->radius[i];
		maxs[i] = light->origin[i] + light->radius[i];
	}

	count = CM_BoxLeafnums (mins, maxs, leafs, r_worldmodel->numLeafs, nullptr);
	if (count < 1)
		Com_Error (ERR_FATAL, "R_MarkLightLeaves: count < 1");

	// convert leafs to clusters
	for (i = 0; i < count; i++)
		leafs[i] = CM_LeafCluster (leafs[i]);

	memset (&vis, 0, (((r_worldmodel->numLeafs + 31) >> 5) << 2));
	for (i = 0; i < count; i++)
		vis[leafs[i] >> 3] |= (1 << (leafs[i] & 7));

	for (i = 0; i < ((r_worldmodel->numLeafs + 31) >> 5); i++)
		((long *)light->vis)[i] &= ((long *)vis)[i];

	return true;
}


/**
 * 
 */
bool InLightVISEntity()
{
	int		leafs[MAX_MAP_LEAFS];
	int		i, count;
	int		longs;
	vec3_t	mins, maxs;

	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		return true;

	if (!r_worldmodel)
		return false;

	if (CURRENT_ENTITY->angles[0] || CURRENT_ENTITY->angles[1] || CURRENT_ENTITY->angles[2])
	{
		for (i = 0; i < 3; i++)
		{
			mins[i] = CURRENT_ENTITY->origin[i] - CURRENT_MODEL->radius;
			maxs[i] = CURRENT_ENTITY->origin[i] + CURRENT_MODEL->radius;
		}
	}
	else
	{
		VectorAdd(CURRENT_ENTITY->origin, CURRENT_ENTITY->model->maxs, maxs);
		VectorAdd(CURRENT_ENTITY->origin, CURRENT_ENTITY->model->mins, mins);
	}

	count = CM_BoxLeafnums(mins, maxs, leafs, r_worldmodel->numLeafs, nullptr);
	if (count < 1)
		Com_Error(ERR_FATAL, "InLightVISEntity: count < 1");

	longs = (CM_NumClusters() + 31) >> 5;

	// convert leafs to clusters
	for (i = 0; i < count; i++)
		leafs[i] = CM_LeafCluster(leafs[i]);

	memset(&CURRENT_ENTITY->vis, 0, (((r_worldmodel->numLeafs + 31) >> 5) << 2));
	for (i = 0; i < count; i++)
		CURRENT_ENTITY->vis[leafs[i] >> 3] |= (1 << (leafs[i] & 7));

	return R_HasSharedLeafs(currentShadowLight->vis, CURRENT_ENTITY->vis);
}


/**
 * 
 */
void R_AddLightInteraction(worldShadowLight_t *light)
{
	r_lightTimestamp++;
	light->numInteractionSurfs = 0; // set to zero for ingame editor
	
	R_MarkLightCasting(r_worldmodel->nodes, true, light);
	qsort(light->interaction, light->numInteractionSurfs, sizeof(msurface_t*), (int(*)(const void *, const void *))lightSurfSort);
}


/**
 * 
 */
void R_CalcStaticLightInteraction()
{
	worldShadowLight_t *light;

	for (light = shadowLight_static; light; light = light->s_next)
	{
		if (!R_MarkLightLeaves(light)) // out of bsp or no area data
			continue;

		R_DrawBspModelVolumes(true, light);
		R_AddLightInteraction(light);
	}

	Com_Printf(S_COLOR_MAGENTA "R_CalcStaticLightInteraction: " S_COLOR_GREEN "%i" S_COLOR_WHITE " lights\n", r_numWorldShadowLights);
}


/**
 * 
 */
void DeleteShadowVertexBuffers ()
{
	worldShadowLight_t *light;

	for (light = shadowLight_static; light; light = light->s_next)
	{

		glDeleteBuffers (1, &light->vboId);
		glDeleteBuffers (1, &light->iboId);
	}

	numPreCachedLights = 0;
}


/**
 * 
 */
void R_ClearWorldLights()
{
	worldShadowLight_t *light, *next;

	if (shadowLight_static)
	{
		for (light = shadowLight_static; light; light = next)
		{
			next = light->s_next;
			free(light);
		}
		shadowLight_static = nullptr;
	}

	memset(shadowLightsBlock, 0, sizeof(worldShadowLight_t)* MAX_WORLD_SHADOW_LIGHTS);

	r_numWorldShadowLights = 0;
}


/**
 * Loads the current matrix with a tranformation used for light filters
 */
void R_CalcCubeMapMatrix(bool model)
{
	float   a, b, c;
	mat4_t  m;

	a = currentShadowLight->angles[2] + (currentShadowLight->speed[2] * r_newrefdef.time * 1000);
	b = currentShadowLight->angles[0] + (currentShadowLight->speed[0] * r_newrefdef.time * 1000);
	c = currentShadowLight->angles[1] + (currentShadowLight->speed[1] * r_newrefdef.time * 1000);

	Mat4_Identity(m);

	if (a) Mat4_Rotate(m, -a, 1.f, 0.f, 0.f);
	if (b) Mat4_Rotate(m, -b, 0.f, 1.f, 0.f);
	if (c) Mat4_Rotate(m, -c, 0.f, 0.f, 1.f);

	if (model)
	{
		if (CURRENT_ENTITY->angles[1])
			Mat4_Rotate(m, CURRENT_ENTITY->angles[1], 0, 0, 1);
		if (CURRENT_ENTITY->angles[0])
			Mat4_Rotate(m, CURRENT_ENTITY->angles[0], 0, 1, 0);
		if (CURRENT_ENTITY->angles[2])
			Mat4_Rotate(m, CURRENT_ENTITY->angles[2], 1, 0, 0);
	}

	Mat4_Translate(m, -currentShadowLight->origin[0], -currentShadowLight->origin[1], -currentShadowLight->origin[2]);
	Mat4_Copy(m, currentShadowLight->cubeMapMatrix);
}


/**
 * R_ClipLightPlane
 */
void VectorLerp (const vec3_t from, const vec3_t to, float frac, vec3_t out)
{
	if (frac <= 0.0f)
	{
		out[0] = from[0];
		out[1] = from[1];
		out[2] = from[2];
		return;
	}

	if (frac >= 1.0f)
	{
		out[0] = to[0];
		out[1] = to[1];
		out[2] = to[2];
		return;
	}

	out[0] = from[0] + (to[0] - from[0]) * frac;
	out[1] = from[1] + (to[1] - from[1]) * frac;
	out[2] = from[2] + (to[2] - from[2]) * frac;
}


/**
 * PointOnPlaneSide
 */
int PointOnPlaneSide(const vec3_t point, struct cplane_s *plane)
{
	float dist = 0.0f;

	if (plane->type < 3)
		dist = point[plane->type] - plane->dist;
	else
		dist = DotProduct(point, plane->normal) - plane->dist;

	if (dist > ON_EPSILON)
		return SIDE_FRONT;
	if (dist < -ON_EPSILON)
		return SIDE_BACK;

	return SIDE_ON;
}


/**
 * 
 */
static void R_ClipLightPlane (const mat4_t mvpMatrix, vec3_t mins, vec3_t maxs, int numPoints, vec3_t *points, int stage)
{
	vec3_t	clipped[MAX_LIGHT_PLANE_VERTICES];
	float	dists[MAX_LIGHT_PLANE_VERTICES];
	int		sides[MAX_LIGHT_PLANE_VERTICES];
	bool	front, back;
	vec4_t	in, out;
	vec3_t	point;
	float	scale;
	int		i, numClipped;

	if (stage == 5) // fully clipped, so add points in normalized device coordinates
	{
		for (i = 0; i < numPoints; i++)
		{
			in[0] = points[i][0];
			in[1] = points[i][1];
			in[2] = points[i][2];
			in[3] = 1.0f;

			Mat4_MultiplyVector (mvpMatrix, in, out);

			scale = 1.0f;

			point[0] = out[0] * scale;
			point[1] = out[1] * scale;
			point[2] = out[2] * scale;

			AddPointToBounds (point, mins, maxs);
		}

		return;
	}

	if (numPoints > MAX_LIGHT_PLANE_VERTICES - 2)
		Com_Error (false, "R_ClipLightPlane: MAX_LIGHT_PLANE_VERTICES hit");

	// determine sides for each point
	front = false;
	back = false;

	for (i = 0; i < numPoints; i++)
	{
		dists[i] = DotProduct (points[i], frustum[stage].normal) - frustum[stage].dist;

		if (dists[i] > ON_EPSILON)
		{
			sides[i] = SIDE_FRONT;
			front = true;
			continue;
		}

		if (dists[i] < -ON_EPSILON)
		{
			sides[i] = SIDE_BACK;
			back = true;
			continue;
		}

		sides[i] = SIDE_ON;
	}

	if (!front)
		return;		// not clipped

	if (!back)
	{
		R_ClipLightPlane (mvpMatrix, mins, maxs, numPoints, points, stage + 1);
		return;
	}

	// handle wraparound case
	VectorCopy (points[0], points[i]);

	dists[i] = dists[0];
	sides[i] = sides[0];

	// clip it
	numClipped = 0;

	for (i = 0; i < numPoints; i++)
	{
		if (sides[i] == SIDE_ON)
		{
			VectorCopy (points[i], clipped[numClipped]);
			numClipped++;
			continue;
		}

		if (sides[i] == SIDE_FRONT)
		{
			VectorCopy (points[i], clipped[numClipped]);
			numClipped++;
		}

		if (sides[i + 1] == SIDE_ON || sides[i + 1] == sides[i])
			continue;

		if (dists[i] == dists[i + 1])
		{
			VectorCopy (points[i], clipped[numClipped]);
			numClipped++;
		}
		else
			VectorLerp (points[i], points[i + 1], dists[i] / (dists[i] - dists[i + 1]), clipped[numClipped++]);
	}

	// continue
	R_ClipLightPlane (mvpMatrix, mins, maxs, numClipped, clipped, stage + 1);
}


/**
 * 
 */
void R_SetViewLightScreenBounds()
{
	int			i, scissor[4],
		cornerIndices[6][4] = { { 3, 2, 6, 7 }, { 0, 1, 5, 4 }, { 2, 3, 1, 0 }, { 4, 5, 7, 6 }, { 1, 3, 7, 5 }, { 2, 0, 4, 6 } };
	vec3_t		mins = { Q_INFINITY, Q_INFINITY, Q_INFINITY },
		maxs = { -Q_INFINITY, -Q_INFINITY, -Q_INFINITY },
		points[5];
	float		depth[2];

	currentShadowLight->scissor[0] = r_newrefdef.viewport[0];
	currentShadowLight->scissor[1] = r_newrefdef.viewport[1];
	currentShadowLight->scissor[2] = r_newrefdef.viewport[2];
	currentShadowLight->scissor[3] = r_newrefdef.viewport[3];

	currentShadowLight->depthBounds[0] = 0.0f;
	currentShadowLight->depthBounds[1] = 1.0f;

	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		return;

	if (r_useLightScissors->value || (gl_state.depthBoundsTest && r_useDepthBounds->value))
	{
		// copy the corner points of each plane and clip to the frustum
		for (i = 0; i < 6; i++)
		{
			VectorCopy(currentShadowLight->corners[cornerIndices[i][0]], points[0]);
			VectorCopy(currentShadowLight->corners[cornerIndices[i][1]], points[1]);
			VectorCopy(currentShadowLight->corners[cornerIndices[i][2]], points[2]);
			VectorCopy(currentShadowLight->corners[cornerIndices[i][3]], points[3]);

			R_ClipLightPlane(r_newrefdef.modelViewProjectionMatrixTranspose, mins, maxs, 4, points, 0);
		}

		// check if any corner point is not in front of the near plane
		for (i = 0; i < 8; i++)
		{
			if (PointOnPlaneSide(currentShadowLight->corners[i], &frustum[4]) != SIDE_FRONT)
			{
				mins[2] = -1.0f;
				break;
			}
		}

		// transform into screen space
		mins[0] = (0.5f + 0.5f * mins[0]) * r_newrefdef.viewport[2] + r_newrefdef.viewport[0];
		mins[1] = (0.5f + 0.5f * mins[1]) * r_newrefdef.viewport[3] + r_newrefdef.viewport[1];
		mins[2] = (0.5f + 0.5f * mins[2]);
		maxs[0] = (0.5f + 0.5f * maxs[0]) * r_newrefdef.viewport[2] + r_newrefdef.viewport[0];
		maxs[1] = (0.5f + 0.5f * maxs[1]) * r_newrefdef.viewport[3] + r_newrefdef.viewport[1];
		maxs[2] = (0.5f + 0.5f * maxs[2]);

	}

	// set the scissor rectangle
	if (r_useLightScissors->value)
	{
		scissor[0] = max((long)(floor(mins[0])), r_newrefdef.viewport[0]);
		scissor[1] = max((long)(floor(mins[1])), r_newrefdef.viewport[1]);
		scissor[2] = min((long)(ceil(maxs[0])), r_newrefdef.viewport[0] + r_newrefdef.viewport[2]);
		scissor[3] = min((long)(ceil(maxs[1])), r_newrefdef.viewport[1] + r_newrefdef.viewport[3]);

		if (scissor[0] > scissor[2] || scissor[1] > scissor[3])
		{
			currentShadowLight->scissor[0] = r_newrefdef.viewport[0];
			currentShadowLight->scissor[1] = r_newrefdef.viewport[1];
			currentShadowLight->scissor[2] = r_newrefdef.viewport[2];
			currentShadowLight->scissor[3] = r_newrefdef.viewport[3];
		}
		else
		{
			currentShadowLight->scissor[0] = scissor[0];
			currentShadowLight->scissor[1] = scissor[1];
			currentShadowLight->scissor[2] = scissor[2] - scissor[0];
			currentShadowLight->scissor[3] = scissor[3] - scissor[1];
		}
	}

	// set the depth bounds
	if (r_useDepthBounds->value)
	{
		depth[0] = max(mins[2], 0.0f);
		depth[1] = min(maxs[2], 1.0f);

		if (depth[0] > depth[1])
		{
			currentShadowLight->depthBounds[0] = 0.0f;
			currentShadowLight->depthBounds[1] = 1.0f;
		}
		else
		{
			currentShadowLight->depthBounds[0] = depth[0];
			currentShadowLight->depthBounds[1] = depth[1];
		}
	}
}


/**
 * 
 */
void R_DrawLightFlare()
{
	float		dist, dist2, scale;
	vec3_t		v, tmp;
	vec3_t		vert_array[MAX_FLARES_VERTEX];
	vec2_t		tex_array[MAX_FLARES_VERTEX];
	vec4_t		color_array[MAX_FLARES_VERTEX];

	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		return;

	if (!currentShadowLight->flare)
		return;

	if (currentShadowLight->isNoWorldModel)
		return;

	if (currentShadowLight->isAmbient)
		return;

	if (!r_drawFlares->value)
		return;

	if (gl_state.depthBoundsTest && r_useDepthBounds->value)
		GL_Disable(GL_DEPTH_BOUNDS_TEST_EXT);

	if (r_useLightScissors->value)
		GL_Disable(GL_SCISSOR_TEST);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, getVbo().ibo_quadTris);
	glEnableVertexAttribArray(ATT_POSITION);
	glEnableVertexAttribArray(ATT_TEX0);
	glEnableVertexAttribArray(ATT_COLOR);

	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, vert_array);
	glVertexAttribPointer(ATT_TEX0, 2, GL_FLOAT, false, 0, tex_array);
	glVertexAttribPointer(ATT_COLOR, 4, GL_FLOAT, false, 0, color_array);

	GL_BindProgram(particlesProgram, 0);

	GL_MBind(GL_TEXTURE0, r_flare->texnum);
	GL_MBindRect(GL_TEXTURE1, depthMap->texnum);

	glUniform2f(particle_depthParams, r_newrefdef.depthParms[0], r_newrefdef.depthParms[1]);
	glUniform2f(particle_mask, 1.0, 0.0);
	glUniform1f(particle_colorModulate, 1.0);
	glUniform1f(particle_thickness, 10.0 * 1.5);
	glUniformMatrix4fv(particle_mvp, 1, false, (const float *)r_newrefdef.modelViewProjectionMatrix);
	glUniformMatrix4fv(particle_mv, 1, false, (const float *)r_newrefdef.modelViewMatrix);

	// Color Fade
	VectorSubtract(currentShadowLight->flareOrigin, r_origin, v);
	dist2 = VectorLength(v);
	dist = dist2 * 0.1; // 10.0 * 0.01

	scale = ((1024 - dist2) / 1024) * 0.5;

	VectorScale(currentShadowLight->color, scale, tmp);

	VectorScaleAndAdd(currentShadowLight->flareOrigin, vup, vert_array[0], -1 - dist);
	VectorScaleAndAdd(vert_array[0], vright, vert_array[0], 1 + dist);
	VA_SetElem2(tex_array[0], 0, 1);
	VA_SetElem4(color_array[0], tmp[0], tmp[1], tmp[2], 1);

	VectorScaleAndAdd(currentShadowLight->flareOrigin, vup, vert_array[1], -1 - dist);
	VectorScaleAndAdd(vert_array[1], vright, vert_array[1], -1 - dist);
	VA_SetElem2(tex_array[1], 0, 0);
	VA_SetElem4(color_array[1], tmp[0], tmp[1], tmp[2], 1);

	VectorScaleAndAdd(currentShadowLight->flareOrigin, vup, vert_array[2], 1 + dist);
	VectorScaleAndAdd(vert_array[2], vright, vert_array[2], -1 - dist);
	VA_SetElem2(tex_array[2], 1, 0);
	VA_SetElem4(color_array[2], tmp[0], tmp[1], tmp[2], 1);

	VectorScaleAndAdd(currentShadowLight->flareOrigin, vup, vert_array[3], 1 + dist);
	VectorScaleAndAdd(vert_array[3], vright, vert_array[3], 1 + dist);
	VA_SetElem2(tex_array[3], 1, 1);
	VA_SetElem4(color_array[3], tmp[0], tmp[1], tmp[2], 1);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, nullptr);

	GL_BindNullProgram();

	if (gl_state.depthBoundsTest && r_useDepthBounds->value)
		GL_Enable(GL_DEPTH_BOUNDS_TEST_EXT);

	if (r_useLightScissors->value)
		GL_Enable(GL_SCISSOR_TEST);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(ATT_POSITION);
	glDisableVertexAttribArray(ATT_TEX0);
	glDisableVertexAttribArray(ATT_COLOR);
}


/**
 * Flare editing highlights.
 */
void R_LightFlareOutLine()
{
	vec3_t		v[8], tmpOrg;

	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		return;

	if (!currentShadowLight->flare)
		return;

	if (!flareEdit)
		return;

	if (gl_state.depthBoundsTest && r_useDepthBounds->value)
		GL_Disable(GL_DEPTH_BOUNDS_TEST_EXT);


	GL_Disable(GL_SCISSOR_TEST);
	GL_Disable(GL_STENCIL_TEST);
	GL_Disable(GL_CULL_FACE);

	glEnableVertexAttribArray(ATT_POSITION);
	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, vCache);

	// setup program
	GL_BindProgram(genericProgram, 0);
	glUniform1i(gen_attribColors, 0);
	glUniform1i(gen_sky, 0);
	glUniform1i(gen_3d, 1);
	glUniform4f(gen_color, currentShadowLight->color[0], currentShadowLight->color[1], currentShadowLight->color[2], 1.0);
	glUniformMatrix4fv(gen_mvp, 1, false, (const float *)r_newrefdef.modelViewProjectionMatrix);

	// draw light to flare connector
	glEnable(GL_LINE_SMOOTH);
	glLineWidth(3.0);

	VA_SetElem3(vCache[0], currentShadowLight->origin[0], currentShadowLight->origin[1], currentShadowLight->origin[2]);
	VA_SetElem3(vCache[1], currentShadowLight->flareOrigin[0], currentShadowLight->flareOrigin[1], currentShadowLight->flareOrigin[2]);

	glDrawArrays(GL_LINES, 0, 2);

	glDisable(GL_LINE_SMOOTH);

	// draw center of flare
	VectorCopy(currentShadowLight->flareOrigin, tmpOrg);
	VectorSet(v[0], tmpOrg[0] - 1, tmpOrg[1] - 1, tmpOrg[2] - 1);
	VectorSet(v[1], tmpOrg[0] - 1, tmpOrg[1] - 1, tmpOrg[2] + 1);
	VectorSet(v[2], tmpOrg[0] - 1, tmpOrg[1] + 1, tmpOrg[2] - 1);
	VectorSet(v[3], tmpOrg[0] - 1, tmpOrg[1] + 1, tmpOrg[2] + 1);
	VectorSet(v[4], tmpOrg[0] + 1, tmpOrg[1] - 1, tmpOrg[2] - 1);
	VectorSet(v[5], tmpOrg[0] + 1, tmpOrg[1] - 1, tmpOrg[2] + 1);
	VectorSet(v[6], tmpOrg[0] + 1, tmpOrg[1] + 1, tmpOrg[2] - 1);
	VectorSet(v[7], tmpOrg[0] + 1, tmpOrg[1] + 1, tmpOrg[2] + 1);


	//front
	VA_SetElem3(vCache[0], v[0][0], v[0][1], v[0][2]);
	VA_SetElem3(vCache[1], v[1][0], v[1][1], v[1][2]);
	VA_SetElem3(vCache[2], v[3][0], v[3][1], v[3][2]);
	VA_SetElem3(vCache[3], v[0][0], v[0][1], v[0][2]);
	VA_SetElem3(vCache[4], v[2][0], v[2][1], v[2][2]);
	VA_SetElem3(vCache[5], v[3][0], v[3][1], v[3][2]);
	//right
	VA_SetElem3(vCache[6], v[0][0], v[0][1], v[0][2]);
	VA_SetElem3(vCache[7], v[4][0], v[4][1], v[4][2]);
	VA_SetElem3(vCache[8], v[5][0], v[5][1], v[5][2]);
	VA_SetElem3(vCache[9], v[0][0], v[0][1], v[0][2]);
	VA_SetElem3(vCache[10], v[1][0], v[1][1], v[1][2]);
	VA_SetElem3(vCache[11], v[5][0], v[5][1], v[5][2]);
	//bottom
	VA_SetElem3(vCache[12], v[0][0], v[0][1], v[0][2]);
	VA_SetElem3(vCache[13], v[4][0], v[4][1], v[4][2]);
	VA_SetElem3(vCache[14], v[6][0], v[6][1], v[6][2]);
	VA_SetElem3(vCache[15], v[0][0], v[0][1], v[0][2]);
	VA_SetElem3(vCache[16], v[2][0], v[2][1], v[2][2]);
	VA_SetElem3(vCache[17], v[6][0], v[6][1], v[6][2]);
	//top
	VA_SetElem3(vCache[18], v[1][0], v[1][1], v[1][2]);
	VA_SetElem3(vCache[19], v[5][0], v[5][1], v[5][2]);
	VA_SetElem3(vCache[20], v[7][0], v[7][1], v[7][2]);
	VA_SetElem3(vCache[21], v[1][0], v[1][1], v[1][2]);
	VA_SetElem3(vCache[22], v[3][0], v[3][1], v[3][2]);
	VA_SetElem3(vCache[23], v[7][0], v[7][1], v[7][2]);
	//left
	VA_SetElem3(vCache[24], v[2][0], v[2][1], v[2][2]);
	VA_SetElem3(vCache[25], v[3][0], v[3][1], v[3][2]);
	VA_SetElem3(vCache[26], v[7][0], v[7][1], v[7][2]);
	VA_SetElem3(vCache[27], v[2][0], v[2][1], v[2][2]);
	VA_SetElem3(vCache[28], v[6][0], v[6][1], v[6][2]);
	VA_SetElem3(vCache[29], v[7][0], v[7][1], v[7][2]);
	//back
	VA_SetElem3(vCache[30], v[4][0], v[4][1], v[4][2]);
	VA_SetElem3(vCache[31], v[5][0], v[5][1], v[5][2]);
	VA_SetElem3(vCache[32], v[7][0], v[7][1], v[7][2]);
	VA_SetElem3(vCache[33], v[4][0], v[4][1], v[4][2]);
	VA_SetElem3(vCache[34], v[6][0], v[6][1], v[6][2]);
	VA_SetElem3(vCache[35], v[7][0], v[7][1], v[7][2]);

	glDrawArrays(GL_TRIANGLES, 0, 36);

	GL_BindNullProgram();

	if (r_useLightScissors->value)
		GL_Enable(GL_SCISSOR_TEST);
	if (gl_state.depthBoundsTest && r_useDepthBounds->value)
		GL_Enable(GL_DEPTH_BOUNDS_TEST_EXT);

	GL_Enable(GL_STENCIL_TEST);
	GL_Enable(GL_CULL_FACE);
	glDisableVertexAttribArray(ATT_POSITION);
}


/**
 * 
 */
void R_DrawLightBounds()
{
	vec3_t v[8];
	vec3_t tmpOrg;

	if (!r_debugLights->value)
		return;

	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		return;

	if (!currentShadowLight->isStatic)
		return;

	GL_Disable(GL_SCISSOR_TEST);

	if (gl_state.depthBoundsTest && r_useDepthBounds->value)
		GL_Disable(GL_DEPTH_BOUNDS_TEST_EXT);

	GL_Disable(GL_BLEND);
	GL_Disable(GL_STENCIL_TEST);
	GL_Disable(GL_CULL_FACE);

	if (r_debugLights->value == 2)
		GL_Disable(GL_DEPTH_TEST);

	// setup program
	GL_BindProgram(genericProgram, 0);
	glUniform1i(gen_attribColors, 0);
	glUniform1i(gen_sky, 0);
	glUniform1i(gen_3d, 1);
	glUniform4f(gen_color, currentShadowLight->color[0], currentShadowLight->color[1], currentShadowLight->color[2], 1.0);
	glUniformMatrix4fv(gen_mvp, 1, false, (const float *)r_newrefdef.modelViewProjectionMatrix);

	glEnableVertexAttribArray(ATT_POSITION);
	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, vCache);

	VectorCopy(currentShadowLight->origin, tmpOrg);

	VectorSet(v[0], tmpOrg[0] - 5, tmpOrg[1] - 5, tmpOrg[2] - 5);
	VectorSet(v[1], tmpOrg[0] - 5, tmpOrg[1] - 5, tmpOrg[2] + 5);
	VectorSet(v[2], tmpOrg[0] - 5, tmpOrg[1] + 5, tmpOrg[2] - 5);
	VectorSet(v[3], tmpOrg[0] - 5, tmpOrg[1] + 5, tmpOrg[2] + 5);
	VectorSet(v[4], tmpOrg[0] + 5, tmpOrg[1] - 5, tmpOrg[2] - 5);
	VectorSet(v[5], tmpOrg[0] + 5, tmpOrg[1] - 5, tmpOrg[2] + 5);
	VectorSet(v[6], tmpOrg[0] + 5, tmpOrg[1] + 5, tmpOrg[2] - 5);
	VectorSet(v[7], tmpOrg[0] + 5, tmpOrg[1] + 5, tmpOrg[2] + 5);

	//front
	VA_SetElem3(vCache[0], v[0][0], v[0][1], v[0][2]);
	VA_SetElem3(vCache[1], v[1][0], v[1][1], v[1][2]);
	VA_SetElem3(vCache[2], v[3][0], v[3][1], v[3][2]);
	VA_SetElem3(vCache[3], v[0][0], v[0][1], v[0][2]);
	VA_SetElem3(vCache[4], v[2][0], v[2][1], v[2][2]);
	VA_SetElem3(vCache[5], v[3][0], v[3][1], v[3][2]);
	//right
	VA_SetElem3(vCache[6], v[0][0], v[0][1], v[0][2]);
	VA_SetElem3(vCache[7], v[4][0], v[4][1], v[4][2]);
	VA_SetElem3(vCache[8], v[5][0], v[5][1], v[5][2]);
	VA_SetElem3(vCache[9], v[0][0], v[0][1], v[0][2]);
	VA_SetElem3(vCache[10], v[1][0], v[1][1], v[1][2]);
	VA_SetElem3(vCache[11], v[5][0], v[5][1], v[5][2]);
	//bottom
	VA_SetElem3(vCache[12], v[0][0], v[0][1], v[0][2]);
	VA_SetElem3(vCache[13], v[4][0], v[4][1], v[4][2]);
	VA_SetElem3(vCache[14], v[6][0], v[6][1], v[6][2]);
	VA_SetElem3(vCache[15], v[0][0], v[0][1], v[0][2]);
	VA_SetElem3(vCache[16], v[2][0], v[2][1], v[2][2]);
	VA_SetElem3(vCache[17], v[6][0], v[6][1], v[6][2]);
	//top
	VA_SetElem3(vCache[18], v[1][0], v[1][1], v[1][2]);
	VA_SetElem3(vCache[19], v[5][0], v[5][1], v[5][2]);
	VA_SetElem3(vCache[20], v[7][0], v[7][1], v[7][2]);
	VA_SetElem3(vCache[21], v[1][0], v[1][1], v[1][2]);
	VA_SetElem3(vCache[22], v[3][0], v[3][1], v[3][2]);
	VA_SetElem3(vCache[23], v[7][0], v[7][1], v[7][2]);
	//left
	VA_SetElem3(vCache[24], v[2][0], v[2][1], v[2][2]);
	VA_SetElem3(vCache[25], v[3][0], v[3][1], v[3][2]);
	VA_SetElem3(vCache[26], v[7][0], v[7][1], v[7][2]);
	VA_SetElem3(vCache[27], v[2][0], v[2][1], v[2][2]);
	VA_SetElem3(vCache[28], v[6][0], v[6][1], v[6][2]);
	VA_SetElem3(vCache[29], v[7][0], v[7][1], v[7][2]);
	//back
	VA_SetElem3(vCache[30], v[4][0], v[4][1], v[4][2]);
	VA_SetElem3(vCache[31], v[5][0], v[5][1], v[5][2]);
	VA_SetElem3(vCache[32], v[7][0], v[7][1], v[7][2]);
	VA_SetElem3(vCache[33], v[4][0], v[4][1], v[4][2]);
	VA_SetElem3(vCache[34], v[6][0], v[6][1], v[6][2]);
	VA_SetElem3(vCache[35], v[7][0], v[7][1], v[7][2]);

	glDrawArrays(GL_TRIANGLES, 0, 36);

	glDisableVertexAttribArray(ATT_POSITION);
	GL_BindNullProgram();
	GL_Enable(GL_CULL_FACE);
	GL_Enable(GL_BLEND);

	if (r_debugLights->value == 2)
		GL_Enable(GL_DEPTH_TEST);

	if (r_shadows->value)
		GL_Enable(GL_STENCIL_TEST);

	if (r_useLightScissors->value)
		GL_Enable(GL_SCISSOR_TEST);

	if (gl_state.depthBoundsTest && r_useDepthBounds->value)
		GL_Enable(GL_DEPTH_BOUNDS_TEST_EXT);
}
