#include "r_util.h"


// ===============================================================================
// = MODULE VARIABLES
// ===============================================================================
int REGISTRATION_SEQUENCE = 0;


// ===============================================================================
// = PUBLIC FUNCTIONS
// ===============================================================================

/**
 * 
 */
int R_RegistrationSequence()
{
	return REGISTRATION_SEQUENCE;
}


/**
 * 
 */
void R_IncrementRegistrationSequence()
{
	++REGISTRATION_SEQUENCE;
}


/**
 * 
 */
void R_SetRegistrationSequence(int _value)
{
	REGISTRATION_SEQUENCE = _value;
}


/**
 * 
 */
void R_ModelBounds(Model* model, vec3_t mins, vec3_t maxs)
{
	if (model)
	{
		VectorCopy(model->mins, mins);	/// \fixme Replace with an actual function.
		VectorCopy(model->maxs, maxs);	/// \fixme Replace with an actual function.
	}
}


/**
 * 
 */
void R_ModelRadius(Model* model, vec3_t rad)
{
	if (model) rad[0] = model->radius;
}


/**
 * 
 */
void R_ModelCenter(Model* model, vec3_t center)
{
	if (model) VectorCopy(model->center, center); /// \fixme Replace with an actual function.
}


/**
 * 
 */
float R_RadiusFromBounds(vec3_t mins, vec3_t maxs)
{
	vec3_t corner;
	for (int i = 0; i < 3; i++)
		corner[i] = fabs(mins[i]) > fabs(maxs[i]) ? fabs(mins[i]) : fabs(maxs[i]);

	return VectorLength(corner);
}


/**
 * 
 */
bool R_HasSharedLeafs(byte* v1, byte* v2)
{
	int numLeafs = r_worldmodel->numLeafs;
	while (numLeafs > 32)
	{
		unsigned int* v1_x4 = (unsigned int*)v1;
		unsigned int* v2_x4 = (unsigned int*)v2;
	
		if (*v1_x4 & *v2_x4)
			return true;

		numLeafs -= 32;
		v1 += 4;
		v2 += 4;
	}

	for (int i = 0; i < numLeafs; i++)
	{
		if (v1[i >> 3] & (1 << (i & 7)))
			if (v2[i >> 3] & (1 << (i & 7)))
				return true;
	}

	return false;
}
