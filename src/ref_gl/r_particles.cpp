/*
Copyright (C) 2004-2011 Quake2xp Team.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "r_local.h"
#include "r_program.h"

#include "../client/Particle.h"


#define MAX_PARTICLE_VERT 4096*4

int CL_PMpointcontents (vec3_t point);

void MakeNormalVectors (vec3_t forward, vec3_t right, vec3_t up);


vec4_t ParticleColor[MAX_PARTICLE_VERT];
vec3_t ParticleVert[MAX_PARTICLE_VERT];
vec2_t ParticleTextCoord[MAX_PARTICLE_VERT];


/**
 * 
 */
int SortPart (Particle* a, Particle* b)
{
	return (a->type + a->flags) - (b->type + b->flags);
}


/**
 * 
 */
void R_DrawParticles()
{
	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		return;

	unsigned int texture = -1, flagId, flags = -1;
	index_t ParticleIndex[MAX_INDICES];
	int len, loc, partVert = 0, index = 0;
	vec3_t point, width;
	vec3_t move, vec, dir1, dir2, dir3, spdir;
	vec3_t up, right;
	vec3_t axis[3];
	vec3_t oldOrigin;
	float scale, r, g, b, a;
	float c, d, s;

	// setup program
	GL_BindProgram(particlesProgram, 0);

	glEnableVertexAttribArray(ATT_POSITION);
	glEnableVertexAttribArray(ATT_TEX0);
	glEnableVertexAttribArray(ATT_COLOR);

	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, ParticleVert);
	glVertexAttribPointer(ATT_TEX0, 2, GL_FLOAT, false, 0, ParticleTextCoord);
	glVertexAttribPointer(ATT_COLOR, 4, GL_FLOAT, false, 0, ParticleColor);

	GL_MBindRect(GL_TEXTURE1, depthMap->texnum);

	glUniform2f(particle_depthParams, r_newrefdef.depthParms[0], r_newrefdef.depthParms[1]);
	glUniformMatrix4fv(particle_mvp, 1, false, (const float *)r_newrefdef.modelViewProjectionMatrix);
	glUniformMatrix4fv(particle_mv, 1, false, (const float *)r_newrefdef.modelViewMatrix);

	GL_DepthMask(0); // no z buffering
	GL_Enable(GL_BLEND);

	qsort(r_newrefdef.particles, r_newrefdef.num_particles, sizeof(Particle), (int(*)(const void *, const void *))SortPart);

	unsigned int texId = 0;
	Particle* p = r_newrefdef.particles;
	for (int i = 0; i < r_newrefdef.num_particles; i++, p++)
	{
		/// \fixme	There has to be a better way to do this. Many of these are just doing an array index lookup.
		switch (p->type)
		{
		case Particle::PT_BUBBLE:
			texId = r_particletexture[Particle::PT_BUBBLE]->texnum;
			break;

		case Particle::PT_FLY:
			texId = fly[((int)(r_newrefdef.time * 10)) & (MAX_FLY - 1)]->texnum;
			break;

		case Particle::PT_BLOOD:
			texId = r_particletexture[Particle::PT_BLOOD]->texnum;
			break;

		case Particle::PT_BLOOD2:
			texId = r_particletexture[Particle::PT_BLOOD2]->texnum;
			break;

		case Particle::PT_BLASTER:
			texId = r_particletexture[Particle::PT_BLASTER]->texnum;
			break;

		case Particle::PT_SMOKE:
			texId = r_particletexture[Particle::PT_SMOKE]->texnum;
			break;

		case Particle::PT_SPLASH:
			texId = r_particletexture[Particle::PT_SPLASH]->texnum;
			break;

		case Particle::PT_SPARK:
			texId = r_particletexture[Particle::PT_SPARK]->texnum;
			break;

		case Particle::PT_BEAM:
			texId = r_particletexture[Particle::PT_BEAM]->texnum;
			break;

		case Particle::PT_SPIRAL:
			texId = r_particletexture[Particle::PT_SPIRAL]->texnum;
			break;

		case Particle::PT_FLAME:
			texId = flameanim[((int)((r_newrefdef.time - p->time) * 10)) % MAX_FLAMEANIM]->texnum;
			break;

		case Particle::PT_BLOODSPRAY:
			texId = r_blood[((int)((r_newrefdef.time - p->time) * 15)) % MAX_BLOOD]->texnum;
			break;

		case Particle::PT_EXPLODE:
			texId = r_explode[((int)((r_newrefdef.time - p->time) * 35)) % MAX_EXPLODE]->texnum;
			break;

		case Particle::PT_WATERPLUME:
			texId = r_particletexture[Particle::PT_WATERPLUME]->texnum;
			break;

		case Particle::PT_WATERCIRCLE:
			texId = r_particletexture[Particle::PT_WATERCIRCLE]->texnum;
			break;

		case Particle::PT_BLOODDRIP:
			texId = r_particletexture[Particle::PT_BLOODDRIP]->texnum;
			break;

		case Particle::PT_BLOODMIST:
			texId = r_particletexture[Particle::PT_BLOODMIST]->texnum;
			break;

		case Particle::PT_BLASTER_BOLT:
			texId = r_particletexture[Particle::PT_BLASTER_BOLT]->texnum;
			break;

		default:
			texId = r_particletexture[Particle::PT_DEFAULT]->texnum;
			break;
		}

		scale = p->size;
		flagId = p->flags;

		if (texture != texId || flags != flagId)
		{
			if (partVert)
				glDrawElements(GL_TRIANGLES, index, GL_UNSIGNED_SHORT, ParticleIndex);

			texture = texId;
			flags = flagId;
			partVert = 0;
			index = 0;

			GL_MBind(GL_TEXTURE0, texId);
			GL_BlendFunc(p->sFactor, p->dFactor);

			if (p->sFactor == GL_ONE && p->dFactor == GL_ONE)
				glUniform2f(particle_mask, 1.0, 0.0); //color
			else
				glUniform2f(particle_mask, 0.0, 1.0); //alpha

			if (p->flags & PARTICLE_NOFADE)
				glUniform1f(particle_thickness, 0.0);
			else
				glUniform1f(particle_thickness, scale*0.75); // soft blend scale

			if (p->flags & PARTICLE_OVERBRIGHT)
				glUniform1f(particle_colorModulate, 2.0);
			else
				glUniform1f(particle_colorModulate, 1.0);
		}

		r = p->color[0];
		g = p->color[1];
		b = p->color[2];
		a = p->alpha;

		if (p->flags & PARTICLE_STRETCH)
		{
			VectorSubtract(p->origin, r_newrefdef.vieworg, point);
			CrossProduct(point, p->length, width);
			VectorNormalizeFast(width);
			VectorScale(width, scale, width);

			VA_SetElem2(ParticleTextCoord[partVert + 0], 1, 1);
			VA_SetElem3(ParticleVert[partVert + 0], p->origin[0] + width[0],				p->origin[1] + width[1],				p->origin[2] + width[2]);
			VA_SetElem4(ParticleColor[partVert + 0], r, g, b, a);

			VA_SetElem2(ParticleTextCoord[partVert + 1], 0, 0);
			VA_SetElem3(ParticleVert[partVert + 1], p->origin[0] - width[0],
				p->origin[1] - width[1],
				p->origin[2] - width[2]);
			VA_SetElem4(ParticleColor[partVert + 1], r, g, b, a);

			VectorAdd(point, p->length, point);
			CrossProduct(point, p->length, width);
			VectorNormalizeFast(width);
			VectorScale(width, scale, width);

			VA_SetElem2(ParticleTextCoord[partVert + 2], 0, 0);
			VA_SetElem3(ParticleVert[partVert + 2], p->origin[0] + p->length[0] - width[0],
				p->origin[1] + p->length[1] - width[1],
				p->origin[2] + p->length[2] - width[2]);
			VA_SetElem4(ParticleColor[partVert + 2], r, g, b, a);

			VA_SetElem2(ParticleTextCoord[partVert + 3], 1, 1);
			VA_SetElem3(ParticleVert[partVert + 3], p->origin[0] + p->length[0] + width[0],
				p->origin[1] + p->length[1] + width[1],
				p->origin[2] + p->length[2] + width[2]);
			VA_SetElem4(ParticleColor[partVert + 3], r, g, b, a);

			ParticleIndex[index++] = partVert + 0;
			ParticleIndex[index++] = partVert + 1;
			ParticleIndex[index++] = partVert + 3;
			ParticleIndex[index++] = partVert + 3;
			ParticleIndex[index++] = partVert + 1;
			ParticleIndex[index++] = partVert + 2;

			partVert += 4;
		}

		if (p->flags & PARTICLE_SPIRAL)
		{
			VectorCopy(p->origin, move);
			VectorCopy(p->length, vec);
			len = VectorNormalize(vec);
			MakeNormalVectors(vec, right, up);

			for (loc = 0; loc < len; loc++)
			{
				d = loc * 0.1;
				c = cos(d);
				s = sin(d);

				VectorScale(right, c * 5.0f, dir1);
				VectorScaleAndAdd(dir1, up, dir1, s * 5.0f);

				d = (loc + 1) * 0.1;
				c = cos(d);
				s = sin(d);

				VectorScale(right, c * 5.0f, dir2);
				VectorScaleAndAdd(dir2, up, dir2, s * 5.0f);
				VectorAdd(dir2, vec, dir2);

				d = (loc + 2) * 0.1;
				c = cos(d);
				s = sin(d);

				VectorScale(right, c * 5.0f, dir3);
				VectorScaleAndAdd(dir3, up, dir3, s * 5.0f);
				VectorScaleAndAdd(dir3, vec, dir3, 2.0f);

				VectorAdd(move, dir1, point);
				VectorSubtract(dir2, dir1, spdir);

				VectorSubtract(point, r_origin, point);
				CrossProduct(point, spdir, width);

				if (VectorLength(width))
					VectorNormalizeFast(width);
				else
					VectorCopy(vup, width);


				VA_SetElem2(ParticleTextCoord[partVert + 0], 0.5, 1);
				VA_SetElem3(ParticleVert[partVert + 0], point[0] + width[0] + r_origin[0],
					point[1] + width[1] + r_origin[1],
					point[2] + width[2] + r_origin[2]);
				VA_SetElem4(ParticleColor[partVert + 0], r, g, b, a);

				VA_SetElem2(ParticleTextCoord[partVert + 1], 0.5, 0);
				VA_SetElem3(ParticleVert[partVert + 1], point[0] - width[0] + r_origin[0],
					point[1] - width[1] + r_origin[1],
					point[2] - width[2] + r_origin[2]);
				VA_SetElem4(ParticleColor[partVert + 1], r, g, b, a);

				VectorAdd(move, dir2, point);
				VectorSubtract(dir3, dir2, spdir);

				VectorSubtract(point, r_origin, point);
				CrossProduct(point, spdir, width);

				if (VectorLength(width))
					VectorNormalizeFast(width);
				else
					VectorCopy(vup, width);

				VA_SetElem2(ParticleTextCoord[partVert + 2], 0.5, 0);
				VA_SetElem3(ParticleVert[partVert + 2], point[0] - width[0] + r_origin[0],
					point[1] - width[1] + r_origin[1],
					point[2] - width[2] + r_origin[2]);
				VA_SetElem4(ParticleColor[partVert + 2], r, g, b, a);

				VA_SetElem2(ParticleTextCoord[partVert + 3], 0.5, 1);
				VA_SetElem3(ParticleVert[partVert + 3], point[0] + width[0] + r_origin[0],
					point[1] + width[1] + r_origin[1],
					point[2] + width[2] + r_origin[2]);
				VA_SetElem4(ParticleColor[partVert + 3], r, g, b, a);

				ParticleIndex[index++] = partVert + 0;
				ParticleIndex[index++] = partVert + 1;
				ParticleIndex[index++] = partVert + 3;
				ParticleIndex[index++] = partVert + 3;
				ParticleIndex[index++] = partVert + 1;
				ParticleIndex[index++] = partVert + 2;

				partVert += 4;

				VectorAdd(move, vec, move);
			}
		}

		if (p->flags & PARTICLE_DIRECTIONAL)
		{
			// find orientation vectors
			VectorSubtract(r_newrefdef.vieworg, p->origin, axis[0]);
			VectorSubtract(p->previous_origin, p->origin, axis[1]);
			CrossProduct(axis[0], axis[1], axis[2]);
			VectorNormalizeFast(axis[1]);
			VectorNormalizeFast(axis[2]);

			// find normal
			CrossProduct(axis[1], axis[2], axis[0]);
			VectorNormalizeFast(axis[0]);

			VectorScaleAndAdd(p->origin, axis[1], oldOrigin, -p->len);
			VectorScale(axis[2], p->size, axis[2]);

			VA_SetElem2(ParticleTextCoord[partVert + 0], 1, 1);
			VA_SetElem3(ParticleVert[partVert + 0], oldOrigin[0] + axis[2][0],
				oldOrigin[1] + axis[2][1],
				oldOrigin[2] + axis[2][2]);
			VA_SetElem4(ParticleColor[partVert + 0], r, g, b, a);

			VA_SetElem2(ParticleTextCoord[partVert + 1], 0, 1);
			VA_SetElem3(ParticleVert[partVert + 1], p->origin[0] + axis[2][0],
				p->origin[1] + axis[2][1],
				p->origin[2] + axis[2][2]);
			VA_SetElem4(ParticleColor[partVert + 1], r, g, b, a);

			VA_SetElem2(ParticleTextCoord[partVert + 2], 0, 0);
			VA_SetElem3(ParticleVert[partVert + 2], p->origin[0] - axis[2][0],
				p->origin[1] - axis[2][1],
				p->origin[2] - axis[2][2]);
			VA_SetElem4(ParticleColor[partVert + 2], r, g, b, a);

			VA_SetElem2(ParticleTextCoord[partVert + 3], 1, 0);
			VA_SetElem3(ParticleVert[partVert + 3], oldOrigin[0] - axis[2][0],
				oldOrigin[1] - axis[2][1],
				oldOrigin[2] - axis[2][2]);
			VA_SetElem4(ParticleColor[partVert + 3], r, g, b, a);

			ParticleIndex[index++] = partVert + 0;
			ParticleIndex[index++] = partVert + 1;
			ParticleIndex[index++] = partVert + 3;
			ParticleIndex[index++] = partVert + 3;
			ParticleIndex[index++] = partVert + 1;
			ParticleIndex[index++] = partVert + 2;

			partVert += 4;
		}

		if (p->flags & PARTICLE_ALIGNED)
		{
			// Find axes
			VectorCopy(p->dir, axis[0]);
			MakeNormalVectors(axis[0], axis[1], axis[2]);

			// Scale the axes by radius
			VectorScale(axis[1], p->size, axis[1]);
			VectorScale(axis[2], p->size, axis[2]);

			VA_SetElem2(ParticleTextCoord[partVert + 0], 0, 1);
			VA_SetElem3(ParticleVert[partVert + 0], p->origin[0] + axis[1][0] + axis[2][0],
				p->origin[1] + axis[1][1] + axis[2][1],
				p->origin[2] + axis[1][2] + axis[2][2]);
			VA_SetElem4(ParticleColor[partVert + 0], r, g, b, a);

			VA_SetElem2(ParticleTextCoord[partVert + 1], 0, 0);
			VA_SetElem3(ParticleVert[partVert + 1], p->origin[0] - axis[1][0] + axis[2][0],
				p->origin[1] - axis[1][1] + axis[2][1],
				p->origin[2] - axis[1][2] + axis[2][2]);
			VA_SetElem4(ParticleColor[partVert + 1], r, g, b, a);

			VA_SetElem2(ParticleTextCoord[partVert + 2], 1, 0);
			VA_SetElem3(ParticleVert[partVert + 2], p->origin[0] - axis[1][0] - axis[2][0],
				p->origin[1] - axis[1][1] - axis[2][1],
				p->origin[2] - axis[1][2] - axis[2][2]);
			VA_SetElem4(ParticleColor[partVert + 2], r, g, b, a);

			VA_SetElem2(ParticleTextCoord[partVert + 3], 1, 1);
			VA_SetElem3(ParticleVert[partVert + 3], p->origin[0] + axis[1][0] - axis[2][0],
				p->origin[1] + axis[1][1] - axis[2][1],
				p->origin[2] + axis[1][2] - axis[2][2]);
			VA_SetElem4(ParticleColor[partVert + 3], r, g, b, a);

			ParticleIndex[index++] = partVert + 0;
			ParticleIndex[index++] = partVert + 1;
			ParticleIndex[index++] = partVert + 3;
			ParticleIndex[index++] = partVert + 3;
			ParticleIndex[index++] = partVert + 1;
			ParticleIndex[index++] = partVert + 2;

			partVert += 4;
		}

		if (!(p->flags & PARTICLE_ALIGNED) && !(p->flags & PARTICLE_DIRECTIONAL) &&
			!(p->flags & PARTICLE_SPIRAL) && !(p->flags & PARTICLE_STRETCH))
		{
			//  standart particles 
			VectorScale(vup, scale, up);
			VectorScale(vright, scale, right);

			// hack a scale up to keep particles from disapearing
			scale = (p->origin[0] - r_origin[0]) * vpn[0] +
				(p->origin[1] - r_origin[1]) * vpn[1] +
				(p->origin[2] - r_origin[2]) * vpn[2];

			scale = (scale < 20) ? 1 : 1 + scale * 0.0004;

			if (p->orient)
			{
				float c = (float)cos(DEG2RAD(p->orient)) * scale;
				float s = (float)sin(DEG2RAD(p->orient)) * scale;

				VA_SetElem2(ParticleTextCoord[partVert + 0], 0, 1);
				VA_SetElem3(ParticleVert[partVert + 0], p->origin[0] - right[0] * c - up[0] * s,
					p->origin[1] - right[1] * c - up[1] * s,
					p->origin[2] - right[2] * c - up[2] * s);
				VA_SetElem4(ParticleColor[partVert + 0], r, g, b, a);

				VA_SetElem2(ParticleTextCoord[partVert + 1], 0, 0);
				VA_SetElem3(ParticleVert[partVert + 1], p->origin[0] - right[0] * s + up[0] * c,
					p->origin[1] - right[1] * s + up[1] * c,
					p->origin[2] - right[2] * s + up[2] * c);
				VA_SetElem4(ParticleColor[partVert + 1], r, g, b, a);

				VA_SetElem2(ParticleTextCoord[partVert + 2], 1, 0);
				VA_SetElem3(ParticleVert[partVert + 2], p->origin[0] + right[0] * c + up[0] * s,
					p->origin[1] + right[1] * c + up[1] * s,
					p->origin[2] + right[2] * c + up[2] * s);
				VA_SetElem4(ParticleColor[partVert + 2], r, g, b, a);

				VA_SetElem2(ParticleTextCoord[partVert + 3], 1, 1);
				VA_SetElem3(ParticleVert[partVert + 3], p->origin[0] + right[0] * s - up[0] * c,
					p->origin[1] + right[1] * s - up[1] * c,
					p->origin[2] + right[2] * s - up[2] * c);
				VA_SetElem4(ParticleColor[partVert + 3], r, g, b, a);

				ParticleIndex[index++] = partVert + 0;
				ParticleIndex[index++] = partVert + 1;
				ParticleIndex[index++] = partVert + 3;
				ParticleIndex[index++] = partVert + 3;
				ParticleIndex[index++] = partVert + 1;
				ParticleIndex[index++] = partVert + 2;

				partVert += 4;
			}
			else
			{
				VA_SetElem2(ParticleTextCoord[partVert + 0], 0, 1);
				VA_SetElem3(ParticleVert[partVert + 0], p->origin[0] - right[0] * scale - up[0] * scale,
					p->origin[1] - right[1] * scale - up[1] * scale,
					p->origin[2] - right[2] * scale - up[2] * scale);
				VA_SetElem4(ParticleColor[partVert + 0], r, g, b, a);

				VA_SetElem2(ParticleTextCoord[partVert + 1], 0, 0);
				VA_SetElem3(ParticleVert[partVert + 1], p->origin[0] - right[0] * scale + up[0] * scale,
					p->origin[1] - right[1] * scale + up[1] * scale,
					p->origin[2] - right[2] * scale + up[2] * scale);
				VA_SetElem4(ParticleColor[partVert + 1], r, g, b, a);

				VA_SetElem2(ParticleTextCoord[partVert + 2], 1, 0);
				VA_SetElem3(ParticleVert[partVert + 2], p->origin[0] + right[0] * scale + up[0] * scale,
					p->origin[1] + right[1] * scale + up[1] * scale,
					p->origin[2] + right[2] * scale + up[2] * scale);
				VA_SetElem4(ParticleColor[partVert + 2], r, g, b, a);

				VA_SetElem2(ParticleTextCoord[partVert + 3], 1, 1);
				VA_SetElem3(ParticleVert[partVert + 3], p->origin[0] + right[0] * scale - up[0] * scale,
					p->origin[1] + right[1] * scale - up[1] * scale,
					p->origin[2] + right[2] * scale - up[2] * scale);
				VA_SetElem4(ParticleColor[partVert + 3], r, g, b, a);

				ParticleIndex[index++] = partVert + 0;
				ParticleIndex[index++] = partVert + 1;
				ParticleIndex[index++] = partVert + 3;
				ParticleIndex[index++] = partVert + 3;
				ParticleIndex[index++] = partVert + 1;
				ParticleIndex[index++] = partVert + 2;

				partVert += 4;
			}
		}
	}

	if (partVert)
		glDrawElements(GL_TRIANGLES, index, GL_UNSIGNED_SHORT, ParticleIndex);

	GL_Disable(GL_BLEND);
	GL_DepthMask(1);			// back to normal Z buffering

	GL_BindNullProgram();
	glDisableVertexAttribArray(ATT_POSITION);
	glDisableVertexAttribArray(ATT_TEX0);
	glDisableVertexAttribArray(ATT_COLOR);
}
