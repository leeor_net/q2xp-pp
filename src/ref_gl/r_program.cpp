#include "r_local.h"
#include "r_program.h"

#include "../common/filesystem.h"
#include "../common/shared.h"
#include "../common/string_util.h"

#include <string>
#include <map>

#define MAX_INFO_LOG					4096

const std::string SHADER_PATH				= "shader/";
const std::string SHADER_EXTENSION_VERT		= ".vert";
const std::string SHADER_EXTENSION_FRAG		= ".frag";



std::map<const std::string, GLSL_Program>	SHADER_TABLE;


int r_numPrograms;


static const char* glslExt =
"#version 450 core\n"
"out vec4 fragData;\n";	// out fragColor

static const char* mathDefs =
"#define	CUTOFF_EPSILON	1.0 / 255.0\n"
"#define	PI				3.14159265358979323846\n"
"#define	HALF_PI			1.57079632679489661923\n"
"#define	SQRT_PI			1.77245385090551602729\n"
"#define	SQRT_THREE		1.73205080756887729352\n"
"#define	INV_PI			(1.0 / PI)\n";


static GLSL_Program r_nullProgram;

GLSL_Program* ambientWorldProgram;
GLSL_Program* lightWorldProgram;
GLSL_Program* aliasAmbientProgram;
GLSL_Program* aliasBumpProgram;
GLSL_Program* gaussXProgram;
GLSL_Program* gaussYProgram;
GLSL_Program* glareProgram;
GLSL_Program* bloomdsProgram;
GLSL_Program* bloomfpProgram;
GLSL_Program* motionBlurProgram;
GLSL_Program* ssaoProgram;
GLSL_Program* depthDownsampleProgram;
GLSL_Program* ssaoBlurProgram;
GLSL_Program* refractProgram;
GLSL_Program* lightGlassProgram;
GLSL_Program* thermalProgram;
GLSL_Program* thermalfpProgram;
GLSL_Program* waterProgram;
GLSL_Program* lavaProgram;
GLSL_Program* radialProgram;
GLSL_Program* dofProgram;
GLSL_Program* particlesProgram;
GLSL_Program* shadowProgram;
GLSL_Program* ssProgram;
GLSL_Program* genericProgram;
GLSL_Program* cinProgram;
GLSL_Program* loadingProgram;
GLSL_Program* fxaaProgram;
GLSL_Program* filmGrainProgram;
GLSL_Program* nullProgram;
GLSL_Program* gammaProgram;
GLSL_Program* FboProgram;
GLSL_Program* light2dProgram;
GLSL_Program* fixFovProgram;
GLSL_Program* menuProgram;


size_t ambientWorld_lightmapType;
size_t ambientWorld_ssao;
size_t ambientWorld_parallaxParams;
size_t ambientWorld_colorScale;
size_t ambientWorld_specularScale;
size_t ambientWorld_viewOrigin;
size_t ambientWorld_parallaxType;
size_t ambientWorld_ambientLevel;
size_t ambientWorld_scroll;
size_t ambientWorld_mvp;
size_t ambientWorld_lava;

size_t lightWorld_parallaxParams;
size_t lightWorld_colorScale;
size_t lightWorld_viewOrigin;
size_t lightWorld_parallaxType;
size_t lightWorld_lightOrigin;
size_t lightWorld_lightColor;
size_t lightWorld_fog;
size_t lightWorld_fogDensity;
size_t lightWorld_causticsIntens;
size_t lightWorld_caustics;
size_t lightWorld_specularScale;
size_t lightWorld_roughnessScale;
size_t lightWorld_ambient;
size_t lightWorld_attenMatrix;
size_t lightWorld_cubeMatrix;
size_t lightWorld_scroll;
size_t lightWorld_mvp;
size_t lightWorld_isRgh;
size_t lightWorld_spotLight;
size_t lightWorld_spotParams;
size_t lightWorld_spotMatrix;
size_t lightWorld_autoBump;
size_t lightWorld_autoBumpParams;

size_t ambientAlias_ssao;
size_t ambientAlias_colorModulate;
size_t ambientAlias_addShift;
size_t ambientAlias_isEnvMaping;
size_t ambientAlias_envScale;
size_t ambientAlias_isShell;
size_t ambientAlias_scroll;
size_t ambientAlias_mvp;
size_t ambientAlias_viewOrg;

size_t lightAlias_colorScale;
size_t lightAlias_viewOrigin;
size_t lightAlias_lightOrigin;
size_t lightAlias_lightColor;
size_t lightAlias_fog;
size_t lightAlias_fogDensity;
size_t lightAlias_causticsIntens;
size_t lightAlias_isCaustics;
size_t lightAlias_isRgh;
size_t lightAlias_specularScale;
size_t lightAlias_ambient;
size_t lightAlias_attenMatrix;
size_t lightAlias_cubeMatrix;
size_t lightAlias_mvp;
size_t lightAlias_mv;
size_t lightAlias_spotLight;
size_t lightAlias_spotParams;
size_t lightAlias_spotMatrix;
size_t lightAlias_autoBump;
size_t lightAlias_autoBumpParams;

size_t gen_attribColors;
size_t gen_colorModulate;
size_t gen_color;
size_t gen_sky;
size_t gen_mvp;
size_t gen_orthoMatrix;
size_t gen_3d;

size_t ls_fade;
size_t ls_orthoMatrix;

size_t gamma_control;
size_t gamma_orthoMatrix;

size_t menu_params;
size_t menu_orthoMatrix;

size_t fxaa_screenSize;
size_t fxaa_orthoMatrix;

size_t particle_depthParams;
size_t particle_mask;
size_t particle_thickness;
size_t particle_colorModulate;
size_t particle_mvp;
size_t particle_mv;

size_t water_deformMul;
size_t water_alpha;
size_t water_thickness;
size_t water_screenSize;
size_t water_depthParams;
size_t water_colorModulate;
size_t water_ambient;
size_t water_trans;
size_t water_entity2world;
size_t water_mvp;
size_t water_mv;
size_t water_pm;
size_t water_mirror;

size_t gaussx_matrix;
size_t gaussy_matrix;

size_t glare_params;
size_t glare_matrix;

size_t bloomDS_threshold;
size_t bloomDS_matrix;

size_t bloomFP_params;
size_t bloom_FP_matrix;

size_t ref_deformMul;
size_t ref_mvp;
size_t ref_mvm;
size_t ref_pm;
size_t ref_alpha;
size_t ref_thickness;
size_t ref_thickness2;
size_t ref_viewport;
size_t ref_depthParams;
size_t ref_ambientScale;
size_t ref_mask;
size_t ref_alphaMask;

size_t rb_params;
size_t rb_matrix;
size_t rb_cont;

size_t dof_screenSize;
size_t dof_params;
size_t dof_orthoMatrix;

size_t film_screenRes;
size_t film_rand;
size_t film_frameTime;
size_t film_params;
size_t film_matrix;

size_t mb_params;
size_t mb_orthoMatrix;

size_t depthDS_params;
size_t depthDS_orthoMatrix;

size_t ssao_params;
size_t ssao_vp;
size_t ssao_orthoMatrix;

size_t ssaoB_sapmles;
size_t ssaoB_axisMask;
size_t ssaoB_orthoMatrix;

size_t therm_matrix;
size_t thermf_matrix;

size_t sv_mvp;
size_t sv_lightOrg;

size_t ss_orthoMatrix;
size_t ss_tex;

size_t light2d_orthoMatrix;
size_t light2d_params;

size_t fixfov_orthoMatrix;
size_t fixfov_params;

size_t null_mvp;


/*
==========================================

MISCELLANEOUS

==========================================
*/


/*
===============
R_GetInfoLog

===============
*/
static void R_GetInfoLog (int id, char* log, bool isProgram) {
	int		length, dummy;

	if (isProgram)
		glGetProgramiv (id, GL_INFO_LOG_LENGTH, &length);
	else
		glGetShaderiv (id, GL_INFO_LOG_LENGTH, &length);

	if (length < 1) {
		log[0] = 0;
		return;
	}

	if (length >= MAX_INFO_LOG)
		length = MAX_INFO_LOG - 1;

	if (isProgram)
		glGetProgramInfoLog (id, length, &dummy, log);
	else
		glGetShaderInfoLog (id, length, &dummy, log);

	log[length] = 0;
}


/*
==============
R_LoadIncludes

Search shader texts for '#include' directives
and insert included file contents.
==============
*/

char* R_LoadIncludes (char* glsl) {
	char filename[MAX_QPATH];
	char* token, *p, *oldp, *oldglsl;
	int l, limit = 64;          // limit for prevent infinity recursion

	/// calculate size of glsl with includes
	l = strlen (glsl);
	p = glsl;
	while (1) {
		oldp = p;
		token = Com_ParseExt (&p, true);
		if (!token[0])
			break;

		if (!strcmp (token, "#include"))
		{
			int	li;
			char* buf = nullptr;

			if (limit < 0)
				Com_Error (ERR_FATAL, "R_LoadIncludes: more than 64 includes");

			token = Com_ParseExt (&p, false);
			Q_sprintf (filename, sizeof(filename), "shader/include/%s", token);
			li = FS_LoadFile (filename, reinterpret_cast<void**>(&buf));
			if (!buf)
				Com_Error (ERR_FATAL, "Couldn't load %s", filename);

			oldglsl = glsl;
			glsl = (char*)malloc(l + li + 2);
			memset (glsl, 0, l + li + 2);
			memcpy (glsl, oldglsl, oldp - oldglsl);
			Q_strcat (glsl, "\n", l + li + 1);
			Q_strcat (glsl, buf, l + li + 1);
			Q_strcat (glsl, p, l + li + 1);
			p = oldp - oldglsl + glsl;
			l = strlen (glsl);
			FS_FreeFile (buf);
			limit--;
		}
	}

	return glsl;
}


/**
 * \param	name				Name of the shader program.
 * \param	defs				Unknown
 * \param	vertexSource		Unknown
 * \param	fragmentSource		Unknown
 */
static GLSL_Program* R_CreateProgram(const char* name, const char* vertexSource, const char* fragmentSource)
{
	char log[MAX_INFO_LOG];
	const char* strings[MAX_PROGRAM_DEFS * 3 + 2];
	int numStrings;
	int numLinked = 0;
	int id, vertexId, fragmentId;
	int status;
	int i, j;

	/// \fixme	MAGIC NUMBER 17
	if ((vertexSource && strlen(vertexSource) < 17) || (fragmentSource && strlen(fragmentSource) < 17))
	{
		return nullptr;
	}

	if (SHADER_TABLE.size() >= MAX_PROGRAMS)
	{
		VID_Error(ERR_DROP, "R_CreateProgram(): MAX_PROGRAMS hit");
	}

	GLSL_Program program;
	program.name = name;

	program.numId = BIT(program.numDefs);

	for (i = 0; i < program.numId; i++)
	{
		char* defines = nullptr; /// Berserker's fix
		numStrings = 0;
		vertexId = 0;
		fragmentId = 0;

		if (program.numDefs)
		{
			int len = 0;
			for (j = 0; j < program.numDefs; j++)
			{
				if (i & program.defBits[j])
				{
					len += 8 + strlen(program.defStrings[j]) + 1;
				}
			}

			len++; // for trailing NULL
			defines = (char*)calloc(len, sizeof(char));
			for (j = 0; j < program.numDefs; j++)
			{
				if (i & program.defBits[j])
				{
					Q_strcat(defines, "#define ", len);
					Q_strcat(defines, program.defStrings[j], len);
					Q_strcat(defines, "\n", len);
				}
			}
			strings[numStrings++] = defines;
		}

		strings[numStrings++] = glslExt;
		strings[numStrings++] = mathDefs;

		// compile vertex shader
		if (vertexSource)
		{
			// link includes
			vertexSource = R_LoadIncludes(const_cast<char*>(vertexSource));

			strings[numStrings] = vertexSource;
			vertexId = glCreateShader(GL_VERTEX_SHADER);
			glShaderSource(vertexId, numStrings + 1, strings, nullptr);
			glCompileShader(vertexId);
			glGetShaderiv(vertexId, GL_COMPILE_STATUS, &status);

			if (!status)
			{
				R_GetInfoLog(vertexId, log, false);
				glDeleteShader(vertexId);
				Com_Printf("program '%s': error(s) in vertex shader:\n-----------\n%s\n-----------\n", program.name, log);
				continue;
			}
		}

		// compile fragment shader
		if (fragmentSource)
		{
			// link includes
			fragmentSource = R_LoadIncludes(const_cast<char*>(fragmentSource));
			strings[numStrings] = fragmentSource;
			fragmentId = glCreateShader(GL_FRAGMENT_SHADER);

			glShaderSource(fragmentId, numStrings + 1, strings, nullptr);
			glCompileShader(fragmentId);
			glGetShaderiv(fragmentId, GL_COMPILE_STATUS, &status);

			if (!status)
			{
				R_GetInfoLog(fragmentId, log, false);
				glDeleteShader(fragmentId);
				Com_Printf("program '%s': error(s) in fragment shader:\n-----------\n%s\n-----------\n", program.name, log);
				continue;
			}
		}

		// link the program
		id = glCreateProgram();

		if (vertexId)
		{
			glAttachShader(id, vertexId);
			glDeleteShader(vertexId);
		}

		if (fragmentId)
		{
			glAttachShader(id, fragmentId);
			glDeleteShader(fragmentId);
		}

		glLinkProgram(id);
		glGetProgramiv(id, GL_LINK_STATUS, &status);

		R_GetInfoLog(id, log, true);

		if (!status)
		{
			glDeleteProgram(id);
			Com_Printf("program '%s': link error(s): %s\n", program.name, log);
			continue;
		}

		// TODO: glValidateProgram?
		if (defines) { free(defines); }

		program.id[i] = id;
		numLinked++;
	}

	program.valid = (numLinked == program.numId);

	SHADER_TABLE[program.name] = program;

	return &SHADER_TABLE[program.name];
}


/**
 * 
 */
GLSL_Program* R_FindProgram(const std::string& name, bool vertex, bool fragment, bool geo, bool tess, bool tessEv)
{
	char* vertexSource = nullptr;
	char* fragmentSource = nullptr;

	if (!vertex && !fragment) { return &r_nullProgram; }

	std::string newname = std::string(name) + (!vertex ? "(fragment)" : !fragment ? "(vertex)" : "");

	auto shader_iterator = SHADER_TABLE.find(newname);
	
	if (shader_iterator != SHADER_TABLE.end())
	{
		return &shader_iterator->second;
	}

	std::string _filename;

	if (vertex)
	{
		_filename = SHADER_PATH + name + SHADER_EXTENSION_VERT;
		FS_LoadFile(_filename.c_str(), reinterpret_cast<void**>(&vertexSource));
	}

	if (fragment)
	{
		_filename = SHADER_PATH + name + SHADER_EXTENSION_FRAG;
		FS_LoadFile(_filename.c_str(), reinterpret_cast<void**>(&fragmentSource));
	}

	if (!vertexSource && !fragmentSource)
	{
		return &r_nullProgram;
	}

	GLSL_Program* program = R_CreateProgram(newname.c_str(), vertexSource, fragmentSource);

	if (vertexSource)
	{
		FS_FreeFile(vertexSource);
	}
	if (fragmentSource)
	{
		FS_FreeFile(fragmentSource);
	}

	if (!program || !program->valid)
	{
		return &r_nullProgram;
	}

	return program;
}


/**
 * 
 */
void R_InitPrograms()
{
	int missing = 0, stop = 0, id;
	float sec;

	Com_Printf("\n" BAR_LITE "\n");
	Com_Printf("Loading GLSL Shaders");
	Com_Printf("\n" BAR_LITE "\n");

	int start = Sys_Milliseconds();

	Com_Printf("Load " S_COLOR_YELLOW "null program" S_COLOR_WHITE " ");
	nullProgram = R_FindProgram("null", true, true, false, false, false);
	if (nullProgram->valid)
	{
		Com_Printf("succeeded\n");
		id = nullProgram->id[0];
		null_mvp = glGetUniformLocation(id, "u_modelViewProjectionMatrix");
	}
	else
	{
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}

	Com_Printf("Load " S_COLOR_YELLOW "ambient world program" S_COLOR_WHITE " ");
	ambientWorldProgram = R_FindProgram("ambientWorld", true, true, false, false, false);
	if (ambientWorldProgram->valid) {
		Com_Printf("succeeded\n");
		id = ambientWorldProgram->id[0];

		ambientWorld_lightmapType = glGetUniformLocation(id, "u_LightMapType");
		ambientWorld_ssao = glGetUniformLocation(id, "u_ssao");
		ambientWorld_parallaxParams = glGetUniformLocation(id, "u_parallaxParams");
		ambientWorld_colorScale = glGetUniformLocation(id, "u_ColorModulate");
		ambientWorld_specularScale = glGetUniformLocation(id, "u_specularScale");
		ambientWorld_viewOrigin = glGetUniformLocation(id, "u_viewOriginES");
		ambientWorld_parallaxType = glGetUniformLocation(id, "u_parallaxType");
		ambientWorld_ambientLevel = glGetUniformLocation(id, "u_ambientScale");
		ambientWorld_scroll = glGetUniformLocation(id, "u_scroll");
		ambientWorld_mvp = glGetUniformLocation(id, "u_modelViewProjectionMatrix");
		ambientWorld_lava = glGetUniformLocation(id, "u_isLava");

	}
	else {
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}

	Com_Printf("Load " S_COLOR_YELLOW "light world program" S_COLOR_WHITE " ");
	lightWorldProgram = R_FindProgram("lightWorld", true, true, false, false, false);
	if (lightWorldProgram->valid) {
		Com_Printf("succeeded\n");
		id = lightWorldProgram->id[0];

		lightWorld_parallaxParams = glGetUniformLocation(id, "u_parallaxParams");
		lightWorld_colorScale = glGetUniformLocation(id, "u_ColorModulate");
		lightWorld_viewOrigin = glGetUniformLocation(id, "u_viewOriginES");
		lightWorld_parallaxType = glGetUniformLocation(id, "u_parallaxType");

		lightWorld_lightOrigin = glGetUniformLocation(id, "u_LightOrg");
		lightWorld_lightColor = glGetUniformLocation(id, "u_LightColor");
		lightWorld_fog = glGetUniformLocation(id, "u_fog");
		lightWorld_fogDensity = glGetUniformLocation(id, "u_fogDensity");

		lightWorld_causticsIntens = glGetUniformLocation(id, "u_CausticsModulate");
		lightWorld_caustics = glGetUniformLocation(id, "u_isCaustics");

		lightWorld_specularScale = glGetUniformLocation(id, "u_specularScale");
		lightWorld_ambient = glGetUniformLocation(id, "u_isAmbient");
		lightWorld_attenMatrix = glGetUniformLocation(id, "u_attenMatrix");
		lightWorld_cubeMatrix = glGetUniformLocation(id, "u_cubeMatrix");
		lightWorld_scroll = glGetUniformLocation(id, "u_scroll");
		lightWorld_mvp = glGetUniformLocation(id, "u_modelViewProjectionMatrix");
		lightWorld_isRgh = glGetUniformLocation(id, "u_isRgh");
		lightWorld_roughnessScale = glGetUniformLocation(id, "u_roughnessScale");
		lightWorld_spotLight = glGetUniformLocation(id, "u_spotLight");
		lightWorld_spotParams = glGetUniformLocation(id, "u_spotParams");
		lightWorld_spotMatrix = glGetUniformLocation(id, "u_spotMatrix");
		lightWorld_autoBump = glGetUniformLocation(id, "u_autoBump");
		lightWorld_autoBumpParams = glGetUniformLocation(id, "u_autoBumpParams");
	}
	else {
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}

	Com_Printf("Load " S_COLOR_YELLOW "ambient model program" S_COLOR_WHITE " ");
	aliasAmbientProgram = R_FindProgram("ambientAlias", true, true, false, false, false);
	if (aliasAmbientProgram->valid) {
		Com_Printf("succeeded\n");
		id = aliasAmbientProgram->id[0];

		ambientAlias_isEnvMaping = glGetUniformLocation(id, "u_isEnvMap");
		ambientAlias_ssao = glGetUniformLocation(id, "u_ssao");
		ambientAlias_colorModulate = glGetUniformLocation(id, "u_ColorModulate");
		ambientAlias_addShift = glGetUniformLocation(id, "u_AddShift");
		ambientAlias_envScale = glGetUniformLocation(id, "u_envScale");
		ambientAlias_isShell = glGetUniformLocation(id, "u_isShell");
		ambientAlias_scroll = glGetUniformLocation(id, "u_scroll");
		ambientAlias_mvp = glGetUniformLocation(id, "u_modelViewProjectionMatrix");
		ambientAlias_viewOrg = glGetUniformLocation(id, "u_viewOrg");
	}
	else {
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}


	Com_Printf("Load " S_COLOR_YELLOW "light model program" S_COLOR_WHITE " ");
	aliasBumpProgram = R_FindProgram("lightAlias", true, true, false, false, false);

	if (aliasBumpProgram->valid) {
		Com_Printf("succeeded\n");
		id = aliasBumpProgram->id[0];

		lightAlias_viewOrigin = glGetUniformLocation(id, "u_ViewOrigin");
		lightAlias_lightOrigin = glGetUniformLocation(id, "u_LightOrg");
		lightAlias_lightColor = glGetUniformLocation(id, "u_LightColor");
		lightAlias_fog = glGetUniformLocation(id, "u_fog");
		lightAlias_fogDensity = glGetUniformLocation(id, "u_fogDensity");
		lightAlias_causticsIntens = glGetUniformLocation(id, "u_CausticsModulate");
		lightAlias_isCaustics = glGetUniformLocation(id, "u_isCaustics");
		lightAlias_isRgh = glGetUniformLocation(id, "u_isRgh");
		lightAlias_colorScale = glGetUniformLocation(id, "u_ColorModulate");
		lightAlias_specularScale = glGetUniformLocation(id, "u_specularScale");
		lightAlias_ambient = glGetUniformLocation(id, "u_isAmbient");
		lightAlias_attenMatrix = glGetUniformLocation(id, "u_attenMatrix");
		lightAlias_cubeMatrix = glGetUniformLocation(id, "u_cubeMatrix");
		lightAlias_mvp = glGetUniformLocation(id, "u_modelViewProjectionMatrix");
		lightAlias_mv = glGetUniformLocation(id, "u_modelViewMatrix");
		lightAlias_spotLight = glGetUniformLocation(id, "u_spotLight");
		lightAlias_spotParams = glGetUniformLocation(id, "u_spotParams");
		lightAlias_spotMatrix = glGetUniformLocation(id, "u_spotMatrix");
		lightAlias_autoBump = glGetUniformLocation(id, "u_autoBump");
		lightAlias_autoBumpParams = glGetUniformLocation(id, "u_autoBumpParams");
	}
	else {
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}

	Com_Printf("Load " S_COLOR_YELLOW "gauss blur program" S_COLOR_WHITE " ");
	gaussXProgram = R_FindProgram("gaussX", true, true, false, false, false);
	gaussYProgram = R_FindProgram("gaussY", true, true, false, false, false);


	if (gaussXProgram->valid && gaussYProgram->valid) {
		Com_Printf("succeeded\n");

		id = gaussXProgram->id[0];
		gaussx_matrix = glGetUniformLocation(id, "u_orthoMatrix");

		id = gaussYProgram->id[0];
		gaussy_matrix = glGetUniformLocation(id, "u_orthoMatrix");
	}
	else {
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}

	Com_Printf("Load " S_COLOR_YELLOW "hdrGlare program" S_COLOR_WHITE " ");
	glareProgram = R_FindProgram("glare", true, true, false, false, false);

	if (glareProgram->valid)
	{
		Com_Printf("succeeded\n");

		id = glareProgram->id[0];
		glare_params = glGetUniformLocation(id, "u_glareParams");
		glare_matrix = glGetUniformLocation(id, "u_orthoMatrix");
	}
	else
	{
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}

	Com_Printf("Load " S_COLOR_YELLOW "radial blur program" S_COLOR_WHITE " ");
	radialProgram = R_FindProgram("radialBlur", true, true, false, false, false);

	if (radialProgram->valid)
	{
		Com_Printf("succeeded\n");

		id = radialProgram->id[0];
		rb_params = glGetUniformLocation(id, "u_radialBlurParams");
		rb_matrix = glGetUniformLocation(id, "u_orthoMatrix");
		rb_cont = glGetUniformLocation(id, "u_cont");
	}
	else {
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}

	Com_Printf("Load " S_COLOR_YELLOW "dof blur program" S_COLOR_WHITE " ");
	dofProgram = R_FindProgram("dof", true, true, false, false, false);

	if (dofProgram->valid) {
		Com_Printf("succeeded\n");

		id = dofProgram->id[0];
		dof_screenSize = glGetUniformLocation(id, "u_screenSize");
		dof_params = glGetUniformLocation(id, "u_dofParams");
		dof_orthoMatrix = glGetUniformLocation(id, "u_orthoMatrix");
	}
	else {
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}

	Com_Printf("Load " S_COLOR_YELLOW "motion blur program" S_COLOR_WHITE " ");
	motionBlurProgram = R_FindProgram("mblur", true, true, false, false, false);

	if (motionBlurProgram->valid) {
		Com_Printf("succeeded\n");

		id = motionBlurProgram->id[0];
		mb_params = glGetUniformLocation(id, "u_params");
		mb_orthoMatrix = glGetUniformLocation(id, "u_orthoMatrix");
	}
	else {
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}

	Com_Printf("Load " S_COLOR_YELLOW "ssao program" S_COLOR_WHITE " ");
	ssaoProgram = R_FindProgram("ssao", true, true, false, false, false);
	depthDownsampleProgram = R_FindProgram("depthDownsample", true, true, false, false, false);
	ssaoBlurProgram = R_FindProgram("ssaoBlur", true, true, false, false, false);

	if (ssaoProgram->valid && depthDownsampleProgram->valid && ssaoBlurProgram->valid) {
		Com_Printf("succeeded\n");

		id = depthDownsampleProgram->id[0];
		depthDS_params = glGetUniformLocation(id, "u_depthParms");
		depthDS_orthoMatrix = glGetUniformLocation(id, "u_orthoMatrix");

		id = ssaoProgram->id[0];
		ssao_params = glGetUniformLocation(id, "u_ssaoParms");
		ssao_vp = glGetUniformLocation(id, "u_viewport");
		ssao_orthoMatrix = glGetUniformLocation(id, "u_orthoMatrix");

		id = ssaoBlurProgram->id[0];
		ssaoB_sapmles = glGetUniformLocation(id, "u_numSamples");
		ssaoB_axisMask = glGetUniformLocation(id, "u_axisMask");
		ssaoB_orthoMatrix = glGetUniformLocation(id, "u_orthoMatrix");
	}
	else {
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}

	Com_Printf("Load " S_COLOR_YELLOW "bloom program" S_COLOR_WHITE " ");
	bloomdsProgram = R_FindProgram("bloomds", true, true, false, false, false);
	bloomfpProgram = R_FindProgram("bloomfp", true, true, false, false, false);

	if (bloomdsProgram->valid && bloomfpProgram->valid) {
		Com_Printf("succeeded\n");

		id = bloomdsProgram->id[0];
		bloomDS_threshold = glGetUniformLocation(id, "u_BloomThreshold");
		bloomDS_matrix = glGetUniformLocation(id, "u_orthoMatrix");

		id = bloomfpProgram->id[0];
		bloomFP_params = glGetUniformLocation(id, "u_bloomParams");
		bloom_FP_matrix = glGetUniformLocation(id, "u_orthoMatrix");
	}
	else {
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}

	Com_Printf("Load " S_COLOR_YELLOW "refraction program" S_COLOR_WHITE " ");
	refractProgram = R_FindProgram("refract", true, true, false, false, false);

	if (refractProgram->valid) {
		Com_Printf("succeeded\n");
		id = refractProgram->id[0];
		ref_deformMul = glGetUniformLocation(id, "u_deformMul");
		ref_mvp = glGetUniformLocation(id, "u_modelViewProjectionMatrix");
		ref_mvm = glGetUniformLocation(id, "u_modelViewMatrix");
		ref_pm = glGetUniformLocation(id, "u_projectionMatrix");

		ref_alpha = glGetUniformLocation(id, "u_alpha");
		ref_thickness = glGetUniformLocation(id, "u_thickness");
		ref_thickness2 = glGetUniformLocation(id, "u_thickness2");
		ref_viewport = glGetUniformLocation(id, "u_viewport");
		ref_depthParams = glGetUniformLocation(id, "u_depthParms");
		ref_ambientScale = glGetUniformLocation(id, "u_ambientScale");
		ref_mask = glGetUniformLocation(id, "u_mask");
		ref_alphaMask = glGetUniformLocation(id, "u_ALPHAMASK");
	}
	else {
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}

	Com_Printf("Load " S_COLOR_YELLOW "thermal vision program" S_COLOR_WHITE " ");
	thermalProgram = R_FindProgram("thermal", true, true, false, false, false);

	thermalfpProgram = R_FindProgram("thermalfp", true, true, false, false, false);

	if (thermalProgram->valid && thermalfpProgram) {
		Com_Printf("succeeded\n");

		id = thermalProgram->id[0];
		therm_matrix = glGetUniformLocation(id, "u_orthoMatrix");

		id = thermalfpProgram->id[0];
		thermf_matrix = glGetUniformLocation(id, "u_orthoMatrix");

	}
	else {
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}

	Com_Printf("Load " S_COLOR_YELLOW "water program" S_COLOR_WHITE " ");
	waterProgram = R_FindProgram("water", true, true, false, false, false);
	if (waterProgram->valid) {
		Com_Printf("succeeded\n");
		id = waterProgram->id[0];

		water_deformMul = glGetUniformLocation(id, "u_deformMul");
		water_thickness = glGetUniformLocation(id, "u_thickness");
		water_screenSize = glGetUniformLocation(id, "u_viewport");
		water_depthParams = glGetUniformLocation(id, "u_depthParms");
		water_colorModulate = glGetUniformLocation(id, "u_ColorModulate");
		water_ambient = glGetUniformLocation(id, "u_ambientScale");
		water_trans = glGetUniformLocation(id, "u_TRANS");
		water_entity2world = glGetUniformLocation(id, "g_entityToWorldRot");
		water_mvp = glGetUniformLocation(id, "u_modelViewProjectionMatrix");
		water_mv = glGetUniformLocation(id, "u_modelViewMatrix");
		water_pm = glGetUniformLocation(id, "u_projectionMatrix");
		water_mirror = glGetUniformLocation(id, "u_mirror");

	}
	else {
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}

	Com_Printf("Load " S_COLOR_YELLOW "particles program" S_COLOR_WHITE " ");
	particlesProgram = R_FindProgram("particles", true, true, false, false, false);

	if (particlesProgram->valid) {
		Com_Printf("succeeded\n");
		id = particlesProgram->id[0];

		particle_depthParams = glGetUniformLocation(id, "u_depthParms");
		particle_mask = glGetUniformLocation(id, "u_mask");
		particle_thickness = glGetUniformLocation(id, "u_thickness");
		particle_colorModulate = glGetUniformLocation(id, "u_colorScale");
		particle_mvp = glGetUniformLocation(id, "u_modelViewProjectionMatrix");
		particle_mv = glGetUniformLocation(id, "u_modelViewMatrix");
	}
	else {
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}

	Com_Printf("Load " S_COLOR_YELLOW "generic program" S_COLOR_WHITE " ");
	genericProgram = R_FindProgram("generic", true, true, false, false, false);

	if (genericProgram->valid) {
		Com_Printf("succeeded\n");

		id = genericProgram->id[0];
		gen_attribColors = glGetUniformLocation(id, "u_ATTRIB_COLORS");
		gen_colorModulate = glGetUniformLocation(id, "u_colorScale");
		gen_color = glGetUniformLocation(id, "u_color");
		gen_sky = glGetUniformLocation(id, "u_isSky");
		gen_mvp = glGetUniformLocation(id, "u_modelViewProjectionMatrix");
		gen_orthoMatrix = glGetUniformLocation(id, "u_orthoMatrix");
		gen_3d = glGetUniformLocation(id, "u_3d");
	}
	else {
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}

	Com_Printf("Load " S_COLOR_YELLOW "load screen program" S_COLOR_WHITE " ");
	loadingProgram = R_FindProgram("loading", true, true, false, false, false);

	if (loadingProgram->valid) {
		Com_Printf("succeeded\n");
		id = loadingProgram->id[0];
		ls_fade = glGetUniformLocation(id, "u_colorScale");
		ls_orthoMatrix = glGetUniformLocation(id, "u_orthoMatrix");

	}
	else {
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}

	Com_Printf("Load " S_COLOR_YELLOW "fxaa program" S_COLOR_WHITE " ");
	fxaaProgram = R_FindProgram("fxaa", true, true, false, false, false);

	if (fxaaProgram->valid) {
		Com_Printf("succeeded\n");
		id = fxaaProgram->id[0];
		fxaa_screenSize = glGetUniformLocation(id, "u_ScreenSize");
		fxaa_orthoMatrix = glGetUniformLocation(id, "u_orthoMatrix");
	}
	else {
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}

	Com_Printf("Load " S_COLOR_YELLOW "film grain program" S_COLOR_WHITE " ");
	filmGrainProgram = R_FindProgram("filmGrain", true, true, false, false, false);
	id = filmGrainProgram->id[0];

	film_screenRes = glGetUniformLocation(id, "u_screenSize");
	film_rand = glGetUniformLocation(id, "u_rand");
	film_frameTime = glGetUniformLocation(id, "u_time");
	film_params = glGetUniformLocation(id, "u_params");
	film_matrix = glGetUniformLocation(id, "u_orthoMatrix");

	if (filmGrainProgram->valid)
	{
		Com_Printf("succeeded\n");
	}
	else
	{
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}

	Com_Printf("Load " S_COLOR_YELLOW "gammaramp program" S_COLOR_WHITE " ");
	gammaProgram = R_FindProgram("gamma", true, true, false, false, false);
	if (gammaProgram->valid)
	{
		Com_Printf("succeeded\n");
		id = gammaProgram->id[0];
		gamma_control = glGetUniformLocation(id, "u_control");
		gamma_orthoMatrix = glGetUniformLocation(id, "u_orthoMatrix");
	}
	else
	{
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}


	Com_Printf("Load " S_COLOR_YELLOW "shadow volumes program" S_COLOR_WHITE " ");
	shadowProgram = R_FindProgram("shadow", true, true, false, false, false);
	if (shadowProgram->valid) {
		Com_Printf("succeeded\n");
		id = shadowProgram->id[0];
		sv_mvp = glGetUniformLocation(id, "u_modelViewProjectionMatrix");
		sv_lightOrg = glGetUniformLocation(id, "u_lightOrg");
	}
	else {
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}

	Com_Printf("Load " S_COLOR_YELLOW "light2d program" S_COLOR_WHITE " ");
	light2dProgram = R_FindProgram("light2d", true, true, false, false, false);
	if (light2dProgram->valid) {
		Com_Printf("succeeded\n");
		id = light2dProgram->id[0];
		light2d_orthoMatrix = glGetUniformLocation(id, "u_orthoMatrix");
		light2d_params = glGetUniformLocation(id, "u_params");
	}
	else {
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}

	Com_Printf("Load " S_COLOR_YELLOW "perspective correction program" S_COLOR_WHITE " ");
	fixFovProgram = R_FindProgram("fixfov", true, true, false, false, false);
	if (fixFovProgram->valid) {
		Com_Printf("succeeded\n");
		id = fixFovProgram->id[0];
		fixfov_orthoMatrix = glGetUniformLocation(id, "u_orthoMatrix");
		fixfov_params = glGetUniformLocation(id, "u_params");
	}
	else {
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}

	Com_Printf("Load " S_COLOR_YELLOW "menu background program" S_COLOR_WHITE " ");
	menuProgram = R_FindProgram("menu", true, true, false, false, false);
	if (menuProgram->valid) {
		Com_Printf("succeeded\n");
		id = menuProgram->id[0];
		menu_orthoMatrix = glGetUniformLocation(id, "u_orthoMatrix");
		menu_params = glGetUniformLocation(id, "u_screenSize");
	}
	else {
		Com_Printf(S_COLOR_RED"Failed!\n");
		missing++;
	}


	stop = Sys_Milliseconds();
	sec = (float)stop - (float)start;
	Com_Printf("\nGLSL shaders loading time: " S_COLOR_GREEN "%5.4f" S_COLOR_WHITE " sec\n", sec * 0.001);

	Com_Printf("\n");
}


/**
 * 
 */
void R_ShutdownPrograms()
{
	for (auto shader_iterator = SHADER_TABLE.begin(); shader_iterator != SHADER_TABLE.end(); ++shader_iterator)
	{
		for (int _id = 0; _id < shader_iterator->second.numId; ++_id)
		{
			if (shader_iterator->second.id[_id] != 0)
			{
				glDeleteProgram(shader_iterator->second.id[_id]);
			}
		}
	}
}


/*
=============
R_ListPrograms_f

=============
*/
void R_ListPrograms_f ()
{
	int numInvalid = 0;

	Com_Printf ("        permutations name\n");
	Com_Printf ("-------------------------\n");

	for (auto shader_iterator = SHADER_TABLE.begin(); shader_iterator != SHADER_TABLE.end(); ++shader_iterator)
	{
		GLSL_Program& program = shader_iterator->second;
		if (!program.valid) { numInvalid++; }

		Com_Printf ("  %12i %s%s\n", program.numId, program.name, program.valid ? "" : "(INVALID)");
	}

	Com_Printf ("-------------------\n");
	Com_Printf (" %i programs\n", r_numPrograms);
	Com_Printf ("  %i invalid\n", numInvalid);
}

void R_GLSLinfo_f() {
	
	int i;
	GLint j;
	const char* ver;

	ver = (const char*)glGetString(GL_SHADING_LANGUAGE_VERSION);
	Com_Printf("GLSL Version: " S_COLOR_GREEN "%s\n", ver);

	glGetIntegerv(GL_NUM_SHADING_LANGUAGE_VERSIONS, &j);
	for (i = 0; i < j; ++i) {
		ver = (const char*)glGetStringi(GL_SHADING_LANGUAGE_VERSION, i);
		if (!ver)
			break;
		Com_Printf(S_COLOR_YELLOW"%s\n", ver);
	}
}
/*
============
GL_BindNullProgram

============
*/
void GL_BindNullProgram () {

	if (gl_state.programId) {
		glUseProgram (0);
		gl_state.programId = 0;
	}
}


/*
============
GL_BindProgram

============
*/
void GL_BindProgram (GLSL_Program *program, int defBits) {
	int		id;

	if (!program || program->numId < defBits) {
		GL_BindNullProgram ();
		return;
	}

	id = program->id[defBits];

	if (gl_state.programId != id) {
		glUseProgram (id);
		gl_state.programId = id;
	}
}

