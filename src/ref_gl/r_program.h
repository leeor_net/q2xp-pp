#pragma once

#include "../common/shared.h"

#include <string>


#define	MAX_PROGRAM_UNIFORMS	32
#define	MAX_PROGRAM_DEFS		8						// max permutation defs program can have
#define	MAX_PROGRAM_ID			(1 << MAX_PROGRAM_DEFS)	// max GL indices per program object

#define	MAX_UNIFORM_NAME		64
#define	MAX_DEF_NAME			32
#define	MAX_PROGRAMS			256


/**
 *
 */
typedef enum glsl_attribute
{
	ATT_POSITION = 0,
	ATT_NORMAL = 1,
	ATT_TANGENT = 2,
	ATT_BINORMAL = 3,
	ATT_COLOR = 4,
	ATT_TEX0 = 5,
	ATT_TEX1 = 6,
	ATT_TEX2 = 7,
} glsl_attrib;


/**
*
*/
typedef struct
{
	char name[MAX_UNIFORM_NAME];
} glslUniform_t;


/**
 * 
 */
struct GLSL_Program
{
public:
	GLSL_Program() {}
	~GLSL_Program()
	{}

	std::string name;

	int numDefs = 0;

	unsigned int defBits[MAX_PROGRAM_DEFS] = { 0 };
	
	char defStrings[MAX_PROGRAM_DEFS][MAX_DEF_NAME] = { "\0" };
	
	int numId = 0;
	int id[MAX_PROGRAM_ID] = { 0 };
	
	bool valid = false;		// true if all permutations linked successfully
};


extern GLSL_Program* ambientWorldProgram;
extern GLSL_Program* lightWorldProgram;
extern GLSL_Program* aliasAmbientProgram;
extern GLSL_Program* aliasBumpProgram;
extern GLSL_Program* gaussXProgram;
extern GLSL_Program* gaussYProgram;
extern GLSL_Program* glareProgram;
extern GLSL_Program* bloomdsProgram;
extern GLSL_Program* bloomfpProgram;
extern GLSL_Program* motionBlurProgram;
extern GLSL_Program* ssaoProgram;
extern GLSL_Program* depthDownsampleProgram;
extern GLSL_Program* ssaoBlurProgram;
extern GLSL_Program* refractProgram;
extern GLSL_Program* lightGlassProgram;
extern GLSL_Program* thermalProgram;
extern GLSL_Program* thermalfpProgram;
extern GLSL_Program* waterProgram;
extern GLSL_Program* lavaProgram;
extern GLSL_Program* radialProgram;
extern GLSL_Program* dofProgram;
extern GLSL_Program* particlesProgram;
extern GLSL_Program* shadowProgram;
extern GLSL_Program* ssProgram;
extern GLSL_Program* genericProgram;
extern GLSL_Program* cinProgram;
extern GLSL_Program* loadingProgram;
extern GLSL_Program* fxaaProgram;
extern GLSL_Program* filmGrainProgram;
extern GLSL_Program* nullProgram;
extern GLSL_Program* gammaProgram;
extern GLSL_Program* FboProgram;
extern GLSL_Program* light2dProgram;
extern GLSL_Program* fixFovProgram;
extern GLSL_Program* menuProgram;


extern size_t ambientWorld_lightmapType;
extern size_t ambientWorld_ssao;
extern size_t ambientWorld_parallaxParams;
extern size_t ambientWorld_colorScale;
extern size_t ambientWorld_specularScale;
extern size_t ambientWorld_viewOrigin;
extern size_t ambientWorld_parallaxType;
extern size_t ambientWorld_ambientLevel;
extern size_t ambientWorld_scroll;
extern size_t ambientWorld_mvp;
extern size_t ambientWorld_lava;

extern size_t lightWorld_parallaxParams;
extern size_t lightWorld_colorScale;
extern size_t lightWorld_viewOrigin;
extern size_t lightWorld_parallaxType;
extern size_t lightWorld_lightOrigin;
extern size_t lightWorld_lightColor;
extern size_t lightWorld_fog;
extern size_t lightWorld_fogDensity;
extern size_t lightWorld_causticsIntens;
extern size_t lightWorld_caustics;
extern size_t lightWorld_specularScale;
extern size_t lightWorld_roughnessScale;
extern size_t lightWorld_ambient;
extern size_t lightWorld_attenMatrix;
extern size_t lightWorld_cubeMatrix;
extern size_t lightWorld_scroll;
extern size_t lightWorld_mvp;
extern size_t lightWorld_isRgh;
extern size_t lightWorld_spotLight;
extern size_t lightWorld_spotParams;
extern size_t lightWorld_spotMatrix;
extern size_t lightWorld_autoBump;
extern size_t lightWorld_autoBumpParams;

extern size_t ambientAlias_ssao;
extern size_t ambientAlias_colorModulate;
extern size_t ambientAlias_addShift;
extern size_t ambientAlias_isEnvMaping;
extern size_t ambientAlias_envScale;
extern size_t ambientAlias_isShell;
extern size_t ambientAlias_scroll;
extern size_t ambientAlias_mvp;
extern size_t ambientAlias_viewOrg;

extern size_t lightAlias_colorScale;
extern size_t lightAlias_viewOrigin;
extern size_t lightAlias_lightOrigin;
extern size_t lightAlias_lightColor;
extern size_t lightAlias_fog;
extern size_t lightAlias_fogDensity;
extern size_t lightAlias_causticsIntens;
extern size_t lightAlias_isCaustics;
extern size_t lightAlias_isRgh;
extern size_t lightAlias_specularScale;
extern size_t lightAlias_ambient;
extern size_t lightAlias_attenMatrix;
extern size_t lightAlias_cubeMatrix;
extern size_t lightAlias_mvp;
extern size_t lightAlias_mv;
extern size_t lightAlias_spotLight;
extern size_t lightAlias_spotParams;
extern size_t lightAlias_spotMatrix;
extern size_t lightAlias_autoBump;
extern size_t lightAlias_autoBumpParams;

extern size_t gen_attribColors;
extern size_t gen_colorModulate;
extern size_t gen_color;
extern size_t gen_sky;
extern size_t gen_mvp;
extern size_t gen_orthoMatrix;
extern size_t gen_3d;

extern size_t ls_fade;
extern size_t ls_orthoMatrix;

extern size_t gamma_control;
extern size_t gamma_orthoMatrix;

extern size_t menu_params;
extern size_t menu_orthoMatrix;

extern size_t fxaa_screenSize;
extern size_t fxaa_orthoMatrix;

extern size_t particle_depthParams;
extern size_t particle_mask;
extern size_t particle_thickness;
extern size_t particle_colorModulate;
extern size_t particle_mvp;
extern size_t particle_mv;

extern size_t water_deformMul;
extern size_t water_alpha;
extern size_t water_thickness;
extern size_t water_screenSize;
extern size_t water_depthParams;
extern size_t water_colorModulate;
extern size_t water_ambient;
extern size_t water_trans;
extern size_t water_entity2world;
extern size_t water_mvp;
extern size_t water_mv;
extern size_t water_pm;
extern size_t water_mirror;

extern size_t gaussx_matrix;
extern size_t gaussy_matrix;

extern size_t glare_params;
extern size_t glare_matrix;

extern size_t bloomDS_threshold;
extern size_t bloomDS_matrix;

extern size_t bloomFP_params;
extern size_t bloom_FP_matrix;

extern size_t ref_deformMul;
extern size_t ref_mvp;
extern size_t ref_mvm;
extern size_t ref_pm;
extern size_t ref_alpha;
extern size_t ref_thickness;
extern size_t ref_thickness2;
extern size_t ref_viewport;
extern size_t ref_depthParams;
extern size_t ref_ambientScale;
extern size_t ref_mask;
extern size_t ref_alphaMask;

extern size_t rb_params;
extern size_t rb_matrix;
extern size_t rb_cont;

extern size_t dof_screenSize;
extern size_t dof_params;
extern size_t dof_orthoMatrix;

extern size_t film_screenRes;
extern size_t film_rand;
extern size_t film_frameTime;
extern size_t film_params;
extern size_t film_matrix;

extern size_t mb_params;
extern size_t mb_orthoMatrix;

extern size_t depthDS_params;
extern size_t depthDS_orthoMatrix;

extern size_t ssao_params;
extern size_t ssao_vp;
extern size_t ssao_orthoMatrix;

extern size_t ssaoB_sapmles;
extern size_t ssaoB_axisMask;
extern size_t ssaoB_orthoMatrix;

extern size_t therm_matrix;
extern size_t thermf_matrix;

extern size_t sv_mvp;
extern size_t sv_lightOrg;

extern size_t ss_orthoMatrix;
extern size_t ss_tex;

extern size_t light2d_orthoMatrix;
extern size_t light2d_params;

extern size_t fixfov_orthoMatrix;
extern size_t fixfov_params;

extern size_t null_mvp;


void GL_BindProgram(GLSL_Program *program, int defBits);
