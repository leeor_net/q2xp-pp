/*
Copyright (C) 2004-2013 Quake2xp Team, Berserker.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// r_calcAlias.c: calc triangles for alias models
#include "r_local.h"
#include "r_math.h"
#include "r_program.h"


vec3_t	tempVertexArray	[MAX_VERTICES  * 4];

vec3_t *vertexArray;
vec3_t *normalArray;
vec3_t *tangentArray;
vec3_t *binormalArray;
vec4_t *colorArray;


/**
 * 
 */
void R_Init_AliasArrays()
{
	vertexArray		= static_cast<vec3_t*>(malloc(MAX_TRIANGLES * 3 * sizeof(vec3_t)));
	normalArray		= static_cast<vec3_t*>(malloc(MAX_TRIANGLES * 3 * sizeof(vec3_t)));
	tangentArray	= static_cast<vec3_t*>(malloc(MAX_TRIANGLES * 3 * sizeof(vec3_t)));
	binormalArray	= static_cast<vec3_t*>(malloc(MAX_TRIANGLES * 3 * sizeof(vec3_t)));
	colorArray		= static_cast<vec4_t*>(malloc(MAX_TRIANGLES * 3 * sizeof(vec4_t)));
}


/**
 * 
 */
void R_Clean_AliasArrays()
{
	free(vertexArray);
	free(normalArray);
	free(tangentArray);
	free(binormalArray);
	free(colorArray);
}


/**
 * 
 */
void R_CalcAliasFrameLerp(dmd2header_t *paliashdr, float shellScale)
{
	dmd2frame_t	*frame, *oldframe;
	dtrivertx_t	*v, *ov, *verts;
	float	frontlerp;
	vec3_t	move, vectors[3];
	vec3_t	frontv, backv;
	int		i;
	float	*lerp;

	if (CURRENT_MODEL->numFrames < 1)
		return;

	float backlerp = CURRENT_ENTITY->backlerp;

	frame = (dmd2frame_t*)((byte*)paliashdr + paliashdr->ofs_frames + CURRENT_ENTITY->frame * paliashdr->framesize);
	verts = v = frame->verts;
	oldframe = (dmd2frame_t*)((byte*)paliashdr + paliashdr->ofs_frames + CURRENT_ENTITY->oldframe * paliashdr->framesize);
	ov = oldframe->verts;

	frontlerp = 1.0 - backlerp;

	// move should be the delta back to the previous frame * backlerp
	VectorSubtract(CURRENT_ENTITY->oldorigin, CURRENT_ENTITY->origin, move);

	if (CURRENT_ENTITY->angles[0] || CURRENT_ENTITY->angles[1] || CURRENT_ENTITY->angles[2])
	{
		vec3_t	temp;
		VectorCopy(move, temp);
		AngleVectors(CURRENT_ENTITY->angles, vectors[0], vectors[1], vectors[2]);
		move[0] = DotProduct(temp, vectors[0]);
		move[1] = -DotProduct(temp, vectors[1]);
		move[2] = DotProduct(temp, vectors[2]);
	}

	VectorAdd(move, oldframe->translate, move);

	for (i = 0; i < 3; i++)
	{
		move[i] = backlerp*move[i] + frontlerp*frame->translate[i];
		frontv[i] = frontlerp*frame->scale[i];
		backv[i] = backlerp*oldframe->scale[i];
	}

	lerp = tempVertexArray[0];

	if (CURRENT_ENTITY->flags & (RF_SHELL_RED | RF_SHELL_GREEN | RF_SHELL_BLUE | RF_SHELL_DOUBLE | RF_SHELL_HALF_DAM | RF_SHELL_GOD))
	{
		for (i = 0; i < paliashdr->num_xyz; i++, v++, ov++, lerp += 3)
		{
			float *normal = q_byteDirs[verts[i].lightnormalindex];
			lerp[0] = move[0] + ov->v[0] * backv[0] + v->v[0] * frontv[0] + normal[0] * shellScale;
			lerp[1] = move[1] + ov->v[1] * backv[1] + v->v[1] * frontv[1] + normal[1] * shellScale;
			lerp[2] = move[2] + ov->v[2] * backv[2] + v->v[2] * frontv[2] + normal[2] * shellScale;
		}
	}
	else
	{
		for (i = 0; i < paliashdr->num_xyz; i++, v++, ov++, lerp += 3)
		{
			lerp[0] = move[0] + ov->v[0] * backv[0] + v->v[0] * frontv[0];
			lerp[1] = move[1] + ov->v[1] * backv[1] + v->v[1] * frontv[1];
			lerp[2] = move[2] + ov->v[2] * backv[2] + v->v[2] * frontv[2];
		}
	}
}


/**
 * 
 */
void GL_DrawAliasFrameLerp(dmd2header_t *paliashdr, vec3_t lightColor)
{
	int				index_xyz;
	int				i, j, jj = 0;
	dmd2vertex_t		*tris;
	Image			*skin, *skinNormalmap, *glowskin;
	float			alphaShift, alpha;
	float			backlerp, frontlerp;
	int				index2, oldindex2;
	dmd2frame_t	*frame, *oldframe;
	dtrivertx_t		*verts, *oldverts;

	alphaShift = sin(ref_realtime * CURRENT_MODEL->glowCfg[2]);
	alphaShift = clamp((alphaShift + 1) * 0.5f, CURRENT_MODEL->glowCfg[0], CURRENT_MODEL->glowCfg[1]);

	if (CURRENT_ENTITY->flags & RF_TRANSLUCENT)
		alpha = CURRENT_ENTITY->alpha;
	else
		alpha = 1.0;

	if (CURRENT_ENTITY->flags & (RF_VIEWERMODEL))
		return;

	if (r_skipStaticLights->value)
	{
		if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
			VectorSet(lightColor, 0.5, 0.5, 0.5);
	}
	else
	{
		if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
			VectorSet(lightColor, 0.18, 0.17, 0.14);
	}

	if (r_newrefdef.rdflags & RDF_IRGOGGLES)
		VectorSet(lightColor, 1, 1, 1);

	// select skin
	if (CURRENT_ENTITY->skin)
		skin = CURRENT_ENTITY->skin;	// custom player skin
	else
	{
		if (CURRENT_ENTITY->skinnum >= MAX_MD2SKINS)
		{
			skin = CURRENT_MODEL->skins[0];
			CURRENT_ENTITY->skinnum = 0;
		}
		else
		{
			skin = CURRENT_MODEL->skins[CURRENT_ENTITY->skinnum];
			if (!skin)
			{
				skin = CURRENT_MODEL->skins[0];
				CURRENT_ENTITY->skinnum = 0;
			}
		}
	}

	if (!skin)
		skin = r_missingTexture;

	// select skin
	if (CURRENT_ENTITY->bump)
		skinNormalmap = CURRENT_ENTITY->bump;	// custom player skin
	else
	{
		if (CURRENT_ENTITY->skinnum >= MAX_MD2SKINS)
		{
			skinNormalmap = CURRENT_MODEL->skins_normal[0];
			CURRENT_ENTITY->skinnum = 0;
		}
		else
		{
			skinNormalmap = CURRENT_MODEL->skins_normal[CURRENT_ENTITY->skinnum];
			if (!skin)
			{
				skinNormalmap = CURRENT_MODEL->skins_normal[0];
				CURRENT_ENTITY->skinnum = 0;
			}
		}
	}
	if (!skinNormalmap)
		skinNormalmap = r_defBump;

	glowskin = CURRENT_MODEL->glowtexture[CURRENT_ENTITY->skinnum];

	if (!glowskin)
		glowskin = r_notexture;

	if (!skin)
		skin = r_missingTexture;

	R_CalcAliasFrameLerp(paliashdr, 0);

	glEnableVertexAttribArray(ATT_POSITION);
	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, vertexArray);

	glEnableVertexAttribArray(ATT_NORMAL);
	glVertexAttribPointer(ATT_NORMAL, 3, GL_FLOAT, false, 0, normalArray);

	glEnableVertexAttribArray(ATT_COLOR);
	glVertexAttribPointer(ATT_COLOR, 4, GL_FLOAT, false, 0, colorArray);

	//glBindBufferARB (GL_ARRAY_BUFFER_ARB, CURRENT_MODEL->vboId);
	glEnableVertexAttribArray(ATT_TEX0);
	glVertexAttribPointer(ATT_TEX0, 2, GL_FLOAT, false, 0, CURRENT_MODEL->st);

	tris = (dmd2vertex_t *)((byte *)paliashdr + paliashdr->ofs_tris);
	jj = 0;

	oldframe = (dmd2frame_t *)((byte *)paliashdr + paliashdr->ofs_frames + CURRENT_ENTITY->oldframe * paliashdr->framesize);
	oldverts = oldframe->verts;
	frame = (dmd2frame_t *)((byte *)paliashdr + paliashdr->ofs_frames + CURRENT_ENTITY->frame * paliashdr->framesize);
	verts = frame->verts;
	backlerp = CURRENT_ENTITY->backlerp;
	frontlerp = 1 - backlerp;

	for (i = 0; i < paliashdr->num_tris; i++)
	{
		for (j = 0; j < 3; j++, jj++)
		{
			index_xyz = tris[i].index_xyz[j];
			VectorCopy(tempVertexArray[index_xyz], vertexArray[jj]);

			VA_SetElem4(colorArray[jj], lightColor[0], lightColor[1], lightColor[2], alpha);

			if (CURRENT_MODEL->envMap)
			{
				index2 = verts[index_xyz].lightnormalindex;
				oldindex2 = oldverts[index_xyz].lightnormalindex;
				normalArray[jj][0] = q_byteDirs[oldindex2][0] * backlerp + q_byteDirs[index2][0] * frontlerp;
				normalArray[jj][1] = q_byteDirs[oldindex2][1] * backlerp + q_byteDirs[index2][1] * frontlerp;
				normalArray[jj][2] = q_byteDirs[oldindex2][2] * backlerp + q_byteDirs[index2][2] * frontlerp;
			}
		}
	}

	// setup program
	GL_BindProgram(aliasAmbientProgram, 0);

	if (CURRENT_MODEL->envMap)
		glUniform1i(ambientAlias_isEnvMaping, 1);
	else
		glUniform1i(ambientAlias_isEnvMaping, 0);

	glUniform1i(ambientAlias_isShell, 0);

	glUniform1f(ambientAlias_colorModulate, r_textureColorScale->value);
	glUniform1f(ambientAlias_addShift, alphaShift);

	GL_MBind(GL_TEXTURE0, skin->texnum);
	GL_MBind(GL_TEXTURE1, glowskin->texnum);
	GL_MBind(GL_TEXTURE2, r_envTex->texnum);
	GL_MBind(GL_TEXTURE3, skinNormalmap->texnum);

	glUniform1f(ambientAlias_envScale, CURRENT_MODEL->envScale);

	if (r_ssao->value && !(CURRENT_ENTITY->flags & RF_WEAPONMODEL) && !(r_newrefdef.rdflags & RDF_NOWORLDMODEL) && !(r_newrefdef.rdflags & RDF_IRGOGGLES))
	{
		GL_MBindRect(GL_TEXTURE4, fboColor[fboColorIndex]->texnum);
		glUniform1i(ambientAlias_ssao, 1);
	}
	else
		glUniform1i(ambientAlias_ssao, 0);

	glUniform3fv(ambientAlias_viewOrg, 1, r_origin);
	glUniformMatrix4fv(ambientAlias_mvp, 1, false, (const float *)CURRENT_ENTITY->orMatrix);

	glDrawArrays(GL_TRIANGLES, 0, jj);

	glDisableVertexAttribArray(ATT_POSITION);
	glDisableVertexAttribArray(ATT_NORMAL);
	glDisableVertexAttribArray(ATT_COLOR);
	glDisableVertexAttribArray(ATT_TEX0);
	//glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
	GL_BindNullProgram();
}


/**
 * 
 */
void GL_DrawAliasFrameLerpShell(dmd2header_t *paliashdr)
{
	int			index_xyz, i, j, jj = 0;
	dmd2vertex_t	*tris;
	unsigned	defBits = 0;
	float		scroll = 0.0;
	float		backlerp, frontlerp;
	int			index2, oldindex2;
	dmd2frame_t	*frame, *oldframe;
	dtrivertx_t		*verts, *oldverts;

	if (CURRENT_ENTITY->flags & (RF_VIEWERMODEL))
		return;

	scroll = r_newrefdef.time *0.45;

	if (CURRENT_ENTITY->flags & RF_WEAPONMODEL)
		R_CalcAliasFrameLerp(paliashdr, 0.1);
	else if (CURRENT_ENTITY->flags & RF_CAMERAMODEL2)
		R_CalcAliasFrameLerp(paliashdr, 0.0);
	else
		R_CalcAliasFrameLerp(paliashdr, 0.5);

	jj = 0;
	tris = (dmd2vertex_t *)((byte *)paliashdr + paliashdr->ofs_tris);
	oldframe = (dmd2frame_t *)((byte *)paliashdr + paliashdr->ofs_frames + CURRENT_ENTITY->oldframe * paliashdr->framesize);
	oldverts = oldframe->verts;
	frame = (dmd2frame_t *)((byte *)paliashdr + paliashdr->ofs_frames + CURRENT_ENTITY->frame * paliashdr->framesize);
	verts = frame->verts;
	backlerp = CURRENT_ENTITY->backlerp;
	frontlerp = 1 - backlerp;

	for (i = 0; i < paliashdr->num_tris; i++)
	{
		for (j = 0; j < 3; j++, jj++)
		{
			index_xyz = tris[i].index_xyz[j];
			VectorCopy(tempVertexArray[index_xyz], vertexArray[jj]);

			index2 = verts[index_xyz].lightnormalindex;
			oldindex2 = oldverts[index_xyz].lightnormalindex;

			normalArray[jj][0] = q_byteDirs[oldindex2][0] * backlerp + q_byteDirs[index2][0] * frontlerp;
			normalArray[jj][1] = q_byteDirs[oldindex2][1] * backlerp + q_byteDirs[index2][1] * frontlerp;
			normalArray[jj][2] = q_byteDirs[oldindex2][2] * backlerp + q_byteDirs[index2][2] * frontlerp;

		}
	}

	// setup program
	GL_BindProgram(aliasAmbientProgram, defBits);

	glUniform1i(ambientAlias_isShell, 1);
	glUniform1i(ambientAlias_isEnvMaping, 0);
	glUniform1f(ambientAlias_colorModulate, r_textureColorScale->value);
	glUniform1f(ambientAlias_scroll, scroll);
	glUniform3fv(ambientAlias_viewOrg, 1, r_origin);

	glUniformMatrix4fv(ambientAlias_mvp, 1, false, (const float *)CURRENT_ENTITY->orMatrix);

	if (CURRENT_ENTITY->flags & RF_SHELL_BLUE)
		GL_MBind(GL_TEXTURE0, r_texshell[0]->texnum);
	if (CURRENT_ENTITY->flags & RF_SHELL_RED)
		GL_MBind(GL_TEXTURE0, r_texshell[1]->texnum);
	if (CURRENT_ENTITY->flags & RF_SHELL_GREEN)
		GL_MBind(GL_TEXTURE0, r_texshell[2]->texnum);
	if (CURRENT_ENTITY->flags & RF_SHELL_GOD)
		GL_MBind(GL_TEXTURE0, r_texshell[3]->texnum);
	if (CURRENT_ENTITY->flags & RF_SHELL_HALF_DAM)
		GL_MBind(GL_TEXTURE0, r_texshell[4]->texnum);
	if (CURRENT_ENTITY->flags & RF_SHELL_DOUBLE)
		GL_MBind(GL_TEXTURE0, r_texshell[5]->texnum);

	glEnableVertexAttribArray(ATT_POSITION);
	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, vertexArray);

	glEnableVertexAttribArray(ATT_NORMAL);
	glVertexAttribPointer(ATT_NORMAL, 3, GL_FLOAT, false, 0, normalArray);

	//glBindBufferARB(GL_ARRAY_BUFFER_ARB, CURRENT_MODEL->vboId);
	glEnableVertexAttribArray(ATT_TEX0);
	glVertexAttribPointer(ATT_TEX0, 2, GL_FLOAT, false, 0, CURRENT_MODEL->st);

	glDrawArrays(GL_TRIANGLES, 0, jj);

	GL_Disable(GL_BLEND);

	glDisableVertexAttribArray(ATT_POSITION);
	glDisableVertexAttribArray(ATT_NORMAL);
	glDisableVertexAttribArray(ATT_TEX0);
	//glBindBufferARB (GL_ARRAY_BUFFER_ARB, 0);
	GL_BindNullProgram();
}


/**
 * 
 */
void R_UpdateLightAliasUniforms()
{
	mat4_t	entAttenMatrix, entSpotMatrix;

	glUniform1f(lightAlias_colorScale, r_textureColorScale->value);
	glUniform1i(lightAlias_ambient, (int)currentShadowLight->isAmbient);
	glUniform1f(lightAlias_specularScale, r_specularScale->value);
	glUniform4f(lightAlias_lightColor, currentShadowLight->color[0], currentShadowLight->color[1], currentShadowLight->color[2], 1.0);
	glUniform1i(lightAlias_fog, (int)currentShadowLight->isFog);

	if (CURRENT_ENTITY->flags & RF_WEAPONMODEL)
		glUniform1f(lightAlias_fogDensity, currentShadowLight->fogDensity * 8.0);
	else
		glUniform1f(lightAlias_fogDensity, currentShadowLight->fogDensity);

	glUniform1f(lightAlias_causticsIntens, r_causticIntens->value);
	glUniform3fv(lightAlias_viewOrigin, 1, r_origin);
	glUniform3fv(lightAlias_lightOrigin, 1, currentShadowLight->origin);

	Mat4_TransposeMultiply(CURRENT_ENTITY->matrix, currentShadowLight->attenMatrix, entAttenMatrix);
	glUniformMatrix4fv(lightAlias_attenMatrix, 1, false, (const float *)entAttenMatrix);


	Mat4_TransposeMultiply(CURRENT_ENTITY->matrix, currentShadowLight->spotMatrix, entSpotMatrix);
	glUniformMatrix4fv(lightAlias_spotMatrix, 1, false, (const float *)entSpotMatrix);
	glUniform3f(lightAlias_spotParams, currentShadowLight->hotSpot, 1.f / (1.f - currentShadowLight->hotSpot), currentShadowLight->coneExp);

	if (currentShadowLight->isCone)
		glUniform1i(lightAlias_spotLight, 1);
	else
		glUniform1i(lightAlias_spotLight, 0);

	R_CalcCubeMapMatrix(true);
	glUniformMatrix4fv(lightAlias_cubeMatrix, 1, false, (const float *)currentShadowLight->cubeMapMatrix);

	glUniformMatrix4fv(lightAlias_mvp, 1, false, (const float*)CURRENT_ENTITY->orMatrix);
	glUniformMatrix4fv(lightAlias_mv, 1, false, (const float*)r_newrefdef.modelViewMatrix);
}


/**
 * 
 */
void GL_DrawAliasFrameLerpLight (dmd2header_t *paliashdr)
{
	int				i, j, jj = 0;
	int				index_xyz;
	byte			*binormals, *oldbinormals;
	byte			*tangents, *oldtangents;
	dmd2vertex_t	*tris;
	dmd2frame_t		*frame, *oldframe;
	dtrivertx_t		*verts, *oldverts;
	float			backlerp, frontlerp;
	unsigned		offs, offs2;
	vec3_t			maxs;
	Image			*skin, *skinNormalmap, *rgh;
	int				index2, oldindex2;
	bool			inWater;

	if (CURRENT_ENTITY->flags & (RF_VIEWERMODEL))
		return;

	if (CURRENT_MODEL->noSelfShadow && r_shadows->value)
		GL_Disable(GL_STENCIL_TEST);

	backlerp = CURRENT_ENTITY->backlerp;
	frontlerp = 1 - backlerp;

	offs = paliashdr->num_xyz;

	oldframe = (dmd2frame_t*)((byte*)paliashdr + paliashdr->ofs_frames + CURRENT_ENTITY->oldframe * paliashdr->framesize);
	oldverts = oldframe->verts;
	offs2 = offs*CURRENT_ENTITY->oldframe;
	oldbinormals = CURRENT_MODEL->binormals + offs2;
	oldtangents = CURRENT_MODEL->tangents + offs2;

	frame = (dmd2frame_t*)((byte*)paliashdr + paliashdr->ofs_frames + CURRENT_ENTITY->frame * paliashdr->framesize);
	verts = frame->verts;
	offs2 = offs*CURRENT_ENTITY->frame;
	binormals = CURRENT_MODEL->binormals + offs2;
	tangents = CURRENT_MODEL->tangents + offs2;
	tris = (dmd2vertex_t *)((byte *)paliashdr + paliashdr->ofs_tris);

	// select skin
	if (CURRENT_ENTITY->skin)
		skin = CURRENT_ENTITY->skin;	// custom player skin
	else
	{
		if (CURRENT_ENTITY->skinnum >= MAX_MD2SKINS)
		{
			skin = CURRENT_MODEL->skins[0];
			CURRENT_ENTITY->skinnum = 0;
		}
		else
		{
			skin = CURRENT_MODEL->skins[CURRENT_ENTITY->skinnum];
			if (!skin) {
				skin = CURRENT_MODEL->skins[0];
				CURRENT_ENTITY->skinnum = 0;
			}
		}
	}

	if (!skin)
		skin = r_missingTexture;

	// select skin
	if (CURRENT_ENTITY->bump)
		skinNormalmap = CURRENT_ENTITY->bump;	// custom player skin
	else
	{
		if (CURRENT_ENTITY->skinnum >= MAX_MD2SKINS)
		{
			skinNormalmap = CURRENT_MODEL->skins_normal[0];
			CURRENT_ENTITY->skinnum = 0;
		}
		else
		{
			skinNormalmap = CURRENT_MODEL->skins_normal[CURRENT_ENTITY->skinnum];
			if (!skin)
			{
				skinNormalmap = CURRENT_MODEL->skins_normal[0];
				CURRENT_ENTITY->skinnum = 0;
			}
		}
	}

	if (!skinNormalmap)
		skinNormalmap = r_defBump;
	
	rgh = CURRENT_MODEL->skins_roughness[CURRENT_ENTITY->skinnum];
	if (!rgh)
		rgh = r_notexture;

	R_CalcAliasFrameLerp(paliashdr, 0);
	
	for (i = 0; i < paliashdr->num_tris; i++)
	{
		for (j = 0; j < 3; j++, jj++)
		{
			index_xyz = tris[i].index_xyz[j];
			index2 = verts[index_xyz].lightnormalindex;
			oldindex2 = oldverts[index_xyz].lightnormalindex;

			normalArray[jj][0] = q_byteDirs[oldindex2][0] * backlerp + q_byteDirs[index2][0] * frontlerp;
			normalArray[jj][1] = q_byteDirs[oldindex2][1] * backlerp + q_byteDirs[index2][1] * frontlerp;
			normalArray[jj][2] = q_byteDirs[oldindex2][2] * backlerp + q_byteDirs[index2][2] * frontlerp;

			tangentArray[jj][0] = q_byteDirs[oldtangents[index_xyz]][0] * backlerp + q_byteDirs[tangents[index_xyz]][0] * frontlerp;
			tangentArray[jj][1] = q_byteDirs[oldtangents[index_xyz]][1] * backlerp + q_byteDirs[tangents[index_xyz]][1] * frontlerp;
			tangentArray[jj][2] = q_byteDirs[oldtangents[index_xyz]][2] * backlerp + q_byteDirs[tangents[index_xyz]][2] * frontlerp;

			binormalArray[jj][0] = q_byteDirs[oldbinormals[index_xyz]][0] * backlerp + q_byteDirs[binormals[index_xyz]][0] * frontlerp;
			binormalArray[jj][1] = q_byteDirs[oldbinormals[index_xyz]][1] * backlerp + q_byteDirs[binormals[index_xyz]][1] * frontlerp;
			binormalArray[jj][2] = q_byteDirs[oldbinormals[index_xyz]][2] * backlerp + q_byteDirs[binormals[index_xyz]][2] * frontlerp;

			VectorCopy(tempVertexArray[index_xyz], vertexArray[jj]);
		}
	}

	GL_BindProgram (aliasBumpProgram, 0);

	VectorAdd (CURRENT_ENTITY->origin, CURRENT_ENTITY->model->maxs, maxs);
	
	inWater = (CL_PMpointcontents(maxs) & MASK_WATER) != 0;

	R_UpdateLightAliasUniforms();
	
	if (r_imageAutoBump->value && skinNormalmap == r_defBump)
	{
		glUniform1i(lightAlias_autoBump, 1);
		glUniform2f(lightAlias_autoBumpParams, r_imageAutoBumpScale->value, r_imageAutoSpecularScale->value);
	}
	else
		glUniform1i(lightAlias_autoBump, 0);

	if (inWater && currentShadowLight->castCaustics && !(r_newrefdef.rdflags & RDF_NOWORLDMODEL))
		glUniform1i(lightAlias_isCaustics, 1);
	else
		glUniform1i(lightAlias_isCaustics, 0);

	GL_MBind (GL_TEXTURE0, skinNormalmap->texnum);
	GL_MBind (GL_TEXTURE1, skin->texnum);
	GL_MBind (GL_TEXTURE2, r_caustic[((int)(r_newrefdef.time * 15)) & (MAX_CAUSTICS - 1)]->texnum);
	GL_MBindCube (GL_TEXTURE3, r_lightCubeMap[currentShadowLight->filter]->texnum);

	if (rgh == r_notexture)
	{
		glUniform1i(lightAlias_isRgh, 0);
	}
	else
	{
		glUniform1i(lightAlias_isRgh, 1);
		GL_MBind(GL_TEXTURE4, rgh->texnum);
	}
	
	GL_MBind(GL_TEXTURE5, skinBump->texnum);

	glEnableVertexAttribArray (ATT_POSITION);
	glEnableVertexAttribArray(ATT_TANGENT);
	glEnableVertexAttribArray(ATT_BINORMAL);
	glEnableVertexAttribArray(ATT_NORMAL);
	glEnableVertexAttribArray(ATT_TEX0);

	glVertexAttribPointer (ATT_POSITION, 3, GL_FLOAT, false, 0, vertexArray);
	glVertexAttribPointer (ATT_TANGENT, 3, GL_FLOAT, false, 0, tangentArray);
	glVertexAttribPointer (ATT_BINORMAL, 3, GL_FLOAT, false, 0, binormalArray);
	glVertexAttribPointer (ATT_NORMAL, 3, GL_FLOAT, false, 0, normalArray);
	glVertexAttribPointer (ATT_TEX0, 2, GL_FLOAT, false, 0, CURRENT_MODEL->st);

	glDrawArrays (GL_TRIANGLES, 0, jj);

	glDisableVertexAttribArray (ATT_POSITION);
	glDisableVertexAttribArray (ATT_TANGENT);
	glDisableVertexAttribArray (ATT_BINORMAL);
	glDisableVertexAttribArray (ATT_NORMAL);
	glDisableVertexAttribArray (ATT_TEX0);
	GL_BindNullProgram ();
}
