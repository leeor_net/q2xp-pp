#pragma once

#include "r_local.h"

void BSP_Init();
void BSP_LoadBrushModel(Model*  mod, void *buffer);

byte* BSP_ClusterPVS(int cluster, Model* model);

mleaf_t* BSP_PointInLeaf(vec3_t p, Model* model);
