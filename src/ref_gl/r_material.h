#pragma once

#include "r_local.h"


/**
* Loads a texture's associated material file and processes it.
*
* \param texture	Pointer to an mtexInfo_t to set paramters for.
* \param path		Path of the texture to process.
* 
* \return	Returns \c true if parsing material is successful.
*/
bool Material_Load(mtexInfo_t* texture, char* path);


/**
 * Sets default values for textures.
 */
void Material_SetDefault(mtexInfo_t* texture);