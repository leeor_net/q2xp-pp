#pragma once

#include "r_local.h"

extern const mat3_3_t mat3_identity;
extern const mat4_t	mat4_identity;

void AnglesToMat3(const vec3_t angles, mat3_3_t& m);

void Mat3_Identity(mat3_3_t& m);
void Mat3_Copy(const mat3_3_t& in, mat3_3_t& out);

void Mat3_TransposeMultiplyVector(const mat3_3_t& m, const vec3_t& in, vec3_t& out);

void Mat4_Multiply(const mat4_t a, const mat4_t b, mat4_t out);
void Mat4_Copy(const mat4_t in, mat4_t out);
void Mat4_Transpose(const mat4_t in, mat4_t out);
void Mat4_MultiplyVector(const mat4_t m, const vec3_t in, vec3_t out);
void Mat4_Translate(mat4_t m, float x, float y, float z);
void Mat4_Scale(mat4_t m, float x, float y, float z);
bool Mat4_Invert(const mat4_t in, mat4_t out);
void Mat4_TransposeMultiply(const mat4_t a, const mat4_t b, mat4_t out);
void Mat4_SetOrientation(mat4_t m, const mat3_3_t rotation, const vec3_t translation);
void Mat4_Identity(mat4_t mat);
void Mat4_Rotate(mat4_t m, float angle, float x, float y, float z);
void Mat4_AffineInvert(const mat4_t in, mat4_t out);
void Mat4_SetupTransform(mat4_t m, const mat3_3_t rotation, const vec3_t translation);
void Mat3_Set(mat3_3_t mat, vec3_t x, vec3_t y, vec3_t z);
void Mat4_Set(mat4_t mat, vec4_t x, vec4_t y, vec4_t z, vec4_t w);

void SetPlaneType(cplane_t *plane);
void SetPlaneSignBits(cplane_t *plane);

void AddBoundsToBounds(const vec3_t mins1, const vec3_t maxs1, vec3_t mins2, vec3_t maxs2);