/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
#include "r_local.h"

#include "r_image.h"

#include "../common/filesystem.h"
#include "../common/string_util.h"
#include "../common/util.h"

#include "../win32/winquake.h"


// ===============================================================================
// = DEFINES
// ===============================================================================
#define	MAX_SCRAPS		1
#define	BLOCK_SIZE		256


// ===============================================================================
// = MODULE VARIABLES
// ===============================================================================
int NUM_GL_TEXTURES = 0;
int SCRAP_UPLOADS = 0;

int GL_FILTER_MIN = GL_LINEAR_MIPMAP_LINEAR;
int GL_FILTER_MAX = GL_LINEAR;

bool SCRAP_DIRTY;

// ===============================================================================
// = ARRAYS
// ===============================================================================
std::array<Image, MAX_GLTEXTURES> gltextures;

Image* r_lblendimage = nullptr;
Image* r_particletexture[Particle::PT_MAX] = { '\0' };
Image* r_decaltexture[DECAL_MAX] = { '\0' };

unsigned d_8to24table[256];
unsigned scaled[4096 * 4096];

int SCRAP_ALLOCATED[MAX_SCRAPS][BLOCK_SIZE];
byte SCRAP_TEXELS[MAX_SCRAPS][BLOCK_SIZE * BLOCK_SIZE];

const char* PALETTE_STRINGS[2] = { "RGB", "PAL" };


/**
 * Gets the number of OpenGL textures in use.
 */
int numGlTextures()
{
	return NUM_GL_TEXTURES;
}


/**
 * Increments the gl texture count.
 */
void incrementGlTextures()
{
	NUM_GL_TEXTURES++;
}


/**
 * 
 */
bool texturesAtMax()
{
	return NUM_GL_TEXTURES == MAX_GLTEXTURES;
}


/**
 * 
 */
int glFilterMin()
{
	return GL_FILTER_MIN;
}


/**
 * 
 */
int glFilterMax()
{
	return GL_FILTER_MAX;
}


/**
 * Find an open image slot.
 */
size_t glGetFreeImageIndex()
{
	size_t index = 0;
	for (index; index < NUM_GL_TEXTURES; index++)
	{
		if (gltextures[index].texnum == 0)
		{
			break;
		}
	}

	if (index == numGlTextures())
	{
		if (texturesAtMax())
		{
			VID_Error(ERR_FATAL, "MAX_GLTEXTURES");
		}

		incrementGlTextures();
	}

	return index;
}


/**
 * 
 */
void R_CaptureColorBuffer()
{
	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL) return;
	GL_MBindRect(GL_TEXTURE0, ScreenMap->texnum);
	glCopyTexSubImage2D(GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth(), videoHeight());
}


/**
 * 
 */
void R_CaptureDepthBuffer()
{
	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL) return;
	GL_MBindRect(GL_TEXTURE0, depthMap->texnum);
	glCopyTexSubImage2D(GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth(), videoHeight());
}

#include <algorithm>

/**
 * 
 */
void GL_TextureMode(const std::string& string)
{
	std::string _textureMode = toUppercase(r_textureMode->string);

	if (_textureMode != "GL_NEAREST" &&
		_textureMode != "GL_LINEAR" &&
		_textureMode != "GL_NEAREST_MIPMAP_NEAREST" &&
		_textureMode != "GL_LINEAR_MIPMAP_NEAREST" &&
		_textureMode != "GL_NEAREST_MIPMAP_LINEAR" &&
		_textureMode != "GL_LINEAR_MIPMAP_LINEAR")
		{
			Cvar_Set("r_textureMode", "GL_NEAREST");
		}

	if (_textureMode == "GL_NEAREST")
	{
		GL_FILTER_MIN = GL_NEAREST;
		GL_FILTER_MAX = GL_NEAREST;
	}

	if (_textureMode == "GL_LINEAR")
	{
		GL_FILTER_MIN = GL_LINEAR;
		GL_FILTER_MAX = GL_LINEAR;
	}

	if (_textureMode == "GL_NEAREST_MIPMAP_NEAREST")
	{
		GL_FILTER_MIN = GL_NEAREST_MIPMAP_NEAREST;
		GL_FILTER_MAX = GL_NEAREST;
	}

	if (_textureMode == "GL_LINEAR_MIPMAP_NEAREST")
	{
		GL_FILTER_MIN = GL_LINEAR_MIPMAP_NEAREST;
		GL_FILTER_MAX = GL_LINEAR;
	}

	if (_textureMode == "GL_NEAREST_MIPMAP_LINEAR")
	{
		GL_FILTER_MIN = GL_NEAREST_MIPMAP_LINEAR;
		GL_FILTER_MAX = GL_NEAREST;
	}

	if (_textureMode == "GL_LINEAR_MIPMAP_LINEAR")
	{
		GL_FILTER_MIN = GL_LINEAR_MIPMAP_LINEAR;
		GL_FILTER_MAX = GL_LINEAR;
	}

		// change all the existing mipmap texture objects
	for (int i = 0; i < NUM_GL_TEXTURES; i++)
	{
		if (gltextures[i].type != IT_PIC &&gltextures[i].type != IT_SKY)
		{
			GL_Bind(gltextures[i].texnum);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_FILTER_MIN);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_FILTER_MAX);
			// realtime update anisotropy level
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, r_anisotropic->value);
		}
	}
}


/*
=============================================================================
  scrap allocation

  Allocate all the little status bar obejcts into a single texture
  to crutch up inefficient hardware / drivers
=============================================================================
*/

/**
 * Returns a texture number and the position inside it
 */
int Scrap_AllocBlock(int w, int h, int *x, int *y)
{
	int best = 0;
	int best2 = 0;

	int texnum = 0;
	for (texnum = 0; texnum < MAX_SCRAPS; texnum++)
	{
		best = BLOCK_SIZE;

		for (int i = 0; i < BLOCK_SIZE - w; i++)
		{
			best2 = 0;

			int j = 0;
			for (j = 0; j < w; j++)
			{
				if (SCRAP_ALLOCATED[texnum][i + j] >= best)
					break;
				if (SCRAP_ALLOCATED[texnum][i + j] > best2)
					best2 = SCRAP_ALLOCATED[texnum][i + j];
			}
			if (j == w) // this is a valid spot
			{
				*x = i;
				*y = best = best2;
			}
		}

		if (best + h > BLOCK_SIZE)
			continue;

		for (int i = 0; i < w; i++)
			SCRAP_ALLOCATED[texnum][*x + i] = best + h;

		return texnum;
	}

	return -1;
}


/**
 * 
 */
void GL_ResampleTexture (unsigned *in, int inwidth, int inheight, unsigned *out,  int outwidth, int outheight, bool normalMap)
{
	int			i, j;
	unsigned	*inrow, *inrow2;
	unsigned	frac, fracstep;
	unsigned	p1[4096], p2[4096];
	byte		*pix1, *pix2, *pix3, *pix4;

	fracstep = inwidth*0x10000/outwidth;

	frac = fracstep>>2;
	for (i=0 ; i<outwidth ; i++)
	{
		p1[i] = 4*(frac>>16);
		frac += fracstep;
	}
	frac = 3*(fracstep>>2);
	for (i=0 ; i<outwidth ; i++)
	{
		p2[i] = 4*(frac>>16);
		frac += fracstep;
	}

	if(normalMap)
	{
		float	inv127 = 1.0 / 127.0;
		vec3_t	n, n2, n3, n4;

		for(i = 0; i < outheight; i++, out += outwidth)
		{
			inrow = in + inwidth * (int)((i + 0.25) * inheight / outheight);
			inrow2 = in + inwidth * (int)((i + 0.75) * inheight / outheight);

			for(j = 0; j < outwidth; j++)
			{
				pix1 = (byte *) inrow + p1[j];
				pix2 = (byte *) inrow + p2[j];
				pix3 = (byte *) inrow2 + p1[j];
				pix4 = (byte *) inrow2 + p2[j];

				n[0] = (pix1[0] * inv127 - 1.0);
				n[1] = (pix1[1] * inv127 - 1.0);
				n[2] = (pix1[2] * inv127 - 1.0);

				n2[0] = (pix2[0] * inv127 - 1.0);
				n2[1] = (pix2[1] * inv127 - 1.0);
				n2[2] = (pix2[2] * inv127 - 1.0);

				n3[0] = (pix3[0] * inv127 - 1.0);
				n3[1] = (pix3[1] * inv127 - 1.0);
				n3[2] = (pix3[2] * inv127 - 1.0);

				n4[0] = (pix4[0] * inv127 - 1.0);
				n4[1] = (pix4[1] * inv127 - 1.0);
				n4[2] = (pix4[2] * inv127 - 1.0);

				n[0] += n2[0] + n3[0] + n4[0];
				n[1] += n2[1] + n3[1] + n4[1];
				n[2] += n2[2] + n3[2] + n4[2];

				if(!VectorNormalize(n))
					VectorSet(n, 0, 0, 1);

				((byte *) (out + j))[0] = (byte)(128 + 127 * n[0]);
				((byte *) (out + j))[1] = (byte)(128 + 127 * n[1]);
				((byte *) (out + j))[2] = (byte)(128 + 127 * n[2]);
				((byte *) (out + j))[3] = (pix1[3] + pix2[3] + pix3[3] + pix4[3])>>2;
			}
		}
	}
	else
	{
		for (i=0 ; i<outheight ; i++, out += outwidth)
		{
			inrow = in + inwidth*(int)((i+0.25)*inheight/outheight);
			inrow2 = in + inwidth*(int)((i+0.75)*inheight/outheight);
			for (j=0 ; j<outwidth ; j++)
			{
				pix1 = (byte *)inrow + p1[j];
				pix2 = (byte *)inrow + p2[j];
				pix3 = (byte *)inrow2 + p1[j];
				pix4 = (byte *)inrow2 + p2[j];
				((byte *)(out+j))[0] = (pix1[0] + pix2[0] + pix3[0] + pix4[0])>>2;
				((byte *)(out+j))[1] = (pix1[1] + pix2[1] + pix3[1] + pix4[1])>>2;
				((byte *)(out+j))[2] = (pix1[2] + pix2[2] + pix3[2] + pix4[2])>>2;
				((byte *)(out+j))[3] = (pix1[3] + pix2[3] + pix3[3] + pix4[3])>>2;
			}
		}
	}
}


/**
 * 
 */
void R_MipMap (byte *in, int width, int height)
{
	int		i, j;
	byte	*out;
	int		row;

	if ( width == 1 && height == 1 )
		return;

	row = width * 4;
	out = in;
	width >>= 1;
	height >>= 1;

	if ( width == 0 || height == 0 )
	{
		width += height;	// get largest
		for (i=0 ; i<width ; i++, out+=4, in+=8 )
		{
			out[0] = ( in[0] + in[4] )>>1;
			out[1] = ( in[1] + in[5] )>>1;
			out[2] = ( in[2] + in[6] )>>1;
			out[3] = ( in[3] + in[7] )>>1;
		}
		return;
	}

	for (i=0 ; i<height ; i++, in+=row)
	{
		for (j=0 ; j<width ; j++, out+=4, in+=8)
		{
			out[0] = (in[0] + in[4] + in[row+0] + in[row+4])>>2;
			out[1] = (in[1] + in[5] + in[row+1] + in[row+5])>>2;
			out[2] = (in[2] + in[6] + in[row+2] + in[row+6])>>2;
			out[3] = (in[3] + in[7] + in[row+3] + in[row+7])>>2;
		}
	}
}


/**
 * 
 */
void R_MipNormalMap(byte *in, int width, int height)
{
	int		i, j;
	byte	*out;
	vec3_t	n;
	float	length, inv127 = 1.0f / 127.0f;

	if (width == 1 && height == 1)
		return;

	out = in;
	width <<= 2;
	height >>= 1;

	for (i = 0; i < height; i++, in += width)
	{
		for (j = 0; j < width; j += 8, out += 4, in += 8)
		{
			n[0] = (inv127 * in[0] - 1.0) +
				(inv127 * in[4] - 1.0) +
				(inv127 * in[width + 0] - 1.0) +
				(inv127 * in[width + 4] - 1.0);

			n[1] = (inv127 * in[1] - 1.0) +
				(inv127 * in[5] - 1.0) +
				(inv127 * in[width + 1] - 1.0) +
				(inv127 * in[width + 5] - 1.0);

			n[2] = (inv127 * in[2] - 1.0) +
				(inv127 * in[6] - 1.0) +
				(inv127 * in[width + 2] - 1.0) +
				(inv127 * in[width + 6] - 1.0);

			length = VectorLength(n);

			if (length)
			{
				n[0] /= length;
				n[1] /= length;
				n[2] /= length;
			}
			else
				VectorSet(n, 0.0, 0.0, 1.0);

			out[0] = (byte)(128 + 127 * n[0]);
			out[1] = (byte)(128 + 127 * n[1]);
			out[2] = (byte)(128 + 127 * n[2]);
			out[3] = (in[3] + in[7] + in[width + 3] + in[width + 7]) >> 2;
		}
	}
}


/**
 * 
 */
bool GL_Upload32(unsigned *data, int width, int height, bool mipmap, bool bump)
{
	// scan the texture for any non-255 alpha
	int c = width * height;
	byte* scan = ((byte *)data) + 3;
	int samples = 3; ///\fixme magic numbers
	for (int i = 0; i < c; i++, scan += 4)
	{
		if (*scan != 255)
		{
			samples = 4;
			break;
		}
	}

	int scaled_width = 0, scaled_height = 0;
	for (scaled_width = 1; scaled_width < width; scaled_width <<= 1);
	for (scaled_height = 1; scaled_height < height; scaled_height <<= 1);

	int max_size = 0;
	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &max_size);

	if (mipmap)
	{
		if (r_maxTextureSize->value >= max_size)
			Cvar_SetValue("r_maxTextureSize", max_size);

		if (r_maxTextureSize->value <= 64 && r_maxTextureSize->value > 0)
			Cvar_SetValue("r_maxTextureSize", 64);

		if (r_maxTextureSize->value)
			max_size = (int)r_maxTextureSize->value;
	}

	if (scaled_width > max_size)
		scaled_width = max_size;
	if (scaled_height > max_size)
		scaled_height = max_size;

	if (scaled_width < 1)
		scaled_width = 1;
	if (scaled_height < 1)
		scaled_height = 1;

	int comp = 0;
	if (samples == 3)
	{
		if (gl_state.texture_compression_bptc && mipmap)
			comp = GL_COMPRESSED_RGBA_BPTC_UNORM_ARB;
		else
			comp = GL_RGB;
	}

	if (samples == 4)
	{
		if (gl_state.texture_compression_bptc && mipmap)
			comp = GL_COMPRESSED_RGBA_BPTC_UNORM_ARB;
		else
			comp = GL_RGBA;
	}


	if (scaled_width == width && scaled_height == height)
	{
		if (!mipmap)
		{
			glTexImage2D(GL_TEXTURE_2D, 0, comp, scaled_width, scaled_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
			goto done;
		}
		memcpy(scaled, data, width*height * 4);
	}
	else
	{
		if (bump)
			GL_ResampleTexture(data, width, height, scaled, scaled_width, scaled_height, true);
		else
			GL_ResampleTexture(data, width, height, scaled, scaled_width, scaled_height, false);
	}
	glTexImage2D(GL_TEXTURE_2D, 0, comp, scaled_width, scaled_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, scaled);

	if (mipmap)
	{
		int		miplevel;

		miplevel = 0;
		while (scaled_width > 1 || scaled_height > 1)
		{
			if (bump)
				R_MipNormalMap((byte *)scaled, scaled_width, scaled_height);
			else
				R_MipMap((byte *)scaled, scaled_width, scaled_height);

			scaled_width >>= 1;
			scaled_height >>= 1;
			if (scaled_width < 1)
				scaled_width = 1;
			if (scaled_height < 1)
				scaled_height = 1;
			miplevel++;
			glTexImage2D(GL_TEXTURE_2D, miplevel, comp, scaled_width, scaled_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, scaled);
		}
	}

done:
	if (mipmap)
	{
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, r_anisotropic->value);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_FILTER_MIN);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_FILTER_MAX);
	}
	else
	{
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_FILTER_MAX);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_FILTER_MAX);
	}

	return (samples == 4);
}


/**
 * Returns has_alpha
 * 
 * \todo	Decide if 8bit textures will remain supported.
 */
bool GL_Upload8(byte* data, int width, int height, bool mipmap, bool is_sky)
{
	unsigned trans[512 * 256];	///\fixme MAGIC NUMBERS

	int img_size = width * height;
	if (img_size > sizeof(trans) / 4)
		VID_Error(ERR_DROP, S_COLOR_MAGENTA "GL_Upload8():: " S_COLOR_WHITE " Image dimensions too large (%ix%i)", width, height);

	for (int i = 0; i < img_size; i++)
	{
		int p = data[i];
		trans[i] = d_8to24table[p];

		if (p == 255)	/// transparent, so scan around for another color to avoid alpha fringes
		{				/// \fixme do a full flood fill so mips work...
			if (i > width && data[i - width] != 255)
				p = data[i - width];
			else if (i < img_size - width && data[i + width] != 255)
				p = data[i + width];
			else if (i > 0 && data[i - 1] != 255)
				p = data[i - 1];
			else if (i < img_size - 1 && data[i + 1] != 255)
				p = data[i + 1];
			else
				p = 0;
			// copy rgb components
			((byte*)& trans[i])[0] = ((byte*)& d_8to24table[p])[0];
			((byte*)& trans[i])[1] = ((byte*)& d_8to24table[p])[1];
			((byte*)& trans[i])[2] = ((byte*)& d_8to24table[p])[2];
		}
	}

	return GL_Upload32(trans, width, height, mipmap, false);
}


/**
 * 
 */
void Scrap_Upload()
{
	if (!SCRAP_DIRTY)
		return;

	SCRAP_UPLOADS++;
	GL_Bind(TEXNUM_SCRAPS);
	GL_Upload8(SCRAP_TEXELS[0], BLOCK_SIZE, BLOCK_SIZE, false, false);
	SCRAP_DIRTY = false;
}


/**
 * 
 */
Image* GL_LoadPic(const char* name, byte* pic, int width, int height, ImageType type, int bits)
{
	int index = glGetFreeImageIndex();
	Image* image = &gltextures[index];

	image->alpha = 1.0f;

	image->name = name;
	image->registration_sequence = R_RegistrationSequence();

	image->width = width;
	image->height = height;
	image->type = type;

	// load little pics into the scrap
	if (image->type == IT_PIC && bits == 8 && image->width < 64 && image->height < 64)
	{
		int x, y;

		int texnum = Scrap_AllocBlock(image->width, image->height, &x, &y);
		if (texnum == -1) { goto nonscrap; } /// \fixme	This is absolutely unacceptable, fix this.

		SCRAP_DIRTY = true;

		// copy the texels into the scrap block
		for (int i = 0; i < image->height; i++)
		{
			for (int j = 0, k = 0; j < image->width; j++, k++)
			{
				SCRAP_TEXELS[texnum][(y + i) * BLOCK_SIZE + x + j] = pic[k];
			}
		}

		image->texnum = TEXNUM_SCRAPS + texnum;
		image->scrap = true;
		image->has_alpha = true;
		image->sl = (x + 0.01) / static_cast<float>(BLOCK_SIZE);
		image->sh = (x + image->width - 0.01) / static_cast<float>(BLOCK_SIZE);
		image->tl = (y + 0.01) / static_cast<float>(BLOCK_SIZE);
		image->th = (y + image->height - 0.01) / static_cast<float>(BLOCK_SIZE);
	}
	else
	{
	nonscrap: /// \fixme EW!
		image->scrap = false;
		image->texnum = TEXNUM_IMAGES + index;
		GL_Bind(image->texnum);

		if (bits == 8)
		{
			image->has_alpha = GL_Upload8(pic, width, height, (image->type != IT_PIC && image->type != IT_SKY), image->type == IT_SKY);
		}
		else
		{
			image->has_alpha = GL_Upload32((unsigned *)pic, width, height, (image->type != IT_PIC && image->type != IT_SKY), image->type == IT_BUMP);
		}

		image->upload_width = width;
		image->upload_height = height;

		image->sl = 0;
		image->sh = 1;
		image->tl = 0;
		image->th = 1;
	}

	return image;
}


/*=====================
DevIL Stuff
=====================*/

/**
 * 
 */
void LoadImageErrors()
{
	ILenum Error;
	char message[2048];

	while ((Error = ilGetError()) != IL_NO_ERROR)
	{
		memset(message, 0, 2048);
		sprintf(message, "%d: %s", Error, iluErrorString(Error));
		Com_Printf("%s\n", message);
		//Con_Printf(PRINT_DEVELOPER, "%s", message);
	}
}


/**
 * Loads an image file from disk.
 */
void IL_LoadImage(const char* filename, byte* *pic, int *width, int *height, ILenum type)
{
	unsigned char *buffer = nullptr;

	if (!FS_FileExists(filename))
		return;

	int length = FS_LoadFile(filename, reinterpret_cast<void**>(&buffer));
	if (!buffer)
	{
		Con_Printf(PRINT_DEVELOPER, "Bad image file %s\n", filename);
		return;
	}

	if (!length)
	{
		FS_FreeFile(buffer);
		Con_Printf(PRINT_DEVELOPER, "Bad image file %s\n", filename);
		return;
	}

	ILuint imageID;
	ilGenImages(1, &imageID);
	ilBindImage(imageID);

	if (!ilLoadL(type, (ILvoid *)buffer, (ILint)length))
	{
		FS_FreeFile(buffer);
		free(buffer);
		LoadImageErrors();
		return;
	}

	if (!ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE))
	{
		FS_FreeFile(buffer);
		LoadImageErrors();
		ilDeleteImages(1, &imageID);
		return;
	}

	signed int w = ilGetInteger(IL_IMAGE_WIDTH);
	signed int h = ilGetInteger(IL_IMAGE_HEIGHT);
	ILubyte* image = ilGetData();

	unsigned char* buf = (unsigned char*)malloc(w * h * 4);
	memcpy(buf, image, w * h * 4);
	*pic = buf;

	ilDeleteImages(1, &imageID);
	FS_FreeFile(buffer);

	*width = w;
	*height = h;
	return;
}


/**
 * Loads an image from a memory buffer.
 * 
 * \see IL_LoadImage
 */
void IL_LoadImageBuf(char* buffer, int buff_len, byte* *pic, int* width, int* height, ILenum type)
{
	if (!buffer)
	{
		Con_Printf(PRINT_DEVELOPER, "IL_LoadImageBuff():: empty buffer.");
		return;
	}

	ILuint imageID;
	ilGenImages(1, &imageID);
	ilBindImage(imageID);

	if (!ilLoadL(type, (ILvoid*)buffer, (ILint)buff_len))
	{
		LoadImageErrors();
		return;
	}

	if (!ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE))
	{
		FS_FreeFile(buffer);
		LoadImageErrors();
		ilDeleteImages(1, &imageID);
		return;
	}

	*width = ilGetInteger(IL_IMAGE_WIDTH);
	*height = ilGetInteger(IL_IMAGE_HEIGHT);
	ILubyte* image = ilGetData();

	unsigned char* buf = (unsigned char*)malloc((*width) * (*height) * 4);
	memcpy(buf, image, (*width) * (*height) * 4);
	*pic = buf;

	ilDeleteImages(1, &imageID);

	return;
}


/**
 * Gets a pointer to an \c Image structure.
 * 
 * First searches to see if the image has already been loaded.
 * If it has, returns pointer to the already loaded image. Otherwise
 * will attempt to load the image from disk.
 * 
 * \param	name	Name of the image.
 * \param	type	Type of the texture. See ImageType.
 * \param	load	If the image wasn't already loaded, load it.
 * 
 * \return	Returns a pointer to an \c Image. Will return nullptr
 *			if the image wasn't found or couldn't be loaded.
 */
Image* GL_FindImage(const std::string& name, ImageType type, bool load)
{
	if (name.empty())
	{
		Con_Printf(PRINT_DEVELOPER, "%s: empty file name passed.\n", name);
		return nullptr;
	}

	// look for it
	for (int i = 0; i < NUM_GL_TEXTURES; i++)
	{
		if (gltextures[i].name == name)
		{
			gltextures[i].registration_sequence = R_RegistrationSequence();
			return &gltextures[i];
		}
	}

	if (!load) { return nullptr; }

	// load the pic from disk
	byte* pic = nullptr;

	Image* image = nullptr;
	int width = 0, height = 0;
	if (name.substr(name.size() - 4) == ".tga")
	{
		IL_LoadImage(name.c_str(), &pic, &width, &height, IL_TGA);
		if (!pic) { return nullptr; }
		image = GL_LoadPic(name.c_str(), pic, width, height, type, 32);
	}
	else if (name.substr(name.size() - 4) == ".dds")
	{
		IL_LoadImage(name.c_str(), &pic, &width, &height, IL_DDS);
		if (!pic) { return nullptr; }
		image = GL_LoadPic(name.c_str(), pic, width, height, type, 32);
	}
	else if (name.substr(name.size() - 4) == ".jpg" || (name.size() > 4 && name.substr(name.size() - 5) == ".jpeg")) // fragile
	{
		IL_LoadImage(name.c_str(), &pic, &width, &height, IL_JPG);
		if (!pic) { return nullptr; }
		image = GL_LoadPic(name.c_str(), pic, width, height, type, 24);
	}
	else if (name.substr(name.size() - 4) == ".png")
	{
		IL_LoadImage(name.c_str(), &pic, &width, &height, IL_PNG);
		if (!pic) { return nullptr; }
		image = GL_LoadPic(name.c_str(), pic, width, height, type, 32);
	}
	else
	{
		Com_DPrintf(S_COLOR_MAGENTA "%s: " S_COLOR_WHITE "unsupported texture format.\n", name.c_str());
		return nullptr;
	}

	free(pic);

	return image;
}


/**
 * 
 */
Image* R_RegisterSkin(const std::string& name)
{
	return GL_FindImage(name, IT_SKIN, true);
}


/**
 * 
 */
Image* R_RegisterPlayerBump(const std::string& name)
{
	std::string skin_normal = name.substr(0, name.size() - 4) + "_n.tga";
	Image* img = GL_FindImage(skin_normal, IT_SKIN, true);

	if (!img) { return r_defBump; }

	return img;
}


/**
 * 
 */
static void clearTextureArray(Image* *_imgArr, int _s)
{
	if (!(*_imgArr))
	{
		Com_DPrintf(S_COLOR_RED "clearTextureArray(): " S_COLOR_WHITE "Attempting to free an empty image array.\n");
		return;
	}

	for (int i = 0; i < _s; i++, *_imgArr++)
	{
		(*_imgArr)->registration_sequence = R_RegistrationSequence();
	}
}


/*
 * Any image that was not touched on this registration sequence will be freed.
 */
void GL_FreeUnusedImages()
{
	r_notexture->registration_sequence = R_RegistrationSequence();
	r_missingTexture->registration_sequence = R_RegistrationSequence();
	r_flare->registration_sequence = R_RegistrationSequence();

	clearTextureArray(r_particletexture, Particle::PT_MAX);
	clearTextureArray(r_caustic, MAX_CAUSTICS);
	clearTextureArray(r_waterNormals, MAX_WATER_NORMALS);
	clearTextureArray(fly, MAX_FLY);
	clearTextureArray(flameanim, MAX_FLAMEANIM);
	clearTextureArray(r_blood, MAX_BLOOD);
	clearTextureArray(r_explode, MAX_EXPLODE);
	clearTextureArray(r_decaltexture, DECAL_MAX);
	clearTextureArray(r_texshell, MAX_SHELLS);
	clearTextureArray(r_lightCubeMap, MAX_GLOBAL_FILTERS);

	r_distort->registration_sequence = R_RegistrationSequence();
	r_defBump->registration_sequence = R_RegistrationSequence();
	r_scanline->registration_sequence = R_RegistrationSequence();
	r_envTex->registration_sequence = R_RegistrationSequence();
	r_randomNormalTex->registration_sequence = R_RegistrationSequence();
	r_whiteMap->registration_sequence = R_RegistrationSequence();
	skinBump->registration_sequence = R_RegistrationSequence();

	fboDN->registration_sequence = R_RegistrationSequence();

	for (int i = 0; i < 2; i++)
	{
		fboColor[i]->registration_sequence = R_RegistrationSequence();
	}

	for (int i = 0; i < NUM_GL_TEXTURES; i++)
	{
		if (gltextures[i].registration_sequence == R_RegistrationSequence()) { continue; }	// used this sequence
		if (!gltextures[i].registration_sequence) { continue; }								// free Image slot
		if (gltextures[i].type == IT_PIC) { continue; }										// don't free pics

		// free it
		glDeleteTextures(1, &gltextures[i].texnum);
		gltextures[i].clear();
	}
}


/**
 * 
 */
void GL_InitImages()
{
	R_SetRegistrationSequence(1);
}


/**
 * 
 */
void GL_ShutdownImages()
{
	for (int i = 0; i < NUM_GL_TEXTURES; i++)
	{
		if (gltextures[i].registration_sequence == 0) { continue; }

		glDeleteTextures(1, &gltextures[i].texnum);
		gltextures[i].clear();
	}

	// Berserker's fix for old Q2 bug: free lightmaps
	if (gl_lms.current_lightmap_texture) // no dynamic lightmap
	{
		GLuint ids[MAX_LIGHTMAPS * 3];

		int i = 0;
		for (i; i < gl_lms.current_lightmap_texture; i++)
		{
			ids[i * 3 + 0] = TEXNUM_LIGHTMAPS + i + 1;

			// FIXME: include XPLM ones only when necessary
			ids[i * 3 + 1] = TEXNUM_LIGHTMAPS + i + 1 + MAX_LIGHTMAPS;
			ids[i * 3 + 2] = TEXNUM_LIGHTMAPS + i + 1 + MAX_LIGHTMAPS * 2;
		}

		glDeleteTextures(i * 3, ids);
	}


	/// \todo use image array
	if (thermaltex)
	{
		glDeleteTextures(1, &thermaltex);
	}
	if (bloomtex)
	{
		glDeleteTextures(1, &bloomtex);
	}
	if (fxaatex)
	{
		glDeleteTextures(1, &fxaatex);
	}
	if (fovCorrTex)
	{
		glDeleteTextures(1, &fovCorrTex);
	}
}
