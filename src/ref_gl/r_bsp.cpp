#include "r_bsp.h"

#include "r_image.h"
#include "r_light.h"

#include "r_material.h"
#include "r_model.h"
#include "r_program.h"

#include "r_util.h"

#include "../client/client.h"

#include "../common/string_util.h"


// ===============================================================================
// = DEFINES
// ===============================================================================
#define DEFAULT_LIGHTMAP_SCALE			16;

// ===============================================================================
// = CONSTANTS
// ===============================================================================


// ===============================================================================
// = IMPORTS
// ===============================================================================
void GL_BeginBuildingLightmaps(Model* m);
void GL_EndBuildingLightmaps();
void GL_CreateSurfaceLightmap(msurface_t* surf);

// ===============================================================================
// = MODULE VARIABLES
// ===============================================================================
byte MOD_NOVIS[MAX_MAP_LEAFS / 8];
temp_connect_t* TEMP_EDGES = nullptr;
byte* MOD_BASE = nullptr;		/**<  */

int BSP_SIZE;
int ALIAS_SIZE;
int SPRITE_SIZE;

extern bool RELIGHT_MAP;

int* XPLM_OFFSETS = nullptr; // face light offsets from .xplm file, freed after level is loaded


static byte* _cacheData;
static int _cachePos, _cacheSize;


/**
 * 
 */
typedef struct
{
	vec3_t origin;
	vec3_t color;
	float outcolor[4];
	float size;
	float sizefull;

	int style;

	float lightIntens;
	msurface_t *surf;
	vec3_t lightsurf_origin;
	bool ignore;

} autoLight_t;

autoLight_t r_lightSpawnSurf[MAX_WORLD_SHADOW_LIGHTS];



static void _cacheClose();
static bool _cacheFetch(void *dst, int size);
static bool _cacheOpen(const char* name);
void _rebuildCache(int count, int ci, float* vi);


// ===============================================================================
// = INTERNAL FUNCTIONS
// ===============================================================================
/**
 * 
 */
void _addLightFromSurface(msurface_t* surf)
{
	int i, width = 0, height = 0;
	glpoly_t* poly = nullptr;
	byte* buffer = nullptr;
	byte* p = nullptr;
	float* v = nullptr, surf_bound = 0.0f;
	vec3_t origin = { 0, 0, 0 }, color = { 1, 1, 1 }, tmp, rgbSum;
	vec3_t poly_center, mins, maxs, tmp1, lightOffset, radius;
	char target[MAX_QPATH];

	if (surf->texInfo->flags & (SURF_SKY | SURF_ALPHA | SURF_FLOWING | MSURF_DRAWTURB | SURF_WARP))
		return;
	if (!(surf->texInfo->flags & (SURF_LIGHT)))
		return;
	if (r_numAutoLights >= MAX_WORLD_SHADOW_LIGHTS)
		return;

	int intens = surf->texInfo->value;
	if (intens <= 1000) return;

	r_lightSpawnSurf[r_numAutoLights].lightIntens = intens;

	// =================== find polygon center ===================
	VectorSet(mins, 999999, 999999, 999999);
	VectorSet(maxs, -999999, -999999, -999999);

	for (poly = surf->polys; poly; poly = poly->chain)
	{
		for (i = 0, v = poly->verts[0]; i < poly->numVerts; i++, v += VERTEXSIZE)
		{
			if (v[0] > maxs[0]) maxs[0] = v[0];
			if (v[1] > maxs[1]) maxs[1] = v[1];
			if (v[2] > maxs[2]) maxs[2] = v[2];

			if (v[0] < mins[0]) mins[0] = v[0];
			if (v[1] < mins[1]) mins[1] = v[1];
			if (v[2] < mins[2]) mins[2] = v[2];
		}
	}

	poly_center[0] = (mins[0] + maxs[0]) / 2;
	poly_center[1] = (mins[1] + maxs[1]) / 2;
	poly_center[2] = (mins[2] + maxs[2]) / 2;
	VectorCopy(poly_center, origin);

	// =======calc light surf bounds and light size ==========
	VectorSubtract(maxs, mins, tmp1);
	surf_bound = VectorLength(tmp1);

	/// \fixme	This can be replaced with a table lookup instead of of an if/else chain
	if (surf_bound <= 25) r_lightSpawnSurf[r_numAutoLights].size = 10;
	else if (surf_bound <= 50) r_lightSpawnSurf[r_numAutoLights].size = 15;
	else if (surf_bound <= 100) r_lightSpawnSurf[r_numAutoLights].size = 20;
	else if (surf_bound <= 150) r_lightSpawnSurf[r_numAutoLights].size = 25;
	else if (surf_bound <= 200) r_lightSpawnSurf[r_numAutoLights].size = 30;
	else if (surf_bound <= 250) r_lightSpawnSurf[r_numAutoLights].size = 35;

	r_lightSpawnSurf[r_numAutoLights].sizefull = surf_bound;

	// =================== calc texture color ===================
	GL_Bind(surf->texInfo->diffuse->texnum);
	width = surf->texInfo->diffuse->upload_width;
	height = surf->texInfo->diffuse->upload_height;

	buffer = (byte*)malloc(width * height * 3);
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_UNSIGNED_BYTE, buffer);
	VectorClear(rgbSum);

	for (i = 0, p = buffer; i < width * height; i++, p += 3)
	{
		rgbSum[0] += (float)p[0] * (1.0 / 255);
		rgbSum[1] += (float)p[1] * (1.0 / 255);
		rgbSum[2] += (float)p[2] * (1.0 / 255);
	}

	VectorScale(rgbSum, r_scaleAutoLightColor->value / (width * height), color);

	for (i = 0; i < 3; i++)
	{
		if (color[i] < 0.5) color[i] = color[i] * 0.5;
		else color[i] = color[i] * 0.5 + 0.5;
	}

	VectorCopy(color, r_lightSpawnSurf[r_numAutoLights].color);

	// ============== move light origin in to map bounds ============
	if (surf->flags & MSURF_PLANEBACK)
		VectorNegate(surf->plane->normal, tmp);
	else
		VectorCopy(surf->plane->normal, tmp);

	VectorScaleAndAdd(origin, tmp, origin, 2);
	VectorScaleAndAdd(origin, tmp, lightOffset, 10);

	VectorCopy(origin, r_lightSpawnSurf[r_numAutoLights].origin);
	VectorCopy(tmp, r_lightSpawnSurf[r_numAutoLights].lightsurf_origin);
	r_lightSpawnSurf[r_numAutoLights].surf = surf;
	r_lightSpawnSurf[r_numAutoLights].style = 0;

	///\fixme Magic number (10.0f)
	VectorSet(radius, r_lightSpawnSurf[r_numAutoLights].size * 10.0, r_lightSpawnSurf[r_numAutoLights].size * 10.0, r_lightSpawnSurf[r_numAutoLights].size * 10.0);

	memset(target, 0, sizeof(target));

	R_AddNewWorldLight(lightOffset, r_lightSpawnSurf[r_numAutoLights].color, radius, 0, 0, vec3_origin, vec3_origin, true, 1, 0, 0, false, 1, origin, 10.0, target, 0, 0, 0.0);

	r_numAutoLights++;
	free(buffer);
}


/**
 * 
 */
static void _buildPolygonFromSurface(msurface_t* fa)
{
	fa->numVertices = fa->numEdges;
	fa->numIndices = (fa->numVertices - 2) * 3;

	// reconstruct the polygon
	medge_t* pEdges = CURRENT_MODEL->edges;
	int lNumVerts = fa->numEdges;
	int vertPage = 0;

	// draw texture  <-- ?
	glpoly_t* poly = (glpoly_t*)Hunk_Alloc(sizeof(glpoly_t) + (lNumVerts - 4) * VERTEXSIZE * sizeof(float));
	poly->next = fa->polys;
	poly->flags = fa->flags;
	poly->numVerts = lNumVerts;
	poly->neighbours = (glpoly_t **)Hunk_Alloc(lNumVerts * 4);
	fa->polys = poly;

	CURRENT_MODEL->memorySize += sizeof(glpoly_t) + (lNumVerts - 4) * VERTEXSIZE * sizeof(float);

	vec3_t total;
	VectorClear(total);

	// reserve space for neighbour pointers
	// FIXME: pointers don't need to be 4 bytes
	for (int i = 0; i < lNumVerts; i++)
	{
		int lIndex = CURRENT_MODEL->surfEdges[fa->firstedge + i];
		medge_t* r_pEdge = nullptr;
		float* vec = nullptr;

		if (lIndex > 0)
		{
			r_pEdge = &pEdges[lIndex];
			vec = CURRENT_MODEL->vertexes[r_pEdge->v[0]].position;
		}
		else
		{
			r_pEdge = &pEdges[-lIndex];
			vec = CURRENT_MODEL->vertexes[r_pEdge->v[1]].position;
		}
		
		float s = DotProduct(vec, fa->texInfo->vecs[0]) + fa->texInfo->vecs[0][3];
		s /= fa->texInfo->diffuse->width;

		float t = DotProduct(vec, fa->texInfo->vecs[1]) + fa->texInfo->vecs[1][3];
		t /= fa->texInfo->diffuse->height;

		VectorAdd(total, vec, total);
		VectorCopy(vec, poly->verts[i]);
		poly->verts[i][3] = s;
		poly->verts[i][4] = t;

		// lightmap texture coordinates
		s = DotProduct(vec, fa->texInfo->vecs[0]) + fa->texInfo->vecs[0][3];
		s -= fa->texturemins[0];
		s += fa->light_s * (float)Mod_LoadingModel()->lightmap_scale;
		s += (float)Mod_LoadingModel()->lightmap_scale / 2.0;
		s /= LIGHTMAP_SIZE * (float)Mod_LoadingModel()->lightmap_scale;

		t = DotProduct(vec, fa->texInfo->vecs[1]) + fa->texInfo->vecs[1][3];
		t -= fa->texturemins[1];
		t += fa->light_t * (float)Mod_LoadingModel()->lightmap_scale;
		t += (float)Mod_LoadingModel()->lightmap_scale / 2.0;
		t /= LIGHTMAP_SIZE * (float)Mod_LoadingModel()->lightmap_scale;

		poly->verts[i][5] = s;
		poly->verts[i][6] = t;

		// Store edge data for shadow volumes
		temp_connect_t* tempEdge = TEMP_EDGES + abs(lIndex);
		if (tempEdge->used < 2)
		{
			tempEdge->poly[tempEdge->used] = poly;
			tempEdge->used++;
		}
		else
			Com_DPrintf("_buildPolygonFromSurface(): Edge used by more than 2 surfaces\n");
	}

	poly->numVerts = lNumVerts;

	VectorScale(total, 1.0f / (float)lNumVerts, total);

	fa->c_s = (DotProduct(total, fa->texInfo->vecs[0]) + fa->texInfo->vecs[0][3]) / fa->texInfo->diffuse->width;
	fa->c_t = (DotProduct(total, fa->texInfo->vecs[1]) + fa->texInfo->vecs[1][3]) / fa->texInfo->diffuse->height;
}


/**
 * Setup the neighbor pointers of this surface's polygon.
 */
static void _buildSurfaceNeighbors(msurface_t *surf)
{
	int				i, j, lindex;
	temp_connect_t	*tempEdge;

	if (surf->numEdges > MAX_POLY_VERT)
		Com_DPrintf("BuildSurfaceNeighbors: too many edges %i\n", surf->numEdges);

	for (i = 0; i < surf->numEdges; i++)
	{
		lindex = CURRENT_MODEL->surfEdges[surf->firstedge + i];
		tempEdge = TEMP_EDGES + abs(lindex);

		surf->polys->neighbours[i] = nullptr;
		for (j = 0; j < tempEdge->used; j++)
		{
			if (tempEdge->poly[j] != surf->polys)
				surf->polys->neighbours[i] = tempEdge->poly[j];
		}
	}
}

/**
 * 
 */
void _buildTangentSpaceMatrix(int count)
{
	int ci = 0;
	int smoothAng = (int)r_tbnSmoothAngle->value;

	// Check for existing data
	char cacheName[MAX_QPATH];
	Q_sprintf(cacheName, sizeof(cacheName), "cache/%s", CURRENT_MODEL->name);
	if(_cacheOpen(cacheName))
	{
		int angle;

		if (!_cacheFetch(&angle, sizeof(angle)) || angle != smoothAng)
		{
			Com_Printf(S_COLOR_RED "GL_BuildTangentSpaceMatrix(): ignoring data for %s with angle %d (need %d)\n", cacheName, angle, smoothAng);
			_cacheClose();
			_rebuildCache(count, 0, nullptr);
			return;
		}

		for (int i = 0; i < count; i++)
		{
			msurface_t* si = &CURRENT_MODEL->surfaces[i];

			if (si->texInfo->flags & (SURF_SKY | SURF_NODRAW))
				continue;

			float* vi = si->polys->verts[0];

			for (ci = 0; ci < si->numEdges; ci++, vi += VERTEXSIZE)
			{
				if (!_cacheFetch(vi + 7, 9 * sizeof(*vi)))
				{
					Com_Printf(S_COLOR_RED "GL_BuildTBN: insufficient data in %s\n", cacheName);
					_cacheClose();
					_rebuildCache(count, ci, vi);
					return;
				}
			}
		}
		Com_DPrintf(S_COLOR_MAGENTA "GL_BuildTBN:" S_COLOR_WHITE " using cached data from %s\n", cacheName);
		_cacheClose();
		return;
	}

	_rebuildCache(count, ci, nullptr);
}


/**
 * 
 */
static void _cacheClose()
{
	if (!_cacheData)
		return;

	FS_FreeFile(_cacheData);
	_cacheData = nullptr;
}


/**
 * 
 */
static bool _cacheFetch(void *dst, int size)
{
	if (_cacheSize - _cachePos < size)
	{
		return false;
	}
	else
	{
		memcpy(dst, _cacheData + _cachePos, size);
		_cachePos += size;
		return true;
	}
}


/**
 * 
 */
static bool _cacheOpen(const char* name)
{
	_cacheSize = FS_LoadFile(name, reinterpret_cast<void**>(&_cacheData));
	_cachePos = 0;
	return (_cacheData != nullptr);
}


/**
 * 
 */
static void _calcBspIndices(msurface_t* surf)
{
	surf->numIndices = (surf->numVertices - 2) * 3;
	surf->indices = (index_t*)Hunk_Alloc(surf->numIndices * sizeof(int));

	for (int i = 0, index = 2; i < surf->numIndices; i += 3, index++)
	{
		surf->indices[i + 0] = 0;
		surf->indices[i + 1] = index - 1;
		surf->indices[i + 2] = index;
	}
}


/**
 * 
 */
static void _calcSurfaceBounds(msurface_t* surf)
{
	vec3_t mins, maxs;

	if (surf->polys)
	{
		mins[0] = mins[1] = mins[2] = 999999;
		maxs[0] = maxs[1] = maxs[2] = -999999;

		for (glpoly_t* p = surf->polys; p; p = p->next)
		{
			float* v = p->verts[0];
			for (int i = 0; i < p->numVerts; i++)
			{
				v += VERTEXSIZE;
				for (int j = 0; j < 3; j++)
				{
					if (mins[j] > v[j]) mins[j] = v[j];
					if (maxs[j] < v[j]) maxs[j] = v[j];
				}
			}
		}

		VectorCopy(mins, surf->mins); /// \fixme Replace with an actual function.
		VectorCopy(maxs, surf->maxs); /// \fixme Replace with an actual function.
	}
}


/**
 * Fills in s->texturemins[] and s->extents[]
 */
static void _calcSurfaceExtents(msurface_t* s)
{
	float mins[2], maxs[2], val;
	int i, j, e;
	mvertex_t *v;
	mtexInfo_t *tex;
	int bmins[2], bmaxs[2];

	mins[0] = mins[1] = 999999;
	maxs[0] = maxs[1] = -99999;

	tex = s->texInfo;

	for (i = 0; i < s->numEdges; i++) {
		e = Mod_LoadingModel()->surfEdges[s->firstedge + i];
		if (e >= 0)
			v = &Mod_LoadingModel()->vertexes[Mod_LoadingModel()->edges[e].v[0]];
		else
			v = &Mod_LoadingModel()->vertexes[Mod_LoadingModel()->edges[-e].v[1]];

		for (j = 0; j < 2; j++) {
			val = v->position[0] * tex->vecs[j][0] +
				v->position[1] * tex->vecs[j][1] +
				v->position[2] * tex->vecs[j][2] + tex->vecs[j][3];
			if (val < mins[j])
				mins[j] = val;
			if (val > maxs[j])
				maxs[j] = val;
		}
	}

	for (i = 0; i < 2; i++)
	{
		bmins[i] = floor(mins[i] / Mod_LoadingModel()->lightmap_scale);
		bmaxs[i] = ceil(maxs[i] / Mod_LoadingModel()->lightmap_scale);

		s->texturemins[i] = bmins[i] * Mod_LoadingModel()->lightmap_scale;
		s->extents[i] = (bmaxs[i] - bmins[i]) * Mod_LoadingModel()->lightmap_scale;
	}
}


/**
 * 
 */
static void _clearLightSurf()
{
	memset(r_lightSpawnSurf, 0, sizeof(r_lightSpawnSurf));
	r_numAutoLights = 0;
	r_numIgnoreAutoLights = 0;
}


/**
 * 
 */
static byte* _decompressVis(byte * in, Model*  model)
{
	static byte decompressed[MAX_MAP_LEAFS / 8];
	int c = 0;
	byte *out = nullptr;
	int row = 0;

	row = (model->vis->numclusters + 7) >> 3;
	out = decompressed;

	if (!in)
	{					// no vis info, so make all visible
		while (row)
		{
			*out++ = 0xff; /// \fixme MAGIC NUMBERS
			row--;
		}
		return decompressed;
	}

	do
	{
		if (*in)
		{
			*out++ = *in++;
			continue;
		}

		c = in[1];
		in += 2;
		while (c)
		{
			*out++ = 0;
			c--;
		}
	} while (out - decompressed < row);

	return decompressed;
}


/**
 * 
 */
static void _loadEntityString(lump_t* l)
{
	if (l->filelen > MAX_MAP_ENTSTRING)
		VID_Error(ERR_DROP, "Map's entity lump too large.");

	//memcpy(map_entitystring, MOD_BASE + l->fileofs, l->filelen);
}


/**
 * From the Quake2XP lightmap enhancements.
 */
static bool _loadXPLM()
{
	char tmp[MAX_QPATH], name[MAX_QPATH];
	int *pIB;
	int i, len, numFaces;

	FS_StripExtension(Mod_LoadingModel()->name, tmp, sizeof(tmp));
	Q_sprintf(name, sizeof(name), "%s.xplm", tmp);

	char* buf = nullptr;
	len = FS_LoadFile(name, reinterpret_cast<void**>(&buf));

	if (!buf)
	{
		Com_DPrintf("R_LoadXPLM(): external lightmaps for '%s' not found.\n", Mod_LoadingModel()->name);
		return false;
	}

	// face count & light offsets
	char* pB = buf;
	pIB = (int *)buf;

	numFaces = LittleLong(*pIB++);
	pB += 4;
	len -= 4;

	XPLM_OFFSETS = static_cast<int*>(calloc(numFaces * 4, sizeof(int)));

	for (i = 0; i < numFaces; i++, pIB++)
		XPLM_OFFSETS[i] = LittleLong(*pIB);

	pB += numFaces * 4;
	len -= numFaces * 4;

	// face style counts

	// lightmap scale
	Mod_LoadingModel()->lightmap_scale = *pB++;
	len--;

	Mod_LoadingModel()->lightData = (byte *)Hunk_Alloc(len);
	memcpy(Mod_LoadingModel()->lightData, pB, len);
	Mod_LoadingModel()->memorySize += len;
	Mod_LoadingModel()->useXPLM = true;

	FS_FreeFile(buf);

	Com_Printf("Loaded lightmaps from " S_COLOR_GREEN "\'%s\'" S_COLOR_WHITE ".\n", name);

	return true;
}


/**
 *
 */
static bool _loadXpVis()
{
	char tmp[MAX_QPATH] = { '\0' }, name[MAX_QPATH] = { '\0' };

	FS_StripExtension(Mod_LoadingModel()->name, tmp, sizeof(tmp));
	Q_sprintf(name, sizeof(name), "%s.xpvis", tmp);

	char* buf = nullptr;
	int len = FS_LoadFile(name, reinterpret_cast<void**>(&buf));
	if (!buf)
	{
		Com_Printf("R_LoadXpVis(): external vis for " S_COLOR_GREEN "'%s' " S_COLOR_WHITE "not found.\n", Mod_LoadingModel()->name);
		return false;
	}

	Mod_LoadingModel()->vis = reinterpret_cast<dvis_t*>(Hunk_Alloc(len));
	memcpy(Mod_LoadingModel()->vis, buf, len);
	Mod_LoadingModel()->memorySize += len;
	Mod_LoadingModel()->useXpVis = true;

	Mod_LoadingModel()->vis->numclusters = LittleLong(Mod_LoadingModel()->vis->numclusters);
	for (int i = 0; i < Mod_LoadingModel()->vis->numclusters; i++)
	{
		Mod_LoadingModel()->vis->bitofs[i][0] = LittleLong(Mod_LoadingModel()->vis->bitofs[i][0]);
		Mod_LoadingModel()->vis->bitofs[i][1] = LittleLong(Mod_LoadingModel()->vis->bitofs[i][1]);
	}

	FS_FreeFile(buf);

	Com_Printf("Loaded vis data from " S_COLOR_GREEN "%s" S_COLOR_WHITE ".\n", name);

	return true;
}


/**
 * 
 */
void _rebuildCache(int count, int ci, float* vi)
{
	char cacheName[MAX_QPATH] = { '\0' };
	int smoothAng = (int)r_tbnSmoothAngle->value;
	int i = 0;

	int cj, j;
	float *vj;
	msurface_t *si, *sj;
	vec3_t ni, nj;

	float threshold = cosf(DEG2RAD(r_tbnSmoothAngle->value));

	// Not found, so write it as we calculate it
	Q_sprintf(cacheName, sizeof(cacheName), "cache/%s", CURRENT_MODEL->name);

	FS_MakeDirectory(cacheName);
	FS_File* cacheFile = FS_Open(cacheName, FS_OPEN_WRITE);

	if (cacheFile == nullptr)
		Com_Printf(S_COLOR_RED "GL_BuildTBN: could't open %s for writing\n", CURRENT_MODEL->name);
	else
	{
		Com_Printf(S_COLOR_YELLOW "GL_BuildTBN: calculating %s, with angle %d\n", CURRENT_MODEL->name, smoothAng);
		FS_Write(cacheFile, &smoothAng, sizeof(smoothAng), 1);
	}

	for (i = 0; i < count; i++)
	{
		si = &CURRENT_MODEL->surfaces[i];

		if (si->texInfo->flags & (SURF_SKY | /*SURF_TRANS33 | SURF_TRANS66 | */SURF_NODRAW))
			continue;

		vi = si->polys->verts[0];

		for (ci = 0; ci < si->numEdges; ci++, vi += VERTEXSIZE)
			vi[7] = vi[8] = vi[9] = vi[10] = vi[11] = vi[12] = vi[13] = vi[14] = vi[15] = 0;

		if (si->flags & MSURF_PLANEBACK)
			VectorNegate(si->plane->normal, ni);
		else
			VectorCopy(si->plane->normal, ni);

		for (j = 0; j < count; j++)
		{
			sj = &CURRENT_MODEL->surfaces[j];

			if (!(sj->texInfo->flags & (SURF_SKY | /*SURF_TRANS33 | SURF_TRANS66 | */SURF_NODRAW)))
			{
				if (si->texInfo->diffuse->texnum != sj->texInfo->diffuse->texnum)
					continue;

				if (sj->flags & MSURF_PLANEBACK)
					VectorNegate(sj->plane->normal, nj);
				else
					VectorCopy(sj->plane->normal, nj);

				if (DotProduct(ni, nj) >= threshold) {
					vi = si->polys->verts[0];
					for (ci = 0; ci < si->numEdges; ci++, vi += VERTEXSIZE)
					{
						vj = sj->polys->verts[0];
						for (cj = 0; cj < sj->numEdges; cj++, vj += VERTEXSIZE)
						{
							if (VectorsEqual(vi, vj))
							{
								vi[7] += nj[0];
								vi[8] += nj[1];
								vi[9] += nj[2];
							}
						}
					}
				}
			}
		}

		vi = si->polys->verts[0];
		for (ci = 0; ci < si->numEdges; ci++, vi += VERTEXSIZE)
		{
			vec3_t normal, biTangent, tmp;
			VectorSet(normal, vi[7], vi[8], vi[9]);
			VectorNormalize(normal);

			if (DotProduct(normal, ni) < threshold)
			{
				vi[7] = normal[0] + ni[0];
				vi[8] = normal[1] + ni[1];
				vi[9] = normal[2] + ni[2];
			}
			else
			{
				vi[7] = normal[0];
				vi[8] = normal[1];
				vi[9] = normal[2];
			}

			CrossProduct(normal, si->texInfo->vecs[0], tmp);
			CrossProduct(normal, tmp, biTangent);
			VectorNormalize(biTangent);
			if (DotProduct(biTangent, si->texInfo->vecs[0]) < 0)
			{
				vi[10] = -biTangent[0];
				vi[11] = -biTangent[1];
				vi[12] = -biTangent[2];
			}
			else
			{
				vi[10] = biTangent[0];
				vi[11] = biTangent[1];
				vi[12] = biTangent[2];
			}

			CrossProduct(normal, si->texInfo->vecs[1], tmp);
			CrossProduct(normal, tmp, biTangent);
			VectorNormalize(biTangent);
			if (DotProduct(biTangent, si->texInfo->vecs[1]) < 0)
			{
				vi[13] = -biTangent[0];
				vi[14] = -biTangent[1];
				vi[15] = -biTangent[2];
			}
			else
			{
				vi[13] = biTangent[0];
				vi[14] = biTangent[1];
				vi[15] = biTangent[2];
			}

			if (cacheFile != nullptr)
				FS_Write(cacheFile, vi + 7, sizeof(*vi), 9);
		}
	}
	if (cacheFile != nullptr)
		FS_Close(cacheFile);
}


// ===============================================================================
// = INTERNAL FUNCTIONS
// ===============================================================================
/**
 * 
 */
void BSP_LoadTexinfo(lump_t* l)
{
	if (l->filelen % sizeof(texInfo_t))
		VID_Error(ERR_DROP, "Mod_LoadTexinfo():: incorrect lump size in %s", Mod_LoadingModel()->name);

	texInfo_t* in = (texInfo_t*)(MOD_BASE + l->fileofs);

	int count = l->filelen / sizeof(texInfo_t);

	Mod_LoadingModel()->numTexInfo = count;
	Mod_LoadingModel()->texInfo = (mtexInfo_t*)Hunk_Alloc(count * sizeof(mtexInfo_t));
	Mod_LoadingModel()->memorySize += count * sizeof(mtexInfo_t);

	mtexInfo_t* out = Mod_LoadingModel()->texInfo;
	for (int i = 0; i < count; i++, in++, out++)
	{
		for (int j = 0; j < 2; j++)
		{
			for (int k = 0; k < 4; k++)
				out->vecs[j][k] = LittleFloat(in->vecs[j][k]);
		}

		out->value = LittleLong(in->value);
		out->flags = LittleLong(in->flags);

		int next = LittleLong(in->nexttexInfo);

		if (next > 0)
			out->next = Mod_LoadingModel()->texInfo + next;
		else
			out->next = nullptr;

		Material_SetDefault(out);

		char _basePath[MAX_QPATH] = { '\0' };
		Q_sprintf(_basePath, sizeof(_basePath), "textures/%s", in->texture);
		if(!Material_Load(out, _basePath))
		{
			Com_Printf("** Material \'" S_COLOR_GREEN "%s" S_COLOR_WHITE "\' failed to load.\n", in->texture);

			char _path[MAX_QPATH];
			Q_sprintf(_path, sizeof(_path), "textures/%s.tga", in->texture);
			Image* img = GL_FindImage(_path, IT_WALL, true);
			if (img)
			{
				out->diffuse = img;
				continue;
			}
			Q_sprintf(_path, sizeof(_path), "textures/%s.png", in->texture);
			img = GL_FindImage(_path, IT_WALL, true);
			if (img)
			{
				out->diffuse = img;
				continue;
			}
			Com_Printf("** No suitable alternative found for \'" S_COLOR_GREEN "%s" S_COLOR_WHITE "\'\n", in->texture);
		}

		if (out->diffuse->alpha != 1.0f) // Anything but completely opaque
			out->flags |= SURF_ALPHA;
	}

	// count animation frames
	out = Mod_LoadingModel()->texInfo;
	for (int i = 0; i < count; i++, out++)
	{
		out->numFrames = 1;
		for (mtexInfo_t* step = out->next; step && step != out; step = step->next)
			out->numFrames++;
	}
}


/**
 * 
 */
void BSP_BuildVertexCache()
{
	msurface_t      *surf;
	int         i, idx = 0, numIndices = 0;

	// calc vbo buffer size
	int vb = 0;
	for (i = 0, surf = CURRENT_MODEL->surfaces; i < CURRENT_MODEL->numSurfaces; i++, surf++)
	{
		vb += surf->polys->numVerts;
	}

	// and offsets...
	getVbo().xyz_offset = 0;
	int xyz_size = vb * sizeof(vec3_t);

	getVbo().st_offset = getVbo().xyz_offset + xyz_size;
	int st_size = vb * sizeof(vec2_t);

	getVbo().lm_offset = getVbo().st_offset + st_size;
	int lm_size = vb * sizeof(vec2_t);

	getVbo().nm_offset = getVbo().lm_offset + lm_size;
	int nm_size = vb * sizeof(vec3_t);

	getVbo().tg_offset = getVbo().nm_offset + nm_size;
	int tg_size = vb * sizeof(vec3_t);

	getVbo().bn_offset = getVbo().tg_offset + tg_size;
	int bn_size = vb * sizeof(vec3_t);

	int vbo_size = getVbo().bn_offset + bn_size;

	float* buf = (float*)calloc(vbo_size, sizeof(float));
	if (!buf)
	{
		Com_Error(ERR_DROP, S_COLOR_RED "Unable to create vertex buffer.\n");
	}

	// fill vbo
	vb = 0;
	for (i = 0, surf = CURRENT_MODEL->surfaces; i < CURRENT_MODEL->numSurfaces; i++, surf++)
	{
		int         jj, nv = surf->polys->numVerts;
		glpoly_t    *p = surf->polys;
		float       *v;

		surf->baseIndex = idx;

		v = p->verts[0];
		for (jj = 0; jj < nv; jj++, v += VERTEXSIZE, vb++)
		{
			// vertex data
			buf[getVbo().xyz_offset / 4 + vb * 3 + 0] = v[0];
			buf[getVbo().xyz_offset / 4 + vb * 3 + 1] = v[1];
			buf[getVbo().xyz_offset / 4 + vb * 3 + 2] = v[2];
			// st coords
			buf[getVbo().st_offset / 4 + vb * 2 + 0] = v[3];
			buf[getVbo().st_offset / 4 + vb * 2 + 1] = v[4];
			// lm coords
			buf[getVbo().lm_offset / 4 + vb * 2 + 0] = v[5];
			buf[getVbo().lm_offset / 4 + vb * 2 + 1] = v[6];
			// normals
			buf[getVbo().nm_offset / 4 + vb * 3 + 0] = v[7];
			buf[getVbo().nm_offset / 4 + vb * 3 + 1] = v[8];
			buf[getVbo().nm_offset / 4 + vb * 3 + 2] = v[9];
			// tangents
			buf[getVbo().tg_offset / 4 + vb * 3 + 0] = v[10];
			buf[getVbo().tg_offset / 4 + vb * 3 + 1] = v[11];
			buf[getVbo().tg_offset / 4 + vb * 3 + 2] = v[12];
			// binormals
			buf[getVbo().bn_offset / 4 + vb * 3 + 0] = v[13];
			buf[getVbo().bn_offset / 4 + vb * 3 + 1] = v[14];
			buf[getVbo().bn_offset / 4 + vb * 3 + 2] = v[15];

			idx++;
		}

	}

	glGenBuffers(1, &getVbo().vbo_BSP);
	glBindBuffer(GL_ARRAY_BUFFER, getVbo().vbo_BSP);
	glBufferData(GL_ARRAY_BUFFER, vbo_size, buf, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	Com_DPrintf(S_COLOR_GREEN "%d" S_COLOR_WHITE " kbytes of VBO vertex data\n", vbo_size / 1024);
	free(buf);

	// Gen VAO
	glDeleteVertexArrays(1, &getVao().bsp_a);
	glDeleteVertexArrays(1, &getVao().bsp_l);

	//light surfaces
	glGenVertexArrays(1, &getVao().bsp_l);
	glBindVertexArray(getVao().bsp_l);

	glBindBuffer(GL_ARRAY_BUFFER, getVbo().vbo_BSP);

	glEnableVertexAttribArray(ATT_POSITION);
	glEnableVertexAttribArray(ATT_TEX0);
	glEnableVertexAttribArray(ATT_NORMAL);
	glEnableVertexAttribArray(ATT_TANGENT);
	glEnableVertexAttribArray(ATT_BINORMAL);

	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, BUFFER_OFFSET(getVbo().xyz_offset));
	glVertexAttribPointer(ATT_TEX0, 2, GL_FLOAT, false, 0, BUFFER_OFFSET(getVbo().st_offset));
	glVertexAttribPointer(ATT_NORMAL, 3, GL_FLOAT, false, 0, BUFFER_OFFSET(getVbo().nm_offset));
	glVertexAttribPointer(ATT_TANGENT, 3, GL_FLOAT, false, 0, BUFFER_OFFSET(getVbo().tg_offset));
	glVertexAttribPointer(ATT_BINORMAL, 3, GL_FLOAT, false, 0, BUFFER_OFFSET(getVbo().bn_offset));

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	//ambient surfaces
	glGenVertexArrays(1, &getVao().bsp_a);
	glBindVertexArray(getVao().bsp_a);

	glBindBuffer(GL_ARRAY_BUFFER, getVbo().vbo_BSP);

	glEnableVertexAttribArray(ATT_POSITION);
	glEnableVertexAttribArray(ATT_TEX0);
	glEnableVertexAttribArray(ATT_TEX1);
	glEnableVertexAttribArray(ATT_NORMAL);
	glEnableVertexAttribArray(ATT_TANGENT);
	glEnableVertexAttribArray(ATT_BINORMAL);

	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, BUFFER_OFFSET(getVbo().xyz_offset));
	glVertexAttribPointer(ATT_TEX0, 2, GL_FLOAT, false, 0, BUFFER_OFFSET(getVbo().st_offset));
	glVertexAttribPointer(ATT_TEX1, 2, GL_FLOAT, false, 0, BUFFER_OFFSET(getVbo().lm_offset));
	glVertexAttribPointer(ATT_NORMAL, 3, GL_FLOAT, false, 0, BUFFER_OFFSET(getVbo().nm_offset));
	glVertexAttribPointer(ATT_TANGENT, 3, GL_FLOAT, false, 0, BUFFER_OFFSET(getVbo().tg_offset));
	glVertexAttribPointer(ATT_BINORMAL, 3, GL_FLOAT, false, 0, BUFFER_OFFSET(getVbo().bn_offset));

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}


/**
 * 
 */
void BSP_LoadFaces(lump_t * l)
{
	if (l->filelen % sizeof(dface_t))
		VID_Error(ERR_DROP, "MOD_LoadBmodel: funny lump size in %s", Mod_LoadingModel()->name);

	int count = l->filelen / sizeof(dface_t);

	msurface_t* out = (msurface_t*)Hunk_Alloc(count * sizeof(msurface_t));

	Mod_LoadingModel()->surfaces = out;
	Mod_LoadingModel()->numSurfaces = count;
	Mod_LoadingModel()->memorySize += count * sizeof(msurface_t);

	CURRENT_MODEL = Mod_LoadingModel();

	TEMP_EDGES = (temp_connect_t*)calloc(CURRENT_MODEL->numEdges, sizeof(temp_connect_t));

	GL_BeginBuildingLightmaps(Mod_LoadingModel());

	dface_t* in = (dface_t*)(MOD_BASE + l->fileofs);
	for (int surfnum = 0; surfnum < count; surfnum++, in++, out++)
	{
		out->firstedge = LittleLong(in->firstedge);
		out->numEdges = LittleShort(in->numEdges);
		out->flags = 0;
		out->polys = nullptr;

		if (LittleShort(in->side) != 0)
			out->flags |= MSURF_PLANEBACK;

		out->plane = Mod_LoadingModel()->planes + LittleShort(in->planenum);

		int ti = LittleShort(in->texInfo);
		if (ti < 0 || ti >= Mod_LoadingModel()->numTexInfo)
			VID_Error(ERR_DROP, "Mod_LoadFaces(): bad texInfo number (%i).", ti);

		out->texInfo = Mod_LoadingModel()->texInfo + ti;

		_calcSurfaceExtents(out);

		// lighting info
		for (int i = 0; i < MAXLIGHTMAPS; i++)
			out->styles[i] = in->styles[i];

		int lightOffset = Mod_LoadingModel()->useXPLM ? XPLM_OFFSETS[surfnum] : LittleLong(in->lightofs);
		out->samples = (lightOffset == -1) ? nullptr : Mod_LoadingModel()->lightData + lightOffset;

		// set the drawing flags
		if (out->texInfo->flags & SURF_WARP)
			out->flags |= MSURF_DRAWTURB;

		// create lightmaps and polygons
		if (!(out->texInfo->flags & (SURF_SKY | SURF_ALPHA | SURF_WARP)))
			GL_CreateSurfaceLightmap(out);

		_buildPolygonFromSurface(out);

		if (RELIGHT_MAP)
			_addLightFromSurface(out);

		_calcSurfaceBounds(out);
		_calcBspIndices(out);
	}

	// Build TBN for smoothing bump mapping (Berserker)
	_buildTangentSpaceMatrix(count);
	GL_EndBuildingLightmaps();

	// calc neighbours for shadow volumes
	msurface_t* surf = CURRENT_MODEL->surfaces;
	for (int surfnum = 0; surfnum < count; surfnum++, surf++)
	{
		if (surf->flags & (MSURF_DRAWTURB | MSURF_DRAWSKY))
			continue;

		_buildSurfaceNeighbors(surf);
	}

	free(TEMP_EDGES);
	BSP_BuildVertexCache();
}


/**
*
*/
void BSP_SetParent(mnode_t* node, mnode_t* parent)
{
	node->parent = parent;
	if (node->contents != -1)
		return;

	BSP_SetParent(node->children[0], node);
	BSP_SetParent(node->children[1], node);
}


/**
 * 
 */
void BSP_LoadNodes(lump_t* l)
{
	int i, j, count, p;
	dnode_t *in;
	mnode_t *out;

	in = (dnode_t *)(MOD_BASE + l->fileofs);
	if (l->filelen % sizeof(*in))
		VID_Error(ERR_DROP, "Mod_LoadNodes: incorrect lump size in %s", Mod_LoadingModel()->name);

	count = l->filelen / sizeof(*in);
	out = (mnode_t*)Hunk_Alloc(count * sizeof(*out));

	Mod_LoadingModel()->memorySize += count * sizeof(*out);

	Mod_LoadingModel()->nodes = out;
	Mod_LoadingModel()->numNodes = count;

	for (i = 0; i < count; i++, in++, out++)
	{
		for (j = 0; j < 3; j++)
		{
			out->minmaxs[j] = LittleShort(in->mins[j]);
			out->minmaxs[3 + j] = LittleShort(in->maxs[j]);
		}

		p = LittleLong(in->planenum);
		out->plane = Mod_LoadingModel()->planes + p;

		out->firstsurface = LittleShort(in->firstface);
		out->numsurfaces = LittleShort(in->numfaces);
		out->contents = -1;		// differentiate from leafs

		for (j = 0; j < 2; j++)
		{
			p = LittleLong(in->children[j]);
			if (p >= 0)
				out->children[j] = Mod_LoadingModel()->nodes + p;
			else
				out->children[j] =
				(mnode_t*)(Mod_LoadingModel()->leafs + (-1 - p));
		}
	}

	BSP_SetParent(Mod_LoadingModel()->nodes, nullptr);	// sets nodes and leafs
}


/**
 * 
 */
void BSP_LoadLeafs(lump_t *l)
{
	dleaf_t *in;
	mleaf_t *out;
	int i, j, count, p;
	glpoly_t *poly;

	in = (dleaf_t *)(MOD_BASE + l->fileofs);
	if (l->filelen % sizeof(*in))
		VID_Error(ERR_DROP, "Mod_LoadLeafs: incorrect lump size in %s", Mod_LoadingModel()->name);

	count = l->filelen / sizeof(*in);
	out = (mleaf_t *)Hunk_Alloc(count * sizeof(*out));

	Mod_LoadingModel()->memorySize += count * sizeof(*out);
	Mod_LoadingModel()->leafs = out;
	Mod_LoadingModel()->numLeafs = count;

	for (i = 0; i < count; i++, in++, out++)
	{
		for (j = 0; j < 3; j++)
		{
			out->minmaxs[j] = LittleShort(in->mins[j]);
			out->minmaxs[3 + j] = LittleShort(in->maxs[j]);
		}

		p = LittleLong(in->contents);
		out->contents = p;

		out->cluster = LittleShort(in->cluster);
		out->area = LittleShort(in->area);

		out->firstmarksurface = Mod_LoadingModel()->markSurfaces + LittleShort(in->firstleafface);
		out->numMarkSurfaces = LittleShort(in->numleaffaces);

		if (out->contents & MASK_WATER)
		{
			for (j = 0; j < out->numMarkSurfaces; j++)
			{
				out->firstmarksurface[j]->flags |= MSURF_UNDERWATER;

				for (poly = out->firstmarksurface[j]->polys; poly; poly = poly->next)
					poly->flags |= MSURF_UNDERWATER;

				if (out->contents & CONTENTS_WATER) {
					out->firstmarksurface[j]->flags |= MSURF_WATER;

					for (poly = out->firstmarksurface[j]->polys; poly; poly = poly->next)
						poly->flags |= MSURF_WATER;
				}

			}

		}
	}
}


/**
 * 
 */
void BSP_LoadMarksurfaces(lump_t * l)
{
	int i, j, count;
	short *in;
	msurface_t **out;

	in = (short *)(MOD_BASE + l->fileofs);
	if (l->filelen % sizeof(*in))
		VID_Error(ERR_DROP, "Mod_LoadMarksurfaces: incorrect lump size in %s", Mod_LoadingModel()->name);
	count = l->filelen / sizeof(*in);
	out = (msurface_t**)Hunk_Alloc(count * sizeof(*out));

	Mod_LoadingModel()->markSurfaces = out;
	Mod_LoadingModel()->numMarkSurfaces = count;
	Mod_LoadingModel()->memorySize += count * sizeof(*out);

	for (i = 0; i < count; i++)
	{
		j = LittleShort(in[i]);
		if (j < 0 || j >= Mod_LoadingModel()->numSurfaces)
			VID_Error(ERR_DROP, "Mod_ParseMarksurfaces: bad surface number");

		out[i] = Mod_LoadingModel()->surfaces + j;
	}
}


/**
 * 
 */
void BSP_LoadSurfedges(lump_t * l)
{
	int i, count;
	int *in, *out;

	in = (int *)(MOD_BASE + l->fileofs);
	if (l->filelen % sizeof(*in))
		VID_Error(ERR_DROP, "Mod_LoadSurfedges: incorrect lump size in %s", Mod_LoadingModel()->name);

	count = l->filelen / sizeof(*in);

	if (count < 1 || count >= MAX_MAP_SURFEDGES)
		VID_Error(ERR_DROP, "MOD_LoadBmodel: bad surfEdges count in %s: %i", Mod_LoadingModel()->name, count);

	out = (int*)Hunk_Alloc(count * sizeof(*out));

	Mod_LoadingModel()->memorySize += count * sizeof(*out);
	Mod_LoadingModel()->surfEdges = out;
	Mod_LoadingModel()->numSurfEdges = count;

	for (i = 0; i < count; i++)
		out[i] = LittleLong(in[i]);
}


/**
 * 
 */
void BSP_LoadPlanes(lump_t * l)
{
	int i, j;
	cplane_t *out;
	dplane_t *in;
	int count;
	int bits;

	in = (dplane_t *)(MOD_BASE + l->fileofs);
	if (l->filelen % sizeof(*in))
		VID_Error(ERR_DROP, "Mod_LoadPlanes: incorrect lump size in %s", Mod_LoadingModel()->name);
	count = l->filelen / sizeof(*in);
	out = (cplane_t*)Hunk_Alloc(count * 2 * sizeof(*out));

	Mod_LoadingModel()->planes = out;
	Mod_LoadingModel()->numPlanes = count;
	Mod_LoadingModel()->memorySize += count * sizeof(*out);

	for (i = 0; i < count; i++, in++, out++)
	{
		bits = 0;
		for (j = 0; j < 3; j++)
		{
			out->normal[j] = LittleFloat(in->normal[j]);
			if (out->normal[j] < 0)
				bits |= 1 << j;
		}

		out->dist = LittleFloat(in->dist);
		out->type = LittleLong(in->type);
		out->signbits = bits;
	}
}


/**
 * 
 */
void BSP_GenerateLights(Model*  mod)
{
	r_worldmodel = mod;
	R_InitLightgrid();
}


/**
 * 
 */
void BSP_LoadLighting(lump_t * l)
{
	char* s, *c;

	Mod_LoadingModel()->useXPLM = false;

	if (!l->filelen)
	{
		Mod_LoadingModel()->lightData = nullptr;
		Mod_LoadingModel()->lightmap_scale = 16;
		return;
	}
	
	Mod_LoadingModel()->lightData = (byte*)Hunk_Alloc(l->filelen);
	memcpy(Mod_LoadingModel()->lightData, MOD_BASE + l->fileofs, l->filelen);

	Mod_LoadingModel()->memorySize += l->filelen;
	Mod_LoadingModel()->lightmap_scale = -1;

	if ((s = strstr(CM_EntityString(), "\"lightmap_scale\""))) // resolve lightmap scale
	{  
		c = Com_Parse(&s);  // parse the string itself and then the value
		Mod_LoadingModel()->lightmap_scale = atoi(Com_Parse(&s));
		Com_DPrintf("Resolved lightmap_scale: %i\n", Mod_LoadingModel()->lightmap_scale);
	}

	if (Mod_LoadingModel()->lightmap_scale < 1)  // ensure safe default
		Mod_LoadingModel()->lightmap_scale = DEFAULT_LIGHTMAP_SCALE;
}


/**
 * 
 */
void BSP_LoadVisibility(lump_t* l)
{
	int i;

	Mod_LoadingModel()->useXpVis = false;

	if (_loadXpVis())
		return;

	if (!l->filelen)
	{
		Mod_LoadingModel()->vis = nullptr;
		return;
	}

	Mod_LoadingModel()->vis = (dvis_t*)Hunk_Alloc(l->filelen);
	Mod_LoadingModel()->memorySize += l->filelen;
	memcpy(Mod_LoadingModel()->vis, MOD_BASE + l->fileofs, l->filelen);

	Mod_LoadingModel()->vis->numclusters = LittleLong(Mod_LoadingModel()->vis->numclusters);
	for (i = 0; i < Mod_LoadingModel()->vis->numclusters; i++)
	{
		Mod_LoadingModel()->vis->bitofs[i][0] = LittleLong(Mod_LoadingModel()->vis->bitofs[i][0]);
		Mod_LoadingModel()->vis->bitofs[i][1] = LittleLong(Mod_LoadingModel()->vis->bitofs[i][1]);
	}
}


/**
 * 
 */
void BSP_LoadVertices(lump_t * l)
{
	dvertex_t *in;
	mvertex_t *out;
	int i, count;

	in = (dvertex_t *)(MOD_BASE + l->fileofs);
	if (l->filelen % sizeof(*in))
		VID_Error(ERR_DROP, "BSP_LoadVertices(): incorrect lump size in %s", Mod_LoadingModel()->name);

	count = l->filelen / sizeof(*in);
	out = (mvertex_t*)Hunk_Alloc(count * sizeof(*out));

	Mod_LoadingModel()->vertexes = out;
	Mod_LoadingModel()->numVertexes = count;

	Mod_LoadingModel()->memorySize += count * sizeof(*out);

	for (i = 0; i < count; i++, in++, out++)
	{
		out->position[0] = LittleFloat(in->point[0]);
		out->position[1] = LittleFloat(in->point[1]);
		out->position[2] = LittleFloat(in->point[2]);
	}
}


/**
 *
 */
void BSP_LoadSubModels(lump_t * l)
{
	dmodel_t *in;
	mmodel_t *out;
	int i, j, count;

	in = (dmodel_t *)(MOD_BASE + l->fileofs);
	if (l->filelen % sizeof(*in))
		VID_Error(ERR_DROP, "Mod_LoadsubModels: incorrect lump size in %s", Mod_LoadingModel()->name);

	count = l->filelen / sizeof(*in);
	out = (mmodel_t*)Hunk_Alloc(count * sizeof(*out));

	Mod_LoadingModel()->subModels = out;
	Mod_LoadingModel()->numSubModels = count;
	Mod_LoadingModel()->memorySize += count * sizeof(*out);

	for (i = 0; i < count; i++, in++, out++)
	{
		for (j = 0; j < 3; j++) // spread the mins / maxs by a pixel
		{
			out->mins[j] = LittleFloat(in->mins[j]) - 1;
			out->maxs[j] = LittleFloat(in->maxs[j]) + 1;
			out->origin[j] = LittleFloat(in->origin[j]);
		}

		out->radius = R_RadiusFromBounds(out->mins, out->maxs);
		out->headnode = LittleLong(in->headnode);
		out->firstface = LittleLong(in->firstface);
		out->numfaces = LittleLong(in->numfaces);
	}
}


/**
 * 
 */
void BSP_LoadEdges(lump_t * l)
{
	dedge_t *in;
	medge_t *out;
	int i, count;

	in = (dedge_t*)(MOD_BASE + l->fileofs);
	if (l->filelen % sizeof(*in))
		VID_Error(ERR_DROP, "BSP_LoadEdges(): incorrect lump size in %s", Mod_LoadingModel()->name);

	count = l->filelen / sizeof(*in);
	out = (medge_t*)Hunk_Alloc((count + 1) * sizeof(*out));

	Mod_LoadingModel()->edges = out;
	Mod_LoadingModel()->numEdges = count;
	Mod_LoadingModel()->memorySize += count * sizeof(*out);

	for (i = 0; i < count; i++, in++, out++)
	{
		out->v[0] = (unsigned short)LittleShort(in->v[0]);
		out->v[1] = (unsigned short)LittleShort(in->v[1]);
	}
}


// ===============================================================================
// = PUBLIC FUNCTIONS
// ===============================================================================

/**
 * 
 */
mleaf_t* BSP_PointInLeaf(vec3_t p, Model* model)
{
	mnode_t *node;
	float d;
	cplane_t *plane;

	if (!model || !model->nodes) VID_Error(ERR_DROP, "Mod_PointInLeaf(): bad model");

	node = model->nodes;
	for(;;)
	{
		if (node->contents != -1)	/// \fixme MAGIC NUMBER (sentinel)
			return (mleaf_t *)node;
		plane = node->plane;
		
		d = DotProduct(p, plane->normal) - plane->dist;
		
		if (d > 0) node = node->children[0];
		else node = node->children[1];
	}

	return nullptr;	// never reached  << Would be good to use an assert or if/when
					// ported to C++ throwing an exception.
}


/**
 * 
 */
byte* BSP_ClusterPVS(int cluster, Model* model)
{
	if (cluster == -1 || !model->vis)
		return MOD_NOVIS;
	
	return _decompressVis((byte *)model->vis + model->vis->bitofs[cluster][DVIS_PVS], model);
}


/**
 * \fixme	This isn't really a great name for this.
 */
void BSP_Init()
{
	memset(MOD_NOVIS, 0xff, sizeof(MOD_NOVIS));
	CL_ClearDecals(); ///\todo	Not sure I like this here.
}


/**
 * 
 */
void BSP_LoadBrushModel(Model*  mod, void *buffer)
{
	_clearLightSurf();
	DeleteShadowVertexBuffers();
	R_ClearWorldLights();

	Mod_LoadingModel()->memorySize = 0;
	Mod_LoadingModel()->type = mod_brush;
	if (Mod_LoadingModel() != Mod_KnownModels())
		VID_Error(ERR_DROP, "Mod_LoadBrushModel(): Loaded a brush model after the world.");

	dheader_t* header = (dheader_t*)buffer;
	if (LittleLong(header->version) != BSPVERSION)
		VID_Error(ERR_DROP, "Mod_LoadBrushModel(): Wrong header version in \'%s\' (found %i, expected %i)", mod->name, header->version, BSPVERSION);

	// swap all the lumps
	MOD_BASE = (byte*)header;

	for (int i = 0; i < sizeof(dheader_t) * 0.25; i++)
		((int*)header)[i] = LittleLong(((int*)header)[i]);

	// load into heap
	_loadEntityString(&header->lumps[LUMP_ENTITIES]);
	BSP_LoadVertices(&header->lumps[LUMP_VERTEXES]);
	BSP_LoadEdges(&header->lumps[LUMP_EDGES]);
	BSP_LoadSurfedges(&header->lumps[LUMP_SURFEDGES]);

	if (!_loadXPLM())
		BSP_LoadLighting(&header->lumps[LUMP_LIGHTING]);

	BSP_LoadPlanes(&header->lumps[LUMP_PLANES]);
	BSP_LoadTexinfo(&header->lumps[LUMP_TEXINFO]);

	BSP_LoadFaces(&header->lumps[LUMP_FACES]);

	if (Mod_LoadingModel()->useXPLM)
		free(XPLM_OFFSETS);

	BSP_LoadMarksurfaces(&header->lumps[LUMP_LEAFFACES]);
	BSP_LoadVisibility(&header->lumps[LUMP_VISIBILITY]);
	BSP_LoadLeafs(&header->lumps[LUMP_LEAFS]);
	BSP_LoadNodes(&header->lumps[LUMP_NODES]);
	BSP_LoadSubModels(&header->lumps[LUMP_MODELS]);
	BSP_GenerateLights(mod);

	Load_LightFile();
	R_CalcStaticLightInteraction();

	mod->numFrames = 2;			// regular and alternate animation

	// set up the subModels
	for (int i = 0; i < mod->numSubModels; i++)
	{
		mmodel_t* bm = &mod->subModels[i];
		Model* starmod = &Mod_InlineModels()[i];

		*starmod = *Mod_LoadingModel();

		starmod->firstModelSurface = bm->firstface;
		starmod->numModelSurfaces = bm->numfaces;
		starmod->firstNode = bm->headnode;
		if (starmod->firstNode >= Mod_LoadingModel()->numNodes)
			VID_Error(ERR_DROP, "Inline model %i has bad firstNode", i);

		VectorCopy(bm->maxs, starmod->maxs);
		VectorCopy(bm->mins, starmod->mins);
		starmod->radius = bm->radius;

		if (i == 0)
			*Mod_LoadingModel() = *starmod;

		starmod->numLeafs = bm->visleafs;
	}

	BSP_SIZE += Mod_LoadingModel()->memorySize;
}
