/*
Copyright (C) 1997-2001 Id Software, Inc., 2004-2013 Quake2xp Team.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "r_local.h"


// this is the slow, general version
int BoxOnPlaneSide22 (vec3_t emins, vec3_t emaxs, struct cplane_s *p) {
	int		i;
	float	dist1, dist2;
	int		sides;
	vec3_t	corners[2];

	for (i = 0; i < 3; i++) {
		if (p->normal[i] < 0) {
			corners[0][i] = emins[i];
			corners[1][i] = emaxs[i];
		}
		else {
			corners[1][i] = emins[i];
			corners[0][i] = emaxs[i];
		}
	}
	dist1 = DotProduct (p->normal, corners[0]) - p->dist;
	dist2 = DotProduct (p->normal, corners[1]) - p->dist;
	sides = 0;
	if (dist1 >= 0)
		sides = 1;
	if (dist2 < 0)
		sides |= 2;

	return sides;
}


/*
 =================
 BoundsAndSphereIntersect
 =================
 */
bool BoundsAndSphereIntersect (const vec3_t mins, const vec3_t maxs, const vec3_t origin, float radius) {

	if (r_noCull->value)
		return false;

	if (mins[0] > origin[0] + radius || mins[1] > origin[1] + radius || mins[2] > origin[2] + radius)
		return false;
	if (maxs[0] < origin[0] - radius || maxs[1] < origin[1] - radius || maxs[2] < origin[2] - radius)
		return false;

	return true;
}

/*
===========
BoundsIntersect

===========
*/
bool BoundsIntersect (const vec3_t mins1, const vec3_t maxs1, const vec3_t mins2, const vec3_t maxs2) {
	
	if (r_noCull->value)
		return false;

	if (mins1[0] > maxs2[0] || mins1[1] > maxs2[1] || mins1[2] > maxs2[2])
		return false;
	if (maxs1[0] < mins2[0] || maxs1[1] < mins2[1] || maxs1[2] < mins2[2])
		return false;

	return true;
}
/*
=================
R_CullBox

Returns true if the box is completely outside the frustom
=================
*/
bool R_CullBox (vec3_t mins, vec3_t maxs) {
	int i;

	if (r_noCull->value)
		return false;

	for (i = 0; i < 6; i++)
	if (BOX_ON_PLANE_SIDE (mins, maxs, &frustum[i]) == 2)
		return true;
	return false;
}

bool R_CullConeLight (vec3_t mins, vec3_t maxs, cplane_t *frust) {
	int		i;

	if (r_noCull->value)
		return false;

	for (i = 0; i < 4; i++)
	if (BoxOnPlaneSide22 (mins, maxs, &frust[i]) == 2)
		return true;
	return false;
}

/*
=================
R_CullOrigin

Returns true if the origin is completely outside the frustom
=================
*/
bool R_CullOrigin (vec3_t origin) {
	int i;

	if (r_noCull->value)
		return false;

	for (i = 0; i < 6; i++)
	if (BOX_ON_PLANE_SIDE (origin, origin, &frustum[i]) == 2)
		return true;
	return false;
}


bool R_CullPoint (vec3_t org) {
	int i;

	if (r_noCull->value)
		return false;

	for (i = 0; i < 6; i++)
	if (DotProduct (org, frustum[i].normal) > frustum[i].dist)
		return true;

	return false;
}

bool R_CullSphere (const vec3_t centre, const float radius) {
	int		i;
	cplane_t *p;

	if (r_noCull->value)
		return false;

	for (i = 0, p = frustum; i < 6; i++, p++)
	{
		if (DotProduct (centre, p->normal) - p->dist <= -radius)
			return true;
	}

	return false;
}

bool BoundsIntersectsPoint (vec3_t mins, vec3_t maxs, vec3_t p) {
	
	if (r_noCull->value)
		return false;

	if (p[0] > maxs[0]) return false;
	if (p[1] > maxs[1]) return false;
	if (p[2] > maxs[2]) return false;

	if (p[0] < mins[0]) return false;
	if (p[1] < mins[1]) return false;
	if (p[2] < mins[2]) return false;

	return true;
}

int SignbitsForPlane (cplane_t * out) {
	int bits, j;

	// for fast box on planeside test

	bits = 0;
	for (j = 0; j < 3; j++) {
		if (out->normal[j] < 0)
			bits |= 1 << j;
	}
	return bits;
}


void R_SetFrustum () {
	int i;

	RotatePointAroundVector (frustum[0].normal, vup, vpn,    -(90 - r_newrefdef.fov_x * 0.5));
	RotatePointAroundVector (frustum[1].normal, vup, vpn, 	   90 - r_newrefdef.fov_x * 0.5);
	RotatePointAroundVector (frustum[2].normal, vright, vpn,   90 - r_newrefdef.fov_y * 0.5);
	RotatePointAroundVector (frustum[3].normal, vright, vpn, -(90 - r_newrefdef.fov_y * 0.5));
	VectorCopy	(vpn, frustum[4].normal); 
	VectorNegate(vpn, frustum[5].normal); 

	for (i = 0; i < 6; i++) {
		VectorNormalize(frustum[i].normal);

		frustum[i].type = PLANE_ANYZ;
		frustum[i].dist = DotProduct (r_origin, frustum[i].normal);
		frustum[i].signbits = SignbitsForPlane (&frustum[i]);
	}

	frustum[4].dist += r_zNear->value;
	
	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		frustum[5].dist -= 128.0;
	else
		frustum[5].dist -= r_zFar->value;
}

