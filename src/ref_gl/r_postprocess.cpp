/*
Copyright (C) 2006-2011 Quake2xp Team

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "r_local.h"

#include "r_program.h"

#include "../win32/winquake.h"


/*
====================
GLSL Full Screen
Post Process Effects
====================
*/

void R_DrawFullScreenQuad () {

	glBindBuffer(GL_ARRAY_BUFFER, getVbo().vbo_fullScreenQuad);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, getVbo().ibo_quadTris);
	
	glEnableVertexAttribArray (ATT_POSITION);
	glVertexAttribPointer (ATT_POSITION, 2, GL_FLOAT, false, 0, 0);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, nullptr);

	glDisableVertexAttribArray (ATT_POSITION);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void R_DrawHalfScreenQuad () {

	glBindBuffer(GL_ARRAY_BUFFER, getVbo().vbo_halfScreenQuad);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, getVbo().ibo_quadTris);
	
	glEnableVertexAttribArray(ATT_POSITION);
	glVertexAttribPointer(ATT_POSITION, 2, GL_FLOAT, false, 0, 0);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, nullptr);

	glDisableVertexAttribArray(ATT_POSITION);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void R_DrawQuarterScreenQuad () {
	
	glBindBuffer(GL_ARRAY_BUFFER, getVbo().vbo_quarterScreenQuad);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, getVbo().ibo_quadTris);
	
	glEnableVertexAttribArray(ATT_POSITION);
	glVertexAttribPointer(ATT_POSITION, 2, GL_FLOAT, false, 0, 0);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, nullptr);

	glDisableVertexAttribArray(ATT_POSITION);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void R_Bloom () 
{
	if (!r_bloom->value)
		return;

	if (r_newrefdef.rdflags & (RDF_NOWORLDMODEL | RDF_IRGOGGLES))
		return;

	// downsample and cut color
	GL_MBindRect (GL_TEXTURE0, ScreenMap->texnum);
	glCopyTexSubImage2D (GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth(), videoHeight());

	// setup program
	GL_BindProgram (bloomdsProgram, 0);
	glUniform1f(bloomDS_threshold, r_bloomThreshold->value);
	glUniformMatrix4fv(bloomDS_matrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	R_DrawQuarterScreenQuad ();

	// create bloom texture (set to zero in default state)
	if (!bloomtex) {
		glGenTextures (1, &bloomtex);
		GL_BindRect (bloomtex);
		glTexParameteri (GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri (GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glCopyTexImage2D (GL_TEXTURE_RECTANGLE, 0, GL_RGB, 0, 0, videoWidth() * 0.25, videoHeight() * 0.25, 0);
	}

	// generate star shape
	GL_BindRect (bloomtex);
	glCopyTexSubImage2D (GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth() * 0.25, videoHeight() * 0.25);

	GL_BindProgram (glareProgram, 0);
	glUniform1f(glare_params, r_bloomWidth->value);
	glUniformMatrix4fv(glare_matrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	R_DrawQuarterScreenQuad ();
	glCopyTexSubImage2D (GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth() * 0.25, videoHeight() * 0.25);

	// blur x
	GL_BindRect (bloomtex);
	glCopyTexSubImage2D (GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth() * 0.25, videoHeight() * 0.25);

	GL_BindProgram (gaussXProgram, 0);
	glUniformMatrix4fv(gaussx_matrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	R_DrawQuarterScreenQuad ();

	// blur y
	GL_BindRect (bloomtex);
	glCopyTexSubImage2D (GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth() * 0.25, videoHeight() * 0.25);

	GL_BindProgram (gaussYProgram, 0);
	glUniformMatrix4fv(gaussy_matrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	R_DrawQuarterScreenQuad ();

	// store 2 pass gauss blur 
	glCopyTexSubImage2D (GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth() * 0.25, videoHeight() * 0.25);

	//final pass
	GL_BindProgram (bloomfpProgram, 0);
	GL_MBindRect (GL_TEXTURE0, ScreenMap->texnum);
	GL_MBindRect (GL_TEXTURE1, bloomtex);
	glUniform1f(bloomFP_params, r_bloomIntens->value);
	glUniformMatrix4fv(bloom_FP_matrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	R_DrawFullScreenQuad ();

	GL_BindNullProgram ();
}



void R_ThermalVision () 
{
	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		return;

	if (!(r_newrefdef.rdflags & RDF_IRGOGGLES))
		return;

	if (!thermaltex) {
		glGenTextures (1, &thermaltex);
		GL_MBindRect(GL_TEXTURE0, thermaltex);
		glTexParameteri (GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri (GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glCopyTexImage2D (GL_TEXTURE_RECTANGLE, 0, GL_RGB, 0, 0, videoWidth(), videoHeight(), 0);
	}
	else {
		GL_MBindRect(GL_TEXTURE0, thermaltex);
		glCopyTexSubImage2D (GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth(), videoHeight());
	}

	// setup program
	GL_BindProgram (thermalProgram, 0);
	glUniformMatrix4fv(therm_matrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	R_DrawHalfScreenQuad ();

	// blur x
	GL_BindRect (thermaltex);
	glCopyTexSubImage2D (GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth() / 2, videoHeight() / 2);

	GL_BindProgram (gaussXProgram, 0);
	glUniformMatrix4fv(gaussx_matrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	R_DrawHalfScreenQuad ();

	// blur y
	GL_BindRect (thermaltex);
	glCopyTexSubImage2D (GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth() / 2, videoHeight() / 2);

	GL_BindProgram (gaussYProgram, 0);
	glUniformMatrix4fv(gaussy_matrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	R_DrawHalfScreenQuad ();

	// store 2 pass gauss blur 
	glCopyTexSubImage2D (GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth() / 2, videoHeight() / 2);

	//final pass
	GL_BindProgram (thermalfpProgram, 0);

	GL_BindRect (thermaltex);
	glCopyTexSubImage2D (GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth(), videoHeight());
	glUniformMatrix4fv(thermf_matrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	R_DrawFullScreenQuad ();

	GL_BindNullProgram ();
}



void R_RadialBlur () 
{
	float	blur;

	if (!r_radialBlur->value)
		return;

	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		return;
	
	if (r_newrefdef.fov_x <= r_radialBlurFov->value)
		goto hack;

	if (r_newrefdef.rdflags & (RDF_UNDERWATER | RDF_PAIN)) {

	hack:

	
		GL_MBindRect (GL_TEXTURE0, ScreenMap->texnum);
		glCopyTexSubImage2D (GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth(), videoHeight());

		// setup program
		GL_BindProgram (radialProgram, 0);

		if (r_newrefdef.rdflags & RDF_UNDERWATER)
			blur = 0.0065;
		else
			blur = 0.01;

		// xy = radial center screen space position, z = radius attenuation, w = blur strength
		glUniform4f(rb_params, videoWidth() / 2, videoHeight() / 2, 1.0 / videoHeight(), blur);
		glUniformMatrix4fv(rb_matrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

		R_DrawFullScreenQuad ();

		GL_BindNullProgram ();
	}
}

extern float v_blend[4];

void R_ScreenBlend()
{
	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL) { return; }

	if (!v_blend[3]) { return; }

	GL_MBindRect(GL_TEXTURE0, ScreenMap->texnum);
	glCopyTexSubImage2D(GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth(), videoHeight());

	// setup program
	GL_BindProgram(genericProgram, 0);

	glUniform1i(gen_attribColors, 0);
	glUniform1i(gen_sky, 0);
	glUniform1i(gen_3d, 0);
	glUniform4f(gen_color, v_blend[0], v_blend[1], v_blend[2], v_blend[3]);
	glUniformMatrix4fv(gen_orthoMatrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	GL_Enable(GL_BLEND);

	R_DrawFullScreenQuad();

	GL_Disable(GL_BLEND);

	GL_BindNullProgram();

}


void R_DofBlur () 
{
	float			tmpDist[5], tmpMins[3];
	vec2_t          dofParams;
	trace_t			trace;
	vec3_t			end_trace, v_f, v_r, v_up, tmp, left, right, up, dn;

	if (!r_dof->value)
		return;
	if (r_newrefdef.rdflags & (RDF_NOWORLDMODEL | RDF_IRGOGGLES))
		return;

	//dof autofocus
	if (!r_dofFocus->value) {

		AngleVectors (r_newrefdef.viewangles, v_f, v_r, v_up);
		VectorScaleAndAdd (r_newrefdef.vieworg, v_f, end_trace, 4096);

		VectorScaleAndAdd (end_trace, v_r, right, 96);
		VectorScaleAndAdd (end_trace, v_r, left, -96);
		VectorScaleAndAdd (end_trace, v_up, up, 96);
		VectorScaleAndAdd (end_trace, v_up, dn, -96);

		trace = CL_PMTraceWorld (r_newrefdef.vieworg, vec3_origin, vec3_origin, right, MASK_SHOT, true);
		VectorSubtract (trace.endpos, r_newrefdef.vieworg, tmp);
		tmpDist[0] = VectorLength (tmp);

		trace = CL_PMTraceWorld (r_newrefdef.vieworg, vec3_origin, vec3_origin, left, MASK_SHOT, true);
		VectorSubtract (trace.endpos, r_newrefdef.vieworg, tmp);
		tmpDist[1] = VectorLength (tmp);

		trace = CL_PMTraceWorld (r_newrefdef.vieworg, vec3_origin, vec3_origin, up, MASK_SHOT, true);
		VectorSubtract (trace.endpos, r_newrefdef.vieworg, tmp);
		tmpDist[2] = VectorLength (tmp);

		trace = CL_PMTraceWorld (r_newrefdef.vieworg, vec3_origin, vec3_origin, dn, MASK_SHOT, true);
		VectorSubtract (trace.endpos, r_newrefdef.vieworg, tmp);
		tmpDist[3] = VectorLength (tmp);

		trace = CL_PMTraceWorld (r_newrefdef.vieworg, vec3_origin, vec3_origin, end_trace, MASK_SHOT, true);
		VectorSubtract (trace.endpos, r_newrefdef.vieworg, tmp);
		tmpDist[4] = VectorLength (tmp);

		tmpMins[0] = min (tmpDist[0], tmpDist[1]);
		tmpMins[1] = min (tmpDist[2], tmpDist[3]);
		tmpMins[2] = min (tmpMins[0], tmpMins[1]);

		dofParams[0] = min (tmpMins[2], tmpDist[4]);
		dofParams[1] = r_dofBias->value;
	}
	else {
		dofParams[0] = r_dofFocus->value;
		dofParams[1] = r_dofBias->value;
	}

	// setup program
	GL_BindProgram (dofProgram, 0);
	glUniform2f(dof_screenSize, videoWidth(), videoHeight());
	glUniform4f (dof_params, dofParams[0], dofParams[1], r_newrefdef.depthParms[0], r_newrefdef.depthParms[1]);
	glUniformMatrix4fv(dof_orthoMatrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	GL_MBindRect (GL_TEXTURE0, ScreenMap->texnum);
	glCopyTexSubImage2D (GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth(), videoHeight());
	GL_MBindRect (GL_TEXTURE1, depthMap->texnum);

	R_DrawFullScreenQuad ();

	GL_BindNullProgram ();
}

void R_FXAA () {

	if (!r_fxaa->value)
		return;

	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		return;

	// setup program
	GL_BindProgram (fxaaProgram, 0);

	if (!fxaatex) {
		glGenTextures (1, &fxaatex);
		GL_MBind (GL_TEXTURE0, fxaatex);
		glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glCopyTexImage2D (GL_TEXTURE_2D, 0, GL_RGB, 0, 0, videoWidth(), videoHeight(), 0);
	}
	GL_MBind (GL_TEXTURE0, fxaatex);
	glCopyTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, 0, 0, videoWidth(), videoHeight());

	glUniform2f(fxaa_screenSize, videoWidth(), videoHeight());
	glUniformMatrix4fv(fxaa_orthoMatrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	R_DrawFullScreenQuad ();

	GL_BindNullProgram ();

}

void R_FilmFilter () 
{

	if (!r_filmFilterType->value)
		return;

	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		return;
	 
	// setup program
	GL_BindProgram (filmGrainProgram, 0);

	GL_MBindRect (GL_TEXTURE0, ScreenMap->texnum);
	glCopyTexSubImage2D (GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth(), videoHeight());

	glUniform2f (film_screenRes, videoWidth(), videoHeight());
	glUniform1f (film_rand, crand());
	glUniform1i (film_frameTime, r_framecount);
	glUniform4f (film_params,	r_filmFilterType->value, r_filmFilterNoiseIntens->value, 
								r_filmFilterScratchIntens->value, r_filmFilterVignetIntens->value);

	glUniformMatrix4fv(film_matrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	R_DrawFullScreenQuad ();

	GL_BindNullProgram ();
}

void R_GammaRamp () 
{
	GL_BindProgram (gammaProgram, 0);

	GL_MBindRect (GL_TEXTURE0, ScreenMap->texnum);
	glCopyTexSubImage2D (GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth(), videoHeight());

	glUniform4f (gamma_control, r_brightness->value, r_contrast->value, r_saturation->value, 1 / r_gamma->value);
	glUniformMatrix4fv(gamma_orthoMatrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	R_DrawFullScreenQuad ();

	GL_BindNullProgram ();
}

void R_MotionBlur () 
{
	vec2_t	angles, delta;

	if (r_newrefdef.rdflags & (RDF_NOWORLDMODEL | RDF_IRGOGGLES))
		return;

	// calc camera offsets
	angles[0] = r_newrefdef.viewangles_old[0] - r_newrefdef.viewangles[0]; //YAW left-right
	angles[1] = r_newrefdef.viewangles_old[1] - r_newrefdef.viewangles[1]; //PITCH up-down
	delta[0] = (angles[0] * 2.0 / r_newrefdef.fov_x) * r_motionBlurFrameLerp->value;
	delta[1] = (angles[1] * 2.0 / r_newrefdef.fov_y) * r_motionBlurFrameLerp->value;

	// setup program
	GL_BindProgram(motionBlurProgram, 0);

	glUniform3f(mb_params, delta[0], delta[1], r_motionBlurSamples->value);
	glUniformMatrix4fv(mb_orthoMatrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	GL_MBindRect (GL_TEXTURE0, ScreenMap->texnum);
	glCopyTexSubImage2D(GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth(), videoHeight());

	R_DrawFullScreenQuad ();

	GL_BindNullProgram ();
}

void R_DownsampleDepth() 
{
	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		return;

	if (r_newrefdef.rdflags & RDF_IRGOGGLES)
		return;

	if (!r_ssao->value)
		return;

	GL_DepthRange(0.0, 1.0);
	// downsample the depth buffer
	glBindFramebuffer(GL_FRAMEBUFFER, fboId);
	glDrawBuffer(GL_COLOR_ATTACHMENT2);

	GL_BindProgram(depthDownsampleProgram, 0);
	GL_MBindRect(GL_TEXTURE0, depthMap->texnum);

	glUniform2f(depthDS_params, r_newrefdef.depthParms[0], r_newrefdef.depthParms[1]);
	glUniformMatrix4fv(depthDS_orthoMatrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	R_DrawHalfScreenQuad();

	// restore settings
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	GL_BindNullProgram();
}

void R_SSAO () 
{
	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL) { return; }
	if (r_newrefdef.rdflags & RDF_IRGOGGLES) { return; }
	if (!r_ssao->value) { return; }
	
	// process
	glBindFramebuffer(GL_FRAMEBUFFER, fboId);
	glDrawBuffer(GL_COLOR_ATTACHMENT0);

	GL_BindProgram (ssaoProgram, 0);
	GL_MBindRect(GL_TEXTURE0, fboDN->texnum);
	GL_MBind(GL_TEXTURE1, r_randomNormalTex->texnum);

	glUniform2f (ssao_params, max(r_ssaoIntensity->value, 0.f), r_ssaoScale->value);
	glUniform2f (ssao_vp, videoWidth(), videoHeight());
	glUniformMatrix4fv(ssao_orthoMatrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	R_DrawHalfScreenQuad();

	// blur
	fboColorIndex = 0;

	if (r_ssaoBlur->value)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, fboId);
		GL_MBindRect(GL_TEXTURE1, fboDN->texnum);

		GL_BindProgram(ssaoBlurProgram, 0);

		glUniformMatrix4fv(ssaoB_orthoMatrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

		int numSamples = (int)rintf(4.f * videoHeight() / 1080.f);
		glUniform1i(ssaoB_sapmles, max(numSamples, 1));

		for (int i = 0; i < static_cast<int>(r_ssaoBlur->value); ++i)
		{
			// two-pass shader
			for (int j = 0; j < 2; j++)
			{
				GL_MBindRect(GL_TEXTURE0, fboColor[j]->texnum);
				glDrawBuffer(GL_COLOR_ATTACHMENT0 + (j ^ 1));
				glUniform2f(ssaoB_axisMask, j ? 0.f : 1.f, j ? 1.f : 0.f);
				R_DrawHalfScreenQuad();
			}
		}
	}

	// restore
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	GL_BindNullProgram ();
}

/*
===========================================
 Based on Giliam de Carpentier work
 http://www.decarpentier.nl/lens-distortion
===========================================
*/

void R_FixFov()
{
	if (!r_fixFovStrength->value) { return; }
	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL) { return; }

	// setup program
	GL_BindProgram(fixFovProgram, 0);

	if (!fovCorrTex)
	{
		glGenTextures(1, &fovCorrTex);
		GL_MBind(GL_TEXTURE0, fovCorrTex);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 0, 0, videoWidth(), videoHeight(), 0);
	}

	GL_MBind(GL_TEXTURE0, fovCorrTex);
	glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, videoWidth(), videoHeight());

	vec4_t params = { 0.0f };
	params[0] = r_fixFovStrength->value;
	params[1] = tan(DEG2RAD(r_newrefdef.fov_x) / 2.0f) / (videoWidth() / videoHeight());
	params[2] = videoWidth() / videoHeight();
	params[3] = r_fixFovDistroctionRatio->value;

	glUniform4fv(fixfov_params, 1, params);
	glUniformMatrix4fv(fixfov_orthoMatrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	R_DrawFullScreenQuad();
	GL_BindNullProgram();
}


void R_MenuBackGround()
{
	GL_BindProgram(gammaProgram, 0);

	GL_MBindRect(GL_TEXTURE0, ScreenMap->texnum);
	glCopyTexSubImage2D(GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth(), videoHeight());

	glUniform4f(gamma_control, 0.5f, 1.0f, 1.0f, 1.0f);
	glUniformMatrix4fv(gamma_orthoMatrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	R_DrawFullScreenQuad();

	// blur x
	GL_BindRect(ScreenMap->texnum);
	glCopyTexSubImage2D(GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth(), videoHeight());

	GL_BindProgram(gaussXProgram, 0);
	glUniformMatrix4fv(gaussx_matrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	R_DrawFullScreenQuad();

	// blur y
	GL_BindRect(ScreenMap->texnum);
	glCopyTexSubImage2D(GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth(), videoHeight());

	GL_BindProgram(gaussYProgram, 0);
	glUniformMatrix4fv(gaussy_matrix, 1, false, (const float *)r_newrefdef.orthoMatrix);

	R_DrawFullScreenQuad();

	// store 2 pass gauss blur 
	glCopyTexSubImage2D(GL_TEXTURE_RECTANGLE, 0, 0, 0, 0, 0, videoWidth(), videoHeight());
	GL_BindNullProgram();
}
