#include "r_local.h"

#include "r_math.h"
#include "r_program.h"


extern msurface_t	*scene_surfaces[MAX_MAP_FACES];
static int			num_depth_surfaces;
static vec3_t		modelorg;			// relative to viewpoint

bool R_FillDepthBatch (msurface_t *surf, unsigned *vertices, unsigned *indeces) {
	unsigned	numVertices, numIndices;
	int			i, nv = surf->polys->numVerts;

	numVertices = *vertices;
	numIndices = *indeces;

	if (numVertices + nv > MAX_BATCH_SURFS)
		return false;

	// create indexes
	if (numIndices == 0xffffffff)
		numIndices = 0;

	for (i = 0; i < nv - 2; i++)
	{
		indexArray[numIndices++] = surf->baseIndex;
		indexArray[numIndices++] = surf->baseIndex + i + 1;
		indexArray[numIndices++] = surf->baseIndex + i + 2;
	}
	
	*vertices = numVertices;
	*indeces = numIndices;

	return true;
}

static void GL_DrawDepthPoly () {
	msurface_t	*s;
	int			i;
	unsigned	numIndices = 0xffffffff;
	unsigned	numVertices = 0;

	for (i = 0; i < num_depth_surfaces; i++) {
		s = scene_surfaces[i];

		if (!R_FillDepthBatch (s, &numVertices, &numIndices)) {
			if (numIndices != 0xFFFFFFFF) {
				glDrawElements (GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, indexArray);
				numVertices = 0;
				numIndices = 0xFFFFFFFF;
			}
		}
	}

	// draw the rest
	if (numIndices != 0xFFFFFFFF) {
		glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, indexArray);
	}
}

static void R_RecursiveDepthWorldNode(mnode_t * node) {
	int c, side, sidebit;
	cplane_t *plane;
	msurface_t *surf, **mark;
	mleaf_t *pleaf;
	float dot;

	if (node->contents == CONTENTS_SOLID)
		return;					// solid

	if (node->visframe != r_visframecount)
		return;

	if (R_CullBox(node->minmaxs, node->minmaxs + 3))
		return;

	// if a leaf node, draw stuff
	if (node->contents != -1)
	{
		pleaf = (mleaf_t *)node;

		// check for door connected areas
		if (r_newrefdef.areabits)
		{
			if (!(r_newrefdef.areabits[pleaf->area >> 3] & (1 << (pleaf->area & 7))))
				return;			// not visible
		}

		mark = pleaf->firstmarksurface;
		c = pleaf->numMarkSurfaces;

		if (c)
		{
			do
			{
				if (SurfInFrustum(*mark))
				{
					(*mark)->visframe = r_framecount;
				}
				(*mark)->ent = nullptr;
				mark++;
			} while (--c);
		}

		return;
	}
	// node is just a decision point, so go down the apropriate sides
	// find which side of the node we are on
	plane = node->plane;

	switch (plane->type)
	{
	case PLANE_X:
		dot = modelorg[0] - plane->dist;
		break;

	case PLANE_Y:
		dot = modelorg[1] - plane->dist;
		break;

	case PLANE_Z:
		dot = modelorg[2] - plane->dist;
		break;

	default:
		dot = DotProduct(modelorg, plane->normal) - plane->dist;
		break;
	}

	if (dot >= 0)
	{
		side = 0;
		sidebit = 0;
	}
	else
	{
		side = 1;
		sidebit = MSURF_PLANEBACK;
	}

	// recurse down the children, front side first
	R_RecursiveDepthWorldNode(node->children[side]);

	// draw stuff
	for (c = node->numsurfaces, surf = r_worldmodel->surfaces + node->firstsurface; c; c--, surf++)
	{

		if (surf->visframe != r_framecount) { continue; }

		if ((surf->flags & MSURF_PLANEBACK) != sidebit) { continue; } // wrong side

		if (surf->texInfo->flags & SURF_SKY) { R_AddSkySurface(surf); } // just adds to visible sky bounds
		else if (surf->texInfo->flags & SURF_NODRAW) { continue; }
		else if (surf->texInfo->flags & (SURF_ALPHA)) { continue; }
		else { scene_surfaces[num_depth_surfaces++] = surf; }
	}

	// recurse down the back side
	R_RecursiveDepthWorldNode(node->children[!side]);
}


static void R_AddBModelDepthPolys () {
	int i;
	cplane_t *pplane;
	float dot;
	msurface_t *psurf;

	psurf = &CURRENT_MODEL->surfaces[CURRENT_MODEL->firstModelSurface];

	for (i = 0; i < CURRENT_MODEL->numModelSurfaces; i++, psurf++) {
		// find which side of the node we are on
		pplane = psurf->plane;
		if (pplane->type < 3)
			dot = modelorg[pplane->type] - pplane->dist;
		else
			dot = DotProduct (modelorg, pplane->normal) - pplane->dist;

		// draw the polygon
		if (((psurf->flags & MSURF_PLANEBACK) && (dot < -BACKFACE_EPSILON))
			|| (!(psurf->flags & MSURF_PLANEBACK) && (dot > BACKFACE_EPSILON))) {

			if (psurf->visframe == r_framecount)	// reckless fix
				continue;

			if (psurf->texInfo->flags & (SURF_ALPHA)) {
				continue;
			}
			scene_surfaces[num_depth_surfaces++] = psurf;
		}
	}
}
void R_DrawDepthBrushModel () {
	vec3_t		mins, maxs;
	int			i;
	bool	rotated;
	mat4_t		mvp;

	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		return;

	if (!r_drawEntities->value)
		return;

	if (CURRENT_MODEL->numModelSurfaces == 0)
		return;

	if (CURRENT_ENTITY->angles[0] || CURRENT_ENTITY->angles[1] || CURRENT_ENTITY->angles[2]) {
		rotated = true;
		for (i = 0; i < 3; i++) {
			mins[i] = CURRENT_ENTITY->origin[i] - CURRENT_MODEL->radius;
			maxs[i] = CURRENT_ENTITY->origin[i] + CURRENT_MODEL->radius;
		}
	}
	else {
		rotated = false;
		VectorAdd (CURRENT_ENTITY->origin, CURRENT_MODEL->mins, mins);
		VectorAdd (CURRENT_ENTITY->origin, CURRENT_MODEL->maxs, maxs);
	}

	if (R_CullBox (mins, maxs))
		return;

	VectorSubtract (r_newrefdef.vieworg, CURRENT_ENTITY->origin, modelorg);

	if (rotated) {
		vec3_t temp;
		vec3_t forward, right, up;

		VectorCopy (modelorg, temp);
		AngleVectors (CURRENT_ENTITY->angles, forward, right, up);
		modelorg[0] = DotProduct (temp, forward);
		modelorg[1] = -DotProduct (temp, right);
		modelorg[2] = DotProduct (temp, up);
	}

	R_SetupEntityMatrix (CURRENT_ENTITY);

	Mat4_TransposeMultiply(CURRENT_ENTITY->matrix, r_newrefdef.modelViewProjectionMatrix, mvp);
	glUniformMatrix4fv(null_mvp, 1, false, (const float *)mvp);

	glBindBuffer(GL_ARRAY_BUFFER, getVbo().vbo_BSP);
	glEnableVertexAttribArray(ATT_POSITION);
	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, BUFFER_OFFSET(getVbo().xyz_offset));

	num_depth_surfaces = 0;
	R_AddBModelDepthPolys ();
	GL_DrawDepthPoly();

	glDisableVertexAttribArray (ATT_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void R_CalcAliasFrameLerp (dmd2header_t *paliashdr, float shellScale);
extern vec3_t	tempVertexArray[MAX_VERTICES * 4];

void GL_DrawAliasFrameLerpDepth(dmd2header_t *paliashdr) {
	vec3_t		vertexArray[3 * MAX_TRIANGLES];
	int			index_xyz;
	int			i, j, jj = 0;
	dmd2vertex_t	*tris;

	if (CURRENT_ENTITY->flags & (RF_VIEWERMODEL))
		return;


	R_CalcAliasFrameLerp(paliashdr, 0);

	glEnableVertexAttribArray(ATT_POSITION);
	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, vertexArray);
	Mat4_TransposeMultiply(CURRENT_ENTITY->matrix, r_newrefdef.modelViewProjectionMatrix, CURRENT_ENTITY->orMatrix);
	glUniformMatrix4fv(null_mvp, 1, false, (const float *)CURRENT_ENTITY->orMatrix);

	tris = (dmd2vertex_t *)((byte *)paliashdr + paliashdr->ofs_tris);
	jj = 0;

	for (i = 0; i < paliashdr->num_tris; i++) {
		for (j = 0; j < 3; j++, jj++) {
			index_xyz = tris[i].index_xyz[j];
			VectorCopy(tempVertexArray[index_xyz], vertexArray[jj]);
		}
	}

	glDrawArrays(GL_TRIANGLES, 0, jj);

	glDisableVertexAttribArray(ATT_POSITION);
}


void R_DrawDepthAliasMD3Model()
{

}


void R_DrawDepthAliasMD2Model()
{
	dmd2header_t* paliashdr = nullptr;
	vec3_t bbox[8];

	if (!r_drawEntities->value)
		return;

	if (R_CullAliasModel(bbox, CURRENT_ENTITY))
		return;

	paliashdr = (dmd2header_t *)CURRENT_MODEL->extraData;

	if ((CURRENT_ENTITY->frame >= paliashdr->num_frames) || (CURRENT_ENTITY->frame < 0))
	{
		Com_Printf("R_DrawDepthAliasMD2Model %s: no such frame %d\n", CURRENT_MODEL->name, CURRENT_ENTITY->frame);
		CURRENT_ENTITY->frame = 0;
		CURRENT_ENTITY->oldframe = 0;
	}

	if ((CURRENT_ENTITY->oldframe >= paliashdr->num_frames) || (CURRENT_ENTITY->oldframe < 0))
	{
		Com_Printf("R_DrawDepthAliasMD2Model %s: no such oldframe %d\n", CURRENT_MODEL->name, CURRENT_ENTITY->oldframe);
		CURRENT_ENTITY->frame = 0;
		CURRENT_ENTITY->oldframe = 0;
	}

	R_SetupEntityMatrix(CURRENT_ENTITY);
	GL_DrawAliasFrameLerpDepth(paliashdr);
}


void R_DrawDepthScene()
{
	int i;

	if (!r_drawWorld->value)
		return;

	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		return;

	CURRENT_MODEL = r_worldmodel;

	VectorCopy(r_newrefdef.vieworg, modelorg);

	R_ClearSkyBox();

	GL_BindProgram(nullProgram, 0);

	glBindBuffer(GL_ARRAY_BUFFER, getVbo().vbo_BSP);
	glEnableVertexAttribArray(ATT_POSITION);
	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, BUFFER_OFFSET(getVbo().xyz_offset));
	glUniformMatrix4fv(null_mvp, 1, false, (const float *)r_newrefdef.modelViewProjectionMatrix);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); //debug tool

	num_depth_surfaces = 0;
	R_RecursiveDepthWorldNode(r_worldmodel->nodes);
	GL_DrawDepthPoly();

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(ATT_POSITION);

	R_DrawSkyBox(false);

	for (i = 0; i < r_newrefdef.num_entities; i++)
	{
		CURRENT_ENTITY = &r_newrefdef.entities[i];
		CURRENT_MODEL = CURRENT_ENTITY->model;

		if (!CURRENT_MODEL)
			continue;
		if (CURRENT_ENTITY->flags & RF_TRANSLUCENT)
			continue;
		if (CURRENT_ENTITY->flags & RF_WEAPONMODEL)
			continue;
		if (CURRENT_ENTITY->flags & (RF_SHELL_RED | RF_SHELL_GREEN | RF_SHELL_BLUE | RF_SHELL_DOUBLE | RF_SHELL_HALF_DAM | RF_SHELL_GOD))
			continue;
		if (CURRENT_ENTITY->flags & RF_DISTORT)
			continue;

		if (CURRENT_MODEL->type == mod_brush)
			R_DrawDepthBrushModel();

		if (CURRENT_MODEL->type == mod_alias_md2)
			R_DrawDepthAliasMD2Model();
		else if(CURRENT_MODEL->type == mod_alias_md3)
			R_DrawDepthAliasMD3Model();
	}

	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	GL_BindNullProgram();
}
