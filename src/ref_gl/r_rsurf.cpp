/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
// GL_RSURF.C: surface-related refresh code
//#include <assert.h>

#include "r_local.h"

#include "r_bsp.h"
#include "r_image.h"
#include "r_math.h"
#include "r_program.h"

#include "../win32/winquake.h"


static vec3_t modelorg;				// relative to viewpoint

msurface_t *r_alpha_surfaces;		// all non-entity BSP surfaces with TRANS33/66
msurface_t *r_reflective_surfaces;	// all non-entity BSP surfaces with WARP

msurface_t* interaction[MAX_MAP_FACES];
int numInteractionSurfs;


vec4_t	color_black = { 0.f, 0.f, 0.f, 0.f };

vec3_t	wVertexArray[MAX_BATCH_SURFS];
float	wTexArray[MAX_BATCH_SURFS][2];
float	wLMArray[MAX_BATCH_SURFS][2];
vec4_t	wColorArray[MAX_BATCH_SURFS];

float   wTmu0Array[MAX_BATCH_SURFS][2];
float   wTmu1Array[MAX_BATCH_SURFS][2];
float   wTmu2Array[MAX_BATCH_SURFS][2];

size_t	indexArray[MAX_MAP_VERTS * 3];
vec3_t	nTexArray[MAX_BATCH_SURFS];
vec3_t	tTexArray[MAX_BATCH_SURFS];
vec3_t	bTexArray[MAX_BATCH_SURFS];


byte viewvis[MAX_MAP_LEAFS / 8];


// =============================================================
// = BRUSH MODELS
// =============================================================

/**
 * Returns the proper texture for a given time and base texture
 */
Image* R_TextureAnimation(mtexInfo_t * tex)
{
	if (!tex->next) { return tex->diffuse; }

	int c = CURRENT_ENTITY->frame % tex->numFrames;
	while (c)
	{
		tex = tex->next;
		c--;
	}

	return tex->diffuse;
}


/**
 * 
 */
Image* R_TextureAnimationFx(mtexInfo_t* tex)
{
	if (!tex->next) { return tex->additive; }

	int c = CURRENT_ENTITY->frame % tex->numFrames;
	while (c)
	{
		tex = tex->next;
		c--;
	}

	return tex->additive;
}


Image* R_TextureAnimationNormal(mtexInfo_t * tex)
{
	if (!tex->next) { return tex->normal; }

	int c = CURRENT_ENTITY->frame % tex->numFrames;
	while (c)
	{
		tex = tex->next;
		c--;
	}

	return tex->normal;
}


Image* R_TextureAnimationEnv(mtexInfo_t * tex)
{
	if (!tex->next) { return tex->environment; }

	int c = CURRENT_ENTITY->frame % tex->numFrames;
	while (c) {
		tex = tex->next;
		c--;
	}

	return tex->environment;
}


Image* R_TextureAnimationRgh(mtexInfo_t * tex)
{
	if (!tex->next) { return tex->roughness; }

	int c = CURRENT_ENTITY->frame % tex->numFrames;
	while (c)
	{
		tex = tex->next;
		c--;
	}

	return tex->roughness;
}


/**
 * DrawGLPoly
 */
void DrawGLPoly(msurface_t* fa, bool scrolling)
{
	float scroll = 0.0f;

	int nv = fa->polys->numVerts;
	size_t numIndices = 0, numVertixes = 0;

	glUniform1f(ref_alpha, fa->texInfo->diffuse->alpha);

	if (scrolling)
	{
		GL_MBind(GL_TEXTURE0, r_DSTTex->texnum);
	}
	else
	{
		GL_MBind(GL_TEXTURE0, fa->texInfo->normal->texnum);
	}

	GL_MBind(GL_TEXTURE1, fa->texInfo->diffuse->texnum);
	GL_MBindRect(GL_TEXTURE2, ScreenMap->texnum);
	GL_MBindRect(GL_TEXTURE3, depthMap->texnum);

	if (scrolling)
	{
		scroll = (r_newrefdef.time * 0.15f) - (int)(r_newrefdef.time * 0.15f);
	}

	glpoly_t* p = fa->polys;
	float* v = p->verts[0];

	for (int i = 0; i < nv - 2; i++)
	{
		indexArray[numIndices++] = numVertixes;
		indexArray[numIndices++] = numVertixes + i + 1;
		indexArray[numIndices++] = numVertixes + i + 2;
	}

	for (int i = 0; i < p->numVerts; i++, v += VERTEXSIZE)
	{
		VectorCopy(v, wVertexArray[i]);

		wTexArray[i][0] = v[3] - scroll;
		wTexArray[i][1] = v[4];
	}

	glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, indexArray);
}


void R_DrawChainsRA (bool bmodel)
{
	float colorScale = max(r_lightmapScale->value, 0.33f);

	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL) { return; }

	for (msurface_t* s = r_reflective_surfaces; s; s = s->texturechain)
	{
		if (s->flags & MSURF_LAVA) { continue; }
		R_DrawWaterPolygons(s, bmodel);
	}

	r_reflective_surfaces = nullptr;

	R_CaptureColorBuffer();

	glEnableVertexAttribArray(ATT_POSITION);
	glEnableVertexAttribArray(ATT_TEX0);

	glVertexAttribPointer(ATT_POSITION, 3, GL_FLOAT, false, 0, wVertexArray);
	glVertexAttribPointer(ATT_TEX0, 2, GL_FLOAT, false, 0, wTexArray);

	// setup program
	GL_BindProgram(refractProgram, 0);

	glUniform1f(ref_deformMul, 1.0);
	glUniformMatrix4fv(ref_mvp, 1, false, (const float *)r_newrefdef.modelViewProjectionMatrix);
	glUniformMatrix4fv(ref_mvm, 1, false, (const float *)r_newrefdef.modelViewMatrix);
	glUniformMatrix4fv(ref_pm, 1, false, (const float *)r_newrefdef.projectionMatrix);

	glUniform1f(ref_thickness, 150.0);
	glUniform2f(ref_viewport, videoWidth(), videoHeight());
	glUniform2f(ref_depthParams, r_newrefdef.depthParms[0], r_newrefdef.depthParms[1]);
	glUniform1f(ref_ambientScale, colorScale);
	glUniform1i(ref_alphaMask, 0);

	for (msurface_t* s = r_alpha_surfaces; s; s = s->texturechain)
	{	
		if (s->flags & MSURF_LAVA) { continue; }

		if (s->texInfo->flags & SURF_FLOWING)
		{
			DrawGLPoly(s, true);
		}
		else
		{
			DrawGLPoly(s, false);
		}
	}

	glDisableVertexAttribArray(ATT_POSITION);
	glDisableVertexAttribArray(ATT_TEX0);

	GL_BindNullProgram();

	r_alpha_surfaces = nullptr;
}


/*
===============
SORT AND BATCH 
BSP SURFACES

===============
*/

bool R_FillAmbientBatch (msurface_t *surf, bool newBatch, unsigned *indeces, bool bmodel)
{
	unsigned	numIndices;
	int			i, nv = surf->numEdges;
	float		scroll = 0.0, scale[2];

	numIndices	= *indeces;

	if ((nv-2) * 3 >= MAX_IDX)
		return false;	// force the start new batch

	if (newBatch)
	{
		Image* image = R_TextureAnimation(surf->texInfo);
		Image* fx = R_TextureAnimationFx(surf->texInfo);
		Image* normal = R_TextureAnimationNormal(surf->texInfo);
		Image* environment = R_TextureAnimationEnv(surf->texInfo);

		glUniform1f(ambientWorld_specularScale, image->specularScale ? image->specularScale : r_ambientSpecularScale->value);
		
		if (!r_skipStaticLights->value) 
		{
			if (surf->flags & MSURF_LAVA)
				glUniform1f(ambientWorld_ambientLevel, 0.5);
			else
				glUniform1f(ambientWorld_ambientLevel, r_lightmapScale->value);
		}

		if (!image->parallaxScale)
		{
			scale[0] = r_reliefScale->value / image->width;
			scale[1] = r_reliefScale->value / image->height;
		}
		else
		{
			scale[0] = image->parallaxScale / image->width;
			scale[1] = image->parallaxScale / image->height;
		}
			glUniform4f(ambientWorld_parallaxParams, scale[0], scale[1], image->upload_width, image->upload_height);
	
		if (surf->flags & MSURF_LAVA)
			glUniform1i(ambientWorld_lava, 1);
		else
			glUniform1i(ambientWorld_lava, 0);

		GL_MBind(GL_TEXTURE0, image->texnum);
		GL_MBind(GL_TEXTURE2, fx->texnum);
		GL_MBind(GL_TEXTURE3, normal->texnum);

		if (surf->texInfo->flags & SURF_FLOWING)
		{
			scroll = -64 * ((r_newrefdef.time / 40.0) - (int)(r_newrefdef.time / 40.0));
			if (scroll == 0.0)
				scroll = -64.0;
		}

		if(!bmodel)
			glUniform1f(ambientWorld_scroll, scroll);
		else
			glUniform1f(ambientWorld_scroll, 0.0);
	}

	// create indexes
	if (numIndices == 0xffffffff)
		numIndices = 0;

	for (i = 0; i < nv - 2; i++) {
		indexArray[numIndices++] = surf->baseIndex;
		indexArray[numIndices++] = surf->baseIndex + i + 1;
		indexArray[numIndices++] = surf->baseIndex + i + 2;
		}

	*indeces	= numIndices;

	return true;	
}


int SurfSort(const msurface_t **a, const msurface_t **b)
{
	return	(((*a)->lightmaptexturenum << 26) + ((*a)->texInfo->diffuse->texnum << 13)) -
		(((*b)->lightmaptexturenum << 26) + ((*b)->texInfo->diffuse->texnum << 13));
}

vec3_t		BmodelViewOrg;
int			num_scene_surfaces;
msurface_t* scene_surfaces[MAX_MAP_FACES];


static void GL_DrawLightmappedPoly(bool bmodel)
{
	msurface_t	*s;
	int			i;
	bool	newBatch;
	unsigned	oldTex = 0xffffffff;
	unsigned	oldFlag = 0xffffffff;
	unsigned	numIndices = 0xffffffff;

	// setup program
	GL_BindProgram(ambientWorldProgram, 0);

	glUniform1f(ambientWorld_colorScale, r_textureColorScale->value);
	glUniform3fv(ambientWorld_viewOrigin, 1, bmodel ? BmodelViewOrg : r_origin);
	glUniform1i(ambientWorld_parallaxType, (int)clamp(r_reliefMapping->value, 0.0f, 1.0f));
	glUniform1f(ambientWorld_ambientLevel, r_lightmapScale->value);

	glUniform1i(ambientWorld_lightmapType, (r_worldmodel->useXPLM && r_useRadiosityBump->value) ? 1 : 0);

	if (!bmodel)
	{
		glUniformMatrix4fv(ambientWorld_mvp, 1, false, (const float *)r_newrefdef.modelViewProjectionMatrix);
	}
	else
	{
		glUniformMatrix4fv(ambientWorld_mvp, 1, false, (const float *)CURRENT_ENTITY->orMatrix);
	}

	if (r_ssao->value && !(r_newrefdef.rdflags & RDF_IRGOGGLES) && !(r_newrefdef.rdflags & RDF_NOWORLDMODEL))
	{
		GL_MBindRect(GL_TEXTURE4, fboColor[fboColorIndex]->texnum);
		glUniform1i(ambientWorld_ssao, 1);
	}
	else
		glUniform1i(ambientWorld_ssao, 0);

	qsort(scene_surfaces, num_scene_surfaces, sizeof(msurface_t*), (int(*)(const void *, const void *))SurfSort);

	for (i = 0; i < num_scene_surfaces; i++)
	{
		s = scene_surfaces[i];

		// update lightmaps
		if (gl_state.currenttextures[1] != gl_state.lightmap_textures + s->lightmaptexturenum)
		{
			GL_MBind(GL_TEXTURE1, gl_state.lightmap_textures + s->lightmaptexturenum);

			if (r_worldmodel->useXPLM && r_useRadiosityBump->value)
			{
				GL_MBind(GL_TEXTURE5, gl_state.lightmap_textures + s->lightmaptexturenum + MAX_LIGHTMAPS);
				GL_MBind(GL_TEXTURE6, gl_state.lightmap_textures + s->lightmaptexturenum + MAX_LIGHTMAPS * 2);
			}

			if (numIndices != 0xFFFFFFFF)
			{
				glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, indexArray);
				numIndices = 0xFFFFFFFF;
			}
		}

		// flush batch (new texture)
		if (s->texInfo->diffuse->texnum != oldTex)
		{
			if (numIndices != 0xFFFFFFFF)
			{
				glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, indexArray);
				numIndices = 0xFFFFFFFF;
			}

			oldTex = s->texInfo->diffuse->texnum;
			newBatch = true;
		}
		else
			newBatch = false;

		// fill new batch
		if (!R_FillAmbientBatch(s, newBatch, &numIndices, bmodel))
		{
			if (numIndices != 0xFFFFFFFF)
			{
				glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, indexArray);
				numIndices = 0xFFFFFFFF;
			}
		}
	}

	// draw the rest
	if (numIndices != 0xFFFFFFFF)
	{
		glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, indexArray);
	}

	GL_BindNullProgram();
}


int	r_lightTimestamp;


/**
 * 
 * \param bmodel	Brush model?
 */
bool R_FillLightBatch(msurface_t *surf, bool newBatch, unsigned *indeces, bool bmodel, bool caustics)
{
	unsigned numIndices;
	int i = 0, nv = surf->numEdges;
	float scroll = 0.0, scale[2];

	numIndices = *indeces;

	if ((nv - 2) * 3 >= MAX_IDX)
		return false;	// force the start new batch

	if (newBatch)
	{
		Image* image = R_TextureAnimation(surf->texInfo);
		Image* normalMap = R_TextureAnimationNormal(surf->texInfo);
		Image* rghMap = R_TextureAnimationRgh(surf->texInfo);

		glUniform1f(lightWorld_specularScale, image->specularScale ? image->specularScale : r_specularScale->value);
		glUniform1f(lightWorld_roughnessScale, image->rghScale ? image->rghScale : 0.666); // HAIL SATAN! <- Cute

		if (!rghMap || rghMap == r_notexture)
		{
			glUniform1i(lightWorld_isRgh, 0);
		}
		else
		{
			glUniform1i(lightWorld_isRgh, 1);
			GL_MBind(GL_TEXTURE4, rghMap->texnum);
		}

		if (bmodel)
		{
			if (caustics && currentShadowLight->castCaustics)
			{
				glUniform1i(lightWorld_caustics, 1);
				GL_MBind(GL_TEXTURE3, r_caustic[((int)(r_newrefdef.time * 15)) & (MAX_CAUSTICS - 1)]->texnum);
			}
			else
				glUniform1i(lightWorld_caustics, 0);
	
		}
		else
		{
			if ((surf->flags & MSURF_WATER) && currentShadowLight->castCaustics)
			{
				glUniform1i(lightWorld_caustics, 1);
				GL_MBind(GL_TEXTURE3, r_caustic[((int)(r_newrefdef.time * 15)) & (MAX_CAUSTICS - 1)]->texnum);
			}
			else
				glUniform1i(lightWorld_caustics, 0);
		}

		if (!image->parallaxScale)
		{
			scale[0] = r_reliefScale->value / image->width;
			scale[1] = r_reliefScale->value / image->height;
		}
		else
		{
			scale[0] = image->parallaxScale / image->width;
			scale[1] = image->parallaxScale / image->height;
		}

		glUniform4f(lightWorld_parallaxParams, scale[0], scale[1], image->upload_width, image->upload_height);


		GL_MBind(GL_TEXTURE0, image->texnum);

		if(normalMap)
			GL_MBind(GL_TEXTURE1, normalMap->texnum);

		GL_MBindCube(GL_TEXTURE2, r_lightCubeMap[currentShadowLight->filter]->texnum);

		if (r_imageAutoBump->value && normalMap == r_defBump)
		{
			glUniform1i(lightWorld_autoBump, 1);
			glUniform2f(lightWorld_autoBumpParams, r_imageAutoBumpScale->value, r_imageAutoSpecularScale->value);
		}
		else
			glUniform1i(lightWorld_autoBump, 0);

		if (surf->texInfo->flags & SURF_FLOWING)
		{
			scroll = -64 * ((r_newrefdef.time / 40.0) - (int)(r_newrefdef.time / 40.0));
			if (scroll == 0.0)
				scroll = -64.0;
		}

		if (!bmodel)
			glUniform1f(lightWorld_scroll, scroll);
		else
			glUniform1f(lightWorld_scroll, 0.0);
	}

	// create indexes
	if (numIndices == 0xffffffff)
		numIndices = 0;

	for (i = 0; i < nv - 2; i++)
	{
		indexArray[numIndices++] = surf->baseIndex;
		indexArray[numIndices++] = surf->baseIndex + i + 1;
		indexArray[numIndices++] = surf->baseIndex + i + 2;
	}

	*indeces = numIndices;

	return true;
}


 int lightSurfSort( const msurface_t **a, const msurface_t **b )
{
	return	(((*a)->texInfo->diffuse->texnum) + ((*a)->flags)) -
			(((*b)->texInfo->diffuse->texnum) + ((*b)->flags));
}


 void R_UpdateLightUniforms(bool bModel)
 {
	 mat4_t	entAttenMatrix, entSpotMatrix;

	glUniform1f(lightWorld_colorScale, r_textureColorScale->value);
	glUniform1i(lightWorld_ambient, (int)currentShadowLight->isAmbient);
	glUniform3fv(lightWorld_lightOrigin, 1, currentShadowLight->origin);
	glUniform4f(lightWorld_lightColor, currentShadowLight->color[0], currentShadowLight->color[1], currentShadowLight->color[2], 1.0);
	glUniform1i(lightWorld_fog, (int)currentShadowLight->isFog);
	glUniform1f(lightWorld_fogDensity, currentShadowLight->fogDensity);
	glUniform1i(lightWorld_parallaxType, (int)clamp(r_reliefMapping->value, 0.0f, 1.0f));
	glUniform1f(lightWorld_causticsIntens, r_causticIntens->value);

	 if (bModel)
		glUniform3fv(lightWorld_viewOrigin, 1, BmodelViewOrg);
	 else
		 glUniform3fv(lightWorld_viewOrigin, 1, r_origin);

	 if (!bModel){
		 glUniformMatrix4fv(lightWorld_mvp, 1, false, (const float *)r_newrefdef.modelViewProjectionMatrix);
		 glUniformMatrix4fv(lightWorld_attenMatrix, 1, false, (const float *)currentShadowLight->attenMatrix);
		 glUniformMatrix4fv(lightWorld_spotMatrix, 1, false, (const float *)currentShadowLight->spotMatrix);
	 }
	 else{
		 glUniformMatrix4fv(lightWorld_mvp, 1, false, (const float *)CURRENT_ENTITY->orMatrix);

		 Mat4_TransposeMultiply(CURRENT_ENTITY->matrix, currentShadowLight->attenMatrix, entAttenMatrix);
		 glUniformMatrix4fv(lightWorld_attenMatrix, 1, false, (const float *)entAttenMatrix);

		 Mat4_TransposeMultiply(CURRENT_ENTITY->matrix, currentShadowLight->spotMatrix, entSpotMatrix);
		 glUniformMatrix4fv(lightWorld_spotMatrix, 1, false, (const float *)entSpotMatrix);
	 }
	 
	 glUniform3f(lightWorld_spotParams, currentShadowLight->hotSpot, 1.f / (1.f - currentShadowLight->hotSpot), currentShadowLight->coneExp);

	 if (currentShadowLight->isCone) 
		 glUniform1i(lightWorld_spotLight, 1);
	 else
		 glUniform1i(lightWorld_spotLight, 0);

	 R_CalcCubeMapMatrix(bModel);
	 glUniformMatrix4fv(lightWorld_cubeMatrix, 1, false, (const float *)currentShadowLight->cubeMapMatrix);

 }


 /**
  * 
  */
static void GL_DrawDynamicLightPass(bool bmodel, bool caustics)
{
	bool newBatch = false, oldCaust = false;

	unsigned oldTex = 0xffffffff;
	unsigned oldFlag = 0xffffffff;
	unsigned numIndices = 0xffffffff;
	
	// setup program
	GL_BindProgram(lightWorldProgram, 0);
	R_UpdateLightUniforms(bmodel);

	qsort(interaction, numInteractionSurfs, sizeof(msurface_t*), (int (*)(const void *, const void *))lightSurfSort);

	for (int i = 0; i < numInteractionSurfs; ++i)
	{
		msurface_t* s = interaction[i];
		glpoly_t* poly = s->polys;

		if ((poly->lightTimestamp != r_lightTimestamp) || (s->visframe != r_framecount))
		{
			continue;
		}

		// flush batch (new texture or flag)
		if (s->texInfo->diffuse->texnum != oldTex || s->flags != oldFlag || caustics != oldCaust)
		{
			if (numIndices != 0xffffffff)
			{
				glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, indexArray);
				numIndices = 0xffffffff;
			}

			oldTex = s->texInfo->diffuse->texnum;
			oldFlag = s->flags;
			oldCaust = caustics;
			newBatch = true;
		}
		else
		{
			newBatch = false;
		}

		// fill new batch
		if (!R_FillLightBatch(s, newBatch, &numIndices, bmodel, caustics))
		{
			if (numIndices != 0xffffffff)
			{
				glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, indexArray);
				numIndices = 0xffffffff;
			}
		}
	}

	if (numIndices != 0xffffffff)
	{
		glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, indexArray);
	}

	GL_BindNullProgram();
}


/**
 * 
 */
static void GL_DrawStaticLightPass()
{
	msurface_t	*s;
	int			i;
	bool	newBatch;
	unsigned	oldTex = 0xffffffff;
	unsigned	oldFlag = 0xffffffff;
	unsigned	numIndices = 0xffffffff;

	// setup program
	GL_BindProgram(lightWorldProgram, 0);

	R_UpdateLightUniforms(false);

	for (i = 0; i < currentShadowLight->numInteractionSurfs; i++)
	{
		s = currentShadowLight->interaction[i];
		if ((s->visframe != r_framecount) || (s->ent)) continue;

		// flush batch (new texture or flag)
		if (s->texInfo->diffuse->texnum != oldTex || s->flags != oldFlag)
		{
			if (numIndices != 0xffffffff)
			{
				glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, indexArray);
				numIndices = 0xffffffff;
			}

			oldTex = s->texInfo->diffuse->texnum;
			oldFlag = s->flags;
			newBatch = true;
		}
		else
			newBatch = false;

		// fill new batch
		if (!R_FillLightBatch(s, newBatch, &numIndices, false, false))
		{
			if (numIndices != 0xffffffff)
			{
				glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, indexArray);
				numIndices = 0xffffffff;
			}
		}
	}
	// draw the rest
	if (numIndices != 0xffffffff) {
		glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, indexArray);
	}
	GL_BindNullProgram();
}


bool SurfInFrustum(msurface_t *s)
{
	/// \fixme	Currently breaks in certain circumstances. This is here to act as a sort of no-op
	return true;

	if (s->polys)
	{
		return !R_CullBox(s->mins, s->maxs);
	}

	return true;
}

/*
================
R_RecursiveWorldNode

================
*/
static void R_RecursiveWorldNode(mnode_t * node) {
	int c, side, sidebit;
	cplane_t *plane;
	msurface_t *surf, **mark;
	mleaf_t *pleaf;
	float dot;

	if (node->contents == CONTENTS_SOLID)
		return;					// solid

	if (node->visframe != r_visframecount)
		return;

	//if (R_CullBox(node->minmaxs, node->minmaxs + 3))
	//	return;

	// if a leaf node, draw stuff
	if (node->contents != -1)
	{
		pleaf = (mleaf_t *)node;

		// check for door connected areas
		if (r_newrefdef.areabits)
		{
			// not visible
			if (!(r_newrefdef.areabits[pleaf->area >> 3] & (1 << (pleaf->area & 7))))
			{
				return;
			}
		}

		mark = pleaf->firstmarksurface;
		c = pleaf->numMarkSurfaces;

		if (c)
		{
			do
			{
				if (SurfInFrustum(*mark))
				{
					(*mark)->visframe = r_framecount;
				}
			
				(*mark)->ent = nullptr;
				
				mark++;
			} while (--c);
		}

		return;
	}

	// node is just a decision point, so go down the apropriate sides
	// find which side of the node we are on
	plane = node->plane;

	switch (plane->type)
	{
	case PLANE_X:
		dot = modelorg[0] - plane->dist;
		break;
	
	case PLANE_Y:
		dot = modelorg[1] - plane->dist;
		break;
	
	case PLANE_Z:
		dot = modelorg[2] - plane->dist;
		break;
	
	default:
		dot = DotProduct(modelorg, plane->normal) - plane->dist;
		break;
	}

	if (dot >= 0)
	{
		side = 0;
		sidebit = 0;
	}
	else
	{
		side = 1;
		sidebit = MSURF_PLANEBACK;
	}

	// recurse down the children, front side first
	R_RecursiveWorldNode(node->children[side]);

	// draw stuff
	for (c = node->numsurfaces, surf = r_worldmodel->surfaces + node->firstsurface; c; c--, surf++)
	{
		if (surf->visframe != r_framecount) { continue; }
		if ((surf->flags & MSURF_PLANEBACK) != sidebit) { continue; } // wrong side

		if (surf->texInfo->flags & SURF_NODRAW) { continue; }
		else if (surf->texInfo->flags & SURF_SKY) { R_AddSkySurface(surf); } // just adds to visible sky bounds
		else if ((surf->texInfo->flags & SURF_ALPHA) && !(surf->flags & MSURF_LAVA) && !(surf->flags & MSURF_DRAWTURB))
		{
			// add to the translucent chain
			surf->texturechain = r_alpha_surfaces;
			r_alpha_surfaces = surf;
		}
		else
		{
			if ((surf->flags & MSURF_DRAWTURB) && !(surf->flags & MSURF_LAVA)) // add to the reflective chain
			{
				surf->texturechain = r_reflective_surfaces;
				r_reflective_surfaces = surf;
			}
			else // add to the ambient batch
			{
				scene_surfaces[num_scene_surfaces++] = surf;
			}
		}
	}

	// recurse down the back side
	R_RecursiveWorldNode(node->children[!side]);
}


bool R_MarkLightSurf(msurface_t *surf, bool world, worldShadowLight_t *light)
{
	cplane_t	*plane;
	float		dist;
	glpoly_t	*poly;


	if (surf->flags & MSURF_LAVA)
		goto hack;

	if ((surf->texInfo->flags & (SURF_ALPHA | SURF_SKY | SURF_WARP | SURF_NODRAW)) || (surf->flags & MSURF_DRAWTURB))
		return false;

hack: /// \fixme hack

	plane = surf->plane;
	poly = surf->polys;

	if (poly->lightTimestamp == r_lightTimestamp)
		return false;

	switch (plane->type)
	{
	case PLANE_X:
		dist = light->origin[0] - plane->dist;
		break;
	case PLANE_Y:
		dist = light->origin[1] - plane->dist;
		break;
	case PLANE_Z:
		dist = light->origin[2] - plane->dist;
		break;
	default:
		dist = DotProduct(light->origin, plane->normal) - plane->dist;
		break;
	}

	if (light->isFog && !light->isShadow)
		goto next;

	//the normals are flipped when surf_planeback is 1
	if (((surf->flags & MSURF_PLANEBACK) && (dist > 0)) ||
		(!(surf->flags & MSURF_PLANEBACK) && (dist < 0)))
		return false;

next: /// \fixme hack

	if (fabsf(dist) > light->maxRad)
		return false;

	if (world)
	{
		float	lbbox[6], pbbox[6];

		lbbox[0] = light->origin[0] - light->radius[0];
		lbbox[1] = light->origin[1] - light->radius[1];
		lbbox[2] = light->origin[2] - light->radius[2];
		lbbox[3] = light->origin[0] + light->radius[0];
		lbbox[4] = light->origin[1] + light->radius[1];
		lbbox[5] = light->origin[2] + light->radius[2];

		// surface bounding box
		pbbox[0] = surf->mins[0];
		pbbox[1] = surf->mins[1];
		pbbox[2] = surf->mins[2];
		pbbox[3] = surf->maxs[0];
		pbbox[4] = surf->maxs[1];
		pbbox[5] = surf->maxs[2];

		if (light->_cone && R_CullConeLight(&pbbox[0], &pbbox[3], light->frust))
			return false;

		if (!BoundsIntersect(&lbbox[0], &lbbox[3], &pbbox[0], &pbbox[3]))
			return false;
	}

	poly->lightTimestamp = r_lightTimestamp;

	return true;
}


void R_MarkLightCasting (mnode_t *node, bool precalc, worldShadowLight_t *light)
{
	cplane_t			*plane;
	float				dist;
	msurface_t			**surf;
	mleaf_t				*leaf;
	int					c, cluster;

	if (light->isNoWorldModel)
		return;

	if (node->contents != -1)
	{
		//we are in a leaf
		leaf = (mleaf_t *)node;
		cluster = leaf->cluster;

		if (!(light->vis[cluster>>3] & (1<<(cluster&7))))
			return;

		surf = leaf->firstmarksurface;

		for (c = 0; c < leaf->numMarkSurfaces; c++, surf++){

			if (R_MarkLightSurf((*surf), true, light)) {
				if(!precalc)
					interaction[numInteractionSurfs++] = (*surf);
				else
					light->interaction[light->numInteractionSurfs++] = (*surf);
			}
		}
		return;
	}

	plane = node->plane;
	dist = DotProduct (light->origin, plane->normal) - plane->dist;

	if (dist > light->maxRad)
	{
		R_MarkLightCasting (node->children[0], precalc, light);
		return;
	}
	if (dist < -light->maxRad)
	{
		R_MarkLightCasting (node->children[1], precalc, light);
		return;
	}

	R_MarkLightCasting (node->children[0], precalc, light);
	R_MarkLightCasting (node->children[1], precalc, light);
}

void R_DrawLightWorld()
{

	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		return;

	GL_StencilFunc(GL_EQUAL, 128, 255);
	GL_StencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	GL_StencilMask(0);
	GL_DepthFunc(GL_LEQUAL);

	glBindVertexArray(getVao().bsp_l);

	if (!currentShadowLight->isStatic) {
		r_lightTimestamp++;
		numInteractionSurfs = 0;
		R_MarkLightCasting(r_worldmodel->nodes, false, currentShadowLight);
		if (numInteractionSurfs > 0)
			GL_DrawDynamicLightPass(false, false);
	}
	else
		GL_DrawStaticLightPass();

	glBindVertexArray(0);
}


/*
=============
R_DrawBSP

=============
*/
void R_DrawBSP () {
	entity_t ent;

	if (!r_drawWorld->value)
		return;

	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		return;
	
	CURRENT_MODEL = r_worldmodel;

	VectorCopy(r_newrefdef.vieworg, modelorg);

	// auto cycle the world frame for texture animation
	memset(&ent, 0, sizeof(ent));
	ent.frame = (int) (r_newrefdef.time * 2);
	Mat3_Identity(ent.axis);
	CURRENT_ENTITY = &ent;

	gl_state.currenttextures[0] = gl_state.currenttextures[1] = -1;

	memset(gl_lms.lightmap_surfaces, 0, sizeof(gl_lms.lightmap_surfaces));
		
	R_ClearSkyBox();	

	glBindVertexArray(getVao().bsp_a);

	num_scene_surfaces = 0;
	R_RecursiveWorldNode(r_worldmodel->nodes);
	GL_DrawLightmappedPoly(false);

	glBindVertexArray(0);

	R_DrawSkyBox(true);
}

/*
=================
R_DrawInlineBModel

=================
*/
extern bool bmodelcaust = false;

void R_DrawBrushModel();

static void R_DrawInlineBModel () {
	int i;
	cplane_t *pplane;
	float dot;
	msurface_t *psurf;

	psurf = &CURRENT_MODEL->surfaces[CURRENT_MODEL->firstModelSurface];

	//
	// draw texture
	//

	for (i = 0; i < CURRENT_MODEL->numModelSurfaces; i++, psurf++) {
		// find which side of the node we are on
		pplane = psurf->plane;

		if (pplane->type < 3)
			dot = modelorg[pplane->type] - pplane->dist;
		else
			dot = DotProduct(modelorg, pplane->normal) - pplane->dist;

		// draw the polygon
		if (((psurf->flags & MSURF_PLANEBACK) && (dot < -BACKFACE_EPSILON))
			|| (!(psurf->flags & MSURF_PLANEBACK) && (dot > BACKFACE_EPSILON))) {

			if (psurf->visframe == r_framecount)	// reckless fix
				continue;

			/*===============================
			berserker - flares for brushmodels
			=================================*/
			psurf->visframe = r_framecount;
			psurf->ent = CURRENT_ENTITY;
			// ================================

			if (psurf->texInfo->flags & SURF_ALPHA)
				continue;
			
			if (!(psurf->texInfo->flags & MSURF_DRAWTURB))
				scene_surfaces[num_scene_surfaces++] = psurf;
		}
	}

}


static void R_DrawInlineBModel2()
{
	int i;
	cplane_t *pplane;
	float dot;
	msurface_t *psurf;

	psurf = &CURRENT_MODEL->surfaces[CURRENT_MODEL->firstModelSurface];

	// draw texture
	for (i = 0; i < CURRENT_MODEL->numModelSurfaces; i++, psurf++)
	{
		// find which side of the node we are on
		pplane = psurf->plane;
		dot = DotProduct(modelorg, pplane->normal) - pplane->dist;

		// draw the polygon
		if (((psurf->flags & MSURF_PLANEBACK) && (dot < -BACKFACE_EPSILON)) || (!(psurf->flags & MSURF_PLANEBACK) && (dot > BACKFACE_EPSILON))) {

			if (psurf->visframe == r_framecount) // reckless fix
				continue;

			if (psurf->texInfo->flags & SURF_ALPHA)
				continue;

			if (psurf->flags & MSURF_DRAWTURB)
				R_DrawWaterPolygons(psurf, true);
		}
	}
}


static void R_DrawInlineBModel3()
{
	int i;
	cplane_t *pplane;
	float dot;
	msurface_t *psurf;

	psurf = &CURRENT_MODEL->surfaces[CURRENT_MODEL->firstModelSurface];

	// draw texture
	for (i = 0; i < CURRENT_MODEL->numModelSurfaces; i++, psurf++)
	{
		// find which side of the node we are on
		pplane = psurf->plane;
		dot = DotProduct(modelorg, pplane->normal) - pplane->dist;

		// draw the polygon
		if (((psurf->flags & MSURF_PLANEBACK) && (dot < -BACKFACE_EPSILON)) || (!(psurf->flags & MSURF_PLANEBACK) && (dot > BACKFACE_EPSILON)))
		{
			if (psurf->visframe != r_framecount)
				continue;

			if (psurf->texInfo->flags & SURF_ALPHA)
			{
				psurf->texturechain = r_alpha_surfaces;
				r_alpha_surfaces = psurf;
			}
			else if (psurf->texInfo->flags & SURF_WARP)
			{
				psurf->texturechain = r_reflective_surfaces;
				r_reflective_surfaces = psurf;
			}
		}
	}
}


/**
 * R_DrawBrushModel
 */
int CL_PMpointcontents(vec3_t point);

void R_DrawBrushModel()
{
	vec3_t		mins, maxs, tmp;
	int			i;
	bool	rotated;

	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		return;
	if (CURRENT_MODEL->numModelSurfaces == 0)
		return;

	gl_state.currenttextures[0] = gl_state.currenttextures[1] = -1;

	if (CURRENT_ENTITY->angles[0] || CURRENT_ENTITY->angles[1] || CURRENT_ENTITY->angles[2])
	{
		rotated = true;

		for (i = 0; i < 3; i++)
		{
			mins[i] = CURRENT_ENTITY->origin[i] - CURRENT_MODEL->radius;
			maxs[i] = CURRENT_ENTITY->origin[i] + CURRENT_MODEL->radius;
		}
	}
	else
	{
		rotated = false;
		VectorAdd(CURRENT_ENTITY->origin, CURRENT_MODEL->mins, mins);
		VectorAdd(CURRENT_ENTITY->origin, CURRENT_MODEL->maxs, maxs);
	}

	if (R_CullBox(mins, maxs))
		return;

	memset(gl_lms.lightmap_surfaces, 0, sizeof(gl_lms.lightmap_surfaces));


	VectorSubtract(r_newrefdef.vieworg, CURRENT_ENTITY->origin, modelorg);

	if (rotated)
	{
		vec3_t temp;
		vec3_t forward, right, up;

		VectorCopy(modelorg, temp);
		AngleVectors(CURRENT_ENTITY->angles, forward, right, up);
		modelorg[0] = DotProduct(temp, forward);
		modelorg[1] = -DotProduct(temp, right);
		modelorg[2] = DotProduct(temp, up);
	}

	R_SetupEntityMatrix(CURRENT_ENTITY);

	//Put camera into model space view angle for bmodels parallax
	VectorSubtract(r_origin, CURRENT_ENTITY->origin, tmp);
	Mat3_TransposeMultiplyVector(CURRENT_ENTITY->axis, tmp, BmodelViewOrg);
	GL_DepthMask(0);
	R_DrawInlineBModel2();

	glBindVertexArray(getVao().bsp_a);

	num_scene_surfaces = 0;
	R_DrawInlineBModel();
	GL_DrawLightmappedPoly(true);

	glBindVertexArray(0);

	GL_DepthMask(true);
}


/*
=================
R_DrawBrushModelRA

=================
*/
void R_DrawBrushModelRA () {
	vec3_t		mins, maxs, tmp;
	int			i;
	bool	rotated;

	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		return;
	if (CURRENT_MODEL->numModelSurfaces == 0)
		return;
	
	gl_state.currenttextures[0] = gl_state.currenttextures[1] = -1;

	if (CURRENT_ENTITY->angles[0] || CURRENT_ENTITY->angles[1] || CURRENT_ENTITY->angles[2]) {
		rotated = true;

		for (i = 0; i < 3; i++) {
			mins[i] = CURRENT_ENTITY->origin[i] - CURRENT_MODEL->radius;
			maxs[i] = CURRENT_ENTITY->origin[i] + CURRENT_MODEL->radius;
		}
	}
	else {
		rotated = false;
		VectorAdd(CURRENT_ENTITY->origin, CURRENT_MODEL->mins, mins);
		VectorAdd(CURRENT_ENTITY->origin, CURRENT_MODEL->maxs, maxs);
	}

	if (R_CullBox(mins, maxs))
		return;

	VectorSubtract(r_newrefdef.vieworg, CURRENT_ENTITY->origin, modelorg);

	if (rotated) {
		vec3_t temp;
		vec3_t forward, right, up;

		VectorCopy(modelorg, temp);
		AngleVectors(CURRENT_ENTITY->angles, forward, right, up);

		modelorg[0] = DotProduct(temp, forward);
		modelorg[1] = -DotProduct(temp, right);
		modelorg[2] = DotProduct(temp, up);
	}

	R_SetupEntityMatrix(CURRENT_ENTITY);

	// put camera into model space view angle for bmodels parallax
	VectorSubtract(r_origin, CURRENT_ENTITY->origin, tmp);
	Mat3_TransposeMultiplyVector(CURRENT_ENTITY->axis, tmp, BmodelViewOrg);

	R_DrawInlineBModel3();
	R_DrawChainsRA(true);
}

void R_MarkBrushModelSurfaces () {
	int			i;
	msurface_t	*psurf;
	Model		*clmodel;

	clmodel = CURRENT_ENTITY->model;
	psurf = &clmodel->surfaces[clmodel->firstModelSurface];


	for (i=0 ; i<clmodel->numModelSurfaces ; i++, psurf++){

		if (R_MarkLightSurf (psurf, false, currentShadowLight))
			interaction[numInteractionSurfs++] = psurf;
	}
}

void R_DrawLightBrushModel () {
	vec3_t		mins, maxs, org;
	int			i;
	bool	rotated;
	vec3_t		tmp, oldLight;
	bool	caustics;

	if (r_newrefdef.rdflags & RDF_NOWORLDMODEL)
		return;

	if (CURRENT_MODEL->numModelSurfaces == 0)
		return;

	if (CURRENT_ENTITY->angles[0] || CURRENT_ENTITY->angles[1] || CURRENT_ENTITY->angles[2]) {
		rotated = true;
		for (i = 0; i < 3; i++) {
			mins[i] = CURRENT_ENTITY->origin[i] - CURRENT_MODEL->radius;
			maxs[i] = CURRENT_ENTITY->origin[i] + CURRENT_MODEL->radius;
		}
	} else {
		rotated = false;
		VectorAdd(CURRENT_ENTITY->origin, CURRENT_MODEL->mins, mins);
		VectorAdd(CURRENT_ENTITY->origin, CURRENT_MODEL->maxs, maxs);
	}

	if(currentShadowLight->spherical){
		if(!BoundsAndSphereIntersect(mins, maxs, currentShadowLight->origin, currentShadowLight->radius[0]))
			return;
	}else{
		if(!BoundsIntersect(mins, maxs, currentShadowLight->mins, currentShadowLight->maxs))
			return;
	}

	R_SetupEntityMatrix(CURRENT_ENTITY);
	
	//Put camera into model space view angle for bmodels parallax
	VectorSubtract(r_origin, CURRENT_ENTITY->origin, tmp);
	Mat3_TransposeMultiplyVector(CURRENT_ENTITY->axis, tmp, BmodelViewOrg);

	VectorCopy(currentShadowLight->origin, oldLight);
	VectorSubtract(currentShadowLight->origin, CURRENT_ENTITY->origin, tmp);
	Mat3_TransposeMultiplyVector(CURRENT_ENTITY->axis, tmp, currentShadowLight->origin);

	caustics = false;
	CURRENT_ENTITY->minmax[0] = mins[0];
	CURRENT_ENTITY->minmax[1] = mins[1];
	CURRENT_ENTITY->minmax[2] = mins[2];
	CURRENT_ENTITY->minmax[3] = maxs[0];
	CURRENT_ENTITY->minmax[4] = maxs[1];
	CURRENT_ENTITY->minmax[5] = maxs[2];

	VectorSet(org, CURRENT_ENTITY->minmax[0], CURRENT_ENTITY->minmax[1], CURRENT_ENTITY->minmax[5]);
	if (CL_PMpointcontents2(org, CURRENT_MODEL) & MASK_WATER)
		caustics = true;
	else
	{
		VectorSet(org, CURRENT_ENTITY->minmax[3], CURRENT_ENTITY->minmax[1], CURRENT_ENTITY->minmax[5]);
		if (CL_PMpointcontents2(org, CURRENT_MODEL) & MASK_WATER)
			caustics = true;
		else
		{
			VectorSet(org, CURRENT_ENTITY->minmax[0], CURRENT_ENTITY->minmax[4], CURRENT_ENTITY->minmax[5]);
			if (CL_PMpointcontents2(org, CURRENT_MODEL) & MASK_WATER)
				caustics = true;
			else
			{
				VectorSet(org, CURRENT_ENTITY->minmax[3], CURRENT_ENTITY->minmax[4], CURRENT_ENTITY->minmax[5]);
				if (CL_PMpointcontents2(org, CURRENT_MODEL) & MASK_WATER)
					caustics = true;
			}
		}
	}
	
	GL_StencilFunc(GL_EQUAL, 128, 255);
	GL_StencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	GL_StencilMask(0);
	GL_DepthFunc(GL_LEQUAL);

	glBindVertexArray(getVao().bsp_l);

	r_lightTimestamp++;
	numInteractionSurfs = 0;
	R_MarkBrushModelSurfaces();
	if(numInteractionSurfs > 0)
		GL_DrawDynamicLightPass(true, caustics);
	
	glBindVertexArray(0);

	VectorCopy(oldLight, currentShadowLight->origin);
}


/** 
 * Mark the leaves and nodes that are in the PVS for the current cluster
 */
void R_MarkLeaves()
{
	byte	*vis;
	byte	fatvis[MAX_MAP_LEAFS / 8];
	mnode_t	*node;
	int		i, c;
	mleaf_t	*leaf;
	int		cluster;

	if (r_oldviewcluster == r_viewcluster && r_oldviewcluster2 == r_viewcluster2 /*&& !r_novis->value*/ && r_viewcluster != -1)
		return;

	r_visframecount++;
	r_oldviewcluster = r_viewcluster;
	r_oldviewcluster2 = r_viewcluster2;

	if (/*r_novis->value ||*/ r_viewcluster == -1 || !r_worldmodel->vis)
	{
		// mark everything
		for (i = 0; i < r_worldmodel->numLeafs; i++)
			r_worldmodel->leafs[i].visframe = r_visframecount;
		for (i = 0; i < r_worldmodel->numNodes; i++)
			r_worldmodel->nodes[i].visframe = r_visframecount;
		memset(&viewvis, 0xff, (r_worldmodel->numLeafs + 7) >> 3);	// all visible
		return;
	}

	vis = BSP_ClusterPVS(r_viewcluster, r_worldmodel);
	// may have to combine two clusters because of solid water boundaries
	if (r_viewcluster2 != r_viewcluster)
	{
		memcpy(fatvis, vis, (r_worldmodel->numLeafs + 7) >> 3);
		vis = BSP_ClusterPVS(r_viewcluster2, r_worldmodel);
		c = (r_worldmodel->numLeafs + 31) / 32;
		for (i = 0; i < c; i++)
			((int *)fatvis)[i] |= ((int *)vis)[i];
		vis = fatvis;
	}

	memcpy(&viewvis, vis, (r_worldmodel->numLeafs + 7) >> 3);

	for (i = 0, leaf = r_worldmodel->leafs; i < r_worldmodel->numLeafs; i++, leaf++)
	{
		cluster = leaf->cluster;
		if (cluster == -1)
			continue;
		if (vis[cluster >> 3] & (1 << (cluster & 7)))
		{
			node = (mnode_t *)leaf;
			do
			{
				if (node->visframe == r_visframecount)
					break;
				node->visframe = r_visframecount;
				node = node->parent;
			} while (node);
		}
	}
}
